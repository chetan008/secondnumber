
import UIKit
import Foundation



@objc public class Tagging: UIView {
    
    // MARK: - Apperance
    
    @objc open var cornerRadius: CGFloat {
        get { return textView.layer.cornerRadius }
        set { textView.layer.cornerRadius = newValue }
    }
    
    @objc open var borderWidth: CGFloat {
        get { return textView.layer.borderWidth }
        set { textView.layer.borderWidth = newValue }
    }
    
    @objc open var borderColor: CGColor? {
        get { return textView.layer.borderColor }
        set { textView.layer.borderColor = newValue }
    }
    
    @objc open var textInset: UIEdgeInsets {
        get { return textView.textContainerInset }
        set { textView.textContainerInset = newValue }
    }
    
    @objc override open var backgroundColor: UIColor? {
        get { return textView.backgroundColor }
        set { textView.backgroundColor = newValue }
    }
    
    // MARK: - Properties
    
    @objc open var symbol: String = "@"
    @objc open var tagableList: [String]?
   @objc  open var defaultAttributes: [NSAttributedString.Key: Any] = {
        return [NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.underlineStyle: NSNumber(value: 0)]
    }()
   @objc open var symbolAttributes: [NSAttributedString.Key: Any] = {
        return [NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.underlineStyle: NSNumber(value: 0)]
    }()
   @objc open var taggedAttributes: [NSAttributedString.Key: Any] = {return [NSAttributedString.Key.underlineStyle: NSNumber(value: 1)]}()
    
    @objc  public private(set) var taggedList: [TaggingModel] = []
  @objc   public weak var dataSource: TaggingDataSource?
    
   @objc private var currentTaggingText: String? {
        didSet {
            guard let currentTaggingText = currentTaggingText, let tagableList = tagableList else {return}
            
//            let matchedTagableList = tagableList.filter {
//                $0.contains(currentTaggingText.lowercased()) || $0.contains(currentTaggingText.uppercased())
//            }
            
            
            
            let matchedTagableList = tagableList.filter { $0.localizedCaseInsensitiveContains(currentTaggingText) }
            
            dataSource?.tagging(self, didChangedTagableList: matchedTagableList)
        }
    }
//     private var currentTaggingRange: NSRange?
//   @objc  private var tagRegex: NSRegularExpression! {return try! NSRegularExpression(pattern: "\(symbol)(\\w+)")}
    
    private var currentTaggingRange: NSRange?
    @objc  private var tagRegex: NSRegularExpression! {return try! NSRegularExpression(pattern: "\(symbol)([^\\s\\K]+)") }
    
    
    
//NSRegularExpression(pattern: "\(symbol)(.*)")
    // MARK: - UI Components
    
   @objc public let textView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    // MARK: - Con(De)structor
    
   @objc public init() {
        super.init(frame: .zero)
        
        commonSetup()
    }
    
    @objc public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonSetup()
    }
    
    // MARK: - Public methods
    
   @objc public func updateTaggedList(allText: String, tagText: String) {
        guard let range = currentTaggingRange else {return}
        
        let origin = (allText as NSString).substring(with: range)
        let tag = tagFormat(tagText)
        let replace = tag.appending(" ")
        let changed = (allText as NSString).replacingCharacters(in: range, with: replace)
    let tagRange = NSMakeRange(range.location, tag.utf16.count)
        
    taggedList.append(TaggingModel(text1: tagText, range1: tagRange))
        for i in 0..<taggedList.count-1 {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location > tagRange.location {
                location += replace.count - origin.count
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        textView.text = changed
        updateAttributeText(selectedLocation: range.location+replace.count)
        dataSource?.tagging(self, didChangedTaggedList: taggedList)
    }
    
    // MARK: - Private methods
    
  @objc  private func commonSetup() {
        setProperties()
        addSubview(textView)
        layout()
    }
    
   @objc private func setProperties() {
        backgroundColor = .clear
        textView.delegate = self
    }
    
   @objc private func tagFormat(_ text: String) -> String {
        return symbol.appending(text)
    }
    
}

// MARK: - Layout

@objc extension Tagging {
    
    @objc private func layout() {
        addConstraints(
            [NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0),
             NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0),
             NSLayoutConstraint(item: textView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0),
             NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0)])
    }
    
}

// MARK: - UITextViewDelegate

@objc extension Tagging: UITextViewDelegate {
    
   @objc public func textViewDidChange(_ textView: UITextView) {
        tagging(textView: textView)
        updateAttributeText(selectedLocation: textView.selectedRange.location)
    
    if(!textView.text.contains("@"))
    {
        NotificationCenter.default.post(name: Notification.Name("HideTagTable"), object: nil, userInfo: ["hidetable":"true"])

    }
    }
    
   @objc public func textViewDidChangeSelection(_ textView: UITextView) {
        tagging(textView: textView)
    }
    
   @objc public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        updateTaggedList(range: range, textCount: text.utf16.count)
    
    
        return true
    }
    
    @objc public func textViewDidBeginEditing(_ textView: UITextView) {
        
        var tarsnferStr = "";
        if (UserDefaults.standard.value(forKey: "iSTransferCallForTag") != nil) {
            tarsnferStr = UserDefaults.standard.value(forKey: "iSTransferCallForTag") as! String
        }
         
        var extensionStr = "";
        if (UserDefaults.standard.value(forKey: "ExtentionCall") != nil) {
             extensionStr = UserDefaults.standard.value(forKey: "ExtentionCall") as! String
        }
        
        if (UserDefaults.standard.value(forKey: "isTaggingInPlan") as! Int  == 1 && extensionStr != "true" && tarsnferStr != "true") {
            
            if textView.text == "Add notes and add tags by typing @" {
                textView.text = ""
                //textView.textColor = UIColor.black
            }
        }
        else
        {
            if textView.text == "Add notes..." {
                textView.text = ""
                //textView.textColor = UIColor.black
            }
        }
        
    }
        
}

// MARK: - Tagging Algorithm

extension Tagging {
    
    private func matchedData(taggingCharacters: [Character], selectedLocation: Int, taggingText: String) -> (NSRange?, String?) {
        var matchedRange: NSRange?
        var matchedString: String?
        let tag = String(taggingCharacters.reversed())
        let textRange = NSMakeRange(selectedLocation-tag.count, tag.count)
        
       
        
        guard tag == symbol else {
            let matched = tagRegex.matches(in: taggingText, options: .reportCompletion, range: textRange)
            if matched.count > 0, let range = matched.last?.range {
                matchedRange = range
                matchedString = (taggingText as NSString).substring(with: range).replacingOccurrences(of: symbol, with: "")
            }
            return (matchedRange, matchedString)
        }
        
        matchedRange = textRange
        matchedString = symbol
        return (matchedRange, matchedString)
        
    }
    
    private func tagging(textView: UITextView) {
        
        let selectedLocation = textView.selectedRange.location
        let taggingText = (textView.text as NSString).substring(with: NSMakeRange(0, selectedLocation))
        let space: Character = " "
        let lineBrak: Character = "\n"
        var tagable: Bool = false
        var characters: [Character] = []
        
        print("reversed array : \(Array(taggingText).reversed())")
        
        for char in Array(taggingText).reversed() {
            if char == symbol.first {
                characters.append(char)
                tagable = true
                break
            } else if char == space || char == lineBrak {
                tagable = false
                break
            }
            characters.append(char)
        }
        
        guard tagable else {
            currentTaggingRange = nil
            currentTaggingText = nil
            return
        }
        
        let data = matchedData(taggingCharacters: characters, selectedLocation: selectedLocation, taggingText: taggingText)
        currentTaggingRange = data.0
        currentTaggingText = data.1
    }
    
    private func updateAttributeText(selectedLocation: Int) {
        let attributedString = NSMutableAttributedString(string: textView.text)
        attributedString.addAttributes(defaultAttributes, range: NSMakeRange(0, textView.text.utf16.count))
        taggedList.forEach { (model) in
            let symbolAttributesRange = NSMakeRange(model.range.location, symbol.count)
            let taggedAttributesRange = NSMakeRange(model.range.location+1, model.range.length-1)
            
            attributedString.addAttributes(symbolAttributes, range: symbolAttributesRange)
            attributedString.addAttributes(taggedAttributes, range: taggedAttributesRange)
        }
        
        textView.attributedText = attributedString
        textView.selectedRange = NSMakeRange(selectedLocation, 0)
    }
    
   @objc public func updateTaggedList(range: NSRange, textCount: Int) {
        taggedList = taggedList.filter({ (model) -> Bool in
            if model.range.location < range.location && range.location < model.range.location+model.range.length {
                return false
            }
            if range.length > 0 {
                if range.location <= model.range.location && model.range.location < range.location+range.length {
                    return false
                }
            }
            return true
        })
        
    
        for i in 0..<taggedList.count {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location >= range.location {
                if range.length > 0 {
                    if textCount > 1 {
                        location += textCount - range.length
                    } else {
                        location -= range.length
                    }
                } else {
                    location += textCount
                }
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        currentTaggingText = nil
        dataSource?.tagging(self, didChangedTaggedList: taggedList)
    }
    
}
