import Foundation





@objc public class TaggingModel: NSObject {
//    private override init() {}
    
   @objc public var text: String = ""
   @objc public var range: NSRange = NSMakeRange(0, 0)
    
    init(text1: String,range1 : NSRange) {
        
        text = text1;
        range = range1;
    }

   public func getTagString() -> String { return text}
   public func getTagRange() -> NSRange { return range}
    
    
//   public class func parsedText() -> String { return text }
//   public class func parsedRange() -> NSRange { return range }

}

// public struct TaggingModel {
//     public var text: String
//     public var range: NSRange
//}

