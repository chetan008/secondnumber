//
//  PopUpViewController.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
NS_ASSUME_NONNULL_BEGIN

@interface PopUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starHeight;
@property (strong, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UITextField *txtOtherComment;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIButton *checkBox;

@property (weak, nonatomic) IBOutlet UIButton *firstCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *secondCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *thirdCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *fourthCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *fifthCheckbox;

@property (weak, nonatomic) IBOutlet UIView *viewCheckBoxPopup;
@property (weak, nonatomic) IBOutlet UIView *viewStarPopup;
@property NSString *strIncOutCall;
@property NSString *incOutNumb;
@property NSString *originalToNum;
@property (strong, nonatomic) NSString *strDisplayName;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@end

NS_ASSUME_NONNULL_END
