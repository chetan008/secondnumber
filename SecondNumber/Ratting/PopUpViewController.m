//
//  PopUpViewController.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "PopUpViewController.h"
#import "Constants.h"
#import "UtilsClass.h"
#import "Processcall.h"
#import "WebApiController.h"
#import "CallReminderPopUp.h"
#import "AppDelegate.h"
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define IS_IPHONE5 ([UIScreen mainScreen].bounds.size.height==568)
#import "Constant.h"

@interface PopUpViewController ()<UITextFieldDelegate,EDStarRatingProtocol>
{
    WebApiController *obj;
}
@property BOOL checkBoxSelected;
@property BOOL firstBoxSelected;
@property BOOL secondBoxSelected;
@property BOOL thirdBoxSelected;
@property BOOL fourthBoxSelected;
@property BOOL fifthBoxSelected;

@property NSString *firstStr;
@property NSString *secondStr;
@property NSString *thirdStr;
@property NSString *fourthStr;
@property NSString *fifthStr;

@property (strong,nonatomic) NSArray *colors;


@end




@implementation PopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtOtherComment.delegate = self;
    
    if (IS_IPHONE5) {
        _starHeight.constant = 10;
    } else {
        _starHeight.constant = 50;
    }
    
    [self starRatting];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
}

-(void)starRatting {
    
    _viewCheckBoxPopup.hidden = true;
    _viewStarPopup.hidden = false;
    _starRating.currentVC = @"rattingVC";
    _starRating.backgroundColor  = [UIColor whiteColor];
    _starRating.starImage = [[UIImage imageNamed:@"starselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starRating.starHighlightedImage = [[UIImage imageNamed:@"starselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starRating.maxRating = 5.0;
    _starRating.delegate = self;
    _starRating.horizontalMargin = 10.0;
    _starRating.editable=YES;
    _starRating.rating= 4;
    _starRating.displayMode=EDStarRatingDisplayHalf;
    [_starRating  setNeedsDisplay];
    self.colors = @[ [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f], [UIColor colorWithRed:1.0f green:0.22f blue:0.22f alpha:1.0f], [UIColor colorWithRed:0.27f green:0.85f blue:0.46f alpha:1.0f], [UIColor colorWithRed:0.35f green:0.35f blue:0.81f alpha:1.0f]];
    _starRating.tintColor = self.colors[0];
    
    
    // 20x20 is the size of the checkbox that you want
    // create 2 images sizes 20x20 , one empty square and
    // another of the same square with the checkmark in it
    // Create 2 UIImages with these new images, then:
    
    [_checkBox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_checkBox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_checkBox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateHighlighted];
    _checkBox.adjustsImageWhenHighlighted=YES;
    [_checkBox addTarget:self action:@selector(checkboxSelected:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)checkboxSelected:(id)sender
{
    _checkBoxSelected = !_checkBoxSelected; /* Toggle */
    [_checkBox setSelected:_checkBoxSelected];
    if (_checkBox.isSelected) {
        [[NSUserDefaults standardUserDefaults] setValue:@"true" forKey:INCOUTCALL];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:INCOUTCALL];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)checkBoxPopup {
    _viewCheckBoxPopup.hidden = false;
    _viewStarPopup.hidden = true;
    
    [_firstCheckbox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_firstCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_firstCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateHighlighted];
    _firstCheckbox.adjustsImageWhenHighlighted=YES;
    [_firstCheckbox addTarget:self action:@selector(firstSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [_secondCheckbox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_secondCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_secondCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateHighlighted];
    _secondCheckbox.adjustsImageWhenHighlighted=YES;
    [_secondCheckbox addTarget:self action:@selector(secondSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [_thirdCheckbox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_thirdCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_thirdCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateHighlighted];
    _thirdCheckbox.adjustsImageWhenHighlighted=YES;
    [_thirdCheckbox addTarget:self action:@selector(thirdSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [_fourthCheckbox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_fourthCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_fourthCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateHighlighted];
    _fourthCheckbox.adjustsImageWhenHighlighted=YES;
    [_fourthCheckbox addTarget:self action:@selector(fourthSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [_fifthCheckbox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_fifthCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_fifthCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateHighlighted];
    _fifthCheckbox.adjustsImageWhenHighlighted=YES;
    [_fifthCheckbox addTarget:self action:@selector(fifthSelected:) forControlEvents:UIControlEventTouchUpInside];
    
}




-(void)firstSelected:(id)sender
{
    _firstBoxSelected = !_firstBoxSelected; /* Toggle */
    [_firstCheckbox setSelected:_firstBoxSelected];
    if (_firstCheckbox.isSelected) {
        _firstStr = @"I couldn't hear the other person,";
    }else{
        _firstStr = @"";
    }
}

-(void)secondSelected:(id)sender
{
    _secondBoxSelected = !_secondBoxSelected; /* Toggle */
    [_secondCheckbox setSelected:_secondBoxSelected];
    if (_secondCheckbox.isSelected) {
        _secondStr = @"Other person couldn't hear me,";
    }else{
        _secondStr = @"";
    }
}

-(void)thirdSelected:(id)sender
{
    _thirdBoxSelected = !_thirdBoxSelected; /* Toggle */
    [_thirdCheckbox setSelected:_thirdBoxSelected];
    if (_thirdCheckbox.isSelected) {
        _thirdStr = @"The voice was breaking up,";
    }else{
        _thirdStr = @"";
    }
}

-(void)fourthSelected:(id)sender
{
    _fourthBoxSelected = !_fourthBoxSelected; /* Toggle */
    [_fourthCheckbox setSelected:_fourthBoxSelected];
    if (_fourthCheckbox.isSelected) {
        _fourthStr = @"There was some lag,";
    }else{
        _fourthStr = @"";
    }
}

-(void)fifthSelected:(id)sender
{
    _fifthBoxSelected = !_fifthBoxSelected; /* Toggle */
    [_fifthCheckbox setSelected:_fifthBoxSelected];
    if (_fifthCheckbox.isSelected) {
        _fifthStr = @"Voice was echoing,";
    }else{
        _fifthStr = @"";
    }
}

- (IBAction)notNowClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSString *numbVer = [Default valueForKey:IS_CALL_REMINDER];
    int num = [numbVer intValue];
    if (num == 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self reminderPopup];
        });
    }
    
    
}

- (IBAction)submitClicked:(id)sender {
    if (_firstCheckbox.isSelected || _secondCheckbox.isSelected || _thirdCheckbox.isSelected || _fourthCheckbox.isSelected || _fifthCheckbox.isSelected || _txtOtherComment.text.length > 0){
        [self submintReview];
    } else {
        //        [BasicStuff showAlert:@"CallHippo" message:@"Please Give Review For Better Improvment."];
        [UtilsClass showAlert:@"Please Give Review For Better Improvment." contro:self];
    }
    
}

- (IBAction)btnCheckSubmit:(id)sender {
    if (_starRating.rating <= 2.5) {
        [self checkBoxPopup];
    }else {
        _firstStr = @"";
        _secondStr = @"";
        _thirdStr = @"";
        _fourthStr = @"";
        _fifthStr = @"";
        _txtOtherComment.text = @"";
        [self submintReview];
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtOtherComment resignFirstResponder];
    return true;
}


-(void)submintReview
{
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"rating/%@/add",userID];
    //NSLog(@"URL : %@",url);
    //NSLog(@"ratingStar : %f",_starRating.rating);
    //NSLog(@"reason : %@",[NSString stringWithFormat:@"%@%@%@%@%@%@",_firstStr,_secondStr,_thirdStr,_fourthStr,_fifthStr,_txtOtherComment.text]);
    //NSLog(@"callType : %@",_strIncOutCall);
    //NSLog(@"userId : %@",userID);
    NSDictionary *passDict = @{@"ratingStar":[NSString stringWithFormat:@"%f",_starRating.rating],
                               @"reason":[NSString stringWithFormat:@"%@%@%@%@%@%@",_firstStr,_secondStr,_thirdStr,_fourthStr,_fifthStr,_txtOtherComment.text],
                               @"otherComment":@"",
                               @"callType":_strIncOutCall,
                               @"userId":userID};
    //NSLog(@"Dictonary : %@",passDict);
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    
}

- (void)login:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"ratting response1 %@",response);
    [self dismissViewControllerAnimated:true completion:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************18  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
      NSString *numbVer = [Default valueForKey:IS_CALL_REMINDER];
      int num = [numbVer intValue];
      if (num == 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          [self reminderPopup];
        });
      }
        
    }
//    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//        [self dismissViewControllerAnimated:true completion:nil];
//    } else {
//        @try {
//            if (response1 != (id)[NSNull null] && response1 != nil ){
//                [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
//            }
//        }
//        @catch (NSException *exception) {
//        }
//
//    }
}

-(void)reminderPopup {
    
    if (@available(iOS 13, *))
    {
        //    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
        //    alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CallReminderPopUp *vc = [storyboard instantiateViewControllerWithIdentifier:@"CallReminderPopUp"];
        vc.incOutNumb = _incOutNumb;
        vc.strDisplayName = _strDisplayName;
        vc.originalToNum = vc.incOutNumb;
        //        if([_CallStatus isEqualToString: OUTGOING])
        //        {
        //            vc.strIncOutCall = @"Outgoing";
        //        }
        //        else
        //        {
        //            vc.strIncOutCall = @"Incoming";
        //        }
        
        //    vc.view.backgroundColor = [UIColor clearColor];
        //    self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        
        if (IS_IPAD) {
            
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
            //                        [self presentViewController:vc animated:YES completion:nil];
        } else {
            //        vc.modalPresentationStyle = UIModalPresentationPopover;
            [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
            vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
            //                        [self presentViewController:vc animated:YES completion:nil];
        }
        
        
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
    else
    {
        
        UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CallReminderPopUp *vc = [storyboard instantiateViewControllerWithIdentifier:@"CallReminderPopUp"];
        vc.incOutNumb = _incOutNumb;
        vc.strDisplayName = _strDisplayName;
        vc.originalToNum = vc.incOutNumb;
        
        //        if([_CallStatus isEqualToString: OUTGOING])
        //        {
        //            vc.strIncOutCall = @"Outgoing";
        //        }
        //        else
        //        {
        //            vc.strIncOutCall = @"Incoming";
        //        }
        
        //    vc.view.backgroundColor = [UIColor clearColor];
        //    self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        
        if (IS_IPAD) {
            
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
            //                        [self presentViewController:vc animated:YES completion:nil];
        } else {
            //        vc.modalPresentationStyle = UIModalPresentationPopover;
            [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
            vc.modalPresentationStyle = UIModalPresentationPopover;
            //                        [self presentViewController:vc animated:YES completion:nil];
        }
        
        
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
    

}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

@end
