//
//  NumbersListVC.h
//  SecondNumber
//
//  Created by Apple on 25/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NumbersListVC : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (retain,nonatomic) IBOutlet UITableView *numberTbl;
@property (retain,nonatomic) IBOutlet UIButton *btn_addNumber;
@property (retain,nonatomic) IBOutlet UISearchBar *searchBar;
@property (retain,nonatomic) IBOutlet UILabel *lbl_noNumber;


@end

NS_ASSUME_NONNULL_END
