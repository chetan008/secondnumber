//
//  SubscribeViewController.h
//  SubscribeViewController
//
//  Created by Apple on 18/11/21.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubscribeViewController : UIViewController <SKProductsRequestDelegate,SKPaymentTransactionObserver,WKUIDelegate,WKNavigationDelegate>

@property (strong, nonatomic) IBOutlet UIButton *BtnSubscribe;
@property (strong, nonatomic) IBOutlet UIButton *BtnFreeTrial;

@property (strong, nonatomic) IBOutlet UIView *view_subscribe;

@property (strong, nonatomic) IBOutlet WKWebView *webview;



@end

NS_ASSUME_NONNULL_END
