//
//  BuyNumbersListVC.m
//  SecondNumber
//
//  Created by Apple on 25/11/21.
//

#import "BuyNumbersListVC.h"
#import "Tblcell.h"
#import "AppDelegate.h"
#import "UtilsClass.h"
#import "Processcall.h"
#import "GlobalData.h"
#import "UploadDocumentVC.h"
#import "DialerVC.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad


@interface BuyNumbersListVC ()
{
    NSArray *numbersArr;
    NSMutableArray *capabilityArr;
    
    NSString *selectedTypeFilter;
    WebApiController *obj;
    
    NSInteger selectedNumberTag;
    NSArray *purchasedNumArray;
    
}
@end

@implementation BuyNumbersListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchasedNumArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    /*
     NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchasedNumArray = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSArray class] fromData:data error:nil];
     */
    
    numberTypeArr = [NSArray arrayWithObjects:@"local", nil];
    
    //_menu = [[LMJDropdownMenu alloc] init];
   
    _menu.dataSource = self;
    _menu.delegate   = self;
    
    _menu.layer.borderColor  = [UIColor lightGrayColor].CGColor;
    _menu.layer.borderWidth  = 1;
    _menu.layer.cornerRadius = 1;
    
    _menu.title           = [numberTypeArr firstObject];
    _menu.titleBgColor    = [UIColor clearColor];
    _menu.titleFont       = [UIFont boldSystemFontOfSize:15];
    _menu.titleColor      = [UIColor blackColor];
    _menu.titleAlignment  = NSTextAlignmentCenter;
    _menu.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    
    _menu.rotateIcon      = [UIImage imageNamed:@"Dropdown"];
    _menu.rotateIconSize  = CGSizeMake(15, 15);
    
    _menu.optionBgColor         = [UIColor clearColor];
    _menu.optionFont            = [UIFont systemFontOfSize:13];
    _menu.optionTextColor       = [UIColor blackColor];
    _menu.optionTextAlignment   = NSTextAlignmentLeft;
    _menu.optionNumberOfLines   = 1;
    _menu.optionLineColor       = [UIColor lightGrayColor];
    _menu.optionIconSize        = CGSizeMake(15, 15);
    _menu.optionIconMarginRight = 30;
    
    _numberTbl.dataSource = self;
    _numberTbl.delegate = self;
    
    //numbersArr = [NSArray arrayWithObjects:@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533", nil];
    
    //capabilityArr = [NSMutableArray arrayWithObjects:@"Voice",@"SMS",@"Voice",@"SMS", nil];

    selectedTypeFilter = @"local";
  
    self.navigationController.navigationBarHidden = false;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Buy Numbers";
    
    [self getAvailableNumbers];
    
}

#pragma mark - LMJDropdownMenu DataSource
-(NSUInteger)numberOfOptionsInDropdownMenu:(LMJDropdownMenu *)menu
{
    return numberTypeArr.count;
}
-(CGFloat)dropdownMenu:(LMJDropdownMenu *)menu heightForOptionAtIndex:(NSUInteger)index
{
    return 35;
}
-(NSString *)dropdownMenu:(LMJDropdownMenu *)menu titleForOptionAtIndex:(NSUInteger)index
{
    return [numberTypeArr objectAtIndex:index];
}

#pragma mark - LMJDropdownMenu Delegate
-(void)dropdownMenu:(LMJDropdownMenu *)menu didSelectOptionAtIndex:(NSUInteger)index optionTitle:(NSString *)title
{
    
}
-(void)dropdownMenuDidShow:(LMJDropdownMenu *)menu
{
    
}

#pragma mark :- Tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numbersArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell123"];

    
    cell.buy_numberLbl.text = [[numbersArr objectAtIndex:indexPath.row] valueForKey:@"phoneNumber"];
   // cell.buy_capabilityList = [[UURCCentralizedTokenView alloc]initWithTokenArray:capabilityArr];
    
//    cell.buy_capabilityList.tokenEdgeInsets = UIEdgeInsetsMake(5, 8, 5, 8);
//    cell.buy_capabilityList.unselectedTokenTitleColor = [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
//    cell.buy_capabilityList.unselectedTokenColor = UIColor.whiteColor;
//
//    cell.buy_capabilityList.tokenPadding = 10.0;
    
//    cell.buy_capabilityList.tokenViewLeftPadding = 10.0;
//    cell.buy_capabilityList.tokenViewRightPadding = 10.0;

   
//    for(int i=0;i<capabilityArr.count;i++) {
//        [cell.buy_capabilityList addTokenWithTitle:[capabilityArr objectAtIndex:i] tokenObject:nil];
//    }
    
    
    NSDictionary *temp =  [[numbersArr objectAtIndex:indexPath.row] valueForKey:@"capabilities"] ;
    NSMutableArray *caparr = [[NSMutableArray alloc]init];
    
    for(NSString *key in [temp allKeys])
    {
        if ([key isEqualToString:@"voice"]) {
            if ([[temp valueForKey:@"voice"] boolValue] == true) {
                [caparr addObject:@"voice"];
            }
        }
        else if ([key isEqualToString:@"SMS"]) {
            if ([[temp valueForKey:@"SMS"] boolValue] == true) {
                [caparr addObject:@"SMS"];
            }
        }
        else if ([key isEqualToString:@"MMS"]) {
            if ([[temp valueForKey:@"MMS"] boolValue] == true) {
                [caparr addObject:@"MMS"];
            }
        }
        
    }
    
//    NSLog(@"cap array :: %@",caparr);
    
   
    cell.buy_capabilityList.editable = false;
    cell.buy_capabilityList.direction = KSTokenViewScrollDirectionHorizontal;

    if (cell.buy_capabilityList.tokens.count == 0) {
        for(int i=0;i<caparr.count;i++)
        {
            [cell.buy_capabilityList addTokenWithTitle:[caparr objectAtIndex:i] tokenObject:nil];
        }
    }
    
   
    
//    cell.buy_capabilityList.tokenArray = caparr;
//    [cell.buy_capabilityList reloadTokens];
    
    cell.btn_buy.layer.cornerRadius = 5.0;
    cell.btn_buy.clipsToBounds = YES;
    
    cell.btn_buy.tag = indexPath.row;
    [cell.btn_buy addTarget:self action:@selector(buy_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}
#pragma mark- Token delegates

- (void)actionToken:(NSNumber *)tokenIndex {
    
    
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    
}
- (IBAction)filter_click:(UIBarButtonItem *)sender {
    
    if (@available(iOS 13, *))
    {
       
        
      /*  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
        //      alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BuyNumberFilter *vc = [storyboard instantiateViewControllerWithIdentifier:@"BuyNumberFilter"];
        //vc.selected_value = filter_string;
        //vc.currentView = @"Calllogs";
        
        //vc.tagToHighlight = selLogsTxtTohighlight;
        //vc.delegate = self;
        if (IS_IPAD) {
            
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
            //[self presentViewController:vc animated:YES completion:nil];
        } else {
            [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
            vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
            
            
        }
       //[alertWindow.rootViewController presentViewController:vc animated:NO completion:nil];
       */
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BuyNumberFilter *vc = [storyboard instantiateViewControllerWithIdentifier:@"BuyNumberFilter"];
        
        [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
        vc.delegate = self;
        vc.numberTypeArr = numberTypeArr;
        vc.filterStrToHighlight = selectedTypeFilter;
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
        UIWindow *alertWindow = [[UIApplication sharedApplication] keyWindow];
        [alertWindow.rootViewController presentViewController:vc animated:NO completion:nil];
        
        
        
    }
    else
    {
        UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BuyNumberFilter *vc = [storyboard instantiateViewControllerWithIdentifier:@"BuyNumberFilter"];
       
        vc.delegate = self;
        vc.numberTypeArr = numberTypeArr;
        vc.filterStrToHighlight = selectedTypeFilter;

        if (IS_IPAD) {
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
            vc.modalPresentationCapturesStatusBarAppearance = YES;
        } else {
            [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
            vc.modalPresentationCapturesStatusBarAppearance = YES;
        }
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
}

-(void)NumberFilterVCDidDismisWithData:(NSString *)data
{
    NSLog(@"Trushang : Selected_Filter  %@",data);
    selectedTypeFilter = data;
    
    [self getAvailableNumbers];
    
   
}

//- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
//
//}
//
//- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
//
//}
//
//- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
//    return CGSizeMake(50, 30);
//}
//
//- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
//
//}
//
//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
//
//}
//
//- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
//
//}
//
//- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
//
//}
//
//- (void)setNeedsFocusUpdate {
//
//}

//- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
//    return false;
//}
//
//- (void)updateFocusIfNeeded {
//
//}

#pragma mark- Get available numbers
-(void)getAvailableNumbers
{
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
        NSString *userID = [Default valueForKey:USER_ID];
  
        NSString *url = [NSString stringWithFormat:@"webapi/number/available"];
        NSDictionary *passDict = @{@"userId":userID,
                                   @"searchBy":@"pattern",
                                   @"type":selectedTypeFilter,
                                   @"pattern":@"",
                                   @"limit":@20,
                                   @"country":[_selectedCountry valueForKey:@"name"],
                                   @"country_iso":[_selectedCountry valueForKey:@"shortName"],
        };
        NSLog(@"get numbers req: %@",passDict);
        NSLog(@"get numbers url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getAvailableNumbersRes:response:) andDelegate:self];
}
- (void)getAvailableNumbersRes:(NSString *)apiAlias response:(NSData *)response{
    
    [Processcall hideLoadingWithView];
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"available API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : get available number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            //set number type filter
           
           // if ([Default boolForKey:kISFreeTrial] == false) {
                
                NSDictionary *typeDic = [[response1 valueForKey:@"data"] valueForKey:@"numberType"];
                NSMutableArray *tempType = [[NSMutableArray alloc]init];
                
                for (NSString *key in [typeDic allKeys])
                {
                    [tempType addObject:[typeDic valueForKey:key]];
                }
                NSLog(@"type array :: %@",tempType);
                numberTypeArr = tempType;
          //  }
            
            numbersArr = [[response1 valueForKey:@"data"] valueForKey:@"numbers"];
            
            [_numberTbl reloadData];
            
            if (numbersArr.count == 0) {
                
                _noNumberLbl.hidden = false;
                _numberTbl.hidden = true;
            }
            else{
                _noNumberLbl.hidden = true;
                _numberTbl.hidden = false;
            }
        }
        else
        {
                _noNumberLbl.hidden = false;
                _numberTbl.hidden = true;
        }
    }
}
#pragma mark- IBactions

-(IBAction)buy_click:(id)sender
{
    
    selectedNumberTag = [sender tag];
    
    NSMutableDictionary *valueDict = [NSMutableDictionary dictionaryWithDictionary:[numbersArr objectAtIndex:[sender tag]]];
                                 
   /* if ([[Default valueForKey:kISFreeTrial] boolValue] == true) {
        
        if ([[valueDict valueForKey:@"premium"] boolValue] == true) {
            
            [UtilsClass showAlert:@"This Number is premium.You can not buy this number in free trial.Please upgrade your plan." contro:self];
        }
        else
        {
            
            if ([[valueDict valueForKey:@"isTwilioAddress"] boolValue] == true) {
                
                [self getDocumentVerification];

            }
            else
            {
//                if ([[valueDict valueForKey:@"premium"] boolValue] == false)
//                {
//
//                    [self showFreeNumberPopup];
//                }
//                else
//                {
                    [self estimateAPICall];
              //  }
               
            }
        }
    }
    
    else
    {*/
        if ([[valueDict valueForKey:@"isTwilioAddress"] boolValue] == true) {
            
            [self getDocumentVerification];

        }
        else
        {
            [self estimateAPICall];
        }
   // }
    
    
   
   
}


-(void)estimateAPICall
{
    
   
    NSMutableDictionary *passdict = [[NSMutableDictionary alloc]init];
    
    NSMutableDictionary *valueDict = [NSMutableDictionary dictionaryWithDictionary:[numbersArr objectAtIndex:selectedNumberTag]];
    
    [passdict setValue:[valueDict valueForKey:@"monthlyPrice"] forKey:@"monthlyPrice"];
    [passdict setValue:[valueDict valueForKey:@"premium"] forKey:@"premium"];
    [passdict setValue:[valueDict valueForKey:@"setupCost"] forKey:@"setupCost"];
    
    /*if ([purchasedNumArray count] == 0 && [[valueDict valueForKey:@"premium"] boolValue] == false) {
        
        [passdict setValue:@"freeNumber" forKey:@"type"];
        
    }
    else
    {*/
        [passdict setValue:@"number" forKey:@"type"];
   // }
   

    [passdict setValue:valueDict forKey:@"number"];
    
        NSString *userID = [Default valueForKey:USER_ID];
  
        NSString *url = [NSString stringWithFormat:@"webapi/numbers/number/create/%@/estimate",userID];
    
    
        NSLog(@"get numbers req: %@",passdict);
        NSLog(@"get numbers url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passdict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(estimateRes:response:) andDelegate:self];
}
- (void)estimateRes:(NSString *)apiAlias response:(NSData *)response{
    

    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"estimate API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : get estimate number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
           float price = [[[response1 valueForKey:@"data"] valueForKey:@"prorateValue"]floatValue];
            
            if (price == 0) {
                
               
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:kAlertTitle
                                             message:[NSString stringWithFormat:@"You won't be charged as first number is free"]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                
                
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                    //Handle no, thanks button
                    
                    [self buyNumber:price];
                }];
                
                //Add your buttons to alert controller
                
                
                [alert addAction:noButton];
                
                [self presentViewController:alert animated:NO completion:nil];
            }
            else
            {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"2nd Phone Number"
                                         message:[NSString stringWithFormat:@"Are You Sure Want to Buy This Number %@ for $%0.2f?",[[numbersArr objectAtIndex:selectedNumberTag] valueForKey:@"phoneNumber"],price]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
                [self buyNumber:price];
            }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                //Handle no, thanks button
            }];
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:NO completion:nil];
            }
        }
    }
}
-(void)buyNumber:(double )prorateValue
{
    
//    NSString *timezoneStr = [NSString stringWithFormat:@"%@",[NSTimeZone systemTimeZone]];

    NSDictionary *valueDict = [numbersArr objectAtIndex:selectedNumberTag];
    NSMutableDictionary *passDict = [NSMutableDictionary dictionaryWithDictionary:valueDict];
    NSString *providercost = [valueDict valueForKey:@"monthly_rental_rate"];
    NSString *providersetupcost = [valueDict valueForKey:@"setup_rate"];

    [passDict setValue:[NSString stringWithFormat:@"%@1",[valueDict valueForKey:@"isoCountry"]] forKey:@"contactName"];
    [passDict setValue:@"" forKey:@"couponCode"];
    [passDict setValue:@(prorateValue) forKey:@"prorateCharge"];
    [passDict setValue:providercost forKey:@"providerNumberCost"];
    [passDict setValue:providersetupcost forKey:@"providerNumberSetupCost"];

    float finalprice;
    float price;
    
    float monthlyRate = [[valueDict valueForKey:@"monthlyPrice"] floatValue];
    float setupcost = [[valueDict valueForKey:@"setupCost"] floatValue];
    
    
    
    if ([purchasedNumArray count] == 0) {
        
        finalprice = monthlyRate;
        price =finalprice - setupcost;
    }
    else
    {
        finalprice = monthlyRate+setupcost;
        price = finalprice - setupcost;
    }
    
    [passDict setValue:@(price) forKey:@"price"];
    [passDict setValue:@(finalprice)  forKey:@"finalPrice"];

    NSString *url = [NSString stringWithFormat:@"webapi/numbers/number/create/buy"];


    NSLog(@"get numbers req: %@",passDict);
    NSLog(@"get numbers url : %@%@",SERVERNAME,url);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(buynumberRes:response:) andDelegate:self];

}
- (void)buynumberRes:(NSString *)apiAlias response:(NSData *)response{
    

    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"purchase API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : get purchase number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            [[GlobalData sharedGlobalData] get_Updated_Purchased_Number];
            
            DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
            [[self navigationController] pushViewController:vc animated:YES];
            
        }
    }
}
- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark: get document verified

-(void)getDocumentVerification
{
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
        NSString *userID = [Default valueForKey:USER_ID];

        NSString *url = [NSString stringWithFormat:@"webapi/number/systemverifieddocs"];
    
    NSDictionary *valueDict = [numbersArr objectAtIndex:selectedNumberTag];
    
        NSDictionary *passDict = @{@"country":[valueDict valueForKey:@"countryName"],
                                   @"region":[valueDict valueForKey:@"region"],
                                   @"type":[valueDict valueForKey:@"type"],
                                   @"user":userID
                                   
        };
        NSLog(@"doc verify req: %@",passDict);
        NSLog(@"doc verify url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(docverifyRes:response:) andDelegate:self];
}
- (void)docverifyRes:(NSString *)apiAlias response:(NSData *)response{
    
    [Processcall hideLoadingWithView];
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"doc API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : doc verify number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            if ([[response1 valueForKey:@"ownDocument"] boolValue] == true) {
                
                [self estimateAPICall];
            }
            else
            {
                
                [self showDocPopup];
            }
           
        }
    }
}
-(void)showDocPopup
{
    NSDictionary *valueDict = [numbersArr objectAtIndex:selectedNumberTag];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:kAlertTitle
                                 message:[NSString stringWithFormat:@"Need verification documents as following to active the number , please submit following docs on support portal\nFor Individual : %@\nFor Business: %@",[[valueDict valueForKey:@"twilioDocumentRequire"] valueForKey:@"personal"],[[valueDict valueForKey:@"twilioDocumentRequire"] valueForKey:@"business"]]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
        //Handle no, thanks button
    }];
    
    //Add your buttons to alert controller
    
    
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:NO completion:nil];
}
    

@end
