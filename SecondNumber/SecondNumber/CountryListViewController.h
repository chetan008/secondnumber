//
//  CountryListViewController.h
//  SecondNumber
//
//  Created by Apple on 24/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CountryListViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *back_btn;
@property (retain,nonatomic) IBOutlet UITableView *countryTbl;

@property (retain,nonatomic) IBOutlet UISearchBar *searchbar;
@property (retain,nonatomic)  NSString *fromStr;


@end

NS_ASSUME_NONNULL_END
