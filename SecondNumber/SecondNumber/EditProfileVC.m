//
//  EditProfileVC.m
//  SecondNumber
//
//  Created by Apple on 16/12/21.
//

#import "EditProfileVC.h"
#import "UtilsClass.h"
#import "Processcall.h"
#import "CryptLib.h"
#import "GlobalData.h"
#import "AppDelegate.h"
#import "Phone.h"
#import "Lin_Utility.h"
#import "DialerVC.h"
#import "SubscribeViewController.h"
#import "CountryListViewController.h"


@interface EditProfileVC ()
{
    WebApiController *obj;
    NSDictionary *signupDictionary;
    
    NSString *endpointUserName;
    NSString *parentId;
    NSString *endpointPassWord;
    NSString *userId;
    NSString *authToken;
    NSString *fullName;
    NSString *plivoAuthID;
    NSString *plivoAuthToken;
    NSString *planDisplayName;
    NSString *callTransfer;
    NSMutableArray *Mixallcontact;
    NSMutableArray *endPoints;
    
    bool issignup;
}
@end

@implementation EditProfileVC

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    
    self.navigationController.navigationBarHidden = false;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Profile";
}

- (IBAction)btnSaveClicked:(id)sender {
    
//    if ([[Default valueForKey:kUSEREMAIL] isEqualToString:@""]) {
//
//        //call API for REgister
//        [self callLoginAPI];
//    }
//    else
//    {
//        //Already registered call edit profile
//
//    }
    if([_txt_firstname.text isEqualToString:@""] || [_txt_lastname.text isEqualToString:@""] || [_txt_email.text isEqualToString:@""] || [_txt_contactnum.text isEqualToString:@""])
    {
        [UtilsClass makeToast:@"Please Enter Details"];
    }
    else
    {
        [Default setValue:[NSString stringWithFormat:@"%@ %@",_txt_firstname.text,_txt_lastname.text] forKey:FULL_NAME];
        [Default setValue:_txt_email.text forKey:kUSEREMAIL];
        [self callSignupAPI];
    }
  

}
- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)editProfile
{
    NSDictionary *passDict;
    
    NSString *oldemail = [Default valueForKey:kUSEREMAIL];
    BOOL emailChange = false;
    if (![oldemail isEqualToString:_txt_email.text]) {
        emailChange = true;
    }
    
    passDict = @{@"email":_txt_email.text,
                 @"fullName":[NSString stringWithFormat:@"%@ %@",_txt_firstname.text,_txt_lastname.text],
                 @"phoneNumber":_txt_contactnum.text,
                 @"isEmailChanged":@(emailChange),
                 @"profileUpdate":@(true)
    };
    
        
   
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"webapi/user/%@/setting/edit",userID];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    
    [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(editProfileRes:response:) andDelegate:self];
}
- (void)editProfileRes:(NSString *)apiAlias response:(NSData *)response{
    
   // [Processcall hideLoadingWithView];
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    
    NSLog(@"Encrypted Response : set setting : %@",response1);

    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        [UtilsClass makeToast:@"Profile Updated Successfully"];
        
    }
}

#pragma mark: Login API call

-(void)callSignupAPI
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *emailStr = [Default valueForKey:kUSEREMAIL];
    NSString *fullnameStr = [Default valueForKey:FULL_NAME];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    NSString *timezoneStr = [NSString stringWithFormat:@"%@",[NSTimeZone systemTimeZone]];
    
    signupDictionary = @{@"email":emailStr,
                               @"googleUniqueId":emailStr,
                               @"device":@"ios",
                               @"type":@"signup",
                               @"fullName":fullnameStr,
                               @"timezone":timezoneStr,
                               @"date":dateStr,
                               @"signUpWith":@"apple",
                            @"userLoginType": @"gmail",
                         @"isFreeTrialUser":@(true)

    };
    

//    NSString *str = [self EncryptParam:passDict];
//
//    NSDictionary *passDict1 = @{@"encryptedch":str};
    
  //  NSLog(@"encrypted dic >> %@",passDict1);
    
    
        NSLog(@"Login Dic : %@",signupDictionary);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:signupDictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"googlesignin" andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    
}
- (void)login:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
     [Processcall hideLoadingWithView];

    if([apiAlias isEqualToString:Status_Code])
    {
      
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Login_response : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            [self setLoginResponse:response1];
        }
        else
        {
            
            if ([[[response1 valueForKey:@"error"] valueForKey:@"error"] isEqualToString:@"This email doesn't exist"]) {
                
                [self callEncryptedSignup];
            }
        }
    }
}

-(void)callEncryptedSignup
{
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:signupDictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"webapi/googlesignin" andParams:jsonString SuccessCallback:@selector(signup:response:) andDelegate:self];
}
- (void)signup:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
     [Processcall hideLoadingWithView];

    
    if([apiAlias isEqualToString:Status_Code])
    {
      
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"signup response : %@",response1);
        if ([[response1 valueForKey:@"signupSuccess"] integerValue] == 1) {
            
            issignup = true;
            [self callSignupAPI];
        }
        else
        {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
            
        }
    }
}
-(void)setLoginResponse:(NSDictionary *)response1
{

                self->endpointUserName = [response1 valueForKey:@"data"][@"username"];
                self->endpointPassWord = [response1 valueForKey:@"data"][@"password"];
                self->userId = [response1 valueForKey:@"data"][@"_id"];
                self->authToken = [response1 valueForKey:@"data"][@"authToken"];
                self->fullName = [response1 valueForKey:@"data"][@"fullName"];
                self->plivoAuthID = [response1 valueForKey:@"data"][@"plivoAuthId"];
                self->plivoAuthToken = [response1 valueForKey:@"data"][@"plivoAuthToken"];//
                self->planDisplayName = [response1 valueForKey:@"data"][@"planDisplayName"];
                self->parentId = [response1 valueForKey:@"data"][@"parentId"];
                self->callTransfer = [response1 valueForKey:@"data"][@"callTransfer"];
                endPoints = [response1 valueForKey:@"data"][@"endpointInfo"];
                NSString *email = [response1 valueForKey:@"data"][@"email"];
                NSString *Provider = [response1 valueForKey:@"data"][@"provider"];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
                if (self->endpointUserName != nil && self->endpointPassWord !=nil) {
                    
                   
                    [Default setValue:self->endpointUserName forKey:ENDPOINT_USERNAME];
                    [Default setValue:self->endpointPassWord forKey:ENDPOINT_PASSORD];
                    [Default setValue:email forKey:kUSEREMAIL];
                    [Default setValue:self->userId forKey:USER_ID];
                    [Default setValue:self->authToken forKey:AUTH_TOKEN];
                    [Default setValue:self->fullName forKey:FULL_NAME];
                    [Default setValue:self->plivoAuthID forKey:CALLHIPPO_AUTH_ID];
                    [Default setValue:self->plivoAuthToken forKey:CALLHIPPO_AUTH_TOKEN];
                    [Default setValue:self->planDisplayName forKey:PLAN_DISPLAY_NAME];
                    [Default setValue:self->parentId forKey:PARENT_ID];
                    [Default setValue:Timer_Stop forKey:Timer_Call];
                    [Default setValue:self->callTransfer forKey:CALL_TRANSFER];
                    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
                    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
                    [Default setObject:[NSMutableArray arrayWithArray:[response1 valueForKey:@"data"][@"moduleRights"]] forKey:kMODUALRIGHTS];
                    [Default setValue:[response1 valueForKey:@"data"][@"isDisableSms"] forKey:kSMSRIGHTS];
                    [Default setValue:[response1 valueForKey:@"data"][@"thinqValue"] forKey:kTHINQVALUE];
                    [Default setValue:[response1 valueForKey:@"data"][@"thinqAccess"] forKey:kTHINQACCESS];
                    [Default setValue:[response1 valueForKey:@"data"][@"providerHostname"] forKey:kPROVIDERHOSTNAME];
                    [Default setValue:[response1 valueForKey:@"data"][@"iossipProtocol"] forKey:kIOSSIPPROTOCOL];
                    [Default setValue:[NSString stringWithFormat:@"%@",[response1 valueForKey:@"data"][@"extensionNumber"]] forKey:ownExtensionNumber];
                    [Default setValue:@"login" forKey:UserLoginStatus];
                    [Default setValue:Provider forKey:Login_Provider];
                    
                    [Default setValue:[response1 valueForKey:@"data"][@"blacklist"] forKey:kIsBlackList];
                    [Default setValue:[[[response1 valueForKey:@"data"] valueForKey:@"blockNumberList"] valueForKey:@"number"] forKey:kblackListedArray];
                    
                    [Default setValue:[response1 valueForKey:@"data"][@"recordControl"] forKey:kISRecordingInPlan];
                    
                    if (![[Default valueForKey:kEmailIdForDB] isEqualToString:[Default valueForKey:kUSEREMAIL]]) {
                        
                        [Default setBool:true forKey:kNeedToIgnoreUpdatedContact];
                        
                      //  [UtilsClass makeToast:@"Fetching contacts this may take upto few minutes,Please don't close the app."];
                        [[GlobalData sharedGlobalData] flushAllContactsAndFetchNew];
                        
                    }
                    
                    [Default setValue:[Default valueForKey:kUSEREMAIL] forKey:kEmailIdForDB];
                    
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"twilioEdge"] forKey:kTwilioEdgeValue];
                    
                     NSArray *phoneArray = [[NSUserDefaults standardUserDefaults] objectForKey:kMODUALRIGHTS];
                      
                       
                       if (phoneArray.count > 0) {
                           for (NSDictionary *item in phoneArray) {
                               NSString *name = item[@"name"];
                               
                               if ([name isEqualToString:@"feedBacks"]) {
                                    [Default setValue:item[@"active"] forKey:kISFeedbackRightsForSub];
                               }
                               
                           }
                       }
                    
                    //NSLog(@"login::  blacklisted saved : %@", [Default valueForKey:kblackListedArray]);
                    
                    //pri
                    [Default setValue:@"+1" forKey:klastDialCountryCode];
                    [Default setValue:@"United States" forKey:klastDialCountryName];
                    

                    
                   
                    [Default setValue:[[response1 valueForKey:@"data"]valueForKey:@"postCallSurvey"] forKey:kISPostCallSurvey];
                    
                    BOOL nummask = [[[response1 valueForKey:@"data"] valueForKey:@"numberMasking"] boolValue];
                    [Default setBool:nummask forKey:kIsNumberMask];

                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"] forKey:kTelephonyProviderSwitch];
                     
                    NSLog(@"telephony in login >> %@",[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"]);


                    
                    [Default setValue:[response1 valueForKey:@"data"][@"gamification"] forKey:kbadgeTag];
                    
                    if([[Default valueForKey:kbadgeTag] isEqualToString:@"Beginner"])
                    {
                        [Default setValue:@"badge_beginner" forKey:kbadgeImage];
                    }
                    else if([[Default valueForKey:kbadgeTag] isEqualToString:@"Sharpshooter"])
                    {
                        [Default setValue:@"badge_silver" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Calling Master"])
                    {
                         [Default setValue:@"badge_gold" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"The King"])
                    {
                         [Default setValue:@"badge_platinum" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Ultimate Champion"])
                    {
                         [Default setValue:@"badge_champion" forKey:kbadgeImage];
                    }
                    
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchSipUser"] forKey:kFreeswitchUsername];
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchSipPass"] forKey:kFreeswitchPassword];
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchDomain"] forKey:kFreeswitchDomain];

                    
                        
                    [self activeSubUser:[response1 valueForKey:@"data"][@"activeSubUsers"]];
                    [self outGoingCallCountryRest:[response1 valueForKey:@"data"][@"outCallCountries"]];
                    [self purchaseNumber:response1[@"data"]];
                    
                    NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"];
                    NSString *TwilioToken = @"";
                    if([response1 valueForKey:@"data"][@"twilioToken"])
                    {
                        TwilioToken = [response1 valueForKey:@"data"][@"twilioToken"] ? [response1 valueForKey:@"data"][@"twilioToken"] : @"";
                        [Default setValue:TwilioToken forKey:twilio_token];
                        NSLog(@"LoginVC : 1 :  Twilio token : %@",TwilioToken);
                    }
                    else
                    {
                        [Default setValue:TwilioToken forKey:twilio_token];
                        NSLog(@"LoginVC : 2 :  Twilio token : %@",TwilioToken);
                    }
                    [Default synchronize];
                    NSLog(@"\n \n VOIP TOKEN  %@ \n \n ",Voiptoken);
                    if(![TwilioToken isEqualToString:@""])
                    {
                        //set twilio edge
                        if ([Default valueForKey:kTwilioEdgeValue]) {
                        if (![[Default valueForKey:kTwilioEdgeValue] isEqualToString:@""]) {
                            
                           [TwilioVoiceSDK setEdge:[Default valueForKey:kTwilioEdgeValue]];
                            NSLog(@"edge setup:: %@",[Default valueForKey:kTwilioEdgeValue]);
                        }
                        }
                        
                        [TwilioVoiceSDK registerWithAccessToken:TwilioToken
                                                 deviceToken:[Default valueForKey:@"VOIPTOKEN_DATA"]
                                                  completion:^(NSError *error) {
                        }];
                    }
   
                    
                    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
                    
                     [[Phone sharedInstance] setDelegate:appDelegate];
                    
                    if (endpointUserName!= nil && endpointPassWord != nil) {
                        
                        [[Phone sharedInstance] login:endpointUserName endPointPassword:endpointPassWord deviceToken:[Default valueForKey:@"deviceTokenForPlivo"]];
                    }
                    
                    if ([Default valueForKey:kFreeswitchUsername]) {
                        
    //                   [LinphoneManager.instance startLinphoneCore];
                        
            [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                        
                    
                    }
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [Processcall hideLoadingWithView];
                                        
                        [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"stripeId"] forKey:kStripeId];
                        
                        NSLog(@"stripe %@",[[response1 valueForKey:@"data"] valueForKey:@"stripeId"]);
                        
                        if ([[[response1 valueForKey:@"data"] valueForKey:@"stripeId"] isEqualToString:@""]) {
                            
                            SubscribeViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"SubscribeViewController"];
                        
                            [[self navigationController] pushViewController:vc animated:YES];
                        }
                        else
                        {
                           
                            NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                           NSArray *numbersArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                            
                            if(numbersArr.count>0)
                            {
                                DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
                                [[self navigationController] pushViewController:vc animated:YES];
                            }
                            else
                            {
                                CountryListViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CountryListViewController"];
                                vc.fromStr = @"subscribe";
                                [[self navigationController] pushViewController:vc animated:YES];
                            }
                        }
                        
                        
                       
                        
                        
                
                [appDelegate updatefcmtoken];
                        
                    });
                      
                            
                        
                    
                } else {
                    [Processcall hideLoadingWithView];
                    [UtilsClass showAlert:@"Login Again" contro:self];
                }
}
-(void)purchaseNumber:(NSDictionary *)response1{
    
    
    NSDictionary*numberData = [response1 objectForKey:@"numberData"];
    NSArray *purchase_number = [numberData valueForKey:@"numbers"];
    
    if ([purchase_number count] == 1) {
        if ([[[purchase_number objectAtIndex:0] objectForKey:ISDummyNumber] intValue] == 0) {
            
            return;
        }
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
//    [GlobalData sharedGlobalData].Number_Selection = [[NSMutableArray alloc] init];
//    [[GlobalData sharedGlobalData].Number_Selection addObjectsFromArray:purchase_number];
    
    
}

-(void)activeSubUser:(NSArray *)response{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:ACTIVE_SUB_USER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)outGoingCallCountryRest:(NSArray *)response{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:OUTGOING_CALL_COUNTRY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


@end
