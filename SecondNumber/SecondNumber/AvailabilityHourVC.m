//
//  AvailabilityHourVC.m
//  SecondNumber
//
//  Created by Apple on 26/11/21.
//

#import "AvailabilityHourVC.h"
#import "Tblcell.h"
#import "UtilsClass.h"

@interface AvailabilityHourVC ()
{
    NSArray *daysArray;
    NSMutableArray *dayTimeArray;
    WebApiController *obj;
    NSDateFormatter *timeFormat;
    
}
@end

@implementation AvailabilityHourVC

- (void)viewDidLoad {
    [super viewDidLoad];
    

   
    
    self.navigationController.navigationBarHidden = false;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Availability Hours";
    
    timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm"];
    
    [self viewDesign];
    
    [self getAvailableTime];
}

-(void)viewDesign
{
    _btn_toTime.layer.cornerRadius = 5.0;
    _btn_toTime.clipsToBounds = YES;
    
    _btn_fromTime.layer.cornerRadius = 5.0;
    _btn_fromTime.clipsToBounds = YES;
    
    _btn_customDay_from.layer.cornerRadius = 5.0;
    _btn_customDay_from.clipsToBounds = YES;
    
    _btn_customDay_to.layer.cornerRadius = 5.0;
    _btn_customDay_to.clipsToBounds = YES;
    
    _workingDaysTbl.delegate = self;
    _workingDaysTbl.dataSource = self;
    
    _view_customDayInnerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _view_customDayInnerView.layer.borderWidth = 2.0;
    _view_customDayInnerView.clipsToBounds = YES;
    
    [_timePicker setValue:[UIColor lightTextColor] forKey:@"backgroundColor"];
    [_custom_timepicker setValue:[UIColor lightTextColor] forKey:@"backgroundColor"];

    
}

-(IBAction)workingDaysClick:(id)sender
{
    _view_workingDay.hidden = false;
    _workingDaysTbl.hidden = true;
    
//    [_btn_workingDays setBackgroundColor:[UIColor lightGrayColor]];
//    [_btn_customizeDays setBackgroundColor:[UIColor whiteColor]];
    
   // [_btn_nextWorkingDay setBackgroundImage:[UIImage imageNamed:@"chevron.down"] forState:UIControlStateNormal];
    
    _btn_nextWorkingDay.transform = CGAffineTransformMakeRotation(M_PI_2);
    _btn_nextCustomizeDay.transform = CGAffineTransformMakeRotation(M_PI_2 * 4);
   
}

-(IBAction)customizeDaysClick:(id)sender
{
    _view_workingDay.hidden = true;
    _workingDaysTbl.hidden = false;
    
//    [_btn_customizeDays setBackgroundColor:[UIColor lightGrayColor]];
//    [_btn_workingDays setBackgroundColor:[UIColor whiteColor]];
    
    [self adjustTableHeight];
    
    _btn_nextCustomizeDay.transform = CGAffineTransformMakeRotation(M_PI_2);
    _btn_nextWorkingDay.transform = CGAffineTransformMakeRotation(M_PI_2 * 4);
}
-(IBAction)fromTimeClick:(id)sender forEvent:(UIEvent*)event
{
    _timePicker.hidden = false;
    _timePicker.tag = 0;
    
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    CGFloat x = [touch locationInView:_view_workingDayFromto].x;
    CGFloat y = [touch locationInView:_view_workingDayFromto].y;
    
    _timePicker.frame = CGRectMake(x-_timePicker.frame.size.width, y+10, _timePicker.frame.size.width, _timePicker.frame.size.height);
    
    [self.view bringSubviewToFront:_timePicker];
      
 
    
}
-(IBAction)toTimeClick:(id)sender forEvent:(UIEvent*)event
{
    _timePicker.hidden = false;
    _timePicker.tag = 1;
    
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    CGFloat x = [touch locationInView:_view_workingDayFromto].x;
    CGFloat y = [touch locationInView:_view_workingDayFromto].y;
    
    _timePicker.frame = CGRectMake(x-_timePicker.frame.size.width, y+10, _timePicker.frame.size.width, _timePicker.frame.size.height);
    
    [self.view bringSubviewToFront:_timePicker];
}

-(IBAction)customDayFromClick:(id)sender forEvent:(UIEvent*)event
{
    _custom_timepicker.hidden = false;
    _custom_timepicker.tag = 0;
    
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    CGFloat x = [touch locationInView:_view_customDayFromto].x;
    CGFloat y = [touch locationInView:_view_customDayFromto].y;
    
    _custom_timepicker.frame = CGRectMake(x-_custom_timepicker.frame.size.width, y+10, _custom_timepicker.frame.size.width, _custom_timepicker.frame.size.height);
    
    [self.view bringSubviewToFront:_custom_timepicker];
      
}
-(IBAction)customDayToClick:(id)sender forEvent:(UIEvent*)event
{
    _custom_timepicker.hidden = false;
    _custom_timepicker.tag = 1;

    
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    
    CGFloat x = [touch locationInView:_view_customDayFromto].x;
    CGFloat y = [touch locationInView:_view_customDayFromto].y;
    
    _custom_timepicker.frame = CGRectMake(x-_custom_timepicker.frame.size.width, y+10, _custom_timepicker.frame.size.width, _custom_timepicker.frame.size.height);
    
    [self.view bringSubviewToFront:_custom_timepicker];
}

-(void)adjustTableHeight
{
    _dayTblHeightConstraint.constant = [daysArray count] * 44;
}
#pragma mark :- tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return daysArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   Tblcell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell123"];
   
  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.Availability_daysnameLbl.text = [daysArray objectAtIndex:indexPath.row];
    cell.Availability_dayTimeLbl.text = [dayTimeArray objectAtIndex:indexPath.row];

    
    
   return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _alphaView.hidden = false;
    _view_customDayView.hidden = false;
    
    _lbl_dayName.text = [daysArray objectAtIndex:indexPath.row];
    _lbl_dayTitle.text = [daysArray objectAtIndex:indexPath.row];
    _switch_day.tag = indexPath.row;
    
}
#pragma mark - day switch value change

-(IBAction)daySwitchValueChange:(id)sender {

    NSDictionary *passDict;
    
    if ([_switch_day isOn]) {
        
        NSDictionary *slots = [NSDictionary dictionaryWithObjectsAndKeys:_btn_customDay_from.titleLabel.text,@"start",_btn_customDay_to.titleLabel.text,@"end",@(false),@"errorMultiSlot",@"",@"errorText", nil];
        
        passDict = @{@"day":@(_switch_day.tag),
                                   @"operation":@"add",
                                   @"slots":slots
        };
        
    }
    else{
        
        NSDictionary *slots = [NSDictionary dictionaryWithObjectsAndKeys:_btn_customDay_from.titleLabel.text,@"start",_btn_customDay_to.titleLabel.text,@"end",@(false),@"errorMultiSlot",@"",@"errorText", nil];
        
        passDict = @{@"day":@(_switch_day.tag),
                                   @"operation":@"delete",
                                   @"slots":slots
        };
        
    }
    
    [self editCustomDayTime:passDict];
}

#pragma mark - time picker delegates

- (IBAction)customdatePickerValueChange:(id)sender {
    
    NSDictionary *passDict;
    
    if (_custom_timepicker.tag == 0) {
        
        [_btn_customDay_from setTitle:[timeFormat stringFromDate:_custom_timepicker.date] forState:UIControlStateNormal];
        
       
    }
    else
    {
        [_btn_customDay_to setTitle:[timeFormat stringFromDate:_custom_timepicker.date] forState:UIControlStateNormal];
    }
    
    NSDictionary *slots = [NSDictionary dictionaryWithObjectsAndKeys:_btn_customDay_from.titleLabel.text,@"start",_btn_customDay_to.titleLabel.text,@"end",@(false),@"errorMultiSlot",@"",@"errorText", nil];
    
    passDict = @{@"day":@(_switch_day.tag),
                               @"operation":@"update",
                               @"slots":slots
    };
    
    [self editCustomDayTime:passDict];
    
}
- (IBAction)workingtimePickerValueChange:(id)sender {
    
    if (_timePicker.tag == 0) {
        [_btn_fromTime setTitle:[timeFormat stringFromDate:_timePicker.date] forState:UIControlStateNormal];
        
    }
    else{
        [_btn_toTime setTitle:[timeFormat stringFromDate:_timePicker.date] forState:UIControlStateNormal];
        
    }
    
    NSDictionary *slots = [NSDictionary dictionaryWithObjectsAndKeys:_btn_fromTime.titleLabel.text,@"start",_btn_toTime.titleLabel.text,@"end",@(false),@"errorMultiSlot",@"",@"errorText", nil];
    
    NSDictionary *passDict = @{@"day":@0,
                               @"operation":@"update",
                               @"slots":slots
    };
    
    [self editWorkingDayTime:passDict];
    
    
    
    
    
}
-(void)editWorkingDayTime:(NSDictionary *)params
{
    
        NSString *userID = [Default valueForKey:USER_ID];
  
        NSString *url = [NSString stringWithFormat:@"webapi/user/%@/setting/customTimeMobile/",userID];
        
        NSLog(@"get numbers url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(editWorkingDayTimeRes:response:) andDelegate:self];
}
- (void)editWorkingDayTimeRes:(NSString *)apiAlias response:(NSData *)response{
    
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"available API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
        
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
           
        NSLog(@"Encrypted Response : get available number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
        }
    }
}
-(void)editCustomDayTime:(NSDictionary *)params
{
    
        NSString *userID = [Default valueForKey:USER_ID];
  
        NSString *url = [NSString stringWithFormat:@"webapi/user/%@/setting/customTime/",userID];
        
        NSLog(@"get numbers url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(editCustomTimeRes:response:) andDelegate:self];
}
- (void)editCustomTimeRes:(NSString *)apiAlias response:(NSData *)response{
    
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"available API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
        
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
           
        NSLog(@"Encrypted Response : get available number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
        }
    }
}
-(void)getAvailableTime
{
   
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    
    NSString *aStrUrl = [NSString stringWithFormat:@"webapi/user/%@/setting/time",[Default valueForKey:USER_ID]];
    
    NSLog(@"time URL : %@",aStrUrl);
    

    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getavailableTimeRes:response:) andDelegate:self];
}
- (void)getavailableTimeRes:(NSString *)apiAlias response:(NSData *)response
{
    

    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"get time response : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSMutableArray *data = [response1 valueForKey:@"data"];
        NSArray *startarr = [data valueForKey:@"start"];
        NSArray *endarr = [data valueForKey:@"end"];
        
        NSSet *set = [NSSet setWithArray:startarr];
        NSSet *set1 = [NSSet setWithArray:endarr];
        
        if ([set count] == 1 && [set1 count] == 1) {
            //all objects identical
            
            _view_workingDay.hidden = false;
            _workingDaysTbl.hidden = true;
            
            _btn_nextWorkingDay.transform = CGAffineTransformMakeRotation(M_PI_2);
            _btn_nextCustomizeDay.transform = CGAffineTransformMakeRotation(M_PI_2 * 4);
            

            
        }
        else
        {
            _view_workingDay.hidden = true;
            _workingDaysTbl.hidden = false;
        
            
            [self adjustTableHeight];
            
            _btn_nextCustomizeDay.transform = CGAffineTransformMakeRotation(M_PI_2);
            _btn_nextWorkingDay.transform = CGAffineTransformMakeRotation(M_PI_2 * 4);
            
        }
        
                    
                    [_btn_fromTime setTitle:[NSString stringWithFormat:@"%@ AM",[startarr objectAtIndex:0]]  forState:UIControlStateNormal];
                    [_btn_toTime setTitle: [NSString stringWithFormat:@"%@ PM",[endarr objectAtIndex:0]] forState:UIControlStateNormal];
        
            
            NSMutableArray *dayArraytemp = [[NSMutableArray alloc]init];
            NSMutableArray *dayTimeArraytemp = [[NSMutableArray alloc]init];

            
            for (int i=0; i<[data count]; i++) {
                
                NSDictionary *tempdic = [data objectAtIndex:i];
                
                if ([[tempdic valueForKey:@"day"] intValue] == 1) {
                    
                    [dayArraytemp addObject:@"Monday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
                else if ([[tempdic valueForKey:@"day"] intValue] == 2) {
                    
                    [dayArraytemp addObject:@"Tuesday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
                else if ([[tempdic valueForKey:@"day"] intValue] == 3) {
                    
                    [dayArraytemp addObject:@"Wednesday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
                else if ([[tempdic valueForKey:@"day"] intValue] ==4) {
                    
                    [dayArraytemp addObject:@"Thursday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
                else if ([[tempdic valueForKey:@"day"] intValue] == 5) {
                    
                    [dayArraytemp addObject:@"Friday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
                else if ([[tempdic valueForKey:@"day"] intValue] == 6) {
                    
                    [dayArraytemp addObject:@"Saturday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
                else if ([[tempdic valueForKey:@"day"] intValue] == 7) {
                    
                    [dayArraytemp addObject:@"Sunday"];
                    [dayTimeArraytemp addObject:[NSString stringWithFormat:@"%@ AM to %@ PM",[tempdic valueForKey:@"start"],[tempdic valueForKey:@"end"]]];
                }
            }
            
            daysArray = dayArraytemp;
            dayTimeArray = dayTimeArraytemp;
            
        [_workingDaysTbl reloadData];
        
        
        
    }
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
