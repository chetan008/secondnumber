//
//  BuyNumberFilter.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol NumberFilterVCDelegate <NSObject>
@optional
- (void)NumberFilterVCDidDismisWithData:(NSString *)data;
@end

@interface BuyNumberFilter : UIViewController

@property (nonatomic, weak)   id<NumberFilterVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbl_filter;
@property (nonatomic, strong) NSString *selected_value;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_constraint;
@property (nonatomic, strong) NSString *filterStrToHighlight;

@property (strong, nonatomic) IBOutlet UIView *tagMainView;
@property (strong, nonatomic) IBOutlet UIView *tagAlphaView;
@property (strong, nonatomic) IBOutlet UIView *filterMenuView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagTableViewHeightConstraint;

//@property (strong, nonatomic) IBOutlet UITableView *tagselectionTable;
@property (strong, nonatomic)  NSString *selectedType;

@property (strong, nonatomic)  NSArray *numberTypeArr;


@end

NS_ASSUME_NONNULL_END
