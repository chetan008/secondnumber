//
//  AvailabilityHourVC.h
//  SecondNumber
//
//  Created by Apple on 26/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AvailabilityHourVC : UIViewController <UITabBarDelegate,UITableViewDataSource>

@property (retain,nonatomic) IBOutlet UITableView *workingDaysTbl;

@property (retain,nonatomic) IBOutlet UISwitch *hourSwitch;
@property (retain,nonatomic) IBOutlet UIButton *btn_workingDays;
@property (retain,nonatomic) IBOutlet UIButton *btn_customizeDays;

@property (retain,nonatomic) IBOutlet UIButton *btn_from;
@property (retain,nonatomic) IBOutlet UIButton *btn_to;
@property (retain,nonatomic) IBOutlet UIButton *btn_fromTime;
@property (retain,nonatomic) IBOutlet UIButton *btn_toTime;

@property (retain,nonatomic) IBOutlet UIDatePicker *timePicker;
@property (retain,nonatomic) IBOutlet UIView *view_workingDay;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dayTblHeightConstraint;


@property (retain,nonatomic) IBOutlet UIDatePicker *custom_timepicker;
@property (retain,nonatomic) IBOutlet UIView *alphaView;
@property (retain,nonatomic) IBOutlet UIView *view_customDayView;
@property (retain,nonatomic) IBOutlet UIView *view_customDayInnerView;

@property (retain,nonatomic) IBOutlet UIButton *btn_customDay_from;
@property (retain,nonatomic) IBOutlet UIButton *btn_customDay_to;

@property (retain,nonatomic) IBOutlet UIButton *btn_nextWorkingDay;
@property (retain,nonatomic) IBOutlet UIButton *btn_nextCustomizeDay;
@property (strong, nonatomic) IBOutlet UIView *view_customDayFromto;
@property (strong, nonatomic) IBOutlet UIView *view_workingDayFromto;

@property (strong, nonatomic) IBOutlet UILabel *lbl_dayTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dayName;
@property (strong, nonatomic) IBOutlet UISwitch *switch_day;

@end

NS_ASSUME_NONNULL_END
