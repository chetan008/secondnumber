//
//  BuyNumberFilter.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "BuyNumberFilter.h"
#import "Tblcell.h"

@interface BuyNumberFilter ()
{
    NSArray *mainMenuArray;
    NSArray *mainMenuIconArray;
    NSArray *subMenuArray;
    
    NSArray *tagIdArray;
    NSString *selectedTagId;

    BOOL isSubMenu;
    BOOL isNumberTypeMenu;
    
    NSMutableArray *tagArray;
}
@end

@implementation BuyNumberFilter

- (void)viewDidLoad {
    [super viewDidLoad];
    
//   mainMenuArray = [NSArray arrayWithObjects:@"Number Type",@"Capability",nil];
    
    mainMenuArray = [NSArray arrayWithObjects:@"Number Type",nil];
    
    mainMenuIconArray = [NSArray arrayWithObjects:@"filter_vc",@"filter_vc",nil];
         
    //subMenuArray = [[NSMutableArray alloc]init];
    
    

    [self view_design];
    
    
    
    
    
}
-(void)view_design
{
    if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat topPadding = window.safeAreaInsets.top;
            CGFloat bottomPadding = window.safeAreaInsets.bottom;
            
            CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                                    (self.navigationController.navigationBar.frame.size.height ?: 0.0));
            
            
            if (topPadding < 21.0)
            {
                _top_constraint.constant = 44.0;
            }
            else
            {
                 _top_constraint.constant = 44.0;
            }

        }
        else
        {
            _top_constraint.constant = 44.0;
       }
        
   
    
        isSubMenu =false;
        _tbl_filter.delegate = self;
        _tbl_filter.dataSource = self;
        _tbl_filter.scrollEnabled = NO;
    
        [_tbl_filter reloadData];
        
        
        [self tbl_size];
    
}
#pragma  mark - tableview datasource delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isSubMenu)
        return mainMenuArray.count;
    else
        return subMenuArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [_tbl_filter dequeueReusableCellWithIdentifier:@"cell123"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!isSubMenu)
    {
        cell.buyFilter_itemName.text = mainMenuArray[indexPath.row];
        cell.buyFilter_itemIcon.image = [UIImage imageNamed: mainMenuIconArray[indexPath.row]];
        
        cell.buyFilter_btnNext.hidden = false;
        [cell.buyFilter_btnNext addTarget:self action:@selector(FilterNextClick:) forControlEvents:UIControlEventTouchUpInside];
        
        

        return cell;
    }
    else
    {
        NSLog(@"highlight >>> %@",_filterStrToHighlight);
        
        
        cell.buyFilter_itemName.text = subMenuArray[indexPath.row];
        
        cell.buyFilter_itemIcon.image = [UIImage imageNamed:@"filter_vc"];
        
        cell.buyFilter_btnNext.hidden = true;
        
        if ([_filterStrToHighlight isEqualToString:subMenuArray[indexPath.row]]) {
            
            NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                
            NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",subMenuArray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                
            [titleStr appendAttributedString:FromTitle];
            cell.buyFilter_itemName.attributedText = titleStr;
        }
        
        return cell;
        
        
    }
    
   
    

    
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isSubMenu) {
        
        if (indexPath.row == 0) {
            
            isNumberTypeMenu = true;
            //subMenuArray = [NSMutableArray arrayWithObjects:@"Local",@"Mobile",@"Tollfree",nil];
            subMenuArray = _numberTypeArr;
            _tbl_filter.scrollEnabled = false;

            [self openSubMenu];
            
        }
        else if(indexPath.row == 1)
        {
            isNumberTypeMenu = false;
            subMenuArray = [NSMutableArray arrayWithObjects:@"Voice",@"SMS",nil];
            _tbl_filter.scrollEnabled = false;

            [self openSubMenu];
        }
        
    }
    else
    {
        //isSubMenu = false;
        
        if (isNumberTypeMenu) {
            
//            if(_delegate && [_delegate respondsToSelector:@selector(NumberFilterVCDidDismisWithData:)])
//            {
            _selectedType = [subMenuArray objectAtIndex:indexPath.row];
                [_delegate NumberFilterVCDidDismisWithData:_selectedType];
         //   }
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self dismissViewControllerAnimated:NO completion:nil];
            });
        }
        
    }
       
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (isSubMenu && section == 0) {
        if (isNumberTypeMenu)
            return @"Number Type Filter";
        else
            return @"Capability Filter";
    }
    else
        return @"";
}

#pragma mark - custom methods

- (IBAction)btn_dissmiss_click:(UIButton *)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
//    [_delegate InboxFilterMenuVCDidDismisWithData:@"NoData" filterOption:@""];

}
- (IBAction)FilterNextClick:(UIButton *)sender
{
    [self openSubMenu];
}
-(void)openSubMenu
{
    isSubMenu = true;
    [_tbl_filter reloadData];
    [self tbl_size];
}


-(void)tbl_size
{

//            CGRect frame = self.tbl_filter.frame;
//            frame.size.height = self.tbl_filter.contentSize.height;
//            self.tbl_filter.frame = frame;
    
    dispatch_async(dispatch_get_main_queue(), ^{
           //This code will run in the main thread:
           CGRect frame = self.tbl_filter.frame;
           if (self.tbl_filter.contentSize.height < 350) {
               frame.size.height = self.tbl_filter.contentSize.height;
           }
           else
               frame.size.height = 350;
           //frame.size.height = self.tbl_filter.contentSize.height;
           self.tbl_filter.frame = frame;
       });
     
}

-(void)adjustTableHeight
{
    if ([subMenuArray count]>5) {
        _tagTableViewHeightConstraint.constant = 43.5 * 5;
    }
    else
    {
        _tagTableViewHeightConstraint.constant = 43.5 * [subMenuArray count];
    }
    _tagViewHeightConstraint.constant = _tagTableViewHeightConstraint.constant + 85;
}



@end
