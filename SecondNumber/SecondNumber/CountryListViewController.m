//
//  CountryListViewController.m
//  SecondNumber
//
//  Created by Apple on 24/11/21.
//

#import "CountryListViewController.h"
#import "UURCCentralizedTokenView.h"
#import "Tblcell.h"
#import "BuyNumbersListVC.h"
#import "WebApiController.h"
#import "UtilsClass.h"
#import "Processcall.h"

@interface CountryListViewController ()
{
    NSArray *countryListArr;
    WebApiController *obj;
    
    NSArray *filterData;
}

@end

@implementation CountryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([_fromStr isEqualToString:@"subscribe"]) {
        [_back_btn setImage:[UIImage imageNamed:@""]];
        _back_btn.enabled = false;
    }

    
    _countryTbl.dataSource = self;
    _countryTbl.delegate = self;
    
    self.navigationController.navigationBarHidden = false;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Select Country";
    
    [self getCountryList];
    
    

}

#pragma mark :- tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filterData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell123"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.country_nameLbl.text = [[filterData objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    cell.country_countryImg.image = [UIImage imageNamed:[[[filterData objectAtIndex:indexPath.row] valueForKey:@"shortName"] lowercaseString]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BuyNumbersListVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"BuyNumbersListVC"];
    vc.selectedCountry = [filterData objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -  get country list
-(void)getCountryList
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *aStrUrl = [NSString stringWithFormat:@"webapi/number/countries"];
    
    NSLog(@"country URL : %@",aStrUrl);
    
//    [Default setValue:@"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkRGF0ZSI6IjIwMjEtMTItMTRUMTA6MTY6NDUuMjgxWiIsImlzcyI6IjVkNmQwMDZhZTM1MzAwMTZjOWJjMjdjNCJ9.h584PwJ_wFv9Xj45XznNYHB-7DuchlDxEPGc9ckbjZk" forKey:AUTH_TOKEN];
//    [Default setValue:@"5d6d006ae3530016c9bc27c4" forKey:USER_ID];

    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCountryListResponse:response:) andDelegate:self];
}
- (void)getCountryListResponse:(NSString *)apiAlias response:(NSData *)response
{
    
    [Processcall hideLoadingWithView];
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"get country response : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        countryListArr = [response1 valueForKey:@"countries"];
        filterData = [response1 valueForKey:@"countries"];
        [_countryTbl reloadData];
    }
}
#pragma mark:-  search bar delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name BEGINSWITH %@",searchText] ;

    
    filterData = [countryListArr  filteredArrayUsingPredicate:predicate];
    

    if (searchText.length == 0) {
        filterData = countryListArr;
    }
   
    [_countryTbl reloadData];
}
- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
