//
//  BuyNumbersListVC.h
//  SecondNumber
//
//  Created by Apple on 25/11/21.
//

#import <UIKit/UIKit.h>
#import "LMJDropdownMenu.h"
#import "UURCCentralizedTokenView.h"
#import "BuyNumberFilter.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuyNumbersListVC : UIViewController <LMJDropdownMenuDelegate,LMJDropdownMenuDataSource,UITableViewDelegate,UITableViewDataSource,UURCCentralizedTokenViewDelegate,NumberFilterVCDelegate>
{
    NSArray *numberTypeArr;
}
@property (retain,nonatomic) IBOutlet LMJDropdownMenu *menu;
@property (retain,nonatomic) IBOutlet UITableView *numberTbl;
@property (strong,nonatomic)  NSDictionary *selectedCountry;
@property (strong,nonatomic)  IBOutlet UILabel *noNumberLbl;

@end
NS_ASSUME_NONNULL_END
