//
//  UploadDocumentVC.m
//  SecondNumber
//
//  Created by Apple on 04/01/22.
//

#import "UploadDocumentVC.h"
#import "Processcall.h"
#import "UtilsClass.h"

@interface UploadDocumentVC ()
{
    NSURL *selectedDocURL;
    NSString *selectedDocPath;
    WebApiController *obj;
}
@end

@implementation UploadDocumentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *allreq = [_numberArray valueForKey:@"twilioDocumentRequire"];
    
    _txtview_docRequired.text = [NSString stringWithFormat:@"For Individual: %@ \nFor Business : %@",[allreq valueForKey:@"personal"],[allreq valueForKey:@"business"]];
    
    [self adjustTextViewHeight];
}

-(IBAction)upload_click:(id)sender
{
//    UIDocumentInteractionController *docinteractor = [[UIDocumentInteractionController alloc]init];
//    docinteractor.delegate = self;
//    [docinteractor presentOptionsMenuFromRect:[self.view frame] inView:self.view animated:true];
   
    
    UIDocumentPickerViewController *picker = [[UIDocumentPickerViewController alloc]initWithDocumentTypes:[NSArray arrayWithObjects:@"public.data", nil] inMode:UIDocumentPickerModeImport];
    picker.delegate = self;
    [self presentViewController:picker animated:true completion:nil];
    
}
-(IBAction)submit_click:(id)sender
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *useridStr = [Default valueForKey:USER_ID];
    
    NSString *url = [NSString stringWithFormat:@"webapi/user/%@/address",useridStr];
    
    
   
    NSString *fullnameStr = [Default valueForKey:FULL_NAME];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    NSString *timezoneStr = [NSString stringWithFormat:@"%@",[NSTimeZone systemTimeZone]];
    
   NSMutableDictionary *passDict = @{@"country":[_numberArray valueForKey:@"countryName"],
                               @"countryShortCode":[_numberArray valueForKey:@"isoCountry"],
                               @"addressLine1":_txtAddress1.text,
                               @"addressLine2":_txtAddress2.text,
                               @"name":_txtname.text,
                               @"city":_txtCity.text,
                               @"zipcode":_txtZipcode.text,
                               @"state":_txtState.text,
                            @"provider": @"plivo",
                         @"phoneNumber":[_numberArray valueForKey:@"phoneNumber"],
                              @"numberDetails[contactName]":_txtCity.text,
                              @"numberDetails[type]":[_numberArray valueForKey:@"type"],
                              @"numberDetails[isTwilioAddress]":[_numberArray valueForKey:@"isTwilioAddress"],
                              @"numberDetails[isTwilioBundle]":[_numberArray valueForKey:@"isTwilioBundle"],
                              @"numberDetails[region]":[_numberArray valueForKey:@"region"],
                           @"numberDetails[countryName]:": [_numberArray valueForKey:@"countryName"],
                              @"numberDetails[location]":[_numberArray valueForKey:@"locality"],
                              @"numberDetails[twilioDocumentRequire][personal]":[[_numberArray valueForKey:@"twilioDocumentRequire"] valueForKey:@"personal"],
                              @"numberDetails[twilioDocumentRequire][business]":[[_numberArray valueForKey:@"twilioDocumentRequire"] valueForKey:@"business"],
                           @"numberDetails[twilioDocumentRequire][_id]": [[_numberArray valueForKey:@"twilioDocumentRequire"] valueForKey:@"_id"]

    };
    


    
    
        NSLog(@"Login Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    obj = [[WebApiController alloc] init];
    [obj callAPIWithDocument:url WithImageParameter:passDict strDocPath:selectedDocPath SuccessCallback:@selector(docUploadResponse:response:) andDelegate:self];
    
//    [obj callAPI_POST_RAW:@"googlesignin" andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];

}
- (void)docUploadResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
     [Processcall hideLoadingWithView];

    if([apiAlias isEqualToString:Status_Code])
    {
      
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Login_response : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
            
        }
    }
}
-(void)adjustTextViewHeight
{
    CGFloat fixedWidth = _txtview_docRequired.frame.size.width;
    CGSize newSize = [_txtview_docRequired sizeThatFits:CGSizeMake(fixedWidth, FLT_MAX)];
    _txtviewHeightConstraint.constant = newSize.height;
    [self.view layoutIfNeeded];
    
}

#pragma mark: document interactor
/*-(void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller
{
    _lbl_docname.text = controller.URL.lastPathComponent;
    selectedDocURL = controller.URL;
    
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}*/
-(void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls
{
    _lbl_docname.text = [urls objectAtIndex:0].lastPathComponent;
    selectedDocURL = [urls objectAtIndex:0];
    selectedDocPath = [urls objectAtIndex:0].absoluteString;
    
}
@end
