//
//  NumberSettingsVC.m
//  SecondNumber
//
//  Created by Apple on 16/12/21.
//

#import "NumberSettingsVC.h"
#import "GlobalData.h"
#import "UtilsClass.h"
#import "Processcall.h"

@interface NumberSettingsVC ()
{
    UITextField  *txt_name;
    WebApiController *obj;

}
@end

@implementation NumberSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.navigationController.navigationBarHidden = false;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Settings";
    
    [self viewDesign];
}
-(void)viewDesign
{
    _lbl_name.text = [[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"contactName"];
    _lbl_number.text = [[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"phoneNumber"];
    
    _img_numberflag.image = [UIImage imageNamed:[[[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"shortName"]lowercaseString]];
    
}

-(IBAction)btnNameEdit_click:(id)sender
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Change Name" message:@"Enter Name" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Done"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
        
        if (![self->txt_name.text isEqualToString:@""]) {
            [self saveSetting:@"contactName" value:self->txt_name.text];
        }
    }];
    
    defaultAction.enabled = YES;
    [controller addAction:defaultAction];
        
    [controller addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.delegate = self;
        self->txt_name = textField;
    }];

    [self presentViewController:controller animated:YES completion:nil];
}

-(void)saveSetting:(NSString *)settingName value:(NSString *)value
{
    NSDictionary *passDict;
    
    
        passDict = @{settingName:value};
    
        
   
    NSString *numId = [[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"_id"];
    NSString *url = [NSString stringWithFormat:@"webapi/number/%@/setting",numId];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    
    [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(settingRes:response:) andDelegate:self];
}
- (void)settingRes:(NSString *)apiAlias response:(NSData *)response{
    
   // [Processcall hideLoadingWithView];
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    
    NSLog(@"Encrypted Response : set setting : %@",response1);

    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        _lbl_name.text = txt_name.text;
    
        [[GlobalData sharedGlobalData] get_Updated_Purchased_Number];
        
    }
}
#pragma mark - textfield delegate

-(BOOL)resignFirstResponder
{
    return [txt_name resignFirstResponder];
}

#pragma mark - Switch Changes

- (IBAction)record_valuechange:(id)sender {
    
    if ([_switch_record isOn]) {
        
        _view_pauseRecord.hidden = false;
        
       NSDictionary *passDict = @{
                     @"recordingStatus":@(true)
                     
        };
        
        
        [self saveRecordSetting:passDict];
    }
    else
    {
        _view_pauseRecord.hidden = true;

        NSDictionary *passDict = @{@"recordControl":@(false),
                      @"recordingStatus":@(false),
                      @"recordingTranscript":@(false)
         };
        
        [self saveRecordSetting:passDict];
    }
}
- (IBAction)pauserecord_valuechange:(id)sender {
    
    if ([_switch_pauserecord isOn]) {
        
        
       NSDictionary *passDict = @{
                     @"recordControl":@(true)
                     
        };
        
        
        [self saveRecordSetting:passDict];
    }
    else
    {

        NSDictionary *passDict = @{@"recordControl":@(false)
                      
         };
        
        [self saveRecordSetting:passDict];
    }
}
-(void)saveRecordSetting:(NSDictionary *)params
{
   
   
    NSString *numId = [[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"_id"];
    NSString *url = [NSString stringWithFormat:@"webapi/number/%@/setting",numId];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    
    [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(RecordsettingRes:response:) andDelegate:self];
}
- (void)RecordsettingRes:(NSString *)apiAlias response:(NSData *)response{
    
   // [Processcall hideLoadingWithView];
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    
    NSLog(@"Encrypted Response : set setting : %@",response1);

    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
       
    }
}
- (IBAction)CallQueue_valueChanges:(id)sender {
    
    if ([_switch_pauserecord isOn]) {
        
        [self UpdateCallQueueSwitch:true];
    }
    else
    {
        [self UpdateCallQueueSwitch:false];
    }
}
-(void)UpdateCallQueueSwitch:(BOOL)Value
{
    NSString *numId = [[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"_id"];
    NSString *url = [NSString stringWithFormat:@"webapi/number/%@/setting",numId];
    
    
    NSDictionary *params = @{
                  @"callQueueEnabled":@(Value)
                  
     };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    
    [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(callQueueRes:response:) andDelegate:self];
}
- (void)callQueueRes:(NSString *)apiAlias response:(NSData *)response{
    
   // [Processcall hideLoadingWithView];
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    
    NSLog(@"Encrypted Response : set setting : %@",response1);

    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
       
    }
}
- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)deleteNumber_click:(id)sender {
    [self cancelNumberAPI];
}

-(void)cancelNumberAPI
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSDictionary *passDict = @{@"contactSupport":@(false),
                               @"deleteReason":@"",
                               @"pauseAccount":@(false)
                               
    };
    
    
        NSString *userID = [Default valueForKey:USER_ID];
  
        NSString *url = [NSString stringWithFormat:@"webapi/number/%@",[[_selectedNumberDetail valueForKey:@"number"] valueForKey:@"_id"]];
    
    
        NSLog(@"cancel req: %@",passDict);
        NSLog(@"cancel url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
      
    [obj callAPI_DELETE_RAW:url andParams:jsonString SuccessCallback:@selector(cancelNum:response:) andDelegate:self];
}
- (void)cancelNum:(NSString *)apiAlias response:(NSData *)response{
    
    [Processcall hideLoadingWithView];

    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"cancel API status code  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : get delete number : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            [UtilsClass makeToast:@"Number successfully deleted."];
            [[GlobalData sharedGlobalData] get_Updated_Purchased_Number];
            
            [self.navigationController popViewControllerAnimated:true];
        }
    }
}
@end
