//
//  SettingsVC.m
//  SecondNumber
//
//  Created by Apple on 25/11/21.
//

#import "SettingsVC.h"
#import "Tblcell.h"
#import "AvailabilityHourVC.h"
#import "UtilsClass.h"
#import "LoginViewController.h"

@interface SettingsVC ()
{
    NSArray *optionArr1;
    NSArray *optionArr2;
    WebApiController *obj;
}
@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
   // optionArr1 = [NSArray arrayWithObjects:@"Availability Hours",@"Calling/Number Charges",@"Invoice", nil];
    
    optionArr1 = [NSArray arrayWithObjects:@"Availability Hours", nil];
    
    optionArr2 = [NSArray arrayWithObjects:@"FAQ",@"Contact Us",@"Terms of Use",@"Privacy Policy",@"Cancel Account", nil];
    
    self.navigationController.navigationBar.topItem.title = @"Settings";
    self.navigationItem.hidesBackButton = YES;
    
    _optionTbl.dataSource = self;
    _optionTbl.delegate = self;
    
    
    
}
#pragma mark :- tableview delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return optionArr1.count;
    }
    else
    {
        return optionArr2.count;
    }
   
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell;
    
    if (indexPath.section == 0) {
        
        
            cell = [tableView dequeueReusableCellWithIdentifier:@"cell123"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell123"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
   
    
    
    if (indexPath.section == 0) {
        
        
            cell.setting_optionLbl.text = [optionArr1 objectAtIndex:indexPath.row];
        
       
    }
    else
    {
        cell.setting_optionLbl.text = [optionArr2 objectAtIndex:indexPath.row];
    }
    
    cell.numberslist_countryImg.image = [UIImage imageNamed:@"us"];
   
    cell.numberslist_countryImg.layer.cornerRadius = cell.numberslist_countryImg.frame.size.height /2.0;
    cell.numberslist_countryImg.clipsToBounds = YES;
    cell.numberslist_countryImg.contentMode = UIViewContentModeScaleToFill;
    
   return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        AvailabilityHourVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AvailabilityHourVC"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (IBAction)btn_menu_click:(id)sender {
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}

@end
