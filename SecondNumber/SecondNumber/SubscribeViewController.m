//
//  SubscribeViewController.m
//  SubscribeViewController
//
//  Created by Apple on 18/11/21.
//

#import "SubscribeViewController.h"
#import "CountryListViewController.h"
#import "NumbersListVC.h"
#import "UtilsClass.h"
#import "DialerVC.h"
#import "Processcall.h"

@interface SubscribeViewController ()
{
    WebApiController *obj;
    bool isfreeTrial;
}
@end

@implementation SubscribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    _webview.navigationDelegate = self;
    
    _BtnSubscribe.layer.cornerRadius = _BtnSubscribe.frame.size.height/2;
    _BtnSubscribe.clipsToBounds = true;
    
    _BtnFreeTrial.layer.cornerRadius = _BtnFreeTrial.frame.size.height/2;
    _BtnFreeTrial.clipsToBounds = true;
    
    self.navigationController.navigationBarHidden = true;

    //_webview.contentMode = UIViewContentModeScaleAspectFit;
    
   /* NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSLog(@"date global ::%@",[dateFormatter stringFromDate:[NSDate date]]);
   
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];

    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];

    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* nowDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    NSLog(@"now date :: %@",nowDate);*/
}

- (IBAction)subscribe_click:(id)sender {
    
    /*SKProductsRequest *productreq = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithObject:@"com.plan1.10"]];
    
    productreq.delegate = self;
    [productreq start];*/
    
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    isfreeTrial = false;
    _view_subscribe.hidden = false;
    NSString *str =  [Default valueForKey:kUSEREMAIL];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@""];
  //test  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://callhippo-test.chargebee.com/hosted_pages/plans/second-number?customer[email]=%@",str]];
    
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://callhippo.chargebee.com/hosted_pages/plans/second_number_monthly?customer[email]=%@",str]];
    

    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    

}

-(IBAction)FreeTrialClick:(id)sender
{
   // [Default setBool:true forKey:kISFreeTrial];
    
    isfreeTrial = true;
    _view_subscribe.hidden = false;
    NSString *str =  [Default valueForKey:kUSEREMAIL];
    str = [str stringByReplacingOccurrencesOfString:@"+" withString:@""];
  //test  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://callhippo-test.chargebee.com/hosted_pages/plans/second-number_ft?customer[email]=%@",str]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://callhippo.chargebee.com/hosted_pages/plans/second_number_monthly_ft?customer[email]=%@",str]];
    
    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    
}

#pragma mark: webview delegates
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [Processcall hideLoadingWithView];
    NSLog(@"Finish navigation");
}

-(void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    NSLog(@"response URL :: %@",navigationResponse.response.URL);
    decisionHandler(WKNavigationResponsePolicyAllow);
    
}
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    //if (isfreeTrial  == false) {
        
       // NSURL *url = navigationAction.request.URL;
     
        NSURL *url = webView.URL;
        
        if ([url.lastPathComponent isEqualToString:@"test"]) {
            
            
            
            NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
            NSArray *urlComponents = [url.query componentsSeparatedByString:@"&"];
          
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];

                [queryStringDictionary setObject:value forKey:key];
            }
            NSLog(@"query dic.. %@",queryStringDictionary);
            
            NSArray *allkeys = queryStringDictionary.allKeys;
            if ([allkeys containsObject:@"cus_id"] && [allkeys containsObject:@"sub_id"]) {
                
                //_view_subscribe.hidden = true;
                
                
                
                NSString *userid = [Default valueForKey:USER_ID];
              
               
                NSString *timezoneStr = [NSString stringWithFormat:@"%@",[NSTimeZone systemTimeZone]];
                
                NSString *planid ;
                if (isfreeTrial) {
                    planid = @"5885abfcd485b7141b48a272";
                }
                else
                {
                    planid = @"5885abfcd485b7141b48a271";
                }
                
               NSDictionary *passDict = @{@"amount":@20,
                                           @"description":@"Upgrade Plan And Purchase number",
                                           @"payPeriod1":@"monthly",
                                           @"paymentGateway":@"chargebee",
                                           @"period":@"monthly",
                                           @"planAmount":@20,
                                           @"userId":userid,
                                           @"subscriptionId":[queryStringDictionary valueForKey:@"sub_id"],
                                          @"customerId": [queryStringDictionary valueForKey:@"cus_id"],
                                          @"device":@"mobile",
                                          @"timezone":timezoneStr,
                                          @"planId":planid,
                                          @"redirectFailedUrl":@"https://staging-app.callhippo.com/#!/plan/upgradeFail",
                                          @"redirectSuccessUrl":@"https://staging-app.callhippo.com/#!/addNumber?&price=undefined&period=monthly&inviteUserStatus=true",
                                          @"request_token":@"",
                                          @"seatBaseValue":@1,
                                          @"payPeriod":@2,
                                          @"billingToken":@"",
                                          @"currency": @"USD"
                                          
                };
                [self callSubscribeAPI:passDict];
            }
            
          
        }

    
        decisionHandler(WKNavigationActionPolicyAllow);
    
    
    
    
}

-(void)callSubscribeAPI:(NSDictionary *)passDict
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *userid = [Default valueForKey:USER_ID];
    NSString *urlstr = [NSString stringWithFormat:@"webapi/chargebee/user/v3/%@",userid];
    
    NSLog(@"Login Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:urlstr andParams:jsonString SuccessCallback:@selector(subAPIRes:response:) andDelegate:self];
    
}
- (void)subAPIRes:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    [Processcall hideLoadingWithView];

    if([apiAlias isEqualToString:Status_Code])
    {
      
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"subscription response : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
        //    [Default setValue:@(true) forKey:kISPlanSubscribed];
//            DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
//            [[self navigationController] pushViewController:vc animated:YES];
            
            
            [self getCredits];
            
            CountryListViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CountryListViewController"];
            vc.fromStr = @"subscribe";
            [[self navigationController] pushViewController:vc animated:YES];
            
        }
    }
}

-(void)getCredits
{
    
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";

    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
    NSDictionary *passDict = @{@"userId":userID,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"token":fcmtoken,
                               @"versionInfo":VERSIONINFO,};
    NSLog(@"getcredit : %@",passDict);
    NSLog(@"getcredit : %@%@",SERVERNAME,url);
    NSLog(@"getcredit auth token: %@",[Default valueForKey:AUTH_TOKEN]);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getCredits:response:) andDelegate:self];
}

- (void)getCredits:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
   
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
             NSLog(@"Trushang : getCredits : %@",response1);
       
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"stripeId"] forKey:kStripeId];
        }
    }
}
/*
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    SKProduct *validProduct = nil;
    //int count = [response.products count];
    NSLog(@"response:: %@",response.invalidProductIdentifiers);

    if(response.products.count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
    
    
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];

    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");

            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            //NSString *productID = transaction.payment.productIdentifier;
           // [self doRemoveAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        

        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                
                break;
            case SKPaymentTransactionStatePurchased:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                
                NSLog(@"Transaction state -> Purchased");
                
                
                
                NumbersListVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"NumbersListVC"];
              
                [[self navigationController] pushViewController:vc animated:YES];
            
                break;
            }
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}*/



@end
