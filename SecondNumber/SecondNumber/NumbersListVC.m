//
//  NumbersListVC.m
//  SecondNumber
//
//  Created by Apple on 25/11/21.
//

#import "NumbersListVC.h"
#import "Tblcell.h"
#import "CountryListViewController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "NumberSettingsVC.h"
#import "GlobalData.h"
#import "MainViewController.h"

@interface NumbersListVC ()
{
    NSArray *numbersArr;
}
@end

@implementation NumbersListVC

-(void)viewDidLoad {
    
   [super viewDidLoad];
    
  
    
   // numbersArr = [NSKeyedUnarchiver unarchivedObjectOfClass:[NSArray class] fromData:data error:nil];
    
    
  //  numbersArr = [NSArray arrayWithObjects:@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533",@"+14845933533", nil];
    
//    _btn_addNumber.layer.cornerRadius = _btn_addNumber.frame.size.height /2.0;
//    _btn_addNumber.clipsToBounds = YES;
    
    self.navigationController.navigationBar.topItem.title = @"Numbers";
    self.navigationItem.hidesBackButton = YES;
    
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;

    
}
-(void)viewWillAppear:(BOOL)animated
{
    /*[Default setValue:@"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1ZmVjNTk1NDU5MmY2YzQ2MmEzM2FkYzEiLCJjcmVhdGVkRGF0ZSI6IjIwMjEtMTItMzBUMDY6NDU6MjUuMzI4WiJ9.-chcDgLk-gpoREiB2eXVB3Fnz-RhQBin4Q4I9y47LIY" forKey:AUTH_TOKEN];
    [Default setValue:@"5fec5954592f6c462a33adc1" forKey:USER_ID];
    [[GlobalData sharedGlobalData] get_Updated_Purchased_Number];*/
    
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    numbersArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   
    
    if (numbersArr.count>0) {
        
        _numberTbl.dataSource = self;
        _numberTbl.delegate = self;
        
        [_numberTbl reloadData];
        
        _lbl_noNumber.hidden = true;
    }
    else
    {
        _lbl_noNumber.hidden = false;
        _numberTbl.hidden = true;
        _searchBar.hidden = true;
    }
    
}

#pragma mark :- tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return numbersArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   Tblcell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell123"];
   
   cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
   
    cell.numberslist_numLbl.text = [[[numbersArr objectAtIndex:indexPath.row] valueForKey:@"number"] valueForKey:@"phoneNumber"];
    
    cell.numberslist_countryImg.image = [UIImage imageNamed:[[[[numbersArr objectAtIndex:indexPath.row] valueForKey:@"number"] valueForKey:@"shortName"] lowercaseString]];
   
    //cell.numberslist_countryImg.layer.cornerRadius = cell.numberslist_countryImg.frame.size.height /2.0;
    //cell.numberslist_countryImg.clipsToBounds = YES;
    //cell.numberslist_countryImg.contentMode = UIViewContentModeScaleToFill;
    
   return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NumberSettingsVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NumberSettingsVC"];
    vc.selectedNumberDetail = [numbersArr objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}
-(IBAction)addNumberClick:(id)sender
{
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
   
   CountryListViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"CountryListViewController"];
   
   [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btn_menu_click:(id)sender {
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}

#pragma mark - get purchased number
/*
-(void)getNumberList
{
    [Processcall showLoadingWithView:nil withLabel:nil];
    
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *aStrUrl = [NSString stringWithFormat:@"number/countries"];
    
    NSLog(@"country URL : %@",aStrUrl);
    
    [Default setValue:@"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkRGF0ZSI6IjIwMjEtMTItMTRUMTA6MTY6NDUuMjgxWiIsImlzcyI6IjVkNmQwMDZhZTM1MzAwMTZjOWJjMjdjNCJ9.h584PwJ_wFv9Xj45XznNYHB-7DuchlDxEPGc9ckbjZk" forKey:AUTH_TOKEN];
    [Default setValue:@"5d6d006ae3530016c9bc27c4" forKey:USER_ID];

    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCountryListResponse:response:) andDelegate:self];
}
- (void)getCountryListResponse:(NSString *)apiAlias response:(NSData *)response
{
    
    [Processcall hideLoadingWithView];
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"get country response : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        countryListArr = [response1 valueForKey:@"countries"];
        filterData = [response1 valueForKey:@"countries"];
        [_countryTbl reloadData];
    }
}*/
@end
