//
//  SettingsVC.h
//  SecondNumber
//
//  Created by Apple on 25/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingsVC : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (retain,nonatomic) IBOutlet UITableView *optionTbl;

@end

NS_ASSUME_NONNULL_END
