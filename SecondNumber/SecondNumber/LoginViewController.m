//
//  LoginViewController.m
//  SecondNumber
//
//  Created by Apple on 18/11/21.
//

#import "LoginViewController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "EditProfileVC.h"
#import "AppDelegate.h"
#import "GlobalData.h"
#import "Phone.h"
#import "Lin_Utility.h"
#import "CountryListViewController.h"
#import "DialerVC.h"

@interface LoginViewController ()
{
    WebApiController *obj;
    NSDictionary *signupDictionary;
    
    NSString *endpointUserName;
    NSString *parentId;
    NSString *endpointPassWord;
    NSString *userId;
    NSString *authToken;
    NSString *fullName;
    NSString *plivoAuthID;
    NSString *plivoAuthToken;
    NSString *planDisplayName;
    NSString *callTransfer;
    NSMutableArray *Mixallcontact;
    NSMutableArray *endPoints;
}
@end

@implementation LoginViewController

@synthesize appleIDLoginInfoTextView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = true;
    
    if (@available(iOS 13.0, *)) {
               [self observeAppleSignInState];
               [self setupUI];
           }
    
}

- (void)observeAppleSignInState {
    if (@available(iOS 13.0, *)) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

- (void)handleSignInWithAppleStateChanged:(id)noti {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", noti);
}


- (void)setupUI {
    
   /* _appleIdButton.cornerRadius = CGRectGetHeight(_appleIdButton.frame) * 0.25;
    //_appleIdButton.backgroundColor = [UIColor blackColor];
    if (@available(iOS 13.0, *)) {
        _appleIdButton = [[ASAuthorizationAppleIDButton alloc]initWithAuthorizationButtonType:ASAuthorizationAppleIDButtonTypeSignIn authorizationButtonStyle:ASAuthorizationAppleIDButtonStyleBlack];
    } else {
        // Fallback on earlier versions
        
    }*/
    
    
    // Sign In With Apple
    appleIDLoginInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(.0, 40.0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) * 0.4) textContainer:nil];
    appleIDLoginInfoTextView.font = [UIFont systemFontOfSize:32.0];
    
    [self.view addSubview:appleIDLoginInfoTextView];
    
    [_appleIdButton addTarget:self action:@selector(handleAuthrization:) forControlEvents:UIControlEventTouchUpInside];

    if (@available(iOS 13.0, *)) {
    // Sign In With Apple Button
    ASAuthorizationAppleIDButton *appleIDButton = [ASAuthorizationAppleIDButton new];

    appleIDButton.frame =  CGRectMake(.0, .0, CGRectGetWidth(self.view.frame) - 40.0, 100.0);
    CGPoint origin = CGPointMake(20.0, CGRectGetMidY(self.view.frame));
    CGRect frame = appleIDButton.frame;
    frame.origin = origin;
    appleIDButton.frame = frame;
    appleIDButton.cornerRadius = CGRectGetHeight(appleIDButton.frame)/2;
        
        
    [self.view addSubview:appleIDButton];
        
    
        
    appleIDButton.translatesAutoresizingMaskIntoConstraints = false;
        
    NSLayoutConstraint *centreHorizontallyConstraint = [NSLayoutConstraint
                                              constraintWithItem:appleIDButton
                                              attribute:NSLayoutAttributeCenterX
                                              relatedBy:NSLayoutRelationEqual
                                              toItem:self.view
                                              attribute:NSLayoutAttributeCenterX
                                              multiplier:1.0
                                              constant:0];

    [self.view addConstraint:centreHorizontallyConstraint];
        
        NSLayoutConstraint *centreVerticallyConstraint = [NSLayoutConstraint
                                                  constraintWithItem:appleIDButton
                                                  attribute:NSLayoutAttributeCenterY
                                                  relatedBy:NSLayoutRelationEqual
                                                  toItem:self.view
                                                  attribute:NSLayoutAttributeCenterY
                                                  multiplier:1.0
                                                  constant:0];

        [self.view addConstraint:centreVerticallyConstraint];
        
        [appleIDButton addConstraint:[NSLayoutConstraint constraintWithItem:appleIDButton
                                                              attribute:NSLayoutAttributeWidth
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute: NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:CGRectGetWidth(self.view.frame) - 80.0]];

        // Height constraint
        [appleIDButton addConstraint:[NSLayoutConstraint constraintWithItem:appleIDButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute: NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:70]];
        

        
    [appleIDButton addTarget:self action:@selector(handleAuthrization:) forControlEvents:UIControlEventTouchUpInside];
        
    }

    NSMutableString *mStr = [NSMutableString string];
    [mStr appendString:@"Sign In With Apple \n"];
    appleIDLoginInfoTextView.text = [mStr copy];
    
    appleIDLoginInfoTextView.hidden = true;
    
    
}

- (void)handleAuthrization:(UIButton *)sender {
    if (@available(iOS 13.0, *)) {
        // A mechanism for generating requests to authenticate users based on their Apple ID.
        ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];

        // Creates a new Apple ID authorization request.
        ASAuthorizationAppleIDRequest *request = appleIDProvider.createRequest;
        // The contact information to be requested from the user during authentication.
        request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail,asuthorizationsc];

        // A controller that manages authorization requests created by a provider.
        ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];

        // A delegate that the authorization controller informs about the success or failure of an authorization attempt.
        controller.delegate = self;

        // A delegate that provides a display context in which the system can present an authorization interface to the user.
        controller.presentationContextProvider = self;

        // starts the authorization flows named during controller initialization.
        [controller performRequests];
    }
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)){

    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", controller);
    NSLog(@"%@", authorization);
    
    NSLog(@"authorization.credential：%@", authorization.credential);
    

    NSMutableString *mStr = [NSMutableString string];
    mStr = [appleIDLoginInfoTextView.text mutableCopy];

    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        // ASAuthorizationAppleIDCredential
        ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
        NSString *user = appleIDCredential.user;
        
       // [[NSUserDefaults standardUserDefaults] setValue:user forKey:setCurrentIdentifier];
        [mStr appendString:user?:@""];
        NSString *familyName = appleIDCredential.fullName.familyName;
        [mStr appendString:familyName?:@""];
        NSString *givenName = appleIDCredential.fullName.givenName;
        [mStr appendString:givenName?:@""];
        NSString *email = appleIDCredential.email;
        [mStr appendString:email?:@""];
        NSLog(@"mStr：%@", mStr);
        [mStr appendString:@"\n"];
        appleIDLoginInfoTextView.text = mStr;
        
        NSLog(@"full name :: %@",appleIDCredential.fullName);
        NSLog(@"email :: %@",appleIDCredential.email);
        NSLog(@"token : %@",appleIDCredential.identityToken);
        
       // NSData *data = appleIDCredential.identityToken;
      
        
       // NSString *str1 = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
      //  NSLog(@"utf string :: %@",str1);
        
       
        
        if ([appleIDCredential.email isEqualToString:@""] || [appleIDCredential.email isEqual:[NSNull null]] || appleIDCredential.email == nil) {
            
         
          //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

            EditProfileVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"EditProfileVC"];

            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [Default setValue:appleIDCredential.email forKey:kUSEREMAIL];
            [Default setValue:appleIDCredential.fullName forKey:FULL_NAME];
            
            [self callSignupAPI];
        }
        
    
        
        

    } else if ([authorization.credential isKindOfClass:[ASPasswordCredential class]]) {
        ASPasswordCredential *passwordCredential = authorization.credential;
        NSString *user = passwordCredential.user;
        NSString *password = passwordCredential.password;
        [mStr appendString:user?:@""];
        [mStr appendString:password?:@""];
        [mStr appendString:@"\n"];
        NSLog(@"mStr：%@", mStr);
        appleIDLoginInfoTextView.text = mStr;
    } else {
         mStr = [@"check" mutableCopy];
        appleIDLoginInfoTextView.text = mStr;
    }
}


- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){

    NSLog(@"%s", __FUNCTION__);
    NSLog(@"error ：%@", error);
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"ASAuthorizationErrorCanceled";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"ASAuthorizationErrorFailed";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"ASAuthorizationErrorInvalidResponse";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"ASAuthorizationErrorNotHandled";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"ASAuthorizationErrorUnknown";
            break;
    }

    NSMutableString *mStr = [appleIDLoginInfoTextView.text mutableCopy];
    [mStr appendString:errorMsg];
    [mStr appendString:@"\n"];
    appleIDLoginInfoTextView.text = [mStr copy];

    if (errorMsg) {
        return;
    }

    if (error.localizedDescription) {
        NSMutableString *mStr = [appleIDLoginInfoTextView.text mutableCopy];
        [mStr appendString:error.localizedDescription];
        [mStr appendString:@"\n"];
        appleIDLoginInfoTextView.text = [mStr copy];
    }
    NSLog(@"controller requests：%@", controller.authorizationRequests);
    /*
     ((ASAuthorizationAppleIDRequest *)(controller.authorizationRequests[0])).requestedScopes
     <__NSArrayI 0x2821e2520>(
     full_name,
     email
     )
     */
}

//! Tells the delegate from which window it should present content to the user.
 - (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)){

    NSLog(@"window：%s", __FUNCTION__);
    return self.view.window;
}

- (void)dealloc {

    if (@available(iOS 13.0, *)) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

#pragma mark: Login API call

-(void)callSignupAPI
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *emailStr = [Default valueForKey:kUSEREMAIL];
    NSString *fullnameStr = [Default valueForKey:FULL_NAME];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    NSString *timezoneStr = [NSString stringWithFormat:@"%@",[NSTimeZone systemTimeZone]];
    
    signupDictionary = @{@"email":emailStr,
                               @"googleUniqueId":emailStr,
                               @"device":@"ios",
                               @"type":@"signup",
                               @"fullName":fullnameStr,
                               @"timezone":timezoneStr,
                               @"date":dateStr,
                               @"signUpWith":@"apple",
                            @"userLoginType": @"gmail",
                         @"isFreeTrialUser":@(true)
    };
    

//    NSString *str = [self EncryptParam:passDict];
//
//    NSDictionary *passDict1 = @{@"encryptedch":str};
    
  //  NSLog(@"encrypted dic >> %@",passDict1);
    
    
        NSLog(@"Login Dic : %@",signupDictionary);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:signupDictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"googlesignin" andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    
}
- (void)login:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
     [Processcall hideLoadingWithView];

    if([apiAlias isEqualToString:Status_Code])
    {
      
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Login_response : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            [self setLoginResponse:response1];
        }
        else
        {
            
            if ([[[response1 valueForKey:@"error"] valueForKey:@"error"] isEqualToString:@"This email doesn't exist"]) {
                
                [self callEncryptedSignup];
            }
        }
    }
}

-(void)callEncryptedSignup
{
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:signupDictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"webapi/googlesignin" andParams:jsonString SuccessCallback:@selector(signup:response:) andDelegate:self];
}
- (void)signup:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
     [Processcall hideLoadingWithView];

    
    if([apiAlias isEqualToString:Status_Code])
    {
      
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"signup response : %@",response1);
        if ([[response1 valueForKey:@"signupSuccess"] integerValue] == 1) {
            
            [self callSignupAPI];
        }
        else
        {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
            
        }
    }
}
-(void)setLoginResponse:(NSDictionary *)response1
{

    [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:INCOUTCALL];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:XPH_EXTRA_HEADER_REMINDER];
    
    
                self->endpointUserName = [response1 valueForKey:@"data"][@"username"];
                self->endpointPassWord = [response1 valueForKey:@"data"][@"password"];
                self->userId = [response1 valueForKey:@"data"][@"_id"];
                self->authToken = [response1 valueForKey:@"data"][@"authToken"];
                self->fullName = [response1 valueForKey:@"data"][@"fullName"];
                self->plivoAuthID = [response1 valueForKey:@"data"][@"plivoAuthId"];
                self->plivoAuthToken = [response1 valueForKey:@"data"][@"plivoAuthToken"];//
                self->planDisplayName = [response1 valueForKey:@"data"][@"planDisplayName"];
                self->parentId = [response1 valueForKey:@"data"][@"parentId"];
                self->callTransfer = [response1 valueForKey:@"data"][@"callTransfer"];
                endPoints = [response1 valueForKey:@"data"][@"endpointInfo"];
                NSString *email = [response1 valueForKey:@"data"][@"email"];
                NSString *Provider = [response1 valueForKey:@"data"][@"provider"];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
                if (self->endpointUserName != nil && self->endpointPassWord !=nil) {
                    
                   
                    [Default setValue:self->endpointUserName forKey:ENDPOINT_USERNAME];
                    [Default setValue:self->endpointPassWord forKey:ENDPOINT_PASSORD];
                    [Default setValue:email forKey:kUSEREMAIL];
                    [Default setValue:self->userId forKey:USER_ID];
                    [Default setValue:self->authToken forKey:AUTH_TOKEN];
                    [Default setValue:self->fullName forKey:FULL_NAME];
                    [Default setValue:self->plivoAuthID forKey:CALLHIPPO_AUTH_ID];
                    [Default setValue:self->plivoAuthToken forKey:CALLHIPPO_AUTH_TOKEN];
                    [Default setValue:self->planDisplayName forKey:PLAN_DISPLAY_NAME];
                    [Default setValue:self->parentId forKey:PARENT_ID];
                    [Default setValue:Timer_Stop forKey:Timer_Call];
                    [Default setValue:self->callTransfer forKey:CALL_TRANSFER];
                    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
                    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
                    [Default setObject:[NSMutableArray arrayWithArray:[response1 valueForKey:@"data"][@"moduleRights"]] forKey:kMODUALRIGHTS];
                    [Default setValue:[response1 valueForKey:@"data"][@"isDisableSms"] forKey:kSMSRIGHTS];
                    [Default setValue:[response1 valueForKey:@"data"][@"thinqValue"] forKey:kTHINQVALUE];
                    [Default setValue:[response1 valueForKey:@"data"][@"thinqAccess"] forKey:kTHINQACCESS];
                    [Default setValue:[response1 valueForKey:@"data"][@"providerHostname"] forKey:kPROVIDERHOSTNAME];
                    [Default setValue:[response1 valueForKey:@"data"][@"iossipProtocol"] forKey:kIOSSIPPROTOCOL];
                    [Default setValue:[NSString stringWithFormat:@"%@",[response1 valueForKey:@"data"][@"extensionNumber"]] forKey:ownExtensionNumber];
                    [Default setValue:@"login" forKey:UserLoginStatus];
                    [Default setValue:Provider forKey:Login_Provider];
                    
                    [Default setValue:[response1 valueForKey:@"data"][@"blacklist"] forKey:kIsBlackList];
                    [Default setValue:[[[response1 valueForKey:@"data"] valueForKey:@"blockNumberList"] valueForKey:@"number"] forKey:kblackListedArray];
                    
                    [Default setValue:[response1 valueForKey:@"data"][@"recordControl"] forKey:kISRecordingInPlan];
                    
                    if (![[Default valueForKey:kEmailIdForDB] isEqualToString:[Default valueForKey:kUSEREMAIL]]) {
                        
                        [Default setBool:true forKey:kNeedToIgnoreUpdatedContact];
                        
                       // [UtilsClass makeToast:@"Fetching contacts this may take upto few minutes,Please don't close the app."];
                        [[GlobalData sharedGlobalData] flushAllContactsAndFetchNew];
                        
                    }
                    
                    [Default setValue:[Default valueForKey:kUSEREMAIL] forKey:kEmailIdForDB];
                    
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"twilioEdge"] forKey:kTwilioEdgeValue];
                    
                     NSArray *phoneArray = [[NSUserDefaults standardUserDefaults] objectForKey:kMODUALRIGHTS];
                      
                       
                       if (phoneArray.count > 0) {
                           for (NSDictionary *item in phoneArray) {
                               NSString *name = item[@"name"];
                               
                               if ([name isEqualToString:@"feedBacks"]) {
                                    [Default setValue:item[@"active"] forKey:kISFeedbackRightsForSub];
                               }
                               
                           }
                       }
                    
                    //NSLog(@"login::  blacklisted saved : %@", [Default valueForKey:kblackListedArray]);
                    
                    //pri
                    [Default setValue:@"+1" forKey:klastDialCountryCode];
                    [Default setValue:@"United States" forKey:klastDialCountryName];
                    

                    
                   
                    [Default setValue:[[response1 valueForKey:@"data"]valueForKey:@"postCallSurvey"] forKey:kISPostCallSurvey];
                    
                    BOOL nummask = [[[response1 valueForKey:@"data"] valueForKey:@"numberMasking"] boolValue];
                    [Default setBool:nummask forKey:kIsNumberMask];

                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"] forKey:kTelephonyProviderSwitch];
                     
                    NSLog(@"telephony in login >> %@",[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"]);


                    
                    [Default setValue:[response1 valueForKey:@"data"][@"gamification"] forKey:kbadgeTag];
                    
                    if([[Default valueForKey:kbadgeTag] isEqualToString:@"Beginner"])
                    {
                        [Default setValue:@"badge_beginner" forKey:kbadgeImage];
                    }
                    else if([[Default valueForKey:kbadgeTag] isEqualToString:@"Sharpshooter"])
                    {
                        [Default setValue:@"badge_silver" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Calling Master"])
                    {
                         [Default setValue:@"badge_gold" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"The King"])
                    {
                         [Default setValue:@"badge_platinum" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Ultimate Champion"])
                    {
                         [Default setValue:@"badge_champion" forKey:kbadgeImage];
                    }
                    
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchSipUser"] forKey:kFreeswitchUsername];
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchSipPass"] forKey:kFreeswitchPassword];
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchDomain"] forKey:kFreeswitchDomain];

                    
                        
                    [self activeSubUser:[response1 valueForKey:@"data"][@"activeSubUsers"]];
                    [self outGoingCallCountryRest:[response1 valueForKey:@"data"][@"outCallCountries"]];
                    [self purchaseNumber:response1[@"data"]];
                    
                    NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"];
                    NSString *TwilioToken = @"";
                    if([response1 valueForKey:@"data"][@"twilioToken"])
                    {
                        TwilioToken = [response1 valueForKey:@"data"][@"twilioToken"] ? [response1 valueForKey:@"data"][@"twilioToken"] : @"";
                        [Default setValue:TwilioToken forKey:twilio_token];
                        NSLog(@"LoginVC : 1 :  Twilio token : %@",TwilioToken);
                    }
                    else
                    {
                        [Default setValue:TwilioToken forKey:twilio_token];
                        NSLog(@"LoginVC : 2 :  Twilio token : %@",TwilioToken);
                    }
                    [Default synchronize];
                    NSLog(@"\n \n VOIP TOKEN  %@ \n \n ",Voiptoken);
                    if(![TwilioToken isEqualToString:@""])
                    {
                        //set twilio edge
                        if ([Default valueForKey:kTwilioEdgeValue]) {
                        if (![[Default valueForKey:kTwilioEdgeValue] isEqualToString:@""]) {
                            
                           [TwilioVoiceSDK setEdge:[Default valueForKey:kTwilioEdgeValue]];
                            NSLog(@"edge setup:: %@",[Default valueForKey:kTwilioEdgeValue]);
                        }
                        }
                        
                        [TwilioVoiceSDK registerWithAccessToken:TwilioToken
                                                 deviceToken:[Default valueForKey:@"VOIPTOKEN_DATA"]
                                                  completion:^(NSError *error) {
                        }];
                    }
   
                    
                    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
                    
                     [[Phone sharedInstance] setDelegate:appDelegate];
                    
                    if (endpointUserName!= nil && endpointPassWord != nil) {
                        
                        [[Phone sharedInstance] login:endpointUserName endPointPassword:endpointPassWord deviceToken:[Default valueForKey:@"deviceTokenForPlivo"]];
                    }
                    
                    if ([Default valueForKey:kFreeswitchUsername]) {
                        
    //                   [LinphoneManager.instance startLinphoneCore];
                        
            [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                        

                    }
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [Processcall hideLoadingWithView];
                                        
                        [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"stripeId"] forKey:kStripeId];
                        
                        if ([[[response1 valueForKey:@"data"] valueForKey:@"stripeId"] isEqualToString:@""]) {
                            
                            SubscribeViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"SubscribeViewController"];
                            [[self navigationController] pushViewController:vc animated:YES];
                        }
                        else
                        {
                           
                            NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                           NSArray *numbersArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                            
                            if(numbersArr.count>0)
                            {
                                DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
                                [[self navigationController] pushViewController:vc animated:YES];
                            }
                            else
                            {
                                CountryListViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CountryListViewController"];
                                vc.fromStr = @"subscribe";
                                [[self navigationController] pushViewController:vc animated:YES];
                            }
                        }
                        
                       // [self getCredits];
                        
                            [appDelegate updatefcmtoken];
                    });
                      
                            
                        
                    
                } else {
                    [Processcall hideLoadingWithView];
                    [UtilsClass showAlert:@"Login Again" contro:self];
                }
}
-(void)purchaseNumber:(NSDictionary *)response1{
    
    
    NSDictionary*numberData = [response1 objectForKey:@"numberData"];
    NSArray *purchase_number = [numberData valueForKey:@"numbers"];
    
    
    if ([purchase_number count] == 1) {
        if ([[[purchase_number objectAtIndex:0] objectForKey:ISDummyNumber] intValue] == 0) {
            
            return;
        }
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
//    [GlobalData sharedGlobalData].Number_Selection = [[NSMutableArray alloc] init];
//    [[GlobalData sharedGlobalData].Number_Selection addObjectsFromArray:purchase_number];
    
    
}

-(void)activeSubUser:(NSArray *)response{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:ACTIVE_SUB_USER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)outGoingCallCountryRest:(NSArray *)response{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:OUTGOING_CALL_COUNTRY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(void)getCredits
{
    
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";

    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
    NSDictionary *passDict = @{@"userId":userID,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"token":fcmtoken,
                               @"versionInfo":VERSIONINFO,};
    NSLog(@"getcredit : %@",passDict);
    NSLog(@"getcredit : %@%@",SERVERNAME,url);
    NSLog(@"getcredit auth token: %@",[Default valueForKey:AUTH_TOKEN]);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getCredits:response:) andDelegate:self];
}

- (void)getCredits:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
   
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
            // NSLog(@"Trushang : getCredits : %@",response1);
       
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            
        }
    }
}

/*
- (nonnull ASPresentationAnchor)presentationAnchorForWebAuthenticationSession:(nonnull ASWebAuthenticationSession *)session {
    
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
    
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    
}

- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
    
}

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
    
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    
}

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
    
}

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
    
}

- (void)setNeedsFocusUpdate {
    
}

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
    
}

- (void)updateFocusIfNeeded {
    
}
*/
@end
