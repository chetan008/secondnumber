//
//  NumberSettingsVC.h
//  SecondNumber
//
//  Created by Apple on 16/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NumberSettingsVC : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lbl_number;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UISwitch *switch_record;
@property (weak, nonatomic) IBOutlet UISwitch *switch_pauserecord;

@property (weak, nonatomic) IBOutlet UIButton *btn_verify;
@property (weak, nonatomic) IBOutlet UIButton *btn_editname;
@property (weak, nonatomic) IBOutlet UIImageView *img_numberflag;
@property (strong, nonatomic)  NSArray *selectedNumberDetail;


@property (weak, nonatomic) IBOutlet UISwitch *switch_callqueue;
@property (weak, nonatomic) IBOutlet UIView *view_pauseRecord;


@end

NS_ASSUME_NONNULL_END
