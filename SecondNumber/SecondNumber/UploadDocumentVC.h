//
//  UploadDocumentVC.h
//  SecondNumber
//
//  Created by Apple on 04/01/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UploadDocumentVC : UIViewController<UIDocumentPickerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtname;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress1;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress2;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtZipcode;


@property (weak, nonatomic) IBOutlet UITextView *txtview_docRequired;

@property (weak, nonatomic) IBOutlet UIButton *btn_submit;
@property (weak, nonatomic) IBOutlet UIButton *btn_upload;
@property (weak, nonatomic) IBOutlet UILabel *lbl_docname;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtviewHeightConstraint;

@property (strong, nonatomic) NSDictionary *numberArray;


@end

NS_ASSUME_NONNULL_END
