//
//  LoginViewController.h
//  SecondNumber
//
//  Created by Apple on 18/11/21.
//

#import <UIKit/UIKit.h>
#import <AuthenticationServices/AuthenticationServices.h>
#import "SubscribeViewController.h"

NS_ASSUME_NONNULL_BEGIN
//extern NSString* const setCurrentIdentifier;

@interface LoginViewController : UIViewController <ASAuthorizationControllerDelegate,ASWebAuthenticationPresentationContextProviding>

@property (nonatomic, strong) UITextView *appleIDLoginInfoTextView;
//NSString* const setCurrentIdentifier = @"setCurrentIdentifier";
@property (nonatomic, strong) IBOutlet ASAuthorizationAppleIDButton *appleIdButton;

@end

NS_ASSUME_NONNULL_END
