//
//  UserProfileVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "UserProfileVC.h"
#import "ChangePasswordVC.h"
#import "UtilsClass.h"
#import "MainViewController.h"
#import "Constant.h"


@interface UserProfileVC ()

@end

@implementation UserProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lbl_username.text = [Default valueForKey:FULL_NAME];
    _lbl_useremail.text = [Default valueForKey:kUSEREMAIL];


    self.navigationController.navigationBarHidden = NO;
//    [[UINavigationBar appearance] setTranslucent:false];
//    UINavigationBar.appearance.translucent = false;
    
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;

    [UtilsClass view_navigation_title:self title:@"User Profile"color:UIColor.whiteColor];}

-(IBAction)changePasswordClick:(id)sender
{
    ChangePasswordVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"ChangePasswordVC"];

    [[self navigationController] pushViewController:vc animated:YES];
    
    
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (IBAction)btnBackClicked:(id)sender {
    

    [[self navigationController] popViewControllerAnimated:true];

    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
