//
//  ChangePasswordVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "ChangePasswordVC.h"
#import "UtilsClass.h"
#import "Constant.h"
#import "WebApiController.h"


@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // self.navigationController.navigationBarHidden = NO;

    [UtilsClass view_navigation_title:self title:@"Change Password"color:UIColor.whiteColor];
    
    [_btn_save setEnabled:true];
    

    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    
    _txtNewPassword.delegate = self;
    _txtOldPassword.delegate = self;
   // _txtConfirmPassword.delegate = self;
    
    [_txtOldPassword addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    [_txtNewPassword addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
   /* [_txtConfirmPassword addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];*/
    
   
    
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
  /*  if (textField == _txtOldPassword) {
        if ([_txtOldPassword.text isEqualToString:@""])
        {
            _lblErrorInOld.hidden = false;
            _lblErrorInOld.text = @"Field can't be empty";

        }
    }
    else if (textField == _txtNewPassword) {
        if ([_txtNewPassword.text isEqualToString:@""]) {
            _lblErrorInNew.hidden = false;
            _lblErrorInNew.text = @"Field can't be empty";
        }
    }
    else if (textField == _txtConfirmPassword) {
        
        if ([_txtConfirmPassword.text isEqualToString:@""]) {
            _lblErrorInConfirm.hidden = false;
            _lblErrorInConfirm.text = @"Field can't be empty";
        }
    }*/
    
    if (textField == _txtOldPassword) {
        
        if ([_txtOldPassword.text isEqualToString:@""])
        {
            _lblErrorInOld.hidden = false;
            _lblErrorInOld.text = @"Field can't be empty";
            _viewCondition.hidden = true;

        }
        else
        {
            _lblErrorInOld.hidden = true;
            _lblErrorInOld.text = @"";
            _viewCondition.hidden = true;

        }
    }
    else if (textField == _txtNewPassword) {
     
        if ([_txtNewPassword.text isEqualToString:@""]) {
            _lblErrorInNew.hidden = false;
            _lblErrorInNew.text = @"Field can't be empty";
            _viewCondition.hidden = false;
        }
        else
        {
            if ([self passwordRegexMatch:_txtNewPassword.text] == false) {
                _lblErrorInNew.hidden = false;
                _lblErrorInNew.text = @"Password is too weak";
                _viewCondition.hidden = false;
            }
            else
            {
                _lblErrorInNew.hidden = true;
                _lblErrorInNew.text = @"";
                _viewCondition.hidden = true;

            }
        }
    }
   /* else if (textField == _txtConfirmPassword) {
     
        if ([_txtConfirmPassword.text isEqualToString:@""]) {
            _lblErrorInConfirm.hidden = false;
            _lblErrorInConfirm.text = @"Field can't be empty";
            _viewCondition.hidden = false;
        }
        else
        {
            if ([self passwordRegexMatch:_txtConfirmPassword.text] == false) {
                _lblErrorInConfirm.hidden = false;
                _lblErrorInConfirm.text = @"Password is too weak";
                _viewCondition.hidden = false;
            }
            else
            {
               if (![_txtNewPassword.text isEqualToString:_txtConfirmPassword.text]) {
                   _lblErrorInConfirm.hidden = false;
                   _lblErrorInConfirm.text = @"Confirm Password doesn't match with the password you entered";
                   _viewCondition.hidden = false;
               }
               else
               {
                   
                   _lblErrorInConfirm.hidden = true;
                   _lblErrorInConfirm.text = @"";
                   _viewCondition.hidden = true;
               }
            }
            
        }
    }*/
    
    
}
-(IBAction)textFieldDidChange:(id)sender{
    
    if ([sender tag] == 0) {
        
        if ([_txtOldPassword.text isEqualToString:@""])
        {
            _lblErrorInOld.hidden = false;
            _lblErrorInOld.text = @"Field can't be empty";
            _viewCondition.hidden = true;

        }
        else
        {
            _lblErrorInOld.hidden = true;
            _lblErrorInOld.text = @"";
            _viewCondition.hidden = true;

        }
    }
    else if ([sender tag] == 1) {
     
        if ([_txtNewPassword.text isEqualToString:@""]) {
            _lblErrorInNew.hidden = false;
            _lblErrorInNew.text = @"Field can't be empty";
            _viewCondition.hidden = false;
        }
        else
        {
            if ([self passwordRegexMatch:_txtNewPassword.text] == false) {
                _lblErrorInNew.hidden = false;
                _lblErrorInNew.text = @"Password is too weak";
                _viewCondition.hidden = false;
            }
            else
            {
                _lblErrorInNew.hidden = true;
                _lblErrorInNew.text = @"";
                _viewCondition.hidden = true;

            }
        }
    }
   /* else if ([sender tag] == 2) {
     
        if ([_txtConfirmPassword.text isEqualToString:@""]) {
            _lblErrorInConfirm.hidden = false;
            _lblErrorInConfirm.text = @"Field can't be empty";
            _viewCondition.hidden = false;
        }
        else
        {
            if ([self passwordRegexMatch:_txtConfirmPassword.text] == false) {
                _lblErrorInConfirm.hidden = false;
                _lblErrorInConfirm.text = @"Password is too weak";
                _viewCondition.hidden = false;
            }
            else
            {
               if (![_txtNewPassword.text isEqualToString:_txtConfirmPassword.text]) {
                   _lblErrorInConfirm.hidden = false;
                   _lblErrorInConfirm.text = @"Confirm Password doesn't match with the password you entered";
                   _viewCondition.hidden = false;
               }
               else
               {
                   
                   _lblErrorInConfirm.hidden = true;
                   _lblErrorInConfirm.text = @"";
                   _viewCondition.hidden = true;
               }
            }
            
        }
    }*/
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        
    return true;
}
-(IBAction)saveBtnClick:(id)sender
{
    
    
    
//    if (![_txtOldPassword.text isEqualToString:@""] || ![_txtNewPassword.text isEqualToString:@""] || ![_txtConfirmPassword.text isEqualToString:@""]) {
    
    if (![_txtOldPassword.text isEqualToString:@""] || ![_txtNewPassword.text isEqualToString:@""]) {
        
       // if ([_txtNewPassword.text isEqualToString:_txtConfirmPassword.text]) {
            
            if ([self passwordRegexMatch:_txtNewPassword.text]) {
                
                [self changePasswordAPICall];
            }
        }
        
    //}
}
-(BOOL)passwordRegexMatch:(NSString *)text
{
//    NSString *passRegex = @"^(.*[A-Z])(.*[a-z])(.*[0-9])(.*[@#$%^&+=])(.*(?=\\S+$)).{8,}";
    
    NSString *passRegex = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[@#$%^&+=])(?=.*(\\S+$)).{8,}$";
    
    NSPredicate *predpass = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passRegex];
    
    return [predpass evaluateWithObject:text];
}
-(void)changePasswordAPICall
{
   
    WebApiController *obj;
        NSString *userId = [Default valueForKey:USER_ID];
        
        NSDictionary *passDict;
      
            passDict = @{
                @"password":_txtNewPassword.text,
                @"isFirstTime":@(false),
                @"oldPassword" : _txtOldPassword.text,
                @"user_id" : userId
            };
      
        
        NSLog(@"change password Dictionary ************ : %@",passDict);
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
            
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        
        [obj callAPI_POST_RAW:@"changePassword" andParams:jsonString SuccessCallback:@selector(changepasswordresponse:response:) andDelegate:self];
    
}

- (void)changepasswordresponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];

    NSLog(@"Encrypted Response : change password : %@",response1);

    NSLog(@"API alias:: %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else if([apiAlias isEqualToString:@"404"])
    {
        if([[[response1 valueForKey:@"error"] valueForKey:@"error"] isEqualToString:@"You have entered the wrong current password"])
        {
            _lblErrorInOld.text = [response1 valueForKey:@"error"][@"error"];
            
            _lblErrorInOld.hidden = false;
        }
        else if([[[response1 valueForKey:@"error"] valueForKey:@"error"] isEqualToString:@"You used this password recently. Please choose a different one."])
        {
            _lblErrorInNew.text = [response1 valueForKey:@"error"][@"error"];
           
            _lblErrorInNew.hidden = false;
        }
        else
        {
            [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
        }
        
        
    }
    else
    {
        [UtilsClass makeToast:[response1 valueForKey:@"message"]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
           
            [UtilsClass logoutUser:self error:@"" showAlert:NO];
        });
       
    }
    
}
- (IBAction)btnBackClicked:(id)sender {
    
    [[self navigationController] popViewControllerAnimated:true];
    
}
-(IBAction)btnEyeForOldClick:(id)sender
{
    if ( [_txtOldPassword isSecureTextEntry]) {
        [_txtOldPassword setSecureTextEntry:false];
        [_btnEyeForOld setBackgroundImage:[UIImage imageNamed:@"icon_eye_slash"] forState:UIControlStateNormal];
    }
    else
    {
        [_txtOldPassword setSecureTextEntry:true];
        [_btnEyeForOld setBackgroundImage:[UIImage imageNamed:@"icon_eye"] forState:UIControlStateNormal];
    }
   
}
-(IBAction)btnEyeForNewClick:(id)sender
{
    if ( [_txtNewPassword isSecureTextEntry]) {
        [_txtNewPassword setSecureTextEntry:false];
        [_btnEyeForNew setBackgroundImage:[UIImage imageNamed:@"icon_eye_slash"] forState:UIControlStateNormal];
    }
    else
    {
        [_txtNewPassword setSecureTextEntry:true];
        [_btnEyeForNew setBackgroundImage:[UIImage imageNamed:@"icon_eye"] forState:UIControlStateNormal];
    }
}
-(IBAction)btnEyeForConfirmClick:(id)sender
{
   /* if ([_txtConfirmPassword isSecureTextEntry]) {
        [_txtConfirmPassword setSecureTextEntry:false];
        [_btnEyeForConfirm setBackgroundImage:[UIImage imageNamed:@"icon_eye_slash"] forState:UIControlStateNormal];
    }
    else
    {
        [_txtConfirmPassword setSecureTextEntry:true];
        [_btnEyeForConfirm setBackgroundImage:[UIImage imageNamed:@"icon_eye"] forState:UIControlStateNormal];
    }*/
}

@end

