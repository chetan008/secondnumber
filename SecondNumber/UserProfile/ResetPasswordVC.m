//
//  ResetPasswordVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ResetPasswordVC.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"

@interface ResetPasswordVC ()
{
    WebApiController *obj;
}
@end

@implementation ResetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self view_design];
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (void)viewWillAppear:(BOOL)animated
{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UtilsClass view_bottom_round_edge:self->_viewCurv desiredCurve:1];
    });
}

-(void)view_design
{
    [UtilsClass textfiled_shadow_boder:_txtUserName];
    
    _btnSignIn.layer.cornerRadius = 5.0;
    _img_icon.layer.cornerRadius = _img_icon.frame.size.height / 2;
    _img_icon.clipsToBounds = YES;
}

- (IBAction)btnSigIn:(id)sender {
    [self.txtUserName resignFirstResponder];
    if ([self.txtUserName.text isEqualToString:@""])
    {
        [UtilsClass showAlert:@"Please enter email id." contro:self];
        return;
    }
    else
    {
        
            if([UtilsClass isNetworkAvailable]) {
                [self loginApiCall];
            } else {
                [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
            }
    }
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)loginApiCall {
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSDictionary *passDict = @{@"email":self.txtUserName.text
                               };
    
    //NSLog(@"Login Dic : %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
   // [obj callAPI_POST_RAW:@"forgotpassword" andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    [obj callAPI_POST_RAW:@"mobileforgotpassword" andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"Login_response : %@",response1);
    
    //NSLog(@"TRUSHANG : STATUSCODE **************13  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"forgot response : %@,%@",response1,Status_Code);
        
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        [Processcall hideLoadingWithView];
        [self.navigationController popViewControllerAnimated:YES];
         dispatch_async( dispatch_get_main_queue(), ^{
             [UtilsClass showAlert:@"An email has been sent to reset your password." contro:self];
         });
    }else {
        //        _txtUserName.text = @"";
        [Processcall hideLoadingWithView];
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
                [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
            }
        }
        @catch (NSException *exception) {
        }
    }
        
    }
}




@end
