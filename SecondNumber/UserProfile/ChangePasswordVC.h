//
//  ChangePasswordVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import "UIViewController+LGSideMenuController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangePasswordVC : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtOldPassword;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtNewPassword;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtConfirmPassword;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_save;
@property (strong, nonatomic) IBOutlet UIButton *btnEyeForOld;
@property (strong, nonatomic) IBOutlet UIButton *btnEyeForNew;
@property (strong, nonatomic) IBOutlet UIButton *btnEyeForConfirm;


@property (strong, nonatomic) IBOutlet UILabel *lblNote;
@property (strong, nonatomic) IBOutlet UILabel *lblCondition;
@property (strong, nonatomic) IBOutlet UIButton *lblCondition1;
@property (strong, nonatomic) IBOutlet UIButton *lblCondition2;
@property (strong, nonatomic) IBOutlet UIButton *lblCondition3;
@property (strong, nonatomic) IBOutlet UIButton *lblCondition4;

@property (strong, nonatomic) IBOutlet UIView *viewCondition;


@property (strong, nonatomic) IBOutlet UILabel *lblErrorInOld;
@property (strong, nonatomic) IBOutlet UILabel *lblErrorInNew;
@property (strong, nonatomic) IBOutlet UILabel *lblErrorInConfirm;




@end

NS_ASSUME_NONNULL_END

