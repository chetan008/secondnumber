//
//  UserProfileVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>
#import "UIViewController+LGSideMenuController.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserProfileVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *lbl_username;
@property (strong, nonatomic) IBOutlet UILabel *lbl_useremail;

@end

NS_ASSUME_NONNULL_END

