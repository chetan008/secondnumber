//
//  LoginVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>
#import "CryptLib.h"
#import <GoogleSignIn/GIDSignIn.h>

//#import <reCAPTCHA-OC/reCAPTCHA.h>
//#import <reCAPTCHA-OC/ReCaptchaViewController.h>

@import GoogleSignIn;

NS_ASSUME_NONNULL_BEGIN

//@interface LoginVC : UIViewController<GIDSignInDelegate,RobotCheckResponseDelegate>
@interface LoginVC : UIViewController<GIDSignInDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblOR;
@property (weak, nonatomic) IBOutlet UIImageView *img_icon;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIView *viewCurv;
@property (strong, nonatomic) IBOutlet GIDSignInButton *btnGoogleSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;

@property (weak, nonatomic) IBOutlet UILabel *lbl_facts;
@property (strong, nonatomic) IBOutlet UIImageView *icon_facts;
@property (strong, nonatomic) IBOutlet UITextField *txt_otp;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPass;
@property (weak, nonatomic) IBOutlet UIButton *btnEyeForPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnResendCode;
@property (weak, nonatomic) IBOutlet UILabel *lbl_otpsent;



@end

NS_ASSUME_NONNULL_END
