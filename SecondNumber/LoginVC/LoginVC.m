//
//  LoginVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "CryptLib.h"
#import "LoginVC.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "DialerVC.h"
#import "Lin_Utility.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "GlobalData.h"
#import "IQKeyboardManager.h"
#import "AppDelegate.h"
#import "Phone.h"
#import "ResetPasswordVC.h"
#import "CryptoJS_Objc.h"

#define GoogleClientId @"524143824291-mkqmveba6d9sdpk514vq6ktkt194p102.apps.googleusercontent.com";
//pri-fabric
//#import <reCAPTCHA-OC/ReCaptchaViewController.h>

@import Firebase;
@interface LoginVC () {
    NSString *endpointUserName;
    NSString *parentId;
    NSString *endpointPassWord;
    NSString *userId;
    NSString *authToken;
    NSString *fullName;
    NSString *plivoAuthID;
    NSString *plivoAuthToken;
    NSString *planDisplayName;
    NSString *callTransfer;
    NSMutableArray *Mixallcontact;
    NSMutableArray *endPoints;
    WebApiController *obj;
    
    NSString *recaptchatoken;
}

@property BOOL firstBoxSelected;
@property (nonatomic,strong)GIDSignIn *singIn;
@end

@implementation LoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    

  //Encryption commented   [Default setBool:false forKey:kencryptionInMobile];
    
    recaptchatoken = @"";
    [_btnCheckbox setBackgroundImage:[UIImage imageNamed:@"blank-check-box.png"]forState:UIControlStateNormal];
    [_btnCheckbox setBackgroundImage:[UIImage imageNamed:@"check-box.png"]forState:UIControlStateSelected];
    [_btnCheckbox addTarget:self action:@selector(captchaselected:) forControlEvents:UIControlEventTouchUpInside];
    
    [self settingApi];
    [GIDSignIn sharedInstance].presentingViewController = self;
    [[GIDSignIn sharedInstance] restorePreviousSignIn];
    [GIDSignIn sharedInstance].delegate = self;
    _singIn = [GIDSignIn sharedInstance];
    _singIn.shouldFetchBasicProfile = YES;
    _singIn.clientID = GoogleClientId;
    static NSString* const hasRunAppOnceKey = @"hasRunAppOnceKey";
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:hasRunAppOnceKey] == NO)
    {
        [defaults setBool:YES forKey:hasRunAppOnceKey];
        [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:INCOUTCALL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:XPH_EXTRA_HEADER_REMINDER];
    }
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    self.navigationController.navigationBar.hidden = true;
    _txtUserName.text = Login_Email;
    _txtPassword.text = Login_Pass;
    [self view_design];
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    mainViewController.sideMenuController.leftViewDisabled = true;
    mainViewController.sideMenuController.rightViewDisabled = true;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    
}

-(void)view_design
{
    [self showHippoFacts];
    
    [UtilsClass textfiled_shadow_boder:_txtUserName];
    [UtilsClass textfiled_shadow_boder:_txtPassword];
    [UtilsClass textfiled_shadow_boder:_txt_otp];
    
    _btnSignIn.layer.cornerRadius = 5.0;
     _btnVerify.layer.cornerRadius = 5.0;
    
    _img_icon.layer.cornerRadius = _img_icon.frame.size.height / 2;
    _img_icon.clipsToBounds = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    mainViewController.sideMenuController.leftViewDisabled = false;
}

- (void)viewWillAppear:(BOOL)animated
{
    endPoints = [[NSMutableArray alloc]init];
    [self keybord_setup];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UtilsClass view_bottom_round_edge:self->_viewCurv desiredCurve:1];
    });
}

- (IBAction)btnVerifyClick:(id)sender {
    
    if (![_txt_otp.text isEqualToString:@""]) {
        
        _btnResendCode.hidden = true;

         [self verifyAPICall];
    }
    else
    {
           [UtilsClass showAlert:@"Please Enter OTP" contro:self];
    }
    
}
- (IBAction)btnSigIn:(id)sender {
    
    //if (![recaptchatoken isEqualToString:@""]) {
    
        
       
        [self.txtUserName resignFirstResponder];
            [self.txtPassword resignFirstResponder];
            if ([self.txtUserName.text isEqualToString:@""] || [self.txtPassword.text isEqualToString:@""])
            {
                [UtilsClass showAlert:@"Email and password is required." contro:self];
                _txtPassword.text = @"";
                return;
            }
            else
            {
                if ([self.txtUserName.text isEqualToString:@""])
                {
                    [UtilsClass showAlert:@"Please enter username." contro:self];
                    _txtPassword.text = @"";
                }
                else if([self.txtPassword.text isEqualToString:@""]) {
                    [UtilsClass showAlert:@"Please enter password." contro:self];
                }else {
                    if([UtilsClass isNetworkAvailable]) {
                        [FIRAnalytics logEventWithName:@"ch_login" parameters:@{@"username": self.txtUserName.text}];
                        [self loginApiCall];
                    } else {
                        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
                    }
                }
            }
        
    
   /* }
    else
    {
        [UtilsClass showAlert:@"Please verify that you are a human being." contro:self];
    }*/
}

- (void)loginApiCall {
    
   /* NSString *email = self.txtUserName.text;
    NSString *pass = self.txtPassword.text;
    
    NSString *key  = @"69aaafead5e118e540ace5ba430a1b04";
    
    CryptLib *lib = [[CryptLib alloc]init];
    NSString *cipherusertxt = [lib encryptPlainTextRandomIVWithPlainText:email key:key];
    NSString *cipherpasstxt = [lib encryptPlainTextRandomIVWithPlainText:pass key:key];*/

    

  //  NSLog(@"cipher txt: %@",cipherusertxt);

   // NSString *decyptTxt = [lib decryptCipherTextRandomIVWithCipherText:cipherTxt key:key];
    
  //  NSLog(@"decrypt text : %@",decyptTxt);
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";
    NSString *Version_Info = [Default valueForKey:IS_VERSIONINFO] ? [Default valueForKey:IS_VERSIONINFO] : @"";
    
 /*   bool isencrypt = true;
    NSDictionary *passDict = @{@"email":self.txtUserName.text,
                               @"password":cipherpasstxt,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"versionInfo":VERSIONINFO,
                               @"token":fcmtoken,
                               @"isEncrypt":@(isencrypt)

    };*/
    
    NSString *osversion = [NSString stringWithFormat:@"%d",[[[UIDevice currentDevice] systemVersion] intValue]];
        
        NSLog(@"os version :: %@",osversion);
    
    NSDictionary *passDict = @{@"email":self.txtUserName.text,
                               @"password":self.txtPassword.text,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"versionInfo":VERSIONINFO,
                               @"token":fcmtoken,
                               @"iosOSInfo":osversion

    };
    

//    NSString *str = [self EncryptParam:passDict];
//
//    NSDictionary *passDict1 = @{@"encryptedch":str};
    
  //  NSLog(@"encrypted dic >> %@",passDict1);
    
    
        NSLog(@"Login URL : %@%@",SERVERNAME,Login_URL);
        NSLog(@"Login Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
/*
    NSString *key = @"F8A2BB3F418FEBDD632535271FB50866";
       CryptLib *lib = [[CryptLib alloc]init];
       NSString *cipherpasstxt = [lib encryptPlainTextRandomIVWithPlainText:jsonString key:key];
    
    
    NSDictionary *passDict1 = @{@"encryptedch":cipherpasstxt
                              

    };
    
    NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    
    NSString *jsonString1;
    if (! jsonData1) {
    } else {
        jsonString1 = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
*/
    
    //using cryptojs
   /* NSString *key = @"F8A2BB3F418FEBDD632535271FB50866";
    CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
    NSString *encryptedStr = [cryptojs encrypt:jsonString password:key];
    
    
    NSDictionary *passDict1 = @{@"encryptedch":encryptedStr
                              

    };
    
    NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    
    NSString *jsonString1;
    if (! jsonData1) {
    } else {
        jsonString1 = [[NSString alloc] initWithData:jsonData1 encoding:NSUTF8StringEncoding];
    }*/
    

    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:Login_URL andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    
  //aes  [obj callAPI_POST_RAW:Login_URL andParams:jsonString1 SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
     [Processcall hideLoadingWithView];

    
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Login_response : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            int is2faenable = [[[response1 valueForKey:@"data"] valueForKey:@"is2faEnabled"] intValue];
            
            if (is2faenable == 1) {
/*
<<<<<<< HEAD
               
                [Default setValue:self->endpointUserName forKey:ENDPOINT_USERNAME];
                [Default setValue:self->endpointPassWord forKey:ENDPOINT_PASSORD];
                [Default setValue:email forKey:kUSEREMAIL];
                [Default setValue:self->userId forKey:USER_ID];
                [Default setValue:self->authToken forKey:AUTH_TOKEN];
                [Default setValue:self->fullName forKey:FULL_NAME];
                [Default setValue:self->plivoAuthID forKey:CALLHIPPO_AUTH_ID];
                [Default setValue:self->plivoAuthToken forKey:CALLHIPPO_AUTH_TOKEN];
                [Default setValue:self->planDisplayName forKey:PLAN_DISPLAY_NAME];
                [Default setValue:self->parentId forKey:PARENT_ID];
                [Default setValue:Timer_Stop forKey:Timer_Call];
                [Default setValue:self->callTransfer forKey:CALL_TRANSFER];
                [Default setValue:@"Dialer" forKey:SelectedSideMenu];
                [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
                [Default setObject:[NSMutableArray arrayWithArray:[response1 valueForKey:@"data"][@"moduleRights"]] forKey:kMODUALRIGHTS];
                [Default setValue:[response1 valueForKey:@"data"][@"isDisableSms"] forKey:kSMSRIGHTS];
               // [Default setValue:[response1 valueForKey:@"data"][@"thinqValue"] forKey:kTHINQVALUE];
                [Default setValue:[response1 valueForKey:@"data"][@"thinqAccess"] forKey:kTHINQACCESS];
                [Default setValue:[response1 valueForKey:@"data"][@"providerHostname"] forKey:kPROVIDERHOSTNAME];
                [Default setValue:[response1 valueForKey:@"data"][@"iossipProtocol"] forKey:kIOSSIPPROTOCOL];
                [Default setValue:[NSString stringWithFormat:@"%@",[response1 valueForKey:@"data"][@"extensionNumber"]] forKey:ownExtensionNumber];
                [Default setValue:@"login" forKey:UserLoginStatus];
                [Default setValue:Provider forKey:Login_Provider];
                
                [Default setValue:[response1 valueForKey:@"data"][@"blacklist"] forKey:kIsBlackList];
                [Default setValue:[[[response1 valueForKey:@"data"] valueForKey:@"blockNumberList"] valueForKey:@"number"] forKey:kblackListedArray];
                
                [Default setValue:[response1 valueForKey:@"data"][@"recordControl"] forKey:kISRecordingInPlan];
                [Default setValue:[response1 valueForKey:@"data"][@"callRecordingPermission"] forKey:kcallRecordingPermission];
                [Default setValue:[response1 valueForKey:@"data"][@"disableRecordingAutomation"] forKey:kdisableRecordingAutomation];
                [Default setValue:[response1 valueForKey:@"data"][@"autoPauseRecordingAutomation"] forKey:kautoPauseRecordingAutomation];
               
                
                 NSArray *phoneArray = [[NSUserDefaults standardUserDefaults] objectForKey:kMODUALRIGHTS];
                  
                   
                   if (phoneArray.count > 0) {
                       for (NSDictionary *item in phoneArray) {
                           NSString *name = item[@"name"];
                           
                           if ([name isEqualToString:@"feedBacks"]) {
                                [Default setValue:item[@"active"] forKey:kISFeedbackRightsForSub];
                           }
                           
                       }
                   }
                
                NSLog(@"login::  blacklisted saved : %@", [Default valueForKey:kblackListedArray]);
                
                //pri
                [Default setValue:@"+1" forKey:klastDialCountryCode];
                [Default setValue:@"United States" forKey:klastDialCountryName];
                

                
                [Default setValue:[response1 valueForKey:@"data"][@"gamification"] forKey:kbadgeTag];

                [Default setValue:[[response1 valueForKey:@"data"]valueForKey:@"postCallSurvey"] forKey:kISPostCallSurvey];
                
                BOOL nummask = [[[response1 valueForKey:@"data"] valueForKey:@"numberMasking"] boolValue];
                [Default setBool:nummask forKey:kIsNumberMask];

                [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"] forKey:kTelephonyProviderSwitch];
                 
                NSLog(@"telephony in login >> %@",[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"]);


                
                [Default setValue:[response1 valueForKey:@"data"][@"gamification"] forKey:kbadgeTag];
                
                if([[Default valueForKey:kbadgeTag] isEqualToString:@"Beginner"])
                {
                    [Default setValue:@"badge_beginner" forKey:kbadgeImage];
                }
                else if([[Default valueForKey:kbadgeTag] isEqualToString:@"Sharpshooter"])
                {
                    [Default setValue:@"badge_silver" forKey:kbadgeImage];
                }
                else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Calling Master"])
                {
                     [Default setValue:@"badge_gold" forKey:kbadgeImage];
                }
                else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"The King"])
                {
                     [Default setValue:@"badge_platinum" forKey:kbadgeImage];
                }
                else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Ultimate Champion"])
                {
                     [Default setValue:@"badge_champion" forKey:kbadgeImage];
                }
                    
                [self activeSubUser:[response1 valueForKey:@"data"][@"activeSubUsers"]];
                [self outGoingCallCountryRest:[response1 valueForKey:@"data"][@"outCallCountries"]];
                [self purchaseNumber:response1[@"data"]];
                
                NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"];
                NSString *TwilioToken = @"";
                if([response1 valueForKey:@"data"][@"twilioToken"])
                {
                    TwilioToken = [response1 valueForKey:@"data"][@"twilioToken"] ? [response1 valueForKey:@"data"][@"twilioToken"] : @"";
                    [Default setValue:TwilioToken forKey:twilio_token];
                    NSLog(@"LoginVC : 1 :  Twilio token : %@",TwilioToken);
                }
                else
                {
                    [Default setValue:TwilioToken forKey:twilio_token];
                    NSLog(@"LoginVC : 2 :  Twilio token : %@",TwilioToken);
                }
                [Default synchronize];
                NSLog(@"\n \n VOIP TOKEN  %@ \n \n ",Voiptoken);
                if(![TwilioToken isEqualToString:@""])
                {
                    NSLog(@"device token twilio ::%@",[Default valueForKey:@"VOIPTOKEN_DATA"]);
                    
                    [TwilioVoiceSDK registerWithAccessToken:TwilioToken
                                             deviceToken:[Default valueForKey:@"VOIPTOKEN_DATA"]
                                              completion:^(NSError *error) {
                    }];
                }
//                [GlobalData  get_callhippo_number_selection];
                [GlobalData  get_all_callhippo_contacts];
                Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
                
                 [[Phone sharedInstance] setDelegate:appDelegate];
                
                if (endpointUserName!= nil && endpointPassWord != nil) {
                    
                    [[Phone sharedInstance] login:endpointUserName endPointPassword:endpointPassWord deviceToken:[Default valueForKey:@"deviceTokenForPlivo"]];
                }
                 
                
//                if(Provider != nil)
//                {
//                    if([Provider isEqualToString:Login_Plivo])
//                    {
//                        if(![[Default valueForKey:@"VOIPTOKEN"] isEqualToString:@""])
//                        {
//                        }
//                        else
//                        {
//                        }
//                        //[[Phone sharedInstance] setDelegate:appDelegate];
//                      //  [[Phone sharedInstance] login:self->endpointUserName endPointPassword:self->endpointPassWord];
//
//
//                    }
//
//                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [Processcall hideLoadingWithView];
                                    
                        self->_txtPassword.text = @"";
                        DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
                        [[self navigationController] pushViewController:vc animated:YES];
                        [appDelegate updatefcmtoken];
                });
                  
                        
                    
                
            } else {
                [Processcall hideLoadingWithView];
                [UtilsClass showAlert:@"Login Again" contro:self];
=======
                 head removed */
                //2 factor enable. show otp field
                [self showOTPField];
            }
            else
            {
                //2 factor is not there redirect to dialer
                [self setLoginResponse:response1];
                            
//>>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
            }
            
          
          
         
        }
        else
        {
            _txtPassword.text = @"";
           
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(void)keybord_setup
{
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:NO];
    [[UINavigationBar appearance] setTranslucent:false];
    UINavigationBar.appearance.translucent = false;
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:100.0];
}
-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
    if (error != nil) {
        if (error.code == kGIDSignInErrorCodeHasNoAuthInKeychain) {
        } else {
            [UtilsClass showAlert:@"Not able to login with google, please login with registered email address." contro:self];
        }
        return;
    }
    NSString *userId = user.userID;
    NSString *email = user.profile.email;
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";
    //NSLog(@"FCMTOKEN :%@",fcmtoken);
    NSDictionary *passDict = @{@"email":email,
                               @"googleUniqueId":userId,
                               @"userLoginType":@"gmail",
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"versionInfo":VERSIONINFO,
                               @"token":fcmtoken};
    //NSLog(@"Login URL : %@%@",SERVERNAME,Login_URL);
    //NSLog(@"Login Dic : %@",passDict);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:Login_URL andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}
- (void)settingApi {
    NSDictionary *passDict = @{@"device":@"ios",@"features":@""};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"setting/view" andParams:jsonString SuccessCallback:@selector(setting:response:) andDelegate:self];
}
- (void)setting:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
           /* if ([[[response1 valueForKey:@"feature"] valueForKey:@"loginGmail"] isEqualToString:@"ON"]){
                _btnGoogleSignIn.hidden = false;
                _lblOR.hidden = false;
                
            }else{
                _btnGoogleSignIn.hidden = true;
                _lblOR.hidden = true;
<<<<<<< HEAD
            }
            
         
            
=======
            }*/
            
            NSLog(@"setting response :: %@",response1);
            
        //Encryption commented    [Default setBool:[[[response1 valueForKey:@"feature"] valueForKey:@"encryptionInMobile"] boolValue] forKey:kencryptionInMobile];
            
         //   [Default setBool:true forKey:kencryptionInMobile];
        

// >>>>>>> 7399b3629a7898f704c59a0fdddb2b1391672a3c
        }
        
        
    }
}
- (IBAction)googleLogin:(id)sender {
}
- (IBAction)btnForgetPassword:(id)sender {
    ResetPasswordVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"ResetPasswordVC"];
    [[self navigationController] pushViewController:vc animated:YES];
}

-(void)purchaseNumber:(NSDictionary *)response1{
    
    
    NSDictionary*numberData = [response1 objectForKey:@"numberData"];
    NSArray *purchase_number = [numberData valueForKey:@"numbers"];
    
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
//    [GlobalData sharedGlobalData].Number_Selection = [[NSMutableArray alloc] init];
//    [[GlobalData sharedGlobalData].Number_Selection addObjectsFromArray:purchase_number];
    
    
}

-(void)activeSubUser:(NSArray *)response{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:ACTIVE_SUB_USER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)outGoingCallCountryRest:(NSArray *)response{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:OUTGOING_CALL_COUNTRY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(IBAction)btnEyePasswordClick:(id)sender
{
    if ( [_txtPassword isSecureTextEntry]) {
        [_txtPassword setSecureTextEntry:false];
        [_btnEyeForPassword setBackgroundImage:[UIImage imageNamed:@"icon_eye_slash"] forState:UIControlStateNormal];
    }
    else
    {
        [_txtPassword setSecureTextEntry:true];
        [_btnEyeForPassword setBackgroundImage:[UIImage imageNamed:@"icon_eye"] forState:UIControlStateNormal];
    }
   
}
-(IBAction)btnResendCodeClick:(id)sender
{
    [self ResendCodeAPICall];
}
#pragma mark - 2 Factor Auth

-(void)setLoginResponse:(NSDictionary *)response1
{

      self->endpointUserName = [response1 valueForKey:@"data"][@"username"];
                self->endpointPassWord = [response1 valueForKey:@"data"][@"password"];
                self->userId = [response1 valueForKey:@"data"][@"_id"];
                self->authToken = [response1 valueForKey:@"data"][@"authToken"];
                self->fullName = [response1 valueForKey:@"data"][@"fullName"];
                self->plivoAuthID = [response1 valueForKey:@"data"][@"plivoAuthId"];
                self->plivoAuthToken = [response1 valueForKey:@"data"][@"plivoAuthToken"];//
                self->planDisplayName = [response1 valueForKey:@"data"][@"planDisplayName"];
                self->parentId = [response1 valueForKey:@"data"][@"parentId"];
                self->callTransfer = [response1 valueForKey:@"data"][@"callTransfer"];
                endPoints = [response1 valueForKey:@"data"][@"endpointInfo"];
                NSString *email = [response1 valueForKey:@"data"][@"email"];
                NSString *Provider = [response1 valueForKey:@"data"][@"provider"];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
                if (self->endpointUserName != nil && self->endpointPassWord !=nil) {
                    
                   
                    [Default setValue:self->endpointUserName forKey:ENDPOINT_USERNAME];
                    [Default setValue:self->endpointPassWord forKey:ENDPOINT_PASSORD];
                    [Default setValue:email forKey:kUSEREMAIL];
                    [Default setValue:self->userId forKey:USER_ID];
                    [Default setValue:self->authToken forKey:AUTH_TOKEN];
                    [Default setValue:self->fullName forKey:FULL_NAME];
                    [Default setValue:self->plivoAuthID forKey:CALLHIPPO_AUTH_ID];
                    [Default setValue:self->plivoAuthToken forKey:CALLHIPPO_AUTH_TOKEN];
                    [Default setValue:self->planDisplayName forKey:PLAN_DISPLAY_NAME];
                    [Default setValue:self->parentId forKey:PARENT_ID];
                    [Default setValue:Timer_Stop forKey:Timer_Call];
                    [Default setValue:self->callTransfer forKey:CALL_TRANSFER];
                    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
                    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
                    [Default setObject:[NSMutableArray arrayWithArray:[response1 valueForKey:@"data"][@"moduleRights"]] forKey:kMODUALRIGHTS];
                    [Default setValue:[response1 valueForKey:@"data"][@"isDisableSms"] forKey:kSMSRIGHTS];
                    [Default setValue:[response1 valueForKey:@"data"][@"thinqValue"] forKey:kTHINQVALUE];
                    [Default setValue:[response1 valueForKey:@"data"][@"thinqAccess"] forKey:kTHINQACCESS];
                    [Default setValue:[response1 valueForKey:@"data"][@"providerHostname"] forKey:kPROVIDERHOSTNAME];
                    [Default setValue:[response1 valueForKey:@"data"][@"iossipProtocol"] forKey:kIOSSIPPROTOCOL];
                    [Default setValue:[NSString stringWithFormat:@"%@",[response1 valueForKey:@"data"][@"extensionNumber"]] forKey:ownExtensionNumber];
                    [Default setValue:@"login" forKey:UserLoginStatus];
                    [Default setValue:Provider forKey:Login_Provider];
                    
                    [Default setValue:[response1 valueForKey:@"data"][@"blacklist"] forKey:kIsBlackList];
                    [Default setValue:[[[response1 valueForKey:@"data"] valueForKey:@"blockNumberList"] valueForKey:@"number"] forKey:kblackListedArray];
                    
                    [Default setValue:[response1 valueForKey:@"data"][@"recordControl"] forKey:kISRecordingInPlan];
                    
                    if (![[Default valueForKey:kEmailIdForDB] isEqualToString:[Default valueForKey:kUSEREMAIL]]) {
                        
                        [Default setBool:true forKey:kNeedToIgnoreUpdatedContact];
                        
                        [UtilsClass makeToast:@"Fetching contacts this may take upto few minutes,Please don't close the app."];
                        [[GlobalData sharedGlobalData] flushAllContactsAndFetchNew];
                        
                    }
                    
                    [Default setValue:[Default valueForKey:kUSEREMAIL] forKey:kEmailIdForDB];
                    
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"twilioEdge"] forKey:kTwilioEdgeValue];
                    
                     NSArray *phoneArray = [[NSUserDefaults standardUserDefaults] objectForKey:kMODUALRIGHTS];
                      
                       
                       if (phoneArray.count > 0) {
                           for (NSDictionary *item in phoneArray) {
                               NSString *name = item[@"name"];
                               
                               if ([name isEqualToString:@"feedBacks"]) {
                                    [Default setValue:item[@"active"] forKey:kISFeedbackRightsForSub];
                               }
                               
                           }
                       }
                    
                    //NSLog(@"login::  blacklisted saved : %@", [Default valueForKey:kblackListedArray]);
                    
                    //pri
                    [Default setValue:@"+1" forKey:klastDialCountryCode];
                    [Default setValue:@"United States" forKey:klastDialCountryName];
                    

                    
                   
                    [Default setValue:[[response1 valueForKey:@"data"]valueForKey:@"postCallSurvey"] forKey:kISPostCallSurvey];
                    
                    BOOL nummask = [[[response1 valueForKey:@"data"] valueForKey:@"numberMasking"] boolValue];
                    [Default setBool:nummask forKey:kIsNumberMask];

                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"] forKey:kTelephonyProviderSwitch];
                     
                    NSLog(@"telephony in login >> %@",[[response1 valueForKey:@"data"] valueForKey:@"telephonyProviderSwitch"]);


                    
                    [Default setValue:[response1 valueForKey:@"data"][@"gamification"] forKey:kbadgeTag];
                    
                    if([[Default valueForKey:kbadgeTag] isEqualToString:@"Beginner"])
                    {
                        [Default setValue:@"badge_beginner" forKey:kbadgeImage];
                    }
                    else if([[Default valueForKey:kbadgeTag] isEqualToString:@"Sharpshooter"])
                    {
                        [Default setValue:@"badge_silver" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Calling Master"])
                    {
                         [Default setValue:@"badge_gold" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"The King"])
                    {
                         [Default setValue:@"badge_platinum" forKey:kbadgeImage];
                    }
                    else if ([[Default valueForKey:kbadgeTag] isEqualToString:@"Ultimate Champion"])
                    {
                         [Default setValue:@"badge_champion" forKey:kbadgeImage];
                    }
                    
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchSipUser"] forKey:kFreeswitchUsername];
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchSipPass"] forKey:kFreeswitchPassword];
                    [Default setValue:[[response1 valueForKey:@"data"] valueForKey:@"iosFreeSwitchDomain"] forKey:kFreeswitchDomain];

                    
                        
                    [self activeSubUser:[response1 valueForKey:@"data"][@"activeSubUsers"]];
                    [self outGoingCallCountryRest:[response1 valueForKey:@"data"][@"outCallCountries"]];
                    [self purchaseNumber:response1[@"data"]];
                    
                    NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"];
                    NSString *TwilioToken = @"";
                    if([response1 valueForKey:@"data"][@"twilioToken"])
                    {
                        TwilioToken = [response1 valueForKey:@"data"][@"twilioToken"] ? [response1 valueForKey:@"data"][@"twilioToken"] : @"";
                        [Default setValue:TwilioToken forKey:twilio_token];
                        NSLog(@"LoginVC : 1 :  Twilio token : %@",TwilioToken);
                    }
                    else
                    {
                        [Default setValue:TwilioToken forKey:twilio_token];
                        NSLog(@"LoginVC : 2 :  Twilio token : %@",TwilioToken);
                    }
                    [Default synchronize];
                    NSLog(@"\n \n VOIP TOKEN  %@ \n \n ",Voiptoken);
                    if(![TwilioToken isEqualToString:@""])
                    {
                        //set twilio edge
                        if ([Default valueForKey:kTwilioEdgeValue]) {
                        if (![[Default valueForKey:kTwilioEdgeValue] isEqualToString:@""]) {
                            
                           [TwilioVoiceSDK setEdge:[Default valueForKey:kTwilioEdgeValue]];
                            NSLog(@"edge setup:: %@",[Default valueForKey:kTwilioEdgeValue]);
                        }
                        }
                        
                        [TwilioVoiceSDK registerWithAccessToken:TwilioToken
                                                 deviceToken:[Default valueForKey:@"VOIPTOKEN_DATA"]
                                                  completion:^(NSError *error) {
                        }];
                    }
    //                [GlobalData  get_callhippo_number_selection];
          //old          [GlobalData  get_all_callhippo_contacts];
                  //  Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
                    
                    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
                    
                     [[Phone sharedInstance] setDelegate:appDelegate];
                    
                    if (endpointUserName!= nil && endpointPassWord != nil) {
                        
                        [[Phone sharedInstance] login:endpointUserName endPointPassword:endpointPassWord deviceToken:[Default valueForKey:@"deviceTokenForPlivo"]];
                    }
                    
                    if ([Default valueForKey:kFreeswitchUsername]) {
                        
    //                   [LinphoneManager.instance startLinphoneCore];
                        
            [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                        
                    
                    
                    }
                    
                     
                    
    //                if(Provider != nil)
    //                {
    //                    if([Provider isEqualToString:Login_Plivo])
    //                    {
    //                        if(![[Default valueForKey:@"VOIPTOKEN"] isEqualToString:@""])
    //                        {
    //                        }
    //                        else
    //                        {
    //                        }
    //                        //[[Phone sharedInstance] setDelegate:appDelegate];
    //                      //  [[Phone sharedInstance] login:self->endpointUserName endPointPassword:self->endpointPassWord];
    //
    //
    //                    }
    //
    //                }
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        [Processcall hideLoadingWithView];
                                        
                            self->_txtPassword.text = @"";
                            DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
                            [[self navigationController] pushViewController:vc animated:YES];
                            [appDelegate updatefcmtoken];
                    });
                      
                            
                        
                    
                } else {
                    [Processcall hideLoadingWithView];
                    [UtilsClass showAlert:@"Login Again" contro:self];
                }
}

-(void)showOTPField
{
    //[UtilsClass makeToast:@"Input the code we sent on your email address to access your account"];
    
    [UIView animateKeyframesWithDuration:0.2
                                   delay:0.0
                                 options:UIViewKeyframeAnimationOptionCalculationModeLinear
                              animations:^{
        self->_txtUserName.hidden = true;
        self->_txtPassword.hidden = true;
        self->_txtPassword.text = @"";
        self->_btnSignIn.hidden = true;
        
        self.lbl_facts.hidden = true;
        self.icon_facts.hidden = true;
        self.btnForgotPass.hidden = true;
        self.btnEyeForPassword.hidden = true;
        
        
        
                              }
                              completion:^(BOOL finished) {
                                
        self->_txt_otp.hidden = false;
        self->_btnVerify.hidden=false;
        
        self->_lbl_otpsent.hidden = false;
    }];
    
   
    
    
}

- (void)verifyAPICall {
  
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
 
    NSDictionary *passDict = @{@"email":self.txtUserName.text,
                               @"otp":self.txt_otp.text,
                               @"device":@"ios",
                               @"verifyTwoFactorAuthentication":@(true)

    };
    
        NSLog(@"Login URL : %@%@",SERVERNAME,Login_URL);
        NSLog(@"Login Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"2falogin/" andParams:jsonString SuccessCallback:@selector(verifyresponse:response:) andDelegate:self];
}

- (void)verifyresponse:(NSString *)apiAlias response:(NSData *)response{
    
        NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
        [Processcall hideLoadingWithView];
    
        if([apiAlias isEqualToString:Status_Code])
        {
                // [UtilsClass logoutUser:self];
                [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            NSLog(@"Login_response : %@",response1);
            if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
                [self setLoginResponse:response1];
                
            }
            else
            {
                _txt_otp.text = @"";
                
                _btnResendCode.hidden = false;
                
                @try {
                    if (response1 != (id)[NSNull null] && response1 != nil ){
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                    }
                }
                @catch (NSException *exception) {
                }
            }
        }
}
- (void)ResendCodeAPICall {
  
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
 
    NSDictionary *passDict = @{@"email":self.txtUserName.text
                            
    };
    
        NSLog(@"resend URL : %@%@",SERVERNAME,Login_URL);
        NSLog(@"resend Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }

    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:@"resend2facode" andParams:jsonString SuccessCallback:@selector(resendResponse:response:) andDelegate:self];
}

- (void)resendResponse:(NSString *)apiAlias response:(NSData *)response{
    
        NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
        [Processcall hideLoadingWithView];
    
        if([apiAlias isEqualToString:Status_Code])
        {
               
                [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            NSLog(@"resend response : %@",response1);
            if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
                _btnResendCode.hidden = true;
                
                [UtilsClass makeToast:[NSString stringWithFormat:@"OTP Verification Code has been resent to your email %@",self.txtUserName.text]];
                
            }
            else
            {
               
                @try {
                    if (response1 != (id)[NSNull null] && response1 != nil ){
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                    }
                }
                @catch (NSException *exception) {
                }
            }
        }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _txt_otp) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
            
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 4;
    }
    else
        return YES;
    
}
#pragma mark - Hippo facts
-(void)showHippoFacts
{
    
       NSString *sourceFileString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"HippoFacts" ofType:@"csv"] encoding:NSUTF8StringEncoding error:nil];


       NSMutableArray *csvArray = [[NSMutableArray alloc] init];
           
       csvArray = [[sourceFileString componentsSeparatedByString:@"\n"] mutableCopy];

       int rads = arc4random_uniform(100);
       
       NSString *factsStr = [csvArray objectAtIndex:rads];
       
    _lbl_facts.text = [factsStr  stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
//       NSLog(@"facts line : %@",[factsStr  stringByReplacingOccurrencesOfString:@"\"" withString:@""]);
}

#pragma mark - encryption
-(NSString *)EncryptParam:(NSDictionary *)originalDict
{

    NSString *key  = @"F8A2BB3F418FEBDD632535271FB50866";
      
//      CryptLib *lib = [[CryptLib alloc]init];
//      NSString *cipherusertxt = [lib encryptPlainTextRandomIVWithPlainText:email key:key];
//      NSString *cipherpasstxt = [lib encryptPlainTextRandomIVWithPlainText:pass key:key];

      
    
    return key;
    
    
}
-(IBAction)captchaselected:(id)sender
{
    _firstBoxSelected = !_firstBoxSelected; /* Toggle */
       [_btnCheckbox setSelected:_firstBoxSelected];
       if (_btnCheckbox.isSelected) {
           
      /*
pri-fabric
       ReCaptchaViewController *vc = [ReCaptchaViewController new];
              vc.delegate = self;
              NSString *locale = @"en"; // YOUR LOCALE
              NSString *siteKey = @"6LdKv4EUAAAAALjiiB8csUIsKWWO3eoaMIOoUYmo";
         //  NSString *siteKey = @"6Lf-nb4ZAAAAAMf9iKLHe_vAzmENT2XdbqkspsFE";
              vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
              [self presentViewController:vc animated:false completion:nil];
              [vc loadCaptchaWithSiteKey:siteKey Locale:locale];*/
           
       }else{
           
       }
    
   

}

#pragma mark - robot check delegate

- (void)captchaValidateSuccess:(NSString *)token {
    NSLog(@"Your token : %@", token);
    recaptchatoken = token;
    
    [self.btnCheckbox setSelected:true];

    
}

- (void)captchaDidExpire {
    NSLog(@"token expired");
    recaptchatoken = @"";
    [_btnCheckbox setSelected:false];
    _firstBoxSelected = false;
}

- (void)exitCaptchaView {
    
    
    [self dismissViewControllerAnimated:false completion:^{
        
        if ([self->recaptchatoken isEqualToString:@""]) {
            
            [_btnCheckbox setSelected:false];
            _firstBoxSelected = false;
        }else{
            [_btnCheckbox setSelected:true];
            _firstBoxSelected = true;
        }

    }];
}

@end
