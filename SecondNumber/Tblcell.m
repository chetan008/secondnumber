//
//  Tblcell.m
//  SecondNumber
//
//  Created by Apple on 24/11/21.
//

#import "Tblcell.h"

@implementation Tblcell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *color = self.lbl_send_view.backgroundColor;
    UIColor *color1 = self.lbl_rec_view.backgroundColor;
    [super setSelected:selected animated:animated];
    
    if (selected){
        self.lbl_send_view.backgroundColor = color;
        self.lbl_rec_view.backgroundColor = color1;
    }
}
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    UIColor *color = self.lbl_send_view.backgroundColor;
    UIColor *color1 = self.lbl_rec_view.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted){
        self.lbl_send_view.backgroundColor = color;
        self.lbl_rec_view.backgroundColor = color1;
    }
}


@end
