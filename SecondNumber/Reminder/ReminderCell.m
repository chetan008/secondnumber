//
//  ReminderCell.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ReminderCell.h"
#import "UtilsClass.h"
@implementation ReminderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [UtilsClass view_shadow_boder:_viewCellTop];
    _viewCellTop.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _viewCellTop.layer.borderWidth = 0.5;
    _viewCellTop.layer.cornerRadius = 5.0;
    
    _btnRemoveReminder.layer.cornerRadius = _btnRemoveReminder.frame.size.height / 2;
    _btnRemoveReminder.layer.borderWidth=1.0;
    _btnRemoveReminder.layer.masksToBounds = YES;
    _btnRemoveReminder.layer.borderColor=[[UIColor redColor] CGColor];
    _btnRemoveReminder.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
