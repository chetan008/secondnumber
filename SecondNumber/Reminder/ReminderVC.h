//
//  ReminderVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReminderVC : UIViewController
{
    NSMutableArray *Mixallcontact;
}

@property (strong, nonatomic) IBOutlet UILabel *lblErrorMessage;
@property (strong, nonatomic) IBOutlet UITableView *tblViewReminder;
@property (strong, nonatomic) IBOutlet UIButton *btnScheduledCall;
@property (strong, nonatomic) IBOutlet UIButton *btnCompletedOn;
@end

NS_ASSUME_NONNULL_END
