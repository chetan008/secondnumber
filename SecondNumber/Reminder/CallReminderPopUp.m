//
//  CallReminderPopUp.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "CallReminderPopUp.h"
#import "UtilsClass.h"
#import "Tblcell.h"
#import "Processcall.h"
#import "WebApiController.h"

@interface CallReminderPopUp ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *timeList;
    BOOL select;
    WebApiController *obj;
}
@end

@implementation CallReminderPopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        CGFloat topPadding = window.safeAreaInsets.top;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        //NSLog(@"bottomPadding : %f", bottomPadding);
        if (bottomPadding < 1.0)
        {
            _top_constraint.constant = 28.0;
        }
        else
        {
            _top_constraint.constant = 28.0+topPadding-20.0;
        }
        
    }
    else
    {
        _top_constraint.constant = 28.0;
    }
    
    timeList =  [NSMutableArray arrayWithObjects:@"15 Minutes",@"30 Minutes",@"1 Hour",@"2 Hours",@"tomorrow",@"in a week", nil];
    select = YES;
    [UtilsClass view_shadow_boder:_viewSetReminder];
    _viewSetReminder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _viewSetReminder.layer.borderWidth = 0.5;
    _viewSetReminder.layer.cornerRadius = 5.0;
    
//    [UtilsClass image_round_boder:_imgLogoReminder];
    _imgLogoReminder.layer.cornerRadius = _imgLogoReminder.frame.size.height / 2;
    _imgLogoReminder.clipsToBounds = YES;
    
    [UtilsClass view_shadow_boder:_btnCancel];
    _btnCancel.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnCancel.layer.borderWidth = 0.5;
    _btnCancel.layer.cornerRadius = 5.0;
    
    _tblReminderTimeList.delegate = self;
    _tblReminderTimeList.dataSource = self;
    
    
    _lblDisplayNumber.text = _strDisplayName;

}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (IBAction)btnCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return timeList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Tblcell *cell = [_tblReminderTimeList dequeueReusableCellWithIdentifier:@"Tblcell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblCredit.text = timeList[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _lblSelectedReminder.text = timeList[indexPath.row];
    _tblReminderTimeList.hidden = true;
    select = YES;
    //NSLog(@"-----------count-----------%ld",(long)indexPath.row);
    
    //15,30,1,2,24( 1 day),7(1 week)


    
    if(indexPath.row == 0){
        [self setReminder:@"15"];
    }else if(indexPath.row == 1){
        [self setReminder:@"30"];
    }else if(indexPath.row == 2){
        [self setReminder:@"1"];
    }else if(indexPath.row == 3){
        [self setReminder:@"2"];
    }else if(indexPath.row == 4){
        [self setReminder:@"24"];
    }else if(indexPath.row == 5){
        [self setReminder:@"7"];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)btnDropDown:(id)sender {
    if (select == NO) {
        select = YES;
        _tblReminderTimeList.hidden = true;
    } else {
        select = NO;
        _tblReminderTimeList.hidden = false;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    select = YES;
//    _tblReminderTimeList.hidden = true;
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(void)setReminder:(NSString *)reminderType {
    
    NSString *parentId = [Default valueForKey:PARENT_ID];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"createCallReminder";
    /*
     "":"",
     "reminderNumber":"13093265247",
     "parentId":"5b977a8df19c7c1724650608",
     "createdById":"5b977a8df19c7c1724650608",
     "reminderType":"1",
     "deviceType":"ios"
     
     */
    //working without masking
//    NSDictionary *passDict = @{@"networkStrengthRandomString":@"",
//                               @"reminderNumber":_incOutNumb,
//                               @"parentId":parentId,
//                               @"createdById":userId,
//                               @"reminderType":reminderType,
//                               @"deviceType":@"ios"
//                               };
    
    NSDictionary *passDict = @{@"networkStrengthRandomString":@"",
    @"reminderNumber":_originalToNum,
    @"parentId":parentId,
    @"createdById":userId,
    @"reminderType":reminderType,
    @"deviceType":@"ios"
    };
    NSLog(@"Dictonary ****** : %@",passDict);

    
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login1:response:) andDelegate:self];
}

- (void)login1:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"REMINDER SET RESPONSE: %@",response1);
    [Processcall hideLoadingWithView];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1)
    {
        [UtilsClass makeToast:KRIMINDERSET];
        
        //notification to load call logs
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"newReminderSet_notification" object:self userInfo:nil];
        
    }
    
   // NSLog(@"TRUSHANG : STATUSCODE **************17  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
    
}


@end
