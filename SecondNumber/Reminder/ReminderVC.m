//
//  ReminderVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ReminderVC.h"
#import "ReminderCell.h"
#import "UtilsClass.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UIViewController+LGSideMenuController.h"
#import "GlobalData.h"
#import "MainViewController.h"
#import <AVFoundation/AVAudioSession.h>
@interface ReminderVC ()<UITableViewDelegate,UITableViewDataSource>
{
    WebApiController *obj;
    
    NSDictionary *arrReminders;
    
    NSMutableArray *allPendingRemidersData;
    NSMutableArray *allCompletedRemidersData;
    
    NSMutableArray *allRemiders;
    NSMutableArray *allCompleted;
    
//    NSString *currentView;
     NSString *reminderType;
    bool is_tbl_reload;
}
@end

@implementation ReminderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    Mixallcontact = [[NSMutableArray alloc] init];
    [self contact_get];
    
    //currentView = @"callremindersPendding";
    
    [Default setValue:@"Call Planner" forKey:SelectedSideMenu];
    [Default setValue:@"menu_reminder_active" forKey:SelectedSideMenuImage];
    
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;
    
    //    [self GetReminders:@"0" type:@"pending"];
    //    [self GetReminders:@"0" type:@"completed"];
    [UtilsClass view_navigation_title:self title:@"Call Planner"color:UIColor.whiteColor];
    
    [_btnCompletedOn setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btnScheduledCall setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    //  = [[UIColor colorWithRed:208 green:208 blue:208 alpha:1] CGColor];
    
    [[NSNotificationCenter defaultCenter]
    addObserver:self selector:@selector(newReminderSet_notification:) name:@"newReminderSet_notification" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    is_tbl_reload = true;
    
    allCompleted = [[NSMutableArray alloc] init];
    allRemiders = [[NSMutableArray alloc] init];
    
    allCompletedRemidersData = [[NSMutableArray alloc] init];
    allPendingRemidersData = [[NSMutableArray alloc] init];
    
    [self GetReminders:@"0" type:@"pending"];
     
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    float contentYoffset = aScrollView.contentOffset.y;
    //    //NSLog(@" distanceFromBottom : %f",distanceFromBottom);
    //    //NSLog(@" height : %f",height);
    float halfpoint = aScrollView.contentSize.height / 2;
    if (halfpoint < contentYoffset ) {
        //NSLog(@" you reached end of the table");
        [self scroll_api];
    }else{
        
    }
}

-(void)scroll_api
{
    if ([reminderType isEqualToString:@"pending"]){
        if(is_tbl_reload == true)
        {
            
            is_tbl_reload = false;
            [self GetReminders:[NSString stringWithFormat:@"%lu",(unsigned long)[allRemiders count]] type:@"pending"];
        }
    } else {
        if(is_tbl_reload == true)
        {
            
            is_tbl_reload = false;
            [self GetReminders:[NSString stringWithFormat:@"%lu",(unsigned long)[allCompleted count]] type:@"completed"];
        }
    }
    
}

-(void)Foreground_Action:(NSNotification *)notification
{
    //NSLog(@"\n \n \n \n Trushang_code : Foreground_Action NEW_SMS_VC:");
    [self contact_get];
    
}
-(void)contact_get
{
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([reminderType isEqualToString:@"pending"]){
        return allRemiders.count;
    }else{
        return allCompleted.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ReminderCell *cell = [_tblViewReminder dequeueReusableCellWithIdentifier:@"ReminderCell"];
    
    if ([reminderType isEqualToString:@"pending"]){
        
        cell.btnRemoveReminder.hidden = false;
        cell.btnCallLeading.constant = 56;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = allRemiders[indexPath.row];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        [dateFormatter1 setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        NSDate *expectedTime = [dateFormatter1 dateFromString:dict[@"expectedTime"]];
//        NSDate *updatedDate = [dateFormatter1 dateFromString:dict[@"updatedDate"]];
        
        dateFormatter1 = [[NSDateFormatter alloc] init] ;
        [dateFormatter1 setDateFormat:@"MMM d, yyyy h:mm a"];// here set format which you want...
        
        NSString *convertedExpectedTime = [dateFormatter1 stringFromDate:expectedTime]; //here convert date in NSString
//        NSString *convertedUpdatedDate = [dateFormatter1 stringFromDate:updatedDate];
        
   
        
        
        @try {
            if([dict[@"remindToContactId"] isKindOfClass:[NSDictionary class]])
            {
                //NSLog(@"trush don ");
                

                if (dict[@"remindToContactId"][@"name"] == (id)[NSNull null] || dict[@"remindToContactId"][@"name"] == nil || dict[@"remindToContactId"][@"name"] == [NSNull null] || [dict[@"remindToContactId"][@"name"] isEqualToString:@""]){
                    
                    cell.lblName.text = dict[@"remindToPhoneNumber"];
                    
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        cell.lblName.text = [UtilsClass get_masked_number:dict[@"remindToPhoneNumber"]] ;
                    }
                    cell.bottomConstrintOflblNumber.constant = -20;
                    cell.lblNumber.hidden = true;
                }else{
                    cell.lblName.text = dict[@"remindToContactId"][@"name"];
                    cell.lblNumber.text = dict[@"remindToPhoneNumber"];
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        cell.lblNumber.text = [UtilsClass get_masked_number:dict[@"remindToPhoneNumber"]] ;
                    }
                    cell.bottomConstrintOflblNumber.constant = 18;
                    cell.lblNumber.hidden = false;
                }
            }
            else
            {
                //NSLog(@"trush big boss ");
                if(dict[@"remindToPhoneNumber"])
                {
                    
                    cell.lblName.text = dict[@"remindToPhoneNumber"];
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        cell.lblName.text = [UtilsClass get_masked_number:dict[@"remindToPhoneNumber"]] ;
                    }
                    cell.bottomConstrintOflblNumber.constant = -20;
                    cell.lblNumber.hidden = true;
                }
                else
                {
                    cell.lblName.text = @"";
                    cell.bottomConstrintOflblNumber.constant = -20;
                    cell.lblNumber.hidden = true;
                    cell.lblName.hidden = true;
                }
                
            }
            
            
        }
        @catch (NSException *exception) {
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblDate.text = convertedExpectedTime;
        cell.btnCall.userInteractionEnabled = true;
        cell.btnCall.tag = indexPath.row;
        [cell.btnCall addTarget:self action:@selector(btndidselect:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnCall setImage:[UIImage imageNamed:@"rminderCallImg"] forState:UIControlStateNormal];
        cell.btnRemoveReminder.userInteractionEnabled = true;
        cell.btnRemoveReminder.tag = indexPath.row;
        [cell.btnRemoveReminder addTarget:self action:@selector(btnDelete:) forControlEvents:UIControlEventTouchUpInside];
        [cell.lblDate setTextColor:[UIColor colorWithRed:213/255.0 green:122/255.0 blue:85/255.0 alpha:1.0]];
        
        NSDate *today = [[NSDate alloc] init];
        NSComparisonResult result;
        result = [today compare:expectedTime]; // comparing two dates
        if(result==NSOrderedAscending) {
            //NSLog(@"today is less");
            [cell.lblDate setTextColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];
        } else {
            //NSLog(@"Both dates are same");
            [cell.lblDate setTextColor:[UIColor colorWithRed:213/255.0 green:122/255.0 blue:85/255.0 alpha:1.0]];
        }
        
        
    } else{
        cell.btnRemoveReminder.hidden = true;
        cell.btnCallLeading.constant = 8;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict = allCompleted[indexPath.row];
        
        
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        [dateFormatter1 setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        NSDate *expectedTime = [dateFormatter1 dateFromString:dict[@"expectedTime"]];
        NSDate *updatedDate = [dateFormatter1 dateFromString:dict[@"updatedDate"]];
        
        dateFormatter1 = [[NSDateFormatter alloc] init] ;
        [dateFormatter1 setDateFormat:@"MMM d, yyyy h:mm a"];// here set format which you want...
        
        NSString *convertedExpectedTime = [dateFormatter1 stringFromDate:expectedTime]; //here convert date in NSString
        NSString *convertedUpdatedDate = [dateFormatter1 stringFromDate:updatedDate];
        
        
        if([dict[@"remindToContactId"] isKindOfClass:[NSDictionary class]]){
            if (dict[@"remindToContactId"][@"name"] == (id)[NSNull null] || dict[@"remindToContactId"][@"name"] == nil || dict[@"remindToContactId"][@"name"] == [NSNull null] ||
                [[[dict valueForKey:@"remindToContactId"] valueForKey:@"name"] isEqualToString:@""]){
                cell.lblName.text = dict[@"remindToPhoneNumber"];
                
                if ([Default boolForKey:kIsNumberMask] == true) {
                    cell.lblName.text = [UtilsClass get_masked_number:dict[@"remindToPhoneNumber"]] ;
                }
                
                cell.bottomConstrintOflblNumber.constant = -20;
                cell.lblNumber.hidden = true;
            }else{
                cell.lblName.text = dict[@"remindToContactId"][@"name"];
                cell.lblNumber.text = dict[@"remindToPhoneNumber"];
                
                if ([Default boolForKey:kIsNumberMask] == true) {
                    cell.lblNumber.text = [UtilsClass get_masked_number:dict[@"remindToPhoneNumber"]] ;
                }
                
                cell.bottomConstrintOflblNumber.constant = 18;
                cell.lblNumber.hidden = false;
            }
            
        }else{
            cell.lblName.text = dict[@"remindToPhoneNumber"];
            if ([Default boolForKey:kIsNumberMask] == true) {
                cell.lblName.text = [UtilsClass get_masked_number:dict[@"remindToPhoneNumber"]] ;
            }
            
            cell.bottomConstrintOflblNumber.constant = -20;
            cell.lblNumber.hidden = true;
        }
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblDate.text = convertedUpdatedDate;
        cell.btnCall.userInteractionEnabled = false;
        [cell.btnCall setImage:[UIImage imageNamed:@"reminderCompleted"] forState:UIControlStateNormal];
        [cell.lblDate setTextColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (IBAction)btnScheduledClick:(id)sender {
    
   
    
    reminderType = @"pending";
    
    [_btnCompletedOn setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btnScheduledCall setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_tblViewReminder reloadData];
    
    if (allRemiders.count==0) {
        _lblErrorMessage.hidden=false;
    }
    else
    {
        _lblErrorMessage.hidden=true;

    }
    
    
}
- (IBAction)btnCompletedOnClick:(id)sender {
    
    
    reminderType = @"completed";
    if (allCompleted.count == 0) {
        [self GetReminders:@"0" type:@"completed"];
    }
    
    [_btnScheduledCall setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btnCompletedOn setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
   [_tblViewReminder reloadData];
    
    if (allCompleted.count==0) {
        _lblErrorMessage.hidden=false;
    }
    else
    {
        _lblErrorMessage.hidden=true;

    }
}


-(void)GetReminders:(NSString*)skip type:(NSString*)type{
   
    
    if ([skip intValue] == 0)
    {
       
        if ([type isEqualToString:@"pending"]) {
            
             [allRemiders removeAllObjects];
             [allPendingRemidersData removeAllObjects];
              allRemiders = [[NSMutableArray alloc] init];
             allPendingRemidersData = [[NSMutableArray alloc] init];
        }
        else
        {
            [allCompleted removeAllObjects];
            [allCompletedRemidersData removeAllObjects];
            allCompleted = [[NSMutableArray alloc] init];
            allCompletedRemidersData = [[NSMutableArray alloc] init];
        }
        
       
    }
    
    reminderType = type;
    
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"getcallreminders/%@?skip=%@&limit=20&remindertype=%@",userId,skip,type];
    NSLog(@"reminder url :: %@",url);
    obj = [[WebApiController alloc] init];
    
    //  [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(login:response:) andDelegate:self];
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(login1:response:) andDelegate:self];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (void)login1:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
  
    
    //NSLog(@"TRUSHANG : STATUSCODE **************16  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        //NSLog(@"---------response------------%@",response1);
        
        NSLog(@"Encrypted Response : get reminder : %@",response1);

        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            arrReminders =  response1[@"CallReminders"];
            
            
           // if ([currentView isEqualToString:@"callremindersPendding"]){
             if ([reminderType isEqualToString:@"pending"]){
                
                allPendingRemidersData = [NSMutableArray arrayWithArray:arrReminders[@"callremindersPendding"]];
                if ([allPendingRemidersData count] > 0)
                {
                    _lblErrorMessage.hidden = true;
                    
                    int tot = -1;
                    for (int i = 0; i<allPendingRemidersData.count; i++)
                    {
                        tot = i+1;
                        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:allPendingRemidersData[i]];
                        
                        [allRemiders addObject:allPendingRemidersData[i]];
                        [allPendingRemidersData replaceObjectAtIndex:i withObject:dict];
                    }
                    if(tot == allPendingRemidersData.count)
                    {
                        self.tblViewReminder.delegate = self;
                        self.tblViewReminder.dataSource = self;
                        [self.tblViewReminder reloadData];
                        is_tbl_reload = true;
                    }else{
                        //NSLog(@"tableview data null");
                    }
                    
                }else{
                    if(allRemiders.count < 1){
                        _lblErrorMessage.hidden = false;
                        self.tblViewReminder.delegate = self;
                        self.tblViewReminder.dataSource = self;
                        [self.tblViewReminder reloadData];
                         is_tbl_reload = true;
                    }
                    

//                    self.tblViewReminder.delegate = self;
//                                           self.tblViewReminder.dataSource = self;
//                                           [self.tblViewReminder reloadData];
                }
                //            self.tblViewReminder.delegate = self;
                //            self.tblViewReminder.dataSource = self;
                //            [self.tblViewReminder reloadData];
            }else {
                allCompletedRemidersData = [NSMutableArray arrayWithArray:arrReminders[@"callremindersCompleted"]];
                if ([allCompletedRemidersData count] > 0)
                {
                    _lblErrorMessage.hidden = true;
                    int tot = -1;
                    for (int i = 0; i<allCompletedRemidersData.count; i++)
                    {
                        tot = i+1;
                        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:allCompletedRemidersData[i]];
                        
                        [allCompleted addObject:allCompletedRemidersData[i]];
                        [allCompletedRemidersData replaceObjectAtIndex:i withObject:dict];
                    }
                    if(tot == allCompletedRemidersData.count)
                    {
                        self.tblViewReminder.delegate = self;
                        self.tblViewReminder.dataSource = self;
                        [self.tblViewReminder reloadData];
                        is_tbl_reload = true;
                    }else{
                        //NSLog(@"tableview data null");
                    }
                    
                }else{
                    if(allCompleted.count < 1){
                        _lblErrorMessage.hidden = false;
                        self.tblViewReminder.delegate = self;
                        self.tblViewReminder.dataSource = self;
                        [self.tblViewReminder reloadData];
                         is_tbl_reload = true;
                    }
                    
                }
            }
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}

- (IBAction)btn_menu_click:(id)sender {
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}


-(void)btndidselect:(UIButton *)sender {
    
    int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
    NSMutableArray *arr = allRemiders;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic = arr[row];
    //NSLog(@"callDic : %@",dic);
    
    
    NSString *callfromnumber = @"";
    NSString *callToName = @"";
    NSString *callTonumber = @"";
    NSString *callDepartment = @"";
    
    [[NSUserDefaults standardUserDefaults] setValue:[dic valueForKey:@"_id"] forKey:XPH_EXTRA_HEADER_REMINDER];
    
    if (dic[@"remindToContactId"][@"name"] == (id)[NSNull null] || [[[dic valueForKey:@"remindToContactId"] valueForKey:@"name"] isEqualToString:@""]){
        callToName = dic[@"remindToPhoneNumber"];
    }else{
        callToName = dic[@"remindToContactId"][@"name"];
    }
    
    
    
    callfromnumber = dic[@"departmentNumber"] ? dic[@"departmentNumber"] : [Default valueForKey:SELECTEDNO];
    callDepartment = dic[@"departmentName"] ? dic[@"departmentName"] : [Default valueForKey:Selected_Department];
    [Default setValue:callDepartment forKey:Selected_Department];
    [Default setValue:callfromnumber forKey:SELECTEDNO];
    NSString *remindToPhoneNumber = [[dic valueForKey:@"remindToPhoneNumber"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
    callTonumber = [NSString stringWithFormat:@"+%@",remindToPhoneNumber];// "\(userData?.to! ?? "")"
    
    
    
    if([UtilsClass isNetworkAvailable])
    {
       //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
        
        NSData *data = [Default valueForKey:PURCHASE_NUMBER];
        NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if(purchase_number.count > 0)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
            NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
            
            NSLog(@"results results : %lu",(unsigned long)results.count);
            NSString *numbVer = @"";
            if (results.count == 0){
                numbVer = @"1";
            }else{
                NSDictionary *dic1 = [results objectAtIndex:0];
                numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
            }
            int num = [numbVer intValue];
            if (num == 1) {
                NSString *credit = [Default valueForKey:CREDIT];
                int cre = [credit intValue];
                if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                    switch ([[AVAudioSession sharedInstance] recordPermission]) {
                        case AVAudioSessionRecordPermissionGranted:
                        {
                             if (results.count>0) {
                            
                                NSString *imagename = [[[[results objectAtIndex:0] objectForKey:@"number"]  objectForKey:@"shortName"] lowercaseString];
                                
                                [Default setValue:imagename forKey:Selected_Department_Flag];
                             }
                            
                            [UtilsClass make_outgoing_call_validate1:self callfromnumber:callfromnumber ToName:callToName ToNumber:callTonumber calltype:callDepartment Mixallcontact:Mixallcontact];

                            break;
                        }
                        case AVAudioSessionRecordPermissionDenied:
                        {
                            //                [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
                            [self micPermiiton];
                            break;
                        }
                        case AVAudioSessionRecordPermissionUndetermined:
                            // This is the initial state before a user has made any choice
                            // You can use this spot to request permission here if you want
                            break;
                        default:
                            break;
                            
                    }}else {
                        [UtilsClass showAlert:kCREDITLAW contro:self];
                    }}else{
                        [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                    }}else{
                        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                    }
    }
    else
    {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                //NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)btnDelete:(UIButton *)sender {
     UIAlertController * alert = [UIAlertController
                       alertControllerWithTitle:kAlertTitle
                       message:@"Are You Sure Want to Delete Reminder!"
                       preferredStyle:UIAlertControllerStyleAlert];
         
         
        UIAlertAction* yesButton = [UIAlertAction
                      actionWithTitle:@"Yes"
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction * action) {
                        int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
            NSMutableArray *arr = self->allRemiders;
                        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                        dic = arr[row];
                        NSString *reminderId = [dic valueForKey:@"_id"];
                        
                        NSLog(@"reminderId : %@",reminderId);
                       [self deleteReminders:reminderId];
                      }];
         
        UIAlertAction* noButton = [UIAlertAction
                      actionWithTitle:@"Cancel"
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction * action) {
                            
              }];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    
    
}





-(void)deleteReminders:(NSString*)reminderId{
    //NSLog(@"skit  :%@",skip);
    
    NSString *url = [NSString stringWithFormat:@"deleteCallReminder?deleteReminderIds=%@",reminderId];
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_DELETE_RAW:url andParams:nil SuccessCallback:@selector(deleteRem:response:) andDelegate:self];
}

- (void)deleteRem:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
  //  NSLog(@"---------response------------%@",response1);
    
    NSLog(@"Encrypted Response : delete reminder : %@",response1);

    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        [UtilsClass makeToast:kREMINDERDELETEMSG];
        
        [self GetReminders:@"0" type:@"pending"];

    }
    else {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
                [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
        }
        }
        @catch (NSException *exception) {
        }
    }
}
-(void)newReminderSet_notification:(NSNotification*)notification
{
     if ([[notification name] isEqualToString:@"newReminderSet_notification"]) {
         
         [self GetReminders:@"0" type:@"pending"];
     }
    
}


@end
