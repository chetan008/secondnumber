//
//  ReminderCell.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReminderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIView *viewCellTop;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstrintOflblNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnRemoveReminder;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnCallLeading;


@end

NS_ASSUME_NONNULL_END
