//
//  CallReminderPopUp.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallReminderPopUp : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imgLogoReminder;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *viewSetReminder;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectedReminder;
@property (weak, nonatomic) IBOutlet UITableView *tblReminderTimeList;
@property (strong, nonatomic) NSString *incOutNumb;
@property (strong, nonatomic) NSString *originalToNum;

@property (strong, nonatomic) NSString *strDisplayName;
@property (strong, nonatomic) IBOutlet UILabel *lblDisplayNumber;
@property (strong, nonatomic) NSString *strIncOutCall;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_constraint;
@end

NS_ASSUME_NONNULL_END
