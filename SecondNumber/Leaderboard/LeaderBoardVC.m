//
//  LeaderBoardVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "LeaderBoardVC.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "Tblcell.h"
#import "UtilsClass.h"
#import "Processcall.h"
#import "WebApiController.h"
#import "FilterVC.h"
#import "AppDelegate.h"
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface LeaderBoardVC ()<FilterVCDelegate>
{
    NSMutableArray *leadersArray;
    WebApiController *obj;
    
    NSString *filter_string;

}
@end

@implementation LeaderBoardVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [UtilsClass view_navigation_title:self title:@"Leader Board" color:UIColor.whiteColor];
    
    filter_string = @"today";
    
  
    [self getLeaders:filter_string];
  
    
}

- (IBAction)btn_menu_click:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return leadersArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
        Tblcell *cell = [_tbl_leader dequeueReusableCellWithIdentifier:@"leaderCell"];
        
    cell.lbl_leadername.text = [[leadersArray objectAtIndex:indexPath.row] valueForKey:@"fullName"];
    
    cell.lbl_leaderNoOfCalls.text = [NSString stringWithFormat:@"%@ Calls",[[leadersArray objectAtIndex:indexPath.row] valueForKey:@"completeCall"]];
    
    if ([[[leadersArray objectAtIndex:indexPath.row] valueForKey:@"gamification"] isEqualToString:@"Beginner"]) {
        
        cell.img_leaderbadge.image = [UIImage imageNamed:@"badge_beginner"];

    }
    else if ([[[leadersArray objectAtIndex:indexPath.row] valueForKey:@"gamification"] isEqualToString:@"Sharpshooter"]) {
        
        cell.img_leaderbadge.image = [UIImage imageNamed:@"badge_silver"];

    }
    else if ([[[leadersArray objectAtIndex:indexPath.row] valueForKey:@"gamification"] isEqualToString:@"Calling Master"]) {
        
        cell.img_leaderbadge.image = [UIImage imageNamed:@"badge_gold"];

    }
    else if ([[[leadersArray objectAtIndex:indexPath.row] valueForKey:@"gamification"] isEqualToString:@"The King"]) {
        
        cell.img_leaderbadge.image = [UIImage imageNamed:@"badge_platinum"];

    }
    else if ([[[leadersArray objectAtIndex:indexPath.row] valueForKey:@"gamification"] isEqualToString:@"Ultimate Champion"]) {
        
        cell.img_leaderbadge.image = [UIImage imageNamed:@"badge_champion"];

    }
    
        return cell;
    
}

-(void)getLeaders:(NSString *)serachKey
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    
       NSString *url = [NSString stringWithFormat:@"leaderBoardList"];
       NSDictionary *passDict = @{@"search":serachKey};
       //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
       
       NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                          options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                            error:nil];
       NSString *jsonString;
       if (! jsonData) {
           
       } else {
           jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
       }
       obj = [[WebApiController alloc] init];
       
       //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
       if([UtilsClass isNetworkAvailable]) {
           [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getLeaders:response:) andDelegate:self];
       }else {
           [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
       }
}

- (void)getLeaders:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {

        NSLog(@"leaderboard response :: %@",response1);
        
        leadersArray = [[NSMutableArray alloc]init];
        leadersArray = [[response1 valueForKey:@"data"] valueForKey:@"leaderBoardList"];
        
        [_tbl_leader reloadData];
        
    }
    else {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
[UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
        }
        }
        @catch (NSException *exception) {
        }
    }
        
    }
}
- (IBAction)btn_filter_click:(UIBarButtonItem *)sender
{
    if (@available(iOS 13, *))
    {
                   AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                   UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                   alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
                   //      alertWindow.rootViewController = [[UIViewController alloc] init];
                   alertWindow.windowLevel = UIWindowLevelAlert + 1;
                   [alertWindow makeKeyAndVisible];
                   
                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                   FilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
                    vc.currentView = @"Leaderboard";
                    vc.selected_value = filter_string;
                   vc.delegate = self;
                   if (IS_IPAD) {
                       
                       vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                       //[self presentViewController:vc animated:YES completion:nil];
                   } else {
                       [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                       vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                       //[self presentViewController:vc animated:YES completion:nil];
                   }
                   [alertWindow.rootViewController presentViewController:vc animated:NO completion:nil];
                   
    }
    else
        {
                   UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                   alertWindow.rootViewController = [[UIViewController alloc] init];
                   alertWindow.windowLevel = UIWindowLevelAlert + 1;
                   [alertWindow makeKeyAndVisible];
                   
                   
                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                   FilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
                    vc.currentView = @"Leaderboard";
                    vc.selected_value = filter_string;
                    vc.delegate = self;
                   if (IS_IPAD) {
                       vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                       vc.modalPresentationCapturesStatusBarAppearance = YES;
                   } else {
                       [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                       vc.modalPresentationStyle = UIModalPresentationFullScreen;
                       vc.modalPresentationCapturesStatusBarAppearance = YES;
                   }
                   
                   [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
        }

}
- (void)FilterVCDidDismisWithData:(NSString *)data
{
    
    if(![data isEqualToString:@"NoData"])
    {
        filter_string = data;
        
        NSString *passStr = [[filter_string lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSLog(@"filter string:: %@",passStr);
        
        [self getLeaders:passStr];
    }
    
    
//    if ([data isEqualToString:@"Today"])
//    {
//
//    }
//    else if ([data isEqualToString:@"Yesterday"])
//    {
//
//    }
//    else if ([data isEqualToString:@"Last Week"])
//    {
//
//    }
//    else if ([data isEqualToString:@"Last Month"])
//    {
//
//    }
}
@end
