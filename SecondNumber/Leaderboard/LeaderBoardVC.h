//
//  LeaderBoardVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeaderBoardVC : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tbl_leader;

@end

NS_ASSUME_NONNULL_END
