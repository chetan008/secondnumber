//
//  LeftMenuViewController.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "LeftMenuViewController.h"
#import "WebApiController.h"
#import "Tblcell.h"
#import "UtilsClass.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"

#import "DialerVC.h"
#import "CalllogsVC.h"
#import "FeedbackVC.h"
#import "AddCreditVC.h"
#import "ContactVC.h"
#import "LoginVC.h"
#import "SmsListNew.h"
#include <stdio.h>
#include <stdlib.h>
#import "ModelRights.h"
#import "Processcall.h"
#import "LeaderBoardVC.h"

#import "AppDelegate.h"

#import "Phone.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
#import "CallKitInstance.h"
#import "ReminderVC.h"
#import "SettingVC.h"

#import "ACWViewController.h"
#import "UserProfileVC.h"
#import "ChangePasswordVC.h"
#import "NumbersListVC.h"
#import "SettingsVC.h"

@import GoogleSignIn;
@import TwilioVoice;
@import Firebase;
@implementation LeftMenuViewController
{
    WebApiController *obj;
    NSMutableArray *menuArr;
    NSMutableArray *imgArr;
    NSMutableArray<ModelRights *> *modelRights;
    
}

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self.slideOutAnimationEnabled = YES;
    
    _lblUserName.text = [Default valueForKey:FULL_NAME];
    //p_lblUserEmailID.text =[Default valueForKey:kUSEREMAIL];
    _lbl_badgetag.text = [Default valueForKey:kbadgeTag];
   _img_badge.image = [UIImage imageNamed:[Default valueForKey:kbadgeImage]];
    

    
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lblUserName.text = [Default valueForKey:FULL_NAME];
    //_lblUserEmailID.text =[Default valueForKey:kUSEREMAIL];
    _lbl_badgetag.text = [Default valueForKey:kbadgeTag];
    _img_badge.image = [UIImage imageNamed:[Default valueForKey:kbadgeImage]];

    self.tblLeftSideMenuView.separatorColor = [UIColor lightGrayColor];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    self.tblLeftSideMenuView.backgroundView = imageView;
    
      [self testing_label_display];
    
   /* UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]init];
    tapGesture.numberOfTapsRequired = 1;
    [tapGesture addTarget:self action:@selector(userprofileClick:)];
    
    [_lblUserEmailID addGestureRecognizer:tapGesture];*/
    
   
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    _lblUserName.text = [Default valueForKey:FULL_NAME];
    //p_lblUserEmailID.text =[Default valueForKey:kUSEREMAIL];
    _lbl_badgetag.text = [Default valueForKey:kbadgeTag];
    _img_badge.image = [UIImage imageNamed:[Default valueForKey:kbadgeImage]];

 
}

- (void)viewWillAppear:(BOOL)animated
{
    NSArray *phoneArray = [[NSUserDefaults standardUserDefaults] objectForKey:kMODUALRIGHTS];
    modelRights= [NSMutableArray array];
    
    if (phoneArray.count > 0) {
        for (NSDictionary *item in phoneArray) {
            NSString *name = item[@"name"];
            NSString *active = [NSString stringWithFormat:@"%@",item[@"active"]];
            ModelRights *phone = [[ModelRights alloc]initWithName:name Active:active];
            [modelRights addObject:phone];
        }
        for (int i = 0 ; i < modelRights.count ; i++) {
            if ([modelRights[i].name isEqualToString:@"billing"] && [modelRights[i].active isEqualToString:@"0"]) {
//                    menuArr = [[NSMutableArray alloc] initWithObjects:@"Dialer",@"Numbers",@"All Calls/Inbox",@"Contacts",@"SMS",@"Settings", nil];
//                    imgArr = [[NSMutableArray alloc] initWithObjects:@"menu_dial",@"menu_dial",@"menu_log",@"menu_contact",@"menu_sms",@"menu_setting", nil];
                menuArr = [[NSMutableArray alloc] initWithObjects:@"Dialer",@"Numbers",@"All Calls/Inbox",@"Contacts",@"SMS", nil];
                imgArr = [[NSMutableArray alloc] initWithObjects:@"menu_dial",@"menu_dial",@"menu_log",@"menu_contact",@"menu_sms", nil];
                break;
            } else {
//                    menuArr = [[NSMutableArray alloc] initWithObjects:@"Dialer",@"Numbers",@"All Calls/Inbox",@"Contacts",@"SMS",@"Settings",@"Credit", nil];
//                    imgArr = [[NSMutableArray alloc] initWithObjects:@"menu_dial",@"menu_dial",@"menu_log",@"menu_contact",@"menu_sms",@"menu_setting",@"credit", nil];
                
                menuArr = [[NSMutableArray alloc] initWithObjects:@"Dialer",@"Numbers",@"All Calls/Inbox",@"Contacts",@"SMS",@"Credit", nil];
                imgArr = [[NSMutableArray alloc] initWithObjects:@"menu_dial",@"menu_dial",@"menu_log",@"menu_contact",@"menu_sms",@"credit", nil];
            }
        }
    }
    
    _tblLeftSideMenuView.delegate = self;
    _tblLeftSideMenuView.dataSource = self;
    [_tblLeftSideMenuView reloadData];
//    //NSLog(@" /n /n /n /n LeftMenuCall /n /n /n /n %@",[Default valueForKey:kUSEREMAIL]);
    _lblUserName.text = [Default valueForKey:FULL_NAME];
    //p_lblUserEmailID.text = [Default valueForKey:kUSEREMAIL];
    _lbl_badgetag.text = [Default valueForKey:kbadgeTag];
    _img_badge.image = [UIImage imageNamed:[Default valueForKey:kbadgeImage]];
    
    if (![[Default valueForKey:kUSEREMAIL] isEqualToString:@""] && [Default valueForKey:kUSEREMAIL] != nil) {
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                initWithString:[Default valueForKey:kUSEREMAIL]];
        [attributedString addAttribute:NSUnderlineStyleAttributeName
                                 value:@(NSUnderlineStyleSingle)
                                 range:NSMakeRange(0,[[Default valueForKey:kUSEREMAIL] length])];
        [attributedString addAttribute:NSUnderlineColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, [[Default valueForKey:kUSEREMAIL] length])];
        
        _lblUserEmailID.attributedText = attributedString;
    }

 
    
    
    [self testing_label_display];
}

-(void)testing_label_display
{
    if([Testing_version_display  isEqual: @"true"])
    {
        _lbl_version.text = Testing_version_display_count;
        _lbl_version.hidden = false;
    }
    else
    {
        _lbl_version.text = @"";
        _lbl_version.hidden = true;
    }
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuArr.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblLeftSideMenuView.frame.size.width, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [_tblLeftSideMenuView dequeueReusableCellWithIdentifier:@"Tblcell"];
    NSString *title = [menuArr objectAtIndex:indexPath.row];
    NSString *image = [imgArr objectAtIndex:indexPath.row];
    cell.lblTitle.text = title;
    cell.imgIcone.image = [UIImage imageNamed:image];
    
    
    
  
    
    
    NSString *strSelectedMenu = [Default valueForKey:SelectedSideMenu];
    NSString *strSelectedRow = title;
    
    if ([strSelectedRow isEqualToString:strSelectedMenu]) {
        cell.backgroundColor = [UIColor colorWithRed:0.135 green:0.135 blue:0.135 alpha:0.1];
        //        [cell.imgIcone  setBackgroundColor:[UIColor orangeColor]];
        cell.layer.borderWidth = 1;
        cell.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor colorWithRed:0.8519228101 green:0.8525723815 blue:0.8520234227 alpha:1]);
        NSString *image1 = [Default valueForKey:SelectedSideMenuImage];
        cell.imgIcone.image = [UIImage imageNamed:image1];
        
    }else{
        cell.backgroundColor =[UIColor colorWithRed:0.9996390939 green:1 blue:0.9997561574 alpha:1];
    }
    
    return cell;
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    DialerVC *Dialer = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
    CalllogsVC *Calllogs = [[self storyboard] instantiateViewControllerWithIdentifier: @"CalllogsVC"];
    FeedbackVC *Feedback = [[self storyboard] instantiateViewControllerWithIdentifier: @"FeedbackVC"];
    AddCreditVC *AddCredit = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddCreditVC"];
    ReminderVC *Reminder = [[self storyboard] instantiateViewControllerWithIdentifier: @"ReminderVC"];
    SettingsVC *Settings = [[self storyboard] instantiateViewControllerWithIdentifier: @"SettingsVC"];
//    ViewController *SMSList = [[self storyboard] instantiateViewControllerWithIdentifier: @"ViewController"];
    SmsListNew *SMSList = [[self storyboard] instantiateViewControllerWithIdentifier: @"SmsListNew"];
    ContactVC *ContactVC = [[self storyboard] instantiateViewControllerWithIdentifier: @"ContactVC"];
    LeaderBoardVC *leaderboardVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"LeaderBoardVC"];
    NumbersListVC *numbersVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"NumbersListVC"];
    
    
    UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
    
    //    UIViewController *vc ;
    NSString *title = [menuArr objectAtIndex:indexPath.row];
    NSString *strSelectedMenu = [Default valueForKey:SelectedSideMenu];
    NSString *strSelectedRow = title;
    
    
    if (![strSelectedRow isEqualToString:strSelectedMenu])
    {
        if([title isEqualToString:@"Dialer"])
        {
            [FIRAnalytics logEventWithName:@"ch_dialer" parameters:nil];
            [navigationController setViewControllers:@[Dialer]];
            [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
            
        }
        else if ([title isEqualToString:@"Numbers"])
        {
            [FIRAnalytics logEventWithName:@"ch_numbers" parameters:nil];
            [navigationController setViewControllers:@[numbersVC]];
            [Default setValue:@"dialpanon" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
            
            
        }
        else if ([title isEqualToString:@"Contacts"])
        {
            [FIRAnalytics logEventWithName:@"ch_contacts" parameters:nil];
            [navigationController setViewControllers:@[ContactVC]];
            [Default setValue:@"out-going-call" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
            
            
        }
        else if ([title isEqualToString:@"All Calls/Inbox"])
        {
            [FIRAnalytics logEventWithName:@"ch_call_logs" parameters:nil];
            [navigationController setViewControllers:@[Calllogs]];
            [Default setValue:@"menu_log_active" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
            
            /*    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UIWindow *alertWindow = [appDelegate window];
                alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ACWViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ACWViewController"];
                vc.strCallieeName = @"caller";
                vc.strCallDuration = @"5:22";
                vc.strDepartmentFlag = [Default valueForKey:Selected_Department_Flag];
                vc.strDepartmentName = [NSString stringWithFormat:@"Via %@",[Default valueForKey:Selected_Department]];;
                
                
                    vc.strFromNumber = @"abcxyz";
                    vc.strToNumber = @"s8372938423";
                    vc.strCallStatus = @"Outgoing";
                    
                
                
                //        vc.strFromNumber = [Default valueForKey:SELECTED_NUMBER_VERIFY];
                //        vc.strToNumber = _ContactNumber;
                
                
                    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                
                
                [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];*/
            
            
        }
        else if ([title isEqualToString:@"SMS"])
        {
            
            
            NSString *smsDisp = [Default valueForKey:kSMSRIGHTS];
            int num = [smsDisp intValue];
            if (num == 0) {
                [FIRAnalytics logEventWithName:@"ch_sms" parameters:nil];
                [navigationController setViewControllers:@[SMSList]];
                [Default setValue:@"menu_sms_active" forKey:SelectedSideMenuImage];
                [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
                [Default setValue:title forKey:SelectedSideMenu];
            }else{
                [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
                [UtilsClass showAlert:kSMSMODUALMSG contro:self];
            }
            
            
        }else if ([title isEqualToString:@"Call Planner"])
        {
            
            NSString *numbVer = [Default valueForKey:IS_CALL_PLANNER];
            int num = [numbVer intValue];
            
            //NSLog(@"Callplanner -------------- %@",numbVer);
            
            if (num == 1) {
            
                [FIRAnalytics logEventWithName:@"ch_reminder" parameters:nil];
                [navigationController setViewControllers:@[Reminder]];
                [Default setValue:@"menu_reminder_active" forKey:SelectedSideMenuImage];
                [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
                 [Default setValue:title forKey:SelectedSideMenu];
            }else{
//                [Default setValue:@"Dialer" forKey:SelectedSideMenu];
//                [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
                [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
                [UtilsClass showAlert:@"Call Reminder is not available in your plan Please Upgrade your plan" contro:self];
            }
            
        }else if ([title isEqualToString:@"Settings"])
        {
            [navigationController setViewControllers:@[Settings]];
            [Default setValue:@"menu_selected_setting" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
            [Default setValue:title forKey:SelectedSideMenu];
            
        }
        else if ([title isEqualToString:@"Credit"])
        {
            [navigationController setViewControllers:@[AddCredit]];
            [Default setValue:@"credit_active" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
            
        }
        else if ([title isEqualToString:@"Leaderboard"])
        {
            [navigationController setViewControllers:@[leaderboardVC]];
            [Default setValue:@"menu_leaderboard_active" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
            
        }
        else
        {
            [FIRAnalytics logEventWithName:@"ch_feedback" parameters:nil];
//            assert(NO);
            [navigationController setViewControllers:@[Feedback]];
            [Default setValue:@"baseline_feedback_active" forKey:SelectedSideMenuImage];
            [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
             [Default setValue:title forKey:SelectedSideMenu];
        }
        
       
    }else
    {
        [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
    }
    
}


- (IBAction)btn_logout_click:(UIButton *)sender
{
//    NSString *LoginProvider = [Default valueForKey:Login_Provider];
//    if([LoginProvider isEqualToString:Login_Plivo])
//    {
        
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:@"" showAlert:NO];
 //   }
   
}
-(void)userprofileClick:(UITapGestureRecognizer *)tap
{

    DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"DialerVC"];
    
    
    UserProfileVC *vc1 = [[self storyboard] instantiateViewControllerWithIdentifier: @"UserProfileVC"];

    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;


    [navigationController pushViewController:vc1 animated:YES];
   // [navigationController setViewControllers:@[vc,vc1]];
    [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
    
    
    
        
    
}



@end

