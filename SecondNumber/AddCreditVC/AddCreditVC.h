//
//  AddCreditVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddCreditVC : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver>
@property (weak, nonatomic) IBOutlet UITableView *tblCreaditList;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCredit;
@property (weak, nonatomic) IBOutlet UIView *viewSelectedCredit;
@property (weak, nonatomic) IBOutlet UIView *viewDisplayCredits;
@property (weak, nonatomic) IBOutlet UILabel *lblDisplayCredit;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedCredit;

@end

NS_ASSUME_NONNULL_END
