//
//  AddCreditVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "AddCreditVC.h"
#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"


#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
@import Firebase;
@interface AddCreditVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *credits;
    WebApiController *obj;
    NSArray *creditsIAPArr;
    BOOL select;
    NSInteger selectedCreditIndex;
}
@end

@implementation AddCreditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getCredits];
    [self setUpDesign];
     self.sideMenuController.leftViewSwipeGestureEnabled = YES;
    
    // Do any additional setup after loading the view.
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return credits.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Tblcell *cell = [_tblCreaditList dequeueReusableCellWithIdentifier:@"Tblcell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblCredit.text = credits[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    _lblSelectedCredit.text = credits[indexPath.row];
    _tblCreaditList.hidden = true;
    select = YES;
    
    selectedCreditIndex = indexPath.row;
}

- (IBAction)btnDropDown:(id)sender {
    if (select == NO) {
        select = YES;
        _tblCreaditList.hidden = true;
    } else {
        select = NO;
        _tblCreaditList.hidden = false;
    }
}

- (IBAction)btnAddCreditClicked:(id)sender {
    
    if([UtilsClass isNetworkAvailable]) {
        
        [self addCreditPresed];
    }else {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

- (void)creditAddRes:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    NSLog(@"add credit response :: %@",response1);
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
      
        [self getCredits];
        
    }else {
        @try {
        [UtilsClass showAlert:[[response1 valueForKey:@"error"]valueForKey:@"error"] contro:self];
        }
        @catch (NSException *exception) {
        }
    }
        
    }
}

- (void)addCreditPresed
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:kAlertTitle
                                 message:[NSString stringWithFormat:@"Are you sure you want to add %@ credit to your Account?",_lblSelectedCredit.text]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
        
      /*  SKProductsRequest *productreq = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithObject:[self->creditsIAPArr objectAtIndex:self->selectedCreditIndex]]];
        
        productreq.delegate = self;
        [productreq start];*/
        
                [self addCreditApi];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)addCreditApi {
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *authToken = [Default valueForKey:AUTH_TOKEN];
    NSString *url = [NSString stringWithFormat:@"addCredit/%@",userId];
    NSString *credit = [_lblSelectedCredit.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSDictionary *passDict = @{@"userId":userId,
                               @"authToken" : authToken,
                               @"credits" : credit,
                               @"deviceType" : @"iOS"};  // we60 ,
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
   // [obj callAPI_POST:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(creditAddRes:response:) andDelegate:self];
}

-(void)setUpDesign{
    
//    [UtilsClass view_navigation_title:self title:@"Add Credits"color:UIColor.whiteColor];
    [UtilsClass view_navigation_title:self title:@"Credits"color:UIColor.whiteColor];
    select = YES;
    _lblDisplayCredit.text = [NSString stringWithFormat:@"$%@",[Default valueForKey:CREDIT]];
    credits = [NSMutableArray arrayWithObjects:@"$10",@"$30",@"$50",@"$100", nil];
    
    _btnAddCredit.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _btnAddCredit.layer.borderWidth = 1.0;
    _btnAddCredit.layer.cornerRadius = 5.0;
    
    _viewDisplayCredits.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _viewDisplayCredits.layer.borderWidth = 1.0;
    _viewDisplayCredits.layer.cornerRadius = 5.0;
    
    _viewSelectedCredit.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _viewSelectedCredit.layer.borderWidth = 1.0;
    _viewSelectedCredit.layer.cornerRadius = 5.0;
    
    _tblCreaditList.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _tblCreaditList.layer.borderWidth = 1.0;
    _tblCreaditList.layer.cornerRadius = 5.0;
    
    _tblCreaditList.delegate = self;
    _tblCreaditList.dataSource = self;
    
    creditsIAPArr = [NSArray arrayWithObjects:@"com.credit.10",@"com.credit.30",@"com.credit.50",@"com.credit.100", nil];
}
- (IBAction)btn_menu_click:(UIBarButtonItem *)sender
{
  
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}


-(void)getCredits {
    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *userID = [Default valueForKey:USER_ID];
    //NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
    
    //NSDictionary *passDict = @{@"userId":userID};
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";

       NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
       NSDictionary *passDict = @{@"userId":userID,
                                  @"device":@"ios",
                                  @"deviceInfo":Device_Info,
                                  @"token":fcmtoken,
                                  @"versionInfo":VERSIONINFO,};
       NSLog(@"getcredit : %@",passDict);
       NSLog(@"getcredit : %@%@",SERVERNAME,url);
       NSLog(@"getcredit : %@",[Default valueForKey:AUTH_TOKEN]);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    if([UtilsClass isNetworkAvailable]) {
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getCredits:response:) andDelegate:self];
    }else {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
    
}

- (void)getCredits:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //    [Processcall hideLoadingWithView];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        NSLog(@"getCredits : %@",response1);
        [[NSUserDefaults standardUserDefaults] setValue:response1[@"credit"] forKey:CREDIT];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _lblDisplayCredit.text = [NSString stringWithFormat:@"$%@",[Default valueForKey:CREDIT]];
//        [UtilsClass showAlert:@"Credit added successfully" contro:self];
        
    }
    else {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
[UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
        }
        }
        @catch (NSException *exception) {
        }
    }
        
    }
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    select = YES;
    _tblCreaditList.hidden = true;
}

#pragma mark - In app Purchase credit
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    SKProduct *validProduct = nil;
    //int count = [response.products count];
    NSLog(@"response:: %@",response.invalidProductIdentifiers);

    if(response.products.count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
    
    
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];

    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");

            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            //NSString *productID = transaction.payment.productIdentifier;
           // [self doRemoveAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        

        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                
                break;
            case SKPaymentTransactionStatePurchased:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                
                NSLog(@"Transaction state -> Purchased");
                
                [UtilsClass makeToast:@"Credit Added Successfully."];
            
                break;
            }
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}

@end
