//
//  Contact+CoreDataProperties.m
//  callhippolin
//
//  Created by Apple on 21/06/21.
//  Copyright © 2021 Admin. All rights reserved.
//
//

#import "Contact+CoreDataProperties.h"

@implementation Contact (CoreDataProperties)

+ (NSFetchRequest<Contact *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Contact"];
}

@dynamic company;
@dynamic contactID;
@dynamic email;
@dynamic name;
@dynamic number;


@end
