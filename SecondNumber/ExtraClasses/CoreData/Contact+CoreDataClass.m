//
//  Contact+CoreDataClass.m
//  callhippolin
//
//  Created by Apple on 21/06/21.
//  Copyright © 2021 Admin. All rights reserved.
//
//

#import "Contact+CoreDataClass.h"
#import "CoreDataManager.h"
#import "Constant.h"
#import "GlobalData.h"


@implementation Contact

+(void)addContactToDB:(Contact *)contactObj
{

    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
  

    NSManagedObject *newContact ;
                
    newContact = [NSEntityDescription
                              insertNewObjectForEntityForName:@"Contact"
                              inManagedObjectContext:context];
                
                
                [newContact setValue: contactObj.name forKey:@"name"];
                [newContact setValue: contactObj.email forKey:@"email"];
                [newContact setValue: contactObj.number forKey:@"number"];
                [newContact setValue:contactObj.company forKey:@"company"];
                [newContact setValue:contactObj.contactID forKey:@"contactID"];

                

   
    NSError *err=nil;
    [context save:&err];
   
   
}

+(void)addBatchContactToDB:(NSMutableArray *)contactArray
{
    
    if (@available(iOS 13.0, *)) {
        
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    
    [context performBlock:^{
        
       
        
        
           __block int index = 0;
            NSUInteger total = contactArray.count;
            
            NSBatchInsertRequest *batchreq = [[NSBatchInsertRequest alloc]initWithEntityName:@"Contact" managedObjectHandler:^BOOL(NSManagedObject * _Nonnull obj) {
                
                if (index >= total) {
                    return true;
                }
                
                Contact *copycon = [contactArray objectAtIndex:index];
                
                Contact *newobj = obj;
                newobj.name = copycon.name;
                newobj.email = copycon.email;
                newobj.company = copycon.company;
                newobj.contactID = copycon.contactID;
                newobj.number = copycon.number;
                
                index = index + 1;
                return  false;
                
                
            }];
            
            NSError *err;
            [context executeRequest:batchreq error:&err];
            
            NSLog(@"error saving batch :: %@",err);
            
//            [Default setBool:true forKey:kisContactStoredInDB];
//            [[GlobalData sharedGlobalData] makeArrayFromDBChContact];
            
    }];
        }

         else {
            // Fallback on earlier versions
             
             CoreDataManager *dbm = [[CoreDataManager alloc] init];
             NSManagedObjectContext *context = [dbm managedObjectContext];
               
//             NSManagedObjectContext *bgcontext = [[NSManagedObjectContext alloc]initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//             [bgcontext setParentContext:context];
//
            [context performBlock:^{
                
//                while (true) {
//                    @autoreleasepool {
                    
                        // Code that creates autoreleased objects.

//                             if (contactArray.count == 0) {
//                                 break;
//                             }

                        for (int i =0; i< contactArray.count;i++) {
                            
                            Contact *conobj = [contactArray objectAtIndex:i];
                            
                            Contact *newcon = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
                            
                            newcon.name = conobj.name;
                            newcon.email = conobj.email;
                            newcon.company = conobj.company;
                            newcon.contactID = conobj.contactID;
                            newcon.number = conobj.number;
                           
                             }
//                         }
//
//                     }
                
                
                // only save once per batch insert
                NSError *error = nil;
//                [bgcontext save:&error];
//
//                NSLog(@"error save context < 13  %@",error);

                [context save:&error];
              //  [bgcontext reset];
                
                
                 }];
             
        }
        
    
   
//        NSError *error = nil;
//         [context save:&error];
//
//    NSLog(@"error save context adding   %@",error);
   
}

+(NSArray *)fetchContactFromDB
{
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    [request setResultType:NSManagedObjectResultType];
    
   // NSManagedObject *matches = nil;
    NSError *error;
    
   
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    
//
//    NSLog(@"objects %@",objects);
    NSLog(@"object count :: %lu",(unsigned long)objects.count);
    
//    for (int i=0; i<objects.count; i++) {
//        Contact *con = [objects objectAtIndex:i];
//        NSLog(@"number of searched contact :: %@", con.number);
//        NSLog(@"num access :: %@",[con.number valueForKey:@"_id"]);
//        
//    }
    
 
    return objects;
    
    
}
/*
+(NSArray *)fetchContactFromDBForNumber
{

    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];

    NSPredicate *pred =
    [NSPredicate predicateWithFormat:@"(number.number contains[c] %@)",
     @"111"];
    [request setPredicate:pred];
    NSManagedObject *matches = nil;

    NSError *error;
    
   
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];

    NSLog(@"objects searched %@",objects);
    NSLog(@"object search count :: %lu",(unsigned long)objects.count);
    
    for (int i=0; i<objects.count; i++) {
        Contact *con = [objects objectAtIndex:i];
        NSLog(@"name of searched contact :: %@", con.name);
    }
  
    
  
    
    return objects;
    
    
}*/


+(NSDictionary *)getNumberArrayJson:(NSString *)numStr
{
    //NSError *err;
    NSData *data = [numStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    return jsonDict;
}
+(NSString *)getStringFromNumberArray:(NSArray *)numArray
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:numArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}
+(NSUInteger )allContactCount
{
    NSUInteger count =0 ;
    
    NSError *err;

    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    count = [context countForFetchRequest:request error:&err];
    
    return count;
}
+(void)editContactToDB:(Contact *)newCon
{
    
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];

    NSPredicate *pred =
    [NSPredicate predicateWithFormat:@"(contactID = %@)",
     newCon.contactID];
    
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
   // NSLog(@"error in edit contact to fetch :%@",error);

    
    if (objects.count>0) {
        
        Contact *oldCon = [objects objectAtIndex:0];
        oldCon.name = newCon.name;
        oldCon.company = newCon.company;
        oldCon.email = newCon.email;
        oldCon.number = newCon.number;
        oldCon.contactID = newCon.contactID;
       
        
        [context save:&error];
        
       // NSLog(@"error in edit contact :%@",error);
    }

    
    
}
+(void)deleteFromDB:(NSString *)contactID
{
    
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];

    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];

    NSPredicate *pred =
    [NSPredicate predicateWithFormat:@"(contactID = %@)",
     contactID];
    
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    
    if (objects.count>0) {
        [context deleteObject:[objects objectAtIndex:0]];
        
        [context save:&error];
        
      //  NSLog(@"error in delete contact :%@",error);
    }
    
}
/*
+(void)batchDeleteFromDB:(NSMutableArray *)conArr
{
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    
    NSBatchDeleteRequest *batchDelete = [[NSBatchDeleteRequest alloc]initWithObjectIDs:conArr];

    NSError *err;
    [context executeRequest:batchDelete error:&err];
    
    NSLog(@"error in batch delete  %@",err);
    
    NSLog(@"updated count after delete >>>>> %lu",(unsigned long)[self allContactCount]);
    
}*/

/*+(void)deleteBatchFromDB:(NSMutableArray *)contactArray
{
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    
    [context performBlock:^{
        
       
        if (@available(iOS 13.0, *)) {
        
        
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Contact"];

            NSPredicate *pred =
            [NSPredicate predicateWithFormat:@"(contactID = %@)",
             contactID];
            
            [request setPredicate:pred];
            
            
            NSBatchDeleteRequest *batchreq = [NSBatchDeleteRequest alloc]initWithObjectIDs:<#(nonnull NSArray<NSManagedObjectID *> *)#>;
            
            
            
            NSError *err;
            [context executeRequest:batchreq error:&err];
            
            NSLog(@"error saving batch :: %@",err);
            
//            [Default setBool:true forKey:kisContactStoredInDB];
//            [[GlobalData sharedGlobalData] makeArrayFromDBChContact];
            
            
        }

        
         else {
            // Fallback on earlier versions
             
             CoreDataManager *dbm = [[CoreDataManager alloc] init];
             NSManagedObjectContext *context = [dbm managedObjectContext];
               
             NSManagedObjectContext *bgcontext = [[NSManagedObjectContext alloc]initWithConcurrencyType:NSPrivateQueueConcurrencyType];
             [bgcontext setParentContext:context];
            
            [bgcontext performBlock:^{
                
                while (true) {
                    @autoreleasepool {
                    
                        // Code that creates autoreleased objects.

                             if (contactArray.count == 0) {
                                 break;
                             }

                        for (int i =0; i< contactArray.count;i++) {
                            
                            Contact *conobj = [contactArray objectAtIndex:i];
                            
                            Contact *newcon = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:bgcontext];
                            
                            newcon.name = conobj.name;
                            newcon.email = conobj.email;
                            newcon.company = conobj.company;
                            newcon.contactID = conobj.contactID;
                            newcon.number = conobj.number;
                           
                             }
                         }

                         // only save once per batch insert
                         NSError *error = nil;
                         [bgcontext save:&error];
                    
                    NSLog(@"error save context < 13  %@",error);
                         [bgcontext reset];
                     }
                 }];
             
        }
        
    
    }];
        
  

}*/
+(BOOL)ifExistContact:(NSString *)contactID
{
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    BOOL isexist = false;
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Contact"
                inManagedObjectContext:context];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate *pred =
    [NSPredicate predicateWithFormat:@"(contactID = %@)",
     contactID];
    [request setPredicate:pred];
    
    NSArray *obj = [context executeFetchRequest:request error:nil];
    
    if (obj.count>0) {
        isexist = true;
    }
    
    return isexist;
}
+(void)deleteAllContacts
{
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
   // NSPersistentStoreCoordinator *pscord = [dbm persistentStoreCoordinator];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    [context performBlock:^{
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Contact"];
        NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];

        NSError *deleteError = nil;
        [context executeRequest:delete error:&deleteError];
    }];
   
    
  //  [pscord executeRequest:delete withContext:context error:&deleteError];
    
   // NSLog(@"error in delete contact  %@",deleteError);
}

@end
