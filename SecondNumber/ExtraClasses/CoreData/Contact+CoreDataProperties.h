//
//  Contact+CoreDataProperties.h
//  callhippolin
//
//  Created by Apple on 21/06/21.
//  Copyright © 2021 Admin. All rights reserved.
//
//

#import "Contact+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Contact (CoreDataProperties)

+ (NSFetchRequest<Contact *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *company;
@property (nullable, nonatomic, copy) NSString *contactID;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *name;
@property ( nonatomic, retain) id number;

@end

NS_ASSUME_NONNULL_END
