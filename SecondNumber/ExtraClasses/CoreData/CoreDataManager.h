//
//  CHDataController.h
//  callhippolin
//
//  Created by Apple on 21/06/21.
//  Copyright © 2021 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoreDataManager : NSObject
{
   

}
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
//+ (CoreDataManager*)sharedCoredataManager;
@end

 

NS_ASSUME_NONNULL_END
