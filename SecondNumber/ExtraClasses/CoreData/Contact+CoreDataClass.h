//
//  Contact+CoreDataClass.h
//  callhippolin
//
//  Created by Apple on 21/06/21.
//  Copyright © 2021 Admin. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Contact : NSManagedObject

+(void)addContactToDB:(Contact *)contact;
+(NSDictionary *)getNumberArrayJson:(NSString *)numStr;
+(NSString *)getStringFromNumberArray:(NSArray *)numArray;
+(NSUInteger )allContactCount;
+(NSArray *)fetchContactFromDB;
+(void)editContactToDB:(Contact *)newCon;
+(void)deleteFromDB:(NSString *)contactID;
+(BOOL)ifExistContact:(NSString *)contactID;
+(void)deleteAllContacts;
+(void)addBatchContactToDB:(NSMutableArray *)contactArray;

//+(void)batchDeleteFromDB:(NSMutableArray *)conArr;

//+(NSArray *)fetchContactFromDBForNumber;
@end

NS_ASSUME_NONNULL_END

#import "Contact+CoreDataProperties.h"
