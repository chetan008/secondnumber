
#ifndef MapApp_Constant_h
#define MapApp_Constant_h
// $calldelv@100 //  


#define Login_Email                 @""
#define Login_Pass                 @""

#define OTHER                 @""

//#define SOCKET_URL                                      @"https://staging-phone.callhippo.com"                                      // Staging
//#define SOCKET_URL                                    @"https://dialer.callhippo.com"                                             // live
//#define SOCKET_URL                                    @"https://dev-phone.callhippo.com"                                         // live
//#define SOCKET_URL                                    @"https://fsapi.callhippo.com"                                         // live
#define SOCKET_URL                                    @"https://dialer3.callhippo.com"
//#define SOCKET_URL                                    @"https://dev3-phone.callhippo.com"
// live
//#define SOCKET_URL                                    @"https://blendr-phone.callhippo.com"

//#define SERVERNAME                                      @"https://staging-phone.callhippo.com/api/v3/"                              // dev
//#define SERVERNAME                                    @"https://dialer.callhippo.com/api/v3/"
//#define SERVERNAME                                    @"https://dev-phone.callhippo.com/api/v3/"
//#define SERVERNAME                                    @"https://fsapi.callhippo.com/api/v3/"
#define SERVERNAME                                    @"https://dialer3.callhippo.com/api/v3/"
//#define SERVERNAME                                    @"https://dev3-phone.callhippo.com/api/v3/"
//#define SERVERNAME                                    @"https://blendr-phone.callhippo.com/api/v3/"

#define VERSIONINFO                                     @"1.0"
#define APP_VERSION                                     @"1.0"                                                                      // force to update

#define APP_ID                                          @"1256873576"                                                               // force to update
#define Testing_version_display                         @"true"                                                                    // testing version need to false in live

#define Force_update                                    @"true"                                                                     // true
#define Testing_version_display_count                   @"v 1.0"                                                                         // force to update
#define RemoveAllUserDefult_text                        @"plivo_switch_10.0"

//for testing
//f6daf2fb900988a7e032059eb6914d860216f84070b16bfab9764bc1a187a731
//NPS-d58791f5
// live :8b97d31bd4a3d2d0406d53ded6f725ff29c9f71fcba163f5e38d7fa3746038d7
//  NPS-dd6ff276

#define LIVE_NPS_CLIENT_ID                              @"8b97d31bd4a3d2d0406d53ded6f725ff29c9f71fcba163f5e38d7fa3746038d7"         // wootric
#define LIVE_NPS_ACCOUNT_TOKEN                          @"NPS-dd6ff276"                                                             // wootric

#define STAGING_NPS_CLIENT_ID                           @"f6daf2fb900988a7e032059eb6914d860216f84070b16bfab9764bc1a187a731"         // wootric
#define STAGING_NPS_ACCOUNT_TOKEN                       @"NPS-d58791f5"                                                             // wootric

#define Twilio_access_token                 @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzVkOGYxYmU1NzQ0NjIwYmY1ZmM2ODczNmJiMjhkNjcxLTE1NzY0NzkxNzAiLCJncmFudHMiOnsiaWRlbnRpdHkiOiI1ZDM1YTJhNDk3Y2M3ODJkMDgxY2FhZjItaW9zIiwidm9pY2UiOnsiaW5jb21pbmciOnsiYWxsb3ciOnRydWV9LCJvdXRnb2luZyI6eyJhcHBsaWNhdGlvbl9zaWQiOiJBUGQ4NTJmYzE0YzlkM2VjOTA5NWE3MmNiNGY3ZjVhNDNmIn0sInB1c2hfY3JlZGVudGlhbF9zaWQiOiJDUjg3NTA5NjdmMDg1Mzk5OGNjNmFlNDkyMDU3NWYwNWRhIn19LCJpYXQiOjE1NzY0NzkxNzAsImV4cCI6MTU3NjQ4Mjc3MCwiaXNzIjoiU0s1ZDhmMWJlNTc0NDYyMGJmNWZjNjg3MzZiYjI4ZDY3MSIsInN1YiI6IkFDZjU1MmZjMDMyNDIxNzY2Y2VmZWUzOWRkMWU3OTZlNjQifQ.8gNnSx5ka9b9dMCoabTVrg4v2A_YoZt1wynrLaa5N9I"    // Twilio

#define twilio_test                                     @"true"                                                                      // force to update

#define HOLD_URL @"https://s3.amazonaws.com/callhippo_staging/call_hold/15168789827365a4c70c0bc9e741f843ad5c6.mp3" 
#define Status_Code @"403"
#define Network_Speed_Value @"NetworkSpeedValue"
//#define Login_URL @"login/"
#define COUNTRYTIME @"countrytime"
#define Get_Numbers @"numbers/"
#define Default [NSUserDefaults standardUserDefaults]

#define kREACHABILITYURL "www.google.com"

#define kUSERNAME  @"UserName"
#define kPASSWORD @"Password"
#define kUSEREMAIL @"EmailID"
#define kbadgeTag @"Badgename"
#define kbadgeImage @"BadgeImage"

#define twilio_token  @"TwilioToken"
#define kAUTHENTICATIONSTATUS  @"AuthenticationStatus"
#define kCALLSINFO  @"CallsInfo"
#define kCALLHIPPOCONTACT  @"CallHippoContact"
#define kMODUALRIGHTS  @"moduleRights"
#define kSMSRIGHTS @"smsRights"
#define kTHINQACCESS @"thinqAccess"
#define kTHINQVALUE @"thinqValue"
#define kPROVIDERHOSTNAME  @"providerHostname"
#define kIOSSIPPROTOCOL  @"iossipProtocol"
#define ownExtensionNumber  @"ownExtensionNumber"
#define EndPointInfo  @"endpointInfo"
#define UserLoginStatus  @"userloginstatus"
#define Login_Provider  @"loginprovider"
#define Login_Plivo  @"plivo"
#define Login_Twilio  @"twilio"

#define CallingProvider @"CallingProvider"
//#define IS_Plivo  @"plivo"
//#define IS_Twilio  @"twilio"

#define IS_AutoSwitch  @"auto_switch"
#define IS_AUTO_SWITCH_PLAN  @"auto_switch_plan"
#define IS_DefultNumber  @"defult_number"
#define Defult_Number  @"defult_number"
#define IS_Installed  @"is_installed"

#define Outgoing_Call_Code  @"OutgoingCallCode"
#define Outgoing_Call_CodeName  @"OutgoingCallCodeName"

#define IS_ChatviewDisplayed  @"chatviewDisplayed"
#define IS_ChatviewDisplayed_ThredID  @"chatviewDisplayedThredID"

#define IS_CallLogDisplayed  @"allCallsDisplayed"
//#define kREACHABILITYURL "www.google.com"

#define kISPostCallSurvey  @"isPostCallSurvey"
#define kTagList  @"tagList"


#define DEVICE_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define DEVICE_WIDTH  [UIScreen mainScreen].bounds.size.width
#define widthCalculate(w) (([[UIScreen mainScreen] bounds].size.width) * (w / 320.0))
#define heightCalculate(h) (([[UIScreen mainScreen] bounds].size.height) * (h / 568.0))

#define kLOGINMSG @"Trying to Login, please wait"
#define kLOGOUTMSG @"Trying to Logout, please wait"
#define kLOGINSUCCESS @"SIP registration done successfully From Home View"
#define kLOGINSUCCESS1 @"SIP registration done successfully123 From DashBoar"

#define kLOGOUTSUCCESS @"Logged Out Successfully"
#define kLOGINFAILTITLE @"Login failed"
#define kLOGINFAILMSG @"Login failed. Please check your username and password"
#define kINVALIDEMAIL @"Please login with plivo account"

#define kNOINTERNETTITLE @"No Internet"
#define kNOINTERNETMSG @"Please connect to internet"
#define kINVALIDENTRIESTITLE @"Invalid data"
#define kINVALIDENTRIESMSG @"Please enter username and password"
//#define kINVALIDENTRIESMSG @"Please enter SIP Endpoint Username and password"
#define kINVALIDSIPENDPOINTMSG @"Please enter phone number"
//#define kINVALIDSIPENDPOINTMSG @"Please enter SIP Endpoint or Phone number"

#define kDurationGreaterThanMsg @"Please enter value greater than 0"
#define kDurationLessThanMsg @"Please enter value less than 60"
#define kDurationChangeSuccessMsg @"Duration is updated successfully"

#define kREQUESTFAILED @"Request failed"
#define kINCOMINGCALLREPORTFAILED @"Failed to report incoming call successfully"
#define kSTARTACTIONFAILED @"StartCallAction transaction request failed"
#define kENDACTIONFAILED @"EndCallAction transaction request failed"
#define kCREDITLAW @"Insufficient Credit."
#define Feedback_success @"Thank You for sending us feedback"
#define kNUMBERVERIFYMSG @"Number is not verified,Please contact support@callhippo.com."
#define kDUMMYNUMBERMSG @"Please purchase a number from the dashboard to start using the services"
#define kNUMBERASSIGN @"You haven't been assigned any number."
#define KRIMINDERSET @"Your Reminder has been set Successfully"
#define kSMSMODUALMSG @"You do not have access to this module, please contact your admin for rights"
#define kMESSAGESENTSUCCESSMSG @"Message sent successfully"
#define kCONTACTSAVESUCCESSMSG @"Contact saved successfully"
#define kSUBCONTACTSAVESUCCESSMSG @"Number has been added successfully"
#define kSUBCONTACTDELETESUCCESSMSG @"Number has been deleted Successfully"

#define kCONTACTEDITSUCCESSMSG @"Contact updated successfully"
#define kREMINDERDELETEMSG @"Reminder deleted successfully"
#define kRECORDINGWAITMSG @"Wait a bit! your audio recording is being loaded"

//#define kENDPOINTURL @"@phone.test.plivo.com"

#define kENDPOINTURL @"@phone.plivo.com"

//#define BASE_URL  @"https://staging-phone.callhippo.com/api/v2/"
//#define BASE_URL  @"https://dialer.callhippo.com/api/v2/"
//#define BASE_URL  @"https://f917541c.ngrok.io/api/v2/"
//#define BASE_URL  @"https://dev-phone.callhippo.com/api/v2/"
//#define BASE_URL  @"http://df01a15b.ngrok.io/api/v2/"


#define Speed_url @"updatecall/"
#define Login_URL @"mobilelogin/"
#define Upload_speed_count @"updatecall/"
#define DTMF_URL @"dtmf/plivo"
#define Calllogs_URL @"callLog"
#define GETCONTACT_URL @"contact/"
#define CONTACT_EDIT @"edit"
#define CALLING_CODE @"calling_code"
#define SMSLOG @"smslog/"
#define SMSLOGWITHSKIP @"getSmsLogWithLimitSkip/"
#define SMSSEND @"smssend/"
#define FEEDBACK @"feedback/"
#define ENDPOINT_USERNAME @"Endpoint_Username"
#define PURCHASE_NUMBER @"PURCHASE_NUMBER"
#define ExtentionCall @"ExtentionCall"
#define ExtentionCallProvider @"ExtentionCallProvider"
#define ACTIVE_SUB_USER @"ACTIVE_SUB_USER"
#define OUTGOING_CALL_COUNTRY @"OUTGOING_CALL_COUNTRY"
#define ISDummyNumber @"isNumberPurchased"
#define ENDPOINT_PASSORD @"Endpoint_Password"
#define USER_ID @"User_Id"
#define CALL_SID @"callSid"
#define AUTH_TOKEN @"Auth_Token"
#define CREDIT @"Credit"
#define BLINDTRANSFER @"blindTransferCall/plivo"
#define WARMRANSFER @"makecall/plivo"
#define PHONENUMBER_ARRAY @"PhoneNumber_Array"
#define SELECTED_NUMBER_VERIFY @"Selected_Number_Verify"
#define LOGIN_ALERT @"Enter valid id & password."
#define SELECTEDNO @"SelectedNO"
#define CALL_NUMBER @"Callnumber"
//#define NUMBERID @"NumberID"
#define FULL_NAME @"fullName"
#define CALLHIPPO_AUTH_ID @"Callhippo_Auth_ID"
#define CALLHIPPO_AUTH_TOKEN @"Callhippo_Auth_Token"
#define Default [NSUserDefaults standardUserDefaults]
#define SMARTLOOK @"Smartlook"
#define LOGINGMAIL @"Logingmail"
#define INCOUTCALL @"IncOutCall"
#define Timer_Start @"START"
#define Timer_Stop @"STOP"
#define Timer_Call @"STOP"
#define PLAN_DISPLAY_NAME @"Plandisplayname"
#define PARENT_ID @"ParentId"
#define CALL_TRANSFER @"CallTransfer"
#define FCMTOKENGETFIRST @"FCMTokenGetFirst"

#define selected_country_code_by_popup @"selected_country_code_by_popup"
#define selected_country_image_by_popup @"selected_country_image_by_popup"
#define selected_country_name_by_popup @"selected_country_name_by_popup"

#define INCOMING @"Incoming call"
#define OUTGOING @"Outgoing call"
#define CALLBAND @"As per the governments rules,you are not allowed to call Indian mobile and landline sitting in india from a VOIP service like Callhippo.However,there is no restrictions on calling to other countries."
#define NUMBERID @"NumberID"
#define Selected_Department @"Selected_Department"
#define Selected_Department_Flag @"Selected_Department_Flag"
#define SelectedSideMenu @"SelectedSideMenu"
#define SelectedSideMenuImage @"SelectedSideMenuImage"
#define LastCallSidTwilio @"lastcallsidtwilio"
#define Cell_callhippo_image @"round_callhippo-1"
#define cell_user_image @"logo_drawer_user_v2"
#define IS_HOLD @"ishold"
#define IS_HOLD_BY_API @"isholdbyapi"
#define IS_DTMF_BY_API @"isdtmfbyapi"
#define IS_TRANSFER @"istransfer"
#define IS_CALL_REMINDER @"iscallReminder"
#define IS_ACW_ENABLE @"isacwenable"
#define IS_CALL_PLANNER @"iscallPlaner"
#define ACW_PLAN @"acwPlan"
#define EXTENSION_PLAN @"extensionPlan"
#define XPH_EXTRA_HEADER_REMINDER @"xphExtraHeaderReminder"
#define IS_RECORDING @"isrecording"
#define IS_DISPLAY_TIME @"isdusplaytime"
#define IS_DISPLAY_TIME_PLAN @"isdusplaytimeplan"
#define IS_USER_LIVE @"isuserlive"
#define IS_DEFAULT_NUMBER_PLAN @"isdefaultnumberplan"
#define USER_LAST_CALL_DETAIL @"userLastCallDetail"
#define IS_LAST_CALL_DDETAIL_IN_PLAN @"isLastCallDetailInPlan"
#define ACW_TIME @"acwtime"
#define TRANSFER_FOR_TWILIO @"transferForTwilio"
#define IS_DEVICEINFO @"deviceInfo"
#define IS_VERSIONINFO @"versionInfo"
#define LAST_COUNTRY_CODE @"lastdialcountrycode"
#define IS_PHONENO @"phoneno"
#define DefaultProvider  @"defaultProvider"

#define klastDialCountryCode @"lastDialCountryCode"
#define klastDialNumber @"lastDialNumber"
#define klastDialCountryName @"lastDialCountryName"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define ALLLOGS @"All"
#define MISSEDLOGS @"Missed"
#define VOICEMAILLOGS @"Voicemail"

#define ALLINBOX @"AllInbox"
#define MISSEDINBOX @"MissedInbox"
#define VOICEMAILINBOX @"VoicemailInbox"
#define FEEDBACKINBOX @"FeedbackInbox"
#define TAGFILTERINBOX @"TagFilterInbox"


#define IS_BLIND_TRANSFER_FLAG @"isBlindTransferFlag"
#define IS_TRANSFER_FLAG @"isTransferFlag"

#define kIsBlackList @"isBlackList"
#define kblackListedArray @"blackListedArray"

#define kIsCustomPlan @"isCustomPlan"

#define kISFeedbackInPlan @"isFeedbackInPlan"
#define kISTaggingInPlan @"isTaggingInPlan"
#define kISRecordingInPlan @"isRecordingInPlan"
#define kISNotesInPlan @"isNotesInPlan"

#define kISTransferCallForTag @"iSTransferCallForTag"
#define kISFeedbackRightsForSub @"iSFeedbackRightsForSub"

#define kIsNumberMask @"isNumberMask"

#define kTelephonyProviderSwitch @"TelephonyProviderSwitch"
#define kProviderSwitchA @"ProviderSwitchA"
#define kProviderSwitchB @"ProviderSwitchB"

#define kTwilioEdgeValue @"TwilioEdge"


#define encryptionKey @"F8A2BB3F418FEBDD632535271FB50866"
//#define kencryptionInMobile @"encryptionInMobile"

/* next release
#define kdisableRecordingAutomation @"disableRecordingAutomation"
#define kautoPauseRecordingAutomation @"autoPauseRecordingAutomation"
#define kcallRecordingPermission @"callRecordingPermission"*/
#define kListenTocallRecordRights @"ListenTocallRecordRights"

#define kMissedCallAlert @"MissedcallAlert"
#define kVoicemailCallAlert @"VoicemailAlert"
#define kSMSCallAlert @"SMSAlert"

#define kbackendProvider @"backend_provider"

//contacts
#define kisContactStoredInDB @"contactStoredinDB"

#define kNeedToUpdateContact @"NeedToUpdateContact"
#define kEmailIdForDB @"EmailIdForDB"
#define kisContactSyncingRunning @"isContactSyncingRunning"
#define kNeedToIgnoreUpdatedContact @"NeedToIgnoreUpdatedContact"

#define kanyIntegrationSync @"anyIntegrationSync"

#define Login_Linphone @"linphone"
#define kFreeswitchUsername @"freeswitchUsername"
#define kFreeswitchPassword @"freeswitchPassword"
#define kFreeswitchDomain @"freeswitchDomain"


#define kPlanName @"planname"

#define kAlertTitle @"2nd Phone Number"
#define kStripeId @"StripeId"


#endif
