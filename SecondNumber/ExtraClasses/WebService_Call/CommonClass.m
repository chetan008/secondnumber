//
//  CommonClass.m
//  PainRemedy
//
//  Created by Ratnakala_41 on 5/22/15.
//  Copyright (c) 2015 ratnakala. All rights reserved.
//

#import "CommonClass.h"

@implementation CommonClass

+(NSString *) getDocumentPath {
    
    return [[CommonClass alloc] getDocumentPath];
}

-(NSString *) getDocumentPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+(BOOL) saveFileWithFileName: (NSString *)filename fileData:(NSData *)data {
    return [[CommonClass alloc] saveFileWithFileName:filename fileData:data];
}

-(BOOL) saveFileWithFileName: (NSString *)filename fileData:(NSData *)data {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", [self getDocumentPath], filename];
    //NSLog(@"path : %@", path);
    return [data writeToFile:path atomically:YES];
}

+ (UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    CGFloat scale = [[UIScreen mainScreen]scale];
//    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newHeight), NO, scale);
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize {
    
    float width = newSize.width;
    float height = newSize.height;
    
//    UIGraphicsBeginImageContext(newSize);
    CGFloat scale = [[UIScreen mainScreen]scale];
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    float widthRatio = image.size.width / width;
    float heightRatio = image.size.height / height;
    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
    
    width = image.size.width / divisor;
    height = image.size.height / divisor;
    
    rect.size.width  = width;
    rect.size.height = height;
    
    //indent in case of width or height difference
//    float offset = (width - height) / 2;
//    if (offset > 0) {
//        rect.origin.y = offset;
//    }
//    else {
//        rect.origin.x = -offset;
//    }
    
    rect.origin.y = ([UIScreen mainScreen].bounds.size.height / 2) - (height / 2);
    
    [image drawInRect: rect];
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return smallImage;
    
}

@end
