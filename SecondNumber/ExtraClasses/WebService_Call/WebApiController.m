//
//  WebApiController.m
//  Chirag Lukhi
//
//  Created by Lanetteam on 9/10/12.
//  Copyright (c) 2012 HongYing Dev Group. All rights reserved.
//

#import "WebApiController.h"

@interface WebApiController ()
    
    @end

@implementation WebApiController
    @synthesize finalimage;
    
+ (Reachability *) checkServerConnection {
    return [Reachability reachabilityWithHostName:SERVERNAME];
}
    
- (void)callAPI_POST:(NSString *)apiName andParams:(NSDictionary *)params SuccessCallback:(SEL)successCallback andDelegate:delegateObj {
    //NSLog(@"API MEthod call");
    TSearchInfo *searchInfo = [[TSearchInfo alloc] initWithWebAPIName:apiName];
    for (NSString *key in params) {
        NSString *value = [params objectForKey:key];
        //need Base-64????
        [searchInfo pushSearchField:key andValue:value];
    }
    
    
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:[searchInfo generateAsUrlParam] andAlias:alias_api];
    //NSLog(@"API Url : %@",[SERVERNAME stringByAppendingFormat:@"%@",apiName ]);
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    [webApi setParameter:[searchInfo generateParam]];
    [webApi runApiSuccessCallback:successCallback inDelegate:delegateObj];
}
    
- (void)callAPIWithImage:(NSString *)apiName WithImageParameter:(NSMutableDictionary *)Iparameter WithoutImageParameter:(NSMutableDictionary *)WIparameter strImageName:(NSString *)strImageName SuccessCallback:(SEL)successCallback andDelegate:delegateObj{
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:apiName andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    [webApi ForImageUploding_runApiSuccessCallback:successCallback inDelegate:delegateObj WithImageParameter:Iparameter WithoutImageParameter:WIparameter strImageParameter:strImageName];
}
    
- (void)callAPIWithImage:(NSString *)apiName WithImageParameter:(NSMutableDictionary *)Iparameter WithoutImageParameter:(NSMutableDictionary *)WIparameter SuccessCallback:(SEL)successCallback andDelegate:delegateObj{
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:apiName andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    webApi.imagename = finalimage;
    [webApi ForImageUploding_runApiSuccessCallback:successCallback inDelegate:delegateObj WithImageParameter:Iparameter WithoutImageParameter:WIparameter];
}

- (void)callAPIWithDocument:(NSString *)apiName WithImageParameter:(NSMutableDictionary *)Iparameter  strDocPath:(NSString *)strDocPath SuccessCallback:(SEL)successCallback andDelegate:delegateObj{
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:apiName andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    [webApi ForDocUploding_runApiSuccessCallback:successCallback inDelegate:delegateObj WithImageParameter:Iparameter strPathParameter:strDocPath];
}
    
- (void)callAPI_GET:(NSString *)apiName andParams:(NSDictionary *)params SuccessCallback:(SEL)successCallback andDelegate:delegateObj {
    TSearchInfo *searchInfo = [[TSearchInfo alloc] initWithWebAPIName:apiName];
    for (NSString *key in params) {
        NSString *value = [params objectForKey:key];
        //need Base-64????
        [searchInfo pushSearchField:key andValue:value];
    }
    
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:[SERVERNAME stringByAppendingFormat:@"%@",[searchInfo generateAsUrlParam]] andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    
    if(_isAppendServerURL)
    {
        webApi = [[TWebApi alloc] initWithFullApiName:apiName andAlias:alias_api];
        [webApi setMethodName:apiName];
        
    }
    
    [webApi setParameter:[searchInfo generateParam]];
    [webApi GET_runApiSuccessCallback:successCallback inDelegate:delegateObj];
}

- (void)callAPI_GET_other:(NSString *)apiName andParams:(NSDictionary *)params SuccessCallback:(SEL)successCallback andDelegate:delegateObj {
    TSearchInfo *searchInfo = [[TSearchInfo alloc] initWithWebAPIName:apiName];
    for (NSString *key in params) {
        NSString *value = [params objectForKey:key];
        //need Base-64????
        [searchInfo pushSearchField:key andValue:value];
    }
    
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:[OTHER stringByAppendingFormat:@"%@",[searchInfo generateAsUrlParam]] andAlias:alias_api];
    [webApi setMethodName:[OTHER stringByAppendingFormat:@"%@",apiName ]];
    
    if(_isAppendServerURL)
    {
        webApi = [[TWebApi alloc] initWithFullApiName:apiName andAlias:alias_api];
        [webApi setMethodName:apiName];
        
    }
    
    [webApi setParameter:[searchInfo generateParam]];
    [webApi GET_runApiSuccessCallback:successCallback inDelegate:delegateObj];
}




- (void)callAPIWithVideo:(NSString *)apiName WithImageParameter:(NSMutableDictionary *)Iparameter WithoutImageParameter:(NSMutableDictionary *)WIparameter SuccessCallback:(SEL)successCallback andDelegate:delegateObj
    {
        NSString *alias_api=[[NSString alloc] initWithString:apiName];
        TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:apiName andAlias:alias_api];
        [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
        [webApi ForVideoUploding_runApiSuccessCallback:successCallback inDelegate:delegateObj WithImageParameter:Iparameter WithoutImageParameter:WIparameter];
    }
    
    
- (void)callAPI_POST_RAW:(NSString *)apiName andParams:(NSString *)params SuccessCallback:(SEL)successCallback andDelegate:delegateObj {
    TSearchInfo *searchInfo = [[TSearchInfo alloc] initWithWebAPIName:apiName];
    
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:[searchInfo generateAsUrlParam] andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    [webApi setParameter:[searchInfo generateParam]];
    [webApi runRawApiSuccessCallback:successCallback strParameter:params inDelegate:delegateObj];
}

- (void)callAPI_PUT_RAW:(NSString *)apiName andParams:(NSString *)params SuccessCallback:(SEL)successCallback andDelegate:delegateObj {
    TSearchInfo *searchInfo = [[TSearchInfo alloc] initWithWebAPIName:apiName];
    
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:[searchInfo generateAsUrlParam] andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    [webApi setParameter:[searchInfo generateParam]];
    [webApi runRawApiSuccessCallback_PUT:successCallback strParameter:params inDelegate:delegateObj];
}


- (void)callAPI_DELETE_RAW:(NSString *)apiName andParams:(NSString *)params SuccessCallback:(SEL)successCallback andDelegate:delegateObj {
    TSearchInfo *searchInfo = [[TSearchInfo alloc] initWithWebAPIName:apiName];
    
    NSString *alias_api=[[NSString alloc] initWithString:apiName];
    
    TWebApi *webApi = [[TWebApi alloc] initWithFullApiName:[searchInfo generateAsUrlParam] andAlias:alias_api];
    [webApi setMethodName:[SERVERNAME stringByAppendingFormat:@"%@",apiName ]];
    [webApi setParameter:[searchInfo generateParam]];
    [webApi runRawApiSuccessCallback_DELETE:successCallback strParameter:params inDelegate:delegateObj];
}

@end
