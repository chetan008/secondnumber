//
//  CommonClass.h
//  PainRemedy
//
//  Created by Ratnakala_41 on 5/22/15.
//  Copyright (c) 2015 ratnakala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface CommonClass : NSObject

+(NSString *) getDocumentPath;
-(NSString *) getDocumentPath;

+(BOOL) saveFileWithFileName: (NSString *)filename fileData:(NSData *)data;
-(BOOL) saveFileWithFileName: (NSString *)filename fileData:(NSData *)data;

+ (UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;
+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize;
@end
