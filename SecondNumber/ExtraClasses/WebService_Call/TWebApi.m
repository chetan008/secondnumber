//
//  TWebApi.m
//  Chirag Lukhi
//
//  Created by Lanetteam on 9/7/12.
//  Copyright (c) 2012 HongYing Dev Group. All rights reserved.
//


#import "TWebApi.h"
#import "Reachability.h"
#import "DejalActivityView.h"
#import "CommonUtility.h"
#import "Constant.h"
#import "CryptoJS_Objc.h"
#import <MobileCoreServices/MobileCoreServices.h>

@implementation TWebApi
    @synthesize MethodName,Parameter,imagename;
-(TWebApi *) initWithFullApiName:(NSString *)fullApiName andAlias:(NSString *)apiAlias {
    NSString *_apiAlias;
    NSString *_fullApiName;
    if (apiAlias) _apiAlias = apiAlias;
    else _apiAlias = @"";
    
    if (fullApiName) _fullApiName = fullApiName;
    else _fullApiName = @"";
    
    m_apiAlias = [[NSString alloc] initWithString:_apiAlias];
    m_fullApiName = [[NSString alloc] initWithString:_fullApiName];
    return self;
}
    
-(void)runRawApiSuccessCallback:(SEL)successSelector strParameter:(NSString *)strParameter inDelegate:delegateObj {
    @try {
        
        if (m_data) {
            m_data = nil;
        }
        
        NSURL *_url = [NSURL URLWithString:[MethodName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:300];
        //        [_request setValue:@"application/raw" forHTTPHeaderField:@"Content-Type"];
        [_request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
        // [_request addValue:@"text/plain; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        if([MethodName.lastPathComponent isEqualToString:@"login"])
        {
            
        }
        else
        {
            [_request setValue:[Default valueForKey:AUTH_TOKEN] forHTTPHeaderField:@"authToken"];
        }
        
//        if([MethodName.lastPathComponent isEqualToString:@"available"])
//        {
//
//            [_request setValue:[Default valueForKey:USER_ID] forHTTPHeaderField:@"id"];
//        }
        
        if ([MethodName containsString:@"webapi"] && ![MethodName containsString:@"googlesignin"]) {
            [_request setValue:[Default valueForKey:USER_ID] forHTTPHeaderField:@"id"];
        }
       
        [_request addValue:[[NSString alloc] initWithFormat:@"%lu",(unsigned long)[strParameter length]] forHTTPHeaderField:@"Content-length"];
        [_request setHTTPMethod:@"POST"];
        
        NSLog(@"Encryption API URL :: %@",_url);
       
        
        if ([MethodName containsString:@"webapi/googlesignin"])
        {
            //encrypt request param
            CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
            NSString *encryptedStr = [cryptojs encrypt:strParameter password:encryptionKey];

            NSDictionary *passDict1 = @{@"data":encryptedStr
            };

            NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:nil];
                [_request setHTTPBody:jsonData1];
                NSLog(@"Encrypted param :: %@",jsonData1);
            }
        else
        {
            [_request setHTTPBody:[strParameter dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSLog(@"final param new :: %@",strParameter);
        
      /* Encryption commented
       
       if ([Default boolForKey:kencryptionInMobile] == true) {
            [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
            
            //   encrypt request param
               CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
               NSString *encryptedStr = [cryptojs encrypt:strParameter password:encryptionKey];

               NSDictionary *passDict1 = @{@"encryptedch":encryptedStr
               };

               NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                                  options:NSJSONWritingPrettyPrinted
                                                                    error:nil];

           [_request setHTTPBody:jsonData1];
        }
        else
        {*/
              // [_request setHTTPBody:[strParameter dataUsingEncoding:NSUTF8StringEncoding]];
       // }
        
    

        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
        }
    }
    @catch (NSException *exception) {
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        [DejalBezelActivityView removeViewAnimated:YES];
    }
    @finally {
    }
    
}

-(void)runRawApiSuccessCallback_PUT:(SEL)successSelector strParameter:(NSString *)strParameter inDelegate:delegateObj {
    @try {
        
        if (m_data) {
            m_data = nil;
        }
        
        NSURL *_url = [NSURL URLWithString:[MethodName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:300];
        //        [_request setValue:@"application/raw" forHTTPHeaderField:@"Content-Type"];
        [_request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSLog(@"Encryption API URL :: %@",_url);

        // [_request addValue:@"text/plain; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        if([MethodName.lastPathComponent isEqualToString:@"login"])
        {
            
        }
        else
        {
            [_request setValue:[Default valueForKey:AUTH_TOKEN] forHTTPHeaderField:@"authToken"];
        }
        [_request addValue:[[NSString alloc] initWithFormat:@"%lu",(unsigned long)[strParameter length]] forHTTPHeaderField:@"Content-length"];
        [_request setHTTPMethod:@"PUT"];
        
      

        NSLog(@"PUT API body ::%@",strParameter);
        
      /* Encryption commented
       if ([Default boolForKey:kencryptionInMobile] == true) {
            
        [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
            
        //encrypt request param
        CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
        NSString *encryptedStr = [cryptojs encrypt:strParameter password:encryptionKey];

        NSDictionary *passDict1 = @{@"encryptedch":encryptedStr
        };

        NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                           options:NSJSONWritingPrettyPrinted
                                                        error:nil];
            
            [_request setHTTPBody:jsonData1];
        }
        else
        {*/
             [_request setHTTPBody:[strParameter dataUsingEncoding:NSUTF8StringEncoding]];
      //  }
        
      
        
       
        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            
            [respData setObject:@"No Network Found!" forKey:@"data"];
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
        }
    }
    @catch (NSException *exception) {
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        [DejalBezelActivityView removeViewAnimated:YES];
    }
    @finally {
    }
    
}

-(void)runRawApiSuccessCallback_DELETE:(SEL)successSelector strParameter:(NSString *)strParameter inDelegate:delegateObj {
    @try {
        
        if (m_data) {
            m_data = nil;
        }
        
        NSURL *_url = [NSURL URLWithString:[MethodName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:300];
        //        [_request setValue:@"application/raw" forHTTPHeaderField:@"Content-Type"];
        [_request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSLog(@"Encryption API URL :: %@",_url);

        // [_request addValue:@"text/plain; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        if([MethodName.lastPathComponent isEqualToString:@"login"])
        {
            
        }
        else
        {
            [_request setValue:[Default valueForKey:AUTH_TOKEN] forHTTPHeaderField:@"authToken"];
        }
        [_request addValue:[[NSString alloc] initWithFormat:@"%lu",(unsigned long)[strParameter length]] forHTTPHeaderField:@"Content-length"];
        [_request setHTTPMethod:@"DELETE"];
        
       /* Encryption commented
        if ([Default boolForKey:kencryptionInMobile] == true) {
     
            [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
            
        //encrypt request param
        CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
        NSString *encryptedStr = [cryptojs encrypt:strParameter password:encryptionKey];

        NSDictionary *passDict1 = @{@"encryptedch":encryptedStr
        };

        NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                           options:NSJSONWritingPrettyPrinted
                                                        error:nil];
        
  
        
        [_request setHTTPBody:jsonData1];
        }
        else
        {*/
             [_request setHTTPBody:[strParameter dataUsingEncoding:NSUTF8StringEncoding]];
      //  }
        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
        }
    }
    @catch (NSException *exception) {
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        [DejalBezelActivityView removeViewAnimated:YES];
    }
    @finally {
    }
    
}

-(void)runApiSuccessCallback:(SEL)successSelector inDelegate:delegateObj {
    @try {
        
        if (m_data) {
            m_data = nil;
        }
        NSURL *_url = [NSURL URLWithString:[MethodName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:350];
        [_request addValue:[[NSString alloc] initWithFormat:@"%lu",(unsigned long)[Parameter length]] forHTTPHeaderField:@"Content-length"];
        if([MethodName.lastPathComponent isEqualToString:@"login"])
        {
            
        }
        else
        {
            [_request setValue:[Default valueForKey:AUTH_TOKEN] forHTTPHeaderField:@"authToken"];
        }

        if ([MethodName.lastPathComponent isEqualToString:@"readinbox"]  ) {
            
            [_request setValue:@"ios" forHTTPHeaderField:@"x-ph-devicetype"];
        }
        
      /*  if ([MethodName.lastPathComponent isEqualToString:@"webapi/googlesignin"])
        {
          
            //encrypt request param
            CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
            NSString *encryptedStr = [cryptojs encrypt:Parameter password:encryptionKey];

            NSDictionary *passDict1 = @{@"data":encryptedStr
            };

            NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:nil];
                [_request setHTTPBody:jsonData1];
            }
        else
        {*/
            [_request setHTTPBody:[Parameter dataUsingEncoding:NSUTF8StringEncoding]];
      //  }
        
        
       
        [_request setHTTPMethod:@"POST"];
        NSLog(@"Encryption API URL :: %@",_url);
 
        
       /* Encryption commented
        if ([Default boolForKey:kencryptionInMobile] == true) {
            
        [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];


        //encrypt request param
        CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
        NSString *encryptedStr = [cryptojs encrypt:Parameter password:encryptionKey];

        NSDictionary *passDict1 = @{@"encryptedch":encryptedStr
        };

        NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:passDict1
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
            [_request setHTTPBody:jsonData1];
        }
        else
        {*/
//                 [_request setHTTPBody:[Parameter dataUsingEncoding:NSUTF8StringEncoding]];
       // }
        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
        }
    }
    @catch (NSException *exception) {
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        [DejalBezelActivityView removeViewAnimated:YES];
    }
    @finally {
    }
    
}
-(void)GET_runApiSuccessCallback:(SEL)successSelector inDelegate:delegateObj {
    
    @try {
        if (m_data) {
            m_data = nil;
        }
        
       

        //NSLog(@"method:: %@",MethodName.pathComponents);
        NSArray *arr = MethodName.pathComponents;
        NSString *methodnameStr1=@"";
        if (arr.count > 4) {
            methodnameStr1 = [arr objectAtIndex:4];
        }



        NSURL *_url;
        if ([methodnameStr1 isEqualToString:@"getSmsLogWithLimitSkip"]) {

//            [[[NSCharacterSet URLQueryAllowedCharacterSet] mutableCopy] addCharactersInString:@"+"];
            
            _url = [NSURL URLWithString:[[MethodName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"]];
        }
        else
        {
         _url = [NSURL URLWithString:[MethodName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
        }
        
        
        NSLog(@"Encryption API URL :: %@",_url);

        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        
        

       // NSLog(@"papapi url %@",_url);
        [_request setTimeoutInterval:300];
        [_request setValue:[Default valueForKey:AUTH_TOKEN] forHTTPHeaderField:@"authToken"];
        
//        if([MethodName.lastPathComponent isEqualToString:@"countries"])
//        {
//
//            [_request setValue:[Default valueForKey:USER_ID] forHTTPHeaderField:@"id"];
//        }
        
        if ([MethodName containsString:@"webapi"]) {
            [_request setValue:[Default valueForKey:USER_ID] forHTTPHeaderField:@"id"];
        }
        
        
        NSString *methodnameStr = MethodName.lastPathComponent;
       if ([MethodName.lastPathComponent containsString:@"?"])
       {
           NSArray *temparr = [MethodName.lastPathComponent componentsSeparatedByString:@"?"];
           if (temparr.count>0) {
               methodnameStr = [temparr objectAtIndex:0];
           }
       }
        if ([methodnameStr isEqualToString:@"inbox"] || [methodnameStr isEqualToString:@"callLog"] ||  [methodnameStr isEqualToString:@"callLogMobile"] ) {
            
            [_request setValue:@"ios" forHTTPHeaderField:@"x-ph-devicetype"];
        }
        if ([MethodName.lastPathComponent isEqualToString:@"numbers"]  ) {
            
            [_request setValue:@"ios" forHTTPHeaderField:@"device"];
        }
        
        
        
        [_request setHTTPMethod:@"GET"];
        
      /* Encryption commented
       if ([Default boolForKey:kencryptionInMobile] == true) {
            
            [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
        }*/

        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
            
        }
    }
    @catch (NSException *exception) {
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        [DejalBezelActivityView removeViewAnimated:YES];
    }
    @finally {
    }
}



    
-(void)ForImageUploding_runApiSuccessCallback:(SEL)successSelector  inDelegate:delegateObj WithImageParameter:(NSMutableDictionary *)Iparameter WithoutImageParameter:(NSMutableDictionary *)WIparameter strImageParameter:(NSString *)strImageParameter{
    @try {
        
        if (m_data) {
            m_data = nil;
        }
        NSURL *_url = [NSURL URLWithString:MethodName];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:239];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [_request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (NSString *key in WIparameter) {
            NSString *value = [WIparameter objectForKey:key];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        //        NSInteger i=0;
        //        for (NSString *key in Iparameter) {
        NSObject *value = [Iparameter objectForKey:strImageParameter];
        
        //Image
        //            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.png%d\"\r\n",key,i] dataUsingEncoding:NSUTF8StringEncoding]];
        
        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\".png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\".png\"\r\n",strImageParameter] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:(NSData *)value]];
        //            i++;
        //        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //        //NSLog(@"%@",body);
        
        //[_request addValue:[[NSString alloc] initWithFormat:@"%d",[body length]] forHTTPHeaderField:@"Content-Length"];
        [_request setHTTPMethod:@"POST"];
        
      /* Encryption commented
       if ([Default boolForKey:kencryptionInMobile] == true) {
            
        [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
        }
*/
        [_request setHTTPBody:body];
        
        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            
            //            //NSLog(@"INTERNET CONNECTION NOT AVAILABLE");
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
            
            //            [delegateObj performSelector:m_failCallback withObject:m_apiAlias withObject:respData];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
            
        }
        
    }
    @catch (NSException *exception) {
        //        //NSLog(@"ERROR IN ForImageUploding_runApiSuccessCallback WEBAPI");
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
    }
    @finally
    {
        
    }
}
    
-(void)ForImageUploding_runApiSuccessCallback:(SEL)successSelector  inDelegate:delegateObj WithImageParameter:(NSMutableDictionary *)Iparameter WithoutImageParameter:(NSMutableDictionary *)WIparameter {
    @try {
        
        if (m_data) {
            m_data = nil;
        }
        NSURL *_url = [NSURL URLWithString:MethodName];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:239];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [_request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (NSString *key in WIparameter) {
            NSString *value = [WIparameter objectForKey:key];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        //        NSInteger i=0;
        //        for (NSString *key in Iparameter) {
        NSObject *value = [Iparameter objectForKey:@"photo"];
        
        //Image
        //            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.png%d\"\r\n",key,i] dataUsingEncoding:NSUTF8StringEncoding]];
        
        //        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\".png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"%@.png\"\r\n",imagename] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:(NSData *)value]];
        //            i++;
        //        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //        //NSLog(@"%@",body);
        
        //[_request addValue:[[NSString alloc] initWithFormat:@"%d",[body length]] forHTTPHeaderField:@"Content-Length"];
        [_request setHTTPMethod:@"POST"];
        
        
      /*
       Encryption commented
       if ([Default boolForKey:kencryptionInMobile] == true) {
            
        [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
        }*/

        [_request setHTTPBody:body];
        
        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            
            //            //NSLog(@"INTERNET CONNECTION NOT AVAILABLE");
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
            
            //            [delegateObj performSelector:m_failCallback withObject:m_apiAlias withObject:respData];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
            
        }
        
    }
    @catch (NSException *exception) {
        //        //NSLog(@"ERROR IN ForImageUploding_runApiSuccessCallback WEBAPI");
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
    }
    @finally {
    }
}

-(void)ForDocUploding_runApiSuccessCallback:(SEL)successSelector inDelegate:(id)delegateObj WithImageParameter:(NSMutableDictionary *)Iparameter strPathParameter:(NSString *)strPath
{
@try {
    
    if (m_data) {
        m_data = nil;
    }
    NSURL *_url = [NSURL URLWithString:MethodName];
    NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
    [_request setTimeoutInterval:239];
    
    //NSString *boundary = [self generateBoundaryString];
    NSString *boundary = @"---------------------------WebKitFormBoundarywd3ZwVIqsIbqoaUB";

    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [_request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    [_request setValue:[Default valueForKey:AUTH_TOKEN] forHTTPHeaderField:@"authToken"];
    
    if ([MethodName containsString:@"webapi"]) {
        [_request setValue:[Default valueForKey:USER_ID] forHTTPHeaderField:@"id"];
    }
    
    NSMutableData *httpBody = [NSMutableData data];
    
    [Iparameter enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
        }];
    
    NSString *filename  = [strPath lastPathComponent];
    NSData   *data      = [NSData dataWithContentsOfFile:strPath];
    NSString *mimetype  = [self mimeTypeForPath:strPath];

    [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", filename, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:data];
    [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
   /* [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    for (NSString *key in WIparameter) {
        NSString *value = [WIparameter objectForKey:key];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    NSObject *value = [Iparameter objectForKey:strImageParameter];
    
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\".png\"\r\n",strImageParameter] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:(NSData *)value]];
   
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [_request setHTTPBody:body];
    */
   
    NSLog(@"path :: %@",strPath);
    NSLog(@"httpbody :: %@",httpBody);
    
    [_request setHTTPBody:httpBody];
    [_request setHTTPMethod:@"POST"];
    
  /* Encryption commented
   if ([Default boolForKey:kencryptionInMobile] == true) {
        
    [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
    }
*/
   
    
    
    NSString *netStr = [self checkNetworkConnectivity];
    if([netStr isEqualToString:@"NoAccess"])
    {
        
        NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
        [respData setObject:@"No Network Found!" forKey:@"data"];
        
        //            //NSLog(@"INTERNET CONNECTION NOT AVAILABLE");
        [DejalBezelActivityView removeViewAnimated:YES];
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
        
        //            [delegateObj performSelector:m_failCallback withObject:m_apiAlias withObject:respData];
    }
    else
    {
        m_data = [[NSMutableData alloc] init];
        m_delegate = delegateObj;
        m_successCallback = successSelector;
        [self webserviceResponse:_request];
        
    }
    
}
@catch (NSException *exception) {
    //        //NSLog(@"ERROR IN ForImageUploding_runApiSuccessCallback WEBAPI");
    // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
}
@finally
{
    
}
}

- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework

    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);

    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);

    CFRelease(UTI);

    return mimetype;
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}
-(void)ForVideoUploding_runApiSuccessCallback:(SEL)successSelector  inDelegate:delegateObj WithImageParameter:(NSMutableDictionary *)Iparameter WithoutImageParameter:(NSMutableDictionary *)WIparameter{
    @try {
        
        //        //NSLog(@"WEBAPI: Running web api: %@", MethodName);
        if (m_data) {
            m_data = nil;
        }
        
        NSURL *_url = [NSURL URLWithString:MethodName];
        NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:_url];
        [_request setTimeoutInterval:239];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [_request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (NSString *key in WIparameter) {
            NSString *value = [WIparameter objectForKey:key];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@",key, value] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        //        NSInteger i=0;
        //        for (NSString *key in Iparameter) {
        NSObject *value = [Iparameter objectForKey:@"livestream"];
        
        //Image
        //            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.png%d\"\r\n",key,i] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"livestream\"; filename=\".mp4\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:(NSData *)value]];
        //            i++;
        //        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //        //NSLog(@"%@",body);
        
        //[_request addValue:[[NSString alloc] initWithFormat:@"%d",[body length]] forHTTPHeaderField:@"Content-Length"];
        [_request setHTTPMethod:@"POST"];
        
       /* Encryption commented
        if ([Default boolForKey:kencryptionInMobile] == true) {
            
        [_request setValue:@"true" forHTTPHeaderField:@"encryptedch"];
        }*/

        [_request setHTTPBody:body];
        
        
        NSString *netStr = [self checkNetworkConnectivity];
        if([netStr isEqualToString:@"NoAccess"])
        {
            
            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
            [respData setObject:@"No Network Found!" forKey:@"data"];
            //            //NSLog(@"INTERNET CONNECTION NOT AVAILABLE");
            [DejalBezelActivityView removeViewAnimated:YES];
            // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
            //            [delegateObj performSelector:m_failCallback withObject:m_apiAlias withObject:respData];
        }
        else
        {
            m_data = [[NSMutableData alloc] init];
            m_delegate = delegateObj;
            m_successCallback = successSelector;
            [self webserviceResponse:_request];
        }
        
    }
    @catch (NSException *exception) {
        
        // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
    }
    @finally {
    }
}
    
-(void) alloc {
    m_con = nil;
    m_successCallback = nil;
    m_failCallback = nil;
    m_apiAlias = nil;
    m_fullApiName = nil;
    m_delegate = nil;
    m_data = nil;
}
    
-(void)webserviceResponse:(NSMutableURLRequest *)_request
    {
        NSURLSessionTask *task1 = [[NSURLSession sharedSession] dataTaskWithRequest:_request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            if (error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                [DejalBezelActivityView removeViewAnimated:YES];
            //  [CommonUtility showMessage:@"Could not connect to the server" withTitle:@"Message"];
                });
                //                                       if (!m_successCallback || !m_delegate) return;
                NSLog(@"error in API call : %@",error);
                
            }else
            {
                @try {
                NSHTTPURLResponse *httpResponse = nil;
                if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                        
                    httpResponse = (NSHTTPURLResponse *)response;
                }
                
                    
               /* Encryption commented
                    if ([Default boolForKey:kencryptionInMobile] == true) {
                        
                        self->m_statusCode = [httpResponse statusCode];
                                                    
                        if ([httpResponse statusCode] == 1000) {
                                self->m_error = NO;
                        } else {
                                self->m_error = YES;
                        }
                        
                        [self->m_data appendData:data];
                        
                        if (!self->m_successCallback || !self->m_delegate)
                            return;
                        
                       
                        
                        NSError *err;
                        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:self->m_data options:NSJSONReadingMutableContainers error:&err];
                        
                        CryptoJS_Objc *cryptojs = [[CryptoJS_Objc alloc]init];
                        NSString *decryptedStr = [cryptojs decrypt:[jsonDict valueForKey:@"encryptedch"] password:encryptionKey];


                       // NSDictionary *jsondictdec = [NSJSONSerialization JSONObjectWithData:[decryptedStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
                        
                        NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
                        
                        [respData setObject:[NSNumber numberWithInt:self->m_statusCode] forKey:@"code"];
                        
                        [respData setObject:decryptedStr forKey:@"data"];
                        
                        NSData* data = [[respData objectForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                    
                   
                                                   
                       dispatch_async(dispatch_get_main_queue(), ^{
                            if (!self->m_error) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                                        
                                [self->m_delegate performSelector:self->m_successCallback withObject:[NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]] withObject:data];
    #pragma clang diagnostic pop
                                                     
                            } else {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                                 
                                //NSString *prestring = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //                                                       //NSLog(@"pre print : %@",prestring);
                                                           
                                                     
                                [self->m_delegate performSelector:self->m_successCallback withObject:[NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]] withObject:data];
                                                           
                                                           
    #pragma clang diagnostic pop
                                                  
                            }
                            
                        });
                        
                    }
                    else
                    {*/
                        self->m_statusCode = [httpResponse statusCode];
                                                    
                        if ([httpResponse statusCode] == 1000) {
                                self->m_error = NO;
                        } else {
                                self->m_error = YES;
                        }
                        
                            [self->m_data appendData:data];
                            
                            if (!self->m_successCallback || !self->m_delegate)
                                return;
                                                       
                            NSMutableDictionary *respData = [[NSMutableDictionary alloc] init];
                            [respData setObject:[NSNumber numberWithInt:self->m_statusCode] forKey:@"code"];
                                                       
                            NSString* datastring = [[NSString alloc] initWithData:self->m_data encoding:NSUTF8StringEncoding] ;
                                                       
                            [respData setObject:datastring forKey:@"data"];
                                                       
                        NSData* data = [[respData objectForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                                                       
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (!self->m_error) {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                                            
                                    [self->m_delegate performSelector:self->m_successCallback withObject:[NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]] withObject:data];
        #pragma clang diagnostic pop
                                                         
                                } else {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                                     
                                    //NSString *prestring = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //                                                       //NSLog(@"pre print : %@",prestring);
                                                               
                                                         
                                    [self->m_delegate performSelector:self->m_successCallback withObject:[NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]] withObject:data];
                                                               
                                                               
        #pragma clang diagnostic pop
                                                      
                                }
                                
                            });
                  //  }
                                    
                /*    dispatch_async(dispatch_get_main_queue(), ^{
                        if (!self->m_error) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                                    
                            [self->m_delegate performSelector:self->m_successCallback withObject:[NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]] withObject:data];
#pragma clang diagnostic pop
                                                 
                        } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                             
                            //NSString *prestring = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                                                       //NSLog(@"pre print : %@",prestring);
                                                       
                                                 
                            [self->m_delegate performSelector:self->m_successCallback withObject:[NSString stringWithFormat:@"%ld",(long)[httpResponse statusCode]] withObject:data];
                                                       
                                                       
#pragma clang diagnostic pop
                                              
                        }
                        
                    });
                         */
                    
                }
                
                @catch (NSException *exception) {
                                               
                                               //NSLog(@"\n exception: \n %@",exception.description);
                                          
                    dispatch_async(dispatch_get_main_queue(), ^{
                                               
                        [DejalBezelActivityView removeViewAnimated:YES];
                                                   // [CommonUtility showMessage:@"Internet connection not available" withTitle:@"Message"];
                                       
                    });
                }
                
                @finally {
                                    
                }
            }
        }];
        
        [task1 resume];
    }
-(NSString *)checkNetworkConnectivity
    {
        NSString *networkValue;
        Reachability *rc = [Reachability reachabilityWithHostName:@"www.google.com"];
        NetworkStatus internetStatus = [rc currentReachabilityStatus];
        
        if(internetStatus==0)
        {
            networkValue = @"NoAccess";
        }
        else if(internetStatus==1)
        {
            networkValue = @"ReachableViaWiFi";
            
        } else if(internetStatus==2)
        {
            networkValue = @"ReachableViaWWAN";
        }
        else  if(internetStatus>2)
        {
            networkValue = @"Reachable";
        }
        return networkValue;
    }
    
    @end
