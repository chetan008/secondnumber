//
//  CryptoJS_Objc.m
//  callhippolin
//
//  Created by Apple on 17/05/21.
//  Copyright © 2021 Admin. All rights reserved.
//

#import "CryptoJS_Objc.h"
//#import <JavaScriptCore/JavaScriptCore.h>

@import JavaScriptCore;

@implementation CryptoJS_Objc

JSValue *encryptFunction;
JSValue *decryptFunction;

-(id)init
{
    //
    
    
    JSContext *cryptoJScontext = [[JSContext alloc]init];
    NSString *cryptoJSpath = [[NSBundle mainBundle] pathForResource:@"aes" ofType:@"js"];
    
    if (cryptoJSpath != nil) {
        
        NSString *cryptoJS = [NSString stringWithContentsOfFile:cryptoJSpath encoding:NSUTF8StringEncoding error:nil];
        
        [cryptoJScontext evaluateScript:cryptoJS];
        
        encryptFunction = [cryptoJScontext objectForKeyedSubscript:@"encrypt"];
        
        decryptFunction = [cryptoJScontext objectForKeyedSubscript:@"decrypt"];
    }
    
    return self;
    
}

-(NSString *)encrypt:(NSString *)message password:(NSString *)password
{
    JSValue *encryptedStr = [encryptFunction callWithArguments:[NSArray arrayWithObjects:message,password,nil]];
    return [encryptedStr toString];
}

-(NSString *)decrypt:(NSString *)message password:(NSString *)password
{
    JSValue *decryptedStr = [decryptFunction callWithArguments:[NSArray arrayWithObjects:message,password,nil]];
    return [decryptedStr toString];
}

//private var cryptoJScontext = JSContext()
//
//open class CryptoJS{
//
//   @objc open class AES: CryptoJS{
//
//    @objc fileprivate var encryptFunction: JSValue!
//    @objc  fileprivate var decryptFunction: JSValue!
//
//        override init(){
//            super.init()
//
//            // Retrieve the content of aes.js
//            let cryptoJSpath = Bundle.main.path(forResource: "aes", ofType: "js")
//
//            if(( cryptoJSpath ) != nil){
//                do {
//                    let cryptoJS = try String(contentsOfFile: cryptoJSpath!, encoding: String.Encoding.utf8)
//                    print("Loaded aes.js")
//
//                    // Evaluate aes.js
//                    _ = cryptoJScontext?.evaluateScript(cryptoJS)
//
//                    // Reference functions
//                    encryptFunction = cryptoJScontext?.objectForKeyedSubscript("encrypt")
//                    decryptFunction = cryptoJScontext?.objectForKeyedSubscript("decrypt")
//                }
//                catch {
//                    print("Unable to load aes.js")
//                }
//            }else{
//                print("Unable to find aes.js")
//            }
//
//        }
//
//    @objc   open func encrypt(_ message: String, password: String,options: Any?=nil)->String {
//            if let unwrappedOptions: Any = options {
//                return "\(encryptFunction.call(withArguments: [message, password, unwrappedOptions])!)"
//            }else{
//                return "\(encryptFunction.call(withArguments: [message, password])!)"
//            }
//        }
//    @objc  open func decrypt(_ message: String, password: String,options: Any?=nil)->String {
//            if let unwrappedOptions: Any = options {
//                return "\(decryptFunction.call(withArguments: [message, password, unwrappedOptions])!)"
//            }else{
//                return "\(decryptFunction.call(withArguments: [message, password])!)"
//            }
//        }
//
//    }
@end
