//
//  CryptoJS_Objc.h
//  callhippolin
//
//  Created by Apple on 17/05/21.
//  Copyright © 2021 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CryptoJS_Objc : NSObject

-(NSString *)encrypt:(NSString *)message password:(NSString *)password;
-(NSString *)decrypt:(NSString *)message password:(NSString *)password;
@end

NS_ASSUME_NONNULL_END
