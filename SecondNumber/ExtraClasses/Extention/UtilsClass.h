//
//  UtilsClass.h
//  CallHippoV2_LIN
//
//  Created by Hippo on 29/04/19.
//  Copyright © 2019 Hippo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CallLogsModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface UtilsClass : NSObject
{
     
}
//+ (void)bottomRoundedView:(Float32)desiredCurve view:(UIView *)viw;

@property (nonatomic, strong) NSString *CallStatus;
@property (nonatomic, strong) NSString *ContactName;
@property (nonatomic, strong) NSString *ContactNumber;

+ (BOOL)validateEmail:(NSString *)theEmail;
+ (BOOL)isNetworkAvailable;
+ (void)showAlert:(NSString *)msg contro:(UIViewController *)controller;
+ (void)showAlert:(NSString *)msg view:(UIView *)view;
+ (void)showAlert:(NSString *)msg title:(NSString *)title contro:(UIViewController *)controller;
+(void)keypadtextset:(UIButton *)button text:(NSString *)text tot:(NSInteger)tot;
+(void)textfiled_shadow_boder:(UITextField *)txt;

+(void)view_shadow_boder:(UIView *)view;
+(void)view_shadow_boder_custom:(UIView *)view;

+(void)image_round_boder:(UIImageView *)img;
+(void)SetTextFieldBottomBorder :(UITextField *)textField;
+(void)SetLabelBottomBorder :(UILabel *)label;
    
+ (void)makeAlertInWindow:(NSString*)message;
+(void)view_bottom_round_edge:(UIView *)viw desiredCurve:(NSInteger)desiredCurve;
+(void)view_navigation_title:(UIViewController *)viw title:(NSString*)title color:(UIColor*)color;
+(NSString*)phonenumbercheck:(NSString *)number;
+ (BOOL)isValidNumber:(NSString*)text;
+ (BOOL)isValidNumber_withoutplus:(NSString*)text;
+(void)make_outgoing_call:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact;
+(void)make_outgoing_call1:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact;
+(void)make_outgoing_call_warmtransfer:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact;
+(void)make_outgoing_call_validate:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact;
+(void)make_outgoing_call_validate1:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact;
+(void)button_shadow_boder:(UIButton *)btn;
+(void)contact_save_in_callhippo:(NSString *)contact_name contact_number:(NSString *)contact_number;
+(NSString*)deviceName;
+ (void)makeToast:(NSString*)toastMsg;
+(void)callinge_from_number_check;
//+(void)logoutUser:(UIViewController *)viewcontroler;
+(void)logoutUser:(UIViewController *)viewcontroler error:(NSString *)error showAlert:(BOOL)showAlert;
+(void)twilio_access_token_get;
+(void)twilio_token_registration;
+(NSString *)get_masked_number:(NSString *)number;
+ (void)makeToast:(NSString*)toastMsg inView:(UIView *)view;
//+(void)outboundc2cAPICall:(NSString *)toNum provider:(NSString *)provider phoneNum:(NSString *)phoneNum;
@end
NS_ASSUME_NONNULL_END
