#import "GlobalData.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "WebApiController.h"
#import "Lin_Utility.h"
#import "UtilsClass.h"

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@import SocketIO;

@implementation GlobalData
@synthesize message;
@synthesize global_array;
@synthesize phone_contact_list;
@synthesize callhippo_contact_list;
@synthesize mixall_contact_array;
@synthesize Number_Selection;
@synthesize socket;
@synthesize manager;
@synthesize outcallprovider;
@synthesize Socket_Url;
@synthesize wake_manager;
@synthesize wake_socket;
@synthesize callLogs_global_array;
@synthesize coredbmanager;
@synthesize  chAndDeviceContactList;
@synthesize  chContactListFromDB;

static GlobalData *sharedGlobalData = nil;

+ (GlobalData*)sharedGlobalData {
     NSMutableArray *data = [[NSMutableArray alloc] init];
    
    if (sharedGlobalData == nil) {
        sharedGlobalData = [[super allocWithZone:NULL] init];
        
        sharedGlobalData.global_array = [[NSMutableArray alloc] init];
        sharedGlobalData.callLogs_global_array = [[NSMutableArray alloc] init];
        sharedGlobalData.phone_contact_list = [[NSMutableArray alloc] init];
        sharedGlobalData.callhippo_contact_list = [[NSMutableArray alloc] init];
        sharedGlobalData.mixall_contact_array = [[NSMutableArray alloc] init];
        sharedGlobalData.Number_Selection = [[NSMutableArray alloc] init];
        
        sharedGlobalData.outcallprovider = [[NSMutableArray alloc] init];
        
        sharedGlobalData.callCountryProviderList = [[NSMutableArray alloc]init];
        
        sharedGlobalData.sameCallerIdRestricCountries = [[NSMutableArray alloc]init];

        // initialize your variables here
        sharedGlobalData.message = @"Default Global Message";
        sharedGlobalData.coredbmanager = [[CoreDataManager alloc]init];
        
        sharedGlobalData.chAndDeviceContactList = [[NSMutableArray alloc]init];
        sharedGlobalData.chContactListFromDB = [[NSMutableArray alloc]init];
        sharedGlobalData.updatedContactArray = [[NSMutableArray alloc]init];

        
//        [self get_all_phone_contacts];
//        [self get_all_callhippo_contacts];
//
//
//        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
//        {
//            //NSLog(@"sharedGlobalData.callhippo_contact_list : %@",sharedGlobalData.callhippo_contact_list);
//            [[sharedGlobalData mixall_contact_array] addObjectsFromArray:sharedGlobalData.phone_contact_list];
//            [[sharedGlobalData mixall_contact_array] addObjectsFromArray:sharedGlobalData.callhippo_contact_list];
//        });
        
        
//        [self get_callhippo_number_selection];
        [self get_all_phone_contacts];
      //old  [self get_all_callhippo_contacts];
        [self get_outcallprovider];

        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
        
        });
        
        
    }
//    NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];
//    sharedGlobalData.manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
//    sharedGlobalData.socket = sharedGlobalData.manager.defaultSocket;
//
//    sharedGlobalData.wake_manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
//    sharedGlobalData.wake_socket = sharedGlobalData.manager.defaultSocket;
    
    return sharedGlobalData;
}

#pragma mark - OutGoingCall Providers API

+ (void)get_outcallprovider{
    sharedGlobalData.outcallprovider = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"outcallprovider/%@",userId];
    //NSLog(@"URL Providers : %@",aStrUrl);
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallProviders:response:) andDelegate:self];
}

+ (void)getCallProviders:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************4  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    //NSLog(@"global_getCallHippoProviders : %@",response1);
//    [sharedGlobalData.outcallprovider removeAllObjects];
    sharedGlobalData.outcallprovider = [[NSMutableArray alloc] init];
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        NSArray *arrcontact = [response1 valueForKey:@"outgoingCallProvider"];
        
//        //NSLog(@"Chandaa : %@",arrcontact);
//        [[sharedGlobalData outcallprovider] addObjectsFromArray:arrcontact];
        
        if (arrcontact.count > 0) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSMutableArray arrayWithArray:arrcontact] forKey:@"providersArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
                        //NSLog(@"Chandaa : %@",arrcontact);
            [[sharedGlobalData outcallprovider] addObjectsFromArray:arrcontact];
                   
        }
    }
        
    }
}

#pragma mark - get outcall country provider
+(NSString *)getCallingProvider:(NSString *)shortName
{
   // NSLog(@"out call country list :: %@",[sharedGlobalData callCountryProviderList]);
    
    if ([Default valueForKey:kbackendProvider]) {
        [Default removeObjectForKey:kbackendProvider];
    }
    
    NSArray *arr = [sharedGlobalData.callCountryProviderList valueForKey:@"shortName"];
    
   // NSLog(@"shortname Arr:: %@",arr);
    
    NSString *provider;
    if ([arr containsObject:shortName]) {
        NSUInteger index = [arr indexOfObject:shortName];

// <<<<<<< HEAD
      //  NSLog(@"index:: %lu",(unsigned long)index);
        
        provider = [[sharedGlobalData.callCountryProviderList valueForKey:@"frontend_provider"] objectAtIndex:index];
        
        [Default setValue:[[sharedGlobalData.callCountryProviderList valueForKey:@"backend_provider"] objectAtIndex:index] forKey:kbackendProvider];
        [Default synchronize];
        
        NSLog(@"backend provider:: %@",[[sharedGlobalData.callCountryProviderList valueForKey:@"backend_provider"] objectAtIndex:index]);
        
       // NSLog(@"backend provider in default:: %@",[Default valueForKey:kbackendProvider]);
        
        NSLog(@"provider :: %@ of country :: %@",provider,[sharedGlobalData.callCountryProviderList valueForKey:@"shortName"]);
    }
    else
    {
        provider = @"";
    }
    
    if ([provider isEqualToString:@""] || provider == nil) {
        
        if ([[Default valueForKey:kTelephonyProviderSwitch] isEqualToString:@"A"]) {
            
            provider = [Default valueForKey:kProviderSwitchA];
            
            if ([provider isEqualToString:@""]) {
                provider = @"twilio";
            }
        }
        else
        {
             provider = [Default valueForKey:kProviderSwitchB];
            
            if ([provider isEqualToString:@""]) {
                provider = @"twilio";
            }
        }
    }
            
    return provider;
}


/*+(NSString *)getOutCallProviderByCountry:(NSString *)shortName
=======*/
+(BOOL)getContryRestrictedForSameCallId:(NSString *)shortName
{

    BOOL isrestricted = NO;
    
    if ([sharedGlobalData.sameCallerIdRestricCountries containsObject:shortName]) {
        
        isrestricted = YES;
    }
    
    return isrestricted;
    
}/*
+(NSString *)getOutCallProviderByCountry:(NSString *)shortName
>>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
   // NSLog(@"out call country list :: %@",[sharedGlobalData callCountryProviderList]);
    
    NSArray *arr = [sharedGlobalData.callCountryProviderList valueForKey:@"shortName"];
    
   // NSLog(@"shortname Arr:: %@",arr);
    
    NSString *provider;
    if ([arr containsObject:shortName]) {
        NSUInteger index = [arr indexOfObject:shortName];

      //  NSLog(@"index:: %lu",(unsigned long)index);
        
        provider = [[sharedGlobalData.callCountryProviderList valueForKey:@"frontend_provider"] objectAtIndex:index];
        
        NSLog(@"provider :: %@ of country :: %@",provider,[sharedGlobalData.callCountryProviderList valueForKey:@"shortName"]);
    }
    else
    {
        provider = @"";
    }
    
    return provider;

}
*/ 
#pragma mark - Phone Conetact API

+ (void)get_all_phone_contacts
{
      [[sharedGlobalData phone_contact_list] removeAllObjects];
    //Create repository objects contacts
    CNContactStore *contactStore = [[CNContactStore alloc] init];
    
    //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
    
    NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
    
    // Create a request object
    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
    request.predicate = nil;
    
    

   

    NSDate *methodStart = [NSDate date];
    
    [contactStore enumerateContactsWithFetchRequest:request
                                              error:nil
                                         usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
     {
         // Contact one each function block is executed whenever you get
//         sharedGlobalData.phone_contact_list = [[NSMutableArray alloc] init];
         
         NSString *phoneNumber_int = @"";
         
        
         if( contact.phoneNumbers)
         {
             NSMutableArray *numbersArray = [[NSMutableArray alloc]init];

             for (int i =0 ; i<[contact.phoneNumbers count]; i++) {
                 
                 NSString *phoneNumber = [[[contact.phoneNumbers objectAtIndex:i] value] stringValue];
                 phoneNumber_int = phoneNumber;
                 phoneNumber_int = [phoneNumber_int stringByReplacingOccurrencesOfString:@"(" withString:@""];
                 phoneNumber_int = [phoneNumber_int stringByReplacingOccurrencesOfString:@")" withString:@""];
                 phoneNumber_int = [phoneNumber_int stringByReplacingOccurrencesOfString:@" " withString:@""];
                 phoneNumber_int = [phoneNumber_int stringByReplacingOccurrencesOfString:@"-" withString:@""];
                 phoneNumber_int = [phoneNumber_int stringByReplacingOccurrencesOfString:@"+" withString:@""];
                 
                 NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:phoneNumber,@"number",phoneNumber_int,@"number_int", nil];
                 [numbersArray addObject:temp];
             }
        
             
        
         NSString *FullName = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
            FullName = [FullName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
         //NSString *Number = [NSString stringWithFormat:@"%@",phoneNumber];
         //NSString *Number2 = [NSString stringWithFormat:@"%@",phoneNumber_int];
         NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
         [dic setObject:FullName forKey:@"name"];
        [dic setObject:numbersArray forKey:@"numberArray"];

        // [dic setObject:Number forKey:@"number"];
        // [dic setObject:Number2 forKey:@"number_int"];
         

             
             
                 /*
                  working for single no
                  if(![FullName isEqualToString:@""] && ![Number isEqualToString:@""])
                     {
                         if ([Number isEqualToString:@"(null)"])
                         {
                 
                         }else if ([Number isEqualToString:@""]) {
                 
                         }
                         else
                         {
                             [[sharedGlobalData phone_contact_list] addObject:dic];
                         }
             
             
                     }*/
             
             if(![FullName isEqualToString:@""] && [numbersArray count]!=0)
            {
                         
                             [[sharedGlobalData phone_contact_list] addObject:dic];
                
            }
             
             
             
    }
        
       

     }];
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    

    
    NSLog(@"executionTime = %f", executionTime);
  //  NSLog(@"device contacts: %@", [sharedGlobalData get_phone_contact_list]);
}


#pragma mark - Callhippo Conetact API

+ (void)get_all_callhippo_contacts
{
//    sharedGlobalData.callhippo_contact_list = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contact",userId];
    
    NSLog(@"URL Contact check : %@",aStrUrl);
    
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallHippoContactListresponse:response:) andDelegate:self];
}

+ (void)getCallHippoContactListresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************5  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    //NSLog(@"global_getCallHippoContactListresponse : %@",response1);
    [sharedGlobalData.callhippo_contact_list removeAllObjects];
    sharedGlobalData.callhippo_contact_list = [[NSMutableArray alloc] init];
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
      
        
        NSArray *arrcontact = [response1 valueForKey:@"data"];
        NSMutableArray *arrcontact1 = [[NSMutableArray alloc] init];
        for (int i = 0; i<arrcontact.count; i++) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            dic = [arrcontact objectAtIndex:i];
            NSString *num = [dic valueForKey:@"number"];
             num = [num stringByReplacingOccurrencesOfString:@"(" withString:@""];
             num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
             num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
             num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
             num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
            [dic setValue:num forKey:@"number_int"];
            [arrcontact1 addObject:dic];
        }
        
        NSArray *arr = [[NSArray alloc] init];
         arr = arrcontact1;
        NSLog(@"arrcontact1 : %@",arr);
         [[sharedGlobalData callhippo_contact_list] addObjectsFromArray:arr];
    }
    else
    {
       
    }
        
    }
}


+ (void)get_callhippo_number_selection
{
    sharedGlobalData.Number_Selection = [[NSMutableArray alloc] init];

    NSString *userID = [Default valueForKey:USER_ID];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";
    //    NSString *Version_Info = [Default valueForKey:IS_VERSIONINFO] ? [Default valueForKey:IS_VERSIONINFO] : @"";
    //    NSString *Auth_Token = [Default valueForKey:AUTH_TOKEN] ? [Default valueForKey:AUTH_TOKEN] : @"";
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
    NSDictionary *passDict = @{@"userId":userID,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"token":fcmtoken,
                               @"versionInfo":VERSIONINFO,};
    NSLog(@"getcredit : %@",passDict);
    NSLog(@"getcredit : %@%@",SERVERNAME,url);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getCredits:response:) andDelegate:self];
}

+ (void)get_callhippo_number_selection_response:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************6  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    
    //NSLog(@"get_callhippo_number_selection_response  trush  : %@",response1);
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        
        NSLog(@"Trushang Number List : %@",response1);
        NSDictionary*dic = [response1 valueForKey:@"numberData"];
        NSArray *purchase_number = [dic valueForKey:@"numbers"];
        
////        [Default setObject:purchase_number forKey:PURCHASE_NUMBER];
////        [Default synchronize];
//        NSMutableDictionary *DummyNumber = [[NSMutableDictionary alloc] init];
//        NSMutableDictionary *Number = [[NSMutableDictionary alloc] init];
//        [DummyNumber setValue:@"5ca6e3e8bda41f49c92991cc" forKey:@"_id"];
//        [Number setValue:@"US" forKey:@"shortName"];
//        [Number setValue:@"+187******** " forKey:@"contactName"];
//        [Number setValue:@"+187******** " forKey:@"phoneNumber"];
//        [Number setValue:@"1" forKey:@"numberVerify"];
//        [Number setValue:@"plivo" forKey:@"outgoingCallProvider"];
//        [Number setValue:@"1" forKey:@"ISDummyNumber"];
//        [DummyNumber setValue:Number forKey:@"number"];
//
//        NSMutableArray *Dummynumberarray = [[NSMutableArray alloc] init];
//        [Dummynumberarray addObject:DummyNumber];
//        purchase_number = Dummynumberarray;
//        NSLog(@"purchase_number : %@",Dummynumberarray);
    
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        sharedGlobalData.Number_Selection = [[NSMutableArray alloc] init];
        [[sharedGlobalData Number_Selection] addObjectsFromArray:purchase_number];
    }
    else
    {
        
    }
        
    }
}
- (void)get_Updated_Purchased_Number
{
     sharedGlobalData.Number_Selection = [[NSMutableArray alloc] init];

    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"%@/%@",userId,Get_Numbers];
    
    NSLog(@"get_callhippo_number_selection : %@%@",SERVERNAME,url);
     WebApiController *obj;
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(get_updated_purchased_numberResponse:response:) andDelegate:self];
}


- (void)get_updated_purchased_numberResponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************6  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
        NSLog(@"get_callhippo_number_selection_response  trush  : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        
        //NSLog(@"Numbes : %@",response1);
        NSDictionary*dic = [response1 valueForKey:@"data"];
        NSArray *purchase_number = [dic valueForKey:@"numbers"];
        
        
            if ([purchase_number count] == 1) {
                if ([[[purchase_number objectAtIndex:0] objectForKey:ISDummyNumber] intValue] == 0) {
                    
                    return;
                }
            }
    
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
//        sharedGlobalData.Number_Selection = [[NSMutableArray alloc] init];
//        [[sharedGlobalData Number_Selection] addObjectsFromArray:purchase_number];
    }
    else
    {
        
    }
        
    }
}
+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self)
    {
        if (sharedGlobalData == nil)
        {
            sharedGlobalData = [super allocWithZone:zone];
            
            return sharedGlobalData;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// this is my global function
- (void)myFunc {
    self.message = @"Some Random Text";
}

#pragma mark - Get All Providers

- (NSMutableArray *)get_all_provider{
    return outcallprovider;
}

-(NSMutableArray *)get_callCountryProviderList
{
    return _callCountryProviderList;
}

#pragma mark - blacklisted

+(BOOL)isNumberBlacklisted:(NSString *)number
{
    BOOL isblacklisted = false;
    
        number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    for (int i = 0; i< [[Default valueForKey:kblackListedArray] count]; i++) {
        
        if ([[Default valueForKey:kblackListedArray] containsObject:number]) {
            
            isblacklisted = true;
        }
        
    }
    
    return isblacklisted;
}
-(void)getUpdatedBlackListedArray
{

    NSString *url = [NSString stringWithFormat:@"getBlockNumberList"];
    NSLog(@"get blacklisted number url : %@%@",SERVERNAME,url);
     WebApiController *obj;
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(getUpdatedBlackListedArrayResponse:response:) andDelegate:self];
}
- (void)getUpdatedBlackListedArrayResponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    
   // NSLog(@"get blacklisted number : %@",response1);
        
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        NSDictionary*dic = [response1 valueForKey:@"data"];
    
        [Default setValue:[dic valueForKey:@"number"] forKey:kblackListedArray];
        [Default synchronize];
        
    }
    }
    
}
#pragma mark - Get Phone Contacts

- (NSMutableArray*)get_phone_contact_list
{
    return phone_contact_list;
}

#pragma mark - Get Callhippo Contacts

/*- (NSMutableArray*)get_callhippo_contact_list
{
    return callhippo_contact_list;
}
- (NSMutableArray*)get_mix_contact_list
{
    mixall_contact_array = [[NSMutableArray alloc] init];
    [mixall_contact_array addObjectsFromArray:phone_contact_list];
    NSLog(@"countrjhvhbjhvjh %lu",(unsigned long)callhippo_contact_list.count);
    [mixall_contact_array addObjectsFromArray:callhippo_contact_list];
    return mixall_contact_array;
}*/
- (NSMutableArray*)get_number_selection
{
    return Number_Selection;
}

-(void)edit_contact:(int)index newdic:(NSDictionary *)newdic olddic:(NSMutableDictionary *)olddic title:(NSString *)title
{
    
   if([title isEqualToString:@"edit"])
   {

       NSString *newid = [newdic valueForKey:@"_id"];
       NSUInteger index = [callhippo_contact_list indexOfObjectPassingTest:^BOOL (id obj, NSUInteger idx, BOOL *stop) {
           return [[(NSDictionary *)obj objectForKey:@"_id"] isEqualToString:newid];
       }];
       
       if (index != NSNotFound) {
           [callhippo_contact_list removeObjectAtIndex:index];
           [callhippo_contact_list addObject:newdic];
       }
       
   }
   else if([title isEqualToString:@"add"])
   {
       [callhippo_contact_list addObject:newdic];
   }
   else if([title isEqualToString:@"delete"])
   {
       NSString *newid = [newdic valueForKey:@"_id"];
       NSUInteger index = [callhippo_contact_list indexOfObjectPassingTest:^BOOL (id obj, NSUInteger idx, BOOL *stop) {
           return [[(NSDictionary *)obj objectForKey:@"_id"] isEqualToString:newid];
       }];
       
       if (index != NSNotFound) {
           [callhippo_contact_list removeObjectAtIndex:index];
//           [callhippo_contact_list addObject:newdic];
       }
   }

}
#pragma mark - call logs array

- (void)addDataTo_global_callarray:(NSMutableArray *)array
{
    [callLogs_global_array addObjectsFromArray:array]  ;
}
- (void)replaceDataTo_global_callarray:(NSMutableArray *)array
{
    callLogs_global_array = [[NSMutableArray alloc]init];
    callLogs_global_array = array; ;
}
+(void)getCallhippoContactBySkipLimit:(NSString *)skip
{
    
    [Default setBool:true forKey:kisContactSyncingRunning];
    
    int skipval = [skip intValue];
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    
    
    
        
//        NSString *aStrUrl = [NSString stringWithFormat:@"%@/contactwithskiplimit?skip=%d&limit=1000&deviceType=ios&deleteMobileContacts=true",userId,skipval];
    
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contactwithskiplimit?skip=%d&limit=1000&deviceType=ios",userId,skipval];
  
    
    NSLog(@"URL Contact by skip limit : %@",aStrUrl);
    
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallhippoContactBySkipLimitRes:response:) andDelegate:self];
    
}

+(void)getCallhippoContactBySkipLimitRes:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************5  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
        NSLog(@"contact by skip limit  : %@",response1);
     
//    [sharedGlobalData.callhippo_contact_list removeAllObjects];
//    sharedGlobalData.callhippo_contact_list = [[NSMutableArray alloc] init];
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
      
        
        NSArray *arrcontact = [response1 valueForKey:@"data"];
     //   NSLog(@"count :: %lu",(unsigned long)arrcontact.count);
        
      //  dispatch_async(dispatch_get_main_queue(), ^{
            
        NSLog(@"count added :: %lu",arrcontact.count);
        
        NSMutableArray *conarr = [[NSMutableArray alloc]init];
        
        for (int p=0; p<arrcontact.count; p++) {
            
            NSDictionary *dic = [arrcontact objectAtIndex:p];
           
            NSManagedObjectContext *context = [[sharedGlobalData coredbmanager] managedObjectContext];
            
            Contact *newcon = [[Contact alloc]initWithContext:context];
            newcon.name = NULL_TO_NIL([dic valueForKey:@"name"]);
            newcon.company = NULL_TO_NIL([dic valueForKey:@"company"]);
            newcon.email = NULL_TO_NIL([dic valueForKey:@"email"]);
            newcon.contactID = NULL_TO_NIL([dic valueForKey:@"_id"]);
            //newcon.number = [Contact getStringFromNumberArray:[dic valueForKey:@"numberArray"]];
            newcon.number = NULL_TO_NIL([dic valueForKey:@"numberArray"]);
            
            [conarr addObject:newcon];
            
        //     [Contact addContactToDB:newcon];
        }
        //});
        
        
      //  if ([Contact allContactCount] == 0) {
            
          //  dispatch_sync(dispatch_get_main_queue(), ^{
            
                [Contact addBatchContactToDB:conarr];
          //  });
//        }
//        else{
//            [Contact addBatchContactToDB:conarr];
//        }
        
      
        
        if (arrcontact.count!= 0) {
            
          dispatch_async(dispatch_get_main_queue(), ^{

                [GlobalData  getCallhippoContactBySkipLimit:[NSString stringWithFormat:@"%lu",(unsigned long)[Contact allContactCount]]];
              [Default setBool:true forKey:kNeedToUpdateContact];
                
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"start date :: %@",[Default valueForKey:@"startdate"]);
                
                NSLog(@"end date :: %@",[NSDate date]);
                
            [Default setBool:true forKey:kisContactStoredInDB];
            [Default setBool:false forKey:kisContactSyncingRunning];

            [Default setBool:false forKey:kNeedToUpdateContact];
            [Default setBool:false forKey:kNeedToIgnoreUpdatedContact];
                
            [[GlobalData sharedGlobalData] makeArrayFromDBChContact];
        
           // [UtilsClass makeToast:@"contact syncing completed"];

            
            });
        }
            
   //     });
        
    }
    else
    {
       
    }
        
    }
}
+(void)getCallhippoIntegrationContacts:(NSString *)skip
{
    
    [Default setBool:true forKey:kisContactSyncingRunning];
    
    int skipval = [skip intValue];
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    
    
        NSString *aStrUrl = [NSString stringWithFormat:@"%@/contactwithskiplimit?skip=%d&limit=1000&deviceType=ios",userId,skipval];
    
    
    NSLog(@"URL Contact by skip limit : %@",aStrUrl);
    
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallhippoIntegrationContactsRes:response:) andDelegate:self];
    
}
+(void)getCallhippoIntegrationContactsRes:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************5  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
        NSLog(@"contact by skip limit  : %@",response1);
     
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
      
        
        NSArray *arrcontact = [response1 valueForKey:@"data"];
     
            
        NSLog(@"count added :: %lu",arrcontact.count);
        
        NSMutableArray *conarr = [[NSMutableArray alloc]init];
        
        
        for (int p=0; p<arrcontact.count; p++) {
            
            @autoreleasepool {
            
            NSDictionary *dic = [arrcontact objectAtIndex:p];
        
            NSMutableDictionary *newDic = [NSMutableDictionary dictionaryWithDictionary:dic];

            NSManagedObjectContext *context = [[sharedGlobalData coredbmanager] managedObjectContext];
            Contact *newcon = [[Contact alloc]initWithContext:context];
            newcon.name = NULL_TO_NIL([newDic valueForKey:@"name"]);
            newcon.company = NULL_TO_NIL([newDic valueForKey:@"company"]);
            newcon.email = NULL_TO_NIL([newDic valueForKey:@"email"]);
            newcon.contactID = NULL_TO_NIL([newDic valueForKey:@"_id"]);
            newcon.number = NULL_TO_NIL([newDic valueForKey:@"numberArray"]);
                            
                           
            [[[GlobalData sharedGlobalData] chContactListFromDB] addObject:newDic];
                      
            [conarr addObject:newcon];
                    
            //autorelease complete
        }
        }
        
        if (conarr.count>0) {
            [Contact addBatchContactToDB:conarr];
        }
   
        
        if (arrcontact.count!= 0) {
            
          dispatch_async(dispatch_get_main_queue(), ^{

                [GlobalData  getCallhippoIntegrationContacts:[NSString stringWithFormat:@"%lu",(unsigned long)[Contact allContactCount]]];
                
            });
        }
        else
        {
           // dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"start date :: %@",[Default valueForKey:@"startdate"]);
                
                NSLog(@"end date :: %@",[NSDate date]);
                
           
            [Default setBool:false forKey:kisContactSyncingRunning];
                
        
            [UtilsClass makeToast:@"contact syncing completed"];

            
         //   });
        }
            
   //     });
        
    }
    else
    {
       
    }
        
    }
}
+(void)getCallhippoUpdatedContacts:(NSString *)skip
{

    [Default setBool:true forKey:kisContactSyncingRunning];
    
    int skipval = [skip intValue];
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contactwithskiplimit?skip=0&limit=1000&deviceType=ios&isItMobileContacts=true",userId];
    
    NSLog(@"URL Contact by skip limit : %@",aStrUrl);
    
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallhippoUpdatedContactsRes:response:) andDelegate:self];
    
    
}
+(void)getCallhippoUpdatedContactsRes:(NSString *)apiAlias response:(NSData *)response
{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************5  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    
    NSLog(@"contact updated by skip limit  : %@",response1);
     
//    [sharedGlobalData.callhippo_contact_list removeAllObjects];
//    sharedGlobalData.callhippo_contact_list = [[NSMutableArray alloc] init];
    
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
      
      //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       
        
        NSArray *arrcontact = [response1 valueForKey:@"data"];
        NSLog(@"count updated:: %lu",(unsigned long)arrcontact.count);

        NSMutableArray *addConarr = [[NSMutableArray alloc]init];
        NSArray *existIdArr = [[[GlobalData sharedGlobalData] chContactListFromDB] valueForKey:@"_id"];
//
//        if ([Default boolForKey:kNeedToIgnoreUpdatedContact] == false)
//        {
            for (int p=0; p<arrcontact.count; p++) {
                
                @autoreleasepool {
                
                NSDictionary *dic = [arrcontact objectAtIndex:p];
                NSString *action = [dic valueForKey:@"action"];
            
                NSMutableDictionary *newDic = [NSMutableDictionary dictionaryWithDictionary:dic];

                [newDic removeObjectForKey:@"_id"];
                [newDic setValue:[dic valueForKey:@"contactId"] forKey:@"_id"];
                
                
                if ([action isEqualToString:@"add"]) {
                    
                    /*if (p == 0) {

                       // if (![Contact ifExistContact:[newDic valueForKey:@"_id"]])
                        if (![[[[GlobalData sharedGlobalData] chContactListFromDB] valueForKey:@"_id"] containsObject:[newDic valueForKey:@"_id"]])
                        {
                            [Default setBool:false forKey:kNeedToIgnoreUpdatedContact];
                        }
                    }*/
                       
                        if (![existIdArr containsObject:[newDic valueForKey:@"_id"]])
                        
                     //   if (![Contact ifExistContact:[newDic valueForKey:@"_id"]])

                        {
                             NSManagedObjectContext *context = [[sharedGlobalData coredbmanager] managedObjectContext];
                                Contact *newcon = [[Contact alloc]initWithContext:context];
                                newcon.name = [newDic valueForKey:@"name"];
                                newcon.company = [newDic valueForKey:@"company"];
                                newcon.email = [newDic valueForKey:@"email"];
                                newcon.contactID = [newDic valueForKey:@"_id"];
                                newcon.number = [newDic valueForKey:@"numberArray"];
                                
                               // [[GlobalData sharedGlobalData] editCHcontactToDBArray:newDic title:@"add"];
                            [[[GlobalData sharedGlobalData] chContactListFromDB] addObject:newDic];
                          
                                [addConarr addObject:newcon];
                        
                        }
                 //       }

                }
                else if ([action isEqualToString:@"update"]) {
                    
                    NSManagedObjectContext *context = [[sharedGlobalData coredbmanager] managedObjectContext];
                    Contact *newcon = [[Contact alloc]initWithContext:context];
                    newcon.name = [newDic valueForKey:@"name"];
                    newcon.company = [newDic valueForKey:@"company"];
                    newcon.email = [newDic valueForKey:@"email"];
                    newcon.contactID = [newDic valueForKey:@"_id"];
                    newcon.number = [newDic valueForKey:@"numberArray"];
                    
                    [Contact editContactToDB:newcon];
                    [[GlobalData sharedGlobalData] editCHcontactToDBArray:newDic title:@"edit"];
                    
                }
            
                else if ([action isEqualToString:@"delete"]) {
                    
                    [Contact deleteFromDB:[newDic valueForKey:@"_id"]];
                    [[GlobalData sharedGlobalData] editCHcontactToDBArray:newDic title:@"delete"];
                    
                }
               //autorelease complete
            }
            }
    //    }
        
        if (addConarr.count>0) {
            [Contact addBatchContactToDB:addConarr];
        }
        
       
        if (arrcontact.count!= 0) {
        
            dispatch_async(dispatch_get_main_queue(), ^{
            
            [GlobalData  getCallhippoUpdatedContacts:[NSString stringWithFormat:@"0"]];
                
            });
                
        }
        else
        {
          //  dispatch_async(dispatch_get_main_queue(), ^{
            
           
            NSLog(@"update start :: %@",[Default valueForKey:@"updatestartdate"]);
            NSLog(@"update end :: %@",[NSDate date]);
            
                      
          //      [UtilsClass makeToast:@"contact syncing completed"];

                [Default setBool:false forKey:kNeedToUpdateContact];
                [Default setBool:false forKey:kisContactSyncingRunning];
                          
            
              //  [UtilsClass makeToast:@"contact array added completed"];
                
          //  });
          //  [Default setBool:false forKey:kNeedToIgnoreUpdatedContact];
          
     
       //     }
       // });
            
        
    }
//    else
//    {
//
//    }
//
    }
    }
}
-(void)makeArrayFromDBChContact
{
    
    if (chContactListFromDB.count>0) {
        [chContactListFromDB removeAllObjects];
    }
    
    NSArray *contactArray = [[NSArray alloc]initWithArray:[Contact fetchContactFromDB]];
   // NSArray *contactArray = [Contact fetchContactFromDB];
    
    //NSMutableArray *chContactArray = [[NSMutableArray alloc]init];
    
    for (int p=0; p<contactArray.count; p++) {
        
        Contact *newcon = [contactArray objectAtIndex:p];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        
        [dic setValue:newcon.name forKey:@"name"];
        [dic setValue:newcon.company forKey:@"company"];
        [dic setValue:newcon.contactID forKey:@"_id"];
        [dic setValue:newcon.number forKey:@"numberArray"];
        [dic setValue:newcon.email forKey:@"email"];
        
        [chContactListFromDB addObject:dic];
    
    }
    //chContactListFromDB = chContactArray;
    
}
- (NSMutableArray*)get_chAndDeviceContactList
{
    chAndDeviceContactList = [[NSMutableArray alloc] init];
    [chAndDeviceContactList addObjectsFromArray:phone_contact_list];
    [chAndDeviceContactList addObjectsFromArray:chContactListFromDB];
    
    //NSLog(@"ch contacts:: %@",chContactListFromDB);

    return chAndDeviceContactList;
}
-(void)editCHcontactToDBArray:(NSDictionary *)newdic title:(NSString *)title
{
   
   if([title isEqualToString:@"edit"])
   {

      /* NSString *newid = [newdic valueForKey:@"_id"];
       NSUInteger index = [chContactListFromDB indexOfObjectPassingTest:^BOOL (id obj, NSUInteger idx, BOOL *stop) {
           return [[(NSDictionary *)obj objectForKey:@"_id"] isEqualToString:newid];
       }];
       
       if (index != NSNotFound) {
           [chContactListFromDB removeObjectAtIndex:index];
           [chContactListFromDB addObject:newdic];
       }*/
       
       NSString *newid = [newdic valueForKey:@"_id"];
       
       if ([[self->chContactListFromDB valueForKey:@"_id"] containsObject:newid]) {
           
           NSUInteger index = [[self->chContactListFromDB valueForKey:@"_id"] indexOfObject:newid];
           [self->chContactListFromDB replaceObjectAtIndex:index withObject:newdic];
       }
    
   }
   else if([title isEqualToString:@"add"])
   {
       
       [self->chContactListFromDB addObject:newdic];
   }
   else if([title isEqualToString:@"delete"])
   {
       NSString *newid = [newdic valueForKey:@"_id"];
       
       if ([[self->chContactListFromDB valueForKey:@"_id"] containsObject:newid]) {
           NSUInteger index = [[self->chContactListFromDB valueForKey:@"_id"] indexOfObject:newid];
           [self->chContactListFromDB removeObjectAtIndex:index];
       }
      
   }

    
}
-(void)flushAllContactsAndFetchNew
{
    [Default setBool:true forKey:kisContactSyncingRunning];

    
    NSLog(@"flush contacts :");
    if ([Contact allContactCount] != 0) {
        [Contact deleteAllContacts];
    }
    
       
    [[[GlobalData sharedGlobalData] chContactListFromDB] removeAllObjects];
    
    
   
    [Default setBool:false forKey:kisContactStoredInDB];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
     
        [Default setValue:[NSDate date] forKey:@"startdate"];

    [GlobalData  getCallhippoContactBySkipLimit:[NSString stringWithFormat:@"0"]];
        
    });
}

//+(void)getEndPointsApiCall {
//    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
//    NSString *userID = [Default valueForKey:USER_ID];
//    NSString *url = [NSString stringWithFormat:@"/endpoints/%@/ios",userID];
//    
//    WebApiController *obj;
//    obj = [[WebApiController alloc] init];
//    
//    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
//    //NSLog(@"Final url : %@%@",SERVERNAME,url);
//    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(getEndPoints:response:) andDelegate:self];
//}
//
//- (void)getEndPoints:(NSString *)apiAlias response:(NSData *)response{
//    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//    
//    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//        //        _contryName = response1[@"data"][@"countryName"];
//        //NSLog(@"lastdialcountry %@ : ",response1);
//        NSMutableArray *endPoints = response1[@"endpoints"];
//        for (int i = 0; i<endPoints.count; i++) {
//            //NSLog(@"EndPoinnts for loop %d",i);
//            [Lin_Utility Lin_call_login:endPoints[i][@"iosSipUser"] domain:endPoints[i][@"iosHostName"] password:endPoints[i][@"iosSipPass"] type:endPoints[i][@"iossipProtocol"]];
//        }
//        
//    }
//}
//
//+(void)getEndPoint:(NSString *)lastCallDate {
//    
//    static NSString* const onceCallEndPoints = @"onceCallEndPoints";
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    
//    if ([defaults boolForKey:onceCallEndPoints] == NO)
//    {
//        // Some code you want to run on first use...
//        [defaults setBool:YES forKey:onceCallEndPoints];
//        [self getEndPointsApiCall];
//        NSDate *today = [[NSDate alloc] init];
//        [Default setObject:today forKey:@"endPointApiCall"];
//        [Default synchronize];
//    }
//    
//    //NSLog(@"get default date lastCallDate : %@",lastCallDate);
//    
//    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
//    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
//    NSDate *firstCall = [dateFormatter1 dateFromString:lastCallDate];
//    //NSLog(@"get default date  today3 : %@",firstCall);
//    NSDate *lastCall = [Default objectForKey:@"endPointApiCall"]; // your date
//    
//    NSComparisonResult result;
//    //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
//    
//    result = [lastCall compare:firstCall]; // comparing two dates
//    
//    if(result==NSOrderedAscending) {
//        //NSLog(@"today is less");
//        [self getEndPointsApiCall];
//        [Default setObject:firstCall forKey:@"endPointApiCall"];
//        [Default synchronize];
//    } else {
//        //NSLog(@"Both dates are same");
//    }
//}


@end
