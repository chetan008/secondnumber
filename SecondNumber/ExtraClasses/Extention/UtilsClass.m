//
//  UtilsClass.m
//  CallHippoV2_LIN
//
//  Created by Hippo on 29/04/19.
//  Copyright © 2019 Hippo. All rights reserved.
//

#import "UtilsClass.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#import "Constant.h"
#import "OnCallVC.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "WebApiController.h"
#import <sys/utsname.h>
#import "AppDelegate.h"
#import "UIView+Toast.h"
#import "Lin_Utility.h"
#import "GlobalData.h"
#import "CalllogsVC.h"
#import "CalllogsDetailsVC.h"
#import "LoginVC.h"
#import "Processcall.h"
#import "AppDelegate.h"
#import "twilio_callkit.h"
#import "MainViewController.h"
#import "LoginViewController.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad


@import PhoneNumberKit;
@import SocketIO;
@import NKVPhonePicker;
@implementation UtilsClass

+ (BOOL)validateEmail:(NSString *)theEmail
{
//    BOOL isValid = YES;
//
//    NSRange rangeAt = [theEmail rangeOfString:@"@"];
//    if (0 == rangeAt.length)
//        isValid = NO;
//    else
//    {
//        NSString *domainName = [theEmail substringFromIndex:rangeAt.location + 1];
//        rangeAt = [domainName rangeOfString:@"@"];
//        if (0 != rangeAt.length)
//            isValid = NO;
//        else
//        {
//            NSInteger dotCount = [[domainName componentsSeparatedByString:@"."] count];
//            if(dotCount < 2 || dotCount > 3)
//                isValid = NO;
//            else
//                if(1 != [[theEmail componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"$!~+`/{}?%^*\\=&'#| "]] count])
//                    isValid = NO;
//        }
//    }
//    return isValid;
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    ;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:theEmail];
    
}

+(BOOL)isNetworkAvailable
{
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL,kREACHABILITYURL );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}

+ (void)showAlert:(NSString *)msg contro:(UIViewController *)controller {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle
                                                                   message:msg
                                                            preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         // handle response here.
                                                     }];
    [alert addAction:okAction];
    [controller presentViewController:alert animated:YES completion:^{
    }];
}


+ (void)showAlert:(NSString *)msg view:(UIView *)view {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle
                                                                   message:msg
                                                            preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         // handle response here.
                                                     }];
    [alert addAction:okAction];
    [[[view window] rootViewController] presentViewController:alert animated:NO completion:nil];
//    [controller presentViewController:alert animated:YES completion:^{
//    }];
}




+ (void)showAlert:(NSString *)msg title:(NSString *)title contro:(UIViewController *)controller {
    

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         // handle response here.
                                                     }];
    [alert addAction:okAction];
    [controller presentViewController:alert animated:YES completion:^{
    }];
}

+(void)keypadtextset:(UIButton *)button text:(NSString *)text tot:(NSInteger)tot;
{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
    NSUInteger intVal = tot;
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:25.0] range:NSMakeRange(0,1)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.0] range:NSMakeRange(2,intVal)];
    
    if (intVal == 1){
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:25.0] range:NSMakeRange(0,1)];
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Light" size:25.0] range:NSMakeRange(2,intVal)];
         button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    
    if ([text isEqualToString:@"*\n  "]){
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Light" size:60.0] range:NSMakeRange(0,1)];
        button.contentEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 0);
    }
    if([text isEqualToString:@"0\n+ "])
    {
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Medium" size:25.0] range:NSMakeRange(0,1)];
        [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0] range:NSMakeRange(2,intVal)];
        button.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 10, 0);
    }
    
    [button setAttributedTitle:string forState:normal];
    button.titleLabel.numberOfLines = 0;
    button.tintColor = UIColor.blackColor;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    
}


+(void)textfiled_shadow_boder:(UITextField *)txt
{
    txt.layer.backgroundColor = [UIColor clearColor].CGColor;
    txt.layer.cornerRadius = 5;
    UIView *paddingview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10.0, 10.0)];
    
    txt.leftView = paddingview;
    txt.leftViewMode = UITextFieldViewModeAlways;;
    
    txt.layer.shadowOpacity = 0.4;
    txt.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    txt.layer.shadowRadius = 4.0;
    txt.layer.shadowColor = [UIColor grayColor].CGColor;
    txt.backgroundColor = [UIColor whiteColor];

}

+(void)image_round_boder:(UIImageView *)img {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(img.frame.origin.x, img.frame.origin.y, img.frame.size.width+20, img.frame.size.height+20)];
    view.layer.borderWidth = 1;
    view.layer.cornerRadius =  view.frame.size.height / 2;
    [view addSubview:img];
}

+(void)SetTextFieldBottomBorder :(UITextField *)textField{

    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;

}

+(void)SetLabelBottomBorder :(UILabel *)label{

    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.8;
    border.borderColor = [UIColor grayColor].CGColor;
    border.frame = CGRectMake(0, label.frame.size.height - borderWidth, label.frame.size.width, label.frame.size.height);
    border.borderWidth = borderWidth;
    [label.layer addSublayer:border];
    label.layer.masksToBounds = YES;

}

+(void)view_shadow_boder:(UIView *)view{
    view.layer.backgroundColor = [UIColor clearColor].CGColor;
    view.layer.cornerRadius = 5;
    
    view.layer.shadowOpacity = 0.4;
    view.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    view.layer.shadowRadius = 5.0;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
//    view.layer.borderColor = [[UIColor colorWithRed:83 green:90 blue:89 alpha:50] CGColor];
//    view.layer.borderWidth = 1.0;
    view.backgroundColor = [UIColor whiteColor];
}

+(void)view_shadow_boder_custom:(UIView *)view
{
//    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [view.layer setBorderWidth:1.5f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [view.layer setShadowOpacity:0.8];
    [view.layer setShadowRadius:3.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
}
+(void)button_shadow_boder:(UIButton *)btn
{
    btn.layer.cornerRadius = btn.frame.size.height / 2;
    btn.layer.borderWidth = 2.0;
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.clipsToBounds = true;
    
    btn.imageView.layer.cornerRadius = 7.0f;
    btn.layer.shadowRadius = 3.0;
    btn.layer.shadowColor = [UIColor blackColor].CGColor;
    btn.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    btn.layer.shadowOpacity = 0.2;
    btn.layer.masksToBounds = NO;
    
}


+(void)view_bottom_round_edge:(UIView *)viw desiredCurve:(NSInteger)desiredCurve
{
//    CGFloat offset = viw.frame.size.width / desiredCurve;
//    CGRect bound = viw.bounds;
//
//
//    CGRect rectBound = CGRectMake(bound.origin.x, bound.origin.y, bound.size.width, bound.size.height);
//    UIBezierPath *rectPath = [UIBezierPath bezierPathWithRect:rectBound];
//    CGRect ovalBounds = CGRectMake(bound.origin.x - offset / 2 , bound.origin.y, bound.size.width + offset, bound.size.height);
//    UIBezierPath *ovalPath = [UIBezierPath bezierPathWithRect:ovalBounds];
//    [rectPath appendPath:ovalPath];
//
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    [maskLayer setFrame: bound];
//    [maskLayer setPath: rectPath.CGPath];
//
//    viw.layer.mask = maskLayer;
    
    NSInteger desiredCurve1 = 70;
    UIBezierPath *arrowPath;
    arrowPath = [[UIBezierPath alloc] init];
    [arrowPath moveToPoint:CGPointMake(0.0, 0.0)];
    [arrowPath addLineToPoint:CGPointMake(viw.bounds.size.width, 0)];
    [arrowPath addLineToPoint:CGPointMake(viw.bounds.size.width, viw.bounds.size.height - desiredCurve1)];
    [arrowPath addQuadCurveToPoint:CGPointMake(0, viw.bounds.size.height - desiredCurve1) controlPoint:CGPointMake(viw.bounds.size.width/2, viw.bounds.size.height)];

    [arrowPath addLineToPoint:CGPointMake(0.0, 0.0)];
    [arrowPath closePath];

    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = arrowPath.CGPath;
    maskLayer.frame = viw.bounds;
    maskLayer.masksToBounds = true;
    viw.layer.mask = maskLayer;
    
//    NSInteger marginCurve = 60;
//
//    UIView *example1 = [[UIView alloc] initWithFrame: CGRectMake(0, 0, viw.frame.size.width, viw.frame.size.height / 2)];
//    [example1 setBackgroundColor: [UIColor clearColor]];
//
//    UIBezierPath *curve1 = [UIBezierPath bezierPath];
//    [curve1 moveToPoint: CGPointMake(0, 0)];
//    [curve1 addLineToPoint: CGPointMake(viw.frame.size.width, 0)];
//    [curve1 addLineToPoint: CGPointMake(viw.frame.size.width, viw.frame.size.height - marginCurve)];
//    [curve1 addCurveToPoint: CGPointMake(0, viw.frame.size.height - marginCurve)
//              controlPoint1: CGPointMake(viw.frame.size.width / 2, viw.frame.size.height)
//              controlPoint2: CGPointMake(viw.frame.size.width / 2, viw.frame.size.height)];
//    [curve1 closePath];
//
//    CAShapeLayer *ex1Layer = [CAShapeLayer layer];
//    [ex1Layer setFrame: viw.bounds];
//    [ex1Layer setFillColor: [UIColor brownColor].CGColor];
//    [ex1Layer setPath: curve1.CGPath];
//    viw.layer.mask = ex1Layer;
//    [viw.layer addSublayer: ex1Layer];
//    [viw addSubview: example1];


}
+(void)view_navigation_title:(UIViewController *)viw title:(NSString*)title color:(UIColor*)color
{
    viw.navigationController.navigationBar.hidden = false;
    viw.navigationController.navigationBar.barTintColor = color;
    [viw.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics: UIBarMetricsDefault];
    viw.navigationController.navigationBar.shadowImage = [UIImage new];
    viw.title =title;
    [viw.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor blackColor],
       NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-BOLD" size:24]}];
   
}
+(NSString*)phonenumbercheck:(NSString *)number
{
    @try {
        
        NSError *error1 = nil;
        PhoneNumberKit *papi = [[PhoneNumberKit alloc] init];
        NSString *code =  [papi parse:number withRegion:@"" ignoreType:FALSE error:&error1];
        return code;
    }
    @catch (NSException *exception) {
        return @"invalid_number";
    }
    @finally {
        //NSLog(@"Finally condition");
    }
}
+(NSString*)phoneNumberValid:(NSString *)number
{
    @try {
        
        NSError *error1 = nil;
        PhoneNumberKit *papi = [[PhoneNumberKit alloc] init];
    
        NSString *code =  [papi parse:number withRegion:@"" ignoreType:FALSE error:&error1];
        return code;
    }
    @catch (NSException *exception) {
        return @"invalid_number";
    }
    @finally {
        //NSLog(@"Finally condition");
    }
}
+ (BOOL)isValidNumber:(NSString*)text{
    NSString *regex = @"^([+][0-9]+)?$";
//   @"([+]|-)?((\[0-9\]+\[.\]?\[0-9\]*)|(\[0-9\]*\[.\]?\[0-9\]+))"
    @try {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:text];
    }
    @catch (NSException *exception) {
//        assert(false);
        return NO;
    }
}
+ (BOOL)isValidNumber_withoutplus:(NSString*)text{
    NSString *regex = @"^[0-9]+";
//   @"([+]|-)?((\[0-9\]+\[.\]?\[0-9\]*)|(\[0-9\]*\[.\]?\[0-9\]+))"
    @try {

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        return [predicate evaluateWithObject:text];
    }
    @catch (NSException *exception) {
//        assert(false);
        return NO;
    }
}
+(void)make_outgoing_call:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:ACTIVE_SUB_USER];
    NSArray *activeSubuser = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *name = @"";
    NSString *extentionName = @"";
    NSString *extentionNumber = @"";
    
    
    if([ToName isEqualToString:@""]) {
        name = ToNumber;
        //NSLog(@"Toname1 : %@",name);
    } else
    {
        name = ToName;
        //NSLog(@"Toname2 : %@",name);
    }
    
    //    NSString *countryCode = [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"%@",ToNumber]];
    //    //NSLog(@"Print out Going Call Provider : %@",countryCode);
    //    [Lin_Utility checkProvider:countryCode];
    //    [Lin_Utility checkProvider:countryCode];
    NSString *num = [ToNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
   //p  NSPredicate *filter = [NSPredicate predicateWithFormat:@"number_int contains[c] %@ ",num];
    
    //-mixallcontact
           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",num] ;
           NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num] ;
           
           NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];
           
    
    NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
    if(filteredContacts.count != 0)
    {
        NSDictionary *dic = [filteredContacts objectAtIndex:0];
        //  //NSLog(@"Call Contact Find search dic == == > : %@",dic);
        for (int i = 0; i<filteredContacts.count; i++)
        {
            //NSString *numbers = [filteredContacts[i][@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            
            NSString *numbers;
            if ([[[filteredContacts objectAtIndex:i] valueForKey:@"numberArray"] count]>0) {
                numbers = [[[[[filteredContacts objectAtIndex:i] valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            else
            {
                numbers =@"";
            }
            
            
            if ([num isEqualToString:numbers]) {
                extentionName = filteredContacts[i][@"name"];
                break;
            }
        }
        if(!dic[@"_id"])
        {
            //NSLog(@"Call Contact Find search dic == == > : %@",dic);
            NSString *ContactName = [dic valueForKey:@"name"];
            
            //p NSString *ContactNumber = [dic valueForKey:@"number"];
            
            NSString *ContactNumber ;
            if ([[dic valueForKey:@"numberArray"] count]>0) {
                ContactNumber = [[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            else
            {
                ContactNumber =@"";
            }
            
            
            [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
        }
        
    }
    
    
    
    NSString *address = [NSString stringWithFormat:@"%@",ToNumber];
    address = [address stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *numbStr = [NSString stringWithFormat:@"%@",address];
    //NSLog(@"\n \n \n  phone Number : %@  \n \n \n",address);
    
    //NSLog(@"Trushang : Calling : Screen : make_outgoing_call : %@",vc.debugDescription);
    UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CalllogsVC *calllogs = [storyboard1 instantiateViewControllerWithIdentifier:@"CalllogsVC"];
    CalllogsDetailsVC *calllogsdetails = [storyboard1 instantiateViewControllerWithIdentifier:@"CalllogsDetailsVC"];
    
    
    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    txtText.text = @"";
    [txtText insertText:address];
    NSString *code = txtText.code ? txtText.code : @"";
    NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
    [Default setValue:code forKey:Outgoing_Call_Code];
    [Default setValue:codeName forKey:Outgoing_Call_CodeName];
    [Default synchronize];
    
   
   
    if (num.length == 3 || num.length == 4){
        
        NSString *extentionPlan = [Default valueForKey:EXTENSION_PLAN];
        int extentionInt = [extentionPlan intValue];
        if (extentionInt == 0){
            [UtilsClass showAlert:@"Internal team calling is not available in your plan Please Upgrade your plan" contro:vc];
            return;
        }
        
        NSString *ownExtNumber = [Default valueForKey:ownExtensionNumber];
        if ([ownExtNumber isEqualToString:num]) {
            [UtilsClass makeToast:@"You can't dial own extension number."];
            return;
        }
        BOOL invalidExtention = false;
        for (int i = 0; i<activeSubuser.count; i++)
        {
           // NSLog(@"extensionNumber  :%@",activeSubuser[i][@"extensionNumber"]);
            if ([num isEqualToString:[NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]]]){
                invalidExtention = false;
                if ([extentionName isEqualToString:@""]) {
                    extentionName = activeSubuser[i][@"fullName"];
                }
                extentionNumber = [NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]];
               

                [Default setValue:@"true" forKey:ExtentionCall];
                [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                [Default setValue:@"+1" forKey:klastDialCountryCode];
                [Default setValue:@"United States" forKey:klastDialCountryName];
                [self Twilio_calling:activeSubuser[i][@"twilioEndpoint"][@"webIdentity"] calltype:calltype name:extentionName ToNumber:address vc:vc countryCode:txtText.country.countryCode];
                break;
            }else{
                invalidExtention = true;
            }
        }
        if (invalidExtention == true) {
            [UtilsClass makeToast:@"Please enter valid extension number."];
        }
    }else{
        [Default setValue:@"false" forKey:ExtentionCall];
        [Default setValue:@"" forKey:ExtentionCallProvider];
        NSArray *purchase_number = [[NSArray alloc] init];
        purchase_number = [self getNumbers];
        NSString *calling_number = [Default valueForKey:SELECTEDNO];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
        
        NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
        
        //crash
        //NSDictionary *numberdic = [numbergetarr objectAtIndex:0];
        NSDictionary *numberdic;
        if ([numbergetarr count] > 0) {
            numberdic = [numbergetarr objectAtIndex:0];
        }
        else
        {
            numberdic = [purchase_number objectAtIndex:0];
            NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
            NSString *contactName = [numberdic objectForKey:@"contactName"];
            [Default setValue:phoneNumber forKey:SELECTEDNO];
            [Default setValue:contactName forKey:Selected_Department];
        }
        
        NSDictionary *dic = [numberdic valueForKey:@"number"];
//        NSString *twilioVerify = [dic valueForKey:@"twilioVerify"];
//        int twlVeri = [twilioVerify intValue];
        

      //  if([[dic valueForKey:@"outgoingCallProvider"] isEqualToString:@"plivo"] && twlVeri == 0) {
        
     /*   NSLog(@"shortname to fetch provider :: %@",txtText.country.countryCode);
                    
        NSString *providerStr = [GlobalData getOutCallProviderByCountry:txtText.country.countryCode];
                
                NSLog(@"final provider :: %@",providerStr);
                
        
                if([providerStr isEqualToString:@"plivo"] ){
                    

            [self provider_vise_call:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
        }
        else
        {
            [self Twilio_calling:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
        }*/
        

        

        NSString *fromShortCode = [dic valueForKey:@"shortName"];
        NSString *toShortCode = txtText.country.countryCode;
               
               BOOL initiateCall = false;
               
          /*     if ([fromShortCode isEqualToString:@"IN"] && [[dic valueForKey:@"provider"] isEqualToString: @"airtel"]) {
                   
                   initiateCall = false;
                   
                   if ([fromShortCode isEqualToString:toShortCode]) {
                       
                       [self outboundc2cAPICall:ToNumber provider:[dic valueForKey:@"provider"] phoneNum:calling_number];
                   }
                   else
                   {
                       [UtilsClass makeToast:@"You can't make calls using india number to any other country"];
                   }
               }
        else
        {*/
            if ([fromShortCode isEqualToString:toShortCode]) {
                
               if([GlobalData getContryRestrictedForSameCallId:fromShortCode] == true)
               {
                   initiateCall = false;
                    [UtilsClass showAlert:@"Calling with the same caller id is not allowed" contro:vc];
               }
                else
                {
                    initiateCall = true;
                }
            }
            else
            {
                initiateCall = true;
            }
       // }
       
        if (initiateCall == true) {
        
            /*
        NSString *providerStr = [dic valueForKey:@"outgoingCallProvider"];
>>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
        
        if ([providerStr isEqualToString:@""] || providerStr == nil ) {
        
            NSLog(@"shortname to fetch provider :: %@",txtText.country.countryCode);
               
            providerStr = [GlobalData getOutCallProviderByCountry:txtText.country.countryCode];
        }
        
        NSLog(@"final provider :: %@",providerStr);
        */
        
        
        NSString *frontendprovider = [GlobalData getCallingProvider:txtText.country.countryCode];
               
               NSLog(@"final provider :: %@",frontendprovider);
        
            if ([frontendprovider isEqualToString:@"freeswitch"]) {
                
                [self makeCallThroughLinphone:address calltype:calltype name:name ToNumber:ToNumber vc:vc];
            }
            else if([frontendprovider isEqualToString:@"plivo"]){

                [self provider_vise_call:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
            }
            else
            {
                [self Twilio_calling:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
            }
        }
    }
    
}

//+(void)number_api_call:(NSString *)address calltype:(NSString *)calltype name:(NSString *)name ToNumber:ToNumber vc:(UIViewController *)vc countryCode:(NSString *)countryCode
//{
//    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
//    txtText.text = @"";
//    [txtText insertText:address];
//    NSString *code = txtText.code ? txtText.code : @"";
//    NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
//
//   //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
//
//    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
//    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//
//    // NSLog(@"Number : purchase_number : %@",purchase_number);
//    NSString *calling_number = [Default valueForKey:SELECTEDNO];
//    //  NSLog(@"Selected No : %@",calling_number);
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
//    NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
//    //   NSLog(@"numbergetarr numbergetarr : %@",numbergetarr);
//    if(numbergetarr.count != 0)
//    {
//        NSDictionary *numberdic = [numbergetarr objectAtIndex:0];
//        NSDictionary *dic = [numberdic valueForKey:@"number"];
//        NSString *number = [dic valueForKey:@"phoneNumber"];
//        NSString *outgoingCallProvider = [dic valueForKey:@"outgoingCallProvider"];
//         NSLog(@"*** outgoingCallProvider : number_api_call : %@",outgoingCallProvider);
//        if ([outgoingCallProvider isEqualToString:Login_Twilio])
//        {
//
//            [self Twilio_calling:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
//        }
//        else
//        {
//            [self provider_vise_call:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
//        }
//
//    }
//    else
//    {
//        [self provider_vise_call:address calltype:calltype name:name ToNumber:ToNumber vc:vc countryCode:txtText.country.countryCode];
//    }
//}
+(void)Twilio_calling:(NSString *)address calltype:(NSString *)calltype name:(NSString *)name ToNumber:ToNumber vc:(UIViewController *)vc countryCode:(NSString *)countryCode
{
                   NSString *calling_provider = @"";
                   calling_provider = Login_Twilio;
                   [Default setValue:calling_provider forKey:CallingProvider];
                   [Default synchronize];
                   
                   NSUUID *uuid = [NSUUID UUID];
                   NSString *handle = address;
                   [twilio_callkit sharedInstance].calls_uuids_twilio = [[NSMutableArray alloc] init];
                   [[twilio_callkit sharedInstance] performStartCallActionWithUUID:uuid handle:handle];
                   
                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                   OnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
              //  NewOnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

                   vc1.CallStatus = OUTGOING;//OUTGOING;
                   vc1.CallStatusfinal = calltype;
                   vc1.ContactName = name;
                   vc1.ContactNumber = ToNumber;
                   vc1.IncCallOutCall = @"Outgoing";
                   
                   if (IS_IPAD) {
                       vc1.popoverPresentationController.sourceView = vc1.view;
                       [vc presentViewController:vc1 animated:YES completion:nil];
                   } else {
                       vc1.modalPresentationStyle = UIModalPresentationFullScreen;
                       [vc presentViewController:vc1 animated:true completion:nil];
                   }
}
+(void)provider_vise_call:(NSString *)address calltype:(NSString *)calltype name:(NSString *)name ToNumber:(NSString *)ToNumber vc:(UIViewController *)vc countryCode:(NSString *)countryCode
{
    NSString *LoginProvider = [Default valueForKey:Login_Provider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        if ([address length] > 0) {
            
            NSString *calling_provider = @"";
            calling_provider = Login_Plivo;
            [Default setValue:calling_provider forKey:CallingProvider];
            [Default synchronize];
            
            [Default setValue:address forKey:CALL_NUMBER];
            [Lin_Utility plivo_call_action:address];
            

            
            PlivoOutgoing *outCall;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            OnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
            //NewOnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

            vc1.CallStatus = OUTGOING;//OUTGOING;
            vc1.CallStatusfinal = calltype;
            vc1.ContactName = name;
            vc1.ContactNumber = ToNumber;
            vc1.Plivo_outgoingcall = outCall;
            vc1.IncCallOutCall = @"Outgoing";
            if (IS_IPAD) {
                vc1.popoverPresentationController.sourceView = vc1.view;
                [vc presentViewController:vc1 animated:YES completion:nil];
            } else {
                vc1.modalPresentationStyle = UIModalPresentationFullScreen;
                [vc presentViewController:vc1 animated:true completion:nil];
            }
        }
        
    }
    
}


+(void)make_outgoing_call1:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact
{
    
    NSString *name = @"";
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:ACTIVE_SUB_USER];
    NSArray *activeSubuser = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *extentionName = @"";
    NSString *extentionNumber = @"";
    if([ToName isEqualToString:@""]) {
        name = ToNumber;
        //NSLog(@"Toname1 : %@",name);
    } else
    {
        name = ToName;
        //NSLog(@"Toname2 : %@",name);
    }
    
    
    //    NSString *address = [NSString stringWithFormat:@"%@",ToNumber];
    //NSLog(@"Trushang : Calling : Screen : make_outgoing_call1 : %@",vc.debugDescription);
    UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CalllogsVC *calllogs = [storyboard1 instantiateViewControllerWithIdentifier:@"CalllogsVC"];
    CalllogsDetailsVC *calllogsdetails = [storyboard1 instantiateViewControllerWithIdentifier:@"CalllogsDetailsVC"];
    
    
    ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    txtText.text = @"";
    [txtText insertText:ToNumber];
    NSString *code = txtText.code ? txtText.code : @"";
    NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
    [Default setValue:code forKey:Outgoing_Call_Code];
    [Default setValue:codeName forKey:Outgoing_Call_CodeName];
    
    //pri
    [Default setValue:ToNumber forKey:klastDialNumber];
    [Default setValue:txtText.country.name forKey:klastDialCountryName];
    [Default setValue:[NSString stringWithFormat:@"+%@",txtText.code] forKey:klastDialCountryCode];
                                                                        
//    NSLog(@"last dial code default:: %@",[Default valueForKey:klastDialCountryCode]);
//    NSLog(@"last dial number default:: %@",[Default valueForKey:klastDialNumber]);
//    NSLog(@"last dial country default:: %@",[Default valueForKey:klastDialCountryName]);
    
    [Default synchronize];
    //NSLog(@"Trushang : Patel : Dial Number Info %@",txtText.country.countryCode);
    //NSLog(@"Trushang : Patel : Dial Number Info %@",txtText.country.name);
    //NSLog(@"Trushang : Patel : Dial Number Info %@",txtText.code);
    
    if(vc == calllogs || vc == calllogsdetails)
    {
        
    }
    else
    {
        //         [self callinge_from_number_check];
    }
    
    
    
    
    //    NSString *countryCode = [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"%@",ToNumber]];
    //    //NSLog(@"Print out Going Call Provider : %@",countryCode);
    //    [Lin_Utility checkProvider:countryCode];
    //    [Lin_Utility checkProvider:countryCode];
    
    NSString *num = [ToNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
   
    
    //p NSPredicate *filter = [NSPredicate predicateWithFormat:@"number_int contains[c] %@ ",num];
    
    //-mixallcontact
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",num] ;
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num] ;
    
    NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];

    
    NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
    
    if(filteredContacts.count != 0)
    {
        NSDictionary *dic = [filteredContacts objectAtIndex:0];
        //  //NSLog(@"Call Contact Find search dic == == > : %@",dic);
        for (int i = 0; i<filteredContacts.count; i++)
        {
            //pNSString *numbers = [filteredContacts[i][@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            
            
            NSString *numbers;
            if ([[dic valueForKey:@"numberArray"] count]>0) {
                
                //numbers = [[[[dic valueForKey:@"numberArray"] objectAtIndex:i] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                numbers = [[[[[filteredContacts objectAtIndex:i] valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            else
            {
                numbers =@"";
            }

            
            if ([num isEqualToString:numbers]) {
                extentionName = filteredContacts[i][@"name"];
                break;
            }
        }
        
        if(!dic[@"_id"])
        {
            //NSLog(@"Call Contact Find search dic == == > : %@",dic);
            NSString *ContactName = [dic valueForKey:@"name"];
            //NSString *ContactNumber = [dic valueForKey:@"number"];
            NSString *ContactNumber;
            if ([[dic valueForKey:@"numberArray"] count]>0) {
                ContactNumber = [[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            else
            {
                ContactNumber =@"";
            }

            [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
        }
        
    }
    
    if (num.length == 3 || num.length == 4){
        
        NSString *extentionPlan = [Default valueForKey:EXTENSION_PLAN];
        int extentionInt = [extentionPlan intValue];
        if (extentionInt == 0){
            [UtilsClass showAlert:@"Internal team calling is not available in your plan Please Upgrade your plan" contro:vc];
            return;
        }
        
        NSString *ownExtNumber = [Default valueForKey:ownExtensionNumber];
        if ([ownExtNumber isEqualToString:num]) {
            [UtilsClass makeToast:@"You can't dial own extension number."];
            return;
        }
        
        BOOL invalidExtention = false;
        for (int i = 0; i<activeSubuser.count; i++)
        {
            //NSLog(@"extensionNumber  :%@",activeSubuser[i][@"extensionNumber"]);
            if ([num isEqualToString:[NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]]]){
                invalidExtention = false;
               if ([extentionName isEqualToString:@""]) {
                   extentionName = activeSubuser[i][@"fullName"];
               }
               extentionNumber = [NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]];
                [Default setValue:@"true" forKey:ExtentionCall];
                [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                [Default setValue:@"+1" forKey:klastDialCountryCode];
                [Default setValue:@"United States" forKey:klastDialCountryName];
                [self Twilio_calling1:activeSubuser[i][@"twilioEndpoint"][@"webIdentity"] calltype:calltype name:extentionName vc:vc countryCode:[NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]]];
                break;
                
            }else{
                invalidExtention = true;
            }
        }
        if (invalidExtention == true) {
            [UtilsClass makeToast:@"Please enter valid extension number."];
        }
    }else{
        [Default setValue:@"false" forKey:ExtentionCall];
        [Default setValue:@"" forKey:ExtentionCallProvider];
        NSArray *purchase_number = [[NSArray alloc] init];
        purchase_number = [self getNumbers];
        NSString *calling_number = [Default valueForKey:SELECTEDNO];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
        NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
        
        NSDictionary *numberdic;
        if ([numbergetarr count] > 0) {
            numberdic = [numbergetarr objectAtIndex:0];
        }
        else
        {
            numberdic = [purchase_number objectAtIndex:0 ];
            NSString *phoneNumber = [[numberdic objectForKey:@"number"] objectForKey:@"phoneNumber"];
            NSString *contactName = [[numberdic objectForKey:@"number"] objectForKey:@"contactName"];
            [Default setValue:phoneNumber forKey:SELECTEDNO];
            [Default setValue:contactName forKey:Selected_Department];
        }
        
        NSDictionary *dic = [numberdic valueForKey:@"number"];
//        NSString *twilioVerify = [dic valueForKey:@"twilioVerify"];
//        int twlVeri = [twilioVerify intValue];
//
//        if([[dic valueForKey:@"outgoingCallProvider"] isEqualToString:@"plivo"] && twlVeri == 0) {
        
    /*   NSLog(@"shortname to fetch provider :: %@",txtText.country.countryCode);
                    
        NSString *providerStr = [GlobalData getOutCallProviderByCountry:txtText.country.countryCode];
                
                NSLog(@"final provider :: %@",providerStr);
                
        
                if([providerStr isEqualToString:@"plivo"] ){
                    
                    [self provider_vise_call1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];

        }
        else
        {
            [self Twilio_calling1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
        }*/
        
// <<<<<<< HEAD
    /*old
     NSString *providerStr = [dic valueForKey:@"outgoingCallProvider"];
======= */
        
        
        NSString *fromShortCode = [dic valueForKey:@"shortName"];
        NSString *toShortCode = txtText.country.countryCode;
               
               BOOL initiateCall = false;
               
            /*   if ([fromShortCode isEqualToString:@"IN"] && [[dic valueForKey:@"provider"] isEqualToString: @"airtel"]) {
                   
                   initiateCall = false;
                   
                   if ([fromShortCode isEqualToString:toShortCode]) {
                       
                      
                       [self outboundc2cAPICall:ToNumber provider:[dic valueForKey:@"provider"] phoneNum:calling_number];
                   }
                   else
                   {
                       [UtilsClass makeToast:@"You can't make calls using india number to any other country"];
                   }
               }
        else
        {*/
            if ([fromShortCode isEqualToString:toShortCode]) {
                
               if([GlobalData getContryRestrictedForSameCallId:fromShortCode] == true)
               {
                   initiateCall = false;
                    [UtilsClass showAlert:@"Calling with the same caller id is not allowed" contro:vc];
               }
                else
                {
                    initiateCall = true;
                }
            }
            else
            {
                initiateCall = true;
            }
      //  }
       
        
        if (initiateCall == true) {
        
        /*
        NSString *providerStr = [dic valueForKey:@"outgoingCallProvider"];
>>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
        if ([providerStr isEqualToString:@""] || providerStr == nil ) {
        
            NSLog(@"shortname to fetch provider :: %@",txtText.country.countryCode);
               
            providerStr = [GlobalData getOutCallProviderByCountry:txtText.country.countryCode];
        }
        
        NSLog(@"final provider :: %@",providerStr);
        */
        
        NSString *frontendprovider = [GlobalData getCallingProvider:txtText.country.countryCode];
                      
                      NSLog(@"final provider :: %@",frontendprovider);

            if ([frontendprovider isEqualToString:@"freeswitch"]) {
                
                [self makeCallThroughLinphone:ToNumber calltype:calltype name:name ToNumber:ToNumber vc:vc];
            }
            else if([frontendprovider isEqualToString:@"plivo"]){

                [self provider_vise_call1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];

            }
            else
            {
                [self Twilio_calling1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
            }
        }
        
    }
}

+(void)make_outgoing_call_warmtransfer:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact
{
    
    NSString *name = @"";
    
    if([ToName isEqualToString:@""]) {
        name = ToNumber;
        //NSLog(@"Toname1 : %@",name);
    } else
    {
        name = ToName;
        //NSLog(@"Toname2 : %@",name);
    }
    
    
    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    
    if ([callfromnumber isEqualToString:@"SubUser"]){
        
    }else{
        ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        txtText.text = @"";
        [txtText insertText:ToNumber];
        NSString *code = txtText.code ? txtText.code : @"";
        NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
        [Default setValue:code forKey:Outgoing_Call_Code];
        [Default setValue:codeName forKey:Outgoing_Call_CodeName];
        
        //pri
        [Default setValue:ToNumber forKey:klastDialNumber];
        [Default setValue:txtText.country.name forKey:klastDialCountryName];
        [Default setValue:[NSString stringWithFormat:@"+%@",txtText.code] forKey:klastDialCountryCode];
                                                                            
        
        [Default synchronize];
        
        NSString *num = [ToNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        //NSPredicate *filter = [NSPredicate predicateWithFormat:@"number_int contains[c] %@ ",num];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",num] ;
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num] ;
        
        NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];

        
        NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
        
        if(filteredContacts.count != 0)
        {
            NSDictionary *dic = [filteredContacts objectAtIndex:0];
            //  //NSLog(@"Call Contact Find search dic == == > : %@",dic);
            
            if(!dic[@"_id"])
            {
                //NSLog(@"Call Contact Find search dic == == > : %@",dic);
                NSString *ContactName = [dic valueForKey:@"name"];
               // NSString *ContactNumber = [dic valueForKey:@"number"];
                
                NSString *ContactNumber;
                if ([[dic valueForKey:@"numberArray"] count]>0) {
                    ContactNumber = [[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                }
                else
                {
                    ContactNumber =@"";
                }
                
                [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
            }
            
        }
    }
    
    
    
    if ([callfromnumber isEqualToString:@"SubUser"]){
        [self Twilio_calling1:ToNumber calltype:calltype name:name vc:vc countryCode:@""];
    }else{
        [Default setValue:@"false" forKey:ExtentionCall];
        [Default setValue:@"" forKey:ExtentionCallProvider];
        NSArray *purchase_number = [[NSArray alloc] init];
        purchase_number = [self getNumbers];
        NSString *calling_number = [Default valueForKey:SELECTEDNO];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
        NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
        
        //crash
        //NSDictionary *numberdic = [numbergetarr objectAtIndex:0];
        
        NSDictionary *numberdic;
        if ([numbergetarr count] > 0) {
            numberdic = [numbergetarr objectAtIndex:0];
        }
        else
        {
            numberdic = [purchase_number objectAtIndex:0];
            NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
            NSString *contactName = [numberdic objectForKey:@"contactName"];
            [Default setValue:phoneNumber forKey:SELECTEDNO];
            [Default setValue:contactName forKey:Selected_Department];
        }
        
        NSDictionary *dic = [numberdic valueForKey:@"number"];

//        NSString *twilioVerify = [dic valueForKey:@"twilioVerify"];
//        int twlVeri = [twilioVerify intValue];
//
//        if([[dic valueForKey:@"outgoingCallProvider"] isEqualToString:@"plivo"] && twlVeri == 0) {
        
    /*    NSLog(@"shortname to fetch provider :: %@",txtText.country.countryCode);
                    
        NSString *providerStr = [GlobalData getOutCallProviderByCountry:txtText.country.countryCode];
                
                NSLog(@"final provider :: %@",providerStr);
                
        
                if([providerStr isEqualToString:@"plivo"] ){
                    
                    [self provider_vise_call1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];

        }
        else
        {
                [self Twilio_calling1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
        }*/
        
// <<<<<<< HEAD
     /*
      old
      NSString *providerStr = [dic valueForKey:@"outgoingCallProvider"];
======= */
        
        
        NSString *fromShortCode = [dic valueForKey:@"shortName"];
        NSString *toShortCode = txtText.country.countryCode;
               
               BOOL initiateCall = false;
               
           /*    if ([fromShortCode isEqualToString:@"IN"] && [[dic valueForKey:@"provider"] isEqualToString: @"airtel"]) {
                   
                   initiateCall = false;
                   
                   if ([fromShortCode isEqualToString:toShortCode]) {
                       
                      
                       [self outboundc2cAPICall:ToNumber provider:[dic valueForKey:@"provider"] phoneNum:calling_number];
                   }
                   else
                   {
                       [UtilsClass makeToast:@"You can't make calls using india number to any other country"];
                   }
               }
        else
        {*/
            if ([fromShortCode isEqualToString:toShortCode]) {
                
               if([GlobalData getContryRestrictedForSameCallId:fromShortCode] == true)
               {
                   initiateCall = false;
                    [UtilsClass showAlert:@"Calling with the same caller id is not allowed" contro:vc];
               }
                else
                {
                    initiateCall = true;
                }
            }
            else
            {
                initiateCall = true;
            }
      //  }
       
        
        if (initiateCall == true) {
/*
        NSString *providerStr = [dic valueForKey:@"outgoingCallProvider"];
>>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
        if ([providerStr isEqualToString:@""] || providerStr == nil ) {
        
            NSLog(@"shortname to fetch provider :: %@",txtText.country.countryCode);
               
            providerStr = [GlobalData getOutCallProviderByCountry:txtText.country.countryCode];
        }
        
        NSLog(@"final provider :: %@",providerStr);
        */
        
        NSString *frontendprovider = [GlobalData getCallingProvider:txtText.country.countryCode];
                            
                            NSLog(@"final provider :: %@",frontendprovider);

            if ([frontendprovider isEqualToString:@"freeswitch"]) {
                
                [self makeCallThroughLinphone:ToNumber calltype:calltype name:name ToNumber:ToNumber vc:vc];
            }
            else if([frontendprovider isEqualToString:@"plivo"]){

                [self provider_vise_call1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];

            }
            else
            {
                [self Twilio_calling1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
            }
        }
    }
    
    
}


//+(void)number_api_call_1:(NSString *)ToNumber calltype:(NSString *)calltype name:(NSString *)name vc:(UIViewController *)vc countryCode:(NSString *)countryCode
//{
//
//    ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
//    txtText.text = @"";
//    [txtText insertText:ToNumber];
//
//   //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
//
//    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
//    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//
//    //    NSLog(@"Number : purchase_number : %@",purchase_number);
//    NSString *calling_number = [Default valueForKey:SELECTEDNO];
//    //   NSLog(@"Selected No : %@",calling_number);
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
//    NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
//    //   NSLog(@"numbergetarr numbergetarr : %@",numbergetarr);
//    if(numbergetarr.count != 0)
//    {
//        NSDictionary *numberdic = [numbergetarr objectAtIndex:0];
//        NSDictionary *dic = [numberdic valueForKey:@"number"];
//        NSString *number = [dic valueForKey:@"phoneNumber"];
//        NSString *outgoingCallProvider = [dic valueForKey:@"outgoingCallProvider"];
//        NSLog(@"*** outgoingCallProvider : number_api_call_1 : %@",outgoingCallProvider);
//        if ([outgoingCallProvider isEqualToString:Login_Twilio])
//        {
//           [self Twilio_calling1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
//        }
//        else
//        {
//            [self provider_vise_call1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
//        }
//    }
//    else
//    {
//        [self provider_vise_call1:ToNumber calltype:calltype name:name vc:vc countryCode:txtText.country.countryCode];
//    }
//}
+(void)Twilio_calling1:(NSString *)ToNumber calltype:(NSString *)calltype name:(NSString *)name vc:(UIViewController *)vc countryCode:(NSString *)countryCode
{
    NSString *calling_provider = @"";
    NSString *extentionCall = [Default valueForKey:ExtentionCall];
    calling_provider = Login_Twilio;
    [Default setValue:calling_provider forKey:CallingProvider];
    [Default synchronize];
    
    NSUUID *uuid = [NSUUID UUID];
    NSString *handle = ToNumber;
    [twilio_callkit sharedInstance].calls_uuids_twilio = [[NSMutableArray alloc] init];
    [[twilio_callkit sharedInstance] performStartCallActionWithUUID:uuid handle:handle];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
    //NewOnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

    vc1.CallStatus = OUTGOING;//OUTGOING;
    vc1.CallStatusfinal = calltype;
    vc1.ContactName = name;
    if ([extentionCall isEqualToString:@"true"]) {
        vc1.ContactNumber = countryCode;
    }else{
        vc1.ContactNumber = ToNumber;
    }
    
    vc1.IncCallOutCall = @"Outgoing";
    
    if (IS_IPAD) {
        vc1.popoverPresentationController.sourceView = vc1.view;
        [vc presentViewController:vc1 animated:YES completion:nil];
    } else {
        vc1.modalPresentationStyle = UIModalPresentationFullScreen;
        [vc presentViewController:vc1 animated:true completion:nil];
    }
}

+(void)provider_vise_call1:(NSString *)ToNumber calltype:(NSString *)calltype name:(NSString *)name vc:(UIViewController *)vc countryCode:(NSString *)countryCode
{
    NSString *LoginProvider = [Default valueForKey:Login_Provider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        if ([ToNumber length] > 0) {
            
            NSString *calling_provider = @"";
            calling_provider = Login_Plivo;
            [Default setValue:calling_provider forKey:CallingProvider];
            [Default synchronize];
            
            [Default setValue:ToNumber forKey:CALL_NUMBER];
            
            [Lin_Utility plivo_call_action:ToNumber];
            
            PlivoOutgoing *outCall;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            OnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
            //NewOnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

            vc1.CallStatus = OUTGOING;//OUTGOING;
            vc1.CallStatusfinal = calltype;
            vc1.ContactName = name;
            vc1.ContactNumber = ToNumber;
            vc1.Plivo_outgoingcall = outCall;
            vc1.IncCallOutCall = @"Outgoing";
            if (IS_IPAD) {
                vc1.popoverPresentationController.sourceView = vc1.view;
                [vc presentViewController:vc1 animated:YES completion:nil];
            } else {
                vc1.modalPresentationStyle = UIModalPresentationFullScreen;
                [vc presentViewController:vc1 animated:true completion:nil];
            }
        }
    }

}


+(void)make_outgoing_call_validate:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact
{
//    NSString *ipcode  =  [Default valueForKey:CALLING_CODE];
//    ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString *contactcode  = [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"%@",ToNumber]];
    NSData *outGoingCallData = [[NSUserDefaults standardUserDefaults] objectForKey:OUTGOING_CALL_COUNTRY];
    NSArray *outGoingCallRest = [NSKeyedUnarchiver unarchiveObjectWithData:outGoingCallData];
    ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    txtText.text = @"";
    [txtText insertText:ToNumber];
    for (int i = 0; i<outGoingCallRest.count; i++)
    {
        NSLog(@"extensionNumber  :%@",txtText.code);
        if ([txtText.code isEqualToString:[NSString stringWithFormat:@"%@",outGoingCallRest[i][@"code"]]] && ToNumber.length != 4){
            [UtilsClass showAlert:@"Calls to this country are restricted." contro:vc];
            return;
        }
    }
//    ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    //NSLog(@"ToNumber : %@",[NSString stringWithFormat:@"%@",ToNumber]);
    //NSLog(@"ipcode : %@",ipcode);
    //NSLog(@"contactcode : %@",contactcode);
    
//    NSString *removeCountryCode = [[NSString stringWithFormat:@"+%@",ToNumber] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"+%@",contactcode] withString:@""];
    
//    if ([[NSString stringWithFormat:@"+%@",ToNumber] rangeOfString:[NSString stringWithFormat:@"+%@",contactcode]].location == NSNotFound) {
//        NSString *lastDialCountryCode = [Default valueForKey:LAST_COUNTRY_CODE];
//        lastDialCountryCode = [lastDialCountryCode stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        ToNumber = [NSString stringWithFormat:@"+%@%@",lastDialCountryCode,ToNumber];
//    } else {
//        //NSLog(@"string contains bla!");
//    }

    if ([ToNumber rangeOfString:@"+"].location == NSNotFound) {
        NSString *lastDialCountryCode = [Default valueForKey:LAST_COUNTRY_CODE];
        lastDialCountryCode = [lastDialCountryCode stringByReplacingOccurrencesOfString:@"+" withString:@""];
        ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
        ToNumber = [NSString stringWithFormat:@"+%@%@",lastDialCountryCode,ToNumber];
    } else {
        //NSLog(@"string contains bla!");
    }
    
//    removeCountryCode = [removeCountryCode stringByReplacingOccurrencesOfString:@"+" withString:@""];
//    ToNumber = [NSString stringWithFormat:@"+%@%@",contactcode,removeCountryCode];
//
    //NSLog(@"FinalToToNumber : %@",[NSString stringWithFormat:@"%@",ToNumber]);
//
//    NSString *useForCheckValid  =  [UtilsClass phoneNumberValid:[NSString stringWithFormat:@"%@",ToNumber]];
//
//    if([useForCheckValid isEqualToString:@"invalid_number"])
//    {
//        ToNumber = [[NSString stringWithFormat:@"+%@",ToNumber] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"+%@",contactcode] withString:@""];
//        NSString *lastDialCountryCode = [Default valueForKey:LAST_COUNTRY_CODE];
//        lastDialCountryCode = [lastDialCountryCode stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        ToNumber = [NSString stringWithFormat:@"+%@%@",lastDialCountryCode,ToNumber];
//    }
//
    
//    if([ipcode isEqualToString:[NSString stringWithFormat:@"+%@",contactcode]])
//    {
//        if([ipcode isEqualToString:@"+91"])
//        {
//            [UtilsClass showAlert:CALLBAND contro:vc];
//        }
//        else
//        {
//            //[self callme:sender];
//            if([contactcode isEqualToString:@"invalid_number"])
//            {
//                [UtilsClass makeToast:@"The dialed number might be Invalid."];
//                [self make_outgoing_call:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
//            }
//            else{
//                [self make_outgoing_call:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
//            }
//        }
//    }
//    else
//    {
        if([contactcode isEqualToString:@"invalid_number"])
        {
            ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            if (ToNumber.length != 3 && ToNumber.length != 4){
                [UtilsClass makeToast:@"The dialed number might be Invalid."];
            }
            ToNumber = [NSString stringWithFormat:@"+%@",ToNumber];
            [self make_outgoing_call:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
//            }else {
//                [self make_outgoing_call:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
//            }
            
//            [UtilsClass showAlert:@"Please enter valid number." contro:vc];
        }
        else
        {
             [self make_outgoing_call:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
        }
//    }
}

+(void)make_outgoing_call_validate1:(UIViewController *)vc callfromnumber:(NSString *)callfromnumber ToName:(NSString *)ToName ToNumber:(NSString *)ToNumber calltype:(NSString *)calltype Mixallcontact:(NSMutableArray *)Mixallcontact
{
 
    NSString *contactcode  =  [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"%@",ToNumber]];
    NSData *outGoingCallData = [[NSUserDefaults standardUserDefaults] objectForKey:OUTGOING_CALL_COUNTRY];
       NSArray *outGoingCallRest = [NSKeyedUnarchiver unarchiveObjectWithData:outGoingCallData];
       ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
       NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
       txtText.text = @"";
       [txtText insertText:ToNumber];
       
       for (int i = 0; i<outGoingCallRest.count; i++)
       {
           NSLog(@"extensionNumber  :%@",txtText.code);
           if ([txtText.code isEqualToString:[NSString stringWithFormat:@"%@",outGoingCallRest[i][@"code"]]] && ToNumber.length != 4){
               [UtilsClass showAlert:@"Calls to this country are restricted." contro:vc];
               return;
           }
       }
    if([contactcode isEqualToString:@"invalid_number"])
    {
        ToNumber = [ToNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
        if (ToNumber.length != 3 && ToNumber.length != 4){
            [UtilsClass makeToast:@"The dialed number might be Invalid."];
        }
        ToNumber = [NSString stringWithFormat:@"+%@",ToNumber];
        [self make_outgoing_call1:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
    }
    else
    {
         [self make_outgoing_call1:vc callfromnumber:callfromnumber ToName:ToName ToNumber:ToNumber calltype:calltype Mixallcontact:Mixallcontact];
    }
}

+(void)makeCallThroughLinphone:(NSString *)address calltype:(NSString *)calltype name:(NSString *)name ToNumber:(NSString *)ToNumber vc:(UIViewController *)vc
{

    if (!linphone_core_is_network_reachable(LC)) {
        
        [LinphoneManager.instance setupNetworkReachabilityCallback];
    }
    
    NSString *calling_provider = @"";
    calling_provider = Login_Linphone;
    [Default setValue:calling_provider forKey:CallingProvider];
    [Default synchronize];
    
    
   //[Lin_Utility checkProvider:countryCode];
    
    if ([address length] > 0) {
        
        if([UtilsClass isNetworkAvailable])
        {
        LinphoneManager.instance.providerDelegate.calls_uuids = [[NSMutableArray alloc] init];
        LinphoneAddress *addr = [LinphoneUtils normalizeSipOrPhoneAddress:address];
        [LinphoneManager.instance call:addr];
        if (addr) {
            linphone_address_destroy(addr);
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            OnCallVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
            vc1.CallStatus = OUTGOING;//OUTGOING;
            vc1.CallStatusfinal = calltype;
            vc1.ContactName = name;
            vc1.ContactNumber = ToNumber;
            vc1.IncCallOutCall = @"Outgoing";
            
            if (IS_IPAD) {
                vc1.popoverPresentationController.sourceView = vc1.view;
                [vc presentViewController:vc1 animated:YES completion:nil];
            } else {
                vc1.modalPresentationStyle = UIModalPresentationFullScreen;
                [vc presentViewController:vc1 animated:true completion:nil];
            }
            
        }
        else
        {
            
        }
        }
        else
        {
            [UtilsClass showAlert:@"Please check your internet connection and try again." contro:vc];
        }
        
    }
}



+(void)contact_save_in_callhippo:(NSString *)contact_name contact_number:(NSString *)contact_number
{
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *add = @"add";
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",GETCONTACT_URL,userId,add];
    
    NSDictionary *passDict = @{@"name":contact_name,
                               @"contactType" : @"mobile",
                               @"number": contact_number};
    //NSLog(@"Dictonary : %@",passDict);
    
    //NSLog(@"url : %@%@",SERVERNAME,url);
    WebApiController *obj;
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(contact_save_in_callhippo_response:response:) andDelegate:self];
    
}
+(void)contact_save_in_callhippo_response:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************2  : %@",apiAlias);
    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    //NSLog(@" contact_save_in_callhippo_response  : %@",response1);
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1)
    {
        
    }
    else
    {
       
    }
        
    }
}
+(NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    //NSLog(@"code : %@",code);
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      : @"Simulator",
                              @"x86_64"    : @"Simulator",
                              @"iPod1,1"   : @"iPod Touch",        // (Original)
                              @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" : @"iPhone",            // (Original)
                              @"iPhone1,2" : @"iPhone",            // (3G)
                              @"iPhone2,1" : @"iPhone",            // (3GS)
                              @"iPad1,1"   : @"iPad",              // (Original)
                              @"iPad2,1"   : @"iPad 2",            //
                              @"iPad3,1"   : @"iPad",              // (3rd Generation)
                              @"iPhone3,1" : @"iPhone 4",          // (GSM)
                              @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" : @"iPhone 4S",         //
                              @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   : @"iPad",              // (4th Generation)
                              @"iPad2,5"   : @"iPad Mini",         // (Original)
                              @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" : @"iPhone 6 Plus",     //
                              @"iPhone7,2" : @"iPhone 6",          //
                              @"iPhone8,1" : @"iPhone 6S",         //
                              @"iPhone8,2" : @"iPhone 6S Plus",    //
                              @"iPhone8,4" : @"iPhone SE",         //
                              @"iPhone9,1" : @"iPhone 7",          //
                              @"iPhone9,3" : @"iPhone 7",          //
                              @"iPhone9,2" : @"iPhone 7 Plus",     //
                              @"iPhone9,4" : @"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",          // CDMA
                              @"iPhone10,4": @"iPhone 8",          // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                              @"iPhone10,3": @"iPhone X",          // CDMA
                              @"iPhone10,6": @"iPhone X",          // GSM
                              @"iPhone11,2": @"iPhone XS",         //
                              @"iPhone11,4": @"iPhone XS Max",     //
                              @"iPhone11,6": @"iPhone XS Max",     // China
                              @"iPhone11,8": @"iPhone XR",         //
                              
                              @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = code;
        }
        else {
            deviceName = @"Unknown";
            
            
        }
    }
    
    return deviceName;
}
-(void)show_blur:(UIViewController *)vc
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = vc.view.bounds;
    [vc.view addSubview:visualEffectView];
}

+ (void)makeToast:(NSString*)toastMsg
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.window.rootViewController.view makeToast:toastMsg];
   
}
+ (void)makeToast:(NSString*)toastMsg inView:(UIView *)view
{
    [view makeToast:toastMsg];
}
- (void) socketConnection
{
    
}


+(void)callinge_from_number_check
{
    NSArray *purchase_number = [[NSArray alloc] init];
    purchase_number = [self getNumbers];
    NSString *numbVer = [Default valueForKey:IS_AutoSwitch];
    int num = [numbVer intValue];
                   
  if (num == 1)
  {
        NSString *code = [Default valueForKey:Outgoing_Call_Code];
        NSString *codeName = [Default valueForKey:Outgoing_Call_CodeName];
        
//        if([[Default valueForKey:Outgoing_Call_CodeName] isEqualToString:codeName])
//        {
//
//        }
//        else
//        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.shortName  contains[c] %@",codeName];
            
            NSArray *filteredData = [purchase_number filteredArrayUsingPredicate:predicate];
            //NSLog(@"callinge_from_number_check : filteredData : %@",filteredData);
            if (filteredData.count != 0)
            {
                NSDictionary *dic1 = [filteredData objectAtIndex:0];
                NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                NSString *contactName = [numberdic objectForKey:@"contactName"];
                NSString *shortName = [numberdic objectForKey:@"shortName"];
              //  [self makeToast:[NSString stringWithFormat:@"Switched to %@ number",shortName]];
                [self makeToast:[NSString stringWithFormat:@"Your number is switched based on the dialed number"]];
                //NSLog(@"Selected No : callinge_from_number_check : %@",phoneNumber);
                [Default setValue:phoneNumber forKey:SELECTEDNO];
                [Default setValue:contactName forKey:Selected_Department];
                [Default setValue:@"" forKey:Outgoing_Call_Code];
                [Default setValue:@"" forKey:Outgoing_Call_CodeName];
                [Default synchronize];
                
            }
            else
            {
                
            }
//        }
        
    }
    else
    {
        
    }
    
    
}
+(NSArray*)getNumbers
{
    
   NSArray *purchase_number = [[NSArray alloc] init];
   //pri  purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        //NSLog(@" purchase_number : sharedGlobalData : %@ ",purchase_number);
        
    }
    else
    {
        if ([Default objectForKey:PURCHASE_NUMBER] != nil)
        {
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:PURCHASE_NUMBER];
            purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            //NSLog(@" purchase_number : NSUserDefaults : %@ ",purchase_number);
            
        }
    }
    return purchase_number;
}

+(void)logoutUser:(UIViewController *)viewcontroler error:(NSString *)error showAlert:(BOOL)showAlert
{
    
   // [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
    if (showAlert && ![error isEqualToString:@""]) {
        
        [UtilsClass showAlert:error contro:viewcontroler];
        
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Callhippo"
//                                                                       message:error
//                                                                preferredStyle:(UIAlertControllerStyleAlert)];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
//                                                           style:UIAlertActionStyleDefault
//                                                         handler:^(UIAlertAction * _Nonnull action) {
//                                                             // handle response here.
//
//
//            [self callLoginAPI:viewcontroler];
//
//        }];
//
//
//
//
//        [alert addAction:okAction];
//        [viewcontroler presentViewController:alert animated:YES completion:^{
//
//
//        }];
        
          [self callLoginAPI:viewcontroler];
    }
    else
    {
    
        [self callLoginAPI:viewcontroler];
    }
    
    
}
+(void)callLoginAPI:(UIViewController *)viewcontroler
{
    
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         
         
    NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"] ? [Default valueForKey:@"VOIPTOKEN"] : @"";
                  NSString *twilio_token_string = [Default valueForKey:twilio_token] ? [Default valueForKey:twilio_token] : @"";
                  
                  if(![twilio_token_string isEqualToString:@""])
                  {
                      [TwilioVoiceSDK unregisterWithAccessToken:twilio_token_string
                                                 deviceToken:[Default valueForKey:@"VOIPTOKEN_DATA"]
                                                  completion:^(NSError * _Nullable error) {
                          if (error) {
                              NSLog(@" Twilio : Unregister :  An error occurred while unregistering: %@", [error localizedDescription]);
                              NSString *TwilioToken = @"";
                              [Default setValue:TwilioToken forKey:twilio_token];
                              NSLog(@"Utilsclass : 1 :  Twilio token : %@",TwilioToken);
                          }
                          else {
                              NSLog(@" Twilio : Unregister :  Successfully unregistered for VoIP push notifications.");
                              NSString *TwilioToken = @"";
                              [Default setValue:TwilioToken forKey:twilio_token];
                               NSLog(@"Utilsclass : 2 :  Twilio token : %@",TwilioToken);
                          }
                      }];
                  }
                  else
                  {
                      NSString *TwilioToken = @"";
                      [Default setValue:TwilioToken forKey:twilio_token];
                      NSLog(@"Utilsclass : 3 :  Twilio token : %@",TwilioToken);
                  }
               
               NSString *user_email = [Default valueForKey:kUSEREMAIL];
               if (user_email != (id)[NSNull null] && user_email != nil )
               {
                   [FIRAnalytics logEventWithName:@"ch_logout" parameters:@{@"emailId": user_email}];
                   
               }
               //    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
               //    DialerVC *Dialer = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
               //    UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
               //    [navigationController setViewControllers:@[Dialer]];
               //  [mainViewController hideLeftViewAnimated:YES completionHandler:nil];
               [Processcall showLoadingWithView:viewcontroler.navigationController.view withLabel:nil];
               NSDictionary *passDict = @{@"type":@"ios"};
               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                                  options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                    error:nil];
               NSString *jsonString;
               if (! jsonData) {
                   
               } else {
                   jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
               }
               WebApiController *obj;
               obj = [[WebApiController alloc] init];
               
               //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
               NSString *userId = [Default valueForKey:USER_ID];
               NSString *url = [NSString stringWithFormat:@"logout/%@",userId];
               //NSLog(@"Logout_url : %@",url);
               [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
         
     });
}
+ (void)login:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE ************** logoutUser  : %@",response1);
    //NSLog(@"TRUSHANG : STATUSCODE ************** logoutUser  : %@",apiAlias);
//    if([apiAlias isEqualToString:Status_Code])
//    {
//        UIViewController *view = [[UIViewController alloc] init];
//        [UtilsClass logoutUser:view];
//    }
//    else
//    {
    
    //NSLog(@"logout response : %@",response1);
    if ([[response1 valueForKey:@"success"] integerValue] == 1)
    {
        [Default removeObjectForKey:ENDPOINT_USERNAME];
        [Default removeObjectForKey:ENDPOINT_PASSORD];
        [Default removeObjectForKey:USER_ID];
        [Default removeObjectForKey:AUTH_TOKEN];
        [Default removeObjectForKey:SELECTED_NUMBER_VERIFY]; // not
        [Default removeObjectForKey:SELECTEDNO]; // not
        [Default removeObjectForKey:CALLHIPPO_AUTH_ID];
        [Default removeObjectForKey:CALLHIPPO_AUTH_TOKEN];
        [Default removeObjectForKey:FULL_NAME];
        [Default removeObjectForKey:NUMBERID];  // not
        [Default removeObjectForKey:kUSERNAME];  // not
        [Default removeObjectForKey:kPASSWORD];  // not
        [Default removeObjectForKey:PLAN_DISPLAY_NAME];
        [Default removeObjectForKey:kCALLSINFO];  // not
        [Default removeObjectForKey:kCALLHIPPOCONTACT];  // not
        [Default removeObjectForKey:kMODUALRIGHTS];
        [Default removeObjectForKey:kUSEREMAIL];
        [Default removeObjectForKey:PARENT_ID];
        [Default removeObjectForKey:CALL_TRANSFER];
        [Default removeObjectForKey:CALL_SID];
        [Default removeObjectForKey:Login_Plivo];
        
        [Default removeObjectForKey:CallingProvider];
        [Default removeObjectForKey:DefaultProvider];
        
        [Default setValue:@"logout" forKey:UserLoginStatus];
        
        [Default removeObjectForKey:PURCHASE_NUMBER];
        
        [Default removeObjectForKey:klastDialCountryName];
        [Default removeObjectForKey:klastDialCountryCode];
        [Default removeObjectForKey:klastDialNumber];
        
        if ([[[GlobalData sharedGlobalData] chContactListFromDB] count] > 0) {
            [[[GlobalData sharedGlobalData] chContactListFromDB] removeAllObjects];
        }
        
        [Default setBool:false forKey:kisContactSyncingRunning];
        //        [Default setValue:@"Dialer" forKey:SelectedSideMenu];
        [Default synchronize];
        
//        [[[GlobalData sharedGlobalData] callLogs_global_array] removeAllObjects];
        
        [[CallLogsModel sharedCallLogsData] removeAllObjectsFromArray:ALLLOGS];
        [[CallLogsModel sharedCallLogsData] removeAllObjectsFromArray:MISSEDLOGS];
        [[CallLogsModel sharedCallLogsData] removeAllObjectsFromArray:VOICEMAILLOGS];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
      //  [self linphone_logout];
        
        [[Phone sharedInstance]logout];
        
//        linphone_core_clear_proxy_config(LC);
//        linphone_core_clear_all_auth_info(LC);
        
        
        
//         AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIWindow *mainWindow = [appDelegate window];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        LoginViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [navigationController setViewControllers:@[vc] animated:false];
        
        MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        mainViewController.rootViewController = navigationController;
        [mainViewController setupWithType:11];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
        navController.navigationBar.hidden = true;
        mainWindow.rootViewController = navController;
        [mainWindow setNeedsLayout];
        
    }
    else
    {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
                [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
            }
        }
        @catch (NSException *exception) {
        }
    }
        
//    }
}

+(void)linphone_logout
{
    
    [[GIDSignIn sharedInstance] signOut];
    
    NSString *LoginProvider = [Default valueForKey:Login_Provider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        [[Phone sharedInstance]logout];
        
    }
   
}
+ (void)makeAlertInWindow:(NSString*)message{
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        topWindow.hidden = YES;
        
    }]];
    
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}
+(void)twilio_access_token_get
{
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"";
    url = [NSString stringWithFormat:@"updateTwilioToken/%@/ios",userId];
    
    
    //NSLog(@"Trush Calllogs : URL  : \n \n %@%@ \n \n ",SERVERNAME,url);
    WebApiController *obj;
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(twilio_access_token_get_response:response:) andDelegate:self];
}
+ (void)twilio_access_token_get_response:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    NSLog(@"twilio_access_token_get_response : %@ ",response1);
    if([apiAlias isEqualToString:Status_Code])
    {
        //  [UtilsClass logoutUser:self];
    }
    else
    {
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"] ? [Default valueForKey:@"VOIPTOKEN"] : @"";
            NSString *twilio_token_string = [Default valueForKey:twilio_token] ? [Default valueForKey:twilio_token] : @"";
            
//            [TwilioVoice unregisterWithAccessToken:twilio_token_string
//                                       deviceToken:Voiptoken
//                                        completion:^(NSError * _Nullable error) {
//                NSString *TwilioToken = @"";
//                [Default setValue:TwilioToken forKey:twilio_token];
//                NSLog(@"Utilsclass : 4 :  Twilio token : %@",TwilioToken);
//            }];
            
            
            NSDictionary *data = [response1 valueForKey:@"data"];
            NSString *access_token = [data valueForKey:@"token"] ? [data valueForKey:@"token"] : @"";
            //NSLog(@"access_token : %@",access_token);
            [Default setValue:access_token forKey:twilio_token];
            NSLog(@"Utilsclass : 5 :  Twilio token : %@",access_token);
            [Default synchronize];
        }
    }
}

+(void)twilio_token_registration{
    
    NSString *Voiptoken = [Default valueForKey:@"VOIPTOKEN"];
    NSString *TwilioToken = [Default valueForKey:twilio_token];
    if(![TwilioToken isEqualToString:@""])
    {
        //set twilio edge
        if ([Default valueForKey:kTwilioEdgeValue]) {
            if (![[Default valueForKey:kTwilioEdgeValue] isEqualToString:@""] ) {
                
               [TwilioVoiceSDK setEdge:[Default valueForKey:kTwilioEdgeValue]];
            }
        }
        
        
        
        [TwilioVoiceSDK registerWithAccessToken:TwilioToken
                                 deviceToken:[Default valueForKey:@"VOIPTOKEN_DATA"]
                                  completion:^(NSError *error) {
                                      if (error) {
                                          //NSLog(@"\n  ---***---***---***---*** \n");
                                          NSLog(@" twilio :  An error occurred while registering:---> %@", [error localizedDescription]);
                                          //NSLog(@"\n  ---***---***---***---*** \n");
                                          [self twilio_access_token_get];
                                      }
                                      else {
                                          //NSLog(@"\n  ---***---***---***---*** \n");
                                          NSLog(@" twilio :  Successfully registered for VoIP push notifications.");
                                          //NSLog(@"\n  ---***---***---***---*** \n");
                                      }
                                  }];
    }
}
+(NSString *)get_masked_number:(NSString *)number
{
    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    txtText.text = @"";
    [txtText insertText:number];
    NSString *code = txtText.code ? txtText.code : @"";

    NSLog(@"code>> %@",code);
    NSString *maskedNum = @"";
    BOOL plusflag = false;
    if ([number containsString:@"+"]) {
        maskedNum = @"+";
        plusflag = true;
    }
   
    maskedNum = [NSString stringWithFormat:@"%@%@",maskedNum,code];
    int p;
    if (plusflag == true) {
        p = (int) code.length +1 ;
    }
    else
    {
      p = (int) code.length ;
    }
    
    int n = p + 3;
    for (int i = p; i<number.length; i++) {
        
        
        //for last char & first 3  display and other X
        if (i == number.length - 1 || i < n) {
            maskedNum = [NSString stringWithFormat:@"%@%c",maskedNum,[number characterAtIndex:i]];
            //NSLog(@"masked num in if : %@",maskedNum);
        }
        else
        {
            maskedNum = [NSString stringWithFormat:@"%@X",maskedNum];
            //NSLog(@"masked num in else : %@",maskedNum);

        }
        
    }
    

//    if (number.length > 4) {
//        NSString *firsthalf = [number substringToIndex:4];
//        NSString *lasthalf = [number substringFromIndex:number.length-2];
//        NSString *maskedString = [NSString stringWithFormat:@"%@XXXX%@",firsthalf,lasthalf];
//
//        NSLog(@"masked num :%@",maskedString);
//        return maskedString;
//    }
//    else
//    {
//        NSString *firsthalf = [number substringToIndex:number.length];
//        NSString *lasthalf = [number substringFromIndex:number.length-2];
//        NSString *maskedString = [NSString stringWithFormat:@"%@XXXX%@",firsthalf,lasthalf];
//
//        NSLog(@"masked num :%@",maskedString);
//        return maskedString;
//    }
    
    return maskedNum;
    
}
/*
#pragma mark - Airtel calling
+(void)outboundc2cAPICall:(NSString *)toNum provider:(NSString *)provider phoneNum:(NSString *)phoneNum
    {
        NSString *userID = [Default valueForKey:USER_ID];

        NSString *url = [NSString stringWithFormat:@"outboundC2CCall"];
        NSDictionary *passDict = @{@"userId":userID,
                                   @"toNumber":toNum,
                                   @"provider":provider,
                                   @"phoneNumber":phoneNum,
                                   };
        NSLog(@"outbound c2c : %@",passDict);
        NSLog(@"outbound c2c : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
       WebApiController *obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(outboundc2c:response:) andDelegate:self];

    }

+(void)outboundc2c:(NSString *)apiAlias response:(NSData *)response{
        
        
        NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
        
        if([apiAlias isEqualToString:Status_Code])
        {
            //[UtilsClass logoutUser:self];
             [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
                // NSLog(@"Trushang : getCredits : %@",response1);
            NSLog(@"Encrypted Response : outbound c2c : %@",response1);
            
            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                [UtilsClass makeToast:[response1 valueForKey:@"message"]];
            }
            else{
                [UtilsClass makeToast:[[response1 valueForKey:@"error"] valueForKey:@"error"]];
            }
        }
    }*/
@end

