//
//  GlobalData.h
//  callhippolin
//
//  Created by Admin on 07/06/19.
//  Copyright © 2019 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataManager.h"
#import "Contact+CoreDataClass.h"
#import "CoreDataManager.h"
@import SocketIO;
NS_ASSUME_NONNULL_BEGIN

@interface GlobalData : NSObject {
    NSString *message; // global variable
}

@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSMutableArray *global_array;
@property (nonatomic, retain) NSMutableArray *phone_contact_list;
@property (nonatomic, retain) NSMutableArray *callhippo_contact_list;
@property (nonatomic, retain) NSMutableArray *mixall_contact_array;
@property (nonatomic, retain) NSMutableArray *Number_Selection;
@property (nonatomic, retain) NSMutableArray *outcallprovider;

@property (nonatomic, retain) NSMutableArray *callCountryProviderList;
@property (nonatomic, retain) NSMutableArray *sameCallerIdRestricCountries;


@property (nonatomic, retain) NSMutableArray *callLogs_global_array;

@property (strong, atomic) SocketManager* manager;
@property (strong, atomic) SocketIOClient* socket;

@property (strong, atomic) SocketManager* wake_manager;
@property (strong, atomic) SocketIOClient* wake_socket;

@property (strong, atomic) NSString* Socket_Url;
@property (strong, atomic) CoreDataManager *coredbmanager;

@property (nonatomic, retain) NSMutableArray *chAndDeviceContactList;
@property (nonatomic, retain) NSMutableArray *chContactListFromDB;

@property (nonatomic, retain) NSMutableArray *updatedContactArray;

//+(NSString *)getOutCallProviderByCountry:(NSString *)shortName;
// <<<<<<< HEAD
//=======
//
// >>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
+(BOOL)isNumberBlacklisted:(NSString *)number;

+ (GlobalData*)sharedGlobalData;
+ (void)get_all_callhippo_contacts;
+ (void)get_all_phone_contacts;
+ (void)get_callhippo_number_selection;
- (void)get_Updated_Purchased_Number;


//+(void)getEndPoint:(NSString *)lastCallDate;
+ (void)get_outcallprovider;
//- (void)get_updated_purchased_numberResponse:(NSString *)apiAlias response:(NSData *)response;
//+(void)getEndPointsApiCall;

// global function
- (void) myFunc;
- (NSMutableArray*)get_phone_contact_list;
//- (NSMutableArray*)get_callhippo_contact_list;
//- (NSMutableArray*)get_mix_contact_list;
- (NSMutableArray*)get_number_selection;
- (NSMutableArray*)get_all_provider;

-(NSMutableArray *)get_callCountryProviderList;



-(void)edit_contact:(int)index newdic:(NSDictionary *)newdic olddic:(NSMutableDictionary *)olddic title:(NSString *)title;

- (void)addDataTo_global_callarray:(NSMutableArray *)array;
- (void)replaceDataTo_global_callarray:(NSMutableArray *)array;
-(void)getUpdatedBlackListedArray;

// <<<<<<< HEAD
//+(NSString *)getOutCallProviderByCountry:(NSString *)shortName;
+(NSString *)getCallingProvider:(NSString *)shortName;
// =======
//+(NSString *)getOutCallProviderByCountry:(NSString *)shortName;
+(BOOL)getContryRestrictedForSameCallId:(NSString *)shortName;

// >>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b

+(void)getCallhippoContactBySkipLimit:(NSString *)skip;
-(void)makeArrayFromDBChContact;
- (NSMutableArray*)get_chAndDeviceContactList;
-(void)editCHcontactToDBArray:(NSDictionary *)newdic title:(NSString *)title;
+(void)getCallhippoUpdatedContacts:(NSString *)skip;
-(void)flushAllContactsAndFetchNew;
+(void)getCallhippoIntegrationContacts:(NSString *)skip;

@end

NS_ASSUME_NONNULL_END
