//
//  CallKitInstance.h
//  ObjCVoiceCallingApp
//
//  Created by Siva  on 17/04/17.
//  Copyright © 2017 Plivo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CallKit/CallKit.h>

@import TwilioVoice;
/*!
 * @discussion CallKitInstance class to maintain single Callkit instance in entire app lifecycle
 */
@protocol TwilioDelegate <NSObject>


@optional
- (void)callDidStartRinging_custom:(TVOCall *)call;
- (void)callDidConnect_custom:(TVOCall *)call;
- (void)callDidReconnect_custom:(TVOCall *)call;
- (void)twilio_calldelegate:(TVOCallInvite *)callInvite uuid:(NSUUID *)uuid;
- (void)callDisconnected_custom:(TVOCall *)call;
- (void)call:(TVOCall *)call isReconnectingWithError_custom:(NSError *)error;
- (void)call:(TVOCall *)call didFailToConnectWithError_custom:(NSError *)error;
- (void)call:(TVOCall *)call didDisconnectWithError_custom:(NSError *)error;
@end


@interface twilio_callkit : NSObject <CXProviderDelegate, CXCallObserverDelegate,TVOCallDelegate,AVAudioPlayerDelegate>
{
   
}
+ (twilio_callkit *)sharedInstance;


@property (nonatomic, strong) AVAudioPlayer *ringtonePlayer;
@property (nonatomic, assign) BOOL playCustomRingback;
@property (strong, nonatomic) id<TwilioDelegate> delegate;
@property (strong, nonatomic) NSUUID* callUUID;
@property (strong, nonatomic) CXProvider *callKitProvider;
@property (strong, nonatomic) CXCallController* callKitCallController;
@property (strong, nonatomic) CXCallObserver *callObserver;
@property (nonatomic, strong) NSMutableArray *calls_uuids_twilio;
@property (nonatomic, assign) BOOL userInitiatedDisconnect;
@property (nonatomic, strong) TVODefaultAudioDevice *audioDevice;
@property (nonatomic, strong) TVOCall *activeCall;
@property (strong, nonatomic) TVOCallInvite *twilio_callinvite;
@property (nonatomic, strong) NSMutableDictionary *activeCalls;
@property (nonatomic, strong) NSMutableDictionary *activeCallInvites;
@property (nonatomic, strong) void(^callKitCompletionCallback)(BOOL);
@property(strong, nonatomic) twilio_callkit *providerDelegate;
- (void)reportIncomingCallFrom:(NSString *) from withUUID:(NSUUID *)uuid extHeader:(NSDictionary *)extHeader callinvite:(TVOCallInvite *)callinvite call:(TVOCall *)call;
- (void)performEndCallActionWithUUID:(NSUUID *)uuid;
- (void)configAudioSession:(AVAudioSession *)audioSession;
- (void)performStartCallActionWithUUID:(NSUUID *)uuid handle:(NSString *)handle;



@end
