
//  twilio_callkit.m
//  ObjCVoiceCallingApp
//
//  Created by Siva  on 17/04/17.
//  Copyright © 2017 Plivo. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "twilio_callkit.h"
#import "OnCallVC.h"
#import "LoginVC.h"
#import "DialerVC.h"
#import "Constant.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "WebApiController.h"
#import "UtilsClass.h"
#import "GlobalData.h"
#import "AppDelegate.h"
#import "PopUpViewController.h"
#import "WebApiController.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

@import TwilioVoice;
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
//#import "UIView+Toast.h"

//@synthesize currHours,currMinute,currSeconds,timer;
@implementation twilio_callkit
{
    
    NSDictionary *headers;
    WebApiController *obj;
}

+ (twilio_callkit *)sharedInstance
{
    //Singleton instance
    static twilio_callkit *sharedInstance = nil;
    if(sharedInstance == nil)
    {
        sharedInstance = [[twilio_callkit alloc] init];
        
    }
    return sharedInstance;
}

- (id)init
{
//    NSLog(@"Twilio_callkit :: init");
    if(self = [super init])
    {
       //pp- [self configAudioSession:[AVAudioSession sharedInstance]];
        self.activeCalls = [NSMutableDictionary dictionary];
        CXProviderConfiguration* configuration = [[CXProviderConfiguration alloc] initWithLocalizedName:@"CallHippo"];
        configuration.maximumCallGroups = 1;
        configuration.maximumCallsPerCallGroup = 1;
        configuration.supportedHandleTypes = [[NSSet alloc] initWithObjects:[NSNumber numberWithInt:(int)CXHandleTypePhoneNumber], nil];
        UIImage *icon = [UIImage imageNamed:@"callhippo"];
        configuration.iconTemplateImageData = UIImagePNGRepresentation(icon);
        
//        NSLog(@"Icoming Call Cj %@",configuration);
        self.callKitProvider = [[CXProvider alloc] initWithConfiguration:configuration];
        [self.callKitProvider setDelegate:self queue:dispatch_get_main_queue()];
        self.callKitCallController = [[CXCallController alloc] init];
        [self.callKitCallController.callObserver setDelegate:self queue:dispatch_get_main_queue()];
        
//        self.callKitProvider = [[CXProvider alloc] initWithConfiguration:configuration];
//        self.callKitCallController = [[CXCallController alloc] init];
        
        self.calls_uuids_twilio = [[NSMutableArray alloc] init];
        self.callObserver = [[CXCallObserver alloc] init];
        self.activeCalls = [NSMutableDictionary dictionary];
        self.activeCallInvites = [NSMutableDictionary dictionary];
        
//        [self.callKitProvider setDelegate:self queue:dispatch_get_main_queue()];
//        CXCallController *callController = [[CXCallController alloc] initWithQueue:dispatch_get_main_queue()];
//        [callController.callObserver setDelegate:self queue:dispatch_get_main_queue()];
//        self.callKitCallController = callController;

        self.audioDevice = [TVODefaultAudioDevice audioDevice];
        TwilioVoiceSDK.audioDevice = self.audioDevice;
        
        self.playCustomRingback = YES;
        
        
        

        return self;
    }
    return nil;
}
- (instancetype)initWithType:(CXHandleType)type
                       value:(NSString *)value
{
   NSLog(@"Twilio_callkit :: initWithType");
    return self;
}

- (void)dealloc {
    if (self.callKitProvider) {
        [self.callKitProvider invalidate];
    }
}
- (void)configAudioSession:(AVAudioSession *)audioSession {
    if (@available(iOS 10, *)) {
        // iOS 11 (or newer) ObjC code
        
        NSError *err = nil;
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                             mode:AVAudioSessionModeVoiceChat
                          options:AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP
                            error:&err];
        if (err) {
            err = nil;
        }
        [audioSession setMode:AVAudioSessionModeVoiceChat error:&err];
        if (err) {
            err = nil;
        }
        double sampleRate = 48000.0;
        [audioSession setPreferredSampleRate:sampleRate error:&err];
        if (err) {
            err = nil;
        }
    }
    else
    {
        
    }
    
}


- (void)reportIncomingCallFrom:(NSString *) from withUUID:(NSUUID *)uuid extHeader:(NSDictionary *)extHeader callinvite:(TVOCallInvite *)callinvite call:(TVOCall *)call
{
   
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: reportIncomingCallFrom");
    self.playCustomRingback = NO;
    NSString *Userid = [Default valueForKey:USER_ID];
    if (Userid != nil)
    {
    headers = [[NSMutableDictionary alloc] init];
    headers = extHeader;
        
    NSString *incName;
    incName = extHeader[@"xphfrom"];
    incName = [incName
               stringByReplacingOccurrencesOfString:@": " withString:@""];
    incName = [incName
               stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        
        
        NSString *Phonenumber_get = incName;
        NSLog(@"Twilio_callkit reportIncomingCallFrom : %@",Phonenumber_get);
        
        //    NSLog(@"Mixallcontact : %@",Mixallcontact);
        NSString *str = [Phonenumber_get stringByReplacingOccurrencesOfString:@"+" withString:@""] ;
        
        NSArray *Mixallcontact = [[NSArray alloc] init];
     
        //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
        Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
        
        //p NSPredicate *filter = [NSPredicate predicateWithFormat:@"(number_int contains[c] %@)",str];
        
        //-mixallcontact
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",str] ;
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",str] ;
        
        NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];
        
        NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
        
        NSString *contact_save = @"";
        
        if(filteredContacts.count != 0)
        {
            NSDictionary *dic = [filteredContacts objectAtIndex:0];
            //  NSLog(@"Call Contact Find search dic == == > : %@",dic);
            NSLog(@"Trushang Dic : %@",dic);
            NSString *str = [dic valueForKey:@"name"];
            contact_save = str;
            if(!dic[@"_id"])
            {
                NSLog(@"Call Contact Find search dic == == > : %@",dic);
                NSString *ContactName = [dic valueForKey:@"name"];
                //NSString *ContactNumber = [dic valueForKey:@"number"];
                NSString *ContactNumber;
                if ([[dic valueForKey:@"numberArray"] count]>0) {
                    ContactNumber = [[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                }
                else
                {
                    ContactNumber =@"";
                }
                [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
            }
            
        }
        
        NSString *name = @"";
        NSString *number = Phonenumber_get;
        
        if(extHeader != nil)
        {
            NSDictionary *dic =  extHeader;
            NSLog(@"Twilio_callkit TRTRTRTRTR : %@",dic);
            name = [dic valueForKey:@"xphfrom"];
            number = [dic valueForKey:@"xphfromnumber"];
            

            name = [name
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            name = [name
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
           
            name = [name stringByRemovingPercentEncoding];
            NSString *name1 = [name stringByReplacingOccurrencesOfString:@"+" withString:@""];
            NSLog(@"Twilio_callkit Values : %d",[self validateString:name1 withPattern:@"^[0-9]+$"]);
            if([self validateString:name1 withPattern:@"^[0-9]+$"])
            {
                
                if(![contact_save isEqualToString:@""])
                {
                    name = contact_save;
                }
            }
            else
            {
                //            if(![contact_save isEqualToString:@""])
                //            {
                //                name = contact_save;
                //            }
            }
            
            //User exists
        }
        else
        {
            name = Phonenumber_get;
            
            //User doesn't exist
        }
        
        
        if([self validateString:name withPattern:@"^[0-9]+$"])
        {
            NSString *str = @"+";
            name = [NSString stringWithFormat:@"%@%@",str,name];
            
            if([Default boolForKey:kIsNumberMask] == true)
            {
                name = [UtilsClass get_masked_number:name];
                
                NSLog(@"masked number :: %@",name);
            }
        }
        
        NSLog(@"Final Name  : Trushang --> %@",name);
        
    CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypePhoneNumber value:name];
    
    CXCallUpdate* callUpdate = [[CXCallUpdate alloc] init];
//    callUpdate.remoteHandle = callHandle;
        
        callUpdate.remoteHandle = [[CXHandle alloc] initWithType:CXHandleTypePhoneNumber value:number];
        callUpdate.localizedCallerName = name;
        
    callUpdate.supportsDTMF = TRUE;
    callUpdate.supportsHolding = TRUE;
    callUpdate.supportsGrouping = TRUE;
    callUpdate.supportsUngrouping = TRUE;
    callUpdate.hasVideo = NO;
    
    [self.callKitProvider reportNewIncomingCallWithUUID:uuid update:callUpdate completion:^(NSError * _Nullable error) {
        
        if(error)
        {
//            [[Phone sharedInstance] stopAudioDevice];
        }
        else
        {
            
       //pp-     [self configAudioSession:[AVAudioSession sharedInstance]];
        }
        
    }];
    
    }
    }
}

- (void)provider:(CXProvider *)provider performAnswerCallAction:(CXAnswerCallAction *)action
{
    
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performAnswerCallAction");
        
    //pp- self.audioDevice.enabled = NO;
  //pp- [self configAudioSession:[AVAudioSession sharedInstance]];
        
  [self performAnswerVoiceCallWithUUID:action.callUUID completion:^(BOOL success) {
      if (success) {
          
          [action fulfill];
      } else {
          [action fail];
      }
  }];
        [twilio_callkit sharedInstance].callUUID = [action callUUID];
        
        UIWindow *mainWindow = [UIApplication sharedApplication].windows[0];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
      OnCallVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
  //      NewOnCallVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

        
        
        NSString *incName;
        incName = @"";
        if([headers valueForKey:@"xphfrom"] != nil)
        {
            incName = [headers valueForKey:@"xphfrom"];
            
            incName = [incName
                    stringByReplacingOccurrencesOfString:@": " withString:@""];
            incName = [incName
                    stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        }
        
        
        
        NSString *Phonenumber_get = incName;
        NSLog(@"performAnswerCallAction Patel : %@",Phonenumber_get);
        
        
        NSString *name = @"";
        NSString *transferBy = @"";
        NSString *number = Phonenumber_get;
        NSString *xphto = @"";
       
        
        if(headers != nil)
        {
            NSLog(@"performAnswerCallAction Name Exist ");
            NSDictionary *dic =  headers;
            name = [dic valueForKey:@"xphfrom"];
            number = [dic valueForKey:@"xphfromnumber"];
            transferBy = [dic valueForKey:@"xphfirstcallusername"];
            name = [name
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            name = [name
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            
            number = [number
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            number = [number
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            xphto = [dic valueForKey:@"xphto"];
            
            
            NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
            txtText.text = @"";
//            [txtText insertText:number];
            [txtText insertText:[dic valueForKey:@"xphtotransferNumber"]];
            NSString *Country_ShotName = txtText.country.countryCode ? txtText.country.countryCode : @"";
            [Default setValue:[Country_ShotName lowercaseString] forKey:Selected_Department_Flag];
            [Default setValue:xphto forKey:Selected_Department];
            //User exists
        }
        else
        {
            NSLog(@"performAnswerCallAction Name not Exist ");
            name = Phonenumber_get;
            //User doesn't exist
        }
        
        
        if([self validateString:name withPattern:@"^[0-9]+$"])
        {
            NSString *str = @"+";
            name = [NSString stringWithFormat:@"%@%@",str,name];
        }
        
        if([self validateString:number withPattern:@"^[0-9]+$"])
        {
            NSString *str = @"+";
            number = [NSString stringWithFormat:@"%@%@",str,number];
        }
        
        vc2.CallStatus = INCOMING;
        vc2.CallStatusfinal = INCOMING;
        vc2.ContactName = name ? name : @"";
        vc2.ContactNumber = number ? number : @"";
        vc2.transferCall = transferBy ? transferBy : @"";
        vc2.IncCallOutCall = @"Incoming";
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        {
            NSLog(@"TwilioNotification :: BackGround");
            [vc2 twilio_notification];
        }
//        vc2.Plivo_Incomingcall = incCall;
        [Default setValue:Timer_Start forKey:Timer_Call];
        [Default synchronize];
    
        UINavigationController *navController123 = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        MainViewController *mainviews = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        
        if((UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController] != nil)
        {
            navController123 = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
            NSLog(@"TwilioCallKIt::view not nill");
            
            NSArray *views = navController123.viewControllers;
            mainviews = [views objectAtIndex:0];
        }
        else
        {
            NSLog(@"TwilioCallKIt::view nill");
        }
        
        UINavigationController *navigationControllerfinal = (UINavigationController *)mainviews.rootViewController;
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [arr addObjectsFromArray:navigationControllerfinal.viewControllers];
        
        int osnum = [[[UIDevice currentDevice] systemVersion] intValue];
          if (osnum >= 15 && [arr count] == 0) {
              [arr addObject:vc1];
          }
        [arr addObject:vc2];
        NSLog(@"Nav :: %@",navigationControllerfinal.viewControllers);
        NSLog(@"Views :: %@",arr);
        
        [navigationController setViewControllers:arr animated:true];
        MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        mainViewController.rootViewController = navigationController;
        [mainViewController setupWithType:11];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
        navController.navigationBar.hidden = true;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        navController.navigationBar.hidden = true;
        [appDelegate window].rootViewController = navController;
        [[appDelegate window] setNeedsLayout];
        [[appDelegate window] makeKeyAndVisible];
        NSLog(@"Call 520");
  
    [action fulfill];
    }
}



- (void)provider:(CXProvider *)provider performEndCallAction:(CXEndCallAction *)action
{
    
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performEndCallAction");
    if (self.playCustomRingback) {
        [self stopRingback];
    }
    
        //notification to load call logs
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"callDisconnected_loadCallLogs" object:self userInfo:nil];
        
        
    TVOCallInvite *callInvite = self.activeCallInvites[action.callUUID.UUIDString];
    TVOCall *call = self.activeCalls[action.callUUID.UUIDString];
    
   if (callInvite) {
       NSLog(@"call invite if :: ");
       [callInvite reject];
       [self.activeCallInvites removeObjectForKey:callInvite.uuid.UUIDString];
   } else if (call) {
       NSLog(@"call  if");
       [call disconnect];
   } else {
       NSLog(@"Unknown UUID to perform end-call action with");
   }
    //pp- self.audioDevice.enabled = YES;
        [action fulfill];
    }
   

}

- (void)provider:(CXProvider *)provider performSetMutedCallAction:(CXSetMutedCallAction *)action
{
   
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performSetMutedCallAction");
    TVOCallInvite *callInvite = [[NSString stringWithFormat:@"%@",action.callUUID.UUIDString] mutableCopy];;
    TVOCall *call = [[NSString stringWithFormat:@"%@",action.callUUID.UUIDString] mutableCopy];
    
    if(action.muted)
    {

        if (callInvite) {
           
        } else if (call) {
            [call setMuted:false];
        } else {
            NSLog(@"Unknown UUID to perform end-call action with");
        }
        
    }
    else
    {
        
         if (callInvite) {
                  
               } else if (call) {
                   [call setMuted:true];
               } else {
                   NSLog(@"Unknown UUID to perform end-call action with");
               }
        
    }
    [action fulfill];
    }
     
}

- (void)provider:(CXProvider *)provider performSetHeldCallAction:(CXSetHeldCallAction *)action
{

    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performSetHeldCallAction");
    TVOCall *call = self.activeCalls[action.callUUID.UUIDString];
    if (call && call.state == TVOCallStateConnected)
    {
        if (action.isOnHold)
        {
            NSLog(@"\n \n  CALL HOLD \n \n ");
            if(self.calls_uuids_twilio.count > 1)
            {
                  NSLog(@"\n \n  babyooooo \n \n ");
                [self.calls_uuids_twilio removeObject:call.uuid];
                [self.calls_uuids_twilio insertObject:call.uuid atIndex:0];
//                [self.calls_uuids_twilio replaceObjectAtIndex:0 withObject:call.uuid];
            }
            NSLog(@"\n \n  %@ \n \n ",action.isOnHold ? @"YES" : @"NO");
            [call setOnHold:action.isOnHold];
            [action fulfill];
        }
        else
        {
            if(self.calls_uuids_twilio.count > 1)
            {
                NSLog(@"\n \n  babyooooo 12313 \n \n ");
                [self.calls_uuids_twilio removeObject:call.uuid];
                [self.calls_uuids_twilio insertObject:call.uuid atIndex:1];
            }
            NSLog(@"\n \n  CALL UN HOLD \n \n  ");
            NSLog(@"\n \n  %@ \n \n ",action.isOnHold ? @"YES" : @"NO");
         //pp-   [self configAudioSession:[AVAudioSession sharedInstance]];
            [call setOnHold:action.isOnHold];
            [action fulfill];
        }
       NSLog(@"calls_uuids_twilio : %@",self.calls_uuids_twilio);
    }
    else
    {
        NSLog(@"\n \n  Not Connected \n \n  ");
       [action fulfill];
    }
    }
}

- (void)provider:(CXProvider *)provider performPlayDTMFCallAction:(CXPlayDTMFCallAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
        NSLog(@"Twilio_callkit :: performPlayDTMFCallAction");
        [action fulfill];
    }
    
}

- (void)performStartCallActionWithUUID:(NSUUID *)uuid handle:(NSString *)handle
{
    
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performStartCallActionWithUUID");
    self.playCustomRingback = YES;
   if (uuid == nil || handle == nil) {
        return;
    }

    CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:handle];
    CXStartCallAction *startCallAction = [[CXStartCallAction alloc] initWithCallUUID:uuid handle:callHandle];
    CXTransaction *transaction = [[CXTransaction alloc] initWithAction:startCallAction];

    [self.callKitCallController requestTransaction:transaction completion:^(NSError *error) {
        if (error) {
            NSLog(@"Twilio : StartCallAction transaction request : failed: %@", [error localizedDescription]);
        } else {
            NSLog(@"Twilio : StartCallAction transaction request : successful");

            CXCallUpdate *callUpdate = [[CXCallUpdate alloc] init];
            callUpdate.remoteHandle = callHandle;
            callUpdate.supportsDTMF = YES;
            callUpdate.supportsHolding = YES;
            callUpdate.supportsGrouping = NO;
            callUpdate.supportsUngrouping = NO;
            callUpdate.hasVideo = NO;

            [self.callKitProvider reportCallWithUUID:uuid updated:callUpdate];
            
            
        }
    }];
    }
}

- (void)provider:(CXProvider *)provider performStartCallAction:(CXStartCallAction *)action
{
    
    
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    
    NSLog(@"Twilio_callkit :: performStartCallAction");
    NSLog(@"Twilio_callkit :: **** Twilio calling start ***");
    NSLog(@"Twilio_callkit :: Calling_Provider :: %@ ",[Default valueForKey:CallingProvider]);
   //pp- [self configAudioSession:[AVAudioSession sharedInstance]];
     // self.audioDevice.enabled = NO;
     // self.audioDevice.block();
    
   
        [self.callKitProvider reportOutgoingCallWithUUID:action.callUUID startedConnectingAtDate:[NSDate date]];
//        __weak typeof(self) weakSelf = self;
        [self performVoiceCallWithUUID:action.callUUID client:action.handle.value completion:^(BOOL success) {
//            __strong typeof(self) strongSelf = weakSelf;
            if (success) {
                [self.callKitProvider reportOutgoingCallWithUUID:action.callUUID connectedAtDate:[NSDate date]];
               // [action fulfill];
            } else {
                [self performEndCallActionWithUUID:action.UUID];
              //  [action fail];
            }
            [action fulfill];
        }];
    }
}
- (void)performEndCallActionWithUUID:(NSUUID *)uuid
{

    NSString *callingprovider = [Default valueForKey:CallingProvider];
    [Default setValue:Timer_Stop forKey:Timer_Call];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performEndCallActionWithUUID");
    NSLog(@"Twilio_callkit :: performEndCallActionWithUUID :: UUID %@",uuid);
    if (self.playCustomRingback) {
        [self stopRingback];
    }
        
        NSLog(@"perform end call uuid:: %@",uuid);
        NSLog(@"perform end call active call uuid:: %@",_activeCall.uuid);
        
     CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:uuid];
        
        CXTransaction *transaction = [[CXTransaction alloc] initWithAction:endCallAction];

        [self.callKitCallController requestTransaction:transaction completion:^(NSError *error) {
            if (error) {
                NSLog(@"Twilio :  EndCallAction transaction request failed: %@", [error localizedDescription]);
                [self.callKitProvider reportCallWithUUID:uuid endedAtDate:nil reason:CXCallEndedReasonRemoteEnded];

            }
            else {
                NSLog(@"Twilio :  EndCallAction transaction request successful");
            }
            
            
        }];
        
       
    
    }
}


- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call{
    
    
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
   // NSLog(@"Twilio_callkit :: performEndCallActionWithUUID");
    if(![self.calls_uuids_twilio containsObject:call.UUID])
    {
        [self.calls_uuids_twilio addObject:call.UUID];
    }
    if (call == nil || call.hasEnded == YES) {
        NSLog(@"Twilio_callkit : Disconnected");
        
    }
    
    if (call.isOutgoing == YES && call.hasConnected == NO) {
        NSLog(@"Twilio_callkit : Dialing");
    }
    
    if (call.isOutgoing == NO  && call.hasConnected == NO && call.hasEnded == NO && call != nil) {
        NSLog(@"Twilio_callkit : Incoming");
    }
    
    if (call.hasConnected == YES && call.hasEnded == NO) {
        NSLog(@"Twilio_callkit : Connected");
    }
    if(call.hasEnded)
    {
        NSLog(@"Twilio_callkit : End %@",self.calls_uuids_twilio);
        NSLog(@"Twilio_callkit : active %@",self.activeCall.uuid);
        if(self.calls_uuids_twilio.count > 1)
        {
        
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) {
                NSUUID *uuid = [self.calls_uuids_twilio objectAtIndex:0];
                NSLog(@"\n \n \n \n \n \n \n \n  Trushang_code : www  Callkit www : UUID : %@\n \n \n \n \n ",uuid);
                if (!uuid) {
                    return;
                }
                CXSetHeldCallAction *act = [[CXSetHeldCallAction alloc] initWithCallUUID:uuid onHold:NO];
                CXTransaction *tr = [[CXTransaction alloc] initWithAction:act];
                [self.callKitCallController requestTransaction:tr
                                                                                completion:^(NSError *err)
                 {
                     //                                                                            2398985-af17-4515-b754-72173bf972c4
                     if (err) {
                         NSLog(@" Papi :  Error requesting transaction: %@", err.localizedDescription);
                     } else {
                         NSLog(@" Papi :Requested transaction successfully");
                         //                                                                               [self configAudioSession:[AVAudioSession sharedInstance]];
                     }
                 }];
            }
        }
        
    }
    }
}

#pragma mark - CXProviderDelegate

- (void)providerDidReset:(CXProvider *)provider
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: providerDidReset");
    self.audioDevice.enabled = NO;
    }
}

- (void)providerDidBegin:(CXProvider *)provider
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: providerDidBegin");

    }
}

- (void)provider:(CXProvider *)provider didActivateAudioSession:(AVAudioSession *)audioSession
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
        NSLog(@"Twilio_callkit :: didActivateAudioSession");
        self.audioDevice.enabled = YES;
       //pp- [self configAudioSession:[AVAudioSession sharedInstance]];
    }
}

- (void)provider:(CXProvider *)provider didDeactivateAudioSession:(AVAudioSession *)audioSession
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
        NSLog(@"Twilio_callkit :: didDeactivateAudioSession");
        self.audioDevice.enabled = NO;

    }
}

- (void)provider:(CXProvider *)provider timedOutPerformingAction:(CXAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
        NSLog(@"Twilio_callkit :: timedOutPerformingAction");
    }
}





- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        return [predicate evaluateWithObject:string];
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    
}

-(void)ratting_popup
{
    NSLog(@"Twilio_callkit :: ratting_popup");
    NSLog(@"Incoming iutgoiung : %@",[Default valueForKey:INCOUTCALL]);
    if ([[Default valueForKey:INCOUTCALL] isEqualToString:@"false"])
    {
//        UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
//        alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PopUpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PopUpViewController"];
//        if(incCall != nil)
//        {
            vc.strIncOutCall = INCOMING;
//        }
//        else
//        {
            vc.strIncOutCall = OUTGOING;
//        }
        
        if (IS_IPAD) {
            
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
            //                        [self presentViewController:vc animated:YES completion:nil];
        } else {
            vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
            //                        [self presentViewController:vc animated:YES completion:nil];
        }
        
        
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
    
}

-(void)holdUnholdApiCall:(NSString *)holdplivo {
    
    NSLog(@"Twilio_callkit :: holdUnholdApiCall");
    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *authToken = [Default valueForKey:AUTH_TOKEN];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *plivoAuthToken =  [Default valueForKey:CALLHIPPO_AUTH_TOKEN];
    NSString *plivoAutId = [Default valueForKey:CALLHIPPO_AUTH_ID];
    NSString *url = [NSString stringWithFormat:@"%@",holdplivo];
    NSDictionary *passDict = @{@"authId":plivoAutId,
                               @"authToken":plivoAuthToken,
                               @"callHoldUrl":@"https://s3.amazonaws.com/callhippo_staging/call_hold/15168789827365a4c70c0bc9e741f843ad5c6.mp3",
                               @"calluid":@"1899ca94-518f-413b-9277-96b553f69c5a",
                               @"deviceType":@"iOS",
                               @"userId":userId};
    
//before    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
        obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response {
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"TRUSHANG : STATUSCODE **************1  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    NSLog(@"Hold : Response : %@",response1);
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        // [UtilsClass showAlert:@"Done" contro:self];
    }else {
        NSLog(@"fail hold response");
        @try {
            UIViewController *view = [[UIViewController alloc] init];
            [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:view];
        }
        @catch (NSException *exception) {
        }
    }
    }
}

#pragma mark - Twilio outgoing call
- (void)performVoiceCallWithUUID:(NSUUID *)uuid
                          client:(NSString *)client
                      completion:(void(^)(BOOL success))completionHandler {
    
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Twilio])
    {
        NSLog(@"Twilio_callkit :: performVoiceCallWithUUID");
        NSLog(@"Twilio : Its call Bro");
        NSString *twilio_token_string = [Default valueForKey:twilio_token] ? [Default valueForKey:twilio_token] : @"";
        NSLog(@"Twilio : token  %@",twilio_token_string);
        

        
        if(![twilio_token_string isEqualToString:@""])
        {
            TVOConnectOptions *connectOptions = [TVOConnectOptions optionsWithAccessToken:twilio_token_string block:^(TVOConnectOptionsBuilder *builder)
                                                 {
                NSString *userid = [Default valueForKey:USER_ID];
                NSString *strReminderId = [Default valueForKey:XPH_EXTRA_HEADER_REMINDER];
                strReminderId = @"";
                NSString *selno_without = [Default valueForKey:SELECTEDNO];
                NSString *selno = [selno_without stringByReplacingOccurrencesOfString:@"+" withString:@""];
                if(selno == nil){
                    selno = @"";
                }
                NSString* device = [NSString stringWithFormat: @"iOS"];
                NSString* extentionCall = [Default valueForKey:ExtentionCall];
                NSString* extentionCallProvider = [Default valueForKey:ExtentionCall];
                
                
                if ([Default valueForKey:kbackendProvider]) {
                    NSString *backendProvider = [Default valueForKey:kbackendProvider];
                    
                    if ([backendProvider isEqualToString:@""]) {
                        builder.params = @{
                            @"To": client,
                            @"X-PH-Userid": userid,
                            @"X-PH-Fromnumber": selno,
                            @"X-PH-Devicetype": device,
                            @"X-PH-reminderId": strReminderId,
                            @"X-PH-EXTENSIONCALL": extentionCall,
                            @"provider" : extentionCallProvider
                        };
                    }
                    else
                    {
                        builder.params = @{
                            @"To": client,
                            @"X-PH-Userid": userid,
                            @"X-PH-Fromnumber": selno,
                            @"X-PH-Devicetype": device,
                            @"X-PH-reminderId": strReminderId,
                            @"X-PH-EXTENSIONCALL": extentionCall,
                            @"provider" : extentionCallProvider,
                            @"X-PH-Backendprovider":backendProvider
                        };
                    }
                   
                }
                else
                {
                    builder.params = @{
                        @"To": client,
                        @"X-PH-Userid": userid,
                        @"X-PH-Fromnumber": selno,
                        @"X-PH-Devicetype": device,
                        @"X-PH-reminderId": strReminderId,
                        @"X-PH-EXTENSIONCALL": extentionCall,
                        @"provider" : extentionCallProvider
                    };
                }
                
                builder.uuid = uuid;
                NSLog(@"Twilio : X-ph set");
                NSLog(@"Chetan Xph : %@",builder.params);
            }];
            
            TVOCall *call = [TwilioVoiceSDK connectWithOptions:connectOptions delegate:self];
            
            
            if (!call) {
                NSLog(@"twilio : call running");
                
            }
            else {
                NSLog(@"Twilio : Call Connected by trushang");
                self.activeCalls = [[NSMutableDictionary alloc] init];
                self.activeCall = call;
                self.activeCalls[call.uuid.UUIDString] = call;
                
            
            }
            self.callKitCompletionCallback = completionHandler;
        }
        else
        {
            NSLog(@"Twilio : token  nill");
        }
    }
}

#pragma mark - Twilio incoming call
- (void)performAnswerVoiceCallWithUUID:(NSUUID *)uuid
                            completion:(void(^)(BOOL success))completionHandler {
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Twilio])
    {
    NSLog(@"Twilio_callkit :: performAnswerVoiceCallWithUUID");
    if (_twilio_callinvite != nil)
    {
        TVOCallInvite *callInvite = self.twilio_callinvite;
        
        TVOAcceptOptions *acceptOptions = [TVOAcceptOptions optionsWithCallInvite:callInvite block:^(TVOAcceptOptionsBuilder *builder) {
            builder.uuid = uuid;
        }];
        
        TVOCall *call = [callInvite acceptWithOptions:acceptOptions delegate:self];
//        [[OnCallVC sharedInstance] twilio_calldelegate:callInvite];
        
        if (!call) {
            NSLog(@"twilio : call running");
            completionHandler(NO);
        } else {
                    self.callKitCompletionCallback = completionHandler;
                    self.activeCall = call;
                    self.activeCalls[call.uuid.UUIDString] = call;
        }
//
         [self.activeCallInvites removeObjectForKey:callInvite.uuid.UUIDString];
        
       
        
//        if ([[NSProcessInfo processInfo] operatingSystemVersion].majorVersion < 13) {
//             [self incomingPushHandled];
//        }
    }
    }
    
}
#pragma mark - TVOCallDelegate
- (void)callDidStartRinging:(TVOCall *)call
{
    NSLog(@"callDidStartRinging : twilio_callkit : %@",call.sid);
    if (self.playCustomRingback) {
        [self playRingback];
    }
    NSDictionary* userInfo = @{@"call": self.activeCall};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"callDidStartRinging_custom" object:self userInfo:userInfo];
  //  [_delegate callDidStartRinging_custom:self.activeCall];
}


- (void)callDidConnect:(TVOCall *)call
{
    NSLog(@"callDidConnect : twilio_callkit");
    if (self.playCustomRingback) {
        [self stopRingback];
    }
    self.callKitCompletionCallback(YES);
//    [self configAudioSession:[AVAudioSession sharedInstance]];
    NSDictionary* userInfo = @{@"call": self.activeCall};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"callDidConnect_custom" object:self userInfo:userInfo];
 //   [_delegate callDidConnect_custom:self.activeCall];
    
    
    
    

}
- (void)callDidReconnect:(TVOCall *)call
{
    NSLog(@"Call reconnected : twilio_callkit");
    NSDictionary* userInfo = @{@"call": self.activeCall};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"callDidReconnect_custom" object:self userInfo:userInfo];
 //   [_delegate callDidReconnect_custom:self.activeCall];
}


- (void)callDisconnected:(TVOCall *)call
{
    NSLog(@"Call callDisconnected : twilio_callkit");
    if (self.playCustomRingback) {
        [self stopRingback];
    }
    NSDictionary* userInfo = @{@"call": self.activeCall};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"callDisconnected_custom" object:self userInfo:userInfo];
  //  [_delegate callDisconnected_custom:self.activeCall];
    
   
   
    
    [self.activeCalls removeObjectForKey:call.uuid.UUIDString];
}


- (void)call:(TVOCall *)call isReconnectingWithError:(NSError *)error
{
    NSLog(@"Call is reconnecting  : twilio_callkit");
    NSDictionary* userInfo = @{@"call": self.activeCall};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"isReconnectingWithError_custom" object:self userInfo:userInfo];
 //   [_delegate call:self.activeCall isReconnectingWithError_custom:error];
}
- (void)call:(TVOCall *)call didFailToConnectWithError:(NSError *)error
{
    NSLog(@"Call failed to connect : twilio_callkit : %@", error);
    
    NSDictionary* userInfo = @{@"call": self.activeCall,@"error":error};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"didFailToConnectWithError_custom" object:self userInfo:userInfo];
   // [_delegate call:self.activeCall didFailToConnectWithError_custom:error];
    [self performEndCallActionWithUUID:call.uuid];
    self.callKitCompletionCallback(NO);
}
- (void)call:(TVOCall *)call didDisconnectWithError:(NSError *)error
{
    if (error) {
        NSLog(@"twilio_callkit : didDisconnectWithError : %@",error);
    } else {
         NSLog(@"twilio_callkit : didDisconnectWithError" );
    }
//    CXCallEndedReason reason = CXCallEndedReasonRemoteEnded;
//    [self.callKitProvider reportCallWithUUID:call.uuid endedAtDate:[NSDate date] reason:reason];
    [self performEndCallActionWithUUID:call.uuid];
  //pp-  self.callKitCompletionCallback(YES);
    NSDictionary* userInfo = @{@"call": self.activeCall};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"didDisconnectWithError_custom" object:self userInfo:userInfo];
//    [_delegate call:self.activeCall didDisconnectWithError_custom:error];
}

- (void)setDelegate:(id<TwilioDelegate>)delegate {
  _delegate = delegate;
}

#pragma mark - Ringtone

- (void)playRingback {
    NSString *ringtonePath = [[NSBundle mainBundle] pathForResource:@"us-ring" ofType:@"wav"];
    if ([ringtonePath length] <= 0) {
        NSLog(@"Can't find sound file");
        return;
    }
    
    NSError *error;
    self.ringtonePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:ringtonePath] error:&error];
    if (error != nil) {
        NSLog(@"Failed to initialize audio player: %@", error);
    } else {
        self.ringtonePlayer.delegate = self;
        self.ringtonePlayer.numberOfLoops = -1;
        
        self.ringtonePlayer.volume = 3.0f;
        [self.ringtonePlayer play];
    }
}

- (void)stopRingback {
    if (!self.ringtonePlayer.isPlaying) {
        return;
    }
    
    [self.ringtonePlayer stop];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    if (flag) {
        NSLog(@"Audio player finished playing successfully");
    } else {
        NSLog(@"Audio player finished playing with some error");
    }
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    NSLog(@"Decode error occurred: %@", error);
}

@end
