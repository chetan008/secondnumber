//
//  Phone.m
//  PlivoOutgoingApp
//
//  Created by Iwan BK on 10/2/13.
//  Copyright (c) 2013 Plivo. All rights reserved.
//

#import "Phone.h"

#import "Constants.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
//#import "PlivoEndpoint.h"
#import "AppDelegate.h"

@interface Phone () {
    
}
@end

@implementation Phone {
    
    PlivoOutgoing *outCall;
    PlivoEndpoint *endpoint;

}

+ (Phone *)sharedInstance
{
    static Phone *sharedInstance = nil;
    if(sharedInstance == nil)
    {
        sharedInstance = [[Phone alloc] init];
    }
    return sharedInstance;
    
}

- (id) init
{
    self = [super init];
    
    if (self) {
       // endpoint = [[PlivoEndpoint alloc] iniinitWithDebug:true];
        endpoint = [[PlivoEndpoint alloc]init:[NSDictionary dictionaryWithObjectsAndKeys:@true,@"debug",@true,@"enableTracking", nil]];
    }
    
    return self;
}

- (void)login:(NSString *)endPointUserName endPointPassword:(NSString*)password {
    @try {

        //pri

        NSLog(@"endpoint username : %@, password : %@",endPointUserName,password);

        if(![endPointUserName isEqualToString:@""] && ![password isEqualToString:@""])
        {
            [endpoint login:endPointUserName AndPassword:password];
        }
//        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        [appDelegate RegisterVoipService];
    }
    @catch (NSException *exception) {
        //NSLog(@"Errorr CAtch chetabn phone");
    }
    
}
- (void)login:(NSString *)endPointUserName endPointPassword:(NSString*)password deviceToken:(NSData*)token {
    
    //pri
    if(![endPointUserName isEqualToString:@""] && ![password isEqualToString:@""])
    {
        [endpoint login:endPointUserName AndPassword:password DeviceToken:token];

    }
}

- (PlivoOutgoing *)callWithDest:(NSString *)dest andHeaders:(NSDictionary *)headers
{
    /* construct SIP URI */
    NSString *sipUri = [[NSString alloc]initWithFormat:@"sip:%@@phone.plivo.com", dest];
    
    /* create PlivoOutgoing object */
    outCall = [endpoint createOutgoingCall];
    

    NSLog(@"headers---->%@",headers);
    NSLog(@"sipUri---->%@",sipUri);
    /* do the call */
    [outCall call:sipUri headers:headers];
    return outCall;
}

- (void)setDelegate:(id)delegate
{
    [endpoint setDelegate:delegate];
}

- (void) disableAudio{
	[outCall hold];
}

- (void) enableAudio{
	[outCall unhold];
}

- (void) logout {
    
    NSLog(@"endpoint logout");
    [endpoint logout];
}

- (void)keepAlive
{
    [endpoint keepAlive];
}

//-(void)registerToken:(NSData*)notificationToken
//{
//    [_endpoint registerToken:notificationToken];
//}

//Register pushkit token
- (void)registerToken:(NSData*)token
{
    
    //NSLog(@"Token Registered");
    [endpoint registerToken:token];
}

//receive and pass on (information or a message)
- (void)relayVoipPushNotification:(NSDictionary*)pushdata
{
    //NSLog(@"pushdata : %@",pushdata);
    [endpoint relayVoipPushNotification:pushdata];
    //NSLog(@"pushdata done : %@",pushdata);
}

//To Configure Audio
- (void)configureAudioSession{
    //NSLog(@"Trushang  : configureAudioSession Working");
    [endpoint configureAudioDevice];
}

/*
 * To Start Audio service
 * To handle Audio Interruptions
 * AVAudioSessionInterruptionTypeEnded
 */
- (void)startAudioDevice{
    [endpoint startAudioDevice];
}

/*
 * To Start Audio service
 * To handle Audio Interruptions
 * AVAudioSessionInterruptionTypeBegan
 */
- (void)stopAudioDevice{
    [endpoint stopAudioDevice];
}

////Register pushkit token
//- (void)registerToken:(NSData*)token
//{
//    [endpoint registerToken:token];
//}
//
////receive and pass on (information or a message)
//- (void)relayVoipPushNotification:(NSDictionary*)pushdata
//{
//    [endpoint relayVoipPushNotification:pushdata];
//}
@end
