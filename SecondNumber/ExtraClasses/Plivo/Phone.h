//
//  Phone.h
//  PlivoOutgoingApp
//
//  Created by Iwan BK on 10/2/13.
//  Copyright (c) 2013 Plivo. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "PlivoEndpoint.h"
#import <PlivoVoiceKit/PlivoEndpoint.h>

@interface Phone : NSObject

+ (Phone *)sharedInstance;

/* login */
- (void)login:(NSString *)endPointUserName endPointPassword:(NSString*)password;
- (void)login:(NSString *)endPointUserName endPointPassword:(NSString*)password deviceToken:(NSData*)token;

/* make call with extra headers */
- (PlivoOutgoing *)callWithDest:(NSString *)dest andHeaders:(NSDictionary *)headers;

/* set delegate for plivo endpoint object */
- (void)setDelegate:(id)delegate;

- (void) enableAudio;

- (void) disableAudio;

- (void) logout;

-(void)registerToken:(NSData*)notificationToken;

/* send keepalive data to plivo server*/
- (void)keepAlive;


- (void)configureAudioSession;

/*
 * To Start Audio service
 * To handle Audio Interruptions
 * AVAudioSessionInterruptionTypeEnded
 */
- (void)startAudioDevice;
/*
 * To Start Audio service
 * To handle Audio Interruptions
 * AVAudioSessionInterruptionTypeBegan
 */
- (void)stopAudioDevice;

//receive and pass on (information or a message)
- (void)relayVoipPushNotification:(NSDictionary*)pushdata;

@end
