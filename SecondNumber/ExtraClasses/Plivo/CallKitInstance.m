//
//  CallKitInstance.m
//  ObjCVoiceCallingApp
//
//  Created by Siva  on 17/04/17.
//  Copyright © 2017 Plivo. All rights reserved.
//

#import "CallKitInstance.h"
#import "Phone.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
#import "OnCallVC.h"
#import "LoginVC.h"
#import "DialerVC.h"
#import "Constant.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "WebApiController.h"
#import "UtilsClass.h"
#import "GlobalData.h"
#import "AppDelegate.h"
#import "PopUpViewController.h"
#import "WebApiController.h"
#import "CallInfo.h"


#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
//#import "UIView+Toast.h"
@implementation CallKitInstance
{
    PlivoOutgoing *outCall;
    PlivoIncoming *incCall;
    NSDictionary *headers;
    WebApiController *obj;
    
}

+ (CallKitInstance *)sharedInstance
{
    //Singleton instance
    static CallKitInstance *sharedInstance = nil;
    if(sharedInstance == nil)
    {
        sharedInstance = [[CallKitInstance alloc] init];
    }
    return sharedInstance;
}

- (id)init
{
    if(self = [super init])
    {
        
        CXProviderConfiguration* configuration = [[CXProviderConfiguration alloc] initWithLocalizedName:@"CallHippo"];
        configuration.supportsVideo = FALSE;
        configuration.maximumCallGroups = 1;
        configuration.maximumCallsPerCallGroup = 1;
        //configuration.supportedHandleTypes = [[NSSet alloc] initWithObjects:[NSNumber numberWithInt:(int)CXHandleTypePhoneNumber], nil];
//        configuration.iconTemplateImageData =

        UIImage *icon = [UIImage imageNamed:@"callhippo"];
        configuration.iconTemplateImageData = UIImagePNGRepresentation(icon);
        
        //NSLog(@"Icoming Call Cj %@",configuration);
        self.callKitProvider = [[CXProvider alloc] initWithConfiguration:configuration];
        self.callKitCallController = [[CXCallController alloc] init];
        self.calls_uuids = [[NSMutableArray alloc] init];
        self.callObserver = [[CXCallObserver alloc] init];
        
        [self.callKitProvider setDelegate:self queue:dispatch_get_main_queue()];
        [[Phone sharedInstance] setDelegate:self];
        [self.callObserver setDelegate:self queue:dispatch_get_main_queue()];
        
//        CXCallController *callController = [[CXCallController alloc] initWithQueue:dispatch_get_main_queue()];
//
//        [callController.callObserver setDelegate:self queue:dispatch_get_main_queue()];
//        self.callKitCallController = callController;

        return self;
    }
    return nil;
}
- (instancetype)initWithType:(CXHandleType)type
                       value:(NSString *)value
{
    //NSLog(@"Trushang : Callkit : Type :");
    //NSLog(@"Trushang : Callkit : value :");
    
    return self;
}

- (void)configAudioSession:(AVAudioSession *)audioSession {
    if (@available(iOS 10, *)) {
        // iOS 11 (or newer) ObjC code
        
        NSError *err = nil;
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                             mode:AVAudioSessionModeVoiceChat
                          options:AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP
                            error:&err];
        if (err) {
            NSLog(@"Unable to change audio session because: %@", err.localizedDescription);
            err = nil;
        }
        [audioSession setMode:AVAudioSessionModeVoiceChat error:&err];
        if (err) {
            NSLog(@"Unable to change audio mode because : %@", err.localizedDescription);
            err = nil;
        }
        double sampleRate = 48000.0;
        [audioSession setPreferredSampleRate:sampleRate error:&err];
        if (err) {
            NSLog(@"Unable to change preferred sample rate because : %@", err.localizedDescription);
            err = nil;
        }
    }
    else
    {
        
    }
    
}


- (void)reportIncomingCallFrom:(NSString *) from withUUID:(NSUUID *)uuid extHeader:(NSDictionary *)extHeader incommingcall:(PlivoIncoming *)incommingcall
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Plivo])
    {
    NSString *Userid = [Default valueForKey:USER_ID];
    if (Userid != nil)
    {
        
    headers = [[NSMutableDictionary alloc] init];
    headers = extHeader;
        
    NSString *incName;
    incName = extHeader[@"xphfrom"];
    incName = [incName
               stringByReplacingOccurrencesOfString:@": " withString:@""];
    incName = [incName
               stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        
        
       
       
        NSString *Phonenumber_get = incName;
        //NSLog(@"reportIncomingCallFrom Patel : %@",Phonenumber_get);
        
        //    //NSLog(@"Mixallcontact : %@",Mixallcontact);
        NSString *str = [Phonenumber_get stringByReplacingOccurrencesOfString:@"+" withString:@""] ;
        
        NSArray *Mixallcontact = [[NSArray alloc] init];
       //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
        Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
       
        //pNSPredicate *filter = [NSPredicate predicateWithFormat:@"(number_int contains[c] %@)",str];
        
        //-mixallcontact
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",str] ;
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",str] ;
        
        NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];
        
        NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
        
        NSString *contact_save = @"";
        
        if(filteredContacts.count != 0)
        {
            NSDictionary *dic = [filteredContacts objectAtIndex:0];
            //  //NSLog(@"Call Contact Find search dic == == > : %@",dic);
            //NSLog(@"Trushang Dic : %@",dic);
            NSString *str = [dic valueForKey:@"name"];
            contact_save = str;
            if(!dic[@"_id"])
            {
                //NSLog(@"Call Contact Find search dic == == > : %@",dic);
                NSString *ContactName = [dic valueForKey:@"name"];
                
                NSString *ContactNumber;
                if ([[dic valueForKey:@"numberArray"] count]>0) {
                    ContactNumber = [[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                }
                else
                {
                    ContactNumber =@"";
                }
                
                [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
            }
            
        }
        
        NSString *name = @"";
        NSString *number = Phonenumber_get;
        
        if(extHeader != nil)
        {
            NSDictionary *dic =  extHeader;
            //NSLog(@"TRTRTRTRTR : %@",dic);
            name = [dic valueForKey:@"xphfrom"];
            number = [dic valueForKey:@"xphfromnumber"];
            

            name = [name
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            name = [name
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
           
            
            
            
            name = [name stringByRemovingPercentEncoding];
            NSString *name1 = [name stringByReplacingOccurrencesOfString:@"+" withString:@""];
            //NSLog(@"Values : %d",[self validateString:name1 withPattern:@"^[0-9]+$"]);
            if([self validateString:name1 withPattern:@"^[0-9]+$"])
            {
                
                if(![contact_save isEqualToString:@""])
                {
                    name = contact_save;
                }
            }
            else
            {
                //            if(![contact_save isEqualToString:@""])
                //            {
                //                name = contact_save;
                //            }
            }
            
            //User exists
        }
        else
        {
            name = Phonenumber_get;
            
            //User doesn't exist
        }
        
        
        if([self validateString:name withPattern:@"^[0-9]+$"])
        {
            NSString *str = @"+";
            name = [NSString stringWithFormat:@"%@%@",str,name];
            
            if ([Default boolForKey:kIsNumberMask] == true) {
                name = [UtilsClass get_masked_number:name];
                
                NSLog(@"Final Name mask : Trushang --> %@",name);
            }
        }
        
        NSLog(@"Final Name  : Trushang --> %@",name);
        
    CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypePhoneNumber value:name];
    
    CXCallUpdate* callUpdate = [[CXCallUpdate alloc] init];
    //callUpdate.remoteHandle = callHandle;
        
        callUpdate.remoteHandle = [[CXHandle alloc] initWithType:CXHandleTypePhoneNumber value:number];
        callUpdate.localizedCallerName = name;
        
    callUpdate.supportsDTMF = YES;
    callUpdate.supportsHolding = YES;
    callUpdate.supportsGrouping = TRUE;
    callUpdate.supportsUngrouping = TRUE;
    callUpdate.hasVideo = NO;
   
    incCall = incommingcall;
        
    [[Phone sharedInstance] setDelegate:self];
        
      
       
        
        
     //2.1.12
//    [[[CallKitInstance sharedInstance] callKitProvider] setDelegate:self queue:dispatch_get_main_queue()];
//
//    [[[CallKitInstance sharedInstance] callObserver] setDelegate:self queue:dispatch_get_main_queue()];

        
    [self.callKitProvider reportNewIncomingCallWithUUID:uuid update:callUpdate completion:^(NSError * _Nullable error) {
        
        if(error)
        {
            [[Phone sharedInstance] stopAudioDevice];
            
            if(self->incCall)
            {
                
                if(self->incCall.state != Ongoing)
                {
                    ////NSLog(@"Incoming call - Reject");
                    [self->incCall reject];
                }
                else
                {
                    ////NSLog(@"Incoming call - Hangup");
                    [self->incCall hangup];
                }
                self->incCall = nil;
                
            }
            //2.1.12
            if(self->outCall)
            {
                ////NSLog(@"Outgoing call - Hangup");
                [self->outCall hangup];
                self->outCall = nil;

            }
        }
        else
        {
            [[Phone sharedInstance] configureAudioSession];
//            [[Phone sharedInstance] startAudioDevice];
        }
        
    }];
    
    }
    }
}

- (void)provider:(CXProvider *)provider performAnswerCallAction:(CXAnswerCallAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Plivo])
    {
    if(incCall)
    {
        [CallKitInstance sharedInstance].callUUID = [action callUUID];
        [incCall answer];
        [[Phone sharedInstance] setDelegate:self];
        
        UIWindow *mainWindow = [UIApplication sharedApplication].windows[0];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
        OnCallVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
       // NewOnCallVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

        NSString *incName;
        incName = @"";
        if([headers valueForKey:@"xphfrom"] != nil)
        {
            incName = [headers valueForKey:@"xphfrom"];
            
            incName = [incName
                    stringByReplacingOccurrencesOfString:@": " withString:@""];
            incName = [incName
                    stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        }
        
        
        
        NSString *Phonenumber_get = incName;
        //NSLog(@"performAnswerCallAction Patel : %@",Phonenumber_get);
        
        
        NSString *name = @"";
        NSString *transferBy = @"";
        NSString *number = Phonenumber_get;
        NSString *xphto = @"";
       
        
        if(headers != nil)
        {
            //NSLog(@"performAnswerCallAction Name Exist ");
            NSDictionary *dic =  headers;
            name = [dic valueForKey:@"xphfrom"];
            number = [dic valueForKey:@"xphfromnumber"];
            transferBy = [dic valueForKey:@"xphfirstcallusername"];
            name = [name
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            name = [name
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            
            number = [number
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            number = [number
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            xphto = [dic valueForKey:@"xphto"];
            
            
            NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
            txtText.text = @"";
//            [txtText insertText:number];
            [txtText insertText:[dic valueForKey:@"xphtotransferNumber"]];
            NSString *Country_ShotName = txtText.country.countryCode ? txtText.country.countryCode : @"";
            [Default setValue:[Country_ShotName lowercaseString] forKey:Selected_Department_Flag];
            [Default setValue:xphto forKey:Selected_Department];
            //User exists
        }
        else
        {
            //NSLog(@"performAnswerCallAction Name not Exist ");
            name = Phonenumber_get;
            //User doesn't exist
        }
        
        
        if([self validateString:name withPattern:@"^[0-9]+$"])
        {
            NSString *str = @"+";
            name = [NSString stringWithFormat:@"%@%@",str,name];
        }
        
        if([self validateString:number withPattern:@"^[0-9]+$"])
        {
            NSString *str = @"+";
            number = [NSString stringWithFormat:@"%@%@",str,number];
        }
        
        vc2.CallStatus = INCOMING;
        vc2.CallStatusfinal = INCOMING;
        vc2.ContactName = name ? name : @"";
        vc2.ContactNumber = number ? number : @"";
        vc2.transferCall = transferBy ? transferBy : @"";
        vc2.IncCallOutCall = @"Incoming";
        vc2.Plivo_Incomingcall = incCall;
        [Default setValue:Timer_Start forKey:Timer_Call];
        [Default synchronize];
        
        UINavigationController *navController123 = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
        NSArray *views = navController123.viewControllers;
        MainViewController *mainviews = [views objectAtIndex:0];
        UINavigationController *navigationControllerfinal = (UINavigationController *)mainviews.rootViewController;
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        [arr addObjectsFromArray:navigationControllerfinal.viewControllers];
        
        int osnum = [[[UIDevice currentDevice] systemVersion] intValue];
          if (osnum >= 15 && [arr count] == 0) {
              [arr addObject:vc1];
          }
        
        [arr addObject:vc2];
        NSLog(@"Nav :: %@",navigationControllerfinal.viewControllers);
        NSLog(@"Views :: %@",arr);
        
        [navigationController setViewControllers:arr animated:true];
        MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        mainViewController.rootViewController = navigationController;
        [mainViewController setupWithType:11];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
        navController.navigationBar.hidden = true;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        navController.navigationBar.hidden = true;
        [appDelegate window].rootViewController = navController;
        [[appDelegate window] setNeedsLayout];
        [[appDelegate window] makeKeyAndVisible];
    }
    outCall = nil;
   // [action fulfill];
        [action fulfillWithDateConnected:[NSDate date]];
    }
}
- (void)provider:(CXProvider *)provider performStartCallAction:(CXStartCallAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"perform start call action ::> ");
        
    [[Phone sharedInstance] configureAudioSession];
    [self configAudioSession:[AVAudioSession sharedInstance]];
    
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *selno1 = [Default valueForKey:SELECTEDNO];
    NSString *selno = [Default valueForKey:CALL_NUMBER];
    selno = [selno stringByReplacingOccurrencesOfString:@"+" withString:@""];
    selno = [selno stringByReplacingOccurrencesOfString:@"(" withString:@""];
    selno = [selno stringByReplacingOccurrencesOfString:@")" withString:@""];
    selno = [selno stringByReplacingOccurrencesOfString:@" " withString:@""];
    selno = [selno stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    selno1 = [selno1 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    //NSLog(@"\n \n \n Plivo_call_number_callkit : %@",selno1);
    NSString *strReminderId = [Default valueForKey:XPH_EXTRA_HEADER_REMINDER];
    //NSLog(@"-----------%@-----------",strReminderId);
        
        
        NSDictionary *extraHeaders;
        
        if ([Default valueForKey:kbackendProvider]) {
            
            NSString *backendProvider = [Default valueForKey:kbackendProvider];
           
            
            if ([backendProvider isEqualToString:@""]) {
                extraHeaders = @{@"X-PH-Userid":userID,
                                 @"X-PH-Fromnumber":selno1,@"X-PH-Devicetype":@"iOS",
                    @"X-PH-reminderId":strReminderId
                                               
                };
            }
            else
            {
                extraHeaders = @{@"X-PH-Userid":userID,
                                 @"X-PH-Fromnumber":selno1,@"X-PH-Devicetype":@"iOS",
                    @"X-PH-reminderId":strReminderId,
                    @"X-PH-Backendprovider":backendProvider
                                               
                };
            }
            NSLog(@"extra headers:: %@",extraHeaders);
        }
        else
        {
            
            extraHeaders = @{@"X-PH-Userid":userID,
                             @"X-PH-Fromnumber":selno1,@"X-PH-Devicetype":@"iOS",
                @"X-PH-reminderId":strReminderId
                                           
            };
            NSLog(@"extra headers:: %@",extraHeaders);
        }
       
    
     
        
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:XPH_EXTRA_HEADER_REMINDER];
    PlivoOutgoing *plivo_outgoing_call;
        
     
   // plivo_outgoing_call = [[Phone sharedInstance] callWithDest:selno andHeaders:extraHeaders];
        
        NSLog(@"call with dest ::>> %@",action.handle.value);
        
    outCall = [[Phone sharedInstance] callWithDest:action.handle.value andHeaders:extraHeaders];
      
        
        NSLog(@"perform start call action ::> ");
        //2.1.17

        
        if (incCall != nil) {

            NSLog(@"full fill");
            [action fulfillWithDateStarted:[NSDate date]];

        }
        
       
        
    }
}


- (void)provider:(CXProvider *)provider performEndCallAction:(CXEndCallAction *)action
{
        NSString *callingprovider = [Default valueForKey:CallingProvider];
           if([callingprovider isEqualToString:Login_Plivo])
           {
                //notification to load call logs
               NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"callDisconnected_loadCallLogs" object:self userInfo:nil];
      
               
               [[Phone sharedInstance] stopAudioDevice];
               
                dispatch_async(dispatch_get_main_queue(), ^{
    if(self->incCall)
    {
        
        if(self->incCall.state != Ongoing)
        {
            ////NSLog(@"Incoming call - Reject");
            [self->incCall reject];
        }
        else
        {
            ////NSLog(@"Incoming call - Hangup");
            [self->incCall hangup];
        }
        self->incCall = nil;
        
    }
    
    if(self->outCall)
    {
        ////NSLog(@"Outgoing call - Hangup");
        [self->outCall hangup];
        self->outCall = nil;
        
    }
    
    [action fulfill];
                    
                });
    }

    
}

- (void)provider:(CXProvider *)provider performSetMutedCallAction:(CXSetMutedCallAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
       if([callingprovider isEqualToString:Login_Plivo])
       {
    if(action.muted)
    {

        if(incCall)
        {
            [incCall unmute];
        }
        
        if(outCall)
        {
            [outCall unmute];
        }
        
    }
    else
    {
        
        if(incCall)
        {
            [incCall mute];
        }
        
        if(outCall)
        {
            [outCall mute];
        }
        
    }
       }
     
}

- (void)provider:(CXProvider *)provider performSetHeldCallAction:(CXSetHeldCallAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
       if([callingprovider isEqualToString:Login_Plivo])
       {
    if(action.onHold)
    {

        if(incCall)
        {
            [[Phone sharedInstance] stopAudioDevice];
            [self holdUnholdApiCall:@"callhold/plivo"];
        }
        
        if(outCall)
        {
            [[Phone sharedInstance] stopAudioDevice];
            [self holdUnholdApiCall:@"callhold/plivo"];
        }
    }
    else
    {
        
        
        if(incCall)
        {
            [[Phone sharedInstance] stopAudioDevice];
            [self holdUnholdApiCall:@"callunhold/plivo"];
        }
        
        if(outCall)
        {
             [[Phone sharedInstance] stopAudioDevice];
            [self holdUnholdApiCall:@"callunhold/plivo"];
        }
        [[Phone sharedInstance] startAudioDevice];
        
    }
    [action fulfill];
       }
}

- (void)provider:(CXProvider *)provider performPlayDTMFCallAction:(CXPlayDTMFCallAction *)action
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
       if([callingprovider isEqualToString:Login_Plivo])
       {
           [action fulfill];
       }
    
}

- (void)performStartCallActionWithUUID:(NSUUID *)uuid handle:(NSString *)handle
{
    
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Plivo])
    {
    switch ([[AVAudioSession sharedInstance] recordPermission]) {
            
        case AVAudioSessionRecordPermissionGranted:
        {
            
            NSLog(@"perform start call working calling :");
            
            //2.1.17
            [[CallKitInstance sharedInstance].callKitProvider setDelegate:self queue:dispatch_get_main_queue()];

            [[CallKitInstance sharedInstance].callObserver setDelegate:self queue:dispatch_get_main_queue()];

//            [[Phone sharedInstance] startAudioDevice];


            if(uuid == nil || handle == nil)
            {
                NSLog(@"UUID or Handle nil");
                return;
            }

           // handle = [NSString stringWithFormat:@"+%@",handle];

            CXHandle *callHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:handle];
            
        
            
            NSLog(@"handle final:: %@",callHandle);
            
            CXStartCallAction *startCallAction = [[CXStartCallAction alloc] initWithCallUUID:uuid handle:callHandle];
            CXTransaction *transaction = [[CXTransaction alloc] initWithAction:startCallAction];
            
            [[CallKitInstance sharedInstance].callKitCallController requestTransaction:transaction completion:^(NSError * _Nullable error)
             {
                 
                 if(error)
                 {
                     NSLog(@"perform start call error : %@",error);

                 }
                 else
                 {
                    
                     CXCallUpdate *callUpdate = [[CXCallUpdate alloc] init];
                     callUpdate.remoteHandle = callHandle;
                     callUpdate.supportsDTMF = YES;
                     callUpdate.supportsHolding = YES;
                     callUpdate.supportsGrouping = NO;
                     callUpdate.supportsUngrouping = NO;
                     callUpdate.hasVideo = NO;
                     
                     dispatch_async(dispatch_get_main_queue(), ^{

                        [[CallKitInstance sharedInstance].callKitProvider reportCallWithUUID:uuid updated:callUpdate];
                         
                        
                      //   [[[CallKitInstance sharedInstance] callKitProvider] reportOutgoingCallWithUUID:uuid startedConnectingAtDate:[NSDate date]];
                         
                      //   [[[CallKitInstance sharedInstance] callKitProvider] reportOutgoingCallWithUUID:uuid startedConnectingAtDate:[NSDate date]];

                         
                     });
                 }
             }];
            break;
        }
        case AVAudioSessionRecordPermissionDenied:
            //            [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
            
            break;
        case AVAudioSessionRecordPermissionUndetermined:
            // This is the initial state before a user has made any choice
            // You can use this spot to request permission here if you want
            break;
        default:
            break;
    }
    }
}

- (void)performEndCallActionWithUUID:(NSUUID *)uuid
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    [Default setValue:Timer_Stop forKey:Timer_Call];
    if([callingprovider isEqualToString:Login_Plivo])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CXEndCallAction *endCallAction = [[CXEndCallAction alloc] initWithCallUUID:uuid];
            CXTransaction *trasanction = [[CXTransaction alloc] initWithAction:endCallAction];
            
            [[CallKitInstance sharedInstance].callKitCallController requestTransaction:trasanction completion:^(NSError * _Nullable error) {
                
                if(error)
                {
                    NSLog(@"EndCallAction transaction request failed: %@", [error localizedDescription]);

//                    [[Phone sharedInstance] stopAudioDevice];
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //[UtilityClass makeToast:kREQUESTFAILED];
                        
                        [[Phone sharedInstance] stopAudioDevice];
                        
                        if(self->incCall)
                        {
                            
                            if(self->incCall.state != Ongoing)
                            {
                                NSLog(@"Incoming call - Reject");
                                [self->incCall reject];
                            }
                            else
                            {
                                NSLog(@"Incoming call - Hangup");
                                [self->incCall hangup];
                            }
                            self->incCall = nil;
                            
                        }
                        
                        if(self->outCall)
                        {
                            NSLog(@"Outgoing call - Hangup");
                            [self->outCall hangup];
                            self->outCall = nil;
                            
                        }
                        
                       
                    });
                }
                else
                {
                    
                    NSLog(@"EndCallAction transaction request successful");
                     [[Phone sharedInstance] stopAudioDevice];

                   // [self ratting_popup];
                }
                
                
            }];
            
           
            
        });
        
    }
}

- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call{
   NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"call ended ended obsrver: >>>>>>>");

    if(![self.calls_uuids containsObject:call.UUID])
    {
        [self.calls_uuids addObject:call.UUID];
    }
        
        
    //NSLog(@"Trushang : CXCallState : %@",call.debugDescription);
    //NSLog(@"Trushang : CXCallState : CallUUID : %@",call.UUID);
    if (call == nil || call.hasEnded == YES) {
        NSLog(@"CXCallState : Disconnected");
        
       
       
    }
    
    if (call.isOutgoing == YES && call.hasConnected == NO) {
        NSLog(@"CXCallState : Dialing");
    }
    
    if (call.isOutgoing == NO  && call.hasConnected == NO && call.hasEnded == NO && call != nil) {
        NSLog(@"CXCallState : Incoming");
    }
    
    if (call.hasConnected == YES && call.hasEnded == NO) {
        NSLog(@"CXCallState : Connected");
    }
    if(call.hasEnded)
    {

        //NSLog(@"CXCallState : End %lu",(unsigned long)self.calls_uuids.count);
        
        NSLog(@"call uuids: >>>>>>> %@ ",self.calls_uuids);
        
//        if(self.calls_uuids.count > 2)
//        {
//           // for (int i =0; i<self.calls_uuids.count; i++) {
//
//                NSUUID *uuid = (NSUUID *)[self.calls_uuids objectAtIndex:0];
            
                //NSLog(@"\n \n \n \n \n \n \n \n  Trushang_code : www  Callkit www : UUID : %@\n \n \n \n \n ",uuid);
        if ((incCall != nil && incCall.state == Ongoing) || (outCall != nil && outCall.state == Ongoing))
        {
//                           if (!uuid) {
//                               return;
//                           }
            NSLog(@"call on held action: >>>>>>>  ");
            
                           CXSetHeldCallAction *act = [[CXSetHeldCallAction alloc] initWithCallUUID:[[CallKitInstance sharedInstance]callUUID] onHold:NO];
                           CXTransaction *tr = [[CXTransaction alloc] initWithAction:act];
                           [[CallKitInstance sharedInstance].callKitCallController requestTransaction:tr
                                                                                           completion:^(NSError *err)
                            {
                                //                                                                            2398985-af17-4515-b754-72173bf972c4
                                if (err) {
                                    //NSLog(@"Error requesting transaction: %@", err.localizedDescription);
                                } else {
                                    //NSLog(@"Requested transaction successfully");
                                    //                                                                               [self configAudioSession:[AVAudioSession sharedInstance]];
                                }
                            }];
         //   }
           

        }
        
        
       /* if ((incCall != nil && incCall.state == Ongoing) ||(outCall != nil && outCall.state == Ongoing)) {
            
            CXSetHeldCallAction *act = [[CXSetHeldCallAction alloc] initWithCallUUID:self.callUUID onHold:false];
            CXTransaction *tr = [[CXTransaction alloc] initWithAction:act];
            
            [[CallKitInstance sharedInstance].callKitCallController requestTransaction:tr
                                                                           completion:^(NSError *err)
            {
                
                if (err) {
                    NSLog(@"Error requesting held transaction: %@", err.localizedDescription);
                } else {
                    NSLog(@"Requested held transaction successfully");
                   
                }
            }];
        }*/
        
    }
    }
}


#pragma mark - CXProviderDelegate

- (void)providerDidReset:(CXProvider *)provider
{
   
}

- (void)providerDidBegin:(CXProvider *)provider
{
  
}
- (void)provider:(CXProvider *)provider didActivateAudioSession:(AVAudioSession *)audioSession
{
    NSString *callingprovider = [Default valueForKey:CallingProvider];
    if([callingprovider isEqualToString:Login_Plivo])
    {
        NSLog(@"did activate audio");
        [[Phone sharedInstance] startAudioDevice];
    }
}

- (void)provider:(CXProvider *)provider didDeactivateAudioSession:(AVAudioSession *)audioSession
{

}

- (void)provider:(CXProvider *)provider timedOutPerformingAction:(CXAction *)action
{

}


 
- (void)onIncomingCallRejected:(PlivoIncoming *)incoming
{
    // incCall = incoming;
    
    
    //NSLog(@"\n \n \n \n***************");
    NSLog(@"\n \n ******* CallKitInstance :-- onIncomingCallRejected ********");
    //NSLog(@"\n \n \n \n***************");
    
    if(incCall != nil)
    {
        
    [self performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
    
    //2.1.12
    incCall = nil;
    }

}

- (void)onIncomingCallHangup:(PlivoIncoming *)incoming
{
   
     //  incCall = incoming;
    
    

    //NSLog(@"\n \n \n \n***************");
    //NSLog(@"\n \n ******* CallKitInstance :-- onIncomingCallHangup ********");
    //NSLog(@"\n \n \n \n***************");
    

    // 2.1.12

    if(incCall != nil)
    {
        ////NSLog(@"Hangup Incoming call : UUID IS %@",[CallKitInstance sharedInstance].callUUID);
        
        [self performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
        
        incCall = nil;

        
   }
    
}
 
-(void)onIncomingCallInvalid:(PlivoIncoming *)incoming
{
    
    if (incCall != nil) {
        
        [self performEndCallActionWithUUID:[[CallKitInstance sharedInstance] callUUID]];
        incCall = nil;
    }
    
}
 
 
//2.1.17
-(void)onOutgoingCallAnswered:(PlivoOutgoing *)call
{
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        [[[CallKitInstance sharedInstance] callKitProvider] reportOutgoingCallWithUUID:[[CallKitInstance sharedInstance]callUUID] connectedAtDate:[NSDate date]];
//    });
}
-(void)onOutgoingCallRejected:(PlivoOutgoing *)call
{
    [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[[CallKitInstance sharedInstance] callUUID]];

}
- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        return [predicate evaluateWithObject:string];
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    
}

-(void)ratting_popup
{
    //NSLog(@"Incoming iutgoiung : %@",[Default valueForKey:INCOUTCALL]);
    if ([[Default valueForKey:INCOUTCALL] isEqualToString:@"false"])
    {
//        UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
//        alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PopUpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PopUpViewController"];
        if(incCall != nil)
        {
            vc.strIncOutCall = INCOMING;
        }
        else
        {
            vc.strIncOutCall = OUTGOING;
        }
        
        if (IS_IPAD) {
            
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
            //                        [self presentViewController:vc animated:YES completion:nil];
        } else {
            vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
            //                        [self presentViewController:vc animated:YES completion:nil];
        }
        
        
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
    
}

-(void)holdUnholdApiCall:(NSString *)holdplivo {
    
    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *authToken = [Default valueForKey:AUTH_TOKEN];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *plivoAuthToken =  [Default valueForKey:CALLHIPPO_AUTH_TOKEN];
    NSString *plivoAutId = [Default valueForKey:CALLHIPPO_AUTH_ID];
    NSString *url = [NSString stringWithFormat:@"%@",holdplivo];
    NSDictionary *passDict = @{@"authId":plivoAutId,
                               @"authToken":plivoAuthToken,
                               @"callHoldUrl":@"https://s3.amazonaws.com/callhippo_staging/call_hold/15168789827365a4c70c0bc9e741f843ad5c6.mp3",
                               @"calluid":@"1899ca94-518f-413b-9277-96b553f69c5a",
                               @"deviceType":@"iOS",
                               @"userId":userId};
    
//before    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
        obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response {
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************1  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    //NSLog(@"Hold : Response : %@",response1);
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        // [UtilsClass showAlert:@"Done" contro:self];
    }else {
        //NSLog(@"fail hold response");
        @try {
            UIViewController *view = [[UIViewController alloc] init];
         //p   [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:view];
        }
        @catch (NSException *exception) {
        }
    }
    }
}
@end
