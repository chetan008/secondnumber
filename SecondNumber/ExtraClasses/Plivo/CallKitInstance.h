//
//  CallKitInstance.h
//  ObjCVoiceCallingApp
//
//  Created by Siva  on 17/04/17.
//  Copyright © 2017 Plivo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CallKit/CallKit.h>
#import "Phone.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>

/*!
 * @discussion CallKitInstance class to maintain single Callkit instance in entire app lifecycle
 */

@interface CallKitInstance : NSObject<CXProviderDelegate, CXCallObserverDelegate,PlivoEndpointDelegate>
{
   
}
+ (CallKitInstance *)sharedInstance;
@property (strong, nonatomic) NSUUID* callUUID;
@property (strong, nonatomic) CXProvider *callKitProvider;
@property (strong, nonatomic) CXCallController* callKitCallController;
@property (strong, nonatomic) CXCallObserver *callObserver;
@property(nonatomic, strong) NSMutableArray *calls_uuids;



- (void)reportIncomingCallFrom:(NSString *) from withUUID:(NSUUID *)uuid extHeader:(NSDictionary *)extHeader incommingcall:(PlivoIncoming *)incommingcall;
- (void)performEndCallActionWithUUID:(NSUUID *)uuid;
- (void)configAudioSession:(AVAudioSession *)audioSession;
- (void)performStartCallActionWithUUID:(NSUUID *)uuid handle:(NSString *)handle;
@end
