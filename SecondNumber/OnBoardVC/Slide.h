//
//  Slide.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Slide : UIView
{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *page_imageview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pagetitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pageSubtitle;
@end

NS_ASSUME_NONNULL_END
