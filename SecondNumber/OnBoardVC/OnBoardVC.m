//
//  OnBoardVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "OnBoardVC.h"
#import "Constant.h"
@interface OnBoardVC ()

@end

@implementation OnBoardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self viewdesign];
    
    NSArray *slides = [self createSlides];
    [self setupSlideScrollView:slides];
   
    
    _pageControl.numberOfPages = slides.count;
    _pageControl.currentPage = 0;
    
}

-(void)viewdesign
{
     [[self navigationController] setNavigationBarHidden:YES animated:NO];

    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    UIColor *color1 = (id) [UIColor colorWithRed:230.0/255.0 green:99.0/255.0 blue:92.0/255.0 alpha:1.0].CGColor;

    UIColor *color2 = (id) [UIColor colorWithRed:248.0/255.0 green:152.0/255.0 blue:56.0/255.0 alpha:1.0].CGColor;
    
    gradient.frame = self.view.bounds;
    gradient.colors = @[color2,color1];


    [self.view.layer insertSublayer:gradient atIndex:0];
    
    _btn_getStarted.layer.cornerRadius = 5;
    _btn_getStarted.clipsToBounds = true;
    [_btn_getStarted addTarget:self action:@selector(btn_getStartedClick:) forControlEvents:UIControlEventTouchUpInside];

    [_scrollview setDelegate:self];
    [_pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    

}

-(NSArray *)createSlides
{
    
    NSArray * arr = [[NSBundle mainBundle] loadNibNamed:@"Slide" owner:self options:nil];
    Slide *s1 = [arr objectAtIndex:0];
    
    s1.lbl_pagetitle.text = @"Your Global Phone System";
    s1.lbl_pageSubtitle.text = @"Get your phone number from 50+ countries and call around the globe";
    s1.page_imageview.image = [UIImage imageNamed:@"onboard_country"];
    
    NSArray * arr2 = [[NSBundle mainBundle] loadNibNamed:@"Slide" owner:self options:nil];
    Slide *s2 = [arr2 objectAtIndex:0];
    
    s2.lbl_pagetitle.text = @"Record Your Business Calls";
    s2.lbl_pageSubtitle.text = @"Record your conversations and refer whenever you need it";
    s2.page_imageview.image = [UIImage imageNamed:@"onboard_home"];
    
    NSArray * arr3 = [[NSBundle mainBundle] loadNibNamed:@"Slide" owner:self options:nil];
    Slide *s3 = [arr3 objectAtIndex:0];
    
    s3.lbl_pagetitle.text = @"Never Miss a Call Ever Again";
    s3.lbl_pageSubtitle.text = @"Forward your business calls to your mobile number and never worry about missing an important call";
    s3.page_imageview.image = [UIImage imageNamed:@"onboard_misscall"];
    
    NSArray * arr4 = [[NSBundle mainBundle] loadNibNamed:@"Slide" owner:self options:nil];
    Slide *s4 = [arr4 objectAtIndex:0];
    
    s4.lbl_pagetitle.text = @"100% Professional";
    s4.lbl_pageSubtitle.text = @"Setup personalized greetings,voicemail and IVR and make your customers feel valued";
    s4.page_imageview.image = [UIImage imageNamed:@"onboard_features"];
    
    NSArray *slidesArr = [NSArray arrayWithObjects:s1,s2,s3,s4, nil];
    return slidesArr;
    
}

-(void)setupSlideScrollView:(NSArray *)slides
{

    _scrollview.contentSize = CGSizeMake(self.view.frame.size.width * slides.count, self.scrollview.frame.size.height);
    [_scrollview setPagingEnabled:true] ;
    
    for (int i=0; i < slides.count; i++) {
        
        Slide *slide = (Slide *) [slides objectAtIndex:i];
        slide.frame = CGRectMake(self.slidesView.frame.size.width * ((CGFloat)i), 0, _slidesView.frame.size.width, slide.frame.size.height);
        
        _slidesView.frame = CGRectMake(_slidesView.frame.origin.x, _slidesView.frame.origin.y, _slidesView.frame.size.width, slide.frame.size.height);
        
        _slidesView.center = self.view.center;
        
        
        
        [_slidesView addSubview:slide];
        
    }

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self hideShowLeftSwipeButton];
    CGFloat pageWidth = self.scrollview.frame.size.width;
    float fractionalPage = self.scrollview.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
    
}

-(IBAction)btn_leftSwipeClick:(id)sender
{
    [self hideShowLeftSwipeButton];
    
    if (_pageControl.currentPage > 0) {
       
        _pageControl.currentPage -= 1;
        CGFloat x = _pageControl.currentPage * _scrollview.frame.size.width;
           [_scrollview setContentOffset:CGPointMake(x, 0) animated:YES];
    }
    
}
-(IBAction)btn_getStartedClick:(id)sender
{
    _btn_getStarted.enabled = false;
    [self goToLoginPage];
}

-(IBAction)btn_rightSwipeClick:(id)sender
{
    [self hideShowLeftSwipeButton];
    if (_pageControl.currentPage < 3) {
        
        _pageControl.currentPage += 1;
        CGFloat x = _pageControl.currentPage * _scrollview.frame.size.width;
        [_scrollview setContentOffset:CGPointMake(x, 0) animated:YES];
    }
    else
    {
        [self goToLoginPage];
    }
    
}
-(void)goToLoginPage
{
    [Default setValue:@"1" forKey:IS_Installed];
    
    LoginVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"LoginVC"];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
   
    [[self navigationController] pushViewController:vc animated:YES];
}
-(void)changePage:(id)sender
{
    [self hideShowLeftSwipeButton];
    CGFloat x = _pageControl.currentPage * _scrollview.frame.size.width;
           [_scrollview setContentOffset:CGPointMake(x, 0) animated:YES];
}

-(void)hideShowLeftSwipeButton
{
    if (_pageControl.currentPage >= 1) {
        _btn_leftswipe.hidden = false;
    }
    else
    {
        _btn_leftswipe.hidden = true;
    }
    
    if(_pageControl.currentPage == 3)
    {
        [_btn_rightswipe setImage:[UIImage imageNamed:@"next-arrow-white"] forState:UIControlStateNormal];
    }
    else
    {
         [_btn_rightswipe setImage:[UIImage imageNamed:@"right-arrow-white"] forState:UIControlStateNormal];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
