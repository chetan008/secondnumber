//
//  OnBoardVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>
#import "Slide.h"
#import "LoginVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface OnBoardVC : UIViewController<UIScrollViewDelegate>

@property (assign, nonatomic) NSInteger index;

@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageSubtitles;
@property (strong, nonatomic) NSArray *pageImages;

@property (strong, nonatomic) IBOutlet UIView *slidesView;
@property (strong, nonatomic) IBOutlet UIButton *btn_getStarted;
@property (strong, nonatomic) IBOutlet UIButton *btn_leftswipe;
@property (strong, nonatomic) IBOutlet UIButton *btn_rightswipe;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@end

NS_ASSUME_NONNULL_END
