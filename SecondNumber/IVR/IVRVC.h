//
//  IVRVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "WebApiController.h"
@import TwilioVoice;
NS_ASSUME_NONNULL_BEGIN
@protocol IVRdelegate <NSObject>

@optional
- (void)dtmfdigits:(NSString *)data;
- (void)Linphone_dtmfdigits:(NSString *)data;
@end

@interface IVRVC : UIView
{
    WebApiController *obj;
    NSString *dtmf_digit;
}

@property (nonatomic, strong)   id<IVRdelegate> delegate;

@property char digit;
@property (weak, nonatomic) IBOutlet UIButton *btn_one;
@property (weak, nonatomic) IBOutlet UIButton *btn_two;
@property (weak, nonatomic) IBOutlet UIButton *btn_three;
@property (weak, nonatomic) IBOutlet UIButton *btn_four;
@property (weak, nonatomic) IBOutlet UIButton *btn_five;
@property (weak, nonatomic) IBOutlet UIButton *btn_six;
@property (weak, nonatomic) IBOutlet UIButton *btn_seven;
@property (weak, nonatomic) IBOutlet UIButton *btn_eight;
@property (weak, nonatomic) IBOutlet UIButton *btn_nine;
@property (weak, nonatomic) IBOutlet UIButton *btn_star;
@property (weak, nonatomic) IBOutlet UIButton *btn_zero;
@property (weak, nonatomic) IBOutlet UIButton *btn_has;
@property (weak, nonatomic) IBOutlet UITextField *txt_call_number_text;
@end

NS_ASSUME_NONNULL_END
