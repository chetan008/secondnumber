//
//  IVRVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "IVRVC.h"
#import "UtilsClass.h"
#import "WebApiController.h"

@implementation IVRVC
@synthesize digit;

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) [self setup];
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) [self setup];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self setup];
    return self;
}

- (void)setup
{
    [self keypad_setup];
}
-(void)keypad_setup
{
    _txt_call_number_text.text = @"";
    [UtilsClass keypadtextset:_btn_one text:@"1\n  " tot:2];
    [UtilsClass keypadtextset:_btn_two text:@"2\nABC" tot:3];
    [UtilsClass keypadtextset:_btn_three text:@"3\nDEF" tot:3];
    [UtilsClass keypadtextset:_btn_four text:@"4\nGHI" tot:3];
    [UtilsClass keypadtextset:_btn_five text:@"5\nJKL" tot:3];
    [UtilsClass keypadtextset:_btn_six text:@"6\nMNO" tot:3];
    [UtilsClass keypadtextset:_btn_seven text:@"7\nPQRS" tot:4];
    [UtilsClass keypadtextset:_btn_eight text:@"8\nTUV" tot:3];
    [UtilsClass keypadtextset:_btn_nine text:@"9\nWXYZ" tot:4];
    [UtilsClass keypadtextset:_btn_zero text:@"0\n  " tot:2];
    [UtilsClass keypadtextset:_btn_star text:@"*\n  " tot:2];
    [UtilsClass keypadtextset:_btn_has text:@"#\n  " tot:2];
    
}
- (IBAction)btn_dialpad_click:(UIButton *)sender
{
    
    
    if([sender tag] == 1)
    {
        
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@1",_txt_call_number_text.text];
        digit = '1';
        dtmf_digit = @"1";
        //         //NSLog(@"Trushang : DTMF : %c",digit);
        
    }
    else if ([sender tag] == 2)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@2",_txt_call_number_text.text];
        digit = '2';
        dtmf_digit = @"2";
        
    }
    else if ([sender tag] == 3)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@3",_txt_call_number_text.text];
        digit = '3';
        dtmf_digit = @"3";
    }
    else if ([sender tag] == 4)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@4",_txt_call_number_text.text];
        dtmf_digit = @"4";
    }
    else if ([sender tag] == 5)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@5",_txt_call_number_text.text];
        digit = '5';
        dtmf_digit = @"5";
    }
    else if ([sender tag] == 6)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@6",_txt_call_number_text.text];
        digit = '6';
        dtmf_digit = @"6";
    }
    else if ([sender tag] == 7)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@7",_txt_call_number_text.text];
        digit = '7';
        dtmf_digit = @"7";
    }
    else if ([sender tag] == 8)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@8",_txt_call_number_text.text];
        digit = '8';
        dtmf_digit = @"8";
    }
    else if ([sender tag] == 9)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@9",_txt_call_number_text.text];
        digit = '9';
        dtmf_digit = @"9";
    }
    else if ([sender tag] == 10)
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@*",_txt_call_number_text.text];
        digit = '*';
        dtmf_digit = @"*";
        
    }
    else if ([sender tag] == 11)
    {
        _txt_call_number_text.text =  [NSString stringWithFormat:@"%@0",_txt_call_number_text.text];
        digit = '0';
        dtmf_digit = @"0";
        
    }
    else
    {
        _txt_call_number_text.text = [NSString stringWithFormat:@"%@#",_txt_call_number_text.text];
        digit = '#';
        dtmf_digit = @"#";
        
    }
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if ([LoginProvider isEqualToString:Login_Plivo]){
        [self dtmf_call:dtmf_digit];
    }else if ([LoginProvider isEqualToString:Login_Linphone]){
        NSString *isdtmf = [Default valueForKey:IS_DTMF_BY_API];
        int dtmf = [isdtmf intValue];
        if (dtmf == 1) {
            [self dtmf_call:dtmf_digit];
        }else{
            if(_delegate && [_delegate respondsToSelector:@selector(dtmfdigits:)])
            {
                [_delegate Linphone_dtmfdigits:dtmf_digit];
            }
        }
    }
    else{
        NSString *isdtmf = [Default valueForKey:IS_DTMF_BY_API];
        int dtmf = [isdtmf intValue];
        if (dtmf == 1) {
            [self dtmf_call:dtmf_digit];
        }else{
            if(_delegate && [_delegate respondsToSelector:@selector(dtmfdigits:)])
            {
                [_delegate dtmfdigits:dtmf_digit];
            }
        }
    }
    
}
- (void)dtmf_call:(NSString*)dtmf {
    
    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *Auth_Id = [Default valueForKey:CALLHIPPO_AUTH_ID] ? [Default valueForKey:CALLHIPPO_AUTH_ID] : @"";
    NSString *Auth_Token = [Default valueForKey:CALLHIPPO_AUTH_TOKEN] ? [Default valueForKey:CALLHIPPO_AUTH_TOKEN] : @"";
    //     NSString *Call_Hold_Url = [Default valueForKey:HOLD_URL] ? [Default valueForKey:HOLD_URL] : @"";
    NSString *userId = [Default valueForKey:USER_ID] ? [Default valueForKey:USER_ID] : @"";
    NSString *digit = dtmf ? dtmf : @"";
    
    //NSLog(@"FCMTOKEN :%@",fcmtoken);
    NSDictionary *passDict = @{@"authId":Auth_Id,
                               @"authToken":Auth_Token,
                               @"callHoldUrl":HOLD_URL,
                               @"calluid":@"",
                               @"deviceType":@"ios",
                               @"userId":userId,
                               @"digit":digit
    };
    
    //NSLog(@"DTMF_Response Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:DTMF_URL andParams:jsonString SuccessCallback:@selector(DTMF_Response:response:) andDelegate:self];
}

- (void)DTMF_Response:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : dtmf IVR : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
         [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        //NSLog(@"DTMF_Response : %@",response1);
    }
}

@end

