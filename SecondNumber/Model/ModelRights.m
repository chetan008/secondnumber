//
//  ModelRights.m
//  CallHippo
//
//  Created by CallHippo on 13/10/18.
//  Copyright © 2018 JDB Techs. All rights reserved.
//

#import "ModelRights.h"
//#import "BasicStuff.h"
#import "Constant.h"

@implementation ModelRights
@synthesize name;
@synthesize active;
- (void)dealloc {
    
}

- (instancetype)initWithName:(NSString *)name Active:(NSString *)active{
    self = [super init];
    if (self) {
        self.name = name;
        self.active = active;
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.name = [decoder decodeObjectForKey:@"name"];
        self.active = [decoder decodeObjectForKey:@"active"];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:name forKey:@"name"];
    [encoder encodeObject:active forKey:@"active"];

}

+(void) saveData:( NSMutableArray<ModelRights*> *)rightsArray {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:rightsArray];
    
    [Default setObject:data forKey:PHONENUMBER_ARRAY];
    [Default synchronize];
}

+(NSMutableArray <ModelRights *> *) loadRights {
    NSData *data = [Default valueForKey:PHONENUMBER_ARRAY];
    
    NSMutableArray<ModelRights *> *phoneDetail = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return phoneDetail;
}


@end


