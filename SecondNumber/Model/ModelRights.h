//
//  ModelRights.h
//  CallHippo
//
//  Created by CallHippo on 13/10/18.
//  Copyright © 2018 JDB Techs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelRights : NSObject<NSCoding> {
    NSString *name;
    NSString *active;
}
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *active;


- (instancetype)initWithName:(NSString *)name Active:(NSString *)active;
+(void) saveData:( NSMutableArray<ModelRights*> *)rightsArray;
+(NSMutableArray <ModelRights *> *)loadRights;

@end



