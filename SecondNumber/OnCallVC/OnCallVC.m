//
//  OnCallVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "OnCallVC.h"
#import "SecondNumber-Swift.h"
#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "UIImage+animatedGIF.h"
#import "TransferVC.h"
#import "IVRVC.h"
#import "PopUpViewController.h"
#import "UIView+Toast.h"



#import "Phone.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
#import "CallKitInstance.h"
#import "CallReminderPopUp.h"
#import "ACWViewController.h"
#import "AppDelegate.h"
#import "UIViewController+LGSideMenuController.h"
@import Firebase;
@import Tagging;
@import NKVPhonePicker;

#import "twilio_callkit.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
@import TwilioVoice;
@interface OnCallVC ()<IVRdelegate,TransferComplet,PlivoEndpointDelegate,TwilioDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,TaggingDataSource>
{
    BOOL incOutCall;
    
    NSDictionary *extraHeader;
    TransferVC *transferVC;
    IVRVC *ivrvc;
    WebApiController *obj;
    //    NSTimer *timer;
    //    int currMinute;
    //    int currSeconds;
    //    int currHours;
    
    //    PlivoOutgoing *outCall;
    //    PlivoIncoming *incCall;
    
    NSString *endcall;
    NSString *incOutNumb;
    NSString *isSecondTransfer;
    NSString *isnetworkstrenth;
    NSString *isnetworkstrenth_click;
    CGFloat Download_final;
    CGFloat Uownload_final;
    NSMutableArray *arr_speeds;
    NSMutableArray *arr_low_speeds;
    NSString *calling_provider;
    int network_strenth_down;
    NSString *extentionCall;
    
    bool recordControl;
    
    NSArray *omatchedList;
    NSArray *otaggedList;
    
    NSArray *tagsArray;
    NSMutableArray *tagIdArray;
    NSMutableArray *selTagArray;

    NSString *transferFlag;
    NSString *blindTransferFlag;
    
    BOOL isnoteSaved;

    NSDictionary *notesPassDict;
    NSString *isnotesInPlan;

   
    /* next release BOOL recordMainToggle;
    NSString *complianceOption;*/
    
    LinphoneCall *currentCall;
    
}

@property BOOL dialPadSelected;
@property BOOL reviewpopup;
@property BOOL transferSelected;
@property BOOL notesSelected;


@property (strong, nonatomic) IBOutlet Tagging *txtview_tagging;

@end

@implementation OnCallVC
@synthesize currHours,currMinute,currSeconds;
@synthesize timer;
@synthesize lblTimer;
@synthesize Plivo_Incomingcall;
@synthesize Plivo_outgoingcall;
@synthesize activeCall;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    self.lblTimer.text = @"";
    
    extentionCall = [Default valueForKey:ExtentionCall];
    self.reviewpopup = true;
    
    NSString *twilioProvider = [Default valueForKey:CallingProvider];
    if([twilioProvider isEqualToString:Login_Twilio])
    {
        [twilio_callkit sharedInstance].delegate = self;
        dispatch_async(dispatch_get_main_queue(), ^{
        [twilio_callkit sharedInstance].delegate = self;
        });
    }
    
    
    _lbl_speed_text.hidden = true;
    
    [self transferButtonEnable];
    
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transferButtonNotify:)
                                                 name:@"transferButtonNotify"
                                               object:nil];
    
    NSString *provider = [Default valueForKey:CallingProvider];
    if([provider isEqualToString:Login_Linphone])
    {
        
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLinphoneCallUpdate object:nil];
        

    
   [NSNotificationCenter.defaultCenter addObserver:self
                                                      selector:@selector(callUpdate:)
                                                          name:kLinphoneCallUpdate
                                                        object:nil];
    }
   
    // Twilio Delegate Notification Start
    
    // Twilio Delegate Notification End

    [UtilsClass view_shadow_boder_custom:_view_call_details];
    _view_call_details.layer.cornerRadius = 5.0;
    incOutCall = false;
    self->_view_network_strenth.hidden = true;
    endcall = @"";
    isnetworkstrenth = @"1";
    isnetworkstrenth_click = @"1";
    isSecondTransfer = @"false";
    NSLog(@"Oncall VC - View viewDidLoad");
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        [self addObserver];

         [[Phone sharedInstance] setDelegate:self];
        
        if(Plivo_Incomingcall != nil)
        {
            NSLog(@"Oncall VC - Plivo_Incomingcall");
            //            [[Phone sharedInstance] configureAudioSession];
            //            [[Phone sharedInstance] startAudioDevice];
            
           [self enableRecordButton];
            [self enableNotes];

            
            [self TimerStart];
        }
        if(Plivo_outgoingcall)
        {
            NSLog(@"Oncall VC - Plivo_outgoingcall");
            [[Phone sharedInstance] configureAudioSession];
            [[Phone sharedInstance] startAudioDevice];
        }
    }
    NSString *islastcalldetail = [Default valueForKey:USER_LAST_CALL_DETAIL];
    int calldetail = [islastcalldetail intValue];
    if (calldetail == 1 && ![extentionCall isEqualToString:@"true"]) {
        self.view_call_details.hidden = false;
    }else{
        self.view_call_details.hidden = true;
    }
    
    
    extraHeader = [[NSDictionary alloc]init];
    extraHeader = [[NSUserDefaults standardUserDefaults]objectForKey:@"extraHeader"];
    NSLog(@"EXTRA HEADER : %@",extraHeader);
    [self configAudioSession:[AVAudioSession sharedInstance]];
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    currHours=00;
    currMinute=00;
    currSeconds=00;
    
    Download_final = 0.0;
    Uownload_final = 0.0;
    arr_speeds = [[NSMutableArray alloc] init];
    arr_low_speeds = [[NSMutableArray alloc] init];
    network_strenth_down = 0;
    // Do any additional setup after loading the view.
    
    selTagArray = [[NSMutableArray alloc]init];

}

- (void) transferButtonNotify:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"transferButtonNotify"])
        NSLog (@"Successfully received the transferButtonNotify notification!");
    [_btnForword setEnabled:false];
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(void)view_design
{
    if([_CallStatus isEqualToString:OUTGOING])
    {
        [Default setValue:@"false" forKey:kISTransferCallForTag];
    }
    
    // tagging
    _txtview_tagging.accessibilityIdentifier = @"Tagging";
    _txtview_tagging.textView.accessibilityIdentifier = @"TaggingTextView";
    _txtview_tagging.borderColor = [[UIColor darkGrayColor] CGColor];
    _txtview_tagging.borderWidth = 1.0;
    _txtview_tagging.cornerRadius = 5;
    _txtview_tagging.textInset = UIEdgeInsetsMake(16, 16, 16, 16);
    _txtview_tagging.backgroundColor = [UIColor whiteColor];
    _txtview_tagging.defaultAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName : [UIFont boldSystemFontOfSize:15] };
   
    _txtview_tagging.symbolAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor] };
    _txtview_tagging.taggedAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSUnderlineStyleAttributeName : [NSNumber numberWithInt:1] };

    //NSArray *arr = [NSArray arrayWithObjects:@"Vivek TW",@"Vivek tw super",@"clay",@"aam",@"mango", nil];


    tagsArray = [Default objectForKey:kTagList];
    _txtview_tagging.symbol = @"@";
   _txtview_tagging.tagableList = [tagsArray valueForKey:@"name"];
   // _txtview_tagging.tagableList = arr;
    
    _txtview_tagging.textView.textColor = [UIColor lightGrayColor];
    [_txtview_tagging.textView setText:@"Add notes..."];
    
    NSString *trasfertmp = [Default valueForKey:kISTransferCallForTag];
    
    
    if ([[Default valueForKey:kISTaggingInPlan] intValue] == 1 && ![extentionCall isEqualToString:@"true"] &&  ![trasfertmp isEqualToString:@"true"]) {
        
        isnotesInPlan = @"true";
        [_txtview_tagging.textView setText:@"Add notes and add tags by typing @"];
        _txtview_tagging.dataSource = self;
    }
    
    tagIdArray = [[NSMutableArray alloc]init];
  //  selTagNameArray = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideTagTable:) name:@"HideTagTable" object:nil];
   
    
    _view_customerDetail.layer.cornerRadius = 5.0;
    _view_customerDetail.clipsToBounds = true;
    _view_customerDetail.layer.borderWidth = 1.0;
    _view_customerDetail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _view_orderDetail.layer.cornerRadius = 5.0;
    _view_orderDetail.clipsToBounds = true;
    _view_orderDetail.layer.borderWidth = 1.0;
    _view_orderDetail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    [self configAudioSession:[AVAudioSession sharedInstance]];
    
    NSLog(@"Oncall VC - View Design");
    // self.lblTimer.text = @"";
   //p NSString *LoginProvider = [Default valueForKey:Login_Provider];
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        NSLog(@"Oncall VC - Login_Plivo");
         [[Phone sharedInstance] setDelegate:self];
        //        self.lblTimer.hidden = true;
        
        
        if(![ _CallStatus isEqualToString:OUTGOING])
        {
            
            
        }
        else
        {
            _transferCall = @"";
            
        }
        
    }
    else
    {
        
        
        
        //        self.lblTimer.hidden = true;
        NSString *timervalue = [Default valueForKey:Timer_Call];
        NSLog(@"OncallVC timervalue : %@",timervalue);
        if([ _CallStatus isEqualToString:OUTGOING])
        {
            _transferCall = @"";
            
            
            if([timervalue isEqualToString:Timer_Start])
            {
                [Default setValue:Timer_Stop forKey:Timer_Call];
                [self.btnHold setEnabled:YES];
                [self.btnMute setEnabled:YES];
                [self transferButtonEnable];
                [self enableRecordButton];
                [self enableNotes];

                [self TimerStart];
                
            }
        }
    }
    
    
    [self gif_call];
    
    if([_CallStatus isEqualToString:OUTGOING]) {
        [self lastcalluserdetail];
        
        //        [self.btnHold setEnabled:NO];
        [self.btnMute setEnabled:NO];
        
    
       
        
    } else {
        //        [self.btnHold setEnabled:YES];
        [self.btnMute setEnabled:YES];
        
    
       
        
        NSLog(@"Oncallvc :  extraHeader : %@",extraHeader);
        
        NSString *strTransferId = [extraHeader valueForKey:@"xphtransferid"];
        NSLog(@"strTransferId : %@",strTransferId);
        if ([strTransferId isEqualToString:@"0"])
        {
            [self transferButtonEnable];
            [self.btnHold setEnabled:YES];
        }
        else
        {
            [self.btnForword setEnabled:NO];
            [self.btnHold setEnabled:NO];
        }
        
        if (![[extraHeader valueForKey:@"xphlastcalluser"] isEqualToString:@""]){
            //                self.view_call_details.hidden = false;
            self.lblLastCallName.text = [extraHeader valueForKey:@"xphlastcalluser"];
            self.lblLastCallStatus.text = [extraHeader valueForKey:@"xphlastcallstatus"];
            self.lblLastCallDate.text = [extraHeader valueForKey:@"xphlastcallcreateddate"];
            
            if ([[extraHeader valueForKey:@"xphlastcallcalltype"] isEqualToString:@"Outgoing"]){
                self.imgLastCallImage.image = [UIImage imageNamed:@"outgoingcomplete"];
            }else{
                self.imgLastCallImage.image = [UIImage imageNamed:@"incoming_Complete"];
            }
        }else if (![[extraHeader valueForKey:@"xphlastcallstatus"] isEqualToString:@""]) {
//            self.view_call_details.hidden = true;
            
          /*
            self.lblNoPreviosCall.text = @"No previous calls on this number";
            self.lblLastCallStatus.text = @"";
            self.lblLastCallDate.text = @"";
            self.lblLastCallName.text = @"";
            self.imgLastCallImage.hidden = true;*/
            
            self.lblLastCallStatus.text = @"";
            self.lblNoPreviosCall.text = @"";
            
            if([extraHeader valueForKey:@"xphlastcallstatus"] != nil || [extraHeader valueForKey:@"xphlastcallstatus"] != [NSNull null])
            {
                self.lblLastCallName.text = [extraHeader valueForKey:@"xphlastcallstatus"];
            }
            else
            {
                self.lblLastCallName.text = @"";
            }
            if([extraHeader valueForKey:@"xphlastcallcreateddate"] != nil || [extraHeader valueForKey:@"xphlastcallcreateddate"] != [NSNull null])
            {
                self.lblLastCallDate.text = [extraHeader valueForKey:@"xphlastcallcreateddate"];
            }
            else
            {
                self.lblLastCallDate.text = @"";
            }
            if([extraHeader valueForKey:@"xphlastcallcalltype"] != nil || [extraHeader valueForKey:@"xphlastcallcalltype"] != [NSNull null])
            {
                if ([[extraHeader valueForKey:@"xphlastcallcalltype"] isEqualToString:@"Outgoing"]){
                    self.imgLastCallImage.image = [UIImage imageNamed:@"outgoingcomplete"];
                }else{
                    self.imgLastCallImage.image = [UIImage imageNamed:@"incoming_Complete"];
                }
            }

            
            
         //   self.view_call_details.hidden = false;
        }
        else
        {
           // self.lblNoPreviosCall.text = @"No previous calls on this number";
            self.lblNoPreviosCall.text = @"New Caller";
            self.lblLastCallStatus.text = @"";
            self.lblLastCallDate.text = @"";
            self.lblLastCallName.text = @"";
            self.imgLastCallImage.hidden = true;
        }
    }
    
    
    [self.btnDialPad setEnabled:YES];
    [self.btnSpeaker setEnabled:YES];
    
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
   
    
    
    if ([extentionCall isEqualToString:@"true"]) {
        
        _lblCallerName.text = _ContactName;
        
        if ([Default boolForKey:kIsNumberMask] == true) {
            if([self validateString:_ContactName withPattern:@"^[0-9]+$"])
            {
                _lblCallerName.text  = [UtilsClass get_masked_number:_ContactName];
            }
        }
        _ContactNumber = [_ContactNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
        if([_ContactNumber isEqualToString:@""]){
            _ContactNumber = @"";
        }else{
            _ContactNumber = [NSString stringWithFormat:@"+%@",_ContactNumber];
        }
        
        if ([Default boolForKey:kIsNumberMask] == true) {
            _lblCallerNumber.text = [UtilsClass get_masked_number:_ContactNumber];
        }
        
        
        _lblCallStatusIncOrOut.text = _CallStatus;
        
       
    }else{
        _lblCallStatusIncOrOut.text = [NSString stringWithFormat:@"%@ via %@",_CallStatus ? _CallStatus :@"",[Default valueForKey:Selected_Department]];
        if([_ContactName isEqualToString:_ContactNumber])
        {
            _lblCallerName.text = _ContactName ? _ContactName :@"";
            if ([Default boolForKey:kIsNumberMask] == true) {
               // if([self validateString:_ContactName withPattern:@"^[0-9]+$"])
                //{
                    _lblCallerName.text  = [UtilsClass get_masked_number:_ContactName];
                //}
            }
            if (![_transferCall isEqualToString:@""]){
                
                if ([Default boolForKey:kIsNumberMask] == true) {
                     self.lblCallerNumber.text = [NSString stringWithFormat:@"Transferred By %@",[UtilsClass get_masked_number:_transferCall]];
                }
                else
                {
                     self.lblCallerNumber.text = [NSString stringWithFormat:@"Transferred By %@",_transferCall];
                }
               
                _lblCallStatusIncOrOut.text = [NSString stringWithFormat:@"%@",_CallStatus];
                _lblCallerNumber.hidden = false;
            }else {
                _lblCallerNumber.text =  @"";
                _lblCallerNumber.hidden = true;
            }
        }
        else
        {
            _lblCallerName.text = _ContactName ? _ContactName :@"";
            if (![_transferCall isEqualToString:@""]){
                
                if ([Default boolForKey:kIsNumberMask] == true) {
                     _lblCallerNumber.text =  [NSString stringWithFormat:@"%@\nTransferred By %@",[UtilsClass get_masked_number:_ContactNumber],_transferCall];
                }
                else
                {
                    _lblCallerNumber.text =  [NSString stringWithFormat:@"%@\nTransferred By %@",_ContactNumber,_transferCall];
                }
                
                
                _lblCallStatusIncOrOut.text = [NSString stringWithFormat:@"%@",_CallStatus];
            }else{
                if ([Default boolForKey:kIsNumberMask] == true) {
                      _lblCallerNumber.text =   [UtilsClass get_masked_number:_ContactNumber] ?  [UtilsClass get_masked_number:_ContactNumber] :@"";
                }
                else
                {
                     _lblCallerNumber.text =   _ContactNumber ? _ContactNumber :@"";
                }
                
               
            }
            _lblCallerNumber.hidden = false;
        }
        
       
    }
   
   /* rem if ([_CallStatus isEqualToString:OUTGOING] ||[extentionCall isEqualToString:@"true"]) {
        
        _btnPostCallSurvey.hidden = true;
        _lblPostCallSurvey.hidden = true;
    }
    else
    {
        _btnPostCallSurvey.hidden = false;
        _lblPostCallSurvey.hidden = false;
    }*/
    
    [_btnCallCUt setEnabled:true];
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"Permission granted");
        }
        else {
            NSLog(@"Permission denied");
            [self micPermiiton];
        }
    }];


}

-(void)contact_save
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    
     NSLog(@"Oncall VC -  viewWillAppear");
    
    NSString *twilioProvider = [Default valueForKey:CallingProvider];
    if([twilioProvider isEqualToString:Login_Twilio])
    {
        [twilio_callkit sharedInstance].delegate = self;
        dispatch_async(dispatch_get_main_queue(), ^{
                       [twilio_callkit sharedInstance].delegate = self;
                   });
         [self twilio_notification];
    }
    
    
    
//    if([_CallStatus isEqualToString:OUTGOING])
//    {
//        NSLog(@"OncallVC TwilioNotification :: Outgoing");
//        [self twilio_notification];
//    }
//    else
//    {
//        if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground)
//        {
//            NSLog(@"OncallVC TwilioNotification :: ForeGround");
//            [self twilio_notification];
//        }
//    }
    
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        [[Phone sharedInstance] setDelegate:self];
    }
    else
    {
        
    }
    
    [self view_design];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(endCallNotification:)
                                                 name:@"EndCallNotification"
                                               object:nil];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}
-(void)twilio_notification
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callDidStartRinging_custom_noti:) name:@"callDidStartRinging_custom" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callDidConnect_custom_noti:) name:@"callDidConnect_custom" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callDidReconnect_custom_noti:) name:@"callDidReconnect_custom" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callDisconnected_custom_noti:) name:@"callDisconnected_custom" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(isReconnectingWithError_custom_noti:) name:@"isReconnectingWithError_custom" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(didFailToConnectWithError_custom_noti:) name:@"didFailToConnectWithError_custom" object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(didDisconnectWithError_custom_noti:) name:@"didDisconnectWithError_custom" object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
   
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"EndCallNotification" object:nil];
    //    [self TimerStop];
    
    // bottom line comment bcz previsous page move
//    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBarHidden = false;
    [NSNotificationCenter.defaultCenter removeObserver:self];
    [NSNotificationCenter.defaultCenter removeObserver:self name:UIDeviceBatteryLevelDidChangeNotification object:nil];
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:NO];
    
}
- (void)viewDidDisappear:(BOOL)animated
{
    //    [self TimerStop];
    self.navigationController.navigationBarHidden = false;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}
-(void)number:(NSString*)number
{
    NSLog(@"_ContactName_func : %@",number);
}

-(void)Incoming_Call_Report:(UIViewController*)vc FromName:(NSString *)FromName FromNumber:(NSString *)FromNumber
{
    currHours=00;
    currMinute=00;
    currSeconds=00;
    
    _lblCallStatusIncOrOut.text = INCOMING;
    if(![FromName isEqualToString:@""])
    {
        _lblCallerName.text = FromName;
        if ([Default boolForKey:kIsNumberMask] == true) {
            if([self validateString:_ContactName withPattern:@"^[0-9]+$"])
            {
                _lblCallerName.text  = [UtilsClass get_masked_number:_ContactName];
            }
        }
    }
    else
    {
        _lblCallerName.text = FromNumber;
        if ([Default boolForKey:kIsNumberMask] == true) {
             _lblCallerName.text = [UtilsClass get_masked_number:FromNumber] ;
        }
        
    }
    //    self.lblCallerNumber.text = _transferCall;
   [self enableRecordButton];
    [self enableNotes];

    [self TimerStart];
}

#pragma mark - MuteCall

- (IBAction)muteCall:(id)sender {
    
    
    if ([[Default valueForKey:CallingProvider] isEqualToString:Login_Linphone]) {
        
        UIImage *img = [sender imageForState:UIControlStateNormal];
           NSData *data1 = UIImagePNGRepresentation(img);
        
        if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"mutedisebal"])])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setImage:[UIImage imageNamed:@"Muteon"] forState:UIControlStateNormal];
                self->_lblMute.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            });
            linphone_core_enable_mic(LC, false);
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setImage:[UIImage imageNamed:@"mutedisebal"] forState:UIControlStateNormal];
                self->_lblMute.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
            });
            linphone_core_enable_mic(LC, true);
        }
    }
    else
    {
    UIImage *img = [sender imageForState:UIControlStateNormal];
    NSData *data1 = UIImagePNGRepresentation(img);
    
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"mutedisebal"])])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setImage:[UIImage imageNamed:@"Muteon"] forState:UIControlStateNormal];
                _lblMute.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            });
            
            
            if(Plivo_Incomingcall)
            {
                [Plivo_Incomingcall mute];
            }
            if(Plivo_outgoingcall)
            {
                [Plivo_outgoingcall mute];
            }
            
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setImage:[UIImage imageNamed:@"mutedisebal"] forState:UIControlStateNormal];
                _lblMute.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
            });
            if(Plivo_Incomingcall)
            {
                [Plivo_Incomingcall unmute];
            }
            if(Plivo_outgoingcall)
            {
                [Plivo_outgoingcall unmute];
            }
        }
    }else {
        if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"mutedisebal"])])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setImage:[UIImage imageNamed:@"Muteon"] forState:UIControlStateNormal];
                _lblMute.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            });
            self.activeCall.muted = true;
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setImage:[UIImage imageNamed:@"mutedisebal"] forState:UIControlStateNormal];
                _lblMute.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
            });
            self.activeCall.muted = false;
        }
    }
    }
}

#pragma mark - HoldCall

- (IBAction)holdCall:(UIButton*)sender {
    
    
    
    NSString *ishold = [Default valueForKey:IS_HOLD];
    int hold = [ishold intValue];
    if (hold == 1) {
        UIImage *img = [sender imageForState:UIControlStateNormal];
        NSData *data1 = UIImagePNGRepresentation(img);
        
        NSString *LoginProvider = [Default valueForKey:CallingProvider];
        
        if ([LoginProvider isEqualToString:Login_Linphone]) {

            
            if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"holddisable"])])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [sender setImage:[UIImage imageNamed:@"holdon"] forState:UIControlStateNormal];
                    self->_lblHold.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                });
                
                LinphoneManager.instance.speakerBeforePause = LinphoneManager.instance.speakerEnabled;
                
                linphone_call_pause((LinphoneCall *)currentCall);
                [self holdUnholdApiCall:@"callhold/freeswitch"];
                
            }else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [sender setImage:[UIImage imageNamed:@"holddisable"] forState:UIControlStateNormal];
                    self->_lblHold.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
                });
                
                [self configAudioSession:[AVAudioSession sharedInstance]];
            
                
                [self holdUnholdApiCall:@"callunhold/freeswitch"];
                
               linphone_call_resume((LinphoneCall *)currentCall);
    
            }

        }
        else
        {
        if([LoginProvider isEqualToString:Login_Twilio])
        {
            NSString *isholdbyapi = [Default valueForKey:IS_HOLD_BY_API];
            int hold = [isholdbyapi intValue];
            if (hold == 1) {
                if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"holddisable"])])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setImage:[UIImage imageNamed:@"holdon"] forState:UIControlStateNormal];
                        self->_lblHold.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                    });
                    
                    [self holdUnholdApiCall:@"callhold/plivo"];
                }else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setImage:[UIImage imageNamed:@"holddisable"] forState:UIControlStateNormal];
                        self->_lblHold.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
                    });
                    
                    [self holdUnholdApiCall:@"callunhold/plivo"];
                }
            }else{
                if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"holddisable"])])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setImage:[UIImage imageNamed:@"holdon"] forState:UIControlStateNormal];
                        self->_lblHold.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                    });
                    
                    self.activeCall.onHold = true;
                }else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender setImage:[UIImage imageNamed:@"holddisable"] forState:UIControlStateNormal];
                        self->_lblHold.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
                    });
                    
                    self.activeCall.onHold = false;
                }
            }
        }else {
            if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"holddisable"])])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [sender setImage:[UIImage imageNamed:@"holdon"] forState:UIControlStateNormal];
                    self->_lblHold.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                });
                
                [self holdUnholdApiCall:@"callhold/plivo"];
            }else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [sender setImage:[UIImage imageNamed:@"holddisable"] forState:UIControlStateNormal];
                    self->_lblHold.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
                });
                
                [self holdUnholdApiCall:@"callunhold/plivo"];
            }
        }
        }
    }else {
        [UtilsClass showAlert:@"Call Hold is not Available in selected plan." title:@"Please Upgrade your plan." contro:self];
    }
}

#pragma mark - SpeakerCall

- (IBAction)speakerCall:(id)sender {
    
      //[self saveNotes];
    
    
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    
    UIImage *img = [sender imageForState:UIControlStateNormal];
    NSData *data1 = UIImagePNGRepresentation(img);
    
    if ([data1  isEqual:UIImagePNGRepresentation([UIImage imageNamed:@"louddisable"])])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [sender setImage:[UIImage imageNamed:@"loudon"] forState:UIControlStateNormal];
            self->_lblSpeaker.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
        });
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        
    }else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [sender setImage:[UIImage imageNamed:@"louddisable"] forState:UIControlStateNormal];
            self->_lblSpeaker.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
        });
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
    }
    [session setActive:YES error:&error];
    
}
#pragma mark - RecordCall

- (IBAction)recordCall:(id)sender
{
    if(recordControl == true)
    {
        if ([self getRecordControlTrueOrFalse] == true) {
            
            if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_pause_on"]]) {
                
                 [self pauseResumeRecording:@"paused"];
            }
            else if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_on"]]) {
                
                [self pauseResumeRecording:@"in-progress"];

            }
        }
        
        else
        {
            [UtilsClass showAlert:@"Please enable option to pause/resume recording from number settings." contro:self];
        }
    }
    else
    {
        [UtilsClass showAlert:@"Call recording start/pause feature is not available in your plan. Please upgrade your plan." contro:self];
    }
     
    
}
-(void)getRecordIsInPlan
{
    if([[Default valueForKey:kISRecordingInPlan] intValue] == 1)
    {
        recordControl = true;
    }
    else
    {
        recordControl = false;
    }
  
    
}
-(BOOL)getRecordControlTrueOrFalse
{
     NSString *selNo;
    BOOL recordStatus = false;
    if ([_CallStatus isEqualToString:OUTGOING]) {
       
        selNo = [Default valueForKey:SELECTEDNO];

    }
    else
    {
        selNo = [extraHeader valueForKey:@"xphtotransferNumber"];
        selNo = [selNo stringByReplacingOccurrencesOfString:@"+" withString:@""];
        selNo = [NSString stringWithFormat:@"+%@",selNo];
    }
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
               
               if(purchase_number.count > 0 )
               {
                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",selNo];
                   NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                   
                   if (results.count>0) {
                       NSDictionary *dic = [results objectAtIndex:0];
                       recordStatus = [[[dic valueForKey:@"number"] valueForKey:@"recordControl"] boolValue];

                   }
                   else
                       recordStatus = false;
                }
    
    return recordStatus;
    
}
-(void)enableRecordButton
{
    [self getRecordIsInPlan];
    
    if (recordControl == true && [self getRecordControlTrueOrFalse]==true) {
        
        [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
        _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
    }
    
    
    
}
-(void)pauseResumeRecording:(NSString *)pauseResumeStr
{

//        if (recordControl == true)
//        {
                           
        NSString *userId = [Default valueForKey:USER_ID];
        NSString *url = [NSString stringWithFormat:@"recordstatus"];
        NSDictionary *passDict = @{@"recordStatus":pauseResumeStr,@"userId":userId};
        NSLog(@"record status dict: %@",passDict);
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(pauseResumeResponse:response:) andDelegate:self];
//    }
//        else
//        {
//                [UtilsClass showAlert:@"Please enable option to pause/resume recording from number settings." contro:self];
//        }
//
               
                   
    
       
}
- (void)pauseResumeResponse:(NSString *)apiAlias response:(NSData *)response {
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"resume pause response %@",response1);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            // [UtilsClass showAlert:@"Done" contro:self];
            
            if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_pause_on"]]) {
                
                [_btnRecord setImage:[UIImage imageNamed:@"record_on"] forState:UIControlStateNormal];
                 _lblRecord.text = @"RECORD";
                 
            }
            else if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_on"]]) {
                
                [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
                _lblRecord.text = @"PAUSE";
                


            }
        }else {
            @try {
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}
/* Release in next build - compliance recording

- (IBAction)recordCall:(id)sender
{

    if (![extentionCall isEqualToString:@"true"]) {
        
        if(recordControl == true)
        {
            if (recordMainToggle == true) {
                
                if ([self getRecordControlTrueOrFalse] == true) {
                    
                    if ([[Default valueForKey:kcallRecordingPermission] boolValue] == true) {
                        
                        NSString *transfertmp = [Default valueForKey:kISTransferCallForTag] ;
                        
                        if([transfertmp isEqualToString:@"true"])
                        {
                            [UtilsClass makeToast:@"You can not change recording settings for parent calls." inView:self.view];
                        }
                        else
                        {
                        //compliance
                        if ([complianceOption isEqualToString:@"C"]) {
                            [UtilsClass showAlert:@"Call Recording is turned off by default as per your settings." contro:self];
                        }
                        else if ([complianceOption isEqualToString:@"B"]) {
                            
                            if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_pause_on"]]) {
                        
                       
                                [self pauseResumeRecording:@"paused"];
                            }
                            else if ([[_btnRecord imageForState:UIControlStateNormal] isEqual:  [UIImage imageNamed:@"record_on"]]) {
                        
                                [self pauseResumeRecording:@"in-progress"];

                            }
                           
                        }
                        else
                        {
                            if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_pause_on"]]) {
                        
                       
                                [self pauseResumeRecording:@"paused"];
                            }
                            else if ([[_btnRecord imageForState:UIControlStateNormal] isEqual:  [UIImage imageNamed:@"record_on"]]) {
                        
                                [self pauseResumeRecording:@"in-progress"];

                            }
                        }
                        
                    }
                }
                    else
                    {
                        //wothout it
                        if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_pause_on"]]) {
                    
                   
                            [self pauseResumeRecording:@"paused"];
                        }
                        else if ([[_btnRecord imageForState:UIControlStateNormal] isEqual:  [UIImage imageNamed:@"record_on"]]) {
                    
                            [self pauseResumeRecording:@"in-progress"];

                        }
                    }
            
                    
                }
        
                else
                    [UtilsClass makeToast:@"Please enable option to pause/resume recording from number settings." inView:self.view];
                }
            else
                [UtilsClass makeToast:@"call Recording is off as per your settings." inView:self.view];
        }
        else
        {
            [UtilsClass makeToast:@"Call recording start/pause feature is not available in your plan. Please upgrade your plan." inView:self.view];
        }

    }
    
}
-(void)getRecordIsInPlan
{
    if([[Default valueForKey:kISRecordingInPlan] intValue] == 1)
    {
        recordControl = true;
    }
    else
    {
        recordControl = false;
    }
  
    
}
-(BOOL)getRecordControlTrueOrFalse
{
     NSString *selNo;
    BOOL recordStatus = false;
    if ([_CallStatus isEqualToString:OUTGOING]) {
       
        selNo = [Default valueForKey:SELECTEDNO];

    }
    else
    {
        selNo = [extraHeader valueForKey:@"xphtotransferNumber"];
        selNo = [selNo stringByReplacingOccurrencesOfString:@"+" withString:@""];
        selNo = [NSString stringWithFormat:@"+%@",selNo];
    }
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
               
               if(purchase_number.count > 0 )
               {
                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",selNo];
                   NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                   
                   if (results.count>0) {
                       NSDictionary *dic = [results objectAtIndex:0];
                       recordStatus = [[[dic valueForKey:@"number"] valueForKey:@"recordControl"] boolValue];
                       recordMainToggle = [[[dic valueForKey:@"number"] valueForKey:@"recordingStatus"] boolValue];

                   }
                   else
                       recordStatus = false;
                }
    
    return recordStatus;
    
}
-(void)enableRecordButton
{
    
    
    [self getRecordIsInPlan];
    
    //1.record in plan 2. record pause/play toggle on 3.record is on for numner
    if (recordControl == true && [self getRecordControlTrueOrFalse]==true && recordMainToggle == true) {
        
        if ([extentionCall isEqualToString:@"true"]) {
            
            [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
            _lblRecord.text = @"PAUSE";
            _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
        }
       else
       {
        //compliance condition
        
        //compliance in plan
        if ([[Default valueForKey:kcallRecordingPermission] boolValue] == true) {
            
            
        //check C is enable or not
        [self getComplianceOption];
            
            NSString *transfertmp = [Default valueForKey:kISTransferCallForTag] ;
            
            if([transfertmp isEqualToString:@"true"])
            {
                if ([complianceOption isEqualToString:@"C"]) {
                    
                    [_btnRecord setImage:[UIImage imageNamed:@"record_off"] forState:UIControlStateNormal];
                   
                    
                    _lblRecord.text = @"RECORD";
                    complianceOption = @"C";
                }
                else if ([complianceOption isEqualToString:@"B"])
                {
                    if ([[extraHeader valueForKey:@"xphrecordcontrol"] boolValue] == true) {
                        [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
                        _lblRecord.text = @"PAUSE";
                        _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                    }
                    else
                    {
                        [_btnRecord setImage:[UIImage imageNamed:@"record_on"] forState:UIControlStateNormal];
                        _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                        
                        _lblRecord.text = @"RECORD";
                    }
                   
                    complianceOption = @"B";
                }
                else
                {
                    if ([[extraHeader valueForKey:@"xphrecordcontrol"] boolValue] == true) {
                        [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
                        _lblRecord.text = @"PAUSE";
                        _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                    }
                    else
                    {
                        [_btnRecord setImage:[UIImage imageNamed:@"record_on"] forState:UIControlStateNormal];
                        _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
                        
                        _lblRecord.text = @"RECORD";
                    }
                    
                    
                    complianceOption = @"A";
                }
                
            }
            else
            {
        if ([complianceOption isEqualToString:@"C"]) {
            
            [UtilsClass makeToast:@"Call Recording is turned off by default as per your settings." inView:self.view];
            
            [_btnRecord setImage:[UIImage imageNamed:@"record_off"] forState:UIControlStateNormal];
           
            
            _lblRecord.text = @"RECORD";
            complianceOption = @"C";
            
            
        }
        else if ([complianceOption isEqualToString:@"B"])
        {
            [UtilsClass makeToast:@"Call Recording is turned off by default as per your settings.If you wish to record this call please tap the Record button" inView:self.view];
            [_btnRecord setImage:[UIImage imageNamed:@"record_on"] forState:UIControlStateNormal];
            _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            
            _lblRecord.text = @"RECORD";
            complianceOption = @"B";


        }
        else
        {
            //default A
            [UtilsClass makeToast:@"Explicit consent to record call is suggested in this country." inView:self.view];
            [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
            _lblRecord.text = @"PAUSE";
            _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            
            complianceOption = @"A";
        }
            }
        }
        else
        {
            [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
            _lblRecord.text = @"PAUSE";
            _lblRecord.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            
        }
       }
        
//        [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
     
    }
    
}
-(void)getComplianceOption
{
    NSString *transfertmp = [Default valueForKey:kISTransferCallForTag] ;
    
    if([transfertmp isEqualToString:@"true"])
    {
        if ([[extraHeader valueForKey:@"xphrecordingpermission"] isEqualToString:@"2"])
            complianceOption = @"C";
        else if ([[extraHeader valueForKey:@"xphrecordingpermission"] isEqualToString:@"1"])
            complianceOption = @"B";
        else if ([[extraHeader valueForKey:@"xphrecordingpermission"] isEqualToString:@"0"])
            complianceOption = @"A";
    }
    else{
    
    NSString *toNumber;
    
    toNumber = _ContactNumber ? _ContactNumber : _ContactName;
    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    txtText.text = @"";
    [txtText insertText:toNumber];
    NSString *shortName = txtText.country.countryCode ? txtText.country.countryCode : @"";
    
    NSLog(@"disable :: %@",[Default valueForKey:kdisableRecordingAutomation]);
    
    if ([[Default valueForKey:kdisableRecordingAutomation] containsObject:shortName] ) {
       // isC = true;
        complianceOption = @"C";
    }else if ([[Default valueForKey:kautoPauseRecordingAutomation] containsObject:shortName])
    {
        complianceOption = @"B";
    }
    else
    {
        complianceOption = @"A";
    }
    
    }
}


-(void)pauseResumeRecording:(NSString *)pauseResumeStr
{

//        if (recordControl == true)
//        {
                           
        NSString *userId = [Default valueForKey:USER_ID];
        NSString *url = [NSString stringWithFormat:@"recordstatus"];
        NSDictionary *passDict = @{@"recordStatus":pauseResumeStr,@"userId":userId};
        NSLog(@"record status dict: %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
        obj = [[WebApiController alloc] init];
     // before  [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(pauseResumeResponse:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(pauseResumeResponse:response:) andDelegate:self];
//    }
//        else
//        {
//                [UtilsClass showAlert:@"Please enable option to pause/resume recording from number settings." contro:self];
//        }
//
               
       
}
- (void)pauseResumeResponse:(NSString *)apiAlias response:(NSData *)response {
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"resume pause response %@",response1);
    
    NSLog(@"Encrypted Response : pause resume : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            // [UtilsClass showAlert:@"Done" contro:self];
            
            if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_pause_on"]]) {
                
                [_btnRecord setImage:[UIImage imageNamed:@"record_on"] forState:UIControlStateNormal];
                 _lblRecord.text = @"RECORD";
                 
            }
            else if ([[_btnRecord imageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"record_on"]]) {
                
                [_btnRecord setImage:[UIImage imageNamed:@"record_pause_on"] forState:UIControlStateNormal];
                _lblRecord.text = @"PAUSE";
                


            }
        }else {
            @try {
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}*/

#pragma mark - notes and integration
-(void)enableNotes
{
    
    [_btn_Notes setEnabled:true];
    
    if ([[Default valueForKey:kISNotesInPlan] intValue] == 1) {
        
        _notesSelected = true;
           
        _lblNotes.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
        [_btn_Notes setEnabled:true];
           
        [_btn_Notes setSelected:_notesSelected];
           
        [_btn_Notes setImage:[UIImage imageNamed:@"notesIcon_orange"] forState:UIControlStateNormal];
        _lblNotes.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
           
        _txtview_tagging.hidden = false;
        
    }
    
}
-(IBAction)notesClick:(id)sender
{
    if ([[Default valueForKey:kISNotesInPlan] intValue] == 1) {
        
    if(_transferSelected)
    {
        _transferSelected = !_transferSelected; /* Toggle */
        [_btnForword setSelected:_transferSelected];
        
        [_btnForword setImage:[UIImage imageNamed:@"forwarddisable"] forState:UIControlStateNormal];
        _lblForword.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
        [transferVC removeFromSuperview ];
    }
    
    if (_dialPadSelected) {
        
        _dialPadSelected = !_dialPadSelected; /* Toggle */
        [_btnDialPad setSelected:_dialPadSelected];
        
        [_btnDialPad setImage:[UIImage imageNamed:@"dialpaddisable"] forState:UIControlStateNormal];
        _lblDialervc.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
               [ivrvc removeFromSuperview ];
    }
    
    
    
    
    _notesSelected = !_notesSelected; /* Toggle */
    [_btn_Notes setSelected:_notesSelected];
    
    if (_btn_Notes.isSelected) {

        [_btn_Notes setImage:[UIImage imageNamed:@"notesIcon_orange"] forState:UIControlStateNormal];
        _lblNotes.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];

        _txtview_tagging.hidden = false;
        
        
    }
    else
    {
        [_btn_Notes setImage:[UIImage imageNamed:@"notesIcon_grey"] forState:UIControlStateNormal];

        _txtview_tagging.hidden = true;
         _lblNotes.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];

    }
    
    }
    else
    {
        [UtilsClass showAlert:@"Call Notes feature is not available in your plan. Please upgrade your plan." contro:self];
    }
    
}
-(void)saveNotes
{
    
  //  if ([[Default valueForKey:kISTaggingInPlan] intValue] == 1) {
    
    if ([[Default valueForKey:kISNotesInPlan] intValue] == 1) {
        
        if (![_txtview_tagging.textView.text isEqualToString:@"Add notes and add tags by typing @"] && ![_txtview_tagging.textView.text isEqualToString:@"Add notes..."]) {
            
            isnoteSaved = YES;
        
        NSString *userId = [Default valueForKey:USER_ID];
        NSString *url = [NSString stringWithFormat:@"savenote/twilio"];
            
        NSLog(@"tags id array : %@",tagIdArray);
        
           
            
        BOOL blindTransferBool;
        BOOL transferBool;
        BOOL extensionBool;

         if ([_CallStatus isEqualToString:OUTGOING]) {
             
             if ([[Default valueForKey:extentionCall] isEqualToString:@"true"])
                 extensionBool = true;
             else
                 extensionBool = false;
             
             if ([[Default valueForKey:IS_BLIND_TRANSFER_FLAG] isEqualToString:@"true"])
                 blindTransferBool = true;
             else
                 blindTransferBool = false;
             
             transferBool = false;
             
        
         }
        else
        {
            if ([[extraHeader valueForKey:@"xphextensioncall"] isEqualToString:@"true"])
                extensionBool = true;
            else
                extensionBool = false;
            
            if ([[extraHeader valueForKey:@"xphblindtransfer"] isEqualToString:@"true"])
                blindTransferBool = true;
            else
                blindTransferBool = false;
            
            //take reverse bool same as ACW
            if ([[extraHeader valueForKey:@"xphcalltransfer"] isEqualToString:@"true"])
                transferBool = false;
            else
                transferBool = true;
            
        }
        
        NSMutableArray *selectedTagArr;
            
            if (tagIdArray.count>0) {
                selectedTagArr = tagIdArray;
            }
            else
            {
                selectedTagArr = [[NSMutableArray alloc]init];
            }
            
        
        NSDictionary *passDict = @{@"description":_txtview_tagging.textView.text,
                                   @"userId":userId,
                                   @"tags":selectedTagArr,
                                   @"calluid":@"",
                                   @"internalCalling":@(extensionBool),
                                   @"isBlindTransfer":@(blindTransferBool),
                                   @"isTransferedCall": @(transferBool)
        };
            
        notesPassDict = [NSDictionary dictionaryWithObjectsAndKeys:@(extensionBool),@"internalCalling",@(blindTransferBool),@"isBlindTransfer",@(transferBool),@"isTransferedCall", nil];
            
            NSLog(@"notes pass dict :: %@",notesPassDict);
           
            
        NSLog(@"notes param dict: %@",passDict);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {

        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(saveNotesResponse:response:) andDelegate:self];
        }
   // }
    }
    
}
- (void)saveNotesResponse:(NSString *)apiAlias response:(NSData *)response {

    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];

    //NSLog(@"api alias %@",apiAlias);

  //  NSLog(@"save notes response %@",response1);
    
    NSLog(@"Encrypted Response : save notes : %@",response1);


    if([apiAlias isEqualToString:Status_Code])
    {
    //[UtilsClass logoutUser:self];
     [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
}
#pragma mark - Post call Survey
-(IBAction)postCallSurveyClick:(id)sender
{
    [self startStopPostCallSurvey];
}
-(void)startStopPostCallSurvey
{
    NSLog(@"extra headers: %@",extraHeader);
    
    if ([[Default valueForKey:kISPostCallSurvey] boolValue] == true) {
       
        BOOL postCallSurvey = [[extraHeader valueForKey:@"xphpostcallsurvey"] boolValue];
           
           if (postCallSurvey == true)
           {
                                  
                NSString *userId = [Default valueForKey:USER_ID];
               NSString *parentId = [Default valueForKey:PARENT_ID];
               NSString *url = [NSString stringWithFormat:@"callSurveyOnClick"];
                   
               NSDictionary *passDict = @{@"parentId":parentId,@"userId":userId};
                   
               NSLog(@"call survey  dict: %@",passDict);
               
               NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                                  options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                    error:nil];
               NSString *jsonString;
               if (! jsonData) {

               } else {
                   jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
               }
               
                   obj = [[WebApiController alloc] init];
               
               
               [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(postCallSurveyResponse:response:) andDelegate:self];
               
               
               
            //before   [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(postCallSurveyResponse:response:) andDelegate:self];
               
           }
            else
               {
                      
                   [UtilsClass showAlert:@"Please enable option of Post call survey from number setting" contro:self];

               }
    }
    else
    {
         [UtilsClass showAlert:@"Post Call survey is not available in your plan, please contact support@callhippo.com to use Post Call survey" contro:self];
    }
   
}
- (void)postCallSurveyResponse:(NSString *)apiAlias response:(NSData *)response {
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
   // NSLog(@"api alias %@",apiAlias);

    //NSLog(@"post call survey response %@",response1);
    
    NSLog(@"Encrypted Response : post call survey : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            // [UtilsClass showAlert:@"Done" contro:self];
            
            
        }else {
            @try {
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}
#pragma mark - DialPadCall

- (IBAction)dialPadCall:(id)sender
{
    
    if (_notesSelected) {

        _notesSelected = !_notesSelected; /* Toggle */
        [_btn_Notes setSelected:_notesSelected];

        [_btn_Notes setImage:[UIImage imageNamed:@"notesIcon_grey"] forState:UIControlStateNormal];
         _lblNotes.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];

        _txtview_tagging.hidden = true;
        
    }
    
    _dialPadSelected = !_dialPadSelected; /* Toggle */
    [_btnDialPad setSelected:_dialPadSelected];
    if (_btnDialPad.isSelected) {
        [_btnDialPad setImage:[UIImage imageNamed:@"dialpadon"] forState:UIControlStateNormal];
        _lblDialervc.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
        ivrvc = [[[NSBundle mainBundle] loadNibNamed:@"IVRVC" owner:self options:nil] objectAtIndex:0];
        ivrvc.frame = CGRectMake(0, 0, self.view.frame.size.width,414 );
        ivrvc.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        ivrvc.delegate = self;
        [self.view addSubview:ivrvc];
    }else{
        [_btnDialPad setImage:[UIImage imageNamed:@"dialpaddisable"] forState:UIControlStateNormal];
        _lblDialervc.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
        [ivrvc removeFromSuperview ];
    }
}


#pragma mark - EndCall

- (IBAction)endCall:(id)sender {
    
     
    self.reviewpopup = false;
    
    NSLog(@"end call click");
    
    
    endcall = @"end";
    NSLog(@"Oncall VC ----> endCall ----> CallingProvider ----> %@",[Default valueForKey:CallingProvider]);
    calling_provider = @"";
    
    if ([[Default valueForKey:CallingProvider] isEqualToString:Login_Linphone]) {

        self.reviewpopup = false;

       NSLog(@"Trushang : Call : End 101");
       
       LinphoneCall *currentcall = linphone_core_get_current_call(LC);
       //linphone_call_terminate((LinphoneCall *)currentCall);
    
    if (linphone_core_is_in_conference(LC) ||                                           // In conference
           (linphone_core_get_conference_size(LC) > 0) // Only one conf
           )
       {
           NSLog(@"Trushang : Call : End 10");
           LinphoneManager.instance.conf = TRUE;
           linphone_core_terminate_conference(LC);
           [LinphoneManager.instance.providerDelegate performEndCallActionWithUUID];
       } else if (currentcall != NULL) {
           NSLog(@"Trushang : Call : End 11");
           linphone_call_terminate(currentcall);
           [LinphoneManager.instance.providerDelegate performEndCallActionWithUUID];
       } else {
           const MSList *calls = linphone_core_get_calls(LC);
           if (bctbx_list_size(calls) == 1) {
               NSLog(@"Trushang : Call : End 12");
               linphone_call_terminate((LinphoneCall *)(calls->data));
               [LinphoneManager.instance.providerDelegate performEndCallActionWithUUID];
           }
       }
       
    
       [LinphoneManager.instance.providerDelegate performEndCallActionWithUUID];
        
        
      /*  dispatch_async(dispatch_get_main_queue(), ^{
            if (self->incOutCall == true){
                self->incOutCall = false;
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                    [self ratting_popup];
                }
            }else{
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                    [self reminderPopup];
                }
            }
        });*/
        
        
    }
    else
    {
    if([[Default valueForKey:CallingProvider] isEqualToString:Login_Twilio])
    {
        if (self.activeCall != nil)
        {
           
            
            [[twilio_callkit sharedInstance] performEndCallActionWithUUID:self.activeCall.uuid];
            if(![_CallStatus isEqualToString:OUTGOING])
            {
                [self TimerStop];
                [activeCall disconnect];
            }
            else
            {
                [self TimerStop];
                [activeCall disconnect];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->incOutCall == true){
                    self->incOutCall = false;
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                        NSLog(@"***********************ratting_popup");
                        [self ratting_popup];
                    }
                }else{
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                        NSLog(@"***********************ratting_popup");
                      //rem  [self reminderPopup];
                    }
                }
            });
//            [twilio_callkit sharedInstance].callKitCompletionCallback(YES);
            NSLog(@"Oncall VC active call found $$$$$$$$$$$$$ ");
        }
        else
        {
            NSLog(@"Oncall VC active call not found $$$$$$$$$$$$$ ");
        }
    }
    else
    {
       // NSString *LoginProvider = [Default valueForKey:Login_Provider];
        NSString *LoginProvider = [Default valueForKey:CallingProvider];

        if([LoginProvider isEqualToString:Login_Plivo])
        {
            //        [[Phone sharedInstance] setDelegate:self];
            //         [[Phone sharedInstance] stopAudioDevice];
            [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
            if([_CallStatus isEqualToString:OUTGOING])
            {
                
                if(Plivo_outgoingcall != nil)
                {
                    
                    NSLog(@"Oncall VC - Plivo_outgoingcall end");
                    [[Phone sharedInstance] stopAudioDevice];
                    [Plivo_outgoingcall hangup];
                    Plivo_outgoingcall = nil;
                    [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self->incOutCall == true){
                            self->incOutCall = false;
                            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                            {
                                [self ratting_popup];
                            }
                        }else{
                            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                            {
                              //rem  [self reminderPopup];
                            }
                        }
                    });
                }
                
            }
            else
            {
                NSLog(@"Trusahng : Oncall VC :  Incomming");
                
                if(Plivo_Incomingcall)
                {
                    NSLog(@"Oncall VC - Plivo_Incomingcall end");
                    [[Phone sharedInstance] stopAudioDevice];
                    [Plivo_Incomingcall hangup];
                    Plivo_Incomingcall = nil;
                    [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self->incOutCall == true){
                            self->incOutCall = false;
                            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                            {
                                [self ratting_popup];
                            }
                        }else{
                            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                            {
                              //rem  [self reminderPopup];
                            }
                        }
                    });
                }
            }
        }
        
    }
    
    //    [self.navigationController popViewControllerAnimated:YES];
    //    [self dismissViewControllerAnimated:YES completion:nil];
    //    [self.navigationController popViewControllerAnimated:YES];
    }
    
    if([_CallStatus isEqualToString:OUTGOING])
    {
      //  dispatch_async(dispatch_get_main_queue(), ^{
            [self TimerStop];
            [self dismissViewControllerAnimated:YES completion:nil];
      //  });
        
    }
    else
    {

        NSLog(@"pop on call in end call");
        
        NSLog(@"%@",self.navigationController.viewControllers);
        
        [self TimerStop];
        [[self navigationController] popViewControllerAnimated:NO];
        
       
      /*  int osnum = [[[UIDevice currentDevice] systemVersion] intValue];
        
        if (osnum > 14) {
            
            [[self navigationController] popViewControllerAnimated:NO];
        }*/
    }
    
}
- (IBAction)forwordCall:(id)sender {
    
    if (_notesSelected) {

        _notesSelected = !_notesSelected; /* Toggle */
        [_btn_Notes setSelected:_notesSelected];

        [_btn_Notes setImage:[UIImage imageNamed:@"notesIcon_grey"] forState:UIControlStateNormal];
         _lblNotes.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];

        _txtview_tagging.hidden = true;
        
    }
    
    
    NSString *istransfer = [Default valueForKey:IS_TRANSFER];
    int trans = [istransfer intValue];
    if (trans == 1) {
        
        [Default setValue:@"true" forKey:IS_TRANSFER_FLAG];
        
        _transferSelected = !_transferSelected; /* Toggle */
        [_btnForword setSelected:_transferSelected];
        if (_btnForword.isSelected) {
            [_btnForword setImage:[UIImage imageNamed:@"forwardon"] forState:UIControlStateNormal];
            
            _lblForword.textColor =  [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f];
            
            
            transferVC = [[[NSBundle mainBundle] loadNibNamed:@"TransferVC" owner:self options:nil] objectAtIndex:0];
            transferVC.delegate = self;
            if (IS_IPHONE_5)
            {
                transferVC.frame = CGRectMake(0, 0, self.view.frame.size.width,320);
            }
            else
            {
                transferVC.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height/2 + 70);
            }
            
            transferVC.layer.borderWidth = 0.0;
            transferVC.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            
            if([_CallStatus isEqualToString: OUTGOING]) {
                transferVC.callStatus = @"Outgoing";
                transferVC.fromNumber = [Default valueForKey:SELECTEDNO];
                transferVC.toNumber = _ContactNumber;
                
                if ([Default boolForKey:kIsNumberMask] == true) {
                           transferVC.toNumber = [UtilsClass get_masked_number:_ContactNumber];
                       }
                       
                
                transferVC.toCalleName = _lblCallerName.text;
            } else {
                transferVC.callStatus = @"Incoming";
            }
            NSLog(@"isSecondTransfer ----- %@",isSecondTransfer);
            transferVC.isSecondTransfer = isSecondTransfer;
            isSecondTransfer = @"true";
            
            [self.view addSubview:transferVC];
        }else{
            [_btnForword setImage:[UIImage imageNamed:@"forwarddisable"] forState:UIControlStateNormal];
            _lblForword.textColor =  [UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1.0f];
            [transferVC removeFromSuperview ];
        }
    }else {
        [UtilsClass showAlert:@"Call Transfer is not Available in selected plan." title:@"Please Upgrade your plan." contro:self];
    }
}

-(void)holdUnholdApiCall:(NSString *)holdplivo {
    
    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *authToken = [Default valueForKey:AUTH_TOKEN];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *plivoAuthToken =  [Default valueForKey:CALLHIPPO_AUTH_TOKEN];
    NSString *plivoAutId = [Default valueForKey:CALLHIPPO_AUTH_ID];
    NSString *url = [NSString stringWithFormat:@"%@",holdplivo];
    NSDictionary *passDict = @{@"authId":plivoAutId,
                               @"authToken":plivoAuthToken,
                               @"callHoldUrl":@"https://s3.amazonaws.com/callhippo_staging/call_hold/15168789827365a4c70c0bc9e741f843ad5c6.mp3",
                               @"calluid":@"1899ca94-518f-413b-9277-96b553f69c5a",
                               @"deviceType":@"iOS",
                               @"userId":userId};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
        obj = [[WebApiController alloc] init];
   
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
   //before [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
}
- (void)login:(NSString *)apiAlias response:(NSData *)response {
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//    NSLog(@"fail hold response %@",response1);
//    NSLog(@"TRUSHANG : STATUSCODE **************31  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : hold unhold  : %@",response1);
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            // [UtilsClass showAlert:@"Done" contro:self];
        }else {
            NSLog(@"fail hold response");
            @try {
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                 //   [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}

-(void)gif_call {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"ig_call" withExtension:@"gif"];
    //    self.imgCallingGIF.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    self.imgCallingGIF.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
}

-(void)TimerStart
{
    //    [NSTimer scheduledTimerWithTimeInterval:1.0
    //                                     target:self
    //                                   selector:@selector(TimerStart:)
    //                                   userInfo:nil
    //                                    repeats:YES];
    
    
    incOutCall = true;
    timer = [[NSTimer alloc] init];
    
        currHours=00;
        currMinute=00;
        currSeconds=00;
   
    
     timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(TimerStart:) userInfo:nil repeats:YES];
    

}

-(void)TimerResert
{
    
}

-(void)TimerStop
{
    //    self.lblTimer.hidden = true;
    
    [timer invalidate];
     
    if (!isnoteSaved) {
        [self saveNotes];
    }
     
}

-(void)TimerStart:(NSTimer *)timer
{
//    NSLog(@"\n \n Timer Call \n \n ");
    self.lblTimer.hidden = false;
    currSeconds+=1;
    if(currSeconds==60) {
        
        currSeconds=0;
        currMinute+=1;
        
        if(currMinute==60) {
            currMinute=0;
            currHours+=1;
        }
    }
    
   
    
    NSString *Trush_timer = [NSString stringWithFormat:@"%@%02d%@%02d%@%02d",@"\n \n  Trush_Time : ",currHours,@":",currMinute,@":",currSeconds];
    NSString *Trush_timer1 = [NSString stringWithFormat:@"%02d%@%02d",currMinute,@":",currSeconds];
    self.lblTimer.text = Trush_timer1;
    
    if([UtilsClass isNetworkAvailable])
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
            {
               //  [self updateStats];
            }
        });
    }
    else
    {
       // [self view_strenth];
    }
    
//    NSLog(@"%@",Trush_timer);
    
}

- (void) endCallNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"EndCallNotification"])
        NSLog (@"Successfully received the test notification!");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self TimerStop];
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    
    
}



- (void)transferComplet{
    [_btnForword setEnabled:false];
    [_btnCallCUt setEnabled:false];
    [_btnHold setEnabled:false];
    //    [_btnMute setEnabled:false];
    [_btnDialPad setEnabled:false];
}

- (void)endCallButtonEnableComplet{
    [_btnCallCUt setEnabled:true];
    //    [_btnHold setEnabled:true];
    //    [_btnMute setEnabled:true];
    //    [_btnDialPad setEnabled:true];
}

- (void)configAudioSession:(AVAudioSession *)audioSession {
    if (@available(iOS 10, *)) {
        // iOS 11 (or newer) ObjC code
        
        NSError *err = nil;
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                             mode:AVAudioSessionModeVoiceChat
                          options:AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowBluetoothA2DP
                            error:&err];
        if (err) {
            err = nil;
        }
        [audioSession setMode:AVAudioSessionModeVoiceChat error:&err];
        if (err) {
            err = nil;
        }
        double sampleRate = 48000.0;
        [audioSession setPreferredSampleRate:sampleRate error:&err];
        if (err) {
            err = nil;
        }
    }
    else
    {
        
    }
    
}


-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)ratting_popup
{
    NSLog(@"Incoming iutgoiung : %@",[Default valueForKey:INCOUTCALL]);
    
    if(![extentionCall isEqualToString:@"true"]){
        NSString *numbVer = [Default valueForKey:IS_ACW_ENABLE];
        int num = [numbVer intValue];
        if (num == 1) {
            
            if (@available(iOS 13, *)) {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UIWindow *alertWindow = [appDelegate window];
                alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ACWViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ACWViewController"];
                vc.strCallieeName = _lblCallerName.text;
                vc.strCallDuration = lblTimer.text;
                vc.strDepartmentFlag = [Default valueForKey:Selected_Department_Flag];
                vc.strDepartmentName = [NSString stringWithFormat:@"Via %@",[Default valueForKey:Selected_Department]];
                vc.selectedTag = selTagArray;
                
                [self createNotesDicToPass];
                
                vc.notesPassDict = notesPassDict;
                vc.notesInPlan = isnotesInPlan;

                
                if([_CallStatus isEqualToString: OUTGOING])
                {
                    vc.strFromNumber = [Default valueForKey:SELECTEDNO];
                    vc.strToNumber = _ContactNumber;
                    vc.strCallStatus = @"Outgoing";
                    vc.originalToNumTopass = _ContactNumber;
                    
                    if ([Default boolForKey:kIsNumberMask] == true) {
                                              vc.strToNumber = [UtilsClass get_masked_number:_ContactNumber];
                                          }
                    
                }
                else
                {
                    
                    NSString *strFromNumber = [extraHeader valueForKey:@"xphfromnumber"];
                    NSString *strToNumber = [extraHeader valueForKey:@"xphtotransferNumber"];
                    
                    vc.strFromNumber = strFromNumber;
                    vc.strToNumber = strToNumber;
                    vc.strCallStatus = @"Incoming";
                    
                    vc.originalToNumTopass = strToNumber;
                    
                    if ([Default boolForKey:kIsNumberMask] == true) {
                                              vc.strToNumber = [UtilsClass get_masked_number:strToNumber];
                                          }
                }
                //        vc.strFromNumber = [Default valueForKey:SELECTED_NUMBER_VERIFY];
                //        vc.strToNumber = _ContactNumber;
                
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                } else {
                    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                }
                
                [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            } else
            {
                UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                alertWindow.rootViewController = [[UIViewController alloc] init];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ACWViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ACWViewController"];
                vc.strCallieeName = _lblCallerName.text;
                vc.strCallDuration = lblTimer.text;
                vc.strDepartmentFlag = [Default valueForKey:Selected_Department_Flag];
                vc.strDepartmentName = [NSString stringWithFormat:@"Via %@",[Default valueForKey:Selected_Department]];;
                vc.selectedTag = selTagArray;
                
                [self createNotesDicToPass];

                
                vc.notesPassDict = notesPassDict;
                vc.notesInPlan = isnotesInPlan;
                
                if([_CallStatus isEqualToString: OUTGOING])
                {
                    vc.strFromNumber = [Default valueForKey:SELECTEDNO];
                    vc.strToNumber = _ContactNumber;
                    vc.strCallStatus = @"Outgoing";
                    vc.originalToNumTopass = _ContactNumber;
                    
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        vc.strToNumber = [UtilsClass get_masked_number:  _ContactNumber];
                    }
                    
                }
                else
                {
                    
                    NSString *strFromNumber = [extraHeader valueForKey:@"xphfromnumber"];
                    NSString *strToNumber = [extraHeader valueForKey:@"xphtotransferNumber"];
                    
                    vc.strFromNumber = strFromNumber;
                    vc.strToNumber = strToNumber;
                    vc.strCallStatus = @"Incoming";
                    
                    vc.originalToNumTopass = strToNumber;
                    
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        vc.strToNumber = [UtilsClass get_masked_number:  strToNumber];
                    }
                    
                    
                }
                //        vc.strFromNumber = [Default valueForKey:SELECTED_NUMBER_VERIFY];
                //        vc.strToNumber = _ContactNumber;
                
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                } else {
                    vc.modalPresentationStyle = UIModalPresentationPopover;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                }
                
                [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            }
            
            NSLog(@"selected tag >> %@",selTagArray);
            NSLog(@"notes pass dict >> %@",notesPassDict);
        }
        else
        {
            if ([[Default valueForKey:INCOUTCALL] isEqualToString:@"false"])
            {
                
                if (@available(iOS 13, *))
                {
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    UIWindow *alertWindow = [appDelegate window];
                    alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    [alertWindow makeKeyAndVisible];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    PopUpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PopUpViewController"];
                    vc.incOutNumb = _ContactNumber;
                    vc.originalToNum = _ContactNumber;
                    vc.strDisplayName = _lblCallerName.text;
                    
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        vc.incOutNumb = [UtilsClass get_masked_number: _ContactNumber];
                    }
                    
                    if([_CallStatus isEqualToString: OUTGOING])
                    {
                        vc.strIncOutCall = @"Outgoing";
                    }
                    else
                    {
                        vc.strIncOutCall = @"Incoming";
                    }
                    if (IS_IPAD) {
                        vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                        vc.modalPresentationCapturesStatusBarAppearance = YES;
                    } else {
                        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                        vc.modalPresentationCapturesStatusBarAppearance = YES;
                    }
                    [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
                } else
                {
                    
                    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                    alertWindow.rootViewController = [[UIViewController alloc] init];
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    [alertWindow makeKeyAndVisible];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    PopUpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PopUpViewController"];
                    vc.incOutNumb = _ContactNumber;
                    vc.originalToNum = _ContactNumber;
                    
                    vc.strDisplayName = _lblCallerName.text;
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        vc.incOutNumb = [UtilsClass get_masked_number: _ContactNumber];
                    }
                    if([_CallStatus isEqualToString: OUTGOING])
                    {
                        vc.strIncOutCall = @"Outgoing";
                    }
                    else
                    {
                        vc.strIncOutCall = @"Incoming";
                    }
                    if (IS_IPAD) {
                        vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                        vc.modalPresentationCapturesStatusBarAppearance = YES;
                    } else {
                        vc.modalPresentationStyle = UIModalPresentationPopover;
                        vc.modalPresentationCapturesStatusBarAppearance = YES;
                    }
                    [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
                }
                
            }else{
                
              //rem  [self reminderPopup];
            }
        }
    }
    
    
    
}


-(void)reminderPopup {
    if(![extentionCall isEqualToString:@"true"]){
        NSString *numbVer = [Default valueForKey:IS_CALL_REMINDER];
        int num = [numbVer intValue];
        if (num == 1) {
            
            if (@available(iOS 13, *)) {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UIWindow *alertWindow = [appDelegate window];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                CallReminderPopUp *vc = [storyboard instantiateViewControllerWithIdentifier:@"CallReminderPopUp"];
                vc.incOutNumb = _ContactNumber;
                vc.strDisplayName = _lblCallerName.text;
                vc.originalToNum = _ContactNumber;
                
                if ([Default boolForKey:kIsNumberMask] == true) {
                    vc.incOutNumb = [UtilsClass get_masked_number: _ContactNumber];
                }
                if([_CallStatus isEqualToString: OUTGOING])
                {
                    vc.strIncOutCall = @"Outgoing";
                }
                else
                {
                    vc.strIncOutCall = @"Incoming";
                }
                
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                } else {
                    [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                }
                
                [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            } else {
                UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                alertWindow.rootViewController = [[UIViewController alloc] init];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                CallReminderPopUp *vc = [storyboard instantiateViewControllerWithIdentifier:@"CallReminderPopUp"];
                vc.incOutNumb = _ContactNumber;
                vc.strDisplayName = _lblCallerName.text;
                vc.originalToNum = _ContactNumber;
                if ([Default boolForKey:kIsNumberMask] == true) {
                    vc.incOutNumb = [UtilsClass get_masked_number: _ContactNumber];
                }
                if([_CallStatus isEqualToString: OUTGOING])
                {
                    vc.strIncOutCall = @"Outgoing";
                }
                else
                {
                    vc.strIncOutCall = @"Incoming";
                }
                
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                } else {
                    [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                    vc.modalPresentationStyle = UIModalPresentationPopover;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                }
                
                [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            }
        }
    }
}


// Plivo
#pragma mark - Plivo Delegate

/**

 */

- (void)onOutgoingCallInvalid:(PlivoOutgoing *)call
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        NSLog(@"Oncall VC - On outgoing call invalid");
        Plivo_outgoingcall = call;
        if([_CallStatus isEqualToString:OUTGOING])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self TimerStop];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            
            
        }
        else
        {
           NSLog(@"pop on call in outgoing invalid");
            [self TimerStop];
            [[self navigationController] popViewControllerAnimated:NO];
            
            
        }
        [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
    }
}

/**
 * onOutgoingCallrejected delegate implementation.
 */
- (void)onOutgoingCallRejected:(PlivoOutgoing *)call
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        NSLog(@"Oncall VC - Outgoing call Rejected : UUID IS %@",[CallKitInstance sharedInstance].callUUID);
        
        NSLog(@"Hangup call : %@",endcall);
        
        if([endcall isEqualToString:@""])
        {
            Plivo_outgoingcall = call;
            [Plivo_outgoingcall hangup];
            if([_CallStatus isEqualToString:OUTGOING])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self TimerStop];
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
                
                
            }
            else
            {
                 NSLog(@"pop on call in on outgoing rejected");
                [self TimerStop];
                [[self navigationController] popViewControllerAnimated:NO];
                
                
            }
            [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->incOutCall == true){
                    self->incOutCall = false;
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                        [self ratting_popup];
                    }
                }else{
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                       //rem [self reminderPopup];
                    }
                }
            });
        }
        
    }
    
}

/**
 * onOutgoingCallRinging delegate implementation.
 */
- (void)onOutgoingCallRinging:(PlivoOutgoing *)call
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        NSLog(@"Oncall VC - On outgoing call ringing");
        Plivo_outgoingcall = call;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self configAudioSession:[AVAudioSession sharedInstance]];
            [[Phone sharedInstance] configureAudioSession];
            [[Phone sharedInstance] startAudioDevice];
        });
        

    }
}

- (void)onCalling:(PlivoOutgoing *)call
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        NSLog(@"Oncall VC - On Caling");
        Plivo_outgoingcall = call;
    }
}

/**
 * onOutgoingCallHangup delegate implementation.
 */
- (void)onOutgoingCallHangup:(PlivoOutgoing *)call
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"Oncall VC - Hangup call : UUID IS %@",[CallKitInstance sharedInstance].callUUID);
        Plivo_outgoingcall = call;
        [Plivo_outgoingcall hangup];
        if([_CallStatus isEqualToString:OUTGOING])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self TimerStop];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            
        }
        else
        {
            NSLog(@"pop on call in outgoing hangup");
            [self TimerStop];
            [[self navigationController] popViewControllerAnimated:NO];
            
            
        }
        [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self->incOutCall == true){
                self->incOutCall = false;
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                    [self ratting_popup];
                }
            }else{
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                  //rem  [self reminderPopup];
                }
            }
        });
    }
}

/**
 * onOutgoingCallAnswered delegate implementation
 */
- (void)onOutgoingCallAnswered:(PlivoOutgoing *)call
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"Oncall VC - On outgoing call answered");
        
       
        Plivo_outgoingcall = call;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.btnHold setEnabled:YES];
            [self.btnMute setEnabled:YES];
            [self transferButtonEnable];
            [self enableRecordButton];
            [self enableNotes];
            [self TimerStart];
        });
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//                   [[[CallKitInstance sharedInstance] callKitProvider] reportOutgoingCallWithUUID:[[CallKitInstance sharedInstance]callUUID] connectedAtDate:[NSDate date]];
//               });
    }
}
//- (void)provider:(CXProvider *)provider didActivateAudioSession:(AVAudioSession *)audioSession
//{
//    NSString *callingprovider = [Default valueForKey:CallingProvider];
//    if([callingprovider isEqualToString:Login_Plivo])
//    {
//        NSLog(@"did activate audio");
//        [[Phone sharedInstance] startAudioDevice];
//    }
//}

-(void)handleInterruption:(NSNotification *)notification
{
    
    NSLog(@"interuption");
    
    if(self.Plivo_Incomingcall != nil || self.Plivo_outgoingcall != nil)
    {
        
//        NSDictionary *userInfo = notification.userInfo;
//        int interruptionTypeRawValue = (int) [userInfo valueForKey:AVAudioSessionInterruptionTypeKey];
//
//        AVAudioSessionInterruptionType *interruptionType = [AVAudioSession ]
        
        if ([[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] isEqualToNumber:[NSNumber numberWithInt:AVAudioSessionInterruptionTypeBegan]]) {
            NSLog(@"InterruptionTypeBegan");
            
            [[Phone sharedInstance] stopAudioDevice];

            
        } else {
            NSLog(@"InterruptionTypeEnded");
            
            
            
            [[AVAudioSession sharedInstance] setActive:true error:nil];
            [[Phone sharedInstance] startAudioDevice];
            
            if (Plivo_Incomingcall != nil && Plivo_Incomingcall.state == Ongoing) {
                [Plivo_Incomingcall unhold];
            }
            if (Plivo_outgoingcall != nil && Plivo_outgoingcall.state == Ongoing) {
                          [Plivo_outgoingcall unhold];
            }
            
            
        }
        
    }
}
-(void)handleMediaReset:(NSNotification *)notification
{
    NSLog(@"media reset");

    
    [[Phone sharedInstance] configureAudioSession];
    [[Phone sharedInstance] startAudioDevice];
}

-(void)addObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleInterruption:) name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMediaReset:) name:AVAudioSessionMediaServicesWereResetNotification object:[AVAudioSession sharedInstance]];
    

}
/**
 * onIncomingCallRejected implementation.
 */
- (void)onIncomingCallRejected:(PlivoIncoming *)incoming
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"Oncall VC - Incoming call Rejected");
        NSLog(@"Hangup call : %@",endcall);
        if([endcall isEqualToString:@""])
        {
           //pp--- Plivo_Incomingcall = nil;
            Plivo_Incomingcall = incoming;
            [Plivo_Incomingcall hangup];
            Plivo_Incomingcall = nil;
            if([_CallStatus isEqualToString:OUTGOING])
            {
                NSLog(@"Oncall VC - Incoming call Rejected OUTGOING");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self TimerStop];
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
                
                
            }
            else
            {
                NSLog(@"Oncall VC - Incoming call Rejected INCOMING");
                
                NSLog(@"pop on call in incoming rejected");
                [self TimerStop];
                [[self navigationController] popViewControllerAnimated:NO];
                
                
                
            }
            [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->incOutCall == true){
                    self->incOutCall = false;
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                        [self ratting_popup];
                    }
                    else
                    {
                        NSString *numbVer = [Default valueForKey:IS_ACW_ENABLE];
                        int num = [numbVer intValue];
                        if (num == 1)
                        {
                            [self endWorkApi];
                        }
                    }
                }else{
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                       //rem [self reminderPopup];
                    }
                }
            });
        }
        
    }
}

/**
 * onIncomingCallHangup delegate implementation.
 */
- (void)onIncomingCallHangup:(PlivoIncoming *)incoming
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"Oncall VC - Incoming call Hangup");
       //pp--- Plivo_Incomingcall = nil;
        Plivo_Incomingcall = incoming;
        [Plivo_Incomingcall hangup];
        Plivo_Incomingcall = nil;
        if([_CallStatus isEqualToString:OUTGOING])
        {
            NSLog(@"Oncall VC - Incoming call Hangup OUTGOING  Plivo");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self TimerStop];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            
        }
        else
        {
            NSLog(@"Oncall VC - Incoming call Hangup Incoming");
            dispatch_async(dispatch_get_main_queue(), ^{
                
                 NSLog(@"pop on call in on incoming hangup");
                [self TimerStop];
                [[self navigationController] popViewControllerAnimated:NO];
            });
        }
        [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self->incOutCall == true){
                self->incOutCall = false;
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                    [self ratting_popup];
                }
                else
                {
                    NSString *numbVer = [Default valueForKey:IS_ACW_ENABLE];
                    int num = [numbVer intValue];
                    if (num == 1)
                    {
                        [self endWorkApi];
                    }
                }
            }else{
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                   //rem [self reminderPopup];
                }
            }
        });
    }
}


/**
 * onIncomingCall delegate implementation
 */

- (void)onIncomingCall:(PlivoIncoming *)incoming
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        NSLog(@"Oncall VC - onIncomingCall");
        Plivo_Incomingcall = incoming;
        switch ([[AVAudioSession sharedInstance] recordPermission]) {
                
            case AVAudioSessionRecordPermissionGranted:
            {
                break;
            }
            case AVAudioSessionRecordPermissionDenied:
                break;
            case AVAudioSessionRecordPermissionUndetermined:
                printf("No permition");
                break;
            default:
                break;
        }
    }
}
//2.1.17
-(void)onIncomingCallInvalid:(PlivoIncoming *)incoming
{
    NSString *LoginProvider = [Default valueForKey:CallingProvider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        NSLog(@"Oncall VC - On incoming call invalid");
        Plivo_Incomingcall = incoming;
        
            [self TimerStop];
            [[self navigationController] popViewControllerAnimated:NO];
            
        Plivo_Incomingcall = nil;

        
        [[CallKitInstance sharedInstance] performEndCallActionWithUUID:[CallKitInstance sharedInstance].callUUID];
    }
}
- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call{
    
    if(call.hasEnded)
    {
        //NSLog(@"CXCallState : End %lu",(unsigned long)self.calls_uuids.count);
        
        NSLog(@"call ended ended ended ");
        
    }
}
//-(void)onIncomingCallAnswered:(PlivoIncoming *)incoming
//{
//    Plivo_Incomingcall = incoming;
//}
-(void)view_strenth
{
    if([isnetworkstrenth isEqualToString:@"1"])
    {
        
        self->_view_network_strenth.hidden = false;
        self->isnetworkstrenth = @"0";
        NSLog(@"view_network_strenth : show");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self->isnetworkstrenth = @"1";
            self->_view_network_strenth.hidden = true;
            NSLog(@"view_network_strenth : hide");
        });
        
    }
    
}

-(void)upload_strenth
{
    
    network_strenth_down = network_strenth_down + 1;
    if(network_strenth_down == 2)
    {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSLog(@"Trushang :  Speed Update : %@",[NSDate date]);
            self->network_strenth_down = 1;
            int myInt = (int) self->Download_final;
            NSString *speed = [NSString stringWithFormat:@"%d",myInt];
            [self->arr_speeds addObject:speed];
        });
        
    }
}


- (IBAction)btn_network_strenth_click:(UIButton *)sender
{
    self->isnetworkstrenth_click = @"0";
    self->isnetworkstrenth = @"0";
    self->_view_network_strenth.hidden = true;
    
}

-(void)lastcalluserdetail
{
    
    //    https://dialer.callhippo.com/api/v2/lastcalluserdetail/:user/:phonenumber
    
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"lastcalluserdetail/%@/%@",userId,_ContactNumber];
    NSLog(@"URL : %@%@",SERVERNAME,aStrUrl);
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(lastcalluserdetail:response:) andDelegate:self];
}
- (void)lastcalluserdetail:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"getlastcalluserdetail : %@",response1);
   // self.view_call_details.hidden = true;
    /*callStatus = Cancelled;
     callType = Outgoing;
     createddate = "Oct 2, 2019";
     fullName = Chetan;*/
  //  NSLog(@"TRUSHANG : STATUSCODE **************32  : %@",apiAlias);
    NSLog(@"Encrypted Response : last call detail : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            NSLog(@"responsse last call detail ---------------- %@",response1);
            
            if([response1 valueForKey:@"data"] != nil)
            {
                
                if([response1 valueForKey:@"data"] != [NSNull null])
                {
                    if ([[response1 valueForKey:@"data"][@"fullName"] isEqualToString:@""]){
                        
                        
                      /*  self.lblNoPreviosCall.text = @"No previous calls on this number";
                        self.lblLastCallStatus.text = @"";
                        self.lblLastCallDate.text = @"";
                        self.lblLastCallName.text = @"";
                        self.imgLastCallImage.hidden = true;
                     //   self.view_call_details.hidden = false;*/
                        
                        self.lblLastCallStatus.text = @"";
                        self.lblNoPreviosCall.text = @"";
                        
                        if([response1 valueForKey:@"data"][@"callStatus"] != nil || [response1 valueForKey:@"data"][@"callStatus"] != [NSNull null])
                        {
                            self.lblLastCallName.text = [response1 valueForKey:@"data"][@"callStatus"];
                        }
                        else
                        {
                            self.lblLastCallName.text = @"";
                        }
                        if([response1 valueForKey:@"data"][@"createddate"] != nil || [response1 valueForKey:@"data"][@"createddate"] != [NSNull null])
                        {
                            self.lblLastCallDate.text = [response1 valueForKey:@"data"][@"createddate"];
                        }
                        else
                        {
                            self.lblLastCallDate.text = @"";
                        }
                        if([response1 valueForKey:@"data"][@"callType"] != nil || [response1 valueForKey:@"data"][@"callType"] != [NSNull null])
                        {
                            if ([[response1 valueForKey:@"data"][@"callType"] isEqualToString:@"Outgoing"]){
                                self.imgLastCallImage.image = [UIImage imageNamed:@"outgoingcomplete"];
                            }else{
                                self.imgLastCallImage.image = [UIImage imageNamed:@"incoming_Complete"];
                            }
                        }
                    }
                    else{
                        self.imgLastCallImage.hidden = false;
                         self.lblNoPreviosCall.text = @"";
                     //   self.view_call_details.hidden = false;
                        if([response1 valueForKey:@"data"][@"fullName"] != nil || [response1 valueForKey:@"data"][@"fullName"] != [NSNull null])
                        {
                            self.lblLastCallName.text = [response1 valueForKey:@"data"][@"fullName"];
                        }
                        else
                        {
                            self.lblLastCallName.text = @"";
                        }
                        if([response1 valueForKey:@"data"][@"fullName"] != nil || [response1 valueForKey:@"data"][@"fullName"] != [NSNull null])
                        {
                            self.lblLastCallName.text = [response1 valueForKey:@"data"][@"fullName"];
                        }
                        else
                        {
                            self.lblLastCallName.text = @"";
                        }
                        if([response1 valueForKey:@"data"][@"callStatus"] != nil || [response1 valueForKey:@"data"][@"callStatus"] != [NSNull null])
                        {
                            self.lblLastCallStatus.text = [response1 valueForKey:@"data"][@"callStatus"];
                        }
                        else
                        {
                            self.lblLastCallStatus.text = @"";
                        }
                        if([response1 valueForKey:@"data"][@"createddate"] != nil || [response1 valueForKey:@"data"][@"createddate"] != [NSNull null])
                        {
                            self.lblLastCallDate.text = [response1 valueForKey:@"data"][@"createddate"];
                        }
                        else
                        {
                            self.lblLastCallDate.text = @"";
                        }
                        if([response1 valueForKey:@"data"][@"callType"] != nil || [response1 valueForKey:@"data"][@"callType"] != [NSNull null])
                        {
                            if ([[response1 valueForKey:@"data"][@"callType"] isEqualToString:@"Outgoing"]){
                                self.imgLastCallImage.image = [UIImage imageNamed:@"outgoingcomplete"];
                            }else{
                                self.imgLastCallImage.image = [UIImage imageNamed:@"incoming_Complete"];
                            }
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                   // self.lblNoPreviosCall.text = @"No previous calls on this number";
                    self.lblNoPreviosCall.text = @"New Caller";
                    self.lblLastCallStatus.text = @"";
                    self.lblLastCallDate.text = @"";
                    self.lblLastCallName.text = @"";
                    self.imgLastCallImage.hidden = true;
                  //  self.view_call_details.hidden = false;
                }
            }
            else
            {
               // self.lblNoPreviosCall.text = @"No previous calls on this number";
                self.lblNoPreviosCall.text = @"New Caller";
                self.lblLastCallStatus.text = @"";
                self.lblLastCallDate.text = @"";
                self.lblLastCallName.text = @"";
                self.imgLastCallImage.hidden = true;
              //  self.view_call_details.hidden = false;
            }
            
            
            
        }
        else {
           // self.lblNoPreviosCall.text = @"No previous calls on this number";
            self.lblNoPreviosCall.text = @"New Caller";
            self.lblLastCallStatus.text = @"";
            self.lblLastCallDate.text = @"";
            self.lblLastCallName.text = @"";
            self.imgLastCallImage.hidden = true;
          //  self.view_call_details.hidden = false;
        }
        
    }
    
}
- (IBAction)btn_close_details:(UIButton *)sender
{
    _view_call_details.hidden  = true;
    
    if([_CallStatus isEqualToString: OUTGOING]) {
        [FIRAnalytics setUserPropertyString:@"" forName:@"close_lastcalldetail_outgoing"];
    }else {
        [FIRAnalytics setUserPropertyString:@"" forName:@"close_lastcalldetail_incoming"];
    }
}



- (void)endWorkApi {
    
    //    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    if(![extentionCall isEqualToString:@"true"]){
        
        NSString *userId = [Default valueForKey:USER_ID];
        NSString *parentId = [Default valueForKey:PARENT_ID];
        
        
        //    NSString *newString = [someString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSLog(@"Test 1 %@",userId);
        NSLog(@"Test 2 %@",parentId);
        
        NSDictionary *passDict;
        
        NSString *strFromNumber;
        NSString *strToNumber;
        
        if ([_CallStatus isEqualToString:OUTGOING]) {
            
            strToNumber = _ContactNumber;
            strFromNumber = [Default valueForKey:SELECTEDNO] ? [Default valueForKey:SELECTEDNO] : @"";
            if ([Default boolForKey:kIsNumberMask] == true) {
                strToNumber = [UtilsClass get_masked_number: _ContactNumber];
            }
        }
        else
        {
            NSDictionary *extraHeader = [[NSDictionary alloc]init];
                   extraHeader = [[NSUserDefaults standardUserDefaults]objectForKey:@"extraHeader"];
                   
            strFromNumber = [extraHeader valueForKey:@"xphfromnumber"];
            strToNumber = [extraHeader valueForKey:@"xphtotransferNumber"];
        }
        
        
       
        
        
        
        BOOL boolValue = [[extraHeader valueForKey:@"xphcalltransfer"] boolValue];
        
        //@try {
            
            passDict = @{
                @"userId":userId,
                @"parentId":parentId,
                @"afterCallWorkNote" : @"",
                @"acwDuration" : @"00:00",
                @"CallratingStar":@"4.5",
                @"to" : strToNumber,
                @"from":strFromNumber,
                @"isTransferedCall":@(!boolValue)
            };
       /* }
        @catch (NSException *exception) {
        }*/
        
        NSLog(@"Login Dic ************ : %@",passDict);
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
            
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        
        [obj callAPI_POST_RAW:@"endaftercallwork" andParams:jsonString SuccessCallback:@selector(endWorkApi:response:) andDelegate:self];
    }
}

- (void)endWorkApi:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//    NSLog(@"Login_response : %@",response1);
//    NSLog(@"TRUSHANG : STATUSCODE **************11  : %@",apiAlias);
    
    //    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    //    localNotification.alertBody = @"Kill State resoponse ";
    //    localNotification.soundName = UILocalNotificationDefaultSoundName;
    //    localNotification.applicationIconBadgeNumber = 0;
    //    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    NSLog(@"Encrypted Response : end ACW : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
    
}

- (void)api_network_strenth_upload
{
    
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *from = @"";
    NSString *to = @"";
    if([_CallStatus isEqualToString: OUTGOING]) {
        to = _ContactNumber;
        from = [Default valueForKey:SELECTEDNO] ? [Default valueForKey:SELECTEDNO] : @"";
        if ([Default boolForKey:kIsNumberMask] == true) {
            to = [UtilsClass get_masked_number: _ContactNumber];
        }
    }else
    {
        NSString *strFromNumber = [extraHeader valueForKey:@"xphfromnumber"];
        NSString *strToNumber = [extraHeader valueForKey:@"xphtotransferNumber"];
        
        from = strFromNumber;
        to = strToNumber;
    }
    
    
    NSString *userId = [Default valueForKey:USER_ID] ? [Default valueForKey:USER_ID] : @"";
    //NSString *provider = [Default valueForKey:Login_Provider] ? [Default valueForKey:Login_Provider] : @"";
    NSString *provider = [Default valueForKey:CallingProvider] ? [Default valueForKey:CallingProvider] : @"";
    NSString *networkStrength = [arr_speeds componentsJoinedByString:@","];
    NSString *networkStrength_low = [arr_low_speeds componentsJoinedByString:@","];
    NSDictionary *passDict = @{
        @"from":from,
        @"to":to,
        @"user":userId,
        @"provider":provider,
        @"value":networkStrength,
        @"lowValue":networkStrength_low
    };
    
    NSLog(@"api_network_strenth_upload URL : %@%@",SERVERNAME,Upload_speed_count);
    NSLog(@"api_network_strenth_upload Dic : %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    
    [obj callAPI_POST_RAW:Upload_speed_count andParams:jsonString SuccessCallback:@selector(Netwokrk_strenth_upload:response:) andDelegate:self];
}

- (void)Netwokrk_strenth_upload:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
   // NSLog(@"TRUSHANG : STATUSCODE **************24  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : network strength : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"api_network_strenth_upload : response : %@",response1);
    }
}

//Twilio SDK

-(void)twilio_calldelegate:(TVOCallInvite *)callInvite uuid:(NSUUID *)uuid
{
    NSLog(@"twilio_calldelegate : OncallVC");
    
    //    TVOAcceptOptions *acceptOptions = [TVOAcceptOptions optionsWithCallInvite:callInvite block:^(TVOAcceptOptionsBuilder *builder) {
    //      builder.uuid = uuid;
    //    }];
    //
    //    TVOCall *call = [callInvite acceptWithOptions:acceptOptions delegate:self];
    //
    //            if (!call) {
    //
    //            } else {
    //    //                    self.callKitCompletionCallback = completionHandler;
    //    //                    self.activeCall = call;
    //                NSLog(@"twilio_calldelegate : call : call :call : OncallVC");
    //            }
}

#pragma mark - Custom TVOCallDelegate

- (void)callDidStartRinging_custom:(TVOCall *)call
{
    NSLog(@"callDidStartRinging : OncallVc");
    activeCall = call;
}


- (void)callDidConnect_custom:(TVOCall *)call
{
    NSLog(@"callDidConnect : OncallVc Incoming call connected");
    activeCall = call;
    self.lblTimer.hidden = false;
    [self.btnHold setEnabled:YES];
    [self.btnMute setEnabled:YES];
    [self transferButtonEnable];
    [self enableRecordButton];
    [self enableNotes];
    [self TimerStart];
}
- (void)callDidReconnect_custom:(TVOCall *)call
{
    NSLog(@"Call reconnected : OncallVC");
    activeCall = call;
}


- (void)callDisconnected_custom:(TVOCall *)call {
    if ([call isEqual:self.activeCall]) {
        activeCall = nil;
    }
    if([_CallStatus isEqualToString:OUTGOING])
    {
        NSLog(@"Oncall VC : callDisconnected_custom : Outgoing call Hangup OUTGOING Twlilo");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self TimerStop];
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    }
    else
    {
        NSLog(@"Oncall VC : callDisconnected_custom : Incoming call Hangup Incoming Twlilo");
        dispatch_async(dispatch_get_main_queue(), ^{
            
             NSLog(@"pop on call calldiconeected custom");
            [self TimerStop];
            [[self navigationController] popViewControllerAnimated:NO];
        });
    }
    
    NSString *connectedString = self.reviewpopup ? @"YES" : @"NO";
    
    NSLog(@"value of reviewpopup %@",connectedString);
    
    if(self.reviewpopup == true){
        self.reviewpopup = false;
        NSLog(@"***********************ratting_popup11");
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self->incOutCall == true){
                self->incOutCall = false;
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                    [self ratting_popup];
                }
                else
                {
                    NSString *numbVer = [Default valueForKey:IS_ACW_ENABLE];
                    int num = [numbVer intValue];
                    if (num == 1)
                    {
                        [self endWorkApi];
                    }
                }
            }else {
                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                {
                   //rem [self reminderPopup];
                }
            }
        });
    }
}


- (void)call:(TVOCall *)call isReconnectingWithError_custom:(NSError *)error
{
    NSLog(@"Call is reconnecting  : OncallVC");
}
- (void)call:(TVOCall *)call didFailToConnectWithError_custom:(NSError *)error
{
    NSLog(@"Call failed to connect : OncallVC : %@", error);
    [self callDisconnected_custom:call];
    
    if (error)
    {
        NSLog(@"Access token : %@",error.localizedDescription);
        NSString *userEmailId = [Default valueForKey:kUSEREMAIL];
        [FIRAnalytics logEventWithName:@"twilio_token_expire" parameters:@{@"tw_uname": userEmailId}];
        [UtilsClass twilio_access_token_get];
    }
    else
    {
        
    }
}
- (void)call:(TVOCall *)call didDisconnectWithError_custom:(NSError *)error
{
    NSLog(@"Call didDisconnectWithError : OncallVC");
    if (error) {
        NSLog(@"Call failed: %@", error);
    } else {
        NSLog(@"Call disconnected");
    }
    [self callDisconnected_custom:call];
}


- (void)dtmfdigits:(NSString *)data {
    [activeCall sendDigits:data];
}
-(void)Linphone_dtmfdigits:(NSString *)data {
   
    linphone_call_send_dtmfs(currentCall, [data UTF8String]);
}

- (void)transferButtonEnable {
    if([extentionCall isEqualToString:@"true"]){
        [_btnForword setEnabled:false];
        [_btnHold setEnabled:true];
    }else{
        if ([[Default valueForKey:CallingProvider] isEqualToString:Login_Twilio]){
            NSString *istransfer = [Default valueForKey:TRANSFER_FOR_TWILIO];
            int transfer = [istransfer intValue];
            if (transfer == 1) {
                [_btnForword setEnabled:true];
            }else{
                [_btnForword setEnabled:false];
            }
        }else{
            [_btnForword setEnabled:true];
        }
    }
}
// trushang

-(void) callDidStartRinging_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : callDidStartRinging_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
    activeCall = (TVOCall*)userInfo[@"call"];
}

-(void) callDidConnect_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : callDidConnect_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
    activeCall = (TVOCall*)userInfo[@"call"];
    
    self.lblTimer.hidden = false;
    [self.btnHold setEnabled:YES];
    [self.btnMute setEnabled:YES];
    [self transferButtonEnable];
    [self enableRecordButton];
    [self enableNotes];
    [self TimerStart];
}

-(void) callDidReconnect_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : callDidReconnect_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
    activeCall = (TVOCall*)userInfo[@"call"];
}

-(void) callDisconnected_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : callDisconnected_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
    activeCall = (TVOCall*)userInfo[@"call"];
    
    if([_CallStatus isEqualToString:OUTGOING])
    {
        NSLog(@"Oncall VC : callDisconnected_custom : Outgoing call Hangup OUTGOING Twlilo");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self TimerStop];
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        
    }
    else
    {
        NSLog(@"Oncall VC : callDisconnected_custom : Incoming call Hangup Incoming Twlilo");
        dispatch_async(dispatch_get_main_queue(), ^{
             NSLog(@"pop on call in call disconned custom noti");
            [self TimerStop];
            [[self navigationController] popViewControllerAnimated:NO];
            
        });
    }
    
    NSString *connectedString = self.reviewpopup ? @"YES" : @"NO";
    
    NSLog(@"value of reviewpopup 1 %@",connectedString);
    
    if(self.reviewpopup == true){
        self.reviewpopup = false;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->incOutCall == true){
            self->incOutCall = false;
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
            {
                [self ratting_popup];
            }
            else
            {
                NSString *numbVer = [Default valueForKey:IS_ACW_ENABLE];
                int num = [numbVer intValue];
                if (num == 1)
                {
                    [self endWorkApi];
                }
            }
        }else {
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
            {
               //rem [self reminderPopup];
            }
        }
    });}
}

-(void) isReconnectingWithError_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : isReconnectingWithError_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
    activeCall = (TVOCall*)userInfo[@"call"];
}

-(void) didFailToConnectWithError_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : didFailToConnectWithError_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
//    NSLog(@"NOTI : [] [] : %@",userInfo);
    activeCall = (TVOCall*)userInfo[@"call"];
    NSError *error = userInfo[@"error"];
    [self callDisconnected_custom:activeCall];
    if (error)
    {
        NSLog(@"Access token : %@",error.localizedDescription);
        NSString *userEmailId = [Default valueForKey:kUSEREMAIL];
        [FIRAnalytics logEventWithName:@"twilio_token_expire" parameters:@{@"tw_uname": userEmailId}];
        [UtilsClass twilio_access_token_get];
    }
    else
    {
        
    }
}

-(void) didDisconnectWithError_custom_noti:(NSNotification*)notification
{
    NSLog(@"Trushang [] [] Notification : didDisconnectWithError_custom_noti");
    NSDictionary* userInfo = notification.userInfo;
    activeCall = (TVOCall*)userInfo[@"call"];
    [self callDisconnected_custom:activeCall];
}

#pragma mark - tableview delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return omatchedList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *tblcell = [tableView dequeueReusableCellWithIdentifier:@"Tblcell"];
    
    tblcell.lbl_tagname.text = [omatchedList objectAtIndex:indexPath.row];
    
    return tblcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //tagging.updateTaggedList(allText: tagging.textView.text, tagText: matchedList[indexPath.row])
    
    [_txtview_tagging updateTaggedListWithAllText:_txtview_tagging.textView.text tagText:omatchedList[indexPath.row]];
    NSUInteger i = [[tagsArray valueForKey:@"name"] indexOfObjectIdenticalTo:omatchedList[indexPath.row]];
    [tagIdArray addObject:[[tagsArray valueForKey:@"id"] objectAtIndex:i]];
    [selTagArray addObject:[tagsArray objectAtIndex:i]];

  //  [selTagNameArray addObject:[[tagsArray valueForKey:@"name"] objectAtIndex:i]];
    
    _tbl_taggableList.hidden = true;

    
}
-(void)tableHeight
{
    
    if (omatchedList.count >= 2) {
        
        _tagTableHeightConstraint.constant = 60;
    }
    else if(omatchedList.count == 1)
    {
        _tagTableHeightConstraint.constant  = 30;
    }
    else
    {
        _tagTableHeightConstraint.constant  = 0;
    }
    
}
#pragma mark - tagging datasources
-(void)tagging:(Tagging *)tagging didChangedTaggedList:(NSArray<TaggingModel *> *)taggedList
{
   // otaggedList = taggedList;
    
//
//    _tbl_taggableList.hidden = false;
//     [_tbl_taggableList reloadData];
//
//    [self tableHeight];
    
    
//      if (taggedList.count < tagIdArray.count) {
//    NSMutableArray *tagidClone = [[NSMutableArray alloc]init];
//
//    for (int i=0; i<taggedList.count; i++) {
//
//
//        TaggingModel *tm = taggedList[i];
//        NSUInteger p = [[tagsArray valueForKey:@"name"] indexOfObjectIdenticalTo:tm.text];
//
//        NSString *tagidStr = [[tagsArray valueForKey:@"id"] objectAtIndex:p];
//
//        if ([tagIdArray containsObject:tagidStr]) {
//
//            [tagidClone addObject:tagidStr];
//
//        }
//
//    }
//
//    if (tagIdArray.count>0) {
//
//        [tagIdArray removeAllObjects];
//        [tagIdArray addObjectsFromArray:tagidClone];
//    }
//
//      }
//
   
    
    
   
    NSLog(@"tag id array... %@",tagIdArray);

    if (tagIdArray.count>0) {
        if (taggedList.count < tagIdArray.count) {

                NSLog(@"last object removed:: %@",taggedList);
                  NSLog(@"tag id array %@",taggedList);

               [tagIdArray removeLastObject];
           
           }
    }

    
        
    NSLog(@"tagged list :: %@",taggedList);

}
-(void)tagging:(Tagging *)tagging didChangedTagableList:(NSArray<NSString *> *)tagableList
{
    
    NSLog(@"taggable list :: %@",tagableList);
    NSLog(@"text  :: %@",_txtview_tagging.textView.text);
    
    if (![_txtview_tagging.textView.text isEqualToString:@"Add notes and add tags by typing @"] ) {
        
        
        unichar character = [_txtview_tagging.textView.text characterAtIndex:_txtview_tagging.textView.text.length - 1];
           if (character == '@') {

               NSLog(@"last char is @ ::");
               omatchedList = [tagsArray valueForKey:@"name"];
           }
           else
           {
                omatchedList = tagableList;
               
           }
           
            _tbl_taggableList.hidden = false;
           NSLog(@"textview text:: %@",_txtview_tagging.textView.text);

           [_tbl_taggableList reloadData];
           [self tableHeight];
    }
    
   
    
}
-(void)hideTagTable:(NSNotification *)notification
{
    NSLog(@"notification called");
    _tbl_taggableList.hidden = true;
}


- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        return [predicate evaluateWithObject:string];
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    
}


-(void)createNotesDicToPass
{
    BOOL blindTransferBool;
    BOOL transferBool;
    BOOL extensionBool;

     if ([_CallStatus isEqualToString:OUTGOING]) {
         
         if ([[Default valueForKey:extentionCall] isEqualToString:@"true"])
             extensionBool = true;
         else
             extensionBool = false;
         
         if ([[Default valueForKey:IS_BLIND_TRANSFER_FLAG] isEqualToString:@"true"])
             blindTransferBool = true;
         else
             blindTransferBool = false;
         
         transferBool = false;
         
    
     }
    else
    {
        if ([[extraHeader valueForKey:@"xphextensioncall"] isEqualToString:@"true"])
            extensionBool = true;
        else
            extensionBool = false;
        
        if ([[extraHeader valueForKey:@"xphblindtransfer"] isEqualToString:@"true"])
            blindTransferBool = true;
        else
            blindTransferBool = false;
        
        //take reverse bool same as ACW
        if ([[extraHeader valueForKey:@"xphcalltransfer"] isEqualToString:@"true"])
            transferBool = false;
        else
            transferBool = true;
        
    }
    
    notesPassDict = [NSDictionary dictionaryWithObjectsAndKeys:@(extensionBool),@"internalCalling",@(blindTransferBool),@"isBlindTransfer",@(transferBool),@"isTransferedCall", nil];
               
    
}

#pragma mark - linphone methods
- (void)callUpdate:(NSNotification *)notif {

    NSLog(@"call update being called");
    
  
    LinphoneCall *call = [[notif.userInfo objectForKey:@"call"] pointerValue];
    
    currentCall = call;
    
    LinphoneCallState state = [[notif.userInfo objectForKey:@"state"] intValue];
    NSString *message = [notif.userInfo objectForKey:@"message"];
    
    
    
    NSLog(@"Trushang You are working : %u",state);
    
    NSLog(@"Linphone message : %@",message);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate soket_disconnect];
    NSString *name = [NSString stringWithFormat:@"%u",state];
    
    switch (state) {
            
        case LinphoneCallIncomingReceived:
            NSLog(@"Trushang_Patel_call :  LinphoneCallIncomingReceived");
            name = [NSString stringWithFormat:@"LinphoneCallIncomingReceived"];
            break;
            
        case LinphoneCallIncomingEarlyMedia: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallIncomingEarlyMedia");
            name = [NSString stringWithFormat:@"LinphoneCallIncomingEarlyMedia"];
            break;
        }
        case LinphoneCallOutgoingInit: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallOutgoingInit");
            name = [NSString stringWithFormat:@"LinphoneCallOutgoingInit"];
            break;
        }
        case LinphoneCallPausedByRemote:
            NSLog(@"Trushang_Patel_call :  LinphoneCallPausedByRemote");
            name = [NSString stringWithFormat:@"LinphoneCallPausedByRemote"];
            
        case LinphoneCallConnected: {
            
           

            NSLog(@"Trushang_Patel_call :  LinphoneCallConnected");
            name = [NSString stringWithFormat:@"LinphoneCallConnected"];
            // call connected
            if([_CallStatus isEqualToString: OUTGOING]) {
                [self.btnHold setEnabled:YES];
            }
            
            [self.btnMute setEnabled:YES];
            [self.btnDialPad setEnabled:YES];
            [self.btnSpeaker setEnabled:YES];
            [self.btnRecord setEnabled:YES];
            [self.btnPostCallSurvey setEnabled:YES];
            
            
            //            [self.btnForword setEnabled:YES];
            NSString *timervalue = [Default valueForKey:Timer_Call];
            NSLog(@"LinphoneCallConnected timervalue : %@",timervalue);
            if([ _CallStatus isEqualToString:OUTGOING])
            {
                if([timervalue isEqualToString:Timer_Stop])
                {
                    [Default setValue:Timer_Stop forKey:Timer_Call];
                    
                    [self enableRecordButton];
                     [self enableNotes];
                    [self TimerStart];
                }
            } else
            {
                //                if([timervalue isEqualToString:Timer_Start])
                //                {
                [Default setValue:Timer_Stop forKey:Timer_Call];
                NSLog(@"\n \n \n \n \n Baby dolan solan di \n \n \n \n \n \n ");
                [self TimerStart];
                
                //                }
            }
            break;
        }
        case LinphoneCallStreamsRunning: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallStreamsRunning");
            name = [NSString stringWithFormat:@"LinphoneCallStreamsRunning"];
            
            
            break;
        }
        case LinphoneCallUpdatedByRemote: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallUpdatedByRemote");
            name = [NSString stringWithFormat:@"LinphoneCallUpdatedByRemote"];
            break;
        }
        case LinphoneCallError: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallError");
            
            name = [NSString stringWithFormat:@"LinphoneCallError"];
//            if (incOutCall == true){
//                incOutCall = false;
//                if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
//                {
//
//                }
//                else
//                {
//                    NSString *numbVer = [Default valueForKey:IS_ACW_ENABLE];
//                    int num = [numbVer intValue];
//                    if (num == 1)
//                    {
//                        [self endWorkApi];
//                    }
//                }
//            }
        }
        case LinphoneCallEnd: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallEnd");
            name = [NSString stringWithFormat:@"LinphoneCallEnd"];
            [self TimerStop];
            
            [LinphoneManager.instance.providerDelegate performEndCallActionWithUUID];
            
            if([_CallStatus isEqualToString:OUTGOING])
            {
                //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                
                //                });
            }
            else
            {
                [[self navigationController] popViewControllerAnimated:NO];
                
            }
            
            self.reviewpopup = false;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->incOutCall == true){
                    self->incOutCall = false;
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                        [self ratting_popup];
                    }
                }else{
                    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
                    {
                       //rem [self reminderPopup];
                    }
                }
            });
            NSLog(@"Total speed : %@",arr_low_speeds);
            if (arr_low_speeds.count != 0)
            {
                [self api_network_strenth_upload];
            }
            break;
        }
        case LinphoneCallEarlyUpdatedByRemote:
            NSLog(@"Trushang_Patel_call :  LinphoneCallEarlyUpdatedByRemote");
            name = [NSString stringWithFormat:@"LinphoneCallEarlyUpdatedByRemote"];
        case LinphoneCallEarlyUpdating:
            NSLog(@"Trushang_Patel_call :  LinphoneCallEarlyUpdating");
            name = [NSString stringWithFormat:@"LinphoneCallEarlyUpdating"];
        case LinphoneCallIdle:
            NSLog(@"Trushang_Patel_call :  LinphoneCallIdle");
            name = [NSString stringWithFormat:@"LinphoneCallIdle"];
            break;
        case LinphoneCallOutgoingEarlyMedia:
            NSLog(@"Trushang_Patel_call :  LinphoneCallOutgoingEarlyMedia");
            name = [NSString stringWithFormat:@"LinphoneCallOutgoingEarlyMedia"];
        case LinphoneCallOutgoingProgress: {
            NSLog(@"Trushang_Patel_call :  LinphoneCallOutgoingProgress");
            name = [NSString stringWithFormat:@"LinphoneCallOutgoingProgress"];
            break;
        }
        case LinphoneCallOutgoingRinging:
            NSLog(@"Trushang_Patel_call :  LinphoneCallOutgoingRinging");
            name = [NSString stringWithFormat:@"LinphoneCallOutgoingRinging"];
        case LinphoneCallPaused:
            NSLog(@"Trushang_Patel_call :  LinphoneCallPaused");
            name = [NSString stringWithFormat:@"LinphoneCallPaused"];
        case LinphoneCallPausing:
            NSLog(@"Trushang_Patel_call :  LinphoneCallPausing");
            name = [NSString stringWithFormat:@"LinphoneCallPausing"];
        case LinphoneCallRefered:
            NSLog(@"Trushang_Patel_call :  LinphoneCallRefered");
            name = [NSString stringWithFormat:@"LinphoneCallRefered"];
        case LinphoneCallReleased:
            NSLog(@"Trushang_Patel_call :  LinphoneCallReleased");
            name = [NSString stringWithFormat:@"LinphoneCallReleased"];
            break;
        case LinphoneCallResuming: {
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max && call) {
                NSUUID *uuid = (NSUUID *)[LinphoneManager.instance.providerDelegate.uuids
                    objectForKey:[NSString stringWithUTF8String:linphone_call_log_get_call_id(
                                                                    linphone_call_get_call_log(call))]];
                if (!uuid) {
                    break;
                }
                CXSetHeldCallAction *act = [[CXSetHeldCallAction alloc] initWithCallUUID:uuid onHold:NO];
                CXTransaction *tr = [[CXTransaction alloc] initWithAction:act];
                [LinphoneManager.instance.providerDelegate.controller requestTransaction:tr
                                                                              completion:^(NSError *err){
                                                                              }];
            }
            break;
        }
        case LinphoneCallUpdating:
            NSLog(@"Trushang_Patel_call :  LinphoneCallUpdating");
            name = [NSString stringWithFormat:@"LinphoneCallUpdating"];
            break;
    }
    
}

- (void)updateStats {
    
    
    if(linphone_core_get_current_call(LC))
    {
        LinphoneCall *call = linphone_core_get_current_call(LC);
        const LinphoneCallStats *stats1;
        stats1 = linphone_call_get_audio_stats(call);
        NSMutableString *result = [[NSMutableString alloc] init];
        NSString *download = @"";
        NSString *upload = @"";
        NSString *networkspeed = @"";
        download = [NSString stringWithFormat:@"%1.1f",
                    linphone_call_stats_get_download_bandwidth(stats1)];
        
        upload = [NSString stringWithFormat:@"%1.1f",
                  linphone_call_stats_get_upload_bandwidth(stats1)];
        
        networkspeed = [Default valueForKey:Network_Speed_Value];
        NSLog(@"Trushang : Final : networkspeed : %@",networkspeed);
        Download_final = [download floatValue];
        Uownload_final = [upload floatValue];
        CGFloat Network_final = [networkspeed floatValue];
        
        [result appendString:[NSString stringWithFormat:@"Download bandwidth: %1.1f kbits",linphone_call_stats_get_download_bandwidth(stats1)]];
        
        [result appendString:[NSString stringWithFormat:@"Upload bandwidth: %1.1f kbits/s",
                              linphone_call_stats_get_upload_bandwidth(stats1)]];
        _lbl_speed_text.text = [NSString stringWithFormat:@"Download : %1.1f Upload %1.1f",Download_final,Uownload_final];
//        NSLog(@"Trushang : view_network_strenth : Download : %@",download);
//        NSLog(@"Trushang : view_network_strenth : Upload : %@",upload);
//
        NSLog(@"Trushang : Final : Upload : %f",Download_final);
        NSLog(@"Trushang : Final : Upload : %f",Network_final);
        
        //            [UtilsClass makeToast:[NSString stringWithFormat:@"Download : %f and Upload %f ",Download_final,Network_final]];
        
        if(Download_final < Network_final)
        {
            if([isnetworkstrenth isEqualToString:@"1"])
            {
                if([isnetworkstrenth_click isEqualToString:@"1"])
                {
                    [self view_strenth];
                    int myInt = (int) self->Download_final;
                    NSString *speed = [NSString stringWithFormat:@"%d",myInt];
                    [self->arr_low_speeds addObject:speed];
                }
            }
            
        }
        
        [self upload_strenth];
    }
}
// >>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
@end
