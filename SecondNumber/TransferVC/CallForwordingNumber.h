//
//  CallForwordingNumber.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallForwordingNumber : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;

@property (strong, nonatomic) IBOutlet UIView *viewCell;
@property (strong, nonatomic) IBOutlet UILabel *lblOnline;

//@property (strong, nonatomic) IBOutlet UIView *viewActive;
@property (weak, nonatomic) IBOutlet UIButton *btn_call_nnumber;

@property (strong, nonatomic) IBOutlet UIView *view_boder;

@property (weak, nonatomic) IBOutlet UILabel *lbl_name_with_number;
@property (weak, nonatomic) IBOutlet UILabel *lbl_number;

@end

NS_ASSUME_NONNULL_END
