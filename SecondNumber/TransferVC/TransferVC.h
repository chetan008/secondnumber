//
//  TransferVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "WebApiController.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TransferComplet <NSObject>

@optional
- (void)transferComplet;
- (void)endCallButtonEnableComplet;
@end
@interface TransferVC : UIView
{
    WebApiController *obj;
    NSMutableArray *subUsers;
     NSMutableArray *Filter_subUsers;
    NSMutableArray *dateArr;
    NSDictionary *extraHeader;
    UIViewController *controller;
    
    NSMutableArray *Mixallcontact;
    NSMutableArray *FilterContact;
    
}
@property (nonatomic, strong)   id<TransferComplet> delegate;
@property NSDictionary *selectedSubUser;
@property NSString *subUserID;
@property NSDictionary *conData;
@property NSString *callStatus;
@property NSString *isSecondTransfer;
@property NSString *fromNumber;
@property NSString *toNumber;
@property NSString *toCalleName;
@property NSString *Selected_tab_name;
@property NSString *contactType;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectSubUserTitle;
// Transfer Now View
@property (strong, nonatomic) IBOutlet UILabel *lblTitleTransferNowView;
@property (strong, nonatomic) IBOutlet UILabel *lblSubUserTransferNow;
@property (strong, nonatomic) IBOutlet UIButton *btnTransferNow;
@property (strong, nonatomic) IBOutlet UIButton *btnTalkTofirstUser;
@property (strong, nonatomic) IBOutlet UILabel *lblBlindUserName;




@property (strong, nonatomic) IBOutlet UIView *viewSwitch;
@property (strong, nonatomic) IBOutlet UIView *viewSwitchCaller;
@property (strong, nonatomic) IBOutlet UIView *viewSwitchSubUser;
@property (strong, nonatomic) IBOutlet UIView *viewMergeEndCon;
@property (strong, nonatomic) IBOutlet UIView *viewMergeLeave;
@property (strong, nonatomic) IBOutlet UIView *viewBlindTransfer;

//Switch View
@property (strong, nonatomic) IBOutlet UILabel *lblSwitchSubUser;
@property (strong, nonatomic) IBOutlet UIView *viewSwitchTransferNow;
@property (strong, nonatomic) IBOutlet UIView *viewSwitchMergeCall;
@property (strong, nonatomic) IBOutlet UILabel *lblSwitchCaller;
@property (strong, nonatomic) IBOutlet UIImageView *imgCaller;
@property (strong, nonatomic) IBOutlet UIImageView *imgSubUser;



//Confrance View
@property (strong, nonatomic) IBOutlet UIView *viewMerge;
@property (strong, nonatomic) IBOutlet UIView *viewMergeSubUser;
@property (strong, nonatomic) IBOutlet UIView *viewMergeCaller;

@property (strong, nonatomic) IBOutlet UIButton *btnEndConMe;
@property (strong, nonatomic) IBOutlet UIButton *btnEndCOn;
@property (strong, nonatomic) IBOutlet UILabel *lblConCaller;
@property (strong, nonatomic) IBOutlet UILabel *lblConSubUser;

@property (strong, nonatomic) IBOutlet UIButton *btnSwitchCaller;
@property (strong, nonatomic) IBOutlet UIButton *btnSwitchSubUser;
@property (strong, nonatomic) IBOutlet UIButton *btnSwitchTransferNow;
@property (strong, nonatomic) IBOutlet UIButton *btnSwitchMerge;
@property (strong, nonatomic) IBOutlet UIButton *btnHangup;



// subuserlist
@property (weak, nonatomic) IBOutlet UIView *view_subuser_list;

@property (strong, nonatomic) IBOutlet UITableView *tblSubUserList;
@property (weak, nonatomic) IBOutlet UIButton *btn_sub_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sub_user;

@property (weak, nonatomic) IBOutlet UIButton *btn_contacts;
@property (weak, nonatomic) IBOutlet UILabel *lbl_contacts;
@property (weak, nonatomic) IBOutlet UITextField *txt_search_text;



@end

NS_ASSUME_NONNULL_END
