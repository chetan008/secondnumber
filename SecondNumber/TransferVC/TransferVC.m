//
//  TransferVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "TransferVC.h"
#import "Tblcell.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "CallForwordingCollCell.h"

#import "UIView+Toast.h"

#import "CallForwordingNumber.h"
#import "Constants.h"
#import "GlobalData.h"
#import "AppDelegate.h"
#import <FirebaseAnalytics/FIRAnalytics.h>

@implementation TransferVC


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) [self setup];
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) [self setup];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self setup];
    return self;
}

- (void)setup
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(endCallNotification:)
                                                     name:@"removipopupnotification"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(warmTransferAcceptedNotification:)
                                                     name:@"WarmTransferAccept"
                                                   object:nil];
        [self setUpDesign];
    });
    
}

-(void)setUpDesign {
    
    //    [_tblSubUserList registerClass:[CallForwordingCollCell class] forCellReuseIdentifier:@"CallForwordingCollCell"];
    //    [_tblSubUserList registerClass:[UINib nibWithNibName:@"CallForwordingCollCell" bundle:nil] forCellReuseIdentifier:@"CallForwordingCollCell"];
    
    _txt_search_text.backgroundColor = [UIColor clearColor];
    _txt_search_text.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _txt_search_text.layer.borderWidth = 1.0;
    _txt_search_text.layer.cornerRadius = 5.0;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, _txt_search_text.frame.size.height)];
    _txt_search_text.leftView = paddingView;
    _txt_search_text.leftViewMode = UITextFieldViewModeAlways;
    _txt_search_text.placeholder = @"Search sub user";
    //_txt_search_text.delegate = self;
    _Selected_tab_name = @"Subuser";
    
    [_btnSwitchMerge setEnabled:false];
    [_btnSwitchTransferNow setEnabled:false];
    [_btnSwitchCaller setEnabled:false];
    [_btnSwitchSubUser setEnabled:false];
    [_btnHangup setEnabled:false];
    _btnTalkTofirstUser.enabled = true;
    extraHeader = [[NSDictionary alloc]init];
    extraHeader = [[NSUserDefaults standardUserDefaults]objectForKey:@"extraHeader"];
    
//    self.layer.borderWidth = 1.0;
//    self.layer.borderColor = [[UIColor colorWithRed:208 green:208 blue:208 alpha:1] CGColor];
    
    //    [UtilsClass view_shadow_boder:self.subviews];
    
    [UtilsClass view_shadow_boder:_btnBack];
    
    [UtilsClass view_shadow_boder:_viewSwitchCaller];
    
    [UtilsClass view_shadow_boder:_viewSwitchSubUser];
    
    [UtilsClass view_shadow_boder:_viewSwitchTransferNow];
    
    [UtilsClass view_shadow_boder:_viewSwitchMergeCall];
    
    [UtilsClass view_shadow_boder:_viewMerge];
    
    [UtilsClass view_shadow_boder:_viewMergeLeave];
    
    [UtilsClass view_shadow_boder:_viewMergeCaller];
    
    [UtilsClass view_shadow_boder:_viewMergeEndCon];
    
    [UtilsClass view_shadow_boder:_viewMergeSubUser];
    
    [UtilsClass view_shadow_boder:_btnTransferNow];
    
    [UtilsClass view_shadow_boder:_btnTalkTofirstUser];
    
    self.view_subuser_list.hidden = false;
    _tblSubUserList.hidden = false;
    //    _lblSelectSubUserTitle.hidden = false;
    _viewBlindTransfer.hidden = true;
    _viewSwitch.hidden = true;
    _viewMerge.hidden = true;
    _viewSwitchCaller.layer.borderWidth = 0.5;
    self.layer.borderWidth = 0;
    //  _viewSwitchSubUser.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _viewSwitchSubUser.layer.borderWidth = 0.5;
    _viewSwitchSubUser.layer.borderColor = [UIColor orangeColor].CGColor;
    
    _btnHangup.layer.borderWidth = 0.5;
    //    _btnHangup.layer.borderColor = [UIColor orangeColor].CGColor;
    
    [self getSubUaser];
    [self contact_get];
}

-(void)contact_get
{
    Mixallcontact = [[NSMutableArray alloc] init];
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    FilterContact = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *arr = [FilterContact sortedArrayUsingDescriptors:@[sortDescriptor] ];
    FilterContact = (NSMutableArray*)arr;
}

-(void)getSubUaser{
    
    NSString *userId = [Default valueForKey:USER_ID];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@/getActiveSubUsers",userId];
    //NSLog(@"Sub User List --- %@",strUrl);
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_GET:strUrl andParams:nil SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : get sub user : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            subUsers = [[NSMutableArray alloc] init];
            subUsers =  [NSMutableArray arrayWithArray:response1[@"data"][@"data"]];
            Filter_subUsers = [[NSMutableArray alloc] init];
            Filter_subUsers =  [NSMutableArray arrayWithArray:response1[@"data"][@"data"]];
            //NSLog(@"Response Of subusers : %@",subUsers);
            if ([subUsers count] > 0) {
                [self->_tblSubUserList reloadData];
            }else {
                [UtilsClass showAlert:@"No sub users." view:self];
            }
            
            [self.tblSubUserList reloadData];
        }
        else {
            
            
            @try{
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                        [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
            
        }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([_Selected_tab_name isEqualToString:@"Subuser"])
    {
        static NSString *CellIdentifier = @"CallForwordingCollCell";
        
        NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"CallForwordingCollCell" owner:nil options:nil];
        
        CallForwordingCollCell *cell = [[CallForwordingCollCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell = [arrData objectAtIndex:0];
        
        cell.lblName.text = self->Filter_subUsers[indexPath.row][@"fullName"];
        //    [cell.viewActive layer].cornerRadius = cell.viewActive.frame.size.width / 2;
        //    [cell.viewActive layer].masksToBounds = true;
        //    [cell.viewActive layer].borderWidth = 0.5;
        
        NSString *strActive = [NSString stringWithFormat:@"%@", self->Filter_subUsers[indexPath.row][@"isAvailable"]];
        
        //NSLog(@"Avilable %@",strActive);
        
        if ([strActive isEqualToString:@"1"]) {
            [cell.lblOnline setTextColor:[UIColor greenColor]];
            cell.lblOnline.text = @"Online ●";
        } else {
            [cell.lblOnline setTextColor:[UIColor redColor]];
            cell.lblOnline.text = @"Offline ●";
        }
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"CallForwordingNumber";
        
        NSArray *arrData = [[NSBundle mainBundle]loadNibNamed:@"CallForwordingNumber" owner:nil options:nil];
        
        CallForwordingNumber *cell = [[CallForwordingNumber alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell = [arrData objectAtIndex:0];
        NSString *name = self->FilterContact[indexPath.row][@"name"];
       //p NSString *number = self->FilterContact[indexPath.row][@"number"];
        
        NSString *number;
        if ([FilterContact[indexPath.row][@"numberArray"] count]>0) {
            number = self->FilterContact[indexPath.row][@"numberArray"][0][@"number"];
        }
        else
        {
            number = @"";
        }
        
        
        if([name isEqualToString:@""])
        {
            cell.lblName.hidden = true;
            cell.lbl_name_with_number.hidden = true;
            cell.lbl_number.hidden = false;
            cell.lbl_number.text = number;
        }
        else
        {
            cell.lblName.hidden = false;
            cell.lbl_name_with_number.hidden = false;
            cell.lbl_number.hidden = true;
            cell.lblName.text = name;
            cell.lbl_name_with_number.text = number;
        }
        
        if([Default boolForKey:kIsNumberMask] == true)
        {
            cell.lbl_name_with_number.text = [UtilsClass get_masked_number:number];
        }
        
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([_Selected_tab_name isEqualToString:@"Subuser"])
    {
        NSString *strActive = [NSString stringWithFormat:@"%@", Filter_subUsers[indexPath.row][@"isAvailable"]];
        
        if ([strActive isEqualToString:@"1"]) {
            self.selectedSubUser = Filter_subUsers[indexPath.row];
            self.subUserID = Filter_subUsers[indexPath.row][@"_id"];
            _contactType = @"SubUser";
            [self didSelecectPerform];
        } else {
            [UtilsClass showAlert:@"User is not available at this time." view:self];
        }
    }
    else
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init ];
        //NSLog(@"filtered array _____---- %@",self->FilterContact[indexPath.row]);
        if([self->FilterContact[indexPath.row][@"name"] isEqualToString:@""]){
           
            //[dic setValue:self->FilterContact[indexPath.row][@"number"] forKey:@"fullName"];
            
            if ([[[FilterContact objectAtIndex:indexPath.row] valueForKey:@"numberArray"] count]>0) {
                NSString *num = [[[[FilterContact objectAtIndex:indexPath.row] valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                
                [dic setValue:num forKey:@"fullName"];
                self.selectedSubUser = dic;
            }
            
            
        }else{
            [dic setValue:self->FilterContact[indexPath.row][@"name"] forKey:@"fullName"];
            self.selectedSubUser = dic;
        }
        
        if (!self->FilterContact[indexPath.row][@"_id"]){
            
            if([_callStatus isEqualToString: OUTGOING]) {
                [FIRAnalytics setUserPropertyString:@"" forName:@"external_transfer_outgoing"];
            }else {
                [FIRAnalytics setUserPropertyString:@"" forName:@"external_transfer_incoming"];
            }
            
            _contactType = @"Number";
          //  self.subUserID = [self->FilterContact[indexPath.row][@"number"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if ([FilterContact[indexPath.row][@"numberArray"] count]>0) {
                self.subUserID = self->FilterContact[indexPath.row][@"numberArray"][0][@"number"];
                self.subUserID = [self.subUserID stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                if (![[[FilterContact objectAtIndex:indexPath.row] valueForKey:@"name"] isEqualToString:@""]) {
                     [UtilsClass contact_save_in_callhippo:FilterContact[indexPath.row][@"name"] contact_number:FilterContact[indexPath.row][@"numberArray"][0][@"number"]];
                }
               
            }
            else
            {
                self.subUserID = @"";
            }
            
        }else{
            _contactType = @"Contact";
            self.subUserID = self->FilterContact[indexPath.row][@"_id"];
        }
        
        [self didSelecectPerform];
        
    }
    
    
}

-(void)didSelecectPerform{
    //    self.lblTitleTransferNowView.text = self.selectedSubUser[@"fullName"];
    //    self.lblSubUserTransferNow.text = [NSString stringWithFormat:@"Talk To %@ First",self.selectedSubUser[@"fullName"]];
    
    //    NSString *strTo = self.extHeader[@"X-PH-From"];
    //    strTo = [strTo stringByReplacingOccurrencesOfString:@": " withString:@""];
    if ([_callStatus isEqualToString:@"Outgoing"]){
        
       
        _lblSwitchCaller.text = _toCalleName;
        _lblConCaller.text = _toCalleName;
        
        if ([Default boolForKey:kIsNumberMask] == true) {
        if([self validateString:[_toCalleName stringByReplacingOccurrencesOfString:@"+" withString:@""] withPattern:@"^[0-9]+$"])
        {
            _lblSwitchCaller.text = [UtilsClass get_masked_number:_toCalleName];
            _lblConCaller.text = [UtilsClass get_masked_number:_toCalleName];
        }
        }
    }else{
        
        NSString *xphstr = [[extraHeader valueForKey:@"xphfrom"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
        xphstr = [NSString stringWithFormat:@"+%@",xphstr];
        
        _lblSwitchCaller.text = xphstr;
        _lblConCaller.text = xphstr;
        
        if ([Default boolForKey:kIsNumberMask] == true) {
               if([self validateString:[[extraHeader valueForKey:@"xphfrom"] stringByReplacingOccurrencesOfString:@"+" withString:@""] withPattern:@"^[0-9]+$"])
               {
                   _lblSwitchCaller.text = [UtilsClass get_masked_number:xphstr];
                   _lblConCaller.text = [UtilsClass get_masked_number:xphstr];
               }
               }
    }
    
    _lblConSubUser.text = self.selectedSubUser[@"fullName"];
    _lblSwitchSubUser.text = self.selectedSubUser[@"fullName"];
    _lblBlindUserName.text = self.selectedSubUser[@"fullName"];
    
    if ([Default boolForKey:kIsNumberMask] == true) {
    if([self validateString:[self.selectedSubUser[@"fullName"] stringByReplacingOccurrencesOfString:@"+" withString:@""] withPattern:@"^[0-9]+$"])
    {
        _lblSwitchSubUser.text = [UtilsClass get_masked_number:self.selectedSubUser[@"fullName"]];
        _lblConSubUser.text = [UtilsClass get_masked_number:self.selectedSubUser[@"fullName"]];
        _lblBlindUserName.text = [UtilsClass get_masked_number:self.selectedSubUser[@"fullName"]];
    }
    }
    
    NSString *to = self.selectedSubUser[@"fullName"];
    if ([to length] > 9){
        to=[to substringToIndex:9];
        to=[NSString stringWithFormat:@"%@...",to];
        [_btnTalkTofirstUser setTitle:[NSString stringWithFormat:@"Talk to %@",to] forState:UIControlStateNormal];
        
        if ([Default boolForKey:kIsNumberMask] == true) {
        if([self validateString:[self.selectedSubUser[@"fullName"] stringByReplacingOccurrencesOfString:@"+" withString:@""] withPattern:@"^[0-9]+$"])
        {
             [_btnTalkTofirstUser setTitle:[NSString stringWithFormat:@"Talk to %@",[UtilsClass get_masked_number:to]] forState:UIControlStateNormal];
        }
        }
    }else{
        [_btnTalkTofirstUser setTitle:[NSString stringWithFormat:@"Talk to %@",self.selectedSubUser[@"fullName"]] forState:UIControlStateNormal];
        
        if ([Default boolForKey:kIsNumberMask] == true) {
               if([self validateString:[self.selectedSubUser[@"fullName"] stringByReplacingOccurrencesOfString:@"+" withString:@""] withPattern:@"^[0-9]+$"])
               {
                    [_btnTalkTofirstUser setTitle:[NSString stringWithFormat:@"Talk to %@",[UtilsClass get_masked_number:self.selectedSubUser[@"fullName"]]] forState:UIControlStateNormal];
               }
               }
    }
    
   
    
    
    // cell.lbl_location.text =[NSString stringWithFormat:@"via %@ |%@  %@",to,dict[@"time"],dict[@"callStatus"]];
    
    
    
    self.view_subuser_list.hidden = YES;
    self.tblSubUserList.hidden = YES;
    self.lblSelectSubUserTitle.hidden = YES;
    self.lblSelectSubUserTitle.hidden = YES;
    self.viewBlindTransfer.hidden = NO;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_Selected_tab_name isEqualToString:@"Subuser"])
    {
        return Filter_subUsers.count;
    }
    else
    {
        return FilterContact.count;
    }
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnWarmTransferSubUser:(id)sender {
    [self warmCallTransfer];
}


- (IBAction)btnTalkToFirstUser:(id)sender {
    [self warmTransfer];
    _btnTalkTofirstUser.enabled = false;
}

- (IBAction)btnSwitchCaller:(id)sender {
    
    
    UIImage *secondImage = [UIImage imageNamed:@"call-details-user"];
    
    NSData *imgData1 = UIImagePNGRepresentation(_imgCaller.image);
    NSData *imgData2 = UIImagePNGRepresentation(secondImage);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    if(isCompare)
    {
        [self warmtransferswapuser:self.subUserID];
        [_imgCaller setImage:[UIImage imageNamed:@"out-going-call"]];
        [_imgSubUser setImage:[UIImage imageNamed:@"call-details-user"]];
        _viewSwitchCaller.layer.borderColor = [UIColor orangeColor].CGColor;
        _viewSwitchSubUser.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [self makeToast:[NSString stringWithFormat:@"%@ on call",_lblSwitchCaller.text]];
        
    }else {
        //NSLog(@"caller clicked ");
    }
    
    /*if ([_imgCaller.image  isEqual:[UIImage imageNamed:@"call-details-user"]])
     {
     [self warmtransferswapuser:self.subUserID];
     [_imgCaller setImage:[UIImage imageNamed:@"out-going-call"]];
     [_imgSubUser setImage:[UIImage imageNamed:@"call-details-user"]];
     _viewSwitchCaller.layer.borderColor = [UIColor orangeColor].CGColor;
     _viewSwitchSubUser.layer.borderColor = [UIColor lightGrayColor].CGColor;
     [UtilsClass makeToast:[NSString stringWithFormat:@"%@ on call",_lblSwitchCaller.text]];
     
     }else {
     //NSLog(@"caller clicked ");
     }*/
}

- (IBAction)btnSwitchSubUser:(id)sender {
    
    
    UIImage *secondImage = [UIImage imageNamed:@"call-details-user"];
    
    NSData *imgData1 = UIImagePNGRepresentation(_imgSubUser.image);
    NSData *imgData2 = UIImagePNGRepresentation(secondImage);
    
    BOOL isCompare =  [imgData1 isEqual:imgData2];
    if(isCompare)
    {
        NSString *strFrom = @"";
        if ([_callStatus isEqualToString:@"Outgoing"]){
            strFrom = _toNumber;
        }else{
            strFrom = [extraHeader valueForKey:@"xphfromtransferNumber"];
        }
        
        [self warmtransferswapuser:strFrom];
        [_imgCaller setImage:[UIImage imageNamed:@"call-details-user"]];
        [_imgSubUser setImage:[UIImage imageNamed:@"out-going-call"]];
        _viewSwitchCaller.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _viewSwitchSubUser.layer.borderColor = [UIColor orangeColor].CGColor;
        [self makeToast:[NSString stringWithFormat:@"%@ on call",_lblSwitchSubUser.text]];
    }else {
        //NSLog(@"btnSwitchSubUser clicked ");
    }
}

- (IBAction)btnSwichMerge:(id)sender {
    [self mergeTransferedCall];
}

- (IBAction)btnSwitchTransferNow:(id)sender {
    [self endConferenceCall:false actionOnUser:@"main"];
}

- (IBAction)btnBack:(id)sender {
    //NSLog(@"close clicked");
    self.view_subuser_list.hidden = false;
    _tblSubUserList.hidden = false;
    //    _lblSelectSubUserTitle.hidden = false;
    _viewBlindTransfer.hidden = true;
    _viewSwitch.hidden = true;
    _viewMerge.hidden = true;
    //    [self removeFromSupperView];
}

- (IBAction)btnTransferNow:(id)sender {
    [self makeToast:[NSString stringWithFormat:@"Transfer to %@",self.selectedSubUser[@"fullName"]]];
    [self blinfTransfer];
    [Default setValue:@"true" forKey:IS_BLIND_TRANSFER_FLAG];
}

- (IBAction)btnRemoveCaller:(id)sender {
    
    [self endConferenceCall:false actionOnUser:@"customer"];
}

- (IBAction)btnRemoveSubUser:(id)sender {
    
    [self endConferenceCall:false actionOnUser:@"subUser"];
}

- (IBAction)btnEndCOnForMe:(id)sender {
    [self endConferenceCall:false actionOnUser:@"main"];
}

- (IBAction)btnEndCOn:(id)sender {
    [self endConferenceCall:true actionOnUser:@""];
}



- (void)removePreviousView:(UIView*)previousView FromSuperView:(UIView*)view{
    for (UIView *subView in view.subviews) {
        if (![subView isKindOfClass:[previousView class]]) {
            [subView removeFromSuperview];
        }
    }
}

-(void)warmTransfer{
    
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSDictionary *passDict = @{@"transferType" : @"Warm",
                               @"callType" : _callStatus,
                               @"subUserId":self.subUserID,
                               @"userId":userID,
                               @"contactType" : _contactType};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:@"transferCall/plivo" andParams:jsonString SuccessCallback:@selector(warmTransfer:response:) andDelegate:self];
    
}

- (void)warmTransfer:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
   // NSLog(@"TRUSHANG : STATUSCODE **************  : %@",response1);
    
    NSLog(@"Encrypted Response : warm transfer : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //         [UtilsClass showAlert:@"Done" contro:self.vi];
            //NSLog(@"Done warmTransfer");
            
            [_delegate transferComplet];
            _conData = response1[@"data"];
            self.view_subuser_list.hidden = YES;
            _tblSubUserList.hidden = YES;
            self.lblSelectSubUserTitle.hidden = YES;
            _viewBlindTransfer.hidden = YES;
            _viewMerge.hidden = YES;
            _viewSwitch.hidden = NO;
            _btnTalkTofirstUser.enabled = true;
        }
        else {
            _btnTalkTofirstUser.enabled = true;
            @try{
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                        [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }
        
    }
}

-(void)blinfTransfer{
    
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSDictionary *passDict = @{@"subUserId":self.subUserID,
                               @"userId":userID,
                               @"callType":_callStatus,
                               @"transferType":@"Cold",
                               @"contactType" : _contactType};
    
    
    //NSLog(@"Params blinfTransfer : %@",passDict);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:@"transferCall/plivo" andParams:jsonString SuccessCallback:@selector(blinfTransferr:response:) andDelegate:self];
}

- (void)blinfTransferr:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE ************** &&&& : %@",apiAlias);
    
    NSLog(@"Encrypted Response : blind transfer : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //         [UtilsClass showAlert:@"Done" contro:self.vi];
            //NSLog(@"Done warmTransfer");
            //        [[NSNotificationCenter defaultCenter]postNotificationName:@"EndCallNotification"object:self];
            [_delegate transferComplet];
            [self makeToast:[NSString stringWithFormat:@"Transfer to %@",self.selectedSubUser[@"fullName"]]];
            
            [self removeFromSupperView];
        }
        else {
            
            @try{
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                        [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                    
                    
                    //                [self removeFromSupperView];
                }
            }
            @catch (NSException *exception) {
                //            [self removeFromSupperView];
            }
        }
        
    }
    
    
}

-(void)warmtransferswapuser:(NSString *)userToBeDeaf{
    
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *url = @"warmtransferswapuser/plivo";
    
    NSDictionary *passDict = @{@"userToBeDeaf":userToBeDeaf,
                               @"subUserId":self.subUserID,
                               @"userId":userID};
    
    //NSLog(@"Params : %@",passDict);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(warmtransferswapuser:response:) andDelegate:self];
}

- (void)warmtransferswapuser:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : warm trasnsfer wap user : %@",response1);

    
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //         [UtilsClass showAlert:@"Done" contro:self.vi];
            //NSLog(@"Done warmtransferswapuser");
            //        [self removeFromSupperView];
        }
        else {
            
            @try{
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                        [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }
        
    }
}


-(void)endConferenceCall:(BOOL)endConference actionOnUser:(NSString *)actionOnUser{
    
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSDictionary *passDict = @{
        @"userId":userID,
        @"endConference":@(endConference),
        @"actionOnUser":actionOnUser
    };
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:@"endConferenceCall/plivo" andParams:jsonString SuccessCallback:@selector(endConferenceCall:response:) andDelegate:self];
    
}
- (void)endConferenceCall:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : end conference : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            //NSLog(@"Done endConferenceCall");
            [self removeFromSupperView];
        }
        else {
            @try{
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                        [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }
        
    }
    
}


-(void)mergeTransferedCall{
    
    NSString *userID = [Default valueForKey:USER_ID];
    
    
    NSDictionary *passDict = @{@"userId":userID};
    //NSLog(@"Params : %@",passDict);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:@"mergeTransferedCall/plivo" andParams:jsonString SuccessCallback:@selector(mergeTransferedCall:response:) andDelegate:self];
}

- (void)mergeTransferedCall:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : merge transfer call : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //NSLog(@"Done mergeTransferedCall");
            self.view_subuser_list.hidden = YES;
            self.tblSubUserList.hidden = YES;
            self.lblSelectSubUserTitle.hidden = YES;
            self.viewBlindTransfer.hidden = YES;
            self.viewSwitch.hidden = YES;
            self.viewMerge.hidden = NO;
            
        }
        else {
            @try{
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                       [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }
    }
}

-(void)warmCallTransfer{
    
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSDictionary *passDict = @{@"transferType" : @"Warm",
                               @"callType" : _callStatus,
                               @"subUserId":self.subUserID,
                               @"userId":userID,
                               @"contactType" : _contactType};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:@"transferCall/plivo" andParams:jsonString SuccessCallback:@selector(warmCallTransfer:response:) andDelegate:self];
    
}
- (void)warmCallTransfer:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
    NSLog(@"Encrypted Response : warm call transfer : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *view = [[UIViewController alloc] init];
        //[UtilsClass logoutUser:view];
        [UtilsClass logoutUser:view error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //NSLog(@"Done mergeTransferedCall");
            [self makeToast:[NSString stringWithFormat:@"Transfer to %@",_lblSwitchSubUser.text]];
            [self removeFromSupperView];
        }
        else {
            @try{
                
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    if ([_callStatus isEqualToString:@"Outgoing"]){
                        [UtilsClass makeAlertInWindow:[response1 valueForKey:@"error"][@"error"]];
                    }else{
                        [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] view:self];
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }
        
    }
}

- (void) endCallNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"removipopupnotification"])
        NSLog (@"Successfully received the removipopupnotification notification!");
    [self removeFromSupperView];
}

- (void) warmTransferAcceptedNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"WarmTransferAccept"])
        NSLog (@"Successfully received the removipopupnotification notification!");
    [_btnSwitchMerge setEnabled:true];
    [_btnSwitchTransferNow setEnabled:true];
    [_btnSwitchCaller setEnabled:true];
    [_btnSwitchSubUser setEnabled:true];
    [_btnHangup setEnabled:true];
}

-(void) removeFromSupperView{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"removipopupnotification" object:nil];
    [_delegate endCallButtonEnableComplet];
    [self removeFromSuperview];
}


- (IBAction)btnHangupClicked:(id)sender {
    [self endConferenceCall:true actionOnUser:@""];
}

- (IBAction)btn_sub_user_click:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^{
        self->_lbl_sub_user.hidden = false;
        [self->_btn_sub_user setTitleColor:main_orrange_clolor_final forState:UIControlStateNormal];
        self->_lbl_sub_user.backgroundColor = main_orrange_clolor_final;
        self->_txt_search_text.text = @"";
        [self->_txt_search_text resignFirstResponder];
        self->_txt_search_text.placeholder = @"Search sub user";
        self->_Selected_tab_name = @"Subuser";
        [self->_tblSubUserList reloadData];
    } completion: ^(BOOL finished) {
        self->_lbl_contacts.hidden = true;
        [self->_btn_contacts setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self->_lbl_contacts.backgroundColor = [UIColor lightGrayColor];
    }];
}

- (IBAction)btn_contact_click:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^{
        self->_lbl_contacts.hidden = false;
        [self->_btn_contacts setTitleColor:main_orrange_clolor_final forState:UIControlStateNormal];
        self->_lbl_contacts.backgroundColor = main_orrange_clolor_final;
        self->_txt_search_text.text = @"";
        [self->_txt_search_text resignFirstResponder];
        self->_txt_search_text.placeholder = @"Search contact and add number";
        self->_Selected_tab_name = @"Contacts";
        [self->_tblSubUserList reloadData];
    } completion: ^(BOOL finished) {
        self->_lbl_sub_user.hidden = true;
        [self->_btn_sub_user setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self->_lbl_sub_user.backgroundColor = [UIColor lightGrayColor];
    }];
}
- (IBAction)txt_search_value_change:(UITextField *)sender
{
    NSLog(@"Subuser : Search : %@",_txt_search_text.text);
    if ([_Selected_tab_name isEqualToString:@"Subuser"])
    {
        if(subUsers.count != 0)
        {
            NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"fullName contains[c] %@",_txt_search_text.text];
            NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name]];
            NSArray *filter_date = [subUsers filteredArrayUsingPredicate:predicate_final];
            if(filter_date.count != 0)
            {
                Filter_subUsers = [[NSMutableArray alloc] init];
                [Filter_subUsers addObjectsFromArray:filter_date];
                [_tblSubUserList reloadData];
            }
            else
            {
                Filter_subUsers = subUsers;
                [_tblSubUserList reloadData];
            }
        }
    }
    if ([_Selected_tab_name isEqualToString:@"Contacts"])
    {
        NSString *num = [_txt_search_text.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
        num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        NSLog(@"Contacts : Contacts :: %@",num);
        NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"name contains[c] %@",_txt_search_text.text];
        
        
       //p  NSPredicate *predicate_number = [NSPredicate predicateWithFormat:@"number_int contains[c] %@",num];
        
        //-mixallcontact
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",num] ;
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num] ;
        
        NSPredicate *predicateNumber = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];
        
        NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name, predicateNumber]];
        
        NSArray *_filteredData1 = [Mixallcontact filteredArrayUsingPredicate:predicate_final];
//        NSLog(@"_filteredData : %@",_filteredData1);
        if(_filteredData1.count != 0)
        {
            FilterContact = [[NSMutableArray alloc] init];
            FilterContact = (NSMutableArray*)_filteredData1;
            [_tblSubUserList reloadData];
        }
        else
        {
            if ([_txt_search_text.text isEqualToString:@""])
            {
                FilterContact = [[NSMutableArray alloc] init];
                FilterContact = Mixallcontact;
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                             ascending:YES];
                NSArray *arr = [FilterContact sortedArrayUsingDescriptors:@[sortDescriptor] ];
                FilterContact = (NSMutableArray*)arr;
                [_tblSubUserList reloadData];
            } else {
                FilterContact = [[NSMutableArray alloc] init];
                if(_txt_search_text.text.length > 9)
                {
//                    if ([UtilsClass isValidNumber_withoutplus:num])
//                    {
                        NSLog(@"in ifff------%@",_txt_search_text.text);
                        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init ];
                        [dic setValue:@"" forKey:@"id"];
                        [dic setValue:@"" forKey:@"name"];
                    
                        //p [dic setValue:_txt_search_text.text forKey:@"number"];
                    NSMutableArray *numbersArraytmp = [[NSMutableArray alloc]init];
                    NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:_txt_search_text.text,@"number", nil];
                    [numbersArraytmp addObject:temp];
                    
                    
                    [dic setObject:numbersArraytmp forKey:@"numberArray"];
                        [FilterContact addObject:dic];
//                    }else{
//                        NSLog(@"in else------%@",_txt_search_text.text);
//                    }
                }
                [_tblSubUserList reloadData];
            }
        }
    }
}
- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        return [predicate evaluateWithObject:string];
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    
}

@end


