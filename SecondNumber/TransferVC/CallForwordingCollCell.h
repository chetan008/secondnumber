//
//  CallForwordingCollCell.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallForwordingCollCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;

@property (strong, nonatomic) IBOutlet UIView *viewCell;
@property (strong, nonatomic) IBOutlet UILabel *lblOnline;

//@property (strong, nonatomic) IBOutlet UIView *viewActive;

@property (strong, nonatomic) IBOutlet UIView *view_boder;

@end

NS_ASSUME_NONNULL_END
