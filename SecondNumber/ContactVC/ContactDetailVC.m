//
//  ContactDetailVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ContactDetailVC.h"
#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "AddEditContactVC.h"
#import "OnCallVC.h"
#import "Constant.h"
#import "NewsmsVC.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "GlobalData.h"
#import "UIViewController+LGSideMenuController.h"
@import NKVPhonePicker;

@interface ContactDetailVC ()<UITableViewDataSource,UITableViewDelegate,SecVSDelegate>
{
    NSMutableDictionary *dataContact;
    NSMutableDictionary *dicprint;
    NSString *phoneNumber;
    NSArray *purchase_number;
    WebApiController *obj;
    NSString *selected_fromnumner;
    NSString *selected_callToName;
    NSString *selected_callTonumber;
    NSString *selected_calltypes;
    UIView *Blur_view;
    NSMutableArray *details_array;
    NSString *index_value;
    NSMutableArray *CallHistoryArr;
    bool is_tbl_reload;
    bool is_title_added;
    
    NSMutableArray *numbersArray;
    NSMutableArray *timeList;
    
    NSString *remindToNumStr;
}

@end
NSString *fromAddEdit = @"0";
@implementation ContactDetailVC

@synthesize ContactDetailsDic;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //
    
    timeList =  [NSMutableArray arrayWithObjects:@"15 Minutes",@"30 Minutes",@"1 Hour",@"2 Hours",@"tomorrow",@"in a week", nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    index_value = @"";
    self.flag_num_selection = @"0";
    Mixallcontact = [[NSMutableArray alloc] init];
    details_array = [[NSMutableArray alloc] init];
    //     [self GetNumberFromContact];
    //     [self getCallHippoContactList];

    
    [self getNumbers];
    //[self contact_get];
    //[self view_design];

}
- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"conatct detail dict :: %@",ContactDetailsDic);
    
    numbersArray = [[NSMutableArray alloc]init];
 
    if (![fromAddEdit isEqualToString:@"1"]) {
         [self contact_get];

    }

    if (ContactDetailsDic[@"_id"]) {
         [self getContactDetail:[ContactDetailsDic valueForKey:@"_id"]];
    }
    else
    {
        numbersArray = [ContactDetailsDic valueForKey:@"numberArray"];
        [self view_design];
    }
   
    //[_tblDetail reloadData];
    
    is_tbl_reload = true;
    is_title_added = false;
    // details_array = [[NSMutableArray alloc] init];
    index_value = @"";
    selected_fromnumner = @"";
    selected_callToName= @"";
    selected_callTonumber= @"";
    selected_calltypes= @"";
    
}
-(void)call_history:(NSString*)skip number:(NSString*)number
{
    //    //NSLog(@"Api call : %@",details_array);
    if ([skip intValue] == 0)
    {
        CallHistoryArr = [[NSMutableArray alloc] init];
    }
    if ([skip intValue] > 0)
    {
        
    }
    else
    {
//        [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    }
    
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"";
    url = [NSString stringWithFormat:@"contactLogs/%@/%@/?skip=%@&limit=20",userId,number,skip];
    NSLog(@"Trush Calllogs : URL  :  %@%@ ",SERVERNAME,url);
    NSLog(@"authhhhh :: %@",[Default valueForKey:AUTH_TOKEN]);
    
    obj = [[WebApiController alloc] init];
    
    
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(login1:response:) andDelegate:self];
    
}
- (void)login1:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"Call_history : response1 : %@",response1);
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : call history : %@",response1);

        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSLog(@"responseee:: %@",response1);
            
            NSMutableArray *arr = response1[@"data"][@"contactLogs"];
            
            if(arr.count > 0)
            {

                if(is_title_added == false)
                {
                    NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] init];
                    [dic1 setValue:@"Title" forKey:@"Name"];
                    [dic1 setValue:[NSString stringWithFormat:@"Call history"] forKey:@"title"];
                    [details_array addObject:dic1];
                    is_title_added = true;
                }
                
                [details_array addObjectsFromArray:arr];
                is_tbl_reload = true;
                //            //NSLog(@"Response : details_array : %@",details_array);
                _tblDetail.delegate = self;
                _tblDetail.dataSource = self;
                [_tblDetail reloadData];
                
            }
            
          
            
        }
       
        
       if (details_array.count>0) {
                 
           
           NSArray *temp = [details_array valueForKey:@"title"];
           if ([temp containsObject:@"Call history"])
           {
                   _lblEmptyConversation.hidden = true;
           }
           else
           {
               _lblEmptyConversation.hidden = false;
               [self adjustEmptyConversationLable];
           }
       }
       else
       {
           _lblEmptyConversation.hidden = false;
           [self adjustEmptyConversationLable];

       }
        
    }
    
}
-(void)Foreground_Action:(NSNotification *)notification
{
    //NSLog(@"\n \n \n \n Trushang_code : Foreground_Action :");
    //    [self contact_get];
    
}
-(void)contact_get
{
    Mixallcontact = [[NSMutableArray alloc] init];
    _filteredData = [[NSMutableArray alloc] init];
    
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    _filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    
    NSArray *arr = [_filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
    _filteredData = (NSMutableArray*)arr;
    
    NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"name contains[c] %@",_contact_vc_selection_search_text];
    
    NSString *num = [_contact_vc_selection_search_text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    //NSPredicate *predicate_number = [NSPredicate predicateWithFormat:@"number_int contains[c] %@",_contact_vc_selection_search_text];
   // NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name, predicate_number]];
    
    NSPredicate *predicate_number2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num];
    
    NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name, predicate_number2]];
    
    NSArray *_filteredData1 = [Mixallcontact filteredArrayUsingPredicate:predicate_final];
    
    
    //NSLog(@"_filteredData : %@",_filteredData1);
    if(_filteredData1.count != 0)
    {
        _filteredData = [[NSMutableArray alloc] init];
        _filteredData = (NSMutableArray*)_filteredData1;
       // NSLog(@"_filteredData : %@",_filteredData);
        //NSLog(@"_contact_vc_selection_index : %ld",(long)_contact_vc_selection_index);
        
        ContactDetailsDic = [_filteredData objectAtIndex:_contact_vc_selection_index];
        
    }
    else
    {
        if([_filteredData objectAtIndex:_contact_vc_selection_index] != nil)
        {
            ContactDetailsDic = [_filteredData objectAtIndex:_contact_vc_selection_index];
        }
        
    }
    
    
    //    [self view_design];
}


-(void)view_design
{
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    [UtilsClass view_navigation_title:self title:@"Contact Details" color:UIColor.whiteColor];
    _imgUser.layer.cornerRadius = _imgUser.frame.size.width / 2;
    _imgUser.clipsToBounds = true;
    _imgUser.layer.borderWidth = 0.6;
    _imgUser.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    _btnCall.layer.cornerRadius = _btnCall.frame.size.width / 2;
    _btnCall.clipsToBounds = true;
    _btnCall.layer.borderWidth = 0.6;
    _btnCall.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [UtilsClass button_shadow_boder:_btnCall];
    
    _btnSMS.layer.cornerRadius = _btnSMS.frame.size.width / 2;
    _btnSMS.clipsToBounds = true;
    _btnSMS.layer.borderWidth = 0.6;
    _btnSMS.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [UtilsClass button_shadow_boder:_btnSMS];
    
    _btnEdit.layer.cornerRadius = _btnEdit.frame.size.width / 2;
    _btnEdit.clipsToBounds = true;
    _btnEdit.layer.borderWidth = 0.6;
    _btnEdit.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [UtilsClass button_shadow_boder:_btnEdit];
    
    _btnDelete.layer.cornerRadius = _btnDelete.frame.size.width / 2;
    _btnDelete.clipsToBounds = true;
    _btnDelete.layer.borderWidth = 0.6;
    _btnDelete.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [UtilsClass button_shadow_boder:_btnDelete];
    
    _btn_contact_call.layer.cornerRadius = _btn_contact_call.frame.size.width / 2;
    _btn_contact_call.clipsToBounds = true;
    _btn_contact_call.layer.borderWidth = 0.6;
    _btn_contact_call.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [UtilsClass button_shadow_boder:_btn_contact_call];
    
    _btn_contact_sms.layer.cornerRadius = _btn_contact_sms.frame.size.width / 2;
    _btn_contact_sms.clipsToBounds = true;
    _btn_contact_sms.layer.borderWidth = 0.6;
    _btn_contact_sms.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [UtilsClass button_shadow_boder:_btn_contact_sms];
    
    
    
    self.viewTopDetail.backgroundColor = [UIColor whiteColor];
    self.viewTopDetail.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    self.viewTopDetail.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.viewTopDetail.layer.shadowOpacity = 1.0f;
    self.viewTopDetail.layer.shadowRadius = 0.0f;
    self.viewTopDetail.layer.masksToBounds = NO;
    self.viewTopDetail.layer.cornerRadius = 0.0f;
    
    
     //    if ([fromAddEdit isEqualToString:@"0"]) {
        
        _lblUserName.text = [ContactDetailsDic valueForKey:@"name"];
        //    if ([[ContactDetailsDic valueForKey:@"number"] isEqualToString:@"(null)"] || [[ContactDetailsDic valueForKey:@"number"] isEqualToString:@""]){
        //        _lblNumber.text = @"";
        //    }else{
        
        //    }
        
      /*  if ([ContactDetailsDic valueForKey:@"number"] == nil || [ContactDetailsDic valueForKey:@"number"] == (id)[NSNull null]) {
            // nil branch
            _lblNumber.text = @"";
        } else {
            // category name is set
            _lblNumber.text = [ContactDetailsDic valueForKey:@"number"];
        }*/
        
        _view_callhippo_view.hidden = true;
        _view_contact_view.hidden = true;
        
        //    _lblEdit.hidden = true;
        //    _lblDelete.hidden = true;
        //    _btnDelete.hidden = true;
        //    _btnEdit.hidden = true;
        //NSLog(@"ContactDetailsDic : %@",ContactDetailsDic);
        
        dicprint = [[NSMutableDictionary alloc] init];
        details_array = [[NSMutableArray alloc] init];
        [dicprint removeAllObjects];
        [details_array removeAllObjects];
        //NSLog(@"Viewdesing : details_array : %@",details_array);
        
        //pri
           
        
        if (ContactDetailsDic[@"_id"])
        {
            if(![[ContactDetailsDic valueForKey:@"_id"] isEqualToString:@""])
            {
                
                _view_callhippo_view.hidden = false;
                
                if(![[ContactDetailsDic valueForKey:@"email"] isEqualToString:@""])
                {
                    dicprint = [[NSMutableDictionary alloc] init];
                    [dicprint setValue:@"email" forKey:@"Name"];
                    [dicprint setValue:[NSString stringWithFormat:@"%@",[ContactDetailsDic valueForKey:@"email"]] forKey:@"email"];
                    [details_array addObject:dicprint];
                }
                if(![[ContactDetailsDic valueForKey:@"company"] isEqualToString:@""])
                {
                    dicprint = [[NSMutableDictionary alloc] init];
                    [dicprint setValue:@"company" forKey:@"Name"];
                    [dicprint setValue:[NSString stringWithFormat:@"%@",[ContactDetailsDic valueForKey:@"company"]] forKey:@"company"];
                    [details_array addObject:dicprint];
                }
                
    //            if ([ContactDetailsDic valueForKey:@"number"] == nil || [ContactDetailsDic valueForKey:@"number"] == (id)[NSNull null])
                if ([[ContactDetailsDic objectForKey:@"numberArray"] count]>0)
                
                {
                    
                    NSString *number = [[[ContactDetailsDic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                    
                    if([number length] < 6)
                    {
                        _btnSMS.enabled = false;
                        _btn_contact_sms.enabled = false;
                        
                    }
                    else
                    {
                        _btnSMS.enabled = true;
                        _btn_contact_sms.enabled = true;
                    }
                    
                    number = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    [self call_history:@"0" number:number];
                }
                
                
                
                            _tblDetail.delegate = self;
                            _tblDetail.dataSource = self;
                            [_tblDetail reloadData];
            }
            
            
        }
        else
        {
            
//            if ([ContactDetailsDic valueForKey:@"number"] == nil || [ContactDetailsDic valueForKey:@"number"] == (id)[NSNull null])
//            {
//
//            }
//            else
//            {
//            if ([[ContactDetailsDic objectForKey:@"numberArray"] count]>0)
//
//            {
//                NSString *number = [[[ContactDetailsDic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                
            if ([[ContactDetailsDic objectForKey:@"numberArray"] count]>0)
            {
            [self call_history:@"0" number:[[[ContactDetailsDic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"]];
            
            /*
             working for single device contact
             NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[[[ContactDetailsDic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"],@"number",nil];

            [numbersArray addObject:temp];*/
//            [_tblDetail reloadData];
            if([[[[ContactDetailsDic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] length] < 6)
            {
                _btnSMS.enabled = false;
                _btn_contact_sms.enabled = false;
            }
            else
            {
                _btnSMS.enabled = true;
                _btn_contact_sms.enabled = true;
            }
            
            }
//                NSString *number = [ContactDetailsDic valueForKey:@"number"];
//                number = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
                
            //}
            
          //p  _view_contact_view.hidden = false;
            
            _tblDetail.delegate = self;
            _tblDetail.dataSource = self;
            [_tblDetail reloadData];
        }
   
   
    
}
-(void)fillContactDetails
{
   
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _tbl_reminderpopup)
    {
        return timeList.count;
    }
    else if (tableView == _tbl_number_selection)
    {
        //        //NSLog(@"purchase_number.count  : %lu",(unsigned long)purchase_number.count);
        return purchase_number.count;
    }
    else
    {
        return details_array.count + numbersArray.count ;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _tbl_number_selection)
    {
        Tblcell *cell = [_tbl_number_selection dequeueReusableCellWithIdentifier:@"cell123"];
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        cell.img_select_con_num.image = [UIImage imageNamed:imagename];
        cell.lbl_select_con_name.text = contactName;
        cell.lbl_select_con_number.text  = phoneNumber;
        
        _tbl_number_selection.rowHeight = 64.0;
        [cell layoutIfNeeded];
        [cell.contentView layoutIfNeeded];
        if (index_value == [numberdic valueForKey:@"_id"])
        {
            cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:233.0/255.0 blue:230.0/255.0 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = UIColor.clearColor;
        }
        
        return cell;
    }
    else if (tableView == _tbl_reminderpopup)
    {
        Tblcell *cell = [_tbl_reminderpopup dequeueReusableCellWithIdentifier:@"Tblcell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblCredit.text = timeList[indexPath.row];
        return cell;
    }
    else
    {
       
        Tblcell *cell = [_tblDetail dequeueReusableCellWithIdentifier:@"cell123"];
        
        if (indexPath.row < numbersArray.count) {
            
            Tblcell *cell = [_tblDetail dequeueReusableCellWithIdentifier:@"NumbersCell"];
            
           // [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
            
           
            cell.lblNumber.text = [[numbersArray objectAtIndex:indexPath.row] valueForKey:@"number"];
            
            if ([GlobalData isNumberBlacklisted:cell.lblNumber.text]) {
                
                [cell.numberBlacklistBtn setImage:[UIImage imageNamed:@"icon_unblock"] forState:UIControlStateNormal];
                cell.numberReminderBtn.enabled = false;
                
            }
            else
            {
                [cell.numberBlacklistBtn setImage:[UIImage imageNamed:@"icon_block"] forState:UIControlStateNormal];
                
                cell.numberReminderBtn.enabled = true;

            }
            
            if([[[numbersArray objectAtIndex:indexPath.row] valueForKey:@"number"] length] < 6)
            {
                cell.numberSmsBtn.hidden = true;
                cell.numberBlacklistBtn.hidden = true;
            }
            else
            {
                cell.numberSmsBtn.hidden = false;
                cell.numberBlacklistBtn.hidden = false;

            }
            
            if ([Default boolForKey:kIsNumberMask] == true) {
                if([[[numbersArray objectAtIndex:indexPath.row] valueForKey:@"number"] length] > 6)
                {
                cell.lblNumber.text = [UtilsClass get_masked_number:[[numbersArray objectAtIndex:indexPath.row] valueForKey:@"number"]];
                }
            }
            
            cell.numberCallBtn.tag = indexPath.row;
            cell.numberSmsBtn.tag = indexPath.row;
            cell.numberBlacklistBtn.tag = indexPath.row;
            cell.numberReminderBtn.tag = indexPath.row;
            
            
            
            [cell.numberCallBtn addTarget:self action:@selector(btn_call_click:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.numberSmsBtn addTarget:self action:@selector(btn_sms_click:) forControlEvents:UIControlEventTouchUpInside];
            
         //    [cell.numberReminderBtn addTarget:self action:@selector(btn_addreminder_click:) forControlEvents:UIControlEventTouchUpInside];
            
             [cell.numberBlacklistBtn addTarget:self action:@selector(btn_blacklist_click:) forControlEvents:UIControlEventTouchUpInside];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]init];
            tapGesture.numberOfTapsRequired = 1;
            [cell.numberReminderBtn addGestureRecognizer:tapGesture];
            cell.numberReminderBtn.tag = indexPath.row;
            [tapGesture addTarget:self action:@selector(btn_addreminder_click:)];
            
            return cell;

        }
        else
        {
            
           // [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            
           if(details_array.count  > 0)
            {
                
                NSUInteger index = indexPath.row - numbersArray.count;
                
                NSDictionary *dic = [details_array objectAtIndex:index];
                if([[dic valueForKey:@"Name"] isEqualToString:@"email"])
                {
                    Tblcell *cell = [_tblDetail dequeueReusableCellWithIdentifier:@"cell123"];
                    cell.img_select_con_num.image = [UIImage imageNamed:@"calldetails_mail"];
                    cell.lbl_select_con_name.text = [NSString stringWithFormat:@"%@",[ContactDetailsDic valueForKey:@"email"]];
                    _tblDetail.rowHeight = 64.0;
                    return cell;
                }
                else if([[dic valueForKey:@"Name"] isEqualToString:@"company"])
                {
                    Tblcell *cell = [_tblDetail dequeueReusableCellWithIdentifier:@"cell123"];
                    cell.img_select_con_num.image = [UIImage imageNamed:@"calldetails_company"];
                    cell.lbl_select_con_name.text = [NSString stringWithFormat:@"%@",[ContactDetailsDic valueForKey:@"company"]];
                    _tblDetail.rowHeight = 64.0;
                    return cell;
                }
                else if([[dic valueForKey:@"Name"] isEqualToString:@"Title"])
                {
                    Tblcell *cell = [_tblDetail dequeueReusableCellWithIdentifier:@"cell123"];
                    cell.img_select_con_num.image = [UIImage imageNamed:@"menu_reminder"];
                    cell.lbl_select_con_name.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"title"]];
                    _tblDetail.rowHeight = 50.0;
                    return cell;
                }
                else
                {
                    
                        Tblcell *cell1 = [_tblDetail dequeueReusableCellWithIdentifier:@"cell321"];
                        //             cell1.con_det_img_call_status.image = [UIImage imageNamed:@"calldetails_company"];
                        cell1.con_det_lbl_call_date.text = [NSString stringWithFormat:@"%@ %@",[dic valueForKey:@"date"],[dic valueForKey:@"time"]];
                        cell1.con_det_lbl_call_time.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"callDuration"]];
                        
                        cell1.con_det_lbl_call_status.text = [NSString stringWithFormat:@"via %@ | %@",[dic valueForKey:@"DepartmentName"] , [dic valueForKey:@"callStatus"]];
                        
                        cell1.con_det_lbl_call_username.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"callerName"]];
                        
                        
                        
                        if ([[dic valueForKey:@"callType"]  isEqual: @"Incoming"])
                        {
                            
                            if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"rejected"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"incoming_Complete"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"missedincoming"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"incoming_Complete"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Unavailable"]) {
                                
                            }else{
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"incoming"];
                            }
                        }
                        else
                        {
                            if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"rejected"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"missedout"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"transfer"]) {
                                
                            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Cancelled"]) {
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"missedout"];
                            }else{
                                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
                            }
                            
                            
                        }
                        _tblDetail.rowHeight = 64.0;
                        return cell1;
                         
                    
                    
                }
                
                
                
            }
            
        
    }
       
        
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _tbl_number_selection)
    {
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        //NSLog(@"data : %@",numberdic);
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        [Default setValue:phoneNumber forKey:SELECTEDNO];
        [Default setValue:contactName forKey:Selected_Department];
        
        NSString *imagename = [[numberdic  objectForKey:@"shortName"] lowercaseString];
        [Default setValue:imagename forKey:Selected_Department_Flag];
        
        [UtilsClass make_outgoing_call_validate:self callfromnumber:selected_fromnumner ToName:selected_callToName ToNumber:selected_callTonumber calltype:selected_calltypes Mixallcontact:Mixallcontact];
        
        
        
        
        [self view_selectnumber_down];
    }
    else if (tableView == _tbl_reminderpopup)
    {
        Tblcell *cell = [_tbl_reminderpopup cellForRowAtIndexPath:indexPath];
        
        if(indexPath.row == 0){
            [self setReminder:@"15" remNum:remindToNumStr];
        }else if(indexPath.row == 1){
            [self setReminder:@"30" remNum:remindToNumStr];
        }else if(indexPath.row == 2){
            [self setReminder:@"1" remNum:remindToNumStr];
        }else if(indexPath.row == 3){
            [self setReminder:@"2" remNum:remindToNumStr];
        }else if(indexPath.row == 4){
            [self setReminder:@"24" remNum:remindToNumStr];
        }else if(indexPath.row == 5){
            [self setReminder:@"7" remNum:remindToNumStr];
        }
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    

    float  height = self.tblDetail.frame.size.height;
    float contentYoffset = aScrollView.contentOffset.y;
    float distanceFromBottom = aScrollView.contentSize.height - contentYoffset;
    //    //NSLog(@" distanceFromBottom : %f",distanceFromBottom);
    //    //NSLog(@" height : %f",height);
    
    float halfpoint = aScrollView.contentSize.height / 2;
    if (halfpoint < contentYoffset ) {
        //NSLog(@" you reached end of the table");
        [self scroll_api];
    }else{
        
    }
}

-(void)scroll_api
{
    if(is_tbl_reload == true)
    {
        //NSLog(@"Api call Counter %@",[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]]);
        is_tbl_reload = false;
        //        [self GetCallLogs:[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]]];
        
        
//        if ([ContactDetailsDic valueForKey:@"number"] == nil || [ContactDetailsDic valueForKey:@"number"] == (id)[NSNull null])
//        {
//
//        }
//        else
//        {
//            NSString *number = [ContactDetailsDic valueForKey:@"number"];
        if ([[ContactDetailsDic objectForKey:@"numberArray"] count]>0)
        
        {
            NSString *number = [[[ContactDetailsDic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
            number = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
            [self call_history:[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]] number:number];
        }
    }
    else
    {
        
    }
}

- (IBAction)btn_back_click:(UIBarButtonItem *)sender
{
    [[self navigationController] popViewControllerAnimated:true];
    
}

- (IBAction)btn_edit_click:(UIButton *)sender
{
    if ([Default boolForKey:kIsNumberMask] == true) {
        [UtilsClass makeToast:@"number can not be edited as number masking is on , please contact your admin ."];
    }
    else
    {
    AddEditContactVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddEditContactVC"];
    vc.delegate = self;
    vc.ContactDetailsDic = ContactDetailsDic;
    vc.controller = @"edit";
    [[self navigationController] pushViewController:vc animated:YES];
    }
}

- (IBAction)btn_sms_click:(UIButton *)sender
{
    //pri NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];

    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        
        NSString *numbVer = [Default valueForKey:ISDummyNumber];
        int num = [numbVer intValue];
        if (num == 1)
        {
            
            
           /* if(![[ContactDetailsDic valueForKey:@"_id"] isEqualToString:@""])
            {
                
                callTonumber = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag] valueForKey:@"number"];
            }
            else
            {
               
                callTonumber = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag] valueForKey:@"number"];
            }*/
            
           /*
            working for single device contact
            
            if (ContactDetailsDic[@"_id"]) {
                
                if ([[[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag]valueForKey:@"number"] isEqualToString:@""] ) {
                    
                    [UtilsClass showAlert:@"Please enter valid number." contro:self];
                            }
                else {
                                
                    NSString *smsDisp = [Default valueForKey:kSMSRIGHTS];
                    int num = [smsDisp intValue];
                    if (num == 0) {
                        NewsmsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"NewsmsVC"];
                        vc.controller = @"dialerVC";
                        vc.numberfromothercontroller = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag]valueForKey:@"number"];
                        vc.namefromothercontroller = [ContactDetailsDic valueForKey:@"name"];
                        vc.strFromNumber = @"";
                        vc.VcFrom = @"ContactDetailVC";
                        [[self navigationController] pushViewController:vc animated:YES];
                    }else{
                                        [UtilsClass showAlert:kSMSMODUALMSG contro:self];
                                    }
                              //  }
                            }
            }
            else
            {
                if ([[ContactDetailsDic valueForKey:@"number"] isEqualToString:@""] ) {
                    
                    [UtilsClass showAlert:@"Please enter valid number." contro:self];
                            }
                else {
                                
                    NSString *smsDisp = [Default valueForKey:kSMSRIGHTS];
                    int num = [smsDisp intValue];
                    if (num == 0) {
                        NewsmsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"NewsmsVC"];
                        vc.controller = @"dialerVC";
                        vc.numberfromothercontroller = [ContactDetailsDic valueForKey:@"number"];
                        vc.namefromothercontroller = [ContactDetailsDic valueForKey:@"name"];
                        vc.strFromNumber = @"";
                        vc.VcFrom = @"ContactDetailVC";
                        [[self navigationController] pushViewController:vc animated:YES];
                    }else{
                                        [UtilsClass showAlert:kSMSMODUALMSG contro:self];
                                    }
                              //  }
                            }
            }*/
            
            NSString *numberStr = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag]valueForKey:@"number"];
            
            if ([numberStr isEqualToString:@""] || numberStr == nil || numberStr == (id)[NSNull null]) {
                
                 [UtilsClass showAlert:@"Please enter valid number." contro:self];
            }
            else
            {
                NSString *smsDisp = [Default valueForKey:kSMSRIGHTS];
                int num = [smsDisp intValue];
                if (num == 0) {
                    NewsmsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"NewsmsVC"];
                    vc.controller = @"dialerVC";
                    vc.numberfromothercontroller = numberStr;
                    vc.namefromothercontroller = [ContactDetailsDic valueForKey:@"name"];
                    vc.strFromNumber = @"";
                    vc.VcFrom = @"ContactDetailVC";
                    [[self navigationController] pushViewController:vc animated:YES];
                }else{
                    [UtilsClass showAlert:kSMSMODUALMSG contro:self];
                                }
            }
            
            
            
        }else {
            [UtilsClass showAlert:kDUMMYNUMBERMSG contro:self];
        }
        
    }else{
        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
    }

    
}



- (IBAction)btn_delete_clicked:(id)sender {
    
    if ([Default boolForKey:kIsNumberMask] == true) {
           [UtilsClass makeToast:@"number can not be edited as number masking is on , please contact your admin ."];
       }
       else
       {
           [self deleteContact];
       }
}


- (IBAction)btn_call_click:(UIButton *)sender
{
    [Default removeObjectForKey:@"extraHeader"];
    
//    if ([ContactDetailsDic valueForKey:@"number"] == nil || [ContactDetailsDic valueForKey:@"number"] == (id)[NSNull null]) {
    

    NSString *numberStr =[[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag]valueForKey:@"number"];
    
    if ([numberStr isEqualToString:@""] || numberStr == nil || numberStr == (id)[NSNull null]) {
        
    
//    if ([[[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag]valueForKey:@"number"] isEqualToString:@""]) {
        
    
        [UtilsClass showAlert:@"Please enter valid number." contro:self];
    } else {
       
        //pri NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
        
        NSData *data = [Default valueForKey:PURCHASE_NUMBER];
        NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSString *numbVer = [Default valueForKey:SELECTED_NUMBER_VERIFY];
        NSString *callfromnumber = [Default valueForKey:SELECTEDNO];
        NSString *callToName= @"";
        NSString *callTonumber = @"";
        NSString *calltypes = OUTGOING;
        
        /*
         
         working for single device contact
         
        if (ContactDetailsDic[@"_id"])
        {
            if(![[ContactDetailsDic valueForKey:@"_id"] isEqualToString:@""])
            {
                callToName = [ContactDetailsDic valueForKey:@"name"];
                
                //callTonumber = [ContactDetailsDic valueForKey:@"number"];
                callTonumber = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag] valueForKey:@"number"];
            }
            else
            {
                callToName = [ContactDetailsDic valueForKey:@"name"];
               // callTonumber = [ContactDetailsDic valueForKey:@"number"];
                callTonumber = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag] valueForKey:@"number"];
            }
        }
        else
        {
            callToName = [ContactDetailsDic valueForKey:@"name"];
            //callTonumber = [ContactDetailsDic valueForKey:@"number"];
            callTonumber = [ContactDetailsDic  valueForKey:@"number"];
        }
         
        */
        
        callToName = [ContactDetailsDic  valueForKey:@"name"];
        callTonumber = [[[ContactDetailsDic objectForKey:@"numberArray"] objectAtIndex:sender.tag] valueForKey:@"number"];
        
        callTonumber = [callTonumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
        if(purchase_number.count > 0 || callTonumber.length == 3 || callTonumber.length == 4)
        {
            callTonumber = [NSString stringWithFormat:@"+%@",callTonumber];
            
            
            
            NSString *numbVer1 = [Default valueForKey:ISDummyNumber];
            int num = [numbVer1 intValue];
            if (num == 1 || callTonumber.length == 5  || callTonumber.length == 4)
            {
                int num = [numbVer intValue];
                if (num == 1  || callTonumber.length == 5 || callTonumber.length == 4) {
                    NSString *credit = [Default valueForKey:CREDIT];
                    float cre = [credit floatValue];
                    if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                        switch ([[AVAudioSession sharedInstance] recordPermission]) {
                            case AVAudioSessionRecordPermissionGranted:
                            {
                                selected_fromnumner = callfromnumber;
                                selected_callToName= callToName;
                                selected_callTonumber= callTonumber;
                                selected_calltypes= calltypes;
                                
                                NSString *numbVer = [Default valueForKey:IS_AutoSwitch];
                                int num = [numbVer intValue];
                                if (num == 1)
                                {
                                    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
                                    txtText.text = @"";
                                    [txtText insertText:selected_callTonumber];
                                    NSString *code = txtText.code ? txtText.code : @"";
                                    NSString *Country_ShotName = txtText.country.countryCode ? txtText.country.countryCode : @"";
                                    NSString *Country_FullName = txtText.country.name ? txtText.country.name : @"";
                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.shortName  contains[c] %@",Country_ShotName];
                                    NSArray *filteredData = [purchase_number filteredArrayUsingPredicate:predicate];
                                    
                                    //pri
                                    [Default setValue:selected_callTonumber forKey:klastDialNumber];
                                    [Default setValue:txtText.country.name forKey:klastDialCountryName];
                                    [Default setValue:[NSString stringWithFormat:@"+%@",txtText.code] forKey:klastDialCountryCode];
                                                                     
//                                    NSLog(@"last dial code default:: %@",[Default valueForKey:klastDialCountryCode]);
//                                    NSLog(@"last dial number default:: %@",[Default valueForKey:klastDialNumber]);
//                                    NSLog(@"last dial country default:: %@",[Default valueForKey:klastDialCountryName]);
                                    
                                    if (filteredData.count != 0)
                                    {
                                        NSDictionary *dic1 = [filteredData objectAtIndex:0];
                                        NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                                        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
                                        NSString *contactName = [numberdic objectForKey:@"contactName"];
                                        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                                        index_value = [numberdic objectForKey:@"_id"];
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                if(callTonumber.length == 4 || callTonumber.length == 5)
                                {
                                    [UtilsClass make_outgoing_call_validate:self callfromnumber:selected_fromnumner ToName:selected_callToName ToNumber:selected_callTonumber calltype:selected_calltypes Mixallcontact:Mixallcontact];
                                }else{
                                    [_tbl_number_selection reloadData];
                                    [self view_selectnumber_up];
                                }
                                break;
                            }
                            case AVAudioSessionRecordPermissionDenied:
                            {
                                [self micPermiiton];
                                break;
                            }
                            case AVAudioSessionRecordPermissionUndetermined:
                                break;
                            default:
                                break;
                        }}else {
                            [UtilsClass showAlert:kCREDITLAW contro:self];
                        }
                }else{
                    [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                }
            }
            else
            {
                [UtilsClass showAlert:kDUMMYNUMBERMSG contro:self];
            }
        }else
        {
            [UtilsClass showAlert:kNUMBERASSIGN contro:self];
        }
    }
}


-(void)Delete{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *contactID = [ContactDetailsDic valueForKey:@"_id"];
    NSString *url = [NSString stringWithFormat:@"%@%@",GETCONTACT_URL,contactID];
    
    NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
    [newdic setValue:[ContactDetailsDic valueForKey:@"_id"] forKey:@"_id"];
  //  [[GlobalData sharedGlobalData] edit_contact:1 newdic:newdic olddic:newdic title:@"delete"];
    
    [Contact deleteFromDB:[ContactDetailsDic valueForKey:@"_id"]];
    [[GlobalData sharedGlobalData] editCHcontactToDBArray:newdic title:@"delete"];
    
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_DELETE_RAW:url andParams:nil SuccessCallback:@selector(login:response:) andDelegate:self];
    
    
    
}


- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"Contact_API_Response : %@",response1);
    [Processcall hideLoadingWithView];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : delete contact : %@",response1);

        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //        [UtilsClass showAlert:@"Done" contro:self];
            [[self navigationController] popViewControllerAnimated:true];
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}

- (void)secVCDidDismisWithData:(NSMutableDictionary *)data{
    //NSLog(@"delegate called chetanQ");
    fromAddEdit = @"1";
    [ContactDetailsDic removeAllObjects];
    ContactDetailsDic = [[NSMutableDictionary alloc] init];
    ContactDetailsDic = data;
    
//    numbersArray = [ContactDetailsDic valueForKey:@"numberArray"];
//    [self view_design];

}

- (void)deleteContact
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:kAlertTitle
                                 message:@"Are You Sure Want to Delete Contact!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        //Handle your yes please button action here
        [self Delete];
    }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
        //Handle no, thanks button
    }];
    
    //Add your buttons to alert controller
    
    
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:NO completion:nil];
}
-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                //NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Phone Contact
-(void)GetNumberFromContact
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        //NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
            // Contact one each function block is executed whenever you get
            NSString *phoneNumber = @"";
            if( contact.phoneNumbers)
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            
            //             //NSLog(@"phoneNumber = %@", phoneNumber);
            //             //NSLog(@"givenName = %@", contact.givenName);
            //             //NSLog(@"familyName = %@", contact.familyName);
            //             //NSLog(@"email = %@", contact.emailAddresses);
            
            
            NSString *FullName = [NSString stringWithFormat:@"%@%@",contact.givenName,contact.familyName];
            NSString *Number = [NSString stringWithFormat:@"%@",phoneNumber];
            if([Number isEqualToString:@"(null)"])
            {
                Number = @"";
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:FullName forKey:@"name"];
            [dic setObject:Number forKey:@"number"];
            [arrContactList addObject:contact];
            [Mixallcontact addObject:dic];
        }];
        
        //        //NSLog(@"arr contact : %lu",(unsigned long)arrContactList.count);
        
    }
}
#pragma mark - CallHippo Contact
-(void)getCallHippoContactList
{
    
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contact",userId];
    
    //NSLog(@"URL Contact check : %@",aStrUrl);

    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallHippoContactListresponse:response:) andDelegate:self];
}
- (void)getCallHippoContactListresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"getCallHippoContactListresponse : %@",response1);
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            arrcontact = [response1 valueForKey:@"data"];
            [Mixallcontact addObjectsFromArray:arrcontact];
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
    
}
-(void) view_selectnumber_up {
    
    
    if ([self.flag_num_selection isEqual : @"0" ])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height-64;
        Blur_view = [[UIView alloc] init];
        Blur_view.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
        Blur_view.backgroundColor = UIColor.blackColor;
        Blur_view.alpha = 0.7;
        [self.view insertSubview:Blur_view belowSubview:_view_number_selection];
        
        
        self.flag_num_selection = @"1";
        [self updateViewConstraints];
        self->_view_nmbr_selection_bottomConstraint.constant = 0;
       
    }
}
-(void)updateViewConstraints
{
    
    if ([self.flag_num_selection isEqual : @"1"])
    {
        _tbl_nmbr_selection_heightConstraint.constant = _tbl_number_selection.contentSize.height ;
       
    }
    else
    {
        _tbl_nmbr_selection_heightConstraint.constant = 0;
    }
     [super updateViewConstraints];
}
-(void) view_selectnumber_down
{
    if ([self.flag_num_selection isEqual : @"1" ])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        [Blur_view removeFromSuperview];
//
        
        self.flag_num_selection = @"0";
        [self updateViewConstraints];
        self->_view_nmbr_selection_bottomConstraint.constant = 100;
    }
    
     
       
}

- (void)handleTap:(UITapGestureRecognizer *)sender
{
    if ([self.flag_num_selection  isEqual: @"0"]) {
        [self view_selectnumber_up];
    }
    else {
        [self view_selectnumber_down];
    }
}


-(void)getNumbers
{
    purchase_number = [[NSArray alloc]init];
    //pri purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        self.tbl_number_selection.delegate = self;
        self.tbl_number_selection.dataSource = self;
        [self.tbl_number_selection reloadData];
    }
       
}

- (void)getNumbers:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //    [Processcall hideLoadingWithView];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //NSLog(@"Numbes : %@",response1);
            NSDictionary*dic = [response1 valueForKey:@"data"];
            purchase_number = [dic valueForKey:@"numbers"];
            
            
            
            self.tbl_number_selection.delegate = self;
            self.tbl_number_selection.dataSource = self;
            [self.tbl_number_selection reloadData];
            
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    [self.view endEditing:true];
    
    
    [self view_selectnumber_down];
    
}

-(void)numberCallBtnClick:(id)sender
{
    
}

-(void)numberSmsBtnClick:(id)sender
{
    
}

-(void)getContactDetail:(NSString *)contactId
{
     [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
        NSString *url = [NSString stringWithFormat:@"%@%@",GETCONTACT_URL,contactId];
        
    
        //NSLog(@"url : %@%@",SERVERNAME,url);
        
        
        obj = [[WebApiController alloc] init];
        
        
         [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(getConatctDetail:response:) andDelegate:self];
}

- (void)getConatctDetail:(NSString *)apiAlias response:(NSData *)response
{
    
    [Processcall hideLoadingWithView];
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
        
    //NSLog(@"Contact_API_Response : %@",response1);


    if([apiAlias isEqualToString:Status_Code])
    {
           // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : contact detail : %@",response1);

        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            ContactDetailsDic = [[NSMutableDictionary alloc]init];
            ContactDetailsDic = [response1 objectForKey:@"data"];
            
            
            numbersArray = [ContactDetailsDic objectForKey:@"numberArray"];
            
            
            
            //[self fillContactDetails];
            [self view_design];

        }
        
    }

}

-(void)adjustEmptyConversationLable
{
    long height = (details_array.count + numbersArray.count) * 64;
    
    _emptyLblTopConstraint.constant = height + 20;
    
}

#pragma  mark - reminder methods

- (void)btn_addreminder_click:(UITapGestureRecognizer *)touch
{
//    CGPoint pt = [[[sender gestureRecognizers] ]  locationInView: s];
//    CGFloat percentage = pt.x / s.bounds.size.width;
    
    //NSLog(@"sender :: %@",sender.gestureRecognizers);
    
    UIButton *btn =(UIButton *) [touch view];
    NSLog(@"btn tag :: %ld",(long)btn.tag);
    
     //Tblcell *cell = [_tblDetail cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.tag inSection:0]];
    //remindToNumStr =  cell.lblNumber.text;
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    remindToNumStr = [[numbersArray objectAtIndex:path.row] valueForKey:@"number"];
    
    
    NSLog(@"remind to number : %@",remindToNumStr);
    
    NSString *numbVer = [Default valueForKey:IS_CALL_PLANNER];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner -------------- %@",numbVer);
    
    if (num == 1) {
        
        CGFloat x = [touch locationInView:self.view].x;
        CGFloat y = [touch locationInView:self.view].y;
        
        _tbl_reminderpopup.frame = CGRectMake(x-_tbl_reminderpopup.frame.size.width, y+10, _tbl_reminderpopup.frame.size.width, _tbl_reminderpopup.frame.size.height);
        
        NSLog(@"touch location :: %f, %f",x,y);
        
        if (_tbl_reminderpopup.hidden == true) {
            
            _tbl_reminderpopup.hidden = false;
            [self.view bringSubviewToFront:_tbl_reminderpopup];
          
        }
        else
        {
            _tbl_reminderpopup.hidden = true;
        }
        
    }
   
        else
        {
             [UtilsClass showAlert:@"Call Reminder is not available in your plan Please Upgrade your plan" contro:self];
        }
    
    
}
-(void)setReminder:(NSString *)reminderType remNum:(NSString *)reminderNumber {
    
    NSString *parentId = [Default valueForKey:PARENT_ID];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"createCallReminder";
    /*
     "":"",
     "reminderNumber":"13093265247",
     "parentId":"5b977a8df19c7c1724650608",
     "createdById":"5b977a8df19c7c1724650608",
     "reminderType":"1",
     "deviceType":"ios"
     
     */
    NSDictionary *passDict = @{@"networkStrengthRandomString":@"",
                               @"reminderNumber":reminderNumber,
                               @"parentId":parentId,
                               @"createdById":userId,
                               @"reminderType":reminderType,
                               @"deviceType":@"ios"
                               };
    NSLog(@"Dictonary ****** : %@",passDict);

    
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(setreminderResponse:response:) andDelegate:self];
}

- (void)setreminderResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"REMINDER SET RESPONSE: %@",response1);
    [Processcall hideLoadingWithView];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Encrypted Response : set reminder : %@",response1);

    if ([[response1 valueForKey:@"success"] integerValue] == 1)
    {
        [UtilsClass makeToast:KRIMINDERSET];
        
        //notification to load call logs
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"newReminderSet_notification" object:self userInfo:nil];
        
         _tbl_reminderpopup.hidden = true;
        
    }
    
   // NSLog(@"TRUSHANG : STATUSCODE **************17  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
    
}
#pragma mark - blacklist methods

- (IBAction)btn_blacklist_click:(UIButton *)sender
{
    if([[Default valueForKey:kIsBlackList] boolValue] == true)
    {
        if([[sender imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"icon_block"]])
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:kAlertTitle
                                         message:@"Are you sure you want to add this number to blacklist?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
                
                
               // Tblcell *cell = [self->_tblDetail cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sender tag] inSection:0]];
               
              // [self addToBlackList:cell.lblNumber.text];
                NSIndexPath *path = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
                [self addToBlackList:[[self->numbersArray objectAtIndex:path.row] valueForKey:@"number"]] ;
                
                
            }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                //Handle no, thanks button
            }];
            
            //Add your buttons to alert controller
            
            
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:NO completion:nil];
            
            
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                                    alertControllerWithTitle:kAlertTitle
                                                    message:@"Are you sure you want to remove this number from blacklist?"
                                                    preferredStyle:UIAlertControllerStyleAlert];
                       
                       //Add Buttons
                       
                       UIAlertAction* yesButton = [UIAlertAction
                                                   actionWithTitle:@"Yes"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                           //Handle your yes please button action here
                           
                           
                  //         Tblcell *cell = [self->_tblDetail cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sender tag] inSection:0]];
                                     
               // [self removeFromBlackList:cell.lblNumber.text];
                           NSIndexPath *path = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
                                         
                           [self removeFromBlackList:[[self->numbersArray objectAtIndex:path.row] valueForKey:@"number"]] ;
                           
                       }];
                       
                       UIAlertAction* noButton = [UIAlertAction
                                                  actionWithTitle:@"Cancel"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                           //Handle no, thanks button
                       }];
                       
                       //Add your buttons to alert controller
                       
                       
                       
                       [alert addAction:yesButton];
                       [alert addAction:noButton];
                       
                       [self presentViewController:alert animated:NO completion:nil];
            
           
        }
    }
    else{
        
         [UtilsClass makeToast:@"Block Number feature is not available in your plan."];
    }
    
}
-(void)addToBlackList:(NSString *)number
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *url = [NSString stringWithFormat:@"billing/plan/addNumberToBlockList"];
    
    NSString *userid = [Default valueForKey:USER_ID];
    NSString *parentId = [Default valueForKey:PARENT_ID];

    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSDictionary *passDict = @{@"deviceType":@"ios",
                               @"number":number,
                               @"parentId":parentId,
                               @"user":userid
                               
    };
    
        NSLog(@"blacklist URL : %@",url);
        NSLog(@"blacklist Dic : %@",passDict);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(blockListResponse:response:) andDelegate:self];
}
- (void)blockListResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];

   // NSLog(@"blacklist response:: %@",response1);

    NSLog(@"Encrypted Response : add blacklist : %@",response1);


        if([apiAlias isEqualToString:Status_Code])
        {
                [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                NSDictionary*dic = [response1 valueForKey:@"data"];
                
                    [Default setValue:[dic valueForKey:@"number"] forKey:kblackListedArray];
                    [Default synchronize];
                
                
                
                [_tblDetail reloadData];
                
             [UtilsClass makeToast:@"Successfully number blacklisted."];
                
                
            }
            else
            {
                [UtilsClass makeToast:[response1 valueForKey:@"error"]];
            }
        }
}
-(void)removeFromBlackList:(NSString *)number
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *url = [NSString stringWithFormat:@"billing/plan/removeNumberFromBlockList"];
    
    NSString *userid = [Default valueForKey:USER_ID];
    NSString *parentId = [Default valueForKey:PARENT_ID];

    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSDictionary *passDict = @{@"deviceType":@"ios",
                               @"number":number,
                               @"parentId":parentId,
                               @"user":userid
    };
    
    
    
        NSLog(@"blacklist URL : %@",url);
        NSLog(@"blacklist Dic : %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(removeblockListResponse:response:) andDelegate:self];
}
- (void)removeblockListResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];


        if([apiAlias isEqualToString:Status_Code])
        {
                [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            NSLog(@"Encrypted Response : remove blacklist : %@",response1);

            
            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                NSDictionary*dic = [response1 valueForKey:@"data"];
                               
                [Default setValue:[dic valueForKey:@"number"] forKey:kblackListedArray];
                [Default synchronize];
                
                [_tblDetail reloadData];
                
                [UtilsClass makeToast:@"Successfully removed number from blacklist."];


            }
            else
            {
                [UtilsClass makeToast:[response1 valueForKey:@"error"]];
            }
        }
}


@end
