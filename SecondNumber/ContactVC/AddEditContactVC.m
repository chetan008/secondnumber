//
//  AddEditContactVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "AddEditContactVC.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "GlobalData.h"
#import "UIViewController+LGSideMenuController.h"
@interface AddEditContactVC ()
{
    WebApiController *obj;
    
    NSString *name;
    NSString *company;
    NSString *email;
    NSString *number;
    
    NSString *isEdit;
    BOOL isSubContactAddFieldOpen;

    
    NSMutableArray *numbersArray;
    BOOL isContactSaved;
    
    NSMutableDictionary *updatedData;
    
    BOOL showAddMessage;
    
}
@end
@implementation AddEditContactVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    // Do any additional setup after loading the view.
    [UtilsClass view_navigation_title:self title:@"Contact Details"color:UIColor.whiteColor];



    _numbersTblView.dataSource = self;
    _numbersTblView.delegate = self;
    numbersArray = [[NSMutableArray alloc]init];
    updatedData = [[NSMutableDictionary alloc]init];
    
   
    
    if ([_controller isEqualToString:@"edit"]){
        

        
        isContactSaved = true;
        
        numbersArray = [_ContactDetailsDic objectForKey:@"numberArray"];
        [_numbersTblView reloadData];
        [self updateTableHeight];
       // NSLog(@"contact detail dict :: %@",_ContactDetailsDic);
        
        
        _txtName.text = [_ContactDetailsDic valueForKey:@"name"];
        
       
        _txtEmail.text = [_ContactDetailsDic valueForKey:@"email"];
        _txtCompany.text = [_ContactDetailsDic valueForKey:@"company"];
    }else if ([_controller isEqualToString:@"editFromChat"]){
        
        isContactSaved = true;
        
        if ([_ContactDetailsDic valueForKey:@"threadName"] == nil){
            _txtName.text = [_ContactDetailsDic valueForKey:@"name"];
        }else{
            _txtName.text = [_ContactDetailsDic valueForKey:@"threadName"];
        }
        
        if ([_ContactDetailsDic valueForKey:@"threadNumber"] == nil){
             
            
            _txtNumber.text = [_ContactDetailsDic valueForKey:@"number"];
        }else{
             //_txtNumber.text = [_ContactDetailsDic valueForKey:@"threadNumber"];
            
            
        }
       
        
        if ([_ContactDetailsDic valueForKey:@"contactEmail"] == nil){
             _txtEmail.text = [_ContactDetailsDic valueForKey:@"email"];
        }else{
             _txtEmail.text = [_ContactDetailsDic valueForKey:@"contactEmail"];
        }
        
        if ([_ContactDetailsDic valueForKey:@"contactCompany"] == nil){
            _txtCompany.text = [_ContactDetailsDic valueForKey:@"company"];
        }else{
             _txtCompany.text = [_ContactDetailsDic valueForKey:@"contactCompany"];
        }
       
       
       
    }else if ([_controller isEqualToString:@"addFromChat"]){
        
        _txtName.text = [_ContactDetailsDic valueForKey:@""];
        //_txtNumber.text = [_ContactDetailsDic valueForKey:@"threadNumber"];
        _txtEmail.text = [_ContactDetailsDic valueForKey:@""];
        _txtCompany.text = [_ContactDetailsDic valueForKey:@""];
        
        NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[_ContactDetailsDic valueForKey:@"threadNumber"],@"number",nil];
        
            [numbersArray addObject:temp];
            [self updateTableHeight];
        
    }else if([_controller isEqualToString:@"addFromDialer"]){
        _txtName.text = @"";
        //NSLog(@"Strin Number : %@",_number);
        //_txtNumber.text = [NSString stringWithFormat:@"%@",_number];
        _txtEmail.text = @"";
        _txtCompany.text = @"";
        
          NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[NSString stringWithFormat:@"%@",_number],@"number",nil];
        
            [numbersArray addObject:temp];
            [self updateTableHeight];
        
    }else if([_controller isEqualToString:@"editfromcalllogs"]) {
        
        
        [self getContact:[_ContactDetailsDic valueForKey:@"contactId"]];
        isContactSaved = true;


        
        
    }else if([_controller isEqualToString:@"CalllogsDetailsVCChat"]) {
        
        isContactSaved = true;

        
        _txtName.text = [_ContactDetailsDic valueForKey:@"contactName"];
        if ([[_ContactDetailsDic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
                _txtNumber.text = [_ContactDetailsDic valueForKey:@"from"];
        }
        else
        {
                _txtNumber.text =  [_ContactDetailsDic valueForKey:@"to"];//"\(userData?.to! ?? "")"
        }
        

        _txtEmail.text = [_ContactDetailsDic valueForKey:@"contactEmail"];
        _txtCompany.text = [_ContactDetailsDic valueForKey:@"contactCompany"];
    }else if([_controller isEqualToString:@"calllogsDetails"]) {
        
        isContactSaved = true;

        _txtName.text = [_ContactDetailsDic valueForKey:@"contactName"];
        
        if ([[_ContactDetailsDic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
                _txtNumber.text = [_ContactDetailsDic valueForKey:@"from"];
        }
        else
        {
                _txtNumber.text =  [_ContactDetailsDic valueForKey:@"to"];//"\(userData?.to! ?? "")"
        }
        

        _txtEmail.text = [_ContactDetailsDic valueForKey:@"contactEmail"];
        _txtCompany.text = [_ContactDetailsDic valueForKey:@"contactCompany"];
    }else if([_controller isEqualToString:@"addcalllogsDetails"]) {
        _txtName.text = [_ContactDetailsDic valueForKey:@"contactName"];
        
        NSDictionary *temp;
        if ([[_ContactDetailsDic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
            //_txtNumber.text = [_ContactDetailsDic valueForKey:@"from"];
            temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[_ContactDetailsDic valueForKey:@"from"],@"number",nil];
        }
        else
        {
            //_txtNumber.text =  [_ContactDetailsDic valueForKey:@"to"];//"\(userData?.to! ?? "")"
           temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[_ContactDetailsDic valueForKey:@"to"],@"number",nil];
        }
        
        
        
            [numbersArray addObject:temp];
            [self updateTableHeight];
        
        _txtEmail.text = [_ContactDetailsDic valueForKey:@"contactEmail"];
        _txtCompany.text = [_ContactDetailsDic valueForKey:@"contactCompany"];
        
        
    }else if([_controller isEqualToString:@"AddCalllogsDetailsVCChat"]) {
        _txtName.text = [_ContactDetailsDic valueForKey:@"contactName"];
        

        NSDictionary *temp;
        if ([[_ContactDetailsDic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
            //_txtNumber.text = [_ContactDetailsDic valueForKey:@"from"];
            temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[_ContactDetailsDic valueForKey:@"from"],@"number",nil];
        }
        else
        {
            //_txtNumber.text =  [_ContactDetailsDic valueForKey:@"to"];//"\(userData?.to! ?? "")"
           temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",[_ContactDetailsDic valueForKey:@"to"],@"number",nil];
        }
        
        
        
            [numbersArray addObject:temp];
            [self updateTableHeight];
        
        
        _txtEmail.text = [_ContactDetailsDic valueForKey:@"contactEmail"];
        _txtCompany.text = [_ContactDetailsDic valueForKey:@"contactCompany"];
        
    }else {
        _txtName.text = @"";
        _txtNumber.text = @"";
        _txtEmail.text = @"";
        _txtCompany.text = @"";
        
    }
    
}


- (IBAction)btnBackClicked:(id)sender {
    
    
//    if(_delegate && [_delegate respondsToSelector:@selector(secVCDidDismisWithData:)])
//    {
//        [_delegate secVCDidDismisWithData:updatedData];
//    }
    [[self navigationController] popViewControllerAnimated:true];

    
    
}
- (IBAction)btnSaveClicked:(id)sender {
    
    name = _txtName.text;
    name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    company = _txtCompany.text;
    company = [company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    email = _txtEmail.text;
    email = [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    number = self.txtNumber.text;
    number = [number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    
    if ([name isEqualToString:@""] ) {

        [UtilsClass showAlert:@"Name is required." contro:self];
    }else
    if (![email isEqualToString:@""] && ![UtilsClass validateEmail:email]) {
        [UtilsClass showAlert:@"Please enter valid Email." contro:self];
    }else {
    
        if ([_controller isEqualToString:@"edit"]){
            
            [self edit];
            isEdit = @"1";
        }else if ([_controller isEqualToString:@"editFromChat"]){
            [self edit];
            isEdit = @"1";
        }else if ([_controller isEqualToString:@"editfromcalllogs"]){
            [self edit];
            isEdit = @"1";
        }else if ([_controller isEqualToString:@"CalllogsDetailsVCChat"]){
            [self edit];
            isEdit = @"1";
        }else if ([_controller isEqualToString:@"calllogsDetails"]){
            [self edit];
            isEdit = @"1";
        }else {
            
            isEdit = @"0";
           
            showAddMessage = true;

            if (isContactSaved) {

                [self edit];
            }
            else
            {
                
                 Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                
                name = _txtName.text;
                 name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                 company = _txtCompany.text;
                 company = [company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                 email = _txtEmail.text;
                 email = [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                NSString *number = [cell.txtAddNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                

                if ([number isEqualToString:@""]) {
                    [UtilsClass showAlert:@"Number is required" contro:self];
                }
                else
                {
                    [self add:number];

                }
            }
           
        }
    }
}

-(void)edit{
    
    

    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *contactID;
    if ([_ContactDetailsDic valueForKey:@"contactId"] == nil) {
        contactID = [_ContactDetailsDic valueForKey:@"_id"];
    }else {
        contactID = [_ContactDetailsDic valueForKey:@"contactId"];
    }
   
   
    
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSString *edit = @"edit";
    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@",GETCONTACT_URL,contactID,userID,edit];
   // NSLog(@"URL : %@",url);
    
    
       
    


     NSDictionary *passDict = @{@"name":name,
                                   @"company":company,
                                   @"email":email,
                                   @"user":userID
                                };

 
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];

   

    [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    

}


-(void)add:(NSString *)numberToAdd
{
    
    
    if (isContactSaved) {
         [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    }
   
    
    
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *add = @"add";
    NSString *url = [NSString stringWithFormat:@"%@%@/%@",GETCONTACT_URL,userId,add];
    

    
    NSDictionary *passDict = @{@"name":name,
                               @"company":company,
                               @"email":email,
                               @"number": numberToAdd };
    
    
   // NSLog(@"Dictonary : %@",passDict);
    
    //NSLog(@"url : %@%@",SERVERNAME,url);
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
    
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (void)login:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
   // NSLog(@"Contactedit_API_Response : %@",response1);

    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        NSLog(@"Encrypted Response : contact add edit : %@",response1);

    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//        [UtilsClass showAlert:@"Done" contro:self];
        
        NSMutableDictionary *dic = [response1 valueForKey:@"data"];

       // NSLog(@"controller :: %@",_controller);
        
     
        
        if (showAddMessage) {
            
            [UtilsClass makeToast:kCONTACTSAVESUCCESSMSG];
            
        }
        else
        {
            if (isContactSaved) {
                [UtilsClass makeToast:kCONTACTEDITSUCCESSMSG];

            }
            else
            {
                [UtilsClass makeToast:kSUBCONTACTSAVESUCCESSMSG];

            }
            
        }
        
        
      /*  if (isContactSaved) {
            if (![_controller isEqualToString:@"edit"]) {
            [UtilsClass makeToast:kCONTACTSAVESUCCESSMSG];
                
        [[self navigationController] popViewControllerAnimated:true];

                
        if(_delegate && [_delegate respondsToSelector:@selector(secVCDidDismisWithData:)])
        {
                [_delegate secVCDidDismisWithData:[response1 valueForKey:@"data"]];
        }


        }
        else
        {
            [[self navigationController] popViewControllerAnimated:true];
            
            
            if(_delegate && [_delegate respondsToSelector:@selector(secVCDidDismisWithData:)])
            {
                [_delegate secVCDidDismisWithData:[response1 valueForKey:@"data"]];
            }

            [UtilsClass makeToast:kCONTACTEDITSUCCESSMSG];

        }
        }
        else
        {
           [UtilsClass makeToast:kSUBCONTACTSAVESUCCESSMSG];
            
//            [[self navigationController] popViewControllerAnimated:true];
//
//
//        if(_delegate && [_delegate respondsToSelector:@selector(secVCDidDismisWithData:)])
//        {
//                           [_delegate secVCDidDismisWithData:[response1 valueForKey:@"data"]];
//    }
            

        }*/

       
    /* old
        if([_controller isEqualToString:@"edit"])
        {
            NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
            [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
            [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
            [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
            [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
            //[newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
            [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
            [newdic setValue:[dic valueForKey:@"user"] forKey:@"user"];
            [[GlobalData sharedGlobalData] edit_contact:1 newdic:newdic olddic:_ContactDetailsDic title:@"edit"];
        }else if ([_controller isEqualToString:@"editFromChat"]){
            NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
            [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
            [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
            [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
            [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
           // [newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
            [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
            [newdic setValue:[dic valueForKey:@"user"] forKey:@"user"];
            [[GlobalData sharedGlobalData] edit_contact:1 newdic:newdic olddic:_ContactDetailsDic title:@"edit"];
        }else if ([_controller isEqualToString:@"editfromcalllogs"]){
            NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
            [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
            [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
            [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
            [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
            //[newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
            [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
            [newdic setValue:[dic valueForKey:@"user"] forKey:@"user"];
            [[GlobalData sharedGlobalData] edit_contact:1 newdic:newdic olddic:_ContactDetailsDic title:@"edit"];
        }else if ([_controller isEqualToString:@"CalllogsDetailsVCChat"]){
            NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
            [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
            [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
            [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
            [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
            //[newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
            [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
            [newdic setValue:[dic valueForKey:@"user"] forKey:@"user"];
            [[GlobalData sharedGlobalData] edit_contact:1 newdic:newdic olddic:_ContactDetailsDic title:@"edit"];
        }else
        {
            if ([isEdit isEqualToString:@"0"]) {
                NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
                [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
                [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
                [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
                [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
                //[newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
                [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
                [newdic setValue:[dic valueForKey:@"user"] forKey:@"user"];
                [[GlobalData sharedGlobalData] edit_contact:1 newdic:newdic olddic:newdic title:@"add"];
            }
            
        
            //_controller = @"edit";
        
        }
        */
        
        if (isContactSaved) {
            
            [self editContactCall:dic];
        }
        else
        {
            [self addContactCall:dic];
        }
        
        
        
                _ContactDetailsDic = [[NSMutableDictionary alloc]init];
               [_ContactDetailsDic setValue:[dic valueForKey:@"_id"] forKey:@"contactId"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"name"] forKey:@"name"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"email"] forKey:@"contactEmail"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"company"] forKey:@"contactCompany"];
               
               [_ContactDetailsDic setObject:[dic objectForKey:@"numberArray"] forKey:@"numberArray"];
               
               updatedData = [response1 objectForKey:@"data"];
               
               numbersArray = [dic objectForKey:@"numberArray"];
               [_rightMarkBtn setEnabled:true];
               isSubContactAddFieldOpen = false;
        
        
        Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [cell.plusBtnAddNumber setBackgroundImage:[UIImage imageNamed:@"addContact"] forState:UIControlStateNormal];
        
        [cell.plusBtnAddNumber setEnabled:true];
        
        
        
        if (showAddMessage || isContactSaved) {
            
            
            [[self navigationController] popViewControllerAnimated:true];
            
            if(_delegate && [_delegate respondsToSelector:@selector(secVCDidDismisWithData:)])
            {
                
                [_delegate secVCDidDismisWithData:[response1 valueForKey:@"data"]];
            }
            
        }
        
        
        //Show success message
    
      if ([isEdit isEqualToString:@"1"]) {
            //[UtilsClass makeToast:kCONTACTEDITSUCCESSMSG];
        }
        else
        {
            //[UtilsClass makeToast:kCONTACTSAVESUCCESSMSG];
            isContactSaved = true;
        }
        
        
        
    }
    else {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
                [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
        }
        }
        @catch (NSException *exception) {
        }
    }
    
    }
}




#pragma mark - numbers table datasource delgate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_controller isEqualToString:@"add"] && isContactSaved == false) {
        return 1;
    }
    else
    {
    return numbersArray.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [_numbersTblView dequeueReusableCellWithIdentifier:@"AddContactCell"];
    
//work    if ([_controller isEqualToString:@"add"]) {
    
   
    if (!isContactSaved) {
        
        //add contacts
        if (numbersArray.count>0) {
            cell.txtAddNumber.text = [[numbersArray objectAtIndex:indexPath.row] valueForKey:@"number"];
        }
        if (numbersArray.count > 1) {
            [cell.deleteBtnAddNumber setHidden:false];
        }
        else
        {
            [cell.deleteBtnAddNumber setHidden:true];
            [cell.plusBtnAddNumber setBackgroundImage:[UIImage imageNamed:@"ic_save"] forState:UIControlStateNormal];
            
            // [cell.deleteBtnAddNumber setEnabled:false];
             [cell.plusBtnAddNumber setHidden:false];
            [cell.plusBtnAddNumber setEnabled:false];
            
            // [cell.deleteBtnAddNumber setHidden:false];
        }
        
    }
    else
    {
        if (numbersArray.count>1) {
            [cell.deleteBtnAddNumber setHidden:false];
        }
        else
        {
             [cell.deleteBtnAddNumber setHidden:true];
        }
    
        if (indexPath.row == numbersArray.count - 1 && numbersArray.count < 6 ) {
            
            // if it is last cell then show plus btn
            [cell.plusBtnAddNumber setHidden:false];
            
            if (isSubContactAddFieldOpen) {
                
                [cell.plusBtnAddNumber setBackgroundImage:[UIImage imageNamed:@"ic_save"] forState:UIControlStateNormal];
                
            }
            else
            {

                [cell.plusBtnAddNumber setBackgroundImage:[UIImage imageNamed:@"addContact"] forState:UIControlStateNormal];

            }
            
        }
        else
        {
            [cell.plusBtnAddNumber setHidden:true];
        }
        
        
        
        
        
        
        
      /*  if (indexPath.row > 0) {
            [cell.deleteBtnAddNumber setHidden:false];
        }
        else
        {
            [cell.deleteBtnAddNumber setHidden:true];
        }
        

        cell.txtAddNumber.text = [NSString stringWithFormat:@"%ld",indexPath.row];*/
        
     cell.txtAddNumber.text = [[numbersArray objectAtIndex:indexPath.row] valueForKey:@"number"];
        
        

    }
    
   
    
    if ( [[cell.plusBtnAddNumber backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"ic_save"]]   && cell.plusBtnAddNumber.enabled)
    {
// w           cell.txtAddNumber.userInteractionEnabled = true;
        
            [cell.deleteBtnAddNumber setEnabled:true];
        
    }
    else
    {
        if (isSubContactAddFieldOpen) {
            [cell.deleteBtnAddNumber setEnabled:false];
        }
        else
        {
            [cell.deleteBtnAddNumber setEnabled:true];
        }
        
   //w     cell.txtAddNumber.userInteractionEnabled = false;

    }
    
    if (indexPath.row == 0) {
           if ([cell.deleteBtnAddNumber isHidden]) {
               
               //cell.plusBtnTrailingConstraint.constant = cell.deleteBtnTrailingConstraint.constant;
               
               NSLayoutConstraint *plusBtnConst =
                   [NSLayoutConstraint constraintWithItem:cell.plusBtnAddNumber attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell.deleteBtnAddNumber attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];

               [cell addConstraint:plusBtnConst];
               
               
           }
           else
           {
              // cell.plusBtnTrailingConstraint.constant = cell.deleteBtnTrailingConstraint.constant;
               
               NSLayoutConstraint *plusBtnConst =
                   [NSLayoutConstraint constraintWithItem:cell.plusBtnAddNumber attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:cell.deleteBtnAddNumber attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];

               [cell addConstraint:plusBtnConst];
           }
       }
    
    if (indexPath.row == 4) {

        //if no > 4 hide plus btn
        [cell.plusBtnAddNumber setHidden:true];
    }
    
    //to set userinteraction enable /disable
    
    if ([[cell.plusBtnAddNumber backgroundImageForState:UIControlStateNormal] isEqual: [UIImage imageNamed:@"ic_save"]]) {
        
        [cell.txtAddNumber setUserInteractionEnabled:true];
        [cell.txtAddNumber becomeFirstResponder];
    }
    else
    {
        if (isSubContactAddFieldOpen) {
            [cell.txtAddNumber setUserInteractionEnabled:false];

        }
        else
        {
            [cell.txtAddNumber setUserInteractionEnabled:true];

        }
    }
    
    cell.plusBtnAddNumber.tag = indexPath.row+1;
    cell.deleteBtnAddNumber.tag = indexPath.row+1;
    cell.txtAddNumber.tag = indexPath.row+1;
    
    
    [cell.plusBtnAddNumber addTarget:self action:@selector(addNumbersClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.deleteBtnAddNumber addTarget:self action:@selector(deleteNumbersClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
   
    
    return cell;
    
}


-(void)addNumbersClick:(id)sender
{
    
    

    
    if (isSubContactAddFieldOpen) {
        
        
        Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sender tag]-1 inSection:0]];
        
        //if save button clicked
        //ad [numbersArray removeLastObject];
        //ad [numbersArray addObject:cell.txtAddNumber.text];
        
        //if contact saved then add to sub otherwise add to main contact
        if (isContactSaved) {
            
            if( ![[[numbersArray lastObject] valueForKey:@"_id"] isEqualToString:@""]) {
                
                //if last added is blank field then add new otherwise edit
                
                if ([[numbersArray valueForKey:@"number"] containsObject:cell.txtAddNumber.text]) {
                    
                    //if number already exist then don't call API
                    isSubContactAddFieldOpen = false;
                    [_rightMarkBtn setEnabled:true];
                    
                    [_numbersTblView reloadData];
                }
                else
                {
                    NSString *subID = [[numbersArray objectAtIndex:[sender tag]-1] valueForKey:@"_id"];
                               
                    [self editSubContact:subID subContactNumber:cell.txtAddNumber.text];
                }
               
            }
            else
            {
                
                 [self saveSubContact:cell.txtAddNumber.text];
            }
            
        }
        else
        {
            name = _txtName.text;
               name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

               company = _txtCompany.text;
               company = [company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

               email = _txtEmail.text;
               email = [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

              NSString *number = [cell.txtAddNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            [self add:number];
        }
       
    
    }
    else
    {
        //if add button clicked
        
        if (numbersArray.count < 5) {
            
            NSDictionary *temp = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"_id",@"",@"deleteStatus",@"",@"number",nil];
            
            [numbersArray addObject:temp];
            

            isSubContactAddFieldOpen = true;
            [_rightMarkBtn setEnabled:false];
            
            [self updateTableHeight];
            
            [_numbersTblView reloadData];
            
            
            
        }
    }
    
        
    
    
    

}

-(void)deleteNumbersClick:(id)sender
{
    if (numbersArray.count > 1) {
        
        //[numbersArray removeObject:[NSNumber numberWithLong:[sender tag]-1]];
        
//        Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sender tag]-1 inSection:0]];
//
//           cell.plusBtnAddNumber.hidden = false;
          
        if (!isSubContactAddFieldOpen) {
            
            // deleting already added number
           // NSLog(@"number array :: %@",numbersArray);
            

            UIAlertController * alert = [UIAlertController
                              alertControllerWithTitle:kAlertTitle
                              message:@"Are You Sure Want to Delete This Number?"
                              preferredStyle:UIAlertControllerStyleAlert];
                
                
               UIAlertAction* yesButton = [UIAlertAction
                             actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                               [self deleteSubContact:[[numbersArray valueForKey:@"_id"] objectAtIndex:[sender tag]-1]];
                               
                             }];
                
               UIAlertAction* noButton = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                   
                     }];
               [alert addAction:yesButton];
               [alert addAction:noButton];
               [self presentViewController:alert animated:YES completion:nil];

            
            
          
        }
        else
        {
            //deleting blank field only
            [numbersArray removeLastObject];
            isSubContactAddFieldOpen = false;
        
        }
          [self updateTableHeight];
          
    }
    else
    {
//        Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sender tag]-1 inSection:0]];
//
//         cell.plusBtnAddNumber.hidden = false;
        
    }
    
}

-(void)updateTableHeight
{
    
        [_numbersTblView reloadData];
           _numbersTblHeightConstraint.constant = (numbersArray.count) * 57;
    
   

}

-(void)saveSubContact:(NSString *)subContactNumber
{
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *contactId;
    
    if ([_ContactDetailsDic valueForKey:@"contactId"] == nil) {
        contactId = [_ContactDetailsDic valueForKey:@"_id"];
         //NSLog(@"_id : %@",contactID);
    }else {
        contactId = [_ContactDetailsDic valueForKey:@"contactId"];
         //NSLog(@"Contact id : %@",contactID);
    }
    
    
        NSString *url = [NSString stringWithFormat:@"%@%@/%@/addSubContact",GETCONTACT_URL,contactId,userId];
        
        
        NSDictionary *passDict = @{
                                   @"number": subContactNumber };
        
        
       // NSLog(@"Dictonary : %@",passDict);
        
       // NSLog(@"url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
            
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        
        
        [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(saveSubContact:response:) andDelegate:self];
}
- (void)saveSubContact:(NSString *)apiAlias response:(NSData *)response{

            NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
            
       // NSLog(@"Contact_API_Response : %@",response1);


        if([apiAlias isEqualToString:Status_Code])
        {
               // [UtilsClass logoutUser:self];
             [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            NSLog(@"Encrypted Response : save sub contact : %@",response1);


            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                isSubContactAddFieldOpen = false;
                [_rightMarkBtn setEnabled:true];

//                if ([[[numbersArray lastObject] valueForKey:@"number"] isEqualToString:@""]) {
//                    [numbersArray removeLastObject];
//                }
                _ContactDetailsDic = [[NSMutableDictionary alloc]init];
                NSDictionary *dic = [response1 objectForKey:@"data"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"_id"] forKey:@"contactId"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"name"] forKey:@"name"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"email"] forKey:@"contactEmail"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"company"] forKey:@"contactCompany"];
                
                [_ContactDetailsDic setObject:[dic objectForKey:@"numberArray"] forKey:@"numberArray"];
                
                updatedData = [response1 objectForKey:@"data"];
                numbersArray = [[response1 objectForKey:@"data"] objectForKey:@"numberArray"];
                
                [_numbersTblView reloadData];
                
                [self editContactCall:dic];
                
                [UtilsClass makeToast:kSUBCONTACTSAVESUCCESSMSG];

            }
            else
            {
                 [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
            }
        }
    
}
-(void)deleteSubContact:(NSString *)subContactId
{
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *contactId;
    
    if ([_ContactDetailsDic valueForKey:@"contactId"] == nil) {
        contactId = [_ContactDetailsDic valueForKey:@"_id"];
         //NSLog(@"_id : %@",contactID);
    }else {
        contactId = [_ContactDetailsDic valueForKey:@"contactId"];
         //NSLog(@"Contact id : %@",contactID);
    }
    
    
    
        NSString *url = [NSString stringWithFormat:@"%@%@/%@",GETCONTACT_URL,contactId,subContactId];
    
       // NSLog(@"url : %@%@",SERVERNAME,url);
      
        obj = [[WebApiController alloc] init];
     
         [obj callAPI_DELETE_RAW:url andParams:nil SuccessCallback:@selector(deleteSubContact:response:) andDelegate:self];
          
}
- (void)deleteSubContact:(NSString *)apiAlias response:(NSData *)response{

            NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
            
       // NSLog(@"Contact_API_Response : %@",response1);


        if([apiAlias isEqualToString:Status_Code])
        {
               // [UtilsClass logoutUser:self];
             [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {

            NSLog(@"Encrypted Response : delete sub contact : %@",response1);

            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                _ContactDetailsDic = [[NSMutableDictionary alloc]init];
                NSDictionary *dic = [response1 objectForKey:@"data"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"_id"] forKey:@"contactId"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"name"] forKey:@"name"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"email"] forKey:@"contactEmail"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"company"] forKey:@"contactCompany"];
                
                [_ContactDetailsDic setObject:[dic objectForKey:@"numberArray"] forKey:@"numberArray"];
                
                updatedData = [response1 objectForKey:@"data"];
                
                numbersArray = [[response1 objectForKey:@"data"] objectForKey:@"numberArray"];
                [_numbersTblView reloadData];
                [self updateTableHeight];
                
                [self editContactCall:dic];
            
            }
            else
            {
                 [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
            }
        }
    
}
-(void)editSubContact:(NSString *)subContactId subContactNumber:(NSString *)subContactNumber
{
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *contactId;
    
    if ([_ContactDetailsDic valueForKey:@"contactId"] == nil) {
        contactId = [_ContactDetailsDic valueForKey:@"_id"];
         //NSLog(@"_id : %@",contactID);
    }else {
        contactId = [_ContactDetailsDic valueForKey:@"contactId"];
         //NSLog(@"Contact id : %@",contactID);
    }
    
    
        NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@/editSubContact",GETCONTACT_URL,contactId,subContactId,userId];
        
        NSDictionary *passDict = @{
                                   @"number": subContactNumber };
        
        
       // NSLog(@"Dictonary : %@",passDict);
        
       // NSLog(@"url : %@%@",SERVERNAME,url);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
            
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        
        
        [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(editSubContact:response:) andDelegate:self];
}
- (void)editSubContact:(NSString *)apiAlias response:(NSData *)response{

        
    
        NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
            
       // NSLog(@"Contact_API_Response : %@",response1);


        if([apiAlias isEqualToString:Status_Code])
        {
              //  [UtilsClass logoutUser:self];
             [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {

            NSLog(@"Encrypted Response : edit sub contact : %@",response1);

            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                isSubContactAddFieldOpen = false;
                [_rightMarkBtn setEnabled:true];
                _ContactDetailsDic = [[NSMutableDictionary alloc]init];
                NSDictionary *dic = [response1 objectForKey:@"data"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"_id"] forKey:@"contactId"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"name"] forKey:@"name"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"email"] forKey:@"contactEmail"];
                [_ContactDetailsDic setValue:[dic valueForKey:@"company"] forKey:@"contactCompany"];
                
                [_ContactDetailsDic setObject:[dic objectForKey:@"numberArray"] forKey:@"numberArray"];

                updatedData = [response1 objectForKey:@"data"];
                
                numbersArray = [[response1 objectForKey:@"data"] objectForKey:@"numberArray"];
                [_numbersTblView reloadData];
                
                [self editContactCall:dic];
                
            
            }
            else
            {
                 [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
            }
        }
    
}

-(void)getContact:(NSString *)contactId
{
    NSString *url = [NSString stringWithFormat:@"%@%@",GETCONTACT_URL,contactId];
            
        
           // NSLog(@"url check : %@%@",SERVERNAME,url);
            
            
            obj = [[WebApiController alloc] init];
            
            
             [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(getConatctDetail:response:) andDelegate:self];
    
}
- (void)getConatctDetail:(NSString *)apiAlias response:(NSData *)response
{
       
       
       NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
           
      // NSLog(@"Contact_API_Response : %@",response1);


       if([apiAlias isEqualToString:Status_Code])
       {
             //  [UtilsClass logoutUser:self];
            [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
       }
       else
       {

           NSLog(@"Encrypted Response : get contact detail : %@",response1);

           if ([[response1 valueForKey:@"success"] integerValue] == 1)
           {
               _ContactDetailsDic = [[NSMutableDictionary alloc]init];
               NSDictionary *dic = [response1 objectForKey:@"data"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"_id"] forKey:@"contactId"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"name"] forKey:@"name"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"email"] forKey:@"contactEmail"];
               [_ContactDetailsDic setValue:[dic valueForKey:@"company"] forKey:@"contactCompany"];
               
               [_ContactDetailsDic setObject:[dic objectForKey:@"numberArray"] forKey:@"numberArray"];
               
               numbersArray = [[response1 objectForKey:@"data"]  objectForKey:@"numberArray"];
               
               [_numbersTblView reloadData];
               [self updateTableHeight];
               
               _txtName.text = [dic valueForKey:@"name"];
               _txtEmail.text =  [dic valueForKey:@"email"];
               _txtCompany.text =  [dic valueForKey:@"company"];

               
           }
           
       }

   }
#pragma mark - textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField.tag != 0) {
        
//work        if ([_controller isEqualToString:@"add"]) {
            if (!isContactSaved) {

                
             [_rightMarkBtn setEnabled:true];
        }
        else
        {
             [_rightMarkBtn setEnabled:false];
        }
       
        
        for (int i =0; i < numbersArray.count; i++) {
            
            Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            if (i == textField.tag - 1) {
                //is  active cell
                 [cell.txtAddNumber setUserInteractionEnabled:true];
                    [cell.deleteBtnAddNumber setEnabled:true];
            }
            else
            {
                
                [cell.txtAddNumber setUserInteractionEnabled:false];
                [cell.plusBtnAddNumber setHidden:true];
                [cell.deleteBtnAddNumber setEnabled:false];

            }
            
        }
        
        
        Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[textField tag]-1 inSection:0]];
        
        [cell.plusBtnAddNumber setHidden:false];
        
        if (cell.txtAddNumber.text.length>0) {
            [cell.plusBtnAddNumber setEnabled:true];
        }
        else
        {
             [cell.plusBtnAddNumber setEnabled:false];
        }
        
        [cell.plusBtnAddNumber setBackgroundImage:[UIImage imageNamed:@"ic_save"] forState:UIControlStateNormal];
        
        
        isSubContactAddFieldOpen = true;
        
        
        
    }
    
    
    
    
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //check if tag is defined or not.. tag is not defined for name and other field
    if (textField.tag > 0) {
        
        Tblcell *cell = [_numbersTblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[textField tag]-1 inSection:0]];
        
        if ([[cell.plusBtnAddNumber backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"ic_save"]] ) {
            
            NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
            if ([str containsString:@"+"]) {
               str =  [str stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            if (str.length == 3 || str.length == 4 || str.length == 5 || str.length >9 ) {
                
                if ( _txtName.text.length > 0 ) {
                     [cell.plusBtnAddNumber setEnabled:true];
                    
                }
                //worked controller add cond
                if (!isContactSaved) {
                    [_rightMarkBtn setEnabled:true];
                }
                
               
            }
            else
            {
                [cell.plusBtnAddNumber setEnabled:false];
                [_rightMarkBtn setEnabled:false];
            }
        }
    }
    return true;
    
}
#pragma mark - contacts DB
-(void)editContactCall:(NSDictionary *)dic
{
    
    NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
    [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
    [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
    [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
    [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
    //[newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
    [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
    
    //edit contact
    
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    Contact *newcon = [[Contact alloc]initWithContext:context];
    newcon.name = [dic valueForKey:@"name"];
    newcon.email = [dic valueForKey:@"email"];
    newcon.company = [dic valueForKey:@"company"];
    newcon.contactID = [dic valueForKey:@"_id"];
    newcon.number = [dic valueForKey:@"numberArray"];
    
    [Contact editContactToDB:newcon];

    
    [[GlobalData sharedGlobalData] editCHcontactToDBArray:newdic  title:@"edit"];
}
-(void)addContactCall:(NSDictionary *)dic
{
    //add contact
    
    NSMutableDictionary *newdic = [[NSMutableDictionary alloc] init];
    [newdic setValue:[dic valueForKey:@"_id"] forKey:@"_id"];
    [newdic setValue:[dic valueForKey:@"company"] forKey:@"company"];
    [newdic setValue:[dic valueForKey:@"email"] forKey:@"email"];
    [newdic setValue:[dic valueForKey:@"name"] forKey:@"name"];
    //[newdic setValue:[dic valueForKey:@"number"] forKey:@"number"];
    [newdic setValue:[dic valueForKey:@"numberArray"] forKey:@"numberArray"];
    
    CoreDataManager *dbm = [[CoreDataManager alloc] init];
    NSManagedObjectContext *context = [dbm managedObjectContext];
    
    Contact *newcon = [[Contact alloc]initWithContext:context];
    newcon.name = [dic valueForKey:@"name"];
    newcon.email = [dic valueForKey:@"email"];
    newcon.company = [dic valueForKey:@"company"];
    newcon.contactID = [dic valueForKey:@"_id"];
    newcon.number = [dic valueForKey:@"numberArray"];
    

    [Contact addContactToDB:newcon];
    
    [[GlobalData sharedGlobalData] editCHcontactToDBArray:newdic title:@"add"];
}
@end

