//
//  AddEditContactVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import "Tblcell.h"

NS_ASSUME_NONNULL_BEGIN
@protocol SecVSDelegate <NSObject>

@optional
- (void)secVCDidDismisWithData:(NSMutableDictionary *)data;
@end

@interface AddEditContactVC : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong)   id<SecVSDelegate> delegate;


@property (nonatomic, strong) NSMutableDictionary *ContactDetailsDic;
@property (nonatomic, strong) NSString *controller;

@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *company;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtName;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtEmail;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtNumber;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *txtCompany;
@property (strong, nonatomic) IBOutlet UIImageView *imgUserIcon;

@property (strong, nonatomic) IBOutlet UIView *numberView1;
@property (strong, nonatomic) IBOutlet UIView *numberView2;
@property (strong, nonatomic) IBOutlet UIView *numberView3;
@property (strong, nonatomic) IBOutlet UIView *numberView4;
@property (strong, nonatomic) IBOutlet UIView *numberView5;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtemailToNum1TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtemailToNum2TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtemailToNum3TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtemailToNum4TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtemailToNum5TopConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *num2ToNum1TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *num3ToNum2TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *num4ToNum3TopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *num5ToNum4TopConstraint;



@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *plusBtn1;
@property (strong, nonatomic) IBOutlet UIButton *plusBtn2;
@property (strong, nonatomic) IBOutlet UIButton *plusBtn3;
@property (strong, nonatomic) IBOutlet UIButton *plusBtn4;
@property (strong, nonatomic) IBOutlet UIButton *plusBtn5;


@property (strong, nonatomic) IBOutlet UITableView *numbersTblView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *numbersTblHeightConstraint;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightMarkBtn;


@end

NS_ASSUME_NONNULL_END
