//
//  ContactVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ContactVC.h"
#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "ContactDetailVC.h"
#import "UIImageView+Letters.h"
#import "AddEditContactVC.h"
#import "SWTableViewCell.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "Singleton.h"
#import "GlobalData.h"
#import "NBPhoneNumberUtil.h"
#import <AVFoundation/AVFoundation.h>
#import "DialerVC.h"
#import <FirebaseCrashlytics/FirebaseCrashlytics.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@import NKVPhonePicker;

@interface ContactVC ()<UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate,SWTableViewCellDelegate>
{
    WebApiController *obj;
    NSArray *purchase_number;
    UISearchController *searchController;
    NSString *phoneNumber;
    NSString *selected_fromnumner;
    NSString *selected_callToName;
    NSString *selected_callTonumber;
    NSString *selected_calltypes;
    UIView *Blur_view;
    NSString *index_value;
    
}

@end
NSString *searchFlag = @"0";
@implementation ContactVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    [self view_design];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    index_value = @"";
    selected_fromnumner = @"";
    selected_callToName= @"";
    selected_callTonumber= @"";
    selected_calltypes= @"";
    self.flag_num_selection = @"0";
    Mixallcontact = [[NSMutableArray alloc] init];
    _filteredData = [[NSMutableArray alloc] init];
    //    [self GetNumberFromContact];
    //    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    //        [self getCallHippoContactList];
    //        [self contact_get];
    [self getNumbers];
    
    if ([[[GlobalData sharedGlobalData] chContactListFromDB] count] == 0) {
        [[GlobalData sharedGlobalData] makeArrayFromDBChContact];
    }
    
    
    //    });
}


-(void)Foreground_Action:(NSNotification *)notification
{
    //NSLog(@"\n \n \n \n Trushang_code : Foreground_Action ContactVC:");
    //     [self contact_foreground_test];
    // [self search_number];
}
-(void)contact_foreground_test
{
    
    
    //    Mixallcontact = [[NSMutableArray alloc] init];
    //    _filteredData = [[NSMutableArray alloc] init];
    
   //old  Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    
    _filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    
    //NSLog(@"ALL CONTACT :%@",_filteredData);
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    
    NSArray *arr = [_filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
    _filteredData = (NSMutableArray*)arr;
    
    //    [self search_number];
    
    _tblViewContact.delegate = self;
    _tblViewContact.dataSource = self;
    [_tblViewContact reloadData];
    
}

-(void)contact_get
{
    
    Mixallcontact = [[NSMutableArray alloc] init];
    _filteredData = [[NSMutableArray alloc] init];
    
  //old  Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    
    _filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    
    //NSLog(@"ALL CONTACT :%@",_filteredData);
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    
    NSArray *arr = [_filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
    _filteredData = (NSMutableArray*)arr;
    
    _tblViewContact.delegate = self;
    _tblViewContact.dataSource = self;
    [_tblViewContact reloadData];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [[UINavigationBar appearance] setTranslucent:false];
    UINavigationBar.appearance.translucent = false;
    index_value = @"";
    [self contact_get];
    [self view_design];
    [_tblViewContact reloadData];
    
}

-(void)view_design
{
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;
    [UtilsClass view_navigation_title:self title:@"Contacts" color:UIColor.whiteColor];
    
    
    //    _btn_add_contact.layer.cornerRadius = _btn_add_contact.frame.size.height / 2;
    //    _btn_add_contact.clipsToBounds = true;
    [UtilsClass button_shadow_boder:_btn_add_contact];
    
    //    [self getCallHippoContactList];
    //    _filteredData = [[NSArray alloc] initWithArray:Mixallcontact];
    //    _tblViewContact.delegate = self;
    //    _tblViewContact.dataSource = self;
    //    [_tblViewContact reloadData];
}

- (IBAction)btn_menu_click:(UIBarButtonItem *)sender
{
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}

- (IBAction)btn_right_menu_search:(UIBarButtonItem *)sender{
    
    if ([searchFlag isEqual: @"0"])
    {
        if(_filteredData.count != 0)
        {
            [self.tblViewContact scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                       atScrollPosition:UITableViewScrollPositionTop animated:NO];
            searchFlag = @"1";
            
            searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
            searchController.searchResultsUpdater = self;
            
            searchController.hidesNavigationBarDuringPresentation = true;
            searchController.dimsBackgroundDuringPresentation = false;
            
            searchController.delegate = self;
            searchController.searchBar.delegate = self;
            
            [searchController.searchBar sizeToFit];
            self.tblViewContact.tableHeaderView = searchController.searchBar;
            
            self.definesPresentationContext = true;
            
            [searchController setActive:YES];
            //        [searchController.searchBar becomeFirstResponder];
        }
    }else {
        if(_filteredData.count != 0)
        {
            searchFlag = @"0";
            self.tblViewContact.tableHeaderView = nil;
            [self.tblViewContact reloadData];
        }
    }
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSLog(@"update search %@",searchController.searchBar.text);
    
    if(![searchController.searchBar.text isEqualToString:@""])
    {
        [self search_number];
    }
    else
    {
        NSLog(@"else");
        [self contact_get];
    }
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.tblViewContact.tableHeaderView = nil;
    _filteredData = [[NSMutableArray alloc] init];
    _filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    searchFlag = @"0";
    self.tblViewContact.tableHeaderView = nil;
    [self.tblViewContact reloadData];
}

- (void)didPresentSearchController:(UISearchController *)searchController
{
    [searchController.searchBar becomeFirstResponder];
}



- (IBAction)btn_new_contact_click:(id)sender {
    AddEditContactVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddEditContactVC"];
    vc.controller = @"add";
    [[self navigationController] pushViewController:vc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == _tbl_number_selection)
    {
      
        return purchase_number.count;
    }
    else
    {
        return _filteredData.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _tbl_number_selection)
    {
        Tblcell *cell = [_tbl_number_selection dequeueReusableCellWithIdentifier:@"cell123"];
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        
        
        cell.img_select_con_num.image = [UIImage imageNamed:imagename];
        cell.lbl_select_con_name.text = contactName;
        cell.lbl_select_con_number.text  = phoneNumber;
        
        _tbl_number_selection.rowHeight = 64.0;
        [cell layoutIfNeeded];
        [cell.contentView layoutIfNeeded];
        if (index_value == [numberdic valueForKey:@"_id"])
        {
            cell.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:233.0/255.0 blue:230.0/255.0 alpha:1.0];
        }
        else
        {
            cell.backgroundColor = UIColor.clearColor;
        }
        return cell;
    }
    else
    {
        
        
        Tblcell *cell = [_tblViewContact dequeueReusableCellWithIdentifier:@"Tblcell"];
        NSDictionary *dic = [_filteredData objectAtIndex:indexPath.row];
        
        cell.lblAutherName.text =[dic valueForKey:@"name"];
        cell.imgIconeSms.layer.borderWidth=0.0;
        
        
        //    if ([[dic valueForKey:@"number"] isEqualToString:@"(null)"]){
        ////NSLog(@"Length : %lu",(unsigned long)[[dic valueForKey:@"number"] length]);
        //        cell.lblLatestMessage.text = @"";
        //    }else{
        
        //    }
        
        //NSLog(@"Cellforrowindexpath : %@ ",dic);
        
        
        
      /* number working
       if ([dic valueForKey:@"number"] == nil || [dic valueForKey:@"number"] == (id)[NSNull null]) {
            // nil branch
            cell.lblLatestMessage.text = @"";
        } else {
            // category name is set
            cell.lblLatestMessage.text = [dic valueForKey:@"number"];
        }*/
        
        if ([[dic objectForKey:@"numberArray"] count]>0) {
            
        [FIRAnalytics logEventWithName:@"ch_contactList" parameters:@{@"email":[Default valueForKey:kUSEREMAIL]}];

            
            [[FIRCrashlytics crashlytics] setCustomValue:[dic objectForKey:@"numberArray"]  forKey:@"NumberLableContact"];

            cell.lblLatestMessage.text = [[[dic objectForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
            
            if ([Default boolForKey:kIsNumberMask] == true) {
                cell.lblLatestMessage.text = [UtilsClass get_masked_number:[[[dic objectForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"]] ;
            }
        }
        else
        {
            cell.lblLatestMessage.text = @"";
            //cell.lblLatestMessage.text = [dic valueForKey:@"number"];
        }
        
       /*
        search display proper number
        
        if (![searchController.searchBar.text isEqualToString:@""] && searchController != nil) {
        
            NSArray *numArr = [[[dic objectForKey:@"numberArray"] allObjects] valueForKey:@"number"];
            
            NSUInteger p =0;
            for (int i =0; i<numArr.count; i++) {
                if ([[numArr objectAtIndex:i] containsString:searchController.searchBar.text]) {
                    p=i;
                }
            }
            
            
            NSLog(@"object index: %ld",(long)p);
            
            cell.lblLatestMessage.text = [numArr objectAtIndex:p];
        }
        */
        
        NSString *name = [dic valueForKey:@"name"];
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (![UtilsClass isValidNumber:name])
        {
            if (dic[@"_id"])
            {
                if(![[dic valueForKey:@"_id"] isEqualToString:@""])
                {
                    cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                }else{
                    cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                }
            }
            else
            {
                [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
                cell.imgIconeSms.layer.cornerRadius = cell.imgIconeSms.frame.size.height / 2;
                cell.imgIconeSms.layer.cornerRadius=cell.imgIconeSms.frame.size.height / 2;
                cell.imgIconeSms.layer.borderWidth=1.0;
                cell.imgIconeSms.layer.masksToBounds = YES;
                cell.imgIconeSms.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                cell.imgIconeSms.clipsToBounds = YES;
            }
        }
        else
        {
            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
        }
       
        
        NSMutableArray *leftUtilityButtons = [NSMutableArray new];
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                    title:@"Details"];
        [leftUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                   title:@"Call"];
        
        cell.leftUtilityButtons = leftUtilityButtons;
        cell.rightUtilityButtons = rightUtilityButtons;
        cell.tag = indexPath.row;
        cell.delegate = self;
        
        return cell;
        
    }
}
-(BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    //    [_tbl_logs reloadData];
    //    [cell hideUtilityButtonsAnimated:YES];
    return YES;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView == _tbl_number_selection)
    {
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        //NSLog(@"data : %@",numberdic);
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        
        [Default setValue:phoneNumber forKey:SELECTEDNO];
        [Default setValue:contactName forKey:Selected_Department];
        NSString *imagename = [[numberdic  objectForKey:@"shortName"] lowercaseString];
        [Default setValue:imagename forKey:Selected_Department_Flag];
        
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
        
        [UtilsClass make_outgoing_call_validate:self callfromnumber:selected_fromnumner ToName:selected_callToName ToNumber:selected_callTonumber calltype:selected_calltypes Mixallcontact:Mixallcontact];
        
        [self view_selectnumber_down];
    }
    else
    {
        [self view_selectnumber_down];
        
        ContactDetailVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"ContactDetailVC"];
        vc.ContactDetailsDic = [_filteredData objectAtIndex:indexPath.row];
        vc.contact_vc_selection_index = indexPath.row;
        vc.contact_vc_selection_search_text = searchController.searchBar.text;
        [[self navigationController] pushViewController:vc animated:YES];
        
    }
}


-(void)search_number
{
    NSString *num = [searchController.searchBar.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"name contains[c] %@",searchController.searchBar.text];
    
    
    //NSPredicate *predicate_number = [NSPredicate predicateWithFormat:@"number_int contains[c] %@",num];
    
    NSPredicate *predicate_number2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num];
    
    NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name, predicate_number2]];
    
    
    //    NSLog(@"MixContact : %@",Mixallcontact);
    NSArray *_filteredData1 = [Mixallcontact filteredArrayUsingPredicate:predicate_final];
    //      ////NSLog(@"_filteredData : %@",_filteredData1);
    if(_filteredData1.count != 0)
    {
        _filteredData = [[NSMutableArray alloc] init];
        _filteredData = (NSMutableArray*)_filteredData1;
        [_tblViewContact reloadData];
    }
    else
    {
        if ([searchController.searchBar.text isEqualToString:@""]) {
            _filteredData = [[NSMutableArray alloc] init];
            _filteredData = Mixallcontact;
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES];
            
            NSArray *arr = [_filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
            _filteredData = (NSMutableArray*)arr;
            [_tblViewContact reloadData];
        } else {
            _filteredData = [[NSMutableArray alloc] init];
            [_tblViewContact reloadData];
        }
        
    }
    
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    
    switch (index) {
        case 0:
        {
            [_tblViewContact reloadData];
           
            
            ContactDetailVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"ContactDetailVC"];
            vc.ContactDetailsDic = [_filteredData objectAtIndex:cell.tag];
            vc.contact_vc_selection_index = cell.tag;
            vc.contact_vc_selection_search_text = searchController.searchBar.text;
            [[self navigationController] pushViewController:vc animated:YES];
            
            break;
        }
            
        default:
            break;
    }
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        {
            
            NSDictionary *dic =  [_filteredData objectAtIndex:cell.tag];
            if ([[dic valueForKey:@"numberArray"] count]>0)
            {
                
            //if ([dic valueForKey:@"number"] == nil || [dic valueForKey:@"number"] == (id)[NSNull null]) {
                
                NSString *numberStr = [[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                
              if ([numberStr isEqualToString:@""] ||  numberStr  == nil || numberStr  == (id)[NSNull null])
            {
                
                [UtilsClass showAlert:@"Please enter valid number." contro:self];
            } else {
                
               
                [_tblViewContact reloadData];
                NSString *callfromnumber = [Default valueForKey:SELECTEDNO];
                NSString *callToName= @"";
                NSString *callTonumber = @"";
                NSString *calltypes = OUTGOING;
              
                callToName = [dic valueForKey:@"name"];
                callTonumber = numberStr;
            
               /*
                working for single device contact
                
                if (dic[@"_id"])
                {
                    if(![[dic valueForKey:@"_id"] isEqualToString:@""])
                    {

                      callToName = [dic valueForKey:@"name"];
                        if ([[dic valueForKey:@"number"] isEqualToString:@""])
                        {
                            if ([[dic valueForKey:@"numberArray"] count]>0) {
                                                                      
                                callTonumber = [[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                                                                  }
                                                              }
                    }
                    else
                    {
                        callToName = [dic valueForKey:@"name"];
                        if ([dic valueForKey:@"number"] == nil || [dic valueForKey:@"number"] == (id)[NSNull null]) {
                            callTonumber = @"";
                        } else {
                            callTonumber = [dic valueForKey:@"number"];
                        }
                        
                       
                    }
                }
                else
                {
                    callToName = [dic valueForKey:@"name"];
                    if ([dic valueForKey:@"number"] == nil || [dic valueForKey:@"number"] == (id)[NSNull null]) {
                        callTonumber = @"";
                    } else {
                        callTonumber = [dic valueForKey:@"number"];
                    }
                }
                
                */
                if([UtilsClass isNetworkAvailable])
                {
                   //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
                    
                    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                    callTonumber = [callTonumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    if(purchase_number.count > 0 || callTonumber.length == 3 || callTonumber.length == 4)
                    {
                        callTonumber = [NSString stringWithFormat:@"+%@",callTonumber];
                        NSString *numbVer = [Default valueForKey:ISDummyNumber];
                        int num = [numbVer intValue];
                        if (num == 1  || callTonumber.length == 4 || callTonumber.length == 5)
                        {
                            NSString *numbVer = [Default valueForKey:SELECTED_NUMBER_VERIFY];
                            int num = [numbVer intValue];
                            if (num == 1 || callTonumber.length == 4 || callTonumber.length == 5) {
                                NSString *credit = [Default valueForKey:CREDIT];
                                float cre = [credit floatValue];
                                if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                                    switch ([[AVAudioSession sharedInstance] recordPermission]) {
                                        case AVAudioSessionRecordPermissionGranted:
                                        {
                                            selected_fromnumner = callfromnumber;
                                            selected_callToName= callToName;
                                            selected_callTonumber= callTonumber;
                                            selected_calltypes= calltypes;
                                            NSString *numbVer = [Default valueForKey:IS_AutoSwitch];
                                            int num = [numbVer intValue];
                                            if (num == 1)
                                            {
                                                NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
                                                txtText.text = @"";
                                                [txtText insertText:selected_callTonumber];
                                                NSString *code = txtText.code ? txtText.code : @"";
                                                NSString *Country_ShotName = txtText.country.countryCode ? txtText.country.countryCode : @"";
                                                NSString *Country_FullName = txtText.country.name ? txtText.country.name : @"";
                                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.shortName  contains[c] %@",Country_ShotName];
                                                NSArray *filteredData = [purchase_number filteredArrayUsingPredicate:predicate];
                                                
                                 //pri
                                 [Default setValue:selected_callTonumber forKey:klastDialNumber];
                                 [Default setValue:txtText.country.name forKey:klastDialCountryName];
                                 [Default setValue:[NSString stringWithFormat:@"+%@",txtText.code] forKey:klastDialCountryCode];
                                 
//                                 NSLog(@"last dial code default:: %@",[Default valueForKey:klastDialCountryCode]);
//                                 NSLog(@"last dial number default:: %@",[Default valueForKey:klastDialNumber]);
//                                  NSLog(@"last dial country default:: %@",[Default valueForKey:klastDialCountryName]);
                                                
                                                
                                                if (filteredData.count != 0)
                                                {
                                                    NSDictionary *dic1 = [filteredData objectAtIndex:0];
                                                    NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                                                    NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
                                                    NSString *contactName = [numberdic objectForKey:@"contactName"];
                                                    NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                                                    index_value = [numberdic objectForKey:@"_id"];
                                                }
                                                else
                                                {
                                                }
                                            }
                                            if(callTonumber.length == 4 || callTonumber.length == 5)
                                            {
                                                [UtilsClass make_outgoing_call_validate:self callfromnumber:selected_fromnumner ToName:selected_callToName ToNumber:selected_callTonumber calltype:selected_calltypes Mixallcontact:Mixallcontact];
                                            }else{
                                                [_tbl_number_selection reloadData];
                                                [self view_selectnumber_up];
                                            }
                                            
                                            break;
                                        }
                                        case AVAudioSessionRecordPermissionDenied:
                                        {
                                            [self micPermiiton];
                                            break;
                                        }
                                        case AVAudioSessionRecordPermissionUndetermined:
                                            break;
                                        default:
                                            break;
                                            
                                    }}else {
                                        [UtilsClass showAlert:kCREDITLAW contro:self];
                                    }}else{
                                        [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                                    }
                        }
                        else
                        {
                            [UtilsClass showAlert:kDUMMYNUMBERMSG contro:self];
                        }
                        
                    }
                    else
                    {
                        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                    }
                }
                else
                {
                    [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
                }
            }
            
        }
            break;
        }
        default:
            break;
    }
}

-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                ////NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
    ////NSLog(@"User canceled picker");
}






-(void) view_selectnumber_up {
    
  
    if ([self.flag_num_selection isEqual : @"0" ])
       {
           CGRect screenRect = [[UIScreen mainScreen] bounds];
           CGFloat screenWidth = screenRect.size.width;
           CGFloat screenHeight = screenRect.size.height-64;
           Blur_view = [[UIView alloc] init];
           Blur_view.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
           Blur_view.backgroundColor = UIColor.blackColor;
           Blur_view.alpha = 0.7;
           [self.view insertSubview:Blur_view belowSubview:_view_number_selection];
           
           
           self.flag_num_selection = @"1";
           [self updateViewConstraints];
           self->_view_nmbr_selection_bottomConstraint.constant = 0;
          
       }
}
-(void)updateViewConstraints
{
    
    if ([self.flag_num_selection isEqual : @"1"])
    {
        _tbl_nmbr_selection_heightConstraint.constant = _tbl_number_selection.contentSize.height ;
       
    }
    else
    {
        _tbl_nmbr_selection_heightConstraint.constant = 0;
    }
     [super updateViewConstraints];
}
-(void) view_selectnumber_down
{
   
    if ([self.flag_num_selection isEqual : @"1" ])
        {
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenHeight = screenRect.size.height;
            [Blur_view removeFromSuperview];
    //
            
            self.flag_num_selection = @"0";
            [self updateViewConstraints];
            self->_view_nmbr_selection_bottomConstraint.constant = 100;
        }
}

-(void)getNumbers {
    purchase_number = [[NSArray alloc]init];
    //pri purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        self.tbl_number_selection.delegate = self;
        self.tbl_number_selection.dataSource = self;
        [self.tbl_number_selection reloadData];
    }
}

- (void)getNumbers:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //    [Processcall hideLoadingWithView];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
        [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //NSLog(@"Numbes : %@",response1);
            NSDictionary*dic = [response1 valueForKey:@"data"];
            purchase_number = [dic valueForKey:@"numbers"];
            
            self.tbl_number_selection.delegate = self;
            self.tbl_number_selection.dataSource = self;
            [self.tbl_number_selection reloadData];
            
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:touch.view];
    [self.view endEditing:true];
    
    
    [self view_selectnumber_down];
    
    
    
}
@end

