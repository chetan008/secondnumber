//
//  ContactVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactVC : UIViewController <CNContactViewControllerDelegate, CNContactPickerDelegate>
{
    NSMutableArray *Mixallcontact;
//     NSMutableArray *filteredData;
}
@property (nonatomic, strong) NSMutableArray *filteredData;
@property (strong, nonatomic) IBOutlet UITableView *tblViewContact;
@property (weak, nonatomic) IBOutlet UIButton *btn_add_contact;


@property (weak, nonatomic) IBOutlet UIView *view_number_selection;
@property (weak, nonatomic) IBOutlet UITableView *tbl_number_selection;
@property (strong, nonatomic) NSString *flag_num_selection;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view_nmbr_selection_bottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tbl_nmbr_selection_heightConstraint;

@end

//"username":"chetan5750892316601117858355948","password":"chetan"

NS_ASSUME_NONNULL_END
