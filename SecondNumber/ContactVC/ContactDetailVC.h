//
//  ContactDetailVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactDetailVC : UIViewController
{
    NSMutableArray *arrContactList;
    NSMutableArray *arrcontact;
    NSMutableArray *Mixallcontact;
}
@property (nonatomic, strong) NSMutableArray *filteredData;
@property (nonatomic, strong) NSMutableDictionary *ContactDetailsDic;
@property (strong, nonatomic) IBOutlet UIView *viewTopDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIButton *btnSMS;
@property (strong, nonatomic) IBOutlet UITableView *tblDetail;

@property (weak, nonatomic) IBOutlet UILabel *lblCall;
@property (weak, nonatomic) IBOutlet UILabel *lblSMS;
@property (weak, nonatomic) IBOutlet UILabel *lblEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;
@property (weak, nonatomic) IBOutlet UIView *view_callhippo_view;
@property (weak, nonatomic) IBOutlet UIView *view_contact_view;

@property (weak, nonatomic) IBOutlet UIButton *btn_contact_call;
@property (weak, nonatomic) IBOutlet UILabel *lbl_contact_call;
@property (weak, nonatomic) IBOutlet UIButton *btn_contact_sms;
@property (weak, nonatomic) IBOutlet UILabel *lbl_contact_sms;
@property (weak, nonatomic) IBOutlet UIView *view_number_selection;
@property (weak, nonatomic) IBOutlet UITableView *tbl_number_selection;
@property (strong, nonatomic) NSString *flag_num_selection;
@property NSInteger contact_vc_selection_index;
@property (strong, nonatomic) NSString *contact_vc_selection_search_text;
@property (strong, nonatomic) IBOutlet UILabel *lblEmptyConversation;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view_nmbr_selection_bottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tbl_nmbr_selection_heightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *emptyLblTopConstraint;

@property (weak, nonatomic) IBOutlet UITableView *tbl_reminderpopup;

@end

NS_ASSUME_NONNULL_END
