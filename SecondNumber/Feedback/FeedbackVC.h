//
//  FeedbackVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedbackVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *txt_view;
@property (weak, nonatomic) IBOutlet UIButton *btnFeedBack;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@end


NS_ASSUME_NONNULL_END

