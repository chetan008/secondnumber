//
//  FeedbackVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "FeedbackVC.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "DialerVC.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "Singleton.h"
#import "OnCallVC.h"
#import "GlobalData.h"


@import Firebase;
@interface FeedbackVC ()<UITextViewDelegate>
{
    WebApiController *obj;
    
}
@end

@implementation FeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [UtilsClass view_navigation_title:self title:@"Feedback"color:UIColor.whiteColor];
    _txt_view.text = @"Write Here";
    _txt_view.textColor = [UIColor lightGrayColor];
    _txt_view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _txt_view.layer.borderWidth = 1.0;
    _txt_view.layer.cornerRadius = 5.0;
    _txt_view.delegate = self;
    _btnFeedBack.layer.cornerRadius = 5.0;
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;
    
    dispatch_async( dispatch_get_main_queue(), ^{
        
     //old   NSMutableArray *arr = [[GlobalData sharedGlobalData] get_mix_contact_list];
        
        NSMutableArray *arr = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
        
        //NSLog(@"Global_Papu  feed: %lu",(unsigned long)arr.count);
        NSMutableArray *arr1 = [[GlobalData sharedGlobalData] get_phone_contact_list];
        //NSLog(@"Global_Papu  feed: %lu",(unsigned long)arr1.count);
       //old NSMutableArray *arr2 = [[GlobalData sharedGlobalData] get_callhippo_contact_list];
        
        NSMutableArray *arr2 = [[GlobalData sharedGlobalData] chContactListFromDB];
        
        //NSLog(@"Global_Papu  feed: %lu",(unsigned long)arr2.count);
    });
    
    
    
    
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    if (textView.textColor == UIColor.lightGrayColor) {
        textView.text = @"";
        textView.textColor = UIColor.blackColor;
    }
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write Here";
        textView.textColor = UIColor.lightGrayColor;
    }
}
- (IBAction)btnFeedBackClicked:(id)sender {
    if([UtilsClass isNetworkAvailable]) {
        if (![_txt_view.text isEqualToString:@"Write Here"] && ![_txt_view.text isEqualToString:@""]) {
            [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
            NSString *userId = [Default valueForKey:USER_ID];
            NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";
            NSString *Version_Info = [Default valueForKey:IS_VERSIONINFO] ? [Default valueForKey:IS_VERSIONINFO] : @"";
            NSString *url = [NSString stringWithFormat:@"feedback/%@/add",userId];
            NSDictionary *passDict = @{@"userId":userId,
                                       @"deviceType":@"ios",
                                       @"deviceInfo":[NSString stringWithFormat:@"%@ ( %@ )",Device_Info,Version_Info],
                                       @"versionInfo":VERSIONINFO,
                                       @"feedback":_txt_view.text};
           
          //berfore  [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                               options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                 error:nil];
            NSString *jsonString;
            if (! jsonData) {

            } else {
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
            
                obj = [[WebApiController alloc] init];
             // before  [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(pauseResumeResponse:response:) andDelegate:self];
            
            [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
            
        } else {
            [UtilsClass showAlert:@"Please write us some feedback!!" contro:self];
        }
    } else {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        [Default setValue:@"Dialer" forKey:SelectedSideMenu];
        [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
//        DialerVC *Dialer = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
//        for (UIViewController *controller in self.navigationController.viewControllers) {
//
//            //Do not forget to import AnOldViewController.h
//            if ([controller isKindOfClass:[Dialer class]]) {
//
//                [self.navigationController popToViewController:controller
//                                                      animated:YES];
//                return;
//            }
//        }
//
        [FIRAnalytics logEventWithName:@"ch_feedback" parameters:nil];
        
        NSArray *viewControllers = [[self navigationController] viewControllers];
        
        MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
        DialerVC *Dialer = [[self storyboard] instantiateViewControllerWithIdentifier: @"DialerVC"];
        UINavigationController *navigationController = (UINavigationController *)mainViewController.rootViewController;
        [navigationController setViewControllers:@[Dialer]];
        [UtilsClass showAlert:Feedback_success contro:self];
        
    }
    else {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
[UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
        }
        }
        @catch (NSException *exception) {
        }
    }
    
    }
}
- (IBAction)btn_menu_click:(UIBarButtonItem *)sender
{
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}


@end
