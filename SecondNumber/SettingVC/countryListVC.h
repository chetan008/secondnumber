//
//  countryListVC.h
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface countryListVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tbl_country_list;
@property (weak, nonatomic) IBOutlet UITextField *txt_search_country;
@property (weak, nonatomic) IBOutlet UIView *view_popup;

@end

NS_ASSUME_NONNULL_END

