//
//  SettingVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingVC : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UISwitch *switchReminder;
@property (strong, nonatomic) IBOutlet UISwitch *switchAvailability;
@property (strong, nonatomic) IBOutlet UISwitch *switchTimeZone;
@property (strong, nonatomic) IBOutlet UISwitch *switchACW;
@property (strong, nonatomic) IBOutlet UISwitch *switchAuto;
@property (strong, nonatomic) IBOutlet UIView *viewDevaultnumber;
@property (weak, nonatomic) IBOutlet UISwitch *switchLastCall;
@property (strong, nonatomic) IBOutlet UITextField *txtDuration;
@property (strong, nonatomic) IBOutlet UIButton *btnDurationEdit;
@property (strong, nonatomic) IBOutlet UIView *viewACWDuration;
@property (strong, nonatomic) IBOutlet UIView *viewACW;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view_acw_height_constraint;
@property (strong, nonatomic) IBOutlet UISwitch *switchMissedAlert;
@property (strong, nonatomic) IBOutlet UISwitch *switchVoicemailAlert;
@property (strong, nonatomic) IBOutlet UISwitch *switchSmsAlert;
@end

NS_ASSUME_NONNULL_END
