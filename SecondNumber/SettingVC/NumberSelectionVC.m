//
//  NumberSelectionVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//

#import "NumberSelectionVC.h"
#import "Tblcell.h"
#import "GlobalData.h"
#import "UtilsClass.h"
#import "WebApiController.h"
@interface NumberSelectionVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *purchase_number;
    NSInteger indexrow;
    WebApiController *obj;
}
@end

@implementation NumberSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    indexrow = 0;
     [UtilsClass view_navigation_title:self title:@"Select Default Number"color:UIColor.whiteColor];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    _tbl_filter.delegate = self;
    _tbl_filter.dataSource = self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return purchase_number.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [_tbl_filter dequeueReusableCellWithIdentifier:@"cell123"];
    NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
    NSDictionary *numberdic = [dic objectForKey:@"number"];
    NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
    NSString *contactName = [numberdic objectForKey:@"contactName"];
    NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
    cell.imgDefaultNumberFlag.image = [UIImage imageNamed:imagename];
    cell.lblDefaultNumberName.text = contactName;
    cell.lblDefaultNumber.text  = phoneNumber;
    
    if (indexrow == indexPath.row)
    {
        cell.imgDefaultNumberSelection.image = [UIImage imageNamed:@"Radio-on"];
    }
    else
    {
        cell.imgDefaultNumberSelection.image = [UIImage imageNamed:@"Radio-off"];
    }
    
     
    
     return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    indexrow = indexPath.row;
    [_tbl_filter reloadData];
    
    NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
    NSDictionary *numberdic = [dic objectForKey:@"number"];
    //NSLog(@"data : %@",numberdic);
    NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
    NSString *contactName = [numberdic objectForKey:@"contactName"];
    NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
    [Default setValue:phoneNumber forKey:SELECTEDNO];
    [Default setValue:[numberdic objectForKey:@"numberVerify"] forKey:SELECTED_NUMBER_VERIFY];
    [Default setValue:contactName forKey:Selected_Department];
    [Default setValue:imagename forKey:Selected_Department_Flag];
    [self updateDefaultNumber:[numberdic objectForKey:@"_id"]];
}

- (void)updateDefaultNumber:(NSString *)numberId{
    
//    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSDictionary *passDict = @{@"numberId":numberId,
                               @"userId" : userID};
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"updatedefaultnumber";
    //NSLog(@"Login Dic : %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//    NSLog(@"TRUSHANG : STATUSCODE **************10  : %@",response1);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
        [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
    
    
     if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //NSLog(@"Numbes : %@",response1);
            NSDictionary*dic = [response1 valueForKey:@"data"];
            NSArray *purchase_number = [dic valueForKey:@"numbers"];
         
         NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
         [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         
         [GlobalData sharedGlobalData].Number_Selection = [[NSMutableArray alloc] init];
         [[GlobalData sharedGlobalData].Number_Selection addObjectsFromArray:purchase_number];
            NSLog(@"Numbes : %@",[GlobalData sharedGlobalData].Number_Selection);
    }
        
    }

}
- (IBAction)btn_back_click:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
