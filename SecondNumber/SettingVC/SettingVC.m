//
//  SettingVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "SettingVC.h"
#import "MainViewController.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "NumberSelectionVC.h"
#import "GlobalData.h"
#import "UIViewController+LGSideMenuController.h"
@import Firebase;
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
@interface SettingVC ()
{
    WebApiController *obj;
    BOOL flagEditDuration;
}
@end

@implementation SettingVC

- (void)viewDidLoad {
    
    // Force a test crash
   // @[][1];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    [Default setValue:@"" forKey:SelectedSideMenu];
    //    [Default setValue:@"" forKey:SelectedSideMenuImage];
    [self viewDesign];
}

- (void)viewWillAppear:(BOOL)animated {
    [UtilsClass view_navigation_title:self title:@"Settings"color:UIColor.whiteColor];
    [Default setValue:@"Settings" forKey:SelectedSideMenu];
    [Default setValue:@"menu_selected_setting" forKey:SelectedSideMenuImage];
}

-(void)viewDesign {
    
    /*
     [[NSUserDefaults standardUserDefaults] setValue:response1[@"callReminder"] forKey:IS_CALL_REMINDER];
     [[NSUserDefaults standardUserDefaults] setValue:response1[@"displayTimezone"] forKey:IS_DISPLAY_TIME];
     */
    
    flagEditDuration = NO;
    
    [_txtDuration setDelegate:self];
    [UtilsClass SetTextFieldBottomBorder:_txtDuration];
    //[_txtDuration addTarget:self action:@selector(txt_duration_value_changed:) forControlEvents:UIControlEventEditingChanged];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap1:)];
    [self.viewDevaultnumber addGestureRecognizer:singleFingerTap];
    
    NSString *numbVer = [Default valueForKey:IS_DISPLAY_TIME];
    int isDisplayTime = [numbVer intValue];
    
    if (isDisplayTime == 1) {
        [_switchTimeZone setOn:YES animated:NO];
    }else {
        [_switchTimeZone setOn:NO animated:NO];
    }
    
    NSString *numbVer1 = [Default valueForKey:IS_CALL_REMINDER];
    int isCallReminder = [numbVer1 intValue];
    
    if (isCallReminder == 1) {
        [_switchReminder setOn:YES animated:NO];
    }else {
        [_switchReminder setOn:NO animated:NO];
    }
    
    NSString *numbVer2 = [Default valueForKey:IS_USER_LIVE];
    int isCallPlanner= [numbVer2 intValue];
    
    if (isCallPlanner == 1) {
        [_switchAvailability setOn:YES animated:NO];
    }else {
        [_switchAvailability setOn:NO animated:NO];
    }
    
    NSString *numbVer3 = [Default valueForKey:IS_ACW_ENABLE];
    int isACW= [numbVer3 intValue];
    
    //_txtDuration.text = [NSString stringWithFormat:@"%@",[Default valueForKey:ACW_TIME]];
    
    if (isACW == 1) {
        [_switchACW setOn:YES animated:NO];
        _viewACWDuration.hidden = false;
        [self updateConstraints];
    }else {
        [_switchACW setOn:NO animated:NO];
        _viewACWDuration.hidden = true;
        [self updateConstraints];
    }
    
    NSString *numbVer4 = [Default valueForKey:IS_AutoSwitch];
    int isAuto= [numbVer4 intValue];
    
    if (isAuto == 1) {
        [_switchAuto setOn:YES animated:NO];
    }else {
        [_switchAuto setOn:NO animated:NO];
    }
    // last call
    NSString *numbVer5 = [Default valueForKey:USER_LAST_CALL_DETAIL];
    int isLastCall = [numbVer5 intValue];
    
    if (isLastCall == 1) {
        [_switchLastCall setOn:YES animated:NO];
    }else {
        [_switchLastCall setOn:NO animated:NO];
    }
    //missed call alert
        NSString *numbVer6 = [Default valueForKey:kMissedCallAlert];
        int missedCallAlert = [numbVer6 intValue];
        
        if (missedCallAlert == 1) {
            [_switchMissedAlert setOn:YES animated:NO];
        }else {
            [_switchMissedAlert setOn:NO animated:NO];
        }
        
        //voicemail call alert
        NSString *numbVer7 = [Default valueForKey:kVoicemailCallAlert];
        int voicemailAlert = [numbVer7 intValue];
        
        if (voicemailAlert == 1) {
            [_switchVoicemailAlert setOn:YES animated:NO];
        }else {
            [_switchVoicemailAlert setOn:NO animated:NO];
        }
        
        //sms call alert
        NSString *numbVer8 = [Default valueForKey:kSMSCallAlert];
        int smsalert = [numbVer8 intValue];
        
        if (smsalert == 1) {
            [_switchSmsAlert setOn:YES animated:NO];
        }else {
            [_switchSmsAlert setOn:NO animated:NO];
        }
    
    [self getSettin];
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;
}
-(void)updateConstraints
{
    
    NSString *numbVer3 = [Default valueForKey:IS_ACW_ENABLE];
    int isACW= [numbVer3 intValue];
    
    if (isACW == 1) {
        _view_acw_height_constraint.constant = 100;
    }
    else
    {
        _view_acw_height_constraint.constant = 70;
    }
    
}
- (void)handleSingleTap1:(UITapGestureRecognizer *)recognizer
{
    NSMutableArray *purchase_number = [[NSMutableArray alloc]init];
     
       NSData *data = [Default valueForKey:PURCHASE_NUMBER];
       purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
       
       NSLog(@"TRUSHANG PURCHASENUMBER : %@",[[GlobalData sharedGlobalData] get_number_selection]);
       if(purchase_number.count > 0)
       {
    NSString *numbVer = [Default valueForKey:IS_DEFAULT_NUMBER_PLAN];
    int num = [numbVer intValue];
//
//    if([[GlobalData sharedGlobalData] get_number_selection].count > 0)
//    {
        if (num == 1) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NumberSelectionVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NumberSelectionVC"];
            [FIRAnalytics setUserPropertyString:@"" forName:@"select_default_number"];
            [[self navigationController] pushViewController:vc animated:YES];
        }else{
            [_switchReminder setOn:NO animated:YES];
            [FIRAnalytics setUserPropertyString:@"" forName:@"select_default_numberPlanUpgrade"];
            [UtilsClass showAlert:@"Default Number is not available in your plan Please Upgrade your plan" contro:self];
        }
    }else{
        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
    }
    
    
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (IBAction)btn_menu_click:(id)sender {
    
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}

- (void)setSetting:(NSString *)userCall setValiue:(NSString *)setVal {
    
    //[Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSDictionary *passDict;
        
        if ([userCall isEqualToString:@"missedAlertStatus"] || [userCall isEqualToString:@"voicemailAlertStatus"] || [userCall isEqualToString:@"smsAlertStatus"]) {
            
            BOOL valuebool;
            if ([setVal isEqualToString:@"true"])
                valuebool = true;
            else
                valuebool = false;
            
            passDict = @{userCall:@(valuebool)};

        }
        else
        {
            passDict = @{userCall:setVal};
        }
    
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"user/%@/setting",userId];
    //NSLog(@"Login Dic : %@",passDict);
    //NSLog(@"url %@",url);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    
    
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_PUT_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
   // [Processcall hideLoadingWithView];
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
   // NSLog(@"Set_Setting_Response : %@",response1);
    
    NSLog(@"Encrypted Response : set setting : %@",response1);

    
    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
        
        if (flagEditDuration == YES) {
            
            [UtilsClass makeToast:kDurationChangeSuccessMsg];
            flagEditDuration = NO;
            [_txtDuration resignFirstResponder];
            [Default setValue:_txtDuration.text forKey:ACW_TIME];
        }
    }

    //NSLog(@"TRUSHANG : STATUSCODE **************14  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }

}
- (IBAction)toggelReminder:(id)sender {
    //NSLog(@"Switch Reminder");
    NSString *numbVer = [Default valueForKey:IS_CALL_PLANNER];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner -------------- %@",numbVer);
    
    
    if (num == 1) {
        if([_switchReminder isOn]){
            [_switchReminder setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:IS_CALL_REMINDER];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_reminder_on"];
            [self setSetting:@"userCallReminder" setValiue:@"true"];
        }else{
            [_switchReminder setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:IS_CALL_REMINDER];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_reminder_off"];
            [self setSetting:@"userCallReminder" setValiue:@"false"];
        }
    }else{
        [_switchReminder setOn:NO animated:YES];
        [UtilsClass showAlert:@"Call Reminder is not available in your plan Please Upgrade your plan" contro:self];
    }
}

- (IBAction)toggelACW:(id)sender {
    //NSLog(@"Switch Reminder");
    NSString *numbVer = [Default valueForKey:ACW_PLAN];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner -------------- %@",numbVer);
    //_txtDuration.text = [NSString stringWithFormat:@"%@",[Default valueForKey:ACW_TIME]];
    
    if (num == 1) {
        if([_switchACW isOn]){
            
            
            
            
            [_switchACW setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:IS_ACW_ENABLE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_acw_switch_on"];
            [self setSetting:@"isAcwEnabled" setValiue:@"true"];
            
            _viewACWDuration.hidden = false;
            [self updateConstraints];
          
            
            
        }else{
            
           
            
            [_switchACW setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:IS_ACW_ENABLE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_acw_switch_off"];
            [self setSetting:@"isAcwEnabled" setValiue:@"false"];
            
            _viewACWDuration.hidden = true;
                       [self updateConstraints];
            [_txtDuration resignFirstResponder];
            
        }
    }else{
        
        _viewACWDuration.hidden = true;
        [self updateConstraints];
        
        [_switchACW setOn:NO animated:YES];
        [UtilsClass showAlert:@"After call work is not available in your plan Please Upgrade your plan" contro:self];
    }
}
-(IBAction)editACWDuration:(id)sender
{
    NSLog(@"duration int:: %d",[_txtDuration.text intValue]);
    
    if ([_txtDuration.text isEqualToString:@""]) {
         [UtilsClass makeToast:@"Please enter valid value"];
    }
    else if([_txtDuration.text intValue] < 1 )
    {
        [UtilsClass makeToast:kDurationGreaterThanMsg];
    }
    else if([_txtDuration.text intValue] > 60)
    {
        [UtilsClass makeToast:kDurationLessThanMsg];
    }
    else
    {
        flagEditDuration = YES;
        [self setSetting:@"acwDuration" setValiue:_txtDuration.text];
    }
    
}
- (IBAction)toggelAvailability:(id)sender {
    
    
    if([_switchAvailability isOn]){
        [_switchAvailability setOn:YES animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:IS_USER_LIVE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_availability_on"];
        [self setSetting:@"userLiveCallSwitch" setValiue:@"true"];
        
    }else{
        
        [_switchAvailability setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:IS_USER_LIVE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_availability_off"];
        [self setSetting:@"userLiveCallSwitch" setValiue:@"false"];
    }
}

- (IBAction)toggelDisplayTimeZone:(id)sender {
    NSString *numbVer = [Default valueForKey:IS_DISPLAY_TIME_PLAN];
    int num = [numbVer intValue];
    
    //NSLog(@"IS_DISPLAY_TIME_plan -------------- %@",numbVer);
    
    if (num == 1) {
        if([_switchTimeZone isOn]){
            [_switchTimeZone setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:IS_DISPLAY_TIME];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_timezone_on"];
            [self setSetting:@"displayTimezone" setValiue:@"true"];
        }else{
            [_switchTimeZone setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:IS_DISPLAY_TIME];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_timezone_off"];
            [self setSetting:@"displayTimezone" setValiue:@"false"];
        }
    }else{
        [_switchTimeZone setOn:NO animated:YES];
        [UtilsClass showAlert:@"Global Connect is not available in your plan Please Upgrade your plan" contro:self];
    }
}


- (IBAction)toggelAutoSwitchNumber:(id)sender {
    
    
    NSString *numbVer = [Default valueForKey:IS_AUTO_SWITCH_PLAN];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner IS_AUTO_SWITCH_PLAN -------------- %@",numbVer);
    
    if (num == 1) {
        
        if([_switchAuto isOn]){
            [_switchAuto setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:IS_AutoSwitch];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_auto_switch_on"];
            [self setSetting:@"autoSwitch" setValiue:@"true"];
        }else{
            [_switchAuto setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:IS_AutoSwitch];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_auto_switch_off"];
            [self setSetting:@"autoSwitch" setValiue:@"false"];
            
        }
    }else{
        [_switchAuto setOn:NO animated:YES];
        [UtilsClass showAlert:@"Auto switch is not available in your plan Please Upgrade your plan" contro:self];
    }
}

- (IBAction)toggelLastCallDetails:(id)sender {
    NSString *numbVer = [Default valueForKey:IS_LAST_CALL_DDETAIL_IN_PLAN];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner -------------- %@",numbVer);
    
    if (num == 1) {
        if([_switchLastCall isOn]){
            [_switchLastCall setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:USER_LAST_CALL_DETAIL];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_lastcall_switch_on"];
            [self setSetting:@"showUserLastCallDetail" setValiue:@"true"];
        }else{
            [_switchLastCall setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:USER_LAST_CALL_DETAIL];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [FIRAnalytics setUserPropertyString:@"" forName:@"set_lastcall_switch_off"];
            [self setSetting:@"showUserLastCallDetail" setValiue:@"false"];
            
        }
    }else{
        [_switchLastCall setOn:NO animated:YES];
        [UtilsClass showAlert:@"Last call detail is not available in your plan Please Upgrade your plan" contro:self];
    }
}
- (IBAction)toggelMissedAlert:(id)sender {

    if([_switchMissedAlert isOn]){
        [_switchMissedAlert setOn:YES animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kMissedCallAlert];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_missedcall_switch_on"];
        [self setSetting:@"missedAlertStatus" setValiue:@"true"];
    }else{
        [_switchMissedAlert setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:kMissedCallAlert];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_voicemail_switch_off"];
        [self setSetting:@"missedAlertStatus" setValiue:@"false"];
        
    }
    
}
- (IBAction)toggelVoicemailAlert:(id)sender {

    if([_switchVoicemailAlert isOn]){
        [_switchVoicemailAlert setOn:YES animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kVoicemailCallAlert];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_voicemail_switch_on"];
        [self setSetting:@"voicemailAlertStatus" setValiue:@"true"];
    }else{
        [_switchVoicemailAlert setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:kVoicemailCallAlert];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_voicemail_switch_off"];
        [self setSetting:@"voicemailAlertStatus" setValiue:@"false"];
        
    }
}
- (IBAction)toggelSMSAlert:(id)sender {
    
    if([_switchSmsAlert isOn]){
        [_switchSmsAlert setOn:YES animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kSMSCallAlert];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_smsalert_switch_on"];
        [self setSetting:@"smsAlertStatus" setValiue:@"true"];
    }else{
        [_switchSmsAlert setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:kSMSCallAlert];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [FIRAnalytics setUserPropertyString:@"" forName:@"set_smsalert_switch_off"];
        [self setSetting:@"smsAlertStatus" setValiue:@"false"];
        
    }
}
- (void)getSettin{
    //NSLog(@"getCredits : Trushang : Api Call : ");
    NSString *userID = [Default valueForKey:USER_ID];
    
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
    NSDictionary *passDict = @{@"userId":userID,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"token":fcmtoken,
                               @"versionInfo":VERSIONINFO};
    
    //NSLog(@"getcredit : %@",passDict);
    //NSLog(@"getcredit : %@%@",SERVERNAME,url);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getCredits:response:) andDelegate:self];
}

- (void)getCredits:(NSString *)apiAlias response:(NSData *)response{
    [Processcall hideLoadingWithView];
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"Credit_API_Response : %@",response1);
    
    //NSLog(@"TRUSHANG : STATUSCODE **************15  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            NSLog(@"credit response::: %@",response1);
            
            [Default setValue:response1[@"autoSwitchPlan"] forKey:IS_AUTO_SWITCH_PLAN];
            [Default setValue:response1[@"autoSwitch"] forKey:IS_AutoSwitch];
            [Default setValue:response1[@"credit"] forKey:CREDIT];
            [Default setValue:response1[@"callHold"] forKey:IS_HOLD];
            [Default setValue:response1[@"callTransfer"] forKey:IS_TRANSFER];
            [Default setValue:response1[@"callReminder"] forKey:IS_CALL_REMINDER];
            [Default setValue:response1[@"isAcwEnabled"] forKey:IS_ACW_ENABLE];
            
            [Default setValue:response1[@"planCallReminder"] forKey:IS_CALL_PLANNER];
            [Default setValue:response1[@"acwPlan"] forKey:ACW_PLAN];
            [Default setValue:response1[@"record"] forKey:IS_RECORDING];
            [Default setValue:response1[@"displayTimezone"] forKey:IS_DISPLAY_TIME];
            [Default setValue:response1[@"timezone"] forKey:IS_DISPLAY_TIME_PLAN];
            [Default setValue:response1[@"isUserLive"] forKey:IS_USER_LIVE];
            [Default setValue:response1[@"userLastCallDetail"] forKey:USER_LAST_CALL_DETAIL];
            [Default setValue:response1[@"isLastCallDetailInPlan"] forKey:IS_LAST_CALL_DDETAIL_IN_PLAN];
            [Default setValue:response1[@"acwDuration"] forKey:ACW_TIME];
            [Default setValue:response1[@"defaultNumberPlan"] forKey:IS_DEFAULT_NUMBER_PLAN];
            
            [Default setValue:response1[@"missedAlertStatus"] forKey:kMissedCallAlert];
            [Default setValue:response1[@"voicemailAlertStatus"] forKey:kVoicemailCallAlert];
            [Default setValue:response1[@"smsAlertStatus"] forKey:kSMSCallAlert];
            
            NSString *numbVer = [response1 valueForKey:@"callReminder"];
            int callReminder = [numbVer intValue];
            
            //NSLog(@"Callplanner -------------- %@",numbVer);
            
            if (callReminder == 1) {
                [_switchReminder setOn:YES animated:YES];
            }else {
                [_switchReminder setOn:NO animated:YES];
            }
            
            NSString *numbVer1 = [response1 valueForKey:@"isUserLive"];
            int isUserLive = [numbVer1 intValue];
            
            //NSLog(@"isUserLive -------------- %@",numbVer1);
            
            if (isUserLive == 1) {
                [_switchAvailability setOn:YES animated:YES];
            }else {
                [_switchAvailability setOn:NO animated:YES];
            }
            
            NSString *numbVer2 = [response1 valueForKey:@"displayTimezone"];
            int displayTimezone = [numbVer2 intValue];
            //NSLog(@"displayTimezone -------------- %@",numbVer2);
            
            if (displayTimezone == 1) {
                [_switchTimeZone setOn:YES animated:YES];
            }else {
                [_switchTimeZone setOn:NO animated:YES];
            }
            
            NSString *numbVer3 = [response1 valueForKey:@"autoSwitch"];
            int autoSwitch = [numbVer3 intValue];
            //NSLog(@"autoSwitch -------------- %@",numbVer3);
            
            if (autoSwitch == 1) {
                [_switchAuto setOn:YES animated:YES];
            }else {
                [_switchAuto setOn:NO animated:YES];
            }
            
            NSString *numbVer4 = [response1 valueForKey:@"isAcwEnabled"];
            int isAcwEnabled = [numbVer4 intValue];
            //NSLog(@"isAcwEnabled -------------- %@",numbVer4);
            
            _txtDuration.text = [NSString stringWithFormat:@"%@",[Default valueForKey:ACW_TIME]];
            
            if (isAcwEnabled == 1) {
                [_switchACW setOn:YES animated:YES];
                _viewACWDuration.hidden = false;
                
                [self updateConstraints];
            }else {
                [_switchACW setOn:NO animated:YES];
                _viewACWDuration.hidden = true;
               [self updateConstraints];

            }
            
            NSString *numbVer5 = [response1 valueForKey:USER_LAST_CALL_DETAIL];
            int userLastCallDetail = [numbVer5 intValue];
            //NSLog(@"userLastCallDetail -------------- %@",numbVer5);
            
            if (userLastCallDetail == 1) {
                [_switchLastCall setOn:YES animated:YES];
            }else {
                [_switchLastCall setOn:NO animated:YES];
            }

            
            NSString *numbVer6 = [response1 valueForKey:@"missedAlertStatus"];
            int missedcallalert = [numbVer6 intValue];
            //NSLog(@"userLastCallDetail -------------- %@",numbVer5);
                       
            if (missedcallalert == 1) {
                    [_switchMissedAlert setOn:YES animated:YES];
            }else {
                    [_switchMissedAlert setOn:NO animated:YES];
            }
                       
            NSString *numbVer7 = [response1 valueForKey:@"voicemailAlertStatus"];
            int voicemailalert = [numbVer7 intValue];
            //NSLog(@"userLastCallDetail -------------- %@",numbVer5);
                       
            if (voicemailalert == 1) {
                    [_switchVoicemailAlert setOn:YES animated:YES];
            }else {
                    [_switchVoicemailAlert setOn:NO animated:YES];
            }
                       
            NSString *numbVer8 = [response1 valueForKey:@"smsAlertStatus"];
            int smsalert = [numbVer8 intValue];
            //NSLog(@"userLastCallDetail -------------- %@",numbVer5);
                       
            if (smsalert == 1) {
                    [_switchSmsAlert setOn:YES animated:YES];
            }else {
                    [_switchSmsAlert setOn:NO animated:YES];
            }
            
        }
        
    }
}

#pragma mark - textfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if ([_txtDuration.text isEqualToString:@""]) {
         [UtilsClass makeToast:@"Please enter valid value"];
    }
    else if([_txtDuration.text intValue] < 1 )
       {
           [UtilsClass makeToast:kDurationGreaterThanMsg];
       }
       else if([_txtDuration.text intValue] > 60)
       {
           [UtilsClass makeToast:kDurationLessThanMsg];
       }
}





@end
