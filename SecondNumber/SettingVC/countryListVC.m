//
//  countryListVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//
#import "countryListVC.h"
#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "UIViewController+LGSideMenuController.h"

@interface countryListVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation countryListVC
{
    NSArray *arrFilter;
    NSArray *arrContry;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self viewdesign];
    [self readjson];
    // Do any additional setup after loading the view.
}

-(void)viewdesign
{
     self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    _view_popup.layer.cornerRadius = 10.0;
    _view_popup.layer.masksToBounds = YES;
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:227.0/225.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, _txt_search_country.frame.size.height - borderWidth, _txt_search_country.frame.size.width, _txt_search_country.frame.size.height);
    border.borderWidth = borderWidth;
    [_txt_search_country.layer addSublayer:border];
    _txt_search_country.layer.masksToBounds = YES;
}
-(void)readjson
{
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    NSArray *arr = [parsedObject objectForKey:@"countryList"];
    
    if (localError != nil) {
        //NSLog(@"%@", [localError userInfo]);
    }
    
    arrFilter = arr;
    arrContry = arr;
    //NSLog(@"arrFilter %@",arrFilter);
    _tbl_country_list.delegate = self;
    _tbl_country_list.dataSource = self;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrFilter.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [_tbl_country_list dequeueReusableCellWithIdentifier:@"cell123"];
    NSDictionary *dic = [arrFilter objectAtIndex:indexPath.row];
    NSString *strname = [[dic valueForKey:@"code"] lowercaseString];
    cell.lbl_con_name.text = [NSString stringWithFormat:@"%@ (%@)",[dic valueForKey:@"name"],[dic valueForKey:@"code"]];

    cell.lbl_con_code.text = [dic valueForKey:@"dial_code"];
    cell.img_con_con.image = [UIImage imageNamed:strname];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = arrFilter[indexPath.row];
    NSString *strname = [[dic valueForKey:@"code"] lowercaseString];
    [Default setValue:[dic valueForKey:@"dial_code"] forKey:selected_country_code_by_popup];
    [Default setValue:strname forKey:selected_country_image_by_popup];
    [Default setValue:[dic valueForKey:@"name"] forKey:selected_country_name_by_popup];
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"countrySet" object:self];
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)txtvaluechange:(UITextField *)sender
{
    //NSLog(@"text :%@",sender.text);
    NSArray *filteredArray = [arrContry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name contains[c] %@) OR (code contains[c] %@)", _txt_search_country.text,_txt_search_country.text.uppercaseString]];

    if (filteredArray.count == 0)
    {
        arrFilter = arrContry;
        [_tbl_country_list reloadData];
    }
    else
    {
        arrFilter = filteredArray;
         [_tbl_country_list reloadData];
    }
}
- (IBAction)btn_call_click:(UIButton *)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}


@end
