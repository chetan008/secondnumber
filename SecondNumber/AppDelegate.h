//
//  AppDelegate.h
//  SecondNumber
//
//  Created by Apple on 18/11/21.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import <PushKit/PushKit.h>
#import "twilio_callkit.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
#import "Slide.h"
#import "OnBoardVC.h"
#import "ProviderDelegate.h"

//#include "FIRMessaging.h"
@import Firebase;
@import SocketIO;
@import GoogleSignIn;
@import TwilioVoice;
@interface AppDelegate : UIResponder <UIApplicationDelegate, PKPushRegistryDelegate, UNUserNotificationCenterDelegate,FIRMessagingDelegate,PlivoEndpointDelegate,GIDSignInDelegate,TVONotificationDelegate>
{
    @private
    UIBackgroundTaskIdentifier bgStartId;
    BOOL startedInBackground;
    PKPushRegistry *pushRegistry;
    PlivoOutgoing *outCall;
    PlivoIncoming *incCall;
}

- (void)registerForNotifications;
-(void)RegisterVoipService;
-(void)RegisterVoipService_twilio;

-(void)updatefcmtoken;
- (void)soket_disconnect;
-(void)soket_connection;


@property (nonatomic, retain) UIAlertController *waitingIndicator;
@property (nonatomic, retain) NSString *configURL;
@property (nonatomic, retain) NSString *message;

@property (nonatomic, strong) UIWindow* window;
@property PKPushRegistry* voipRegistry;
@property twilio_callkit *del_twilio;
@property ProviderDelegate *del;
@property BOOL alreadyRegisteredForNotification;
@property BOOL onlyPortrait;
@property UIApplicationShortcutItem *shortcutItem;

@property (strong, nonatomic) SocketManager* manager;
@property (strong, nonatomic) SocketIOClient* socket;
@property (strong, nonatomic) NSString *Call_sid;

@property (strong, nonatomic) SocketManager* manager1;
@property (strong, nonatomic) SocketIOClient* socket1;
@property NSDictionary *extHeader;

// twilio
@property (nonatomic, strong) void(^incomingPushCompletionCallback)(void);

@property (strong, nonatomic) NSMutableDictionary* twilio_callinvite;
@property (strong, nonatomic) TVOCallInvite* callInvite_incoming;

@end


