//
//  Tblcell.h
//  SecondNumber
//
//  Created by Apple on 24/11/21.
//

#import <UIKit/UIKit.h>
#import "UURCCentralizedTokenView.h"
#import "SWTableViewCell.h"
#import "ACFloatingTextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tblcell : SWTableViewCell


@property (strong, nonatomic) IBOutlet UILabel *buy_numberLbl;
@property (strong, nonatomic) IBOutlet UIButton *btn_buy;
@property (strong, nonatomic) IBOutlet KSTokenView *buy_capabilityList;


@property (strong, nonatomic) IBOutlet UIImageView *numberslist_countryImg;
@property (strong, nonatomic) IBOutlet UILabel *numberslist_numLbl;

@property (strong, nonatomic) IBOutlet UIImageView *country_countryImg;
@property (strong, nonatomic) IBOutlet UILabel *country_nameLbl;

@property (strong, nonatomic) IBOutlet UILabel *setting_optionLbl;

@property (strong, nonatomic) IBOutlet UILabel *Availability_daysnameLbl;
@property (strong, nonatomic) IBOutlet UILabel *Availability_dayTimeLbl;

@property (strong, nonatomic) IBOutlet UILabel *buyFilter_itemName;
@property (strong, nonatomic) IBOutlet UIImageView *buyFilter_itemIcon;
@property (strong, nonatomic) IBOutlet UIButton *buyFilter_btnNext;



// dialvc
@property (weak, nonatomic) IBOutlet UIImageView *img_select_con_num;
@property (weak, nonatomic) IBOutlet UILabel *lbl_select_con_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_select_con_number;

//Drawer
@property (weak, nonatomic) IBOutlet UIImageView *imgIcone;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


//Call Logs
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UIButton *btn_play;
@property (weak, nonatomic) IBOutlet UIButton *btn_call;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UIImageView *img_networkDisturnance;
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_location;
@property (weak, nonatomic) IBOutlet UIView *viewTransfer;
@property (weak, nonatomic) IBOutlet UIImageView *img_calltype;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (strong, nonatomic) IBOutlet UIButton *btn_didslect;

//inbox logs
@property (weak, nonatomic) IBOutlet UIImageView *img_inbox_callStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_inbox_callTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_inbox_callTime;
@property (weak, nonatomic) IBOutlet UIButton *btn_inbox_next;
@property (weak, nonatomic) IBOutlet UIButton *btn_inbox_rowSelect;
@property (weak, nonatomic) IBOutlet UIImageView *img_inbox_networkStrength;

//tagging
@property (weak, nonatomic) IBOutlet UILabel *lbl_tagname;






@property (weak, nonatomic) IBOutlet UIView *view_transfer_background;
@property (weak, nonatomic) IBOutlet UILabel *lbl_transfer_text;
@property (weak, nonatomic) IBOutlet UIImageView *img_transfer_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TransferDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblTransferStatus;
@property (weak, nonatomic) IBOutlet UIButton *btn_PlayTransfer;
@property (weak, nonatomic) IBOutlet UILabel *lbl_transfer_time;
@property (weak, nonatomic) IBOutlet UIImageView *img_transfer_calltype;
@property (weak, nonatomic) IBOutlet UIButton *btn_transfer_next;

//AddCredit
@property (weak, nonatomic) IBOutlet UILabel *lblCredit;

//SMSListing
@property (weak, nonatomic) IBOutlet UILabel *lblAutherName;
@property (weak, nonatomic) IBOutlet UILabel *lblLatestMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconeSms;
@property (strong, nonatomic) IBOutlet UILabel *lblDeptName;

// chat view
@property (weak, nonatomic) IBOutlet UIView *lbl_send_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl_send_text;
@property (weak, nonatomic) IBOutlet UILabel *lbl_send_time;

@property (weak, nonatomic) IBOutlet UIView *lbl_rec_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rec_text;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rec_time;
// country list
@property (weak, nonatomic) IBOutlet UIImageView *img_con_con;
@property (weak, nonatomic) IBOutlet UILabel *lbl_con_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_con_code;

@property (nonatomic, assign) SWCellState stagecell;

// filtervc
@property (weak, nonatomic) IBOutlet UIImageView *img_filter_selection;

@property (weak, nonatomic) IBOutlet UILabel *lbl_filtername;


// filterVC
@property (weak, nonatomic) IBOutlet UILabel *lbl_filter_name;

// defaultNumberVC
@property (strong, nonatomic) IBOutlet UIImageView *imgDefaultNumberFlag;
@property (strong, nonatomic) IBOutlet UILabel *lblDefaultNumberName;
@property (strong, nonatomic) IBOutlet UILabel *lblDefaultNumber;
@property (strong, nonatomic) IBOutlet UIImageView *imgDefaultNumberSelection;


// contactdetail vc

@property (weak, nonatomic) IBOutlet UIImageView *con_det_img_call_status;
@property (weak, nonatomic) IBOutlet UILabel *con_det_lbl_call_date;
@property (weak, nonatomic) IBOutlet UILabel *con_det_lbl_call_time;
@property (weak, nonatomic) IBOutlet UILabel *con_det_lbl_call_status;
@property (weak, nonatomic) IBOutlet UILabel *con_det_lbl_call_username;

//add edit contact

@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtAddNumber;
@property (weak, nonatomic) IBOutlet UIButton *plusBtnAddNumber;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtnAddNumber;

//contact detail

@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UIButton *numberCallBtn;
@property (weak, nonatomic) IBOutlet UIButton *numberSmsBtn;
@property (weak, nonatomic) IBOutlet UIButton *numberReminderBtn;
@property (weak, nonatomic) IBOutlet UIButton *numberBlacklistBtn;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *deleteBtnTrailingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *plusBtnTrailingConstraint;


// leader board

@property (strong, nonatomic) IBOutlet UILabel *lbl_leadername;
@property (strong, nonatomic) IBOutlet UILabel *lbl_leaderNoOfCalls;
@property (strong, nonatomic) IBOutlet UIImageView *img_leaderbadge;


// inbox filter cell
@property (strong, nonatomic) IBOutlet UILabel *lbl_inboxFilterMenuItem;
@property (strong, nonatomic) IBOutlet UIImageView *img_inboxFilterMenuIcon;
@property (strong, nonatomic) IBOutlet UIButton *btn_inboxFilterMenuNext;




//feedback cell
@property (strong, nonatomic) IBOutlet UILabel *lbl_feedbackTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feedbackDesc;

//tag seelction checkbox cell
@property (strong, nonatomic) IBOutlet UILabel *lbl_acwTagListTitle;
@property (strong, nonatomic) IBOutlet UIButton *btn_acwTagCheckmark;


@end

NS_ASSUME_NONNULL_END
