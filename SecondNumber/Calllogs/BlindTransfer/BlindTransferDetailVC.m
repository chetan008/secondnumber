//
//  BlindTransferDetailVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "BlindTransferDetailVC.h"
#import "AddEditContactVC.h"
#import "UtilsClass.h"
#import "OnCallVC.h"
#import "Constant.h"
#import "NewsmsVC.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "HSUAudioStreamPlayer.h"
#import <CommonCrypto/CommonDigest.h>
#import <AVFoundation/AVFoundation.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "WebApiController.h"
#import "GlobalData.h"
#import "ChatViewControllerNew.h"
#import "Processcall.h"
#import "Tblcell.h"
#import "UIViewController+LGSideMenuController.h"
#define DOC_FILE(s) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:s]
@interface BlindTransferDetailVC ()<contactupdate,UITextViewDelegate>
{
    AVPlayer *audioPlayer;
    NSString *btn_click;
    NSString *audioString;
    
    
    NSString *strFromNumber;
    NSString *strFlag;
    NSString *strCountryCode;
    
    NSString *smsFromNumber;
    NSString *smstoNumber;
    
    NSMutableArray *responseArray;
    NSMutableDictionary *respArray;
    NSMutableArray *tblArray;
    
    NSString *acwflag;
    
    NSMutableArray *CallHistoryArr;
    NSMutableArray *details_array;
    
    NSMutableDictionary *transferData;
    
    bool is_tbl_reload;
    bool is_title_added;
    WebApiController *obj;
}
@property (nonatomic, strong) HSUAudioStreamPlayer *player;
@end

@implementation BlindTransferDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    btn_click = @"0";
    acwflag = @"0";
    audioString = @"";
    Mixallcontact = [[NSMutableArray alloc] init];
    //    [self GetNumberFromContact];
    //    [self getCallHippoContactList];
    details_array = [[NSMutableArray alloc] init];
    [self contact_get];
    
    [UtilsClass view_navigation_title:self title:@"Call Details"color:UIColor.whiteColor];
    
    transferData = [_userData valueForKey:@"transfers"];
    //NSLog(@"Transfer data : %@",transferData);
    strFlag = [_userData valueForKey:@"shortName"];
    strCountryCode = [_userData valueForKey:@"countryCode"];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    [self viewUpdate];
    //    });
    
    //    double delayInSeconds = 0.1;
    //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    //        [self viewUpdate];
    //    });
    
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    is_tbl_reload = true;
    is_title_added = false;
    
    tblArray = [[NSMutableArray alloc] init];
    responseArray = [[NSMutableArray alloc] init];
    
}


-(void)Foreground_Action:(NSNotification *)notification
{
    //NSLog(@"\n \n \n \n Trushang_code : Foreground_Action NEW_SMS_VC:");
    [self contact_get];
    
}
-(void)contact_get
{
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(void)viewUpdate {
    
    
    [UtilsClass button_shadow_boder:_btnCall];
    [UtilsClass button_shadow_boder:_img_user];
    _imgRecording.image = [UIImage imageNamed:@"callRecording"];
    _lblRecordingType.text = @"Call Recording";
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *expectedTime = [dateFormatter1 dateFromString:[_userData valueForKey:@"createdDate"]];
    
    dateFormatter1 = [[NSDateFormatter alloc] init] ;
    [dateFormatter1 setDateFormat:@"MMM d, yyyy h:mm a"];// here set format which you want...
    
    NSString *createdDate = [dateFormatter1 stringFromDate:expectedTime]; //here convert date in NSString
    
    _lblDate.text = createdDate;
    _txt_acw.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _txt_acw.layer.borderWidth = 1.0;
    _txt_acw.layer.cornerRadius = 5.0;
    _txt_acw.delegate = self;
    //NSLog(@"callNotes Check %@",[_userData valueForKey:@"callNotes"]);
    if([_userData valueForKey:@"callNotes"])
    {
        if([[_userData valueForKey:@"callNotes"] isEqualToString:@""]){
            _btnNote.hidden = true;
            _btnCallNotes.hidden = true;
        }else{
            _btnNote.hidden = false;
            _btnCallNotes.hidden = false;
        }
    }
    else
    {
        _btnNote.hidden = true;
    }
    
    
    
    if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
    {
        
        smsFromNumber = [_userData valueForKey:@"to"];
        smstoNumber = [_userData valueForKey:@"from"];
        if ([[_userData valueForKey:@"fromName"]  isEqual: @""])
        {
            _lbl_name.text = [_userData valueForKey:@"from"];// "\(userData?.from! ?? "")";
        }
        else
        {
            _lbl_name.text = [_userData valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
            
        }
        
        strFromNumber = [_userData valueForKey:@"to"];
        //        [Default setValue:[_userData valueForKey:@"to"] forKey:SELECTEDNO];
        
        if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
        {
            _lbl_number.hidden = true;
            _lbl_number.text = [_userData valueForKey:@"from"];
        }else {
            _lbl_number.hidden = false;
            _lbl_number.text = [_userData valueForKey:@"from"];//"\(userData?.from! ?? "")";
        }
        
        if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Rejected"]){
            _img_callstatus.image = [UIImage imageNamed:@"rejected"];
            
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Completed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"incoming_Complete"];
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Missed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"missedincoming"];
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Voice Mail"]) {
            _img_callstatus.image = [UIImage imageNamed:@"incoming_Complete"];
            _imgRecording.image = [UIImage imageNamed:@"voiceMail"];
            _lblRecordingType.text = @"Voicemail Recording";
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Unavailable"]){
            
        }else{
            _img_callstatus.image = [UIImage imageNamed:@"incoming"];
        }
        
        _lbl_callstatus.text = [transferData valueForKey:@"callStatus"][0];
        
    }
    else
    {
        smsFromNumber = [_userData valueForKey:@"from"];
        smstoNumber = [_userData valueForKey:@"to"];
        if ([[_userData valueForKey:@"toName"]  isEqual: @""])
        {
            _lbl_name.text =  [_userData valueForKey:@"to"];//"\(userData?.to! ?? "")"
        }
        else
        {
            _lbl_name.text = [_userData valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
        }
        strFromNumber = [_userData valueForKey:@"from"];
        //    [Default setValue:[_userData valueForKey:@"from"] forKey:SELECTEDNO];
        if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
        {
            _lbl_number.text = [_userData valueForKey:@"to"];
            _lbl_number.hidden = true;
        }else {
            _lbl_number.text = [_userData valueForKey:@"to"];
            _lbl_number.hidden = false;
        }
        
        if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Rejected"]){
            _img_callstatus.image = [UIImage imageNamed:@"rejected"];
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Completed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Missed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"missedout"];
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Voice Mail"]) {
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
            _imgRecording.image = [UIImage imageNamed:@"voiceMail"];
            _lblRecordingType.text = @"Voicemail Recording";
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"transfer"]) {
            
        }else if ([[transferData valueForKey:@"callStatus"][0] isEqualToString:@"Cancelled"]) {
            _img_callstatus.image = [UIImage imageNamed:@"missedout"];
        }else{
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
        }
        
        // "\(userData?.to! ?? "")"
        
        _lbl_callstatus.text = [transferData valueForKey:@"callStatus"][0];
        
    }
    //    _lbl_time.text = [transferData valueForKey:@"callDuration"][0];//"\(userData?.time! ?? "")"
    
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    
    
    NSString *recordingDisplay = [NSString stringWithFormat:@"%@",[transferData valueForKey:@"recordingDisplay"][0]];
    
    if ([recordingDisplay isEqualToString:@"1"]){
        [self playerFunction];
    }else{
        _view_record.hidden = true;
    }
    
    //    });
    
    
    // trushang remove
    self.view_profile.backgroundColor = [UIColor colorWithRed:171 green:178 blue:186 alpha:1.0f];
    self.view_profile.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    self.view_profile.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.view_profile.layer.shadowOpacity = 1.0f;
    self.view_profile.layer.shadowRadius = 0.0f;
    self.view_profile.layer.masksToBounds = NO;
    self.view_profile.layer.cornerRadius = 0.0f;
    
    //
}

-(void)playerFunction {
    if (![[transferData valueForKey:@"recordingUrl"][0]  isEqual: @""]) {
        if ([[transferData valueForKey:@"callStatus"][0]  isEqual: @"Completed"] || [[transferData valueForKey:@"callStatus"][0]  isEqual: @"Voice Mail"])
        {
            _view_record.hidden = false;
            //   _lbl_record_time.text = [_userData valueForKey:@"callDuration"];//"\(userData?.callDuration! ?? "")"
            _lbl_record_time.text = @"00:00";
            
            audioString = [_userData valueForKey:@"recordingUrl"];
            
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //        [self audio_url_time];
            //                 });
        }
        
    }else{
        _view_record.hidden = true;
        
    }
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self.player stop];
}

- (IBAction)btn_back_click:(id)sender {
    _userData = nil;
    _userData = [[NSMutableDictionary alloc]init];
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btn_note_click:(UIButton *)sender
{
    
    NSString *numbVer = [Default valueForKey:ACW_PLAN];
    int num = [numbVer intValue];
    //NSLog(@"Callplanner -------------- %@",numbVer);
    if (num == 1) {
        if([acwflag isEqualToString:@"0"])
        {
            acwflag = @"1";
            _txt_acw.text = [NSString stringWithFormat:@"%@",[transferData valueForKey:@"callNotes"][0]];
            _view_acw_display.hidden = false;
            _btnCallNotes.hidden = true;
            [self updateConstraints];
            
        }
        else
        {
            acwflag = @"0";
            _view_acw_display.hidden = true;
            _btnCallNotes.hidden = false;
        }
    }else{
        [UtilsClass showAlert:@"After call work is not available in your plan Please Upgrade your plan" contro:self];
    }
    
    
}

- (IBAction)btnCallNotes:(id)sender {
    _btnCallNotes.hidden = true;
    NSString *numbVer = [Default valueForKey:ACW_PLAN];
    int num = [numbVer intValue];
    //NSLog(@"Callplanner -------------- %@",numbVer);
    if (num == 1) {
        if([acwflag isEqualToString:@"0"])
        {
            acwflag = @"1";
            _txt_acw.text = [NSString stringWithFormat:@"%@",[transferData valueForKey:@"callNotes"][0]];
            _view_acw_display.hidden = false;
            [self updateConstraints];
            
        }
        else
        {
            acwflag = @"0";
            _view_acw_display.hidden = true;
        }
    }else{
        [UtilsClass showAlert:@"After call work is not available in your plan Please Upgrade your plan" contro:self];
    }
}
- (IBAction)btnCallNotsBack:(id)sender {
    acwflag = @"0";
    _view_acw_display.hidden = true;
    _btnCallNotes.hidden = false;
}







- (IBAction)btn_call_click:(UIButton *)sender
{
   //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        NSString *credit = [Default valueForKey:CREDIT];
        float cre = [credit floatValue];
        if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
            
            [Default removeObjectForKey:@"extraHeader"];
            //NSLog(@"Contact details name : %@",_userData);
            NSString *Departmentcall = OUTGOING;
            if ([_userData valueForKey:@"contactId"] != nil)
            {
                if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
                {
                    
                    Departmentcall = OUTGOING;
                }
                else
                {
                    
                    if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
                    {
                        
                        Departmentcall = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"toName"]];
                    }
                    else
                    {
                        Departmentcall = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"fromName"]];
                    }
                }
            }
            else
            {
                Departmentcall = OUTGOING;
            }
            
            NSString *callfromnumber = @"";// = [Default valueForKey:SELECTEDNO];
            NSString *callToName= @"";
            NSString *callTonumber = @"";
            NSString *callDepartment = @"";
            
            if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
            {
                if ([[_userData valueForKey:@"fromName"]  isEqual: @""])
                {
                    callToName = [_userData valueForKey:@"from"];// "\(userData?.from! ?? "")";
                }
                else
                {
                    callToName = [_userData valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
                    
                }
                
                callfromnumber = [_userData valueForKey:@"to"];
                
                [Default setValue:callfromnumber forKey:SELECTEDNO];
                //        [Default setValue:callDepartment forKey:Selected_Department];
                callTonumber = [_userData valueForKey:@"from"];//"\(userData?.from! ?? "")";
                callDepartment = [NSString stringWithFormat:@"%@ ",[_userData valueForKey:@"toName"]];
                [Default setValue:callDepartment forKey:Selected_Department];
                //                if ([[_userData valueForKey:@"toName"]  isEqual: @""])
                //                {
                //
                //                    //             img_callflag.image = UIImage(named: "")
                //                    _lbl_department.text = [_userData valueForKey:@"to"];// "\(userData?.to! ?? "")"
                //
                //                } else
                //                {
                //                    //             img_callflag.image = UIImage(named: "")
                //                    _lbl_department.text = [_userData valueForKey:@"toName"];//"\(userData?.toName! ?? "")"
                //                }
            }
            else
            {
                
                if ([[_userData valueForKey:@"toName"]  isEqual: @""])
                {
                    callToName =  [_userData valueForKey:@"to"];//"\(userData?.to! ?? "")"
                }
                else
                {
                    callToName = [_userData valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
                }
                callfromnumber = [_userData valueForKey:@"from"];
                
                [Default setValue:callfromnumber forKey:SELECTEDNO];
                //      [Default setValue:callDepartment forKey:Selected_Department];
                callTonumber = [_userData valueForKey:@"to"];// "\(userData?.to! ?? "")"
                callDepartment = [NSString stringWithFormat:@"%@ ",[_userData valueForKey:@"fromName"]];
                [Default setValue:callDepartment forKey:Selected_Department];
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
            NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
            
            NSLog(@"results results : %lu",(unsigned long)results.count);
            NSString *numbVer = @"";
            if (results.count == 0){
                numbVer = @"1";
            }else{
                NSDictionary *dic1 = [results objectAtIndex:0];
                numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
            }
            int num = [numbVer intValue];
            if (num == 1) {
                NSString *credit = [Default valueForKey:CREDIT];
                float cre = [credit floatValue];
                if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                    
                    switch ([[AVAudioSession sharedInstance] recordPermission]) {
                        case AVAudioSessionRecordPermissionGranted:
                        {
                            //NSLog(@"ToNumber logs : %@",_lbl_number.text);
                            [UtilsClass make_outgoing_call_validate:self callfromnumber:callfromnumber ToName:_lbl_name.text ToNumber:_lbl_number.text calltype:callDepartment Mixallcontact:Mixallcontact];
                            break;
                        }
                        case AVAudioSessionRecordPermissionDenied:
                        {
                            //                [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
                            [self micPermiiton];
                            break;
                        }
                        case AVAudioSessionRecordPermissionUndetermined:
                            // This is the initial state before a user has made any choice
                            // You can use this spot to request permission here if you want
                            break;
                        default:
                            break;
                            
                    }
                }else {
                    [UtilsClass showAlert:kCREDITLAW contro:self];
                }
            }else{
                [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
            }
        }else{
            [UtilsClass showAlert:kCREDITLAW contro:self];
        }}else{
            [UtilsClass showAlert:kNUMBERASSIGN contro:self];
        }
}

- (IBAction)btn_play_click:(UIButton*)sender {
    NSString *istransfer = [Default valueForKey:IS_RECORDING];
    int trans = [istransfer intValue];
    if([[transferData valueForKey:@"callStatus"][0]  isEqual: @"Voice Mail"])
    {
        if ([btn_click  isEqual: @"0"])
        {
            [self audio_start];
            //        [self playaudio];
            //        [self start_timer];
            btn_click = @"1";
            UIImage *img = [UIImage imageNamed:@"AudioStop"];
            [sender setImage:img forState:UIControlStateNormal];
        }
        else
        {
            //        [self stopplay];
            //        [self stop_timer];
            [self audio_start];
            btn_click = @"0";
            UIImage *img = [UIImage imageNamed:@"AudioPlay"];
            [sender setImage:img forState:UIControlStateNormal];
            // _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
        }
    }
    else
    {
        //        if (trans == 1) {
        if ([btn_click  isEqual: @"0"])
        {
            [self audio_start];
            //        [self playaudio];
            //        [self start_timer];
            btn_click = @"1";
            UIImage *img = [UIImage imageNamed:@"AudioStop"];
            [sender setImage:img forState:UIControlStateNormal];
        }
        else
        {
            //        [self stopplay];
            //        [self stop_timer];
            [self audio_start];
            btn_click = @"0";
            UIImage *img = [UIImage imageNamed:@"AudioPlay"];
            [sender setImage:img forState:UIControlStateNormal];
            // _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
        }
        //        }else {
        //            [UtilsClass showAlert:@"Call Recording is not Available in selected plan." title:@"Please Upgrade your plan." contro:self];
        //        }
    }
    
    
    
    
    
}


-(void) playaudio{
    @try {
        NSURL *audioURL = [NSURL URLWithString:audioString];
        AVPlayerItem *PlayerItem = [AVPlayerItem playerItemWithURL:audioURL];
        AVPlayer *audioPlayer = [AVPlayer playerWithPlayerItem:PlayerItem];
        audioPlayer.volume = 1.0;
        [audioPlayer play];
        //        print("Play")
        //        start_timer()
    } @catch(NSException *exp) {
        //        print("Error")
    }
}

-(void) stopplay
{
    [audioPlayer pause];
}

-(void)start_timer
{
    NSString *time = [transferData valueForKey:@"callDuration"][0];
    //NSLog(@"time %@",time);
    if (!_myTimer) {
        _myTimer = [self createTimer];
    }
}
-(void)stop_timer
{
    [_myTimer invalidate];
}
- (NSTimer *)createTimer {
    return [NSTimer scheduledTimerWithTimeInterval:1.0
                                            target:self
                                          selector:@selector(timerTicked:)
                                          userInfo:nil
                                           repeats:YES];
}
- (void)timerTicked:(NSTimer *)timer {
    
    NSString *time = [transferData valueForKey:@"callDuration"][0];
    NSArray *components = [time componentsSeparatedByString:@":"];
    NSInteger minutes1 = [[components objectAtIndex:0] integerValue];
    NSInteger seconds1 = [[components objectAtIndex:1] integerValue];
    NSNumber *seconds12 = [NSNumber numberWithInteger:(minutes1 * 60) + seconds1];
    //NSLog(@"Totla sec : %@",seconds12);
    
    _currentTimeInSeconds++;
    
    
    _slider_view.maximumValue = seconds12.floatValue;
    _slider_view.value = [NSString stringWithFormat:@"%02d",_currentTimeInSeconds].floatValue;
    
    int seconds = _currentTimeInSeconds % 60;
    int minutes = (_currentTimeInSeconds / 60) % 60;
    int hours = _currentTimeInSeconds / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    
    if([finalTime isEqualToString:time])
    {
        //         _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
        //        [self audio_url_time];
        [self stop_timer];
    }
    else
    {
        _lbl_record_time.text = finalTime;
    }
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                //NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}





- (IBAction)slider_value_change:(UISlider *)sender
{
    
    [self stopProgressMonitor];
}
- (IBAction)slider_touch_click:(UISlider *)sender
{
    //    [self startProgressMonitor];
    //    UISlider *slider = sender;
    double duration = self.player.duration;
    [self.player seekToTime:sender.value * duration];
    
    
    int curent_time = sender.value;
    int seconds = curent_time % 60;
    int minutes = (curent_time / 60) % 60;
    int hours = curent_time / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    _lbl_record_time.text = finalTime;
    [self startProgressMonitor];
}


// custome code
-(void)audio_start
{
    HSUAudioStreamPlayBackState state = self.player.state;
    if (!self.player || state == HSU_AS_STOPPED) {
        NSString *urlString = audioString;
        NSString *urlHash = [NSString stringWithFormat:@"%@", audioString];
        NSString *cacheFile = DOC_FILE(urlHash);
        self.player = [[HSUAudioStreamPlayer alloc]
                       initWithURL:[NSURL URLWithString:urlString]
                       cacheFilePath:cacheFile];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(playBackStateChanged:)
         name:HSUAudioStreamPlayerStateChangedNotification
         object:self.player];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(playBackDurationChanged:)
         name:HSUAudioStreamPlayerDurationUpdatedNotification
         object:self.player];
        [self.player play];
    }
    else if (state == HSU_AS_PLAYING) {
        [self.player pause];
    }
    else if (state == HSU_AS_WAITTING) {
        [self.player stop];
    }
    else if (state == HSU_AS_PAUSED) {
        [self.player play];
    }
    else if (state == HSU_AS_STOPPED) {
        [self.player play];
    }
    else if (state == HSU_AS_FINISHED) {
        [self.player play];
        
    }
    
}
- (void)startProgressMonitor
{
    [self.myTimer invalidate];
    //    NSString *time = [_userData valueForKey:@"callDuration"];
    //    NSArray *components = [time componentsSeparatedByString:@":"];
    //    NSInteger minutes1 = [[components objectAtIndex:0] integerValue];
    //    NSInteger seconds1 = [[components objectAtIndex:1] integerValue];
    //    NSNumber *seconds12 = [NSNumber numberWithInteger:(minutes1 * 60) + seconds1];
    //    //NSLog(@"Totla sec : %@",seconds12);
    //
    //    _slider_view.maximumValue = seconds12.floatValue;
    btn_click = @"1";
    UIImage *img = [UIImage imageNamed:@"AudioStop"];
    [_btn_img_record setImage:img forState:UIControlStateNormal];
    self.myTimer = [NSTimer
                    scheduledTimerWithTimeInterval:1.0
                    target:self
                    selector:@selector(updateProgress)
                    userInfo:nil
                    repeats:YES];
}

- (void)stopProgressMonitor
{
    [self.myTimer invalidate];
    btn_click = @"0";
    UIImage *img = [UIImage imageNamed:@"AudioPlay"];
    [_btn_img_record setImage:img forState:UIControlStateNormal];
    //    _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
    _lbl_record_time.text = @"00:00";
    // [self audio_url_time];  _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
}
- (void)stopProgressMonitor1
{
    [self.myTimer invalidate];
    btn_click = @"0";
    UIImage *img = [UIImage imageNamed:@"AudioPlay"];
    [_btn_img_record setImage:img forState:UIControlStateNormal];
    //    _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
    //    [self audio_url_time];
}

- (void)updateProgress
{
    double progress = self.player.progress;
    self.slider_view.value = progress;
    
    //    self.lbl_record_time.text = [NSString
    //                                  stringWithFormat:@"%gs",
    //                                  ceil(self.player.currentTime)];
    
    int curent_time = self.player.currentTime;
    int seconds = curent_time % 60;
    int minutes = (curent_time / 60) % 60;
    int hours = curent_time / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    _lbl_record_time.text = finalTime;
    //    [self.lbl_time sizeToFit];
}
- (void)playBackDurationChanged:(NSNotification *)notification
{
    //    self.lbl_record_time.text = [NSString
    //                               stringWithFormat:@"%gs",
    //                               ceil(self.player.duration)];
    //    [self.lbl_record_time sizeToFit];
}

- (void)playBackStateChanged:(NSNotification *)notification
{
    HSUAudioStreamPlayer *player = notification.object;
    HSUAudioStreamPlayBackState state = player.state;
    
    if (state == HSU_AS_PLAYING) {
        //        self.lbl_record_time.text = [NSString
        //                                   stringWithFormat:@"%gs",
        //                                   ceil(self.player.duration)];
        //        [self.lbl_record_time sizeToFit];
        [self startProgressMonitor];
    }
    else if (state == HSU_AS_PAUSED) {
        
        [self stopProgressMonitor1];
    }
    else if (state == HSU_AS_WAITTING) {
        
        [self startProgressMonitor];
    }
    else if (state == HSU_AS_STOPPED) {
        
        [self updateProgress];
        [self stopProgressMonitor];
    }
    else if (state == HSU_AS_FINISHED) {
        
        [self updateProgress];
        [self stopProgressMonitor];
    }
}
-(void)audio_url_time
{
    NSURL *url = [NSURL URLWithString:audioString];
    AVURLAsset* audioAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    CMTime audioDuration = audioAsset.duration;
    float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
    
    int curent_time = audioDurationSeconds;
    int seconds = curent_time % 60;
    int minutes = (curent_time / 60) % 60;
    int hours = curent_time / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    _lbl_record_time.text = finalTime;
}
#pragma mark - Phone Contact
-(void)GetNumberFromContact
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        //NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
            // Contact one each function block is executed whenever you get
            NSString *phoneNumber = @"";
            if( contact.phoneNumbers)
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            
            //             //NSLog(@"phoneNumber = %@", phoneNumber);
            //             //NSLog(@"givenName = %@", contact.givenName);
            //             //NSLog(@"familyName = %@", contact.familyName);
            //             //NSLog(@"email = %@", contact.emailAddresses);
            
            
            NSString *FullName = [NSString stringWithFormat:@"%@%@",contact.givenName,contact.familyName];
            NSString *Number = [NSString stringWithFormat:@"%@",phoneNumber];
            if([Number isEqualToString:@"(null)"])
            {
                Number = @"";
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:FullName forKey:@"name"];
            [dic setObject:Number forKey:@"number"];
            [arrContactList addObject:contact];
            [Mixallcontact addObject:dic];
        }];
        
        //        //NSLog(@"arr contact : %lu",(unsigned long)arrContactList.count);
        
    }
}
#pragma mark - CallHippo Contact
-(void)getCallHippoContactList
{
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contact",userId];
    
    obj = [[WebApiController alloc] init];
    
    NSLog(@"URL Contact check : %@",aStrUrl);

    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallHippoContactListresponse:response:) andDelegate:self];
}

- (void)getCallHippoContactListresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        
        //NSLog(@"getCallHippoContactListresponse : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            arrcontact = [response1 valueForKey:@"data"];
            [Mixallcontact addObjectsFromArray:arrcontact];
        } else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
}


- (void) updateConstraints {
    CGSize contentSize = [_txt_acw sizeThatFits:CGSizeMake(_txt_acw.frame.size.width, FLT_MAX)];
    
    [_txt_acw.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            constraint.constant = contentSize.height;
            *stop = YES;
        }
    }];
    
    
}

@end

