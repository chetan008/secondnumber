//
//  BlindTransferDetailVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BlindTransferDetailVC : UIViewController
{
    NSMutableArray *arrContactList;
    NSMutableArray *arrcontact;
    NSMutableArray *Mixallcontact;
    
}
@property (strong, nonatomic) NSMutableDictionary *userData;
//
@property (weak, nonatomic) IBOutlet UIView *view_profile;
@property (strong, nonatomic) IBOutlet UIButton *img_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_number;
 

//
@property (weak, nonatomic) IBOutlet UIView *view_callstatus;
@property (weak, nonatomic) IBOutlet UIImageView *img_callstatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_callstatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;

//
@property (weak, nonatomic) IBOutlet UIView *view_record;
@property (weak, nonatomic) IBOutlet UIButton *btn_img_record;
@property (weak, nonatomic) IBOutlet UILabel *lbl_record_time;

//@IBOutlet weak var view_progressbar: AMProgressBar!

@property (weak, nonatomic) IBOutlet UISlider *slider_view;

@property (weak, nonatomic) NSTimer *myTimer;
@property int currentTimeInSeconds;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIView *view_no_credit;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblRecordingType;
@property (strong, nonatomic) IBOutlet UIImageView *imgRecording;

@property (weak, nonatomic) IBOutlet UIView *view_acw_display;
@property (weak, nonatomic) IBOutlet UITextView *txt_acw;

@property (strong, nonatomic) IBOutlet UIButton *btnNote;
@property (strong, nonatomic) IBOutlet UIButton *btnCallNotes;


@property (strong, nonatomic) IBOutlet UIScrollView *scroll_call_detail;

@end

NS_ASSUME_NONNULL_END
