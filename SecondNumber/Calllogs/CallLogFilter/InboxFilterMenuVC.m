//
//  InboxFilterMenuVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "InboxFilterMenuVC.h"
#import "Tblcell.h"
#import "Constant.h"
#import "UtilsClass.h"


@interface InboxFilterMenuVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *mainMenuArray;
    NSMutableArray *subMenuArray;
    
    NSArray *tagIdArray;
    NSString *selectedTagId;

    BOOL isSubMenu;
    BOOL isTagFilter;
    
    NSMutableArray *tagArray;

}
@end

@implementation InboxFilterMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
   mainMenuArray = [NSArray arrayWithObjects:@"Inbox Filter",@"Tag Filter",@"Mark as Complete",nil];
         
    //subMenuArray = [[NSMutableArray alloc]init];
    
    

    [self view_design];
    
    
    
    
    
}
-(void)view_design
{
    if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat topPadding = window.safeAreaInsets.top;
            CGFloat bottomPadding = window.safeAreaInsets.bottom;
            
            CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                                    (self.navigationController.navigationBar.frame.size.height ?: 0.0));
            
            //NSLog(@"Trushang bottomPadding : %f", topPadding);
            //NSLog(@"Navframe Height=%f",
    //              topbarHeight);
            if (topPadding < 21.0)
            {
                _top_constraint.constant = 44.0;
            }
            else
            {
                 _top_constraint.constant = 44.0;
            }

        }
        else
        {
            _top_constraint.constant = 44.0;
       }
        
   
    
        isSubMenu =false;
        _tbl_filter.delegate = self;
        _tbl_filter.dataSource = self;
        _tbl_filter.scrollEnabled = NO;
    
        [_tbl_filter reloadData];
        
        
        [self tbl_size];
    
}
#pragma  mark - tableview datasource delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isSubMenu)
        return mainMenuArray.count;
    else
        return subMenuArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Tblcell *cell = [_tbl_filter dequeueReusableCellWithIdentifier:@"cell123"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!isSubMenu)
    {
        cell.lbl_inboxFilterMenuItem.text = mainMenuArray[indexPath.row];
        
        if (indexPath.row == 0)
        {
            cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inbox_filter"];
            cell.btn_inboxFilterMenuNext.hidden = false;
            [cell.btn_inboxFilterMenuNext addTarget:self action:@selector(inboxNextClick:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        if (indexPath.row == 1)
        {
            cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inbox_filter"];
            cell.btn_inboxFilterMenuNext.hidden = false;
            [cell.btn_inboxFilterMenuNext addTarget:self action:@selector(inboxNextClick:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else if (indexPath.row == 2)
        {
               cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inbox_markascomplete"];
            cell.btn_inboxFilterMenuNext.hidden = true;

        }

        return cell;
    }
    else
    {
        NSLog(@"highlight >>> %@",_filterStrToHighlight);
        
        
//        if ([subMenuArray[indexPath.row] isEqualToString:_filterStrToHighlight]) {
//
//            NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
//
//            NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",_filterStrToHighlight] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
//
//                [titleStr appendAttributedString:FromTitle];
//            cell.lbl_inboxFilterMenuItem.attributedText = titleStr;
//        }
//        else
//        {
//            cell.lbl_inboxFilterMenuItem.text = subMenuArray[indexPath.row];
//        }
        
        if (isTagFilter) {
            
            cell.lbl_inboxFilterMenuItem.text = subMenuArray[indexPath.row];
            
            cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"tagIcon_black"];
            
            cell.btn_inboxFilterMenuNext.hidden = true;
            
            if ([_filterStrToHighlight isEqualToString:subMenuArray[indexPath.row]]) {
                
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                    
                NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",subMenuArray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                [titleStr appendAttributedString:FromTitle];
                cell.lbl_inboxFilterMenuItem.attributedText = titleStr;
            }
            
         /*   Tblcell *cell = [_tagselectionTable dequeueReusableCellWithIdentifier:@"acwTagCell"];
           
           
           cell.lbl_acwTagListTitle.text = [[subMenuArray valueForKey:@"name"]  objectAtIndex:indexPath.row];
           
           if ([[_selectedTag valueForKey:@"id"] containsObject:[[subMenuArray valueForKey:@"id"] objectAtIndex:indexPath.row]]) {

               [cell.btn_acwTagCheckmark setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
           }
           else
           {
                [cell.btn_acwTagCheckmark setImage:[UIImage imageNamed:@"blank-check-box"] forState:UIControlStateNormal];
           }*/
           
           return cell;
            
            
        }
        else
        {
            cell.lbl_inboxFilterMenuItem.text = subMenuArray[indexPath.row];

            if (indexPath.row == 0)
            {
                cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inboxFilter_missed"];
                cell.btn_inboxFilterMenuNext.hidden = true;
                
                if ([_filterStrToHighlight isEqualToString: MISSEDINBOX]) {
                    
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                    
                NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",subMenuArray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                [titleStr appendAttributedString:FromTitle];
                cell.lbl_inboxFilterMenuItem.attributedText = titleStr;
                }

            }
            else if (indexPath.row == 1)
            {
                cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inboxStatus_voicemail"];
                cell.btn_inboxFilterMenuNext.hidden = true;
                
                if ([_filterStrToHighlight isEqualToString: VOICEMAILINBOX]) {
                    
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                    
                NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",subMenuArray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                [titleStr appendAttributedString:FromTitle];
                cell.lbl_inboxFilterMenuItem.attributedText = titleStr;
                }

            }
            else if (indexPath.row == 2)
            {
                    cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inboxStatus_feedback"];
                    cell.btn_inboxFilterMenuNext.hidden = true;
                
                if ([_filterStrToHighlight isEqualToString: FEEDBACKINBOX]) {
                    
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                    
                NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",subMenuArray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                [titleStr appendAttributedString:FromTitle];
                cell.lbl_inboxFilterMenuItem.attributedText = titleStr;
                }
                
            }
            else if (indexPath.row == 3)
            {
                    cell.img_inboxFilterMenuIcon.image = [UIImage imageNamed:@"inboxStatus_All"];
                    cell.btn_inboxFilterMenuNext.hidden = true;
                
                if ([_filterStrToHighlight isEqualToString: ALLINBOX]) {
                    
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                    
                NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",subMenuArray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                    
                [titleStr appendAttributedString:FromTitle];
                cell.lbl_inboxFilterMenuItem.attributedText = titleStr;
                }
                
            }
        }
        
        return cell;
    }
    
   
    

    
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isSubMenu) {
        
        if (indexPath.row == 0) {
            
            isTagFilter = false;
            subMenuArray = [NSMutableArray arrayWithObjects:@"Missed Call",@"Voicemail",@"Feedback",@"All",nil];
            _tbl_filter.scrollEnabled = false;

            [self openSubMenu];
            
        }
        else if(indexPath.row == 1)
        {
            if ([[Default valueForKey:kISTaggingInPlan] intValue] == 1) {
               isTagFilter = true;
                
               // subMenuArray = [[NSMutableArray alloc]init];
               // [subMenuArray addObject:@"Clear"];
               // [subMenuArray addObjectsFromArray:[[Default objectForKey:kTagList] valueForKey:@"name"]];
                subMenuArray = [[Default objectForKey:kTagList] valueForKey:@"name"];
               _tbl_filter.scrollEnabled = true;
               [self openSubMenu];
                
               // [self openTagList];
                
            }
            else
            {
                if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
                {
                        [_delegate InboxFilterMenuVCDidDismisWithData:@"NoTagInPlan" filterOption:@"tag"];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                         [self dismissViewControllerAnimated:NO completion:nil];
                    });
                }
            }
            

        }
        else
        {
            NSString *name = mainMenuArray[indexPath.row];
           
            if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
            {
               [_delegate InboxFilterMenuVCDidDismisWithData:name filterOption:@""];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self dismissViewControllerAnimated:NO completion:nil];
            });
        }
    }
    else
    {
        //isSubMenu = false;
        
        if (isTagFilter) {
            
            if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
            {
                NSString *strpass;
//                if (indexPath.row == 0)
//                    strpass = @"Clear";
//                else
                
                strpass = [[[Default objectForKey:kTagList] valueForKey:@"id"] objectAtIndex:indexPath.row];
                
                [_delegate InboxFilterMenuVCDidDismisWithData:strpass filterOption:@"tag"];
            }
            
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self dismissViewControllerAnimated:NO completion:nil];
                });
        }
       else
       {
           NSString *name;
                  if (indexPath.row == 0) {
                      name = MISSEDINBOX;
                      
                      if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
                      {
                              [_delegate InboxFilterMenuVCDidDismisWithData:name filterOption:@"inbox"];
                      }
                  }
                  else if (indexPath.row == 1)
                  {
                      name = VOICEMAILINBOX;
                      
                      if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
                      {
                              [_delegate InboxFilterMenuVCDidDismisWithData:name filterOption:@"inbox"];
                      }
                  }
                  else if (indexPath.row == 2)
                  {
                      if ([[Default valueForKey:kISFeedbackInPlan] intValue] == 1 ) {
                         
                          if ( [[Default valueForKey:kISFeedbackRightsForSub] intValue] == 1) {
                              
                              name = FEEDBACKINBOX;
                              
                              if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
                              {
                                      [_delegate InboxFilterMenuVCDidDismisWithData:name filterOption:@"inbox"];
                              }
                          }
                          else
                          {
                              [_delegate InboxFilterMenuVCDidDismisWithData:@"NoFeedbackRights" filterOption:@"inbox"];
                          }
                            
                      }
                      else
                      {
                          if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
                          {
                                  [_delegate InboxFilterMenuVCDidDismisWithData:@"NoFeedbackInPlan" filterOption:@"inbox"];
                          }
                      }
                      
                  }
           else if (indexPath.row == 3)
           {
               name = ALLINBOX;
               
               if(_delegate && [_delegate respondsToSelector:@selector(InboxFilterMenuVCDidDismisWithData:filterOption:)])
               {
                       [_delegate InboxFilterMenuVCDidDismisWithData:name filterOption:@"inbox"];
               }
           }
                                
                  
                  
                  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       [self dismissViewControllerAnimated:NO completion:nil];
                  });
                 
              }
       }
       
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (isSubMenu && section == 0) {
        if (isTagFilter)
            return @"Tag filter";
        else
            return @"Inbox Filter";
    }
    else
        return @"";
}

#pragma mark - custom methods

- (IBAction)btn_dissmiss_click:(UIButton *)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [_delegate InboxFilterMenuVCDidDismisWithData:@"NoData" filterOption:@""];

}
- (IBAction)inboxNextClick:(UIButton *)sender
{
    [self openSubMenu];
}
-(void)openSubMenu
{
    isSubMenu = true;
    [_tbl_filter reloadData];
    [self tbl_size];
}


-(void)tbl_size
{

//            CGRect frame = self.tbl_filter.frame;
//            frame.size.height = self.tbl_filter.contentSize.height;
//            self.tbl_filter.frame = frame;
    
    dispatch_async(dispatch_get_main_queue(), ^{
           //This code will run in the main thread:
           CGRect frame = self.tbl_filter.frame;
           if (self.tbl_filter.contentSize.height < 350) {
               frame.size.height = self.tbl_filter.contentSize.height;
           }
           else
               frame.size.height = 350;
           //frame.size.height = self.tbl_filter.contentSize.height;
           self.tbl_filter.frame = frame;
       });
     
}
#pragma mark - tag methods


-(void)openTagList
{
    _filterMenuView.hidden = true;
    
    _tagMainView.hidden = false;
    _tagAlphaView.hidden= false;
    
    //searchedTagList = tagList;
    isSubMenu = true;
    [self adjustTableHeight];
    
    [_tagselectionTable reloadData];
}
-(void)closeTagList
{
    if (_tagMainView.hidden == false) {
   
    _tagMainView.hidden = true;
    _tagAlphaView.hidden= true;
    
        [self dismissViewControllerAnimated:NO completion:nil];

 
 /*   if (_selectedTag.count>0) {
    _txtview_seelctedTag.text = [[_selectedTag valueForKey:@"name"] componentsJoinedByString:@", "];
        _txtview_seelctedTag.textColor = [UIColor blackColor];
    }
    else
    {
        _txtview_seelctedTag.text = @"Please Select";
        _txtview_seelctedTag.textColor = [UIColor lightGrayColor];
    }*/
        
    }
}
-(void)adjustTableHeight
{
    if ([subMenuArray count]>5) {
        _tagTableViewHeightConstraint.constant = 43.5 * 5;
    }
    else
    {
        _tagTableViewHeightConstraint.constant = 43.5 * [subMenuArray count];
    }
    _tagViewHeightConstraint.constant = _tagTableViewHeightConstraint.constant + 85;
}
-(IBAction)clearAllClick:(id)sender
{
    [_selectedTag removeAllObjects];
    [_tagselectionTable reloadData];
}
-(IBAction)doneClick:(id)sender
{
    [self closeTagList];
}
-(IBAction)selectAllClick:(id)sender
{
    [_selectedTag removeAllObjects];
    [_selectedTag addObjectsFromArray:subMenuArray];
    [_tagselectionTable reloadData];
}
-(void)tapOnBackground:(UITapGestureRecognizer *)tap
{
    [self closeTagList];
}
@end
