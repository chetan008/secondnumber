//
//  InboxFilterMenuVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol InboxFilterMenuDelegate <NSObject>
@optional
- (void)InboxFilterMenuVCDidDismisWithData:(NSString *)data filterOption:(NSString *)option;
@end

@interface InboxFilterMenuVC : UIViewController

@property (nonatomic, strong)   id<InboxFilterMenuDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbl_filter;
@property (nonatomic, strong) NSString *selected_value;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_constraint;
@property (nonatomic, strong) NSString *filterStrToHighlight;

@property (strong, nonatomic) IBOutlet UIView *tagMainView;
@property (strong, nonatomic) IBOutlet UIView *tagAlphaView;
@property (strong, nonatomic) IBOutlet UIView *filterMenuView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagTableViewHeightConstraint;

@property (strong, nonatomic) IBOutlet UITableView *tagselectionTable;
@property (strong, nonatomic)  NSMutableArray *selectedTag;
@end

NS_ASSUME_NONNULL_END
