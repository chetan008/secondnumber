//
//  FilterVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//


#import "FilterVC.h"
#import "Tblcell.h"
#import "UtilsClass.h"
#import "Constant.h"


@interface FilterVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *filterarray;
    NSInteger indexrow;
    
    NSMutableArray *tagArray;

   
}

@end

@implementation FilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self viewdesign];
}
-(void)viewdesign
{
    indexrow = 0;
    
    //filterarray = [[NSMutableArray alloc]init];
    tagArray = [[NSMutableArray alloc]init];
    _selectedTag = [[NSMutableArray alloc]init];

    if ([_currentView isEqualToString:@"Calllogs"]) {
        
        
       
       
       // working filter of missed and vociemail logs
     /*   filterarray = [NSArray arrayWithObjects:ALLLOGS,MISSEDLOGS,VOICEMAILLOGS,nil];
        if ([_selected_value isEqualToString:ALLLOGS])
        {
            indexrow = 0;
        }
        else if ([_selected_value isEqualToString:MISSEDLOGS])
        {
           indexrow = 1;
        }
        else if ([_selected_value isEqualToString:VOICEMAILLOGS])
        {
            indexrow = 2;
        }*/
        
        //new filter of tags
//        [tagArray addObject:@"Clear"];
        
        
      //  tagArray =[Default objectForKey:kTagList]  ;
        
        //[self openTagList];*/
        
        filterarray = [[Default objectForKey:kTagList] valueForKey:@"name"];
        
     /*   UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]init];
        tapgesture.numberOfTapsRequired = 1;
        [tapgesture addTarget:self action:@selector(tapOnBackground:)];
        
        [self.view addGestureRecognizer:tapgesture];
       */
        
        
    }
    else if ([_currentView isEqualToString:@"Leaderboard"])
    {
        filterarray = [NSMutableArray arrayWithObjects:@"Today",@"Yesterday",@"Last Week",@"Last Month",nil];
        
        if ([_selected_value isEqualToString:@"Today"])
        {
            indexrow = 0;
        }
        else if ([_selected_value isEqualToString:@"Yesterday"])
        {
           indexrow = 1;
        }
        else if ([_selected_value isEqualToString:@"Last Week"])
        {
            indexrow = 2;
        }
        else if ([_selected_value isEqualToString:@"Last Month"])
        {
            indexrow = 3;
        }

    }
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat topPadding = window.safeAreaInsets.top;
            CGFloat bottomPadding = window.safeAreaInsets.bottom;
            
            CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                                    (self.navigationController.navigationBar.frame.size.height ?: 0.0));
            
            //NSLog(@"Trushang bottomPadding : %f", topPadding);
            //NSLog(@"Navframe Height=%f",
    //              topbarHeight);
            if (topPadding < 21.0)
            {
                _top_constraint.constant = 44.0;
            }
            else
            {
                 _top_constraint.constant = 44.0;
            }
            
        }
        else
        {
            _top_constraint.constant = 44.0;
        }
        _tbl_filter.delegate = self;
        _tbl_filter.dataSource = self;
        
       // if ([_currentView isEqualToString:@"Calllogs"]) {
            
            _tbl_filter.scrollEnabled = YES;
            [_tbl_filter reloadData];
        
        [self tbl_size];

        
    
    
    
    
    
    
  /*  }
    else
    {
        _tbl_filter.scrollEnabled = NO;
        [_tbl_filter reloadData];
           
           [self tbl_size];
           
    }*/
   
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(void)tbl_size
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //This code will run in the main thread:
        CGRect frame = self.tbl_filter.frame;
        if (self.tbl_filter.contentSize.height < 350) {
            frame.size.height = self.tbl_filter.contentSize.height;
        }
        else
            frame.size.height = 350;
        //frame.size.height = self.tbl_filter.contentSize.height;
        self.tbl_filter.frame = frame;
    });
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   /* if ([_currentView isEqualToString:@"Calllogs"])
        return tagArray.count;
    else*/
        return filterarray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_currentView isEqualToString:@"Calllogs"]) {
        
       
        //new tag filter
        Tblcell *cell = [_tbl_filter dequeueReusableCellWithIdentifier:@"cell123"];
        
        cell.lbl_filtername.text = filterarray[indexPath.row];
        
        if ([filterarray[indexPath.row] isEqualToString:_tagToHighlight] ) {
            
            NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
            
            NSAttributedString  *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",filterarray[indexPath.row]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
            
                [titleStr appendAttributedString:FromTitle];
            cell.lbl_filtername.attributedText = titleStr;
        }
        
//        else
//        {
            
        //}
        
        cell.img_filter_selection.image = [UIImage imageNamed:@"tagIcon_black"];
        
        return cell;
        
     /*   if (indexPath.row == 0)
            cell.img_filter_selection.image = [UIImage imageNamed:@"Close_details"];
        else
            cell.img_filter_selection.image = [UIImage imageNamed:@"tagIcon_black"];
        

         return cell;*/
        
     /*   Tblcell *cell = [_tagselectionTable dequeueReusableCellWithIdentifier:@"acwTagCell"];
       
       
       cell.lbl_acwTagListTitle.text = [[tagArray valueForKey:@"name"]  objectAtIndex:indexPath.row];
       
       if ([[_selectedTag valueForKey:@"id"] containsObject:[[tagArray valueForKey:@"id"] objectAtIndex:indexPath.row]]) {

           [cell.btn_acwTagCheckmark setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
       }
       else
       {
            [cell.btn_acwTagCheckmark setImage:[UIImage imageNamed:@"blank-check-box"] forState:UIControlStateNormal];
       }
       
       return cell;*/
        
    }
    else
    {
        Tblcell *cell = [_tbl_filter dequeueReusableCellWithIdentifier:@"cell123"];
         cell.lbl_filtername.text = filterarray[indexPath.row];
        
         if (indexrow == indexPath.row)
         {
             cell.img_filter_selection.image = [UIImage imageNamed:@"Radio-on"];
         }
         else
         {
             cell.img_filter_selection.image = [UIImage imageNamed:@"Radio-off"];
         }
         return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_currentView isEqualToString:@"Calllogs"]) {
        
        //working for old filter
       /*  indexrow = indexPath.row;
         [_tbl_filter reloadData];
         
         
         NSString *name = filterarray[indexPath.row];
         
         if(_delegate && [_delegate respondsToSelector:@selector(FilterVCDidDismisWithData:)])
         {
             [_delegate FilterVCDidDismisWithData:name];
         }
         
         
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
              [self dismissViewControllerAnimated:NO completion:nil];
         });
         */
        
        //for new tag filter
        
       // NSString *name = filterarray[indexPath.row];
        
      //  NSString *tagIdStr;
       // if (indexPath.row != 0) {
            
          //   tagIdStr =  [[[Default objectForKey:kTagList]  objectAtIndex:indexPath.row-1] valueForKey:@"id"];
            
          //  _tagToHighlight = [[[Default objectForKey:kTagList]  objectAtIndex:indexPath.row-1] valueForKey:@"name"];
       /* }
        else
        {
            tagIdStr = @"Clear";
        }
        */
        
        NSString *tagIdStr =  [[[Default objectForKey:kTagList]  objectAtIndex:indexPath.row] valueForKey:@"id"];
        
        _tagToHighlight = [[[Default objectForKey:kTagList]  objectAtIndex:indexPath.row] valueForKey:@"name"];
        
                if(_delegate && [_delegate respondsToSelector:@selector(FilterVCDidDismisWithData:)])
                {
                    [_delegate FilterVCDidDismisWithData:tagIdStr];
                }
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self dismissViewControllerAnimated:NO completion:nil];
                });
        
        
        
     // new filter
      /*  if ([[_selectedTag valueForKey:@"id"] containsObject:[[tagArray valueForKey:@"id"] objectAtIndex:indexPath.row]]) {
            
            
            [_selectedTag removeObject:[tagArray objectAtIndex:indexPath.row]];
            
            
        }
        else
        {
            
              [_selectedTag addObject:[tagArray objectAtIndex:indexPath.row]];
        }
        NSLog(@"selected tag :: %@",_selectedTag);
        
        [_tagselectionTable reloadData];*/
    }
    else
    {
        indexrow = indexPath.row;
        [_tbl_filter reloadData];
        
        
        NSString *name = filterarray[indexPath.row];
        
        if(_delegate && [_delegate respondsToSelector:@selector(FilterVCDidDismisWithData:)])
        {
            [_delegate FilterVCDidDismisWithData:name];
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [self dismissViewControllerAnimated:NO completion:nil];
        });
    }
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
   /* if (tableView == _tbl_filter) {
        if ([_currentView isEqualToString:@"Calllogs"]) {
            return @"Tag Filter";
        }
        else
            return @"";
    }
    else
    {
        return @"";
    }*/
    if ([_currentView isEqualToString:@"Calllogs"]) {
        return @"Tag Filter";
    }
    
    else
        return @"";
}
- (IBAction)btn_dissmiss_click:(UIButton *)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [_delegate FilterVCDidDismisWithData:@"NoData"];

}
#pragma mark - tag methods


-(void)openTagList
{
    _filterMenuView.hidden = true;
    
    _tagMainView.hidden = false;
    _tagAlphaView.hidden= false;
    
    //searchedTagList = tagList;
    
    [self adjustTableHeight];
    
    [_tagselectionTable reloadData];
}
-(void)closeTagList
{
    if (_tagMainView.hidden == false) {
   
    _tagMainView.hidden = true;
    _tagAlphaView.hidden= true;
    
    [self dismissViewControllerAnimated:NO completion:nil];

 
 /*   if (_selectedTag.count>0) {
    _txtview_seelctedTag.text = [[_selectedTag valueForKey:@"name"] componentsJoinedByString:@", "];
        _txtview_seelctedTag.textColor = [UIColor blackColor];
    }
    else
    {
        _txtview_seelctedTag.text = @"Please Select";
        _txtview_seelctedTag.textColor = [UIColor lightGrayColor];
    }*/
        
    }
}
-(void)adjustTableHeight
{
    if ([tagArray count]>5) {
        _tagTableViewHeightConstraint.constant = 43.5 * 5;
    }
    else
    {
        _tagTableViewHeightConstraint.constant = 43.5 * [tagArray count];
    }
    _tagViewHeightConstraint.constant = _tagTableViewHeightConstraint.constant + 85;
}
-(IBAction)clearAllClick:(id)sender
{
    [_selectedTag removeAllObjects];
    [_tagselectionTable reloadData];
}
-(IBAction)doneClick:(id)sender
{
    [self closeTagList];
}
-(IBAction)selectAllClick:(id)sender
{
    [_selectedTag removeAllObjects];
    [_selectedTag addObjectsFromArray:tagArray];
    [_tagselectionTable reloadData];
}
-(void)tapOnBackground:(UITapGestureRecognizer *)tap
{
    [self closeTagList];
}
@end
