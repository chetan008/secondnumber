//
//  FilterVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@protocol FilterVCDelegate <NSObject>
@optional
- (void)FilterVCDidDismisWithData:(NSString *)data;
@end


@interface FilterVC : UIViewController
@property (nonatomic, strong) NSString *currentView;
@property (nonatomic, strong)   id<FilterVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbl_filter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top_constraint;
@property (nonatomic, strong) NSString *selected_value;

@property(nonatomic,strong) NSString *tagToHighlight;

@property (strong, nonatomic) IBOutlet UIView *tagMainView;
@property (strong, nonatomic) IBOutlet UIView *tagAlphaView;
@property (strong, nonatomic) IBOutlet UIView *filterMenuView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagTableViewHeightConstraint;

@property (strong, nonatomic) IBOutlet UITableView *tagselectionTable;
@property (strong, nonatomic)  NSMutableArray *selectedTag;

@end

NS_ASSUME_NONNULL_END
