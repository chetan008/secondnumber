//
//  CalllogsVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "CalllogsVC.h"
#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "DialerVC.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "CalllogsDetailsVC.h"
#import "UIImageView+Letters.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "HSUAudioStreamPlayer.h"
#import <CommonCrypto/CommonDigest.h>
#import <AVFoundation/AVFoundation.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "GlobalData.h"
#import "FilterVC.h"
#import "AppDelegate.h"
#import "InboxFilterMenuVC.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define DOC_FILE(s) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:s]


@protocol MyDelegate <NSObject>

-(NSString *) getMessageString;

@end

@interface CalllogsVC ()<UITableViewDelegate,UITableViewDataSource,MyDelegate,FilterVCDelegate,contactUpdateFromLogDelegate,InboxFilterMenuDelegate>{
    WebApiController *obj;
    NSMutableArray *callLogsArr;
    NSMutableArray *dateArr;
    AVPlayer *audioPlayer;
    NSString *audioflag;
    
    NSString *selected_text;
    NSString *selected_url;
    NSString *btn_click;
    bool is_logsTbl_reload;
    NSMutableDictionary *grouped_calllogs;
    NSMutableArray *grouped_calllogs_keys;
    NSMutableArray *calllogs;
    NSString *filter_flag;
    NSString *filter_string;
    UIRefreshControl *refreshControl;
    
    NSString *pullTorefreshFlagForAll;
    NSString *pullTorefreshFlagForMissed;
    NSString *pullTorefreshFlagForVoice;

    NSString *APICallingFlag;
    //BOOL isscrollflagnew;
    NSString *filterUrlCalled;
    
//inbox
    NSString *inboxFilterString;
    NSString *inboxFilterFlag;
    
    NSMutableArray *inboxLogsArray;
    NSMutableArray *inboxlogs;
    bool is_inbox_tbl_reload;

    NSString *inbox_APICallingFlag;
    NSMutableArray *SelectedInboxLogsid;
    NSMutableArray *SelectedLogIndexArr;

    //tag
    NSMutableArray *tagLogsArray;
    NSString *tagFilterString;
    NSString *tagFilterIdStr;
    
    bool is_tag_tbl_reload;
    NSString *tag_APICallingFlag;

    NSMutableDictionary *grouped_calllogsTag;
    NSMutableArray *grouped_calllogs_keys_tag;
    NSMutableArray *tagcalllogs;
    
    NSMutableArray *dateArr_tag;
    NSString *selectedTagIdForFilter;
    
    BOOL updateAfterMarkAsRead;
    
    BOOL gotoDetailFromInbox;
    
    NSString *selInboxTxtTohighlight;
    NSString *selLogsTxtTohighlight;
    
}
@property (nonatomic, strong) HSUAudioStreamPlayer *player;
@end

@implementation CalllogsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_btn_inboxMenu setImage:[UIImage imageNamed:@"rightmenu"]];

    SelectedInboxLogsid = [[NSMutableArray alloc] init];
    SelectedLogIndexArr = [[NSMutableArray alloc] init];

    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    Mixallcontact = [[NSMutableArray alloc] init];
    [self contact_get];
    audioPlayer = [[AVPlayer alloc] init];
    btn_click = @"0";
    filter_flag = @"0";
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    audioflag = @"1";
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;
    
//    refreshControl = [[UIRefreshControl alloc]init];
//    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

//    [self.tbl_logs addSubview:refreshControl];
   
    [[NSNotificationCenter defaultCenter]
        addObserver:self selector:@selector(callDisconnected_loadCallLogs:) name:@"callDisconnected_loadCallLogs" object:nil];
    
    APICallingFlag = @"0";

    inbox_APICallingFlag = @"0";

    
    [_tbl_inbox setAllowsMultipleSelectionDuringEditing:YES];
    _tbl_inbox.allowsMultipleSelection = true;
    _tbl_inbox.tableHeaderView = nil;

    
    if (!tagFilterString) {

         tagFilterString = @"";

     }
      [self loadTagCalllogs];


    if (!inboxFilterString) {

           inboxFilterString = ALLINBOX;

       }

       [self loadInobxLogs];
}



-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath *indexPath = self.tbl_logs.indexPathForSelectedRow;
    if (indexPath) {
        [self.tbl_logs deselectRowAtIndexPath:indexPath animated:animated];
    }
}
-(void)Foreground_Action:(NSNotification *)notification
{
    //NSLog(@"\n \n \n \n Trushang_code : Foreground_Action NEW_SMS_VC:");
    [self contact_get];
    
}
-(void)contact_get
{
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [Default setValue:@"All Calls/Inbox" forKey:SelectedSideMenu];
    [Default setValue:@"menu_log_active" forKey:SelectedSideMenuImage];
    audioflag = @"1";
    
    [Default setValue:@"1" forKey:IS_CallLogDisplayed];
    
    

    
  /*  if (!filter_string) {
        
        filter_string = ALLLOGS;
       
    }
     [self loadCalllogs];
   
    NSLog(@"filter str:: %@",filter_string);
    */
    
    // tagFilterString = @"";
    
    if (gotoDetailFromInbox) {
        
        [self loadInobxLogs];
    }
    
//    if (!tagFilterString) {
//
//        tagFilterString = @"";
//
//    }
//     [self loadTagCalllogs];
//
//
//   if (!inboxFilterString) {
//
//          inboxFilterString = ALLINBOX;
//
//      }
//
//
//
//      [self loadInobxLogs];
//
    
    [UtilsClass button_shadow_boder:_btn_dialpad_click];
    
    [self view_desing];
    self.tbl_logs.delegate = self;
    self.tbl_logs.dataSource = self;
    //[self.tbl_logs reloadData];
    
      
    /*if ([filter_string isEqualToString:ALLLOGS]) {
         [UtilsClass view_navigation_title:self title:@"All Calls"color:UIColor.whiteColor];
    }
    else if([filter_string isEqualToString:MISSEDLOGS])
    {
         [UtilsClass view_navigation_title:self title:@"Missed Calls"color:UIColor.whiteColor];
    }
    else if([filter_string isEqualToString:VOICEMAILLOGS])
    {
        [UtilsClass view_navigation_title:self title:@"Voicemails"color:UIColor.whiteColor];
    }*/
    
    if ([tagFilterString isEqualToString:@""]) {
        [UtilsClass view_navigation_title:self title:@"All Calls/Inbox"color:UIColor.whiteColor];
    }
//    else
//    {
//        [UtilsClass view_navigation_title:self title:tagFilterString color:UIColor.whiteColor];
//    }
    
    
   
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [Default setValue:@"0" forKey:IS_CallLogDisplayed];

}

-(void)view_desing
{
    [self.view_play_recoding setHidden:true];
    _view_main.layer.cornerRadius = 10.0;
    _view_main.clipsToBounds = true;
    _view_boder.layer.borderColor = [UIColor colorWithRed:242.0/255.0 green:122.0/255.0 blue:61.0/255.0 alpha:1.0].CGColor;
    _view_boder.layer.borderWidth = 5.0;
    _view_boder.backgroundColor = [UIColor clearColor];
    _view_boder.clipsToBounds = true;
    _btn_close.layer.cornerRadius = _btn_close.frame.size.height / 2 ;
    _btn_close.layer.borderWidth = 2.0;
    _btn_close.layer.borderColor = [UIColor whiteColor].CGColor;
    _btn_close.clipsToBounds = true;
    
    
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(IBAction)tab_allcalls_click:(id)sender
{
    
   // [self killScroll];
    
    _view_mark.hidden = true;
    [_tbl_inbox setEditing:false animated:true];

    [_btn_allCalls setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:247.0/255.0 blue:232.0/255.0 alpha:1.0]];
    [_btn_allCalls.titleLabel setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
    
    [_btn_inbox setBackgroundColor:[UIColor whiteColor]];
    [_btn_inbox.titleLabel setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightRegular]];
    
    
    _tbl_inbox.hidden = true;
    _tbl_logs.hidden = false;
    
    
        [_btn_inboxMenu setImage:[UIImage imageNamed:@"filter_vc"]];
    
    
    
    

    if (dateArr_tag.count > 0 || [[[CallLogsModel sharedCallLogsData] sectionAllLogs] count] > 0) {
        _lblEmptyConversation.hidden = true;
    }
    else
    {
        _lblEmptyConversation.hidden = false;
    }
    
}
-(IBAction)tab_inbox_click:(id)sender
{
   // [self killScroll];

    [_btn_inbox setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:247.0/255.0 blue:232.0/255.0 alpha:1.0]];
    [_btn_inbox.titleLabel setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightSemibold]];
    
    [_btn_allCalls setBackgroundColor:[UIColor whiteColor]];
    [_btn_allCalls.titleLabel setFont:[UIFont systemFontOfSize:18.0 weight:UIFontWeightRegular]];
    
    
    _tbl_logs.hidden = true;
    _tbl_inbox.hidden = false;
    
    [_btn_inboxMenu setImage:[UIImage imageNamed:@"rightmenu"]];
    
    [self showEmptyInboxLable];
    
    
    
}
#pragma mark - scrollview mwthods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
   // UITableView *parentTableView = (UITableView*)aScrollView.superview;

        
         
    
    if (aScrollView == _tbl_logs) {
        
        float contentYoffset = aScrollView.contentOffset.y;
        float halfpoint = aScrollView.contentSize.height / 2;
        
       if (halfpoint < contentYoffset ) {
           NSLog(@" logs table scroll");
           [self scroll_api_calllogs];
       }
    }
    else
    {
        float contentYoffset = aScrollView.contentOffset.y;
        float halfpoint = aScrollView.contentSize.height / 2;
        if (halfpoint < contentYoffset ) {
            NSLog(@" inbox table scroll");
            [self scroll_api_inboxlogs];
        }
    }
    
}

-(void)scroll_api_calllogs
{
 /*   NSLog(@"is logs table reload.... %d",is_logsTbl_reload);
    if(is_logsTbl_reload == true)
    {
        //NSLog(@"Api call Counter %@",[NSString stringWithFormat:@"%lu",(unsigned long)[callLogsArr count]]);
        is_logsTbl_reload = false;
        
     
        
        if ([filter_string isEqualToString:ALLLOGS]) {
            
            if ([[[CallLogsModel sharedCallLogsData] sectionAllLogs] count]>0) {
                
                NSString *count = [[CallLogsModel sharedCallLogsData] getTotalCount:ALLLOGS];
                [self GetCallLogs:count];
            }
            
        }
        else if ([filter_string isEqualToString:MISSEDLOGS]) {
            
            if ([[[CallLogsModel sharedCallLogsData] sectionMissedLogs] count]>0) {
                
                NSString *count = [[CallLogsModel sharedCallLogsData] getTotalCount:MISSEDLOGS];
               [self GetCallLogs:count];
            }
        }
        else if ([filter_string isEqualToString:VOICEMAILLOGS]) {
            
            if ([[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] count]>0) {
                
                NSString *count = [[CallLogsModel sharedCallLogsData] getTotalCount:VOICEMAILLOGS];
                [self GetCallLogs:count];
            }
        }
    }*/
    
    if(is_tag_tbl_reload == true)
    {
        
        if ([tagFilterString isEqualToString:@""]) {
            
            if ([[[CallLogsModel sharedCallLogsData] sectionAllLogs] count]>0)
            {
                is_tag_tbl_reload = false;
               
                NSString *count = [[CallLogsModel sharedCallLogsData] getTotalCount:ALLLOGS];
                [self GetTagCallLogs:count];
            }
        }
        else
        {
            is_tag_tbl_reload = false;
            [self GetTagCallLogs:[NSString stringWithFormat:@"%lu",(unsigned long)[tagLogsArray count]]];
        }
        
    }
    else
    {
        
    }
    
}

-(void)scroll_api_inboxlogs
{
    NSLog(@"is inbox table reload.... %d",is_inbox_tbl_reload);
    if(is_inbox_tbl_reload == true)
    {
        
        is_inbox_tbl_reload = false;
        
        
        if ([inboxFilterString isEqualToString:ALLINBOX]) {
            

            if ([[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] count]>0) {
                
                NSString *count = [[InboxLogsModel sharedInboxLogsData] getTotalCount:ALLINBOX];
                NSLog(@"count in :: %@",count);

                [self getInboxLogs:count];
            }
            
        }
        else if ([inboxFilterString isEqualToString:MISSEDINBOX]) {
            
            if ([[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] count]>0) {
                
                NSString *count = [[InboxLogsModel sharedInboxLogsData] getTotalCount:MISSEDINBOX];
               [self getInboxLogs:count];
            }
        }
        else if ([inboxFilterString isEqualToString:VOICEMAILINBOX]) {
            
            if ([[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] count]>0) {
                
                NSString *count = [[InboxLogsModel sharedInboxLogsData] getTotalCount:VOICEMAILINBOX];
                [self getInboxLogs:count];
            }
        }
        else if ([inboxFilterString isEqualToString:FEEDBACKINBOX]) {
                   
                   if ([[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] count]>0) {
                       
                       NSString *count = [[InboxLogsModel sharedInboxLogsData] getTotalCount:FEEDBACKINBOX];
                       [self getInboxLogs:count];
                   }
               }
    }
    
}
- (void)killScroll
{
    //kill scroll for logs table
    CGPoint offset = _tbl_logs.contentOffset;
    offset.x -= 1.0;
    offset.y -= 1.0;
    [_tbl_logs setContentOffset:offset animated:NO];
    offset.x += 1.0;
    offset.y += 1.0;
    [_tbl_logs setContentOffset:offset animated:NO];
    
    //kill scroll for inbox table
        CGPoint offset1 = _tbl_inbox.contentOffset;
       offset1.x -= 1.0;
       offset1.y -= 1.0;
       [_tbl_inbox setContentOffset:offset1 animated:NO];
       offset1.x += 1.0;
       offset1.y += 1.0;
       [_tbl_inbox setContentOffset:offset1 animated:NO];
       
}

#pragma mark - tableview methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   if(tableView == _tbl_logs)
   
    //return [[CallLogsModel sharedCallLogsData] numberOfSectionsInArray:filter_string];
       
       if ([tagFilterString isEqualToString:@""]) {
           return [[CallLogsModel sharedCallLogsData] numberOfSectionsInArray:ALLLOGS];
       }
       else
       {
           return dateArr_tag.count;
       }
    
    else
        return [[InboxLogsModel sharedInboxLogsData] numberOfSectionsInArray:inboxFilterString];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tbl_logs)
    {
//        return [[CallLogsModel sharedCallLogsData] numberOfLogsInSection:section inArray:filter_string];
        
         if ([tagFilterString isEqualToString:@""])
             return [[CallLogsModel sharedCallLogsData] numberOfLogsInSection:section inArray:ALLLOGS];
         else{
             if (dateArr_tag.count > 0) {
                 
                 NSString *keyname = dateArr_tag[section];
                 NSMutableArray *arr = grouped_calllogsTag[keyname];
                 
                 return arr.count;
             }else
                return 0;
             
         }
        
    }
        else
        {
           
            return [[InboxLogsModel sharedInboxLogsData] numberOfLogsInSection:section inArray:inboxFilterString];
        }
            
          //  return 0;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tbl_logs) {
        
        Tblcell *cell = [_tbl_logs dequeueReusableCellWithIdentifier:@"Tblcell"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        
        
     /*   NSString *keyname = @"";
        NSMutableArray *arr;
        
        NSLog(@"cell for row filter :: %@",filter_string);
        
        if ([filter_string isEqualToString:ALLLOGS]) {
            
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:indexPath.section];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else if ([filter_string isEqualToString:MISSEDLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:indexPath.section];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
        }
        else if([filter_string isEqualToString:VOICEMAILLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:indexPath.section];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
        }
        
        if (arr.count > 0) {
            
            dict = arr[indexPath.row];
            NSString *transferType = [dict valueForKey:@"transferType"];
            NSString *transferflag = [dict valueForKey:@"warmTransferFlag"];
            */
        
        NSString *keyname = @"";
        NSMutableArray *arr ;
        
        if ([tagFilterString isEqualToString: @""]) {
            
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:indexPath.section];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else
        {
            if (dateArr_tag.count > 0) {
            
                keyname = dateArr_tag[indexPath.section];
                arr = grouped_calllogsTag[keyname];
            }
        }
        
        
        dict = arr[indexPath.row];
        NSString *transferType = [dict valueForKey:@"transferType"];
        NSString *transferflag = [dict valueForKey:@"warmTransferFlag"];
            
            NSString *title = @"";
            cell.img_profile.layer.borderWidth = 0.0;
            //    [cell.btn_next addTarget:self
            //                      action:@selector(btnnextclick:)
            //            forControlEvents:UIControlEventTouchUpInside];
            
            
            //    [cell.btn_PlayTransfer addTarget:self
            //                              action:@selector(play_transfer_recoding:)
            //                    forControlEvents:UIControlEventTouchUpInside];
            
            NSArray *sub = dict[@"transfers"];
            if (sub.count > 0) {
                //NSLog(@"transfers_call : %@",sub);
                NSDictionary *dic = sub[0];
                NSString *callStatus = sub[0][@"callStatus"];
                
                NSString *recordingUrl = [dic valueForKey:@"recordingUrl"];
                //            [cell.btn_transfer_next setHidden:true];
                if([callStatus isEqualToString:@"Missed"])
                {
                    cell.img_transfer_profile.image = [UIImage imageNamed:@"Call_outgoing"];
                }
                else if ([callStatus isEqualToString:@"Rejected"])
                {
                    cell.img_transfer_profile.image = [UIImage imageNamed:@"Call_outgoing"];
                }
                else if ([callStatus isEqualToString:@"Completed"])
                {
                    cell.img_transfer_profile.image = [UIImage imageNamed:@"Call_outgoing"];
                    //  [cell.btn_transfer_next setHidden:false];
                }else if ([callStatus isEqualToString:@"Voicemail"])
                {
                    cell.img_transfer_profile.image = [UIImage imageNamed:@"Call_outgoing"];
                    //  [cell.btn_transfer_next setHidden:false];
                }
                else
                {
                    cell.img_transfer_profile.image = [UIImage imageNamed:@"Call_outgoing"];
                }
                //            if(recordingUrl != nil)
                //            {
                //                if ([recordingUrl isEqualToString:@""])
                //                {
                ////                    [cell.btn_transfer_next setHidden:false];
                //                }
                //                else
                //                {
                //                   if ([sub[0][@"recordingDisplay"] isEqualToString:@"1"]){
                //                        cell.btn_transfer_next.hidden = false;
                //                    }else{
                //                        cell.btn_transfer_next.hidden = true;
                //                    }
                //                }
                //            }
                
                cell.btn_transfer_next.tag = indexPath.row;
                cell.btn_transfer_next.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
                [cell.btn_transfer_next addTarget:self action:@selector(play_transfer_recoding:) forControlEvents:UIControlEventTouchUpInside];
                cell.lbl_TransferDetail.text = [NSString stringWithFormat:@"%@ transferred call to %@",sub[0][@"fromName"],sub[0][@"toName"]];
                //        cell.lblTransferStatus.text = sub[0][@"transferType"];
                if([Default boolForKey:kIsNumberMask] == true)
                {
                if([[sub[0] valueForKey:@"toName"] isEqualToString:[sub[0] valueForKey:@"to"]])
                {
                    NSString *masknum = [UtilsClass get_masked_number:[sub[0] valueForKey:@"toName"]];
                    
                    cell.lbl_TransferDetail.text =[NSString stringWithFormat:@"%@ transferred call to %@",sub[0][@"fromName"],masknum] ;
                }
                
                }
                cell.lblTransferStatus.text = sub[0][@"callType"];//[NSString stringWithFormat:@"%@  %@",sub[0][@"callType"],sub[0][@"callDuration"]];
                cell.lbl_transfer_text.text = sub[0][@"transferType"];
                cell.lblTransferStatus.text = callStatus;
                
                cell.view_transfer_background.layer.borderColor = [UIColor lightGrayColor].CGColor;
                cell.view_transfer_background.layer.cornerRadius = 10.0;
                cell.view_transfer_background.layer.borderWidth = 2.0;
                cell.view_transfer_background.backgroundColor = [UIColor clearColor];
                //        [cell.view_transfer_background layoutIfNeeded];
                
                
                
            }
            
            if ([dict[@"callType"] isEqualToString:@"Incoming"]) {
                NSAttributedString *FromTitle = [[NSAttributedString alloc] init];
                //        NSAttributedString *called = [[NSAttributedString alloc] initWithString:@" called " attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                //        NSAttributedString *ToTitle = [[NSAttributedString alloc] initWithString:@"you" attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                
                
                if ([dict[@"fromName"] isEqualToString:@""]) {
                    
                    NSString *num = dict[@"from"];
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        num = [UtilsClass get_masked_number:dict[@"from"]];
                    }
                    
                    FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",num] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                    [titleStr appendAttributedString:FromTitle];
                    //            [titleStr appendAttributedString:called];
                    //            [titleStr appendAttributedString:ToTitle];
                    cell.lbl_title.attributedText = titleStr;
                    title = [NSString stringWithFormat:@"%@",dict[@"from"]];
                }else{
                    FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"fromName"]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                    [titleStr appendAttributedString:FromTitle];
                    //            [titleStr appendAttributedString:called];
                    //            [titleStr appendAttributedString:ToTitle];
                    cell.lbl_title.attributedText = titleStr;
                    title = [NSString stringWithFormat:@"%@",dict[@"fromName"]];
                }
                
                //        tblcell.lbl_title.attributedText = titleStr;
                
                //
                if ([dict[@"toName"] isEqualToString:@""]) {
                    
                    NSString *to = dict[@"to"];
                    if ([to length] > 6){
                        to=[to substringToIndex:6];
                        to=[NSString stringWithFormat:@"%@...",to];
                    }
                    NSString *extensionCall = dict[@"extensionCall"];
                    int exten = [extensionCall intValue];
                    if(exten == 1 ){
                        cell.lbl_location.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                    }else{
                        cell.lbl_location.text = [NSString stringWithFormat:@"via %@ |%@  %@", to,dict[@"time"],dict[@"callStatus"]];
                    }
                }else {
                    NSString *toName = dict[@"toName"];
                    if ([toName length] > 6){
                        toName=[toName substringToIndex:6];
                        toName=[NSString stringWithFormat:@"%@...",toName];
                    }
                    NSString *extensionCall = dict[@"extensionCall"];
                    int exten = [extensionCall intValue];
                    if(exten == 1 ){
                        cell.lbl_location.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                    }else{
                        cell.lbl_location.text = [NSString stringWithFormat:@"via %@ |%@  %@", toName,dict[@"time"],dict[@"callStatus"]];
                    }
                }
                if([dict[@"callStatus"] isEqualToString:@"Missed"]){
                    cell.img_calltype.image = [UIImage imageNamed:@"InComingMissed"];
                }else{
                    cell.img_calltype.image = [UIImage imageNamed:@"calllog-arrow-incoming"];
                }
                
                
            }else if ([dict[@"callType"] isEqualToString:@"Outgoing"]) {
                
                //        NSAttributedString *FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"to"]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:15.0]}];
                
                //        NSAttributedString *FromTitle = [[NSAttributedString alloc] initWithString:@"You" attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                //
                //
                //        NSAttributedString *called = [[NSAttributedString alloc] initWithString:@" called " attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                
                NSAttributedString *ToTitle = [[NSAttributedString alloc] init];
                
                NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                
                
                
                if ([dict[@"toName"] isEqualToString:@""]) {
                    
                    NSString *num = dict[@"to"];
                    if ([Default boolForKey:kIsNumberMask] == true) {
                        num = [UtilsClass get_masked_number:dict[@"to"]];
                    }
                    
                    ToTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",num] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                    //            [titleStr appendAttributedString:FromTitle];
                    //            [titleStr appendAttributedString:called];
                    [titleStr appendAttributedString:ToTitle];
                    
                    cell.lbl_title.attributedText = titleStr;
                    title = [NSString stringWithFormat:@"%@",dict[@"to"]];
                }else
                {
                    ToTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"toName"]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                    //            [titleStr appendAttributedString:FromTitle];
                    //            [titleStr appendAttributedString:called];
                    [titleStr appendAttributedString:ToTitle];
                    
                    cell.lbl_title.attributedText = titleStr;
                    title = [NSString stringWithFormat:@"%@",dict[@"toName"]];
                }
                
                //        tblcell.lbl_title.attributedText = titleStr;
                if ([dict[@"fromName"] isEqualToString:@""]) {
                    NSString *from = dict[@"from"];
                    if ([from length] > 6){
                        from=[from substringToIndex:6];
                        from=[NSString stringWithFormat:@"%@...",from];
                    }
                    NSString *extensionCall = dict[@"extensionCall"];
                    int exten = [extensionCall intValue];
                    if(exten == 1 ){
                        cell.lbl_location.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                    }else{
                        cell.lbl_location.text = [NSString stringWithFormat:@"via %@ |%@  %@", from,dict[@"time"],dict[@"callStatus"]];
                    }
                    
                }else
                {
                    NSString *fromName = dict[@"fromName"];
                    if ([fromName length] > 6){
                        fromName=[fromName substringToIndex:6];
                        fromName=[NSString stringWithFormat:@"%@...",fromName];
                    }
                    NSString *extensionCall = dict[@"extensionCall"];
                    int exten = [extensionCall intValue];
                    if(exten == 1 ){
                        cell.lbl_location.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                    }else{
                        cell.lbl_location.text = [NSString stringWithFormat:@"via %@ |%@  %@", fromName,dict[@"time"],dict[@"callStatus"]];
                    }
                }
                
                if([dict[@"callStatus"] isEqualToString:@"Missed"]){
                    cell.img_calltype.image = [UIImage imageNamed:@"OutGoingMissed"];
                }else{
                    cell.img_calltype.image = [UIImage imageNamed:@"call-log-arrow-outgoing"];
                }
            }
            NSString *networkDisturbance_flag = dict[@"networkDisturbance"];
            int networkDisturbance_flag_1 = [networkDisturbance_flag intValue];
            //NSLog(@"Trusahng : networkDisturbance : %d ",networkDisturbance_flag_1);
            //NSLog(@"Trusahng : networkDisturbance dic : %@",dict);
            if (networkDisturbance_flag_1 == 0 )
            {
                
                cell.img_networkDisturnance.hidden = true;
            }
            else
            {
                cell.img_networkDisturnance.hidden = false;
            }
            NSString *name = title;
            //NSLog(@"Title Of logs : %@",name);
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            
            if ([dict[@"contactType"] isEqualToString:@"mobile"])
            {
                [cell.img_profile setImageWithString:title color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
                
                cell.img_profile.layer.cornerRadius = cell.img_profile.frame.size.height / 2;
                cell.img_profile.layer.cornerRadius=cell.img_profile.frame.size.height / 2;
                cell.img_profile.layer.borderWidth=1.0;
                cell.img_profile.layer.masksToBounds = YES;
                cell.img_profile.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                cell.img_profile.clipsToBounds = YES;
            }//"contactType":"mobile"
            else if ([dict[@"contactType"] isEqualToString:@"callhippo"])
            {
                [cell.img_profile setImage:[UIImage imageNamed:Cell_callhippo_image]];
            }else{
                [cell.img_profile setImage:[UIImage imageNamed:@"logo_drawer_user_v2"]];
            }
            
            
            NSMutableArray *leftUtilityButtons = [[NSMutableArray alloc] init];
            NSMutableArray *rightUtilityButtons = [[NSMutableArray alloc] init];
            
            
            [rightUtilityButtons sw_addUtilityButtonWithColor:
             [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                        title:@"Details"];
            
            int num = [transferflag intValue];
            if(num == 1)
            {
                NSString *calltype = [dict valueForKey:@"callType"];
                NSString *to = [dict valueForKey:@"to"];
                NSString *from = [dict valueForKey:@"from"];
                if ([calltype  isEqual: @"Incoming"])
                {
                    if ([UtilsClass isValidNumber:from])
                    {
                        [leftUtilityButtons sw_addUtilityButtonWithColor:
                         [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                                   title:@"Call"];
                        cell.leftUtilityButtons = leftUtilityButtons;
                    }
                }
                else
                {
                    if ([UtilsClass isValidNumber:to])
                    {
                        [leftUtilityButtons sw_addUtilityButtonWithColor:
                         [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                                   title:@"Call"];
                        cell.leftUtilityButtons = leftUtilityButtons;
                    }
                }
                
                
            }
            else
            {
                [leftUtilityButtons sw_addUtilityButtonWithColor:
                 [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                           title:@"Call"];
                cell.leftUtilityButtons = leftUtilityButtons;
            }
            
            
            
            
            cell.rightUtilityButtons = rightUtilityButtons;
            cell.delegate = self;
            
            cell.tag = indexPath.row;
            cell.contentView.tag = indexPath.section;
            //    [cell layoutIfNeeded];
            //    [tableView layoutIfNeeded];
            
            cell.btn_next.tag = indexPath.row;
            cell.btn_next.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
            [cell.btn_next addTarget:self action:@selector(btnnextclick:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_didslect.tag = indexPath.row;
            cell.btn_didslect.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
            [cell.btn_didslect addTarget:self action:@selector(btndidselect:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            cell.btn_PlayTransfer.tag = indexPath.row;
            cell.btn_PlayTransfer.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
            [cell.btn_PlayTransfer addTarget:self
                                      action:@selector(play_transfer_recoding:)
                            forControlEvents:UIControlEventTouchUpInside];
            
            
           
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
      //  }
            
            
        //}
        
        
        return cell;
    }
    else
    {
        Tblcell *cell = [_tbl_inbox dequeueReusableCellWithIdentifier:@"Tblcell"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgColorView];
        
        
            NSString *keyname = @"";
            NSMutableArray *arr;
            
            NSLog(@"cell for row filter :: %@",inboxFilterString);
            
            if ([inboxFilterString isEqualToString:ALLINBOX]) {
                
                keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:indexPath.section];
                arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
            }
            else if ([inboxFilterString isEqualToString:MISSEDINBOX])
            {
                keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:indexPath.section];
                arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
            }
            else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
            {
                keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:indexPath.section];
                arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
            }
            else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
            {
                keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:indexPath.section];
                arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
            }
        else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:indexPath.section];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
        }
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

            
            if (arr.count > 0) {
            
                dict = arr[indexPath.row];
                
                //set image profile
                    cell.img_inbox_callStatus.layer.cornerRadius = cell.img_inbox_callStatus.frame.size.height / 2;
                
                NSString *title = @"";
                cell.img_inbox_callStatus.layer.borderWidth = 0.0;
                cell.img_inbox_callStatus.layer.cornerRadius=cell.img_inbox_callStatus.frame.size.height / 2;
                    cell.img_inbox_callStatus.layer.borderWidth=1.0;
                    cell.img_inbox_callStatus.layer.masksToBounds = YES;
                    cell.img_inbox_callStatus.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    cell.img_inbox_callStatus.clipsToBounds = YES;
                
                if ([[dict objectForKey:@"feedbacks"] count]>0) {
                    cell.img_inbox_callStatus.image = [UIImage imageNamed:@"inboxStatus_feedback_grey"];
                    
                }
                else if ([[dict objectForKey:@"callTags"] count]>0) {
                    cell.img_inbox_callStatus.image = [UIImage imageNamed:@"tagIcon_grey"];
                    
                }
                else if([[dict valueForKey:@"callStatus"] isEqualToString:@"Missed"])
                {
                    cell.img_inbox_callStatus.image = [UIImage imageNamed:@"inbox_statusMissed"];
                }
                else if([[dict valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"])
                {
                    cell.img_inbox_callStatus.image = [UIImage imageNamed:@"voiceMail"];
                }
                

                
                if ([dict[@"callType"] isEqualToString:@"Incoming"]) {
                    
                    NSAttributedString *FromTitle = [[NSAttributedString alloc] init];
                               
                               
                    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                    
                               
                    if ([dict[@"fromName"] isEqualToString:@""]) {
                        
                        
                                 NSString *num = dict[@"from"];
                                 if ([Default boolForKey:kIsNumberMask] == true) {
                                     num = [UtilsClass get_masked_number:dict[@"from"]];
                                 }
                        //set title & description lable
                        if ([[dict valueForKey:@"read"] intValue] == 0) {
                            
                           FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",num] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                            
                        }
                        else
                        {
                            FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",num] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                        }
                        
                        [titleStr appendAttributedString:FromTitle];
                                  
                        cell.lbl_inbox_callTitle.attributedText = titleStr;
                        title = [NSString stringWithFormat:@"%@",dict[@"from"]];
                    }else{
                        
                        if ([[dict valueForKey:@"read"] intValue] == 0) {
                            
                        FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"fromName"]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                        }
                        else
                        {
                             FromTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"fromName"]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                        }
                        [titleStr appendAttributedString:FromTitle];
                                   
                        cell.lbl_inbox_callTitle.attributedText = titleStr;
                        title = [NSString stringWithFormat:@"%@",dict[@"fromName"]];
                    }
                             
                    if ([dict[@"toName"] isEqualToString:@""]) {
                                   
                        NSString *to = dict[@"to"];
                        if ([to length] > 6){
                                to=[to substringToIndex:6];
                                to=[NSString stringWithFormat:@"%@...",to];
                        }
                        NSString *extensionCall = dict[@"extensionCall"];
                        int exten = [extensionCall intValue];
                        if(exten == 1 ){
                                cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                        }else{
                                cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"via %@ |%@  %@", to,dict[@"time"],dict[@"callStatus"]];
                        }
                    }else {
                                NSString *toName = dict[@"toName"];
                                if ([toName length] > 6){
                                       toName=[toName substringToIndex:6];
                                       toName=[NSString stringWithFormat:@"%@...",toName];
                                   }
                            NSString *extensionCall = dict[@"extensionCall"];
                            int exten = [extensionCall intValue];
                        if(exten == 1 ){
                                cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                        }else{
                                cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"via %@ |%@  %@", toName,dict[@"time"],dict[@"callStatus"]];
                                   }
                               }
                               
                               
                               
                    }
                else if ([dict[@"callType"] isEqualToString:@"Outgoing"]) {
                               
                               
                               
                               NSAttributedString *ToTitle = [[NSAttributedString alloc] init];
                               
                               NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] init];
                               
                               
                               
                               if ([dict[@"toName"] isEqualToString:@""]) {
                                   
                                   NSString *num = dict[@"to"];
                                   if ([Default boolForKey:kIsNumberMask] == true) {
                                       num = [UtilsClass get_masked_number:dict[@"to"]];
                                   }
                                   
                                  if ([[dict valueForKey:@"read"] intValue] == 0) {
                                      
                                      ToTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",num] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                                  }
                                   else
                                   {
                                       ToTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",num] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                                   }
                                   
                                   //            [titleStr appendAttributedString:FromTitle];
                                   //            [titleStr appendAttributedString:called];
                                   [titleStr appendAttributedString:ToTitle];
                                   
                                   cell.lbl_inbox_callTitle.attributedText = titleStr;
                                   title = [NSString stringWithFormat:@"%@",dict[@"to"]];
                               }else
                               {
                                   
                                   if ([[dict valueForKey:@"read"] intValue] == 0) {
                                      ToTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"toName"]] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor blackColor]}];
                                   }
                                   else
                                   {
                                       ToTitle = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",dict[@"toName"]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
                                   }
                                   
                                   //            [titleStr appendAttributedString:FromTitle];
                                   //            [titleStr appendAttributedString:called];
                                   [titleStr appendAttributedString:ToTitle];
                                   
                                   cell.lbl_inbox_callTitle.attributedText = titleStr;
                                   title = [NSString stringWithFormat:@"%@",dict[@"toName"]];
                               }
                               
                               //        tblcell.lbl_title.attributedText = titleStr;
                               if ([dict[@"fromName"] isEqualToString:@""]) {
                                   NSString *from = dict[@"from"];
                                   if ([from length] > 6){
                                       from=[from substringToIndex:6];
                                       from=[NSString stringWithFormat:@"%@...",from];
                                   }
                                   NSString *extensionCall = dict[@"extensionCall"];
                                   int exten = [extensionCall intValue];
                                   if(exten == 1 ){
                                       cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                                   }else{
                                       cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"via %@ |%@  %@", from,dict[@"time"],dict[@"callStatus"]];
                                   }
                                   
                               }else
                               {
                                   NSString *fromName = dict[@"fromName"];
                                   if ([fromName length] > 6){
                                       fromName=[fromName substringToIndex:6];
                                       fromName=[NSString stringWithFormat:@"%@...",fromName];
                                   }
                                   NSString *extensionCall = dict[@"extensionCall"];
                                   int exten = [extensionCall intValue];
                                   if(exten == 1 ){
                                       cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"at %@  %@",dict[@"time"],dict[@"callStatus"]];
                                   }else{
                                       cell.lbl_inbox_callTime.text = [NSString stringWithFormat:@"via %@ |%@  %@", fromName,dict[@"time"],dict[@"callStatus"]];
                                   }
                               }
                               
                               
                           }
                
                // display network strength
                NSString *networkDisturbance_flag = dict[@"networkDisturbance"];
                int networkDisturbance_flag_1 = [networkDisturbance_flag intValue];
                //NSLog(@"Trusahng : networkDisturbance : %d ",networkDisturbance_flag_1);
                //NSLog(@"Trusahng : networkDisturbance dic : %@",dict);
                if (networkDisturbance_flag_1 == 0 )
                {
                    
                    cell.img_inbox_networkStrength.hidden = true;
                }
                else
                {
                    cell.img_inbox_networkStrength.hidden = false;
                }
                
                cell.delegate = self;
                
                cell.tag = indexPath.row;
                cell.contentView.tag = indexPath.section;
                //    [cell layoutIfNeeded];
                //    [tableView layoutIfNeeded];
                
                
                
                if ([_tbl_inbox isEditing]) {
                    
                    cell.btn_inbox_rowSelect.hidden = true;
                    cell.btn_inbox_next.hidden = true;
                    
                    [_tbl_inbox setAllowsMultipleSelectionDuringEditing:YES];
                    
                        //cell.stagecell = kCellStateCenter;
                       // _stagevc = kCellStateCenter;
                    
                    if ([SelectedInboxLogsid containsObject:[dict valueForKey:@"_id"]]) {
                        [cell setSelected:true];
                    }
                    else
                    {
                        [cell setSelected:false];

                    }
                    
                }
                else
                {
                    cell.btn_inbox_rowSelect.hidden = false;
                    cell.btn_inbox_next.hidden = false;
                    

                    cell.btn_inbox_next.tag = indexPath.row;
                    cell.btn_inbox_next.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
                    [cell.btn_inbox_next addTarget:self action:@selector(inboxNextClick:) forControlEvents:UIControlEventTouchUpInside];
                    
                    cell.btn_inbox_rowSelect.tag = indexPath.row;
                    cell.btn_inbox_rowSelect.titleLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
                    [cell.btn_inbox_rowSelect addTarget:self action:@selector(inboxRowSelect:) forControlEvents:UIControlEventTouchUpInside];
                    
                //left right buttons
                NSMutableArray *leftUtilityButtons = [[NSMutableArray alloc] init];
                NSMutableArray *rightUtilityButtons = [[NSMutableArray alloc] init];
                
                
                [rightUtilityButtons sw_addUtilityButtonWithColor:
                 [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                            title:@"Details"];
                
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:
                [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                               title:@"Call"];
                cell.leftUtilityButtons = leftUtilityButtons;
                
                cell.rightUtilityButtons = rightUtilityButtons;
                
                
            }
                
                
        }
        
        
        return cell;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (tableView == _tbl_logs) {
        
      /*  NSString *keyname =@"";
        NSMutableArray *arr ;
        
        if ([filter_string isEqualToString:ALLLOGS]) {
            
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:indexPath.section];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else if ([filter_string isEqualToString:MISSEDLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:indexPath.section];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
        }
        else if([filter_string isEqualToString:VOICEMAILLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:indexPath.section];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
        }
        
            if (arr.count > 0) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict = arr[indexPath.row];
            NSArray *sub = dict[@"transfers"];
            if (sub.count > 0) {
                return 125;
            } else {
                return 65;
            }
        }else {
            return 65;
        }*/
        
        NSString *keyname = @"";
        NSMutableArray *arr;
        
        if ([tagFilterString isEqualToString:@""]) {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:indexPath.section];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else
        {
            if (dateArr_tag.count > 0) {
               
                keyname = dateArr_tag[indexPath.section];
                arr = grouped_calllogsTag[keyname];
            }
        }
        
        
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict = arr[indexPath.row];
            NSArray *sub = dict[@"transfers"];
            if (sub.count > 0) {
                return 125;
            } else {
                return 65;
            }
//        }else {
//            return 65;
//        }
    }
    else
    {
        return 65;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==_tbl_inbox) {
        
        if ([tableView isEditing])
        {
            [_tbl_inbox setAllowsMultipleSelectionDuringEditing:YES];

            NSString *keyname = @"";
            NSMutableArray *arr;

            
           if ([inboxFilterString isEqualToString:ALLINBOX]) {
               
               NSLog(@"sections :%@",[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs]);

               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:indexPath.section];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
           }
           else if ([inboxFilterString isEqualToString:MISSEDINBOX])
           {
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:indexPath.section];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
           }
           else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
           {
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:indexPath.section];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
           }
           else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
           {
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:indexPath.section];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
           }
            else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
            {
                keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:indexPath.section];
                arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
            }
           
           NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

           
           if (arr.count > 0) {
           
               dict = arr[indexPath.row];
           
                       
               Tblcell *cell = [_tbl_inbox dequeueReusableCellWithIdentifier:@"Tblcell"];
               
               [cell setSelected:YES];
               
            [SelectedLogIndexArr addObject:[NSString stringWithFormat:@"%@",indexPath]];
            [SelectedInboxLogsid addObject:[arr[indexPath.row] valueForKey:@"_id"]];
               
               NSLog(@"selected index array : %@",SelectedLogIndexArr);
               NSLog(@"selected index id : %@",SelectedInboxLogsid);

                _view_mark.hidden = false;
                
            
           
           }
            else
                       {
                           _view_mark.hidden = true;
                       }
        }
       
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tbl_inbox) {
        
        
            if ([_tbl_inbox isEditing])
            {
                
                NSString *keyname = @"";
                 NSMutableArray *arr;

                 
                if ([inboxFilterString isEqualToString:ALLINBOX]) {
                    
                    NSLog(@"sections :%@",[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs]);

                    keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:indexPath.section];
                    arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
                }
                else if ([inboxFilterString isEqualToString:MISSEDINBOX])
                {
                    keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:indexPath.section];
                    arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
                }
                else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
                {
                    keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:indexPath.section];
                    arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
                }
                else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
                {
                    keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:indexPath.section];
                    arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
                }
                else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
                {
                    keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:indexPath.section];
                    arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
                }
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

                
        if (arr.count > 0) {
                
            dict = arr[indexPath.row];
            
            Tblcell *cell = [_tbl_inbox dequeueReusableCellWithIdentifier:@"Tblcell"];

            [cell setSelected:false];
                    
        if([SelectedInboxLogsid containsObject:[dict valueForKey:@"_id"]])
        {
            [SelectedInboxLogsid removeObject:[dict valueForKey:@"_id"]];
        }
            if ([SelectedLogIndexArr containsObject:[NSString stringWithFormat:@"%@",indexPath]])
            {
                [SelectedLogIndexArr removeObject:[NSString stringWithFormat:@"%@",indexPath]];
            }
            
           
               NSLog(@"selected index array : %@",SelectedLogIndexArr);
               NSLog(@"selected index id : %@",SelectedInboxLogsid);
            
            
                }
            }
    }
    

}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *Headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    Headerview.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:247.0/255.0 blue:232.0/255.0 alpha:1.0];
    UILabel *lbl_header = [[UILabel alloc] initWithFrame:Headerview.bounds];
    
    NSString *keyname =@"";
    NSMutableArray *arr ;
    
    if (tableView == _tbl_logs) {
        
        if ([tagFilterString isEqualToString:@""]) {
            
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:section];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else
        {
            if(dateArr_tag.count != 0)
            {
                    if(dateArr_tag[section] != [NSNull null] && dateArr_tag[section] != nil )
                    {
                        
                        keyname = dateArr_tag[section];
                        arr = grouped_calllogsTag[keyname];
                    }
            }
        }
       
                
               
                if(![keyname isEqualToString:@""])
                {
                        
                       // NSString *datetoshow = dateArr[section];
                        NSString *datetoshow = keyname;
                
                        lbl_header.text = datetoshow;
                        
                        lbl_header.textAlignment = NSTextAlignmentCenter;
                        lbl_header.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
                        [Headerview addSubview:lbl_header];
                        
                        NSInteger borderThickness = 1;
                        UIView *topBorder = [UIView new];
                        topBorder.backgroundColor = [UIColor lightGrayColor];
                        topBorder.frame = CGRectMake(0, 0, Headerview.frame.size.width, borderThickness);
                        [Headerview addSubview:topBorder];
                        
                        
                        UIView *bottomBorder = [UIView new];
                        bottomBorder.backgroundColor = [UIColor lightGrayColor];
                        bottomBorder.frame = CGRectMake(0, Headerview.frame.size.height - borderThickness, Headerview.frame.size.width, borderThickness);
                        [Headerview addSubview:bottomBorder];
                        
                        
                    }
//            }
//        }
       
      /*     if ([filter_string isEqualToString:ALLLOGS]) {
               
               keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:section];
               arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
           }
           else if ([filter_string isEqualToString:MISSEDLOGS])
           {
               keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:section];
                      arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
           }
           else if([filter_string isEqualToString:VOICEMAILLOGS])
           {
               keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:section];
                      arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
           }
        */
        
    }
    else
    {
        if ([inboxFilterString isEqualToString:ALLINBOX]) {
                      
                keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:section];
                arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
                  }
                  else if ([inboxFilterString isEqualToString:MISSEDINBOX])
                  {
                      keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:section];
                             arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
                  }
                  else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
                  {
                      keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:section];
                             arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
                  }
               else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
               {
                   keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:section];
                          arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
               }
               else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
               {
                   keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:section];
                   arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
               }
        
        if(![keyname isEqualToString:@""])
        {
                
               // NSString *datetoshow = dateArr[section];
                NSString *datetoshow = keyname;
        
                lbl_header.text = datetoshow;
                
                lbl_header.textAlignment = NSTextAlignmentCenter;
                lbl_header.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
                [Headerview addSubview:lbl_header];
                
                NSInteger borderThickness = 1;
                UIView *topBorder = [UIView new];
                topBorder.backgroundColor = [UIColor lightGrayColor];
                topBorder.frame = CGRectMake(0, 0, Headerview.frame.size.width, borderThickness);
                [Headerview addSubview:topBorder];
                
                
                UIView *bottomBorder = [UIView new];
                bottomBorder.backgroundColor = [UIColor lightGrayColor];
                bottomBorder.frame = CGRectMake(0, Headerview.frame.size.height - borderThickness, Headerview.frame.size.width, borderThickness);
                [Headerview addSubview:bottomBorder];
                
                
            }
    }
    
    
    return Headerview;

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44.0;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    id view = [cell superview];

    while (view && [view isKindOfClass:[UITableView class]] == NO) {
        view = [view superview];
    }

    UITableView *tableView = (UITableView *)view;
    
    NSString *keyname = @"";
    NSMutableArray *arr;
    NSString *fromInbox;
    
    if (tableView == _tbl_logs) {
        
        fromInbox = @"0";
        
       /* if ([filter_string isEqualToString:ALLLOGS]) {
            
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else if ([filter_string isEqualToString:MISSEDLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
        }
        else if([filter_string isEqualToString:VOICEMAILLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
        }*/
        
        if([tagFilterString isEqualToString:@""])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else{
            if(dateArr_tag.count != 0)
            {
                keyname = dateArr_tag[cell.contentView.tag];
               arr = grouped_calllogsTag[keyname];
            }
        }
        
        
    }
    else
    {
        fromInbox = @"1";

        if ([inboxFilterString isEqualToString:ALLINBOX]) {
            
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
        }
        else if ([inboxFilterString isEqualToString:MISSEDINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
        }
        else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
        }
        else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
        }
        else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
        }
    }
    
   
    
    
    
    if (arr.count > 0) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//        dic = arr[cell.tag];
        dic = [[NSMutableDictionary alloc]initWithDictionary:arr[cell.tag]];
        
        if ([fromInbox isEqualToString:@"1"]) {
            //mark as complete
            if ([[dic valueForKey:@"read"] intValue] == 0) {
                [SelectedInboxLogsid addObject:[dic valueForKey:@"_id"]];
                [self markAsCompleteByAPICAll:NO];
            }
           gotoDetailFromInbox = true;

        }
        
        //NSLog(@"filterarray : dict : -->  %@ ",dic);
        CalllogsDetailsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CalllogsDetailsVC"];
        vc.userData = dic;
        vc.displayUrl = @"1";
        vc.contactDelegate = self;
        vc.fromInbox = fromInbox;
        [[self navigationController] pushViewController:vc animated:YES];
    }
}
//swipeableTableViewCellShouldHideUtilityButtonsOnSwipe
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    //    [_tbl_logs reloadData];
     //   [cell hideUtilityButtonsAnimated:YES];
    return YES;
}
//canSwipeToState
- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    return YES;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    
    id view = [cell superview];

       while (view && [view isKindOfClass:[UITableView class]] == NO) {
           view = [view superview];
       }

       UITableView *tableView = (UITableView *)view;
       
      
    NSString *keyname = @"";
    NSMutableArray *arr;
    
    bool markInbox;
    
    if (tableView == _tbl_logs) {
        
        markInbox = false;

        
       /* if ([filter_string isEqualToString:ALLLOGS]) {
               
               keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:cell.contentView.tag];
               arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
           }
           else if ([filter_string isEqualToString:MISSEDLOGS])
           {
               keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:cell.contentView.tag];
               arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
           }
           else if([filter_string isEqualToString:VOICEMAILLOGS])
           {
               keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:cell.contentView.tag];
               arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
           }*/
        
        if ([tagFilterString isEqualToString:@""]) {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else
        {
            if(dateArr_tag.count != 0)
            {
                keyname = dateArr_tag[cell.contentView.tag];
                arr = grouped_calllogsTag[keyname];
            }
        }
        
    }
    else
    {
        markInbox = true;
        if ([inboxFilterString isEqualToString:ALLINBOX]) {
               
            
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:cell.contentView.tag];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
           }
           else if ([inboxFilterString isEqualToString:MISSEDINBOX])
           {
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:cell.contentView.tag];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
           }
           else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
           {
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:cell.contentView.tag];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
           }
        else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
        }
        else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:cell.contentView.tag];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
        }
    }
    
   
    
    if (arr.count > 0) {
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//        dic = arr[cell.tag];
        dic = [[NSMutableDictionary alloc]initWithDictionary:arr[cell.tag]];
        
        if (markInbox) {
            //mark as complete
            if ([[dic valueForKey:@"read"] intValue] == 0) {
            [SelectedInboxLogsid addObject:[dic valueForKey:@"_id"]];
            [self markAsCompleteByAPICAll:NO];
            }
        }
        
        NSString *callfromnumber = @"";//= [Default valueForKey:SELECTEDNO];
        NSString *callToName= @"";
        NSString *callTonumber = @"";
        NSString *callDepartment = @"";
        NSString *contactType = [dic valueForKey:@"contactType"];
        NSString *warmTransferFrom = [dic valueForKey:@"warmTransferFrom"];
        NSString *warmTransferTo = [dic valueForKey:@"warmTransferTo"];
        if ([[dic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
            if ([[dic valueForKey:@"fromName"]  isEqual: @""])
            {
                callToName = [dic valueForKey:@"from"];// "\(userData?.from! ?? "")";
            }
            else
            {
                callToName = [dic valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
                
            }
            
            callfromnumber = [dic valueForKey:@"to"];
            
            [Default setValue:callfromnumber forKey:SELECTEDNO];
            //                [Default setValue:callDepartment forKey:Selected_Department];
            callTonumber = [dic valueForKey:@"from"];//"\(userData?.from! ?? "")";
            callDepartment = [NSString stringWithFormat:@"%@ ",[dic valueForKey:@"toName"]];
            [Default setValue:callDepartment forKey:Selected_Department];
            
        }
        else
        {
            
            if ([[dic valueForKey:@"toName"]  isEqual: @""])
            {
                callToName =  [dic valueForKey:@"to"];//"\(userData?.to! ?? "")"
            }
            else
            {
                callToName = [dic valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
            }
            callfromnumber = [dic valueForKey:@"from"];
            
            [Default setValue:callfromnumber forKey:SELECTEDNO];
            //                [Default setValue:callDepartment forKey:Selected_Department];
            callTonumber = [dic valueForKey:@"to"];// "\(userData?.to! ?? "")"
            callDepartment = [NSString stringWithFormat:@"%@ ",[dic valueForKey:@"fromName"]];
            [Default setValue:callDepartment forKey:Selected_Department];
        }
        
        NSString *transferflag = [dic valueForKey:@"warmTransferFlag"];
        int num = [transferflag intValue];
        if(num != 1)
        {
        if([UtilsClass isNetworkAvailable])
        {
           //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
            
            NSData *data = [Default valueForKey:PURCHASE_NUMBER];
            NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if(purchase_number.count > 0 || callTonumber.length == 4 || callTonumber.length == 5)
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                
                NSLog(@"results results : %lu",(unsigned long)results.count);
                NSString *numbVer = @"";
                if (results.count == 0 ){
                    numbVer = @"1";
                }else{
                    NSDictionary *dic1 = [results objectAtIndex:0];
                    numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                }
                int num = [numbVer intValue];
                if (num == 1  || callTonumber.length == 4 || callTonumber.length == 5) {
                    NSString *credit = [Default valueForKey:CREDIT];
                    float cre = [credit floatValue];
                    if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                        switch ([[AVAudioSession sharedInstance] recordPermission]) {
                            case AVAudioSessionRecordPermissionGranted:
                            {
                                if ([UtilsClass isValidNumber:callTonumber])
                                {
                                    [Default setValue:callfromnumber forKey:SELECTEDNO];
                                   
                                    
                                    NSString *imagename = [[dic  objectForKey:@"shortName"] lowercaseString];
                                    [Default setValue:imagename forKey:Selected_Department_Flag];
                                    
                                   
                                    
                                    [UtilsClass make_outgoing_call_validate1:self callfromnumber:callfromnumber ToName:callToName ToNumber:callTonumber calltype:callDepartment Mixallcontact:Mixallcontact];
                                }
                                else
                                {
                                    [UtilsClass makeToast:@"Number you have dialed invalid."];
                                }
                                break;
                            }
                            case AVAudioSessionRecordPermissionDenied:
                            {
                                //                [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
                                [self micPermiiton];
                                break;
                            }
                            case AVAudioSessionRecordPermissionUndetermined:
                                // This is the initial state before a user has made any choice
                                // You can use this spot to request permission here if you want
                                break;
                            default:
                                break;
                                
                        }}else {
                            [UtilsClass showAlert:kCREDITLAW contro:self];
                        }}else{
                            [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                        }}else{
                            [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                        }
        }
        else
        {
            [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
        }
        }else{
            
            NSData *data = [Default valueForKey:PURCHASE_NUMBER];
            NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if(purchase_number.count > 0 || [contactType isEqualToString:@"SubUser"])
            {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                
                NSLog(@"results results : %lu",(unsigned long)results.count);
                NSString *numbVer = @"";
                if (results.count == 0){
                    numbVer = @"1";
                }else{
                    NSDictionary *dic1 = [results objectAtIndex:0];
                    numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                }
                int num = [numbVer intValue];
                if (num == 1) {
                    NSString *credit = [Default valueForKey:CREDIT];
                    float cre = [credit floatValue];
                    if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                        
                        if ([contactType isEqualToString:@"SubUser"]) {
                            [Default setValue:@"true" forKey:ExtentionCall];
                            [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                            [Default setValue:@"+1" forKey:klastDialCountryCode];
                            [Default setValue:@"United States" forKey:klastDialCountryName];
                            [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:contactType ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                        }else{
                            [Default setValue:@"false" forKey:ExtentionCall];
                            [Default setValue:@"" forKey:ExtentionCallProvider];
                            [Default setValue:warmTransferFrom forKey:SELECTEDNO];
                            [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:warmTransferFrom ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                        }}else {
                            [UtilsClass showAlert:kCREDITLAW contro:self];
                        }}else{
                            [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                        }}else{
                [UtilsClass showAlert:kNUMBERASSIGN contro:self];
            }
        }
    }
    
    
    //            break;
    //        }
    //
    //        default:
    //            break;
    //    }
}


#pragma mark -  call logs custom methods



-(void)showEmptyCallLogLable
{
    if ([filter_string isEqualToString:ALLLOGS]) {
        if([[CallLogsModel sharedCallLogsData] sectionAllLogs].count==0)
        {
            _lblEmptyConversation.hidden = false;
            _lblEmptyConversation.text = @"You have no activity";
            [refreshControl removeFromSuperview];
            refreshControl = nil;
            
        }
        else
        {
            _lblEmptyConversation.hidden = true;

        }
    }
    else if ([filter_string isEqualToString:MISSEDLOGS])
    {
        if([[CallLogsModel sharedCallLogsData] sectionMissedLogs].count==0)
        {
            _lblEmptyConversation.hidden = false;
            _lblEmptyConversation.text = @"You have no missed calls in logs";
            [refreshControl removeFromSuperview];
            refreshControl = nil;
            
        }
        else
        {
            _lblEmptyConversation.hidden = true;

        }
    }
    else if ([filter_string isEqualToString:VOICEMAILLOGS])
    {
        if([[CallLogsModel sharedCallLogsData] sectionVoicemailLogs].count==0)
        {
                _lblEmptyConversation.hidden = false;
                _lblEmptyConversation.text = @"You have no voicemails in logs";
                [refreshControl removeFromSuperview];
                refreshControl = nil;
                   
        }
        else
        {
                _lblEmptyConversation.hidden = true;
        }
    }
    
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    //    callLogsArr = [[NSMutableArray alloc] init];
    //    dateArr = [[NSMutableArray alloc] init];
    
    NSLog(@"CALLLOGS : STATUSCODE **************  : %@",response1);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        

        NSMutableArray *allData = [[NSMutableArray alloc]init];
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            NSMutableArray *calllogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
            //NSLog(@"Calllogs :%@",calllogs);
            if ([calllogs count] > 0)
            {
                for (int i = 0; i<calllogs.count; i++) {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:calllogs[i]];
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";
                    [calllogs replaceObjectAtIndex:i withObject:dict];
                }
                
                for (int i = 0; i<calllogs.count; i++) {
                    [self->callLogsArr addObject:calllogs[i]];
                }
                for (NSMutableDictionary *item in calllogs) {
                    
                    NSString *dateToshow = item[@"dateToShow"];
                    if ([self->dateArr containsObject:dateToshow]) {
                        
                    }else
                    {
                        [self->dateArr addObject:dateToshow];
                    }
                }
               
                is_logsTbl_reload = true;
                
                
            }
            
            
        }else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
}

- (IBAction)btn_menu_click:(UIBarButtonItem *)sender
{
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}

-(void)btnnextclick:(UIButton *)sender {
    
    // NSMutableDictionary *dict = [self getSectionwithsender:sender];
    
    int sec = [NSString stringWithFormat:@"%@", sender.titleLabel.text].intValue;
    int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
    

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
   
    NSString *keyname = @"";
    NSMutableArray *arr;
    
   /* if ([filter_string isEqualToString:ALLLOGS]) {
        
        keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:sec];
        arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
    }
    else if ([filter_string isEqualToString:MISSEDLOGS])
    {
        keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:sec];
        arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
    }
    else if([filter_string isEqualToString:VOICEMAILLOGS])
    {
        keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:sec];
        arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
    }*/
    
    if ([tagFilterString isEqualToString:@""]) {
        keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:sec];
        arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
    }
    else
    {
        if(dateArr_tag.count != 0)
        {
            keyname = dateArr_tag[sec];
           arr = grouped_calllogsTag[keyname];
        }
    }
    
    
    
    
    if (arr.count > 0) {
        
//        dict = arr[row];
        dict = [[NSMutableDictionary alloc]initWithDictionary:arr[row]];
        
   
        CalllogsDetailsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CalllogsDetailsVC"];
        vc.userData = dict;
        vc.displayUrl = @"1";
        vc.contactDelegate = self;
        [[self navigationController] pushViewController:vc animated:YES];
    }
    
}

-(void)btndidselect:(UIButton *)sender {
    
    
        
        int sec = [NSString stringWithFormat:@"%@", sender.titleLabel.text].intValue;
        int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        
        NSString *keyname = @"";
               NSMutableArray *arr;
               
          /*     if ([filter_string isEqualToString:ALLLOGS]) {
                   
                   keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:sec];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
               }
               else if ([filter_string isEqualToString:MISSEDLOGS])
               {
                   keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:sec];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
               }
               else if([filter_string isEqualToString:VOICEMAILLOGS])
               {
                   keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:sec];
                   arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
               }
               */
    
    if ([tagFilterString isEqualToString:@""]) {
        keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:sec];
        arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
    }
    else
    {
        if(dateArr_tag.count != 0)
        {
            keyname = dateArr_tag[sec];
           arr = grouped_calllogsTag[keyname];
        }
    }
    
    
    
    
               if (arr.count > 0) {
        
//        dic = arr[row];
        dic = [[NSMutableDictionary alloc]initWithDictionary:arr[row]];
        //NSLog(@"callDic : %@",dic);
        
        
        NSString *callfromnumber = @""; // = [Default valueForKey:SELECTEDNO];
        NSString *callToName = @"";
        NSString *callTonumber = @"";
        NSString *callDepartment = @"";
        NSString *contactType = [dic valueForKey:@"contactType"];
        NSString *warmTransferFrom = [dic valueForKey:@"warmTransferFrom"];
        NSString *warmTransferTo = [dic valueForKey:@"warmTransferTo"];
        
                   
                   
        if ([[dic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
            if ([[dic valueForKey:@"fromName"]  isEqual: @""])
            {
                callToName = [dic valueForKey:@"from"];// "\(userData?.from! ?? "")";
            }
            else
            {
                callToName = [dic valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
                
            }
            callfromnumber = [dic valueForKey:@"to"];
            
            [Default setValue:callfromnumber forKey:SELECTEDNO];
            //        [Default setValue:callDepartment forKey:Selected_Department];
            callTonumber = [dic valueForKey:@"from"];//"\(userData?.from! ?? "")";
            callDepartment = [NSString stringWithFormat:@"%@ ",[dic valueForKey:@"toName"]];
            [Default setValue:callDepartment forKey:Selected_Department];
            
        }
        else
        {
            
            if ([[dic valueForKey:@"toName"]  isEqual: @""])
            {
                callToName =  [dic valueForKey:@"to"];//"\(userData?.to! ?? "")"
            }
            else
            {
                callToName = [dic valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
            }
            
            callfromnumber = [dic valueForKey:@"from"];
            
            [Default setValue:callfromnumber forKey:SELECTEDNO];
            //        [Default setValue:callDepartment forKey:Selected_Department];
            callTonumber = [dic valueForKey:@"to"];// "\(userData?.to! ?? "")"
            callDepartment = [NSString stringWithFormat:@"%@ ",[dic valueForKey:@"fromName"]];
            [Default setValue:callDepartment forKey:Selected_Department];
        }
        
         NSString *transferflag = [dic valueForKey:@"warmTransferFlag"];
               int num = [transferflag intValue];
               if(num != 1)
               {
                   if([UtilsClass isNetworkAvailable])
                   {
                       
                      //pri NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
                       NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                       NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                       
                       if(purchase_number.count > 0 || callTonumber.length == 4 || callTonumber.length == 5)
                       {
                           
                           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                           NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                           
                           NSLog(@"results results : %lu",(unsigned long)results.count);
                           NSString *numbVer = @"";
                           if (results.count == 0){
                               numbVer = @"1";
                           }else{
                               NSDictionary *dic1 = [results objectAtIndex:0];
                               numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                           }
                           int num = [numbVer intValue];
                           if (num == 1) {
                               NSString *credit = [Default valueForKey:CREDIT];
                               float cre = [credit floatValue];
                               if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                                   switch ([[AVAudioSession sharedInstance] recordPermission]) {
                                       case AVAudioSessionRecordPermissionGranted:
                                       {
                                           
                                           if ([UtilsClass isValidNumber:callTonumber])
                                           {
                                               //NSLog(@"Outgoing call from number :%@",callfromnumber);
                                               [Default setValue:callfromnumber forKey:SELECTEDNO];
                                               [Default setValue:callDepartment forKey:Selected_Department];
                                                if (results.count>0) {
                                                    NSString *imagename = [[[[results objectAtIndex:0] objectForKey:@"number"]  objectForKey:@"shortName"] lowercaseString];
                                                    [Default setValue:imagename forKey:Selected_Department_Flag];
                                                }
                                               
                                               
                                               [UtilsClass make_outgoing_call_validate1:self callfromnumber:callfromnumber ToName:callToName ToNumber:callTonumber calltype:callDepartment Mixallcontact:Mixallcontact];
                                           }
                                           else
                                           {
                                               [UtilsClass makeToast:@"Number you have dialed invalid."];
                                           }
                                           
                                           
                                           
                                           break;
                                       }
                                       case AVAudioSessionRecordPermissionDenied:
                                       {
                                           //                [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
                                           [self micPermiiton];
                                           break;
                                       }
                                       case AVAudioSessionRecordPermissionUndetermined:
                                           // This is the initial state before a user has made any choice
                                           // You can use this spot to request permission here if you want
                                           break;
                                       default:
                                           break;
                                           
                                   }}else {
                                       [UtilsClass showAlert:kCREDITLAW contro:self];
                                   }}else{
                                       [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                                   }}else{
                                       [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                                   }
                   }
                   else
                   {
                       [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
                   }
               }else{
                   
                   NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                   NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                   
                   if(purchase_number.count > 0 || [contactType isEqualToString:@"SubUser"])
                   {
                       
                       NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                       NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                       
                       NSLog(@"results results : %lu",(unsigned long)results.count);
                       NSString *numbVer = @"";
                       if (results.count == 0){
                           numbVer = @"1";
                       }else{
                           NSDictionary *dic1 = [results objectAtIndex:0];
                           numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                       }
                       int num = [numbVer intValue];
                       if (num == 1) {
                           NSString *credit = [Default valueForKey:CREDIT];
                           float cre = [credit floatValue];
                           if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                               
                               if ([contactType isEqualToString:@"SubUser"]) {
                                   [Default setValue:@"true" forKey:ExtentionCall];
                                   [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                                   [Default setValue:@"+1" forKey:klastDialCountryCode];
                                   [Default setValue:@"United States" forKey:klastDialCountryName];
                                   [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:contactType ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                               }else{
                                   [Default setValue:@"false" forKey:ExtentionCall];
                                   [Default setValue:@"" forKey:ExtentionCallProvider];
                                   [Default setValue:warmTransferFrom forKey:SELECTEDNO];
                                   [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:warmTransferFrom ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                               }}else {
                                   [UtilsClass showAlert:kCREDITLAW contro:self];
                               }}else{
                                   [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                               }}else{
                       [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                   }
               }
    }
}

-(NSMutableDictionary *)getSectionwithsender:(id)sender
{
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:_tbl_logs]; // maintable --> replace your tableview name
    NSIndexPath *clickedButtonIndexPath = [_tbl_logs indexPathForRowAtPoint:touchPoint];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.dateToShow == %@", dateArr[clickedButtonIndexPath.section]];
    NSMutableArray *filterarray = [[NSMutableArray alloc] initWithArray:[callLogsArr filteredArrayUsingPredicate:predicate]];
    return filterarray[clickedButtonIndexPath.row];
}

- (IBAction)btn_dial_pad_click:(UIButton *)sender
{
    DialerVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"DialerVC"];
    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
    [[self navigationController] pushViewController:vc animated:false];
}
-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                //NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)dismissFromCallLogDetail:(BOOL)isContactUpdated
{

   // NSLog(@"datadara :%@",data);

        if (isContactUpdated) {
            
          //  [self GetCallLogs:0];
//           if (!tagFilterString) {
//
//                 tagFilterString = @"";
//
//             }
              [self loadTagCalllogs];
//
//
//            if (!inboxFilterString) {
//
//                   inboxFilterString = ALLINBOX;
//
//               }
//
//
//
              [self loadInobxLogs];
            
            }
}

- (IBAction)btn_filter_click:(UIBarButtonItem *)sender
{
//    if (isTblScrolling == false) {
    
//    if([APICallingFlag isEqualToString:@"0"])
//    {
        
    [self callLogFilterClickAction];
  
  //  }
    
//}
    
    
    
    
   
    
}
-(void)callLogFilterClickAction
{
   /* [self killScroll];
      
      if ([[CallLogsModel sharedCallLogsData] sectionAllLogs].count > 0){
          
          if ([filter_flag isEqualToString:@"0"])
          {*/
   // if (dateArr_tag.count > 0){
    if ([filter_flag isEqualToString:@"0"])
    {
              filter_flag = @"1";
              
              if (@available(iOS 13, *))
              {
                  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                  UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                  alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
                  //      alertWindow.rootViewController = [[UIViewController alloc] init];
                  alertWindow.windowLevel = UIWindowLevelAlert + 1;
                  [alertWindow makeKeyAndVisible];
                  
                  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  FilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
                  //vc.selected_value = filter_string;
                  vc.currentView = @"Calllogs";
                  
                  vc.tagToHighlight = selLogsTxtTohighlight;
                  vc.delegate = self;
                  if (IS_IPAD) {
                      
                      vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                      //[self presentViewController:vc animated:YES completion:nil];
                  } else {
                      [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                      vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                      //[self presentViewController:vc animated:YES completion:nil];
                  }
                  [alertWindow.rootViewController presentViewController:vc animated:NO completion:nil];
                  
              }
              else
              {
                  UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                  alertWindow.rootViewController = [[UIViewController alloc] init];
                  alertWindow.windowLevel = UIWindowLevelAlert + 1;
                  [alertWindow makeKeyAndVisible];
                  
                  
                  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  FilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
                  //vc.selected_value = filter_string;
                   vc.currentView = @"Calllogs";
                  vc.tagToHighlight = selLogsTxtTohighlight;
                  

                  vc.delegate = self;
                  if (IS_IPAD) {
                      vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                      vc.modalPresentationCapturesStatusBarAppearance = YES;
                  } else {
                      [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                      vc.modalPresentationStyle = UIModalPresentationFullScreen;
                      vc.modalPresentationCapturesStatusBarAppearance = YES;
                  }
                  
                  [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
              }
              
              
              
              
          }
          else
          {
              filter_flag = @"0";
          }
//      }else{
//
//      }
}
- (void)FilterVCDidDismisWithData:(NSString *)data
{
    //new tag filter
    

    /*
    working for old filter
    NSLog(@"api flag in filter:: %@",APICallingFlag);
    
    
    NSLog(@"Trushang : Selected_Filter  %@",data);
    filter_flag = @"0";
    is_logsTbl_reload=true;
    _tbl_logs.scrollEnabled = false;
    APICallingFlag = @"0";
    

    
    
    if ([data isEqualToString:ALLLOGS])
    {
        
         [UtilsClass view_navigation_title:self title:@"All Calls"color:UIColor.whiteColor];
        filter_string = ALLLOGS;
     

        if ([[[CallLogsModel sharedCallLogsData] sectionAllLogs] count]>0) {
            
            
         
            pullTorefreshFlagForAll=@"1";
            [self GetCallLogs:@"1"];
        }
        else
        {
           // pullTorefreshFlag=@"0";
             [self GetCallLogs:@"0"];
        }
        
       
    }
    else if ([data isEqualToString:MISSEDLOGS])
    {
        filter_string = MISSEDLOGS;
         [UtilsClass view_navigation_title:self title:@"Missed Calls"color:UIColor.whiteColor];
        
        if ([[[CallLogsModel sharedCallLogsData] sectionMissedLogs] count]>0) {
            
          
            pullTorefreshFlagForMissed=@"1";
            [self GetCallLogs:@"1"];
        }
        else
        {
            //pullTorefreshFlag=@"0";
             [self GetCallLogs:@"0"];
        }
      
    }
    else if ([data isEqualToString:VOICEMAILLOGS])
    {
        filter_string = VOICEMAILLOGS;
          [UtilsClass view_navigation_title:self title:@"Voicemails"color:UIColor.whiteColor];
        
       if ([[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] count]>0) {
           
          
            pullTorefreshFlagForVoice=@"1";
            [self GetCallLogs:@"1"];
        }
        else
        {
            //pullTorefreshFlag=@"0";
             [self GetCallLogs:@"0"];
        }
      
    }
    else if ([data isEqualToString:@"NoData"])
    {
         _tbl_logs.scrollEnabled = true;
    }
    */
    
    NSLog(@"Trushang : Selected_Filter  %@",data);
       filter_flag = @"0";
       if ([data isEqualToString:@"NoData"] )
       {
//           tagFilterString = @"";
//           [self GetTagCallLogs:@"0"];
       }
    else
    {
        if([data isEqualToString:@"Clear"])
        {
            tagFilterString = @"";
            tagFilterIdStr = @"";
            
            selLogsTxtTohighlight = @"Clear";
            
            [self.tbl_logs reloadData];

        }
        else
        {
            NSUInteger i = [[[Default objectForKey:kTagList] valueForKey:@"id"] indexOfObjectIdenticalTo:data];
             
             tagFilterIdStr = data;
             tagFilterString = [[[Default objectForKey:kTagList] objectAtIndex:i] valueForKey:@"name"];
            
            selLogsTxtTohighlight = tagFilterString;
            [self GetTagCallLogs:@"0"];
        }
      
        
        NSLog(@">>>>>>>>> id :: %@",data);
        NSLog(@">>>>>>>>> tag :: %@",tagFilterString);

        
    }
       
    
}

-(void)callDisconnected_loadCallLogs:(NSNotification*)notification
{
    /* if ([[notification name] isEqualToString:@"callDisconnected_loadCallLogs"]) {
         
        NSLog(@"call disconnected go to call logs");
          
         if ([filter_string isEqualToString:ALLLOGS]) {
             pullTorefreshFlagForAll = @"1";
         }
         else if ([filter_string isEqualToString:MISSEDLOGS])
         {
             pullTorefreshFlagForMissed = @"1";

         }
         else if ([filter_string isEqualToString:VOICEMAILLOGS])
         {
             pullTorefreshFlagForVoice = @"1";

         }
                
        // pass 1 because is pass 0 then array cleared in get calllogs
             
         [self GetCallLogs:@"1"];
         
     }*/
    
    if ([[notification name] isEqualToString:@"callDisconnected_loadCallLogs"]) {
        
        pullTorefreshFlagForAll = @"1";
        [self GetTagCallLogs:@"1"];

    }
    
    
 }
#pragma mark - inbox logs custom methods
-(void)loadInobxLogs
{

        inboxlogs = [[NSMutableArray alloc] init];

        if([UtilsClass isNetworkAvailable])
        {

          //  if ([inboxFilterString isEqualToString:ALLINBOX]) {
                
                
               /* if ([[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] count] > 0) {
                    
    
                    // to refresh logs on incoming call and go to detail page and call
                    
                    //pullTorefreshFlagForAll = @"1";
                            
                    // pass 1 because is pass 0 then array cleared in get calllogs
                    is_inbox_tbl_reload = false;
                     [self getInboxLogs:@"1"];
                    
                }
                else
                {*/
               // is_inbox_tbl_reload = false;

                  //  [self getInboxLogs:@"0"];
               // }
          //  }
         [self getInboxLogs:@"0"];

        }else {
            [_tbl_inbox reloadData];
            [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
        }
}
-(void)getInboxLogs:(NSString*)skip
{
    if ([skip intValue] == 0)
    {
        [[InboxLogsModel sharedInboxLogsData] removeAllObjectsFromArray:inboxFilterString];
        [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];

    }
    
    if ([inbox_APICallingFlag isEqualToString:@"0"] ) {

    inbox_APICallingFlag = @"1";


    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"";
    obj = [[WebApiController alloc] init];
    
        
    if ([inboxFilterString isEqualToString:ALLINBOX]) {
        url = [NSString stringWithFormat:@"%@/inbox?skip=%@&limit=20",userId,skip];
        
         [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(allInboxResponse:response:) andDelegate:self];
    }
    else if ([inboxFilterString isEqualToString:MISSEDINBOX])
    {
        url = [NSString stringWithFormat:@"%@/inbox?skip=%@&limit=20&type=Missed",userId,skip];
         [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(missedInboxResponse:response:) andDelegate:self];
    }
    else if ([inboxFilterString isEqualToString:VOICEMAILINBOX])
    {
        url = [NSString stringWithFormat:@"%@/inbox?skip=%@&limit=20&type=Voicemail",userId,skip];
         [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(voicemailInboxResponse:response:) andDelegate:self];
    }
    else if ([inboxFilterString isEqualToString:FEEDBACKINBOX])
    {
        url = [NSString stringWithFormat:@"%@/inbox?skip=%@&limit=20&type=Feedback",userId,skip];
         [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(feedbackInboxResponse:response:) andDelegate:self];
    }
    else if ([inboxFilterString isEqualToString:TAGFILTERINBOX])
    {
        url = [NSString stringWithFormat:@"%@/inbox?skip=%@&limit=20&type=Tag&tag=%@",userId,skip,selectedTagIdForFilter];
        [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(tagLogsInboxResponse:response:) andDelegate:self];
    }
        
        NSLog(@"tag inbox filter URL  : %@",url);
}
           
           
}
- (void)allInboxResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    [Processcall hideLoadingWithView];
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        NSLog(@"Encrypted Response : all inbox : %@",response1);

        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            inboxlogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"inbox"]];
            //NSLog(@"inbox logs List :%@",inboxlogs);
            
                if (inboxlogs.count > 0) {
                    
//                    if ([pullTorefreshFlagForAll isEqualToString:@"1"])
//                    {
//                        pullTorefreshFlagForAll = @"0";
//                        [[InboxLogsModel sharedInboxLogsData] addObjectsAtTop:inboxlogs toArray:ALLINBOX];
//
//                    }
//                    else
//                    {
                        [[InboxLogsModel sharedInboxLogsData] addObjectsAtBottom:inboxlogs toArray:ALLINBOX];
                   // }
                    
        
                }
                else
                {
                  //  pullTorefreshFlagForAll = @"0";
                
                    [self showEmptyInboxLable];
                }

                self.tbl_inbox.delegate = self;
                self.tbl_inbox.dataSource = self;
                            
                            
                if(is_inbox_tbl_reload == true)
                {
                          
                    self.tbl_inbox.contentOffset = CGPointZero;
            
                }
                is_inbox_tbl_reload = true;
                            
                _tbl_inbox.scrollEnabled = true;
             
            inbox_APICallingFlag = @"0";

            [_tbl_inbox reloadData];
                   
            
            
        }else {
            
             inbox_APICallingFlag = @"0";
            
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }

}

- (void)missedInboxResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    [Processcall hideLoadingWithView];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : missed inbox : %@",response1);

        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            inboxlogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"inbox"]];
           // NSLog(@"inbox missed List :%@",inboxlogs);
       
                if (inboxlogs.count > 0) {
                    
//                    if ([pullTorefreshFlagForMissed isEqualToString:@"1"])
//                    {
//                        pullTorefreshFlagForMissed = @"0";
//                        [[InboxLogsModel sharedInboxLogsData] addObjectsAtTop:inboxlogs toArray:MISSEDLOGS];
//
//                    }
//                    else
//                    {

                        [[InboxLogsModel sharedInboxLogsData] addObjectsAtBottom:inboxlogs toArray:MISSEDINBOX];
                    
                    NSLog(@"filter string:%@",inboxFilterString);
                   // }
                    
                   
                }
                else
                {
                   // pullTorefreshFlagForMissed = @"0";
                
                    [self showEmptyInboxLable];
                }

//                self.tbl_inbox.delegate = self;
//                self.tbl_inbox.dataSource = self;
                            
                            
                if(is_inbox_tbl_reload == true)
                {
                      
                    self.tbl_inbox.contentOffset = CGPointZero;
                            
                }
                is_inbox_tbl_reload = true;
                            
                _tbl_inbox.scrollEnabled = true;
            
            inbox_APICallingFlag = @"0";

            NSLog(@"filter string:%@",inboxFilterString);

            [self.tbl_inbox reloadData];
                   
            NSLog(@"filter string:%@",inboxFilterString);

            
            
        }else {
            
             inbox_APICallingFlag = @"0";
            
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
}

- (void)voicemailInboxResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
       
       [Processcall hideLoadingWithView];
       
       //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
       if([apiAlias isEqualToString:Status_Code])
       {
          // [UtilsClass logoutUser:self];
            [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
       }
       else
       {
           NSLog(@"Encrypted Response : voicemail inbox : %@",response1);

           if ([[response1 valueForKey:@"success"] integerValue] == 1) {
               
               inboxlogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"inbox"]];
              // NSLog(@"inbox voicemail List :%@",inboxlogs);
        
                   if (inboxlogs.count > 0) {
                       
//                       if ([pullTorefreshFlagForVoice isEqualToString:@"1"])
//                       {
//                           pullTorefreshFlagForVoice = @"0";
//                           [[InboxLogsModel sharedInboxLogsData] addObjectsAtTop:inboxlogs toArray:VOICEMAILLOGS];
//
//                       }
//                       else
//                       {
                           [[InboxLogsModel sharedInboxLogsData] addObjectsAtBottom:inboxlogs toArray:VOICEMAILINBOX];
                    //   }
                       
                      
                   }
                   else
                   {
                    //   pullTorefreshFlagForVoice = @"0";
                   
                       [self showEmptyInboxLable];
                   }

               
              

//                   self.tbl_inbox.delegate = self;
//                   self.tbl_inbox.dataSource = self;
                               
                               
                   if(is_inbox_tbl_reload == true)
                   {
                                  
                        self.tbl_inbox.contentOffset = CGPointZero;
                  
                }
                   is_inbox_tbl_reload = true;
                               
                   _tbl_inbox.scrollEnabled = true;
                
                inbox_APICallingFlag = @"0";

               [_tbl_inbox reloadData];
                      
               
               
           }else {
               
                inbox_APICallingFlag = @"0";
               
               @try {
                   if (response1 != (id)[NSNull null] && response1 != nil ){
                       [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                   }
               }
               @catch (NSException *exception) {
               }
           }
           
       }
}
- (void)feedbackInboxResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
       
       [Processcall hideLoadingWithView];
       
       //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
       if([apiAlias isEqualToString:Status_Code])
       {
          // [UtilsClass logoutUser:self];
            [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
       }
       else
       {
           NSLog(@"Encrypted Response : feedback inbox : %@",response1);

           if ([[response1 valueForKey:@"success"] integerValue] == 1) {
               
               inboxlogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"inbox"]];
              // NSLog(@"inbox voicemail List :%@",inboxlogs);
        
                   if (inboxlogs.count > 0) {
                       
//                       if ([pullTorefreshFlagForVoice isEqualToString:@"1"])
//                       {
//                           pullTorefreshFlagForVoice = @"0";
//                           [[InboxLogsModel sharedInboxLogsData] addObjectsAtTop:inboxlogs toArray:FEEDBACKINBOX];
//
//                       }
//                       else
//                       {
                           [[InboxLogsModel sharedInboxLogsData] addObjectsAtBottom:inboxlogs toArray:FEEDBACKINBOX];
                   //    }
                       
                      
                   }
                   else
                   {
                   //    pullTorefreshFlagForVoice = @"0";
                   
                       [self showEmptyInboxLable];
                   }

               
              

//                   self.tbl_inbox.delegate = self;
//                   self.tbl_inbox.dataSource = self;
                               
                               
                   if(is_inbox_tbl_reload == true)
                   {
                                  
                        self.tbl_inbox.contentOffset = CGPointZero;
                   
                               }
                   is_inbox_tbl_reload = true;
                               
                   _tbl_inbox.scrollEnabled = true;
                
                inbox_APICallingFlag = @"0";

               [self.tbl_inbox reloadData];
                      
               
               
           }else {
               
                inbox_APICallingFlag = @"0";
               
               @try {
                   if (response1 != (id)[NSNull null] && response1 != nil ){
                       [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                   }
               }
               @catch (NSException *exception) {
               }
           }
           
       }
}

- (void)tagLogsInboxResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
       
       [Processcall hideLoadingWithView];
       
       //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    
   // NSLog(@"inbox tag List response:%@",response1);
    NSLog(@"Encrypted Response : tag log inbox : %@",response1);

    
       if([apiAlias isEqualToString:Status_Code])
       {
          // [UtilsClass logoutUser:self];
            [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
       }
       else
       {
           
           if ([[response1 valueForKey:@"success"] integerValue] == 1) {
               
               inboxlogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"inbox"]];
               NSLog(@"inbox tag List :%@",inboxlogs);
        
                   if (inboxlogs.count > 0) {
                       
//                       if ([pullTorefreshFlagForVoice isEqualToString:@"1"])
//                       {
//                           pullTorefreshFlagForVoice = @"0";
//                           [[InboxLogsModel sharedInboxLogsData] addObjectsAtTop:inboxlogs toArray:FEEDBACKINBOX];
//
//                       }
//                       else
//                       {
                           [[InboxLogsModel sharedInboxLogsData] addObjectsAtBottom:inboxlogs toArray:TAGFILTERINBOX];
                   //    }
                       
                      
                   }
                   else
                   {
                   //    pullTorefreshFlagForVoice = @"0";
                   
                       [self showEmptyInboxLable];
                   }

               
              

//                   self.tbl_inbox.delegate = self;
//                   self.tbl_inbox.dataSource = self;
                               
                               
                   if(is_inbox_tbl_reload == true)
                   {
                                  
                        self.tbl_inbox.contentOffset = CGPointZero;
                   
                               }
                   is_inbox_tbl_reload = true;
                               
                   _tbl_inbox.scrollEnabled = true;
                
                inbox_APICallingFlag = @"0";

               [self.tbl_inbox reloadData];
                      
               
               
           }else {
               
                inbox_APICallingFlag = @"0";
               
               @try {
                   if (response1 != (id)[NSNull null] && response1 != nil ){
                       [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                   }
               }
               @catch (NSException *exception) {
               }
           }
           
       }
}
- (IBAction)btn_inboxMenu_click:(UIBarButtonItem *)sender
{
    if ([[_btn_inboxMenu image] isEqual:[UIImage imageNamed:@"filter_vc"]]) {
        
        if ([[Default valueForKey:kISTaggingInPlan] intValue] == 1) {
            
             [self callLogFilterClickAction];
        }
        else
        {
            [UtilsClass makeToast:@"Call Tagging feature is not available in your plan"];
        }
       
    }
    else if([[_btn_inboxMenu image] isEqual:[UIImage imageNamed:@"rightmenu"]])
    {
    if (@available(iOS 13, *))
       {
           AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
           UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
           alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
           //      alertWindow.rootViewController = [[UIViewController alloc] init];
           alertWindow.windowLevel = UIWindowLevelAlert + 1;
           [alertWindow makeKeyAndVisible];
           
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
           InboxFilterMenuVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"InboxFilterMenuVC"];
           //vc.selected_value = filter_string;
           //vc.currentView = @"Calllogs";
           vc.filterStrToHighlight = selInboxTxtTohighlight;
           vc.delegate = self;
           if (IS_IPAD) {
               
               vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
               //[self presentViewController:vc animated:YES completion:nil];
           } else {
               [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
               vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
               //[self presentViewController:vc animated:YES completion:nil];
           }
           [alertWindow.rootViewController presentViewController:vc animated:NO completion:nil];
           
       }
       else
       {
           UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
           alertWindow.rootViewController = [[UIViewController alloc] init];
           alertWindow.windowLevel = UIWindowLevelAlert + 1;
           [alertWindow makeKeyAndVisible];
           
           
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
           InboxFilterMenuVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"InboxFilterMenuVC"];
           //vc.selected_value = filter_string;
           //vc.currentView = @"Calllogs";
           vc.filterStrToHighlight = selInboxTxtTohighlight;

           vc.delegate = self;
           if (IS_IPAD) {
               vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
               vc.modalPresentationCapturesStatusBarAppearance = YES;
           } else {
               [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
               vc.modalPresentationStyle = UIModalPresentationFullScreen;
               vc.modalPresentationCapturesStatusBarAppearance = YES;
           }
           
           [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
       }
    }
}
-(void)InboxFilterMenuVCDidDismisWithData:(NSString *)data filterOption:(NSString *)option
{
    if([data isEqualToString:@"Mark as Complete"])
    {
        
//        _stagevc = kCellStateCenter;

        [_tbl_inbox setEditing:true animated:YES];


    }
    else
    {

        _lblEmptyConversation.hidden = true;

        if (![data isEqualToString:@"NoData"]) {
            
            if ([option isEqualToString:@"inbox"]) {
                
                if ([data isEqualToString:@"NoFeedbackInPlan"]) {
                   
                    
                    [UtilsClass makeToast:@"Feedback feature is not available in your plan."];
                }
                
                else if([data isEqualToString:@"NoFeedbackRights"])
                {
                    [UtilsClass makeToast:@"You have no access rights for this module."];
                }
                else
                {
                    is_inbox_tbl_reload=true;
                    _tbl_inbox.scrollEnabled = false;
                    inbox_APICallingFlag = @"0";
                           
                    inboxFilterString = data;
                    selInboxTxtTohighlight = data;
                    [self getInboxLogs:@"0"];
                }
            }
            else if([option isEqualToString:@"tag"])
            {
               if ([data isEqualToString:@"NoTagInPlan"]) {
                   
                   
                   [UtilsClass makeToast:@"Call Tagging feature is not available in your plan"];
                }
                else
                {
                    is_inbox_tbl_reload=true;
                    _tbl_inbox.scrollEnabled = false;
                    inbox_APICallingFlag = @"0";
                           
                    inboxFilterString = TAGFILTERINBOX;
                    selectedTagIdForFilter = data;
                    
                    if ([data isEqualToString:@"Clear"]) {
                        selInboxTxtTohighlight = @"Clear";
                        [self.tbl_inbox reloadData];
                        
                    }
                    else
                    {
                        NSArray *arr = [[Default objectForKey:kTagList] valueForKey:@"id"];
                        NSUInteger index = [arr indexOfObject:data];
                        selInboxTxtTohighlight = [[[Default objectForKey:kTagList] objectAtIndex:index] valueForKey:@"name"];
                        [self getInboxLogs:@"0"];
                    }
                    
                    
                   
                }
                
            }
           
        }
    }
}
-(void)showEmptyInboxLable
{
    if ([inboxFilterString isEqualToString:ALLINBOX]) {
        if([[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs].count==0)
        {
            _lblEmptyConversation.hidden = false;
            _lblEmptyConversation.text = @"There is no calls in your inbox!";
            [refreshControl removeFromSuperview];
            refreshControl = nil;
            
        }
        else
        {
            _lblEmptyConversation.hidden = true;

        }
    }
    else if ([inboxFilterString isEqualToString:MISSEDINBOX])
    {
        if([[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs].count==0)
        {
            _lblEmptyConversation.hidden = false;
            _lblEmptyConversation.text = @"There is no calls in your inbox!";
            [refreshControl removeFromSuperview];
            refreshControl = nil;
            
        }
        else
        {
            _lblEmptyConversation.hidden = true;

        }
    }
    else if ([inboxFilterString isEqualToString:VOICEMAILINBOX])
    {
        if([[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs].count==0)
        {
                _lblEmptyConversation.hidden = false;
                _lblEmptyConversation.text = @"There is no calls in your inbox!";
                [refreshControl removeFromSuperview];
                refreshControl = nil;
                   
        }
        else
        {
                _lblEmptyConversation.hidden = true;
        }
    }
    else if ([inboxFilterString isEqualToString:FEEDBACKINBOX])
    {
        if([[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs].count==0)
        {
                _lblEmptyConversation.hidden = false;
                _lblEmptyConversation.text = @"There is no calls in your inbox!";
                [refreshControl removeFromSuperview];
                refreshControl = nil;
                   
        }
        else
        {
                _lblEmptyConversation.hidden = true;
        }
    }
    else if ([inboxFilterString isEqualToString:TAGFILTERINBOX])
    {
        if([[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs].count==0)
        {
                _lblEmptyConversation.hidden = false;
                _lblEmptyConversation.text = @"There is no calls in your inbox!";
                [refreshControl removeFromSuperview];
                refreshControl = nil;
                   
        }
        else
        {
                _lblEmptyConversation.hidden = true;
        }
    }
    
}
-(IBAction)markSelectedAsComplete:(id)sender
{
    [self markAsCompleteByAPICAll:YES];
}
-(IBAction)clearSelected:(id)sender
{
    [_tbl_inbox setEditing:false animated:true];
    [SelectedLogIndexArr removeAllObjects];
    [SelectedInboxLogsid removeAllObjects];
    
    [_tbl_inbox reloadData];
    _view_mark.hidden = true;
    
}
-(void)markAsCompleteByAPICAll:(BOOL)showProgress
{
    
    updateAfterMarkAsRead = NO;
    //updateAfterMarkAsRead = YES;
    if (showProgress) {
        
        updateAfterMarkAsRead = YES;
       [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    }
    
    
    
        NSString *userId = [Default valueForKey:USER_ID];

        NSString *url = [NSString stringWithFormat:@"%@/readinbox",userId];
        
        NSString *markAsCompleteId = [SelectedInboxLogsid componentsJoinedByString:@","];
        
        NSDictionary *passDict = @{
            @"logIds":markAsCompleteId
        };

        NSLog(@"logs id : %@",passDict);

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
        obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(markAsCompleteResponse:response:) andDelegate:self];
       
       //before [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(markAsCompleteResponse:response:) andDelegate:self];
}
- (void)markAsCompleteResponse:(NSString *)apiAlias response:(NSData *)response{
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
   // NSLog(@"mark as complete log : %@",response1);
    
    NSLog(@"Encrypted Response : mark as complete  : %@",response1);

    
    [Processcall hideLoadingWithView];
    
        if([apiAlias isEqualToString:Status_Code])
        {
                //[UtilsClass logoutUser:self];
             [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
                if ([[response1 valueForKey:@"success"] integerValue] == 1)
                {
                    [SelectedLogIndexArr removeAllObjects];
                    [SelectedInboxLogsid removeAllObjects];
                    
                    [_tbl_inbox setEditing:false animated:YES];
                    _view_mark.hidden = true;

                   // [_tbl_inbox reloadData];
                    
                    if (updateAfterMarkAsRead) {
                        [self getInboxLogs:@"0"];
                    }

                    
                }
        }
}
-(IBAction)inboxRowSelect:(UIButton *)sender
{
    
    if (![_tbl_inbox isEditing]) {
        
    
    NSString *keyname = @"";
       NSMutableArray *arr;
    
    //int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
        int sec = [NSString stringWithFormat:@"%@", sender.titleLabel.text].intValue;
        int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
    
    if ([inboxFilterString isEqualToString:ALLINBOX]) {
           
        
           keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:sec];
           arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
       }
       else if ([inboxFilterString isEqualToString:MISSEDINBOX])
       {
           keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:sec];
           arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
       }
       else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
       {
           keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:sec];
           arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
       }
    else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
    {
        keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:sec];
        arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
    }
        else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:sec];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
        }
    
     if (arr.count > 0) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    //        dic = arr[cell.tag];
            dic = [[NSMutableDictionary alloc]initWithDictionary:arr[row]];
         
         //mark as complete
         if ([[dic valueForKey:@"read"] intValue] == 0) {
         [SelectedInboxLogsid addObject:[dic valueForKey:@"_id"]];
         [self markAsCompleteByAPICAll:NO];
         }
         
            NSString *callfromnumber = @"";//= [Default valueForKey:SELECTEDNO];
            NSString *callToName= @"";
            NSString *callTonumber = @"";
            NSString *callDepartment = @"";
            NSString *contactType = [dic valueForKey:@"contactType"];
            NSString *warmTransferFrom = [dic valueForKey:@"warmTransferFrom"];
            NSString *warmTransferTo = [dic valueForKey:@"warmTransferTo"];
            if ([[dic valueForKey:@"callType"]  isEqual: @"Incoming"])
            {
                if ([[dic valueForKey:@"fromName"]  isEqual: @""])
                {
                    callToName = [dic valueForKey:@"from"];// "\(userData?.from! ?? "")";
                }
                else
                {
                    callToName = [dic valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
                    
                }
                
                callfromnumber = [dic valueForKey:@"to"];
                
                [Default setValue:callfromnumber forKey:SELECTEDNO];
                //                [Default setValue:callDepartment forKey:Selected_Department];
                callTonumber = [dic valueForKey:@"from"];//"\(userData?.from! ?? "")";
                callDepartment = [NSString stringWithFormat:@"%@ ",[dic valueForKey:@"toName"]];
                [Default setValue:callDepartment forKey:Selected_Department];
                
            }
            else
            {
                
                if ([[dic valueForKey:@"toName"]  isEqual: @""])
                {
                    callToName =  [dic valueForKey:@"to"];//"\(userData?.to! ?? "")"
                }
                else
                {
                    callToName = [dic valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
                }
                callfromnumber = [dic valueForKey:@"from"];
                
                [Default setValue:callfromnumber forKey:SELECTEDNO];
                //                [Default setValue:callDepartment forKey:Selected_Department];
                callTonumber = [dic valueForKey:@"to"];// "\(userData?.to! ?? "")"
                callDepartment = [NSString stringWithFormat:@"%@ ",[dic valueForKey:@"fromName"]];
                [Default setValue:callDepartment forKey:Selected_Department];
            }
            
            NSString *transferflag = [dic valueForKey:@"warmTransferFlag"];
            int num = [transferflag intValue];
            if(num != 1)
            {
            if([UtilsClass isNetworkAvailable])
            {
               //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
                
                NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                
                if(purchase_number.count > 0 || callTonumber.length == 4 || callTonumber.length == 5)
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                    NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                    
                   
                    NSString *numbVer = @"";
                    if (results.count == 0 ){
                        numbVer = @"1";
                    }else{
                        NSDictionary *dic1 = [results objectAtIndex:0];
                        numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                    }
                    int num = [numbVer intValue];
                    if (num == 1  || callTonumber.length == 4 || callTonumber.length == 5) {
                        NSString *credit = [Default valueForKey:CREDIT];
                        float cre = [credit floatValue];
                        if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                            switch ([[AVAudioSession sharedInstance] recordPermission]) {
                                case AVAudioSessionRecordPermissionGranted:
                                {
                                    if ([UtilsClass isValidNumber:callTonumber])
                                    {
                                        [Default setValue:callfromnumber forKey:SELECTEDNO];
                                       
                                        
                                        NSString *imagename = [[dic  objectForKey:@"shortName"] lowercaseString];
                                        [Default setValue:imagename forKey:Selected_Department_Flag];
                                        
                                       
                                        
                                        [UtilsClass make_outgoing_call_validate1:self callfromnumber:callfromnumber ToName:callToName ToNumber:callTonumber calltype:callDepartment Mixallcontact:Mixallcontact];
                                    }
                                    else
                                    {
                                        [UtilsClass makeToast:@"Number you have dialed invalid."];
                                    }
                                    break;
                                }
                                case AVAudioSessionRecordPermissionDenied:
                                {
                                    //                [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
                                    [self micPermiiton];
                                    break;
                                }
                                case AVAudioSessionRecordPermissionUndetermined:
                                    // This is the initial state before a user has made any choice
                                    // You can use this spot to request permission here if you want
                                    break;
                                default:
                                    break;
                                    
                            }}else {
                                [UtilsClass showAlert:kCREDITLAW contro:self];
                            }}else{
                                [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                            }}else{
                                [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                            }
            }
            else
            {
                [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
            }
            }else{
                
                NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                
                if(purchase_number.count > 0 || [contactType isEqualToString:@"SubUser"])
                {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                    NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                    
                  
                    NSString *numbVer = @"";
                    if (results.count == 0){
                        numbVer = @"1";
                    }else{
                        NSDictionary *dic1 = [results objectAtIndex:0];
                        numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                    }
                    int num = [numbVer intValue];
                    if (num == 1) {
                        NSString *credit = [Default valueForKey:CREDIT];
                        float cre = [credit floatValue];
                        if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                            
                            if ([contactType isEqualToString:@"SubUser"]) {
                                [Default setValue:@"true" forKey:ExtentionCall];
                                [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                                [Default setValue:@"+1" forKey:klastDialCountryCode];
                                [Default setValue:@"United States" forKey:klastDialCountryName];
                                [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:contactType ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                            }else{
                                [Default setValue:@"false" forKey:ExtentionCall];
                                [Default setValue:@"" forKey:ExtentionCallProvider];
                                [Default setValue:warmTransferFrom forKey:SELECTEDNO];
                                [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:warmTransferFrom ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                            }}else {
                                [UtilsClass showAlert:kCREDITLAW contro:self];
                            }}else{
                                [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                            }}else{
                    [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                }
            }
        }
        
    }
}
-(IBAction)inboxNextClick:(UIButton *)sender
{
    if (![_tbl_inbox isEditing]) {
        
    
    
    int sec = [NSString stringWithFormat:@"%@", sender.titleLabel.text].intValue;
    int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
        
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
       
        NSString *keyname = @"";
        NSMutableArray *arr;
        
        if ([inboxFilterString isEqualToString:ALLINBOX]) {
            
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionAllInboxLogs] objectAtIndex:sec];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsAll] objectForKey:keyname];
        }
        else if ([inboxFilterString isEqualToString:MISSEDINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionMissedInboxLogs] objectAtIndex:sec];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsMissed] objectForKey:keyname];
        }
        else if([inboxFilterString isEqualToString:VOICEMAILINBOX])
        {
            keyname = [[[InboxLogsModel sharedInboxLogsData] sectionVoicemailInboxLogs] objectAtIndex:sec];
            arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsVoicemail] objectForKey:keyname];
        }
    else if([inboxFilterString isEqualToString:FEEDBACKINBOX])
    {
        keyname = [[[InboxLogsModel sharedInboxLogsData] sectionFeddbackInboxLogs] objectAtIndex:sec];
        arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsFeedback] objectForKey:keyname];
    } else if([inboxFilterString isEqualToString:TAGFILTERINBOX])
           {
               keyname = [[[InboxLogsModel sharedInboxLogsData] sectionTagInboxLogs] objectAtIndex:sec];
               arr = [[[InboxLogsModel sharedInboxLogsData] GroupedInboxLogsTag] objectForKey:keyname];
           }
        
        
        if (arr.count > 0) {
            
    //        dict = arr[row];
            dict = [[NSMutableDictionary alloc]initWithDictionary:arr[row]];
            
            if ([[dict valueForKey:@"read"] intValue] == 0) {
            [SelectedInboxLogsid addObject:[dict valueForKey:@"_id"]];
            [self markAsCompleteByAPICAll:NO];
            }
       
            CalllogsDetailsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CalllogsDetailsVC"];
            vc.userData = dict;
            vc.displayUrl = @"1";
            vc.contactDelegate = self;
            vc.fromInbox  =@"1";
            gotoDetailFromInbox = true;

            [[self navigationController] pushViewController:vc animated:YES];
        }
        
    }
}
#pragma mark -  tag call logs custom methods

-(void)loadTagCalllogs {
    
    tagFilterIdStr = @"";
    tagFilterString = @"";
    
        tagLogsArray = [[NSMutableArray alloc] init];
       dateArr_tag = [[NSMutableArray alloc] init];
       
       //        self.tbl_logs.delegate = self;
       //        self.tbl_logs.dataSource = self;
       
       tagcalllogs = [[NSMutableArray alloc] init];
       grouped_calllogsTag = [NSMutableDictionary new];
       grouped_calllogs_keys_tag = [NSMutableArray new];
       
    
    
           
          if([UtilsClass isNetworkAvailable])
          {
              tagFilterString = @"";
              
              [self GetTagCallLogs:@"0"];
          }else {
              [_tbl_logs reloadData];
              [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
          }
           
     

}

-(void)GetTagCallLogs:(NSString*)skip{
    
    _lblEmptyConversation.hidden = true;

    
   
      NSString *userId = [Default valueForKey:USER_ID];
      NSString *url = @"";
      if ([tagFilterString isEqualToString:@""]) {
          
          if ([skip intValue] == 0)
            {
                
                [[CallLogsModel sharedCallLogsData] removeAllObjectsFromArray:ALLLOGS];
                [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
            }
          
          if ([pullTorefreshFlagForAll isEqualToString:@"1"]) {
              
            NSMutableDictionary *alllogs =  [[CallLogsModel sharedCallLogsData] GroupedCallLogsAll];
            NSString *lastAddedDate = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] firstObject];
              
              
            NSString *lastCreatedDate = [[[alllogs objectForKey:lastAddedDate] firstObject] valueForKey:@"createdDate"];
              
              
            url = [NSString stringWithFormat:@"%@/callLogMobile?skip=0&createdDate=%@",userId,lastCreatedDate];
          }
          else
          {
               url = [NSString stringWithFormat:@"%@/callLogMobile?skip=%@&limit=20",userId,skip];
          }
            
          
         
  }
      else
      {
          
          if ([skip intValue] == 0)
            {
                
                [dateArr_tag removeAllObjects];
                [tagLogsArray removeAllObjects];
                [grouped_calllogsTag removeAllObjects];
                [grouped_calllogs_keys_tag removeAllObjects];
                
                tagLogsArray = [[NSMutableArray alloc] init];
                dateArr_tag = [[NSMutableArray alloc] init];
            }
          
            if ([skip intValue] > 0)
            {
                
            }
            else
            {
                [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
            }
            
          
          url = [NSString stringWithFormat:@"%@/callLogMobile?skip=%@&limit=20&tag=%@",userId,skip,tagFilterIdStr];
     
      }
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(tagLogsResponse:response:) andDelegate:self];
    
    NSLog(@"call logs tag url:: %@",url);
    
}
- (void)tagLogsResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    if([apiAlias isEqualToString:Status_Code])
    {
            [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        
    }
    else
    {
        NSLog(@"Encrypted Response : tag logs : %@",response1);

        //NSLog(@"call logs tag response:: %@",response1);
        
        NSMutableArray *allData = [[NSMutableArray alloc]init];
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            if ([tagFilterString isEqualToString:@""]) {
                
                //no any filter on logs
                NSMutableArray *logs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
                
                if (logs.count > 0) {
                                   
                   if ([pullTorefreshFlagForAll isEqualToString:@"1"]) {
                       
                        [[CallLogsModel sharedCallLogsData] addObjectsAtTop:logs toArray:ALLLOGS];
                   }
                    else
                    {
                           [[CallLogsModel sharedCallLogsData] addObjectsAtBottom:logs toArray:ALLLOGS];
                    }
             
                    [self showEmptyTagLable];
            }
            else
            {
                    [self showEmptyTagLable];
            }
                
                   self.tbl_logs.delegate = self;
                self.tbl_logs.dataSource = self;
                
                
                if(is_tag_tbl_reload == true)
                {
                                               
                    self.tbl_logs.contentOffset = CGPointZero;
                                
                }
                is_tag_tbl_reload = true;
                
                _tbl_logs.scrollEnabled = true;
                
                [self.tbl_logs reloadData];
                
                //APICallingFlag = @"0";
                
            }
            else
            {
            
                //if tag filter applied
                
            tagcalllogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
            
            NSLog(@"Calllogs List :%@",tagcalllogs);
            
            if ([tagcalllogs count] > 0)
            {
                
                 _lblEmptyConversation.hidden = true;
                
                
                int tot = -1;
                NSMutableArray *arrayWithSameDate = [NSMutableArray new];
                for (int i = 0; i<tagcalllogs.count; i++)
                {
                    tot = i+1;
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:tagcalllogs[i]];
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";
                    [tagLogsArray addObject:tagcalllogs[i]];
                    
                    [tagcalllogs replaceObjectAtIndex:i withObject:dict];
                    
                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = grouped_calllogsTag[dateToshow];
                    if(! arrayWithSameDate)
                    {
                        [self->dateArr_tag addObject:dateToshow];
                        [self->grouped_calllogs_keys_tag addObject:dateToshow];
                        arrayWithSameDate = [NSMutableArray new];
                        grouped_calllogsTag[dateToshow] = arrayWithSameDate;
                        //grouped_calllogs[dateToshow] = arrayWithSameDate;
                    }
                    [arrayWithSameDate addObject: dict];
                }
                if(tot == tagcalllogs.count)
                {
                    self.tbl_logs.delegate = self;
                    self.tbl_logs.dataSource = self;
                    [self.tbl_logs reloadData];
                    if(is_tag_tbl_reload == true)
                    {
                        [self.tbl_logs scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                    }
                    is_tag_tbl_reload = true;
                }else{
                    //NSLog(@"tableview data null");
                }
                
                _lblEmptyConversation.hidden = true;

                
            }else{
                if(dateArr_tag.count < 1){
                    
                    _lblEmptyConversation.hidden = false;
                    _lblEmptyConversation.text = @"You have no calls in logs";
                }
                else
                {
                    _lblEmptyConversation.hidden = true;

                }
                self.tbl_logs.delegate = self;
                self.tbl_logs.dataSource = self;
                [self.tbl_logs reloadData];
                
            }
            
        }
        }else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
}
-(void)showEmptyTagLable
{
    if ([tagFilterString isEqualToString:@""]) {
        
        if([[CallLogsModel sharedCallLogsData] sectionAllLogs].count==0)
        {
            _lblEmptyConversation.hidden = false;
            _lblEmptyConversation.text = @"You have no calls in logs";
        }
        else
        {
            _lblEmptyConversation.hidden = true;
        }
    }
}

#pragma mark - Contact Methods

-(void)GetNumberFromContact
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        //NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
            // Contact one each function block is executed whenever you get
            NSString *phoneNumber = @"";
            if( contact.phoneNumbers)
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            
            
            
            
            NSString *FullName = [NSString stringWithFormat:@"%@%@",contact.givenName,contact.familyName];
            NSString *Number = [NSString stringWithFormat:@"%@",phoneNumber];
            if([Number isEqualToString:@"(null)"])
            {
                Number = @"";
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:FullName forKey:@"name"];
            [dic setObject:Number forKey:@"number"];
            [arrContactList addObject:contact];
            [Mixallcontact addObject:dic];
        }];
        
        //        //NSLog(@"arr contact : %lu",(unsigned long)arrContactList.count);
        
    }
}
-(void)getCallHippoContactList
{
    
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contact",userId];
    
    obj = [[WebApiController alloc] init];
    
    NSLog(@"URL Contact check : %@",aStrUrl);

    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallHippoContactListresponse:response:) andDelegate:self];
}
- (void)getCallHippoContactListresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : callhippo contacts : %@",response1);

        
        //NSLog(@"getCallHippoContactListresponse : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            arrcontact = [response1 valueForKey:@"data"];
            [Mixallcontact addObjectsFromArray:arrcontact];
        } else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
}




#pragma mark - recording old methods (currently not in use)
-(void)play_transfer_recoding:(UIButton*)sender
{
    //    NSMutableDictionary *dict = [self getSectionwithsender:sender];
    
    int sec = [NSString stringWithFormat:@"%@", sender.titleLabel.text].intValue;
    int row = [NSString stringWithFormat:@"%ld", (long)sender.tag].intValue;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    //    NSString *keyname = grouped_calllogs.allKeys[sec];
    //    NSMutableArray *arr = grouped_calllogs[keyname];
    
    

        
        NSString *keyname = @"";
        NSMutableArray *arr;
        
        if ([filter_string isEqualToString:ALLLOGS]) {
            
            keyname = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] objectAtIndex:sec];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsAll] objectForKey:keyname];
        }
        else if ([filter_string isEqualToString:MISSEDLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] objectAtIndex:sec];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed] objectForKey:keyname];
        }
        else if([filter_string isEqualToString:VOICEMAILLOGS])
        {
            keyname = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] objectAtIndex:sec];
            arr = [[[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail] objectForKey:keyname];
        }
        
        if (arr.count > 0) {
            
//        dict = arr[row];
        dict = [[NSMutableDictionary alloc]initWithDictionary:arr[row]];


        
       
        CalllogsDetailsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"CalllogsDetailsVC"];
        NSArray *sub = dict[@"transfers"];
        vc.userData = dict;
            vc.contactDelegate = self;
            
        if ([[dict valueForKey:@"callType"]  isEqual: @"Incoming"]){
            if ([[dict valueForKey:@"toName"]  isEqual: @""])
            {
                [dict setValue:[dict valueForKey:@"to"] forKey:@"fromDepart"];
                
            } else
            {
                [dict setValue:[dict valueForKey:@"toName"] forKey:@"fromNameDepart"];
            }
        }else{
            if ([[dict valueForKey:@"fromName"]  isEqual: @""]) {
                //             img_callflag.image = UIImage(named: "")
                [dict setValue:[dict valueForKey:@"from"] forKey:@"fromDepart"];//"\(userData?.from! ?? "")"
            } else {
                //             img_callflag.image = UIImage(named: "")
                [dict setValue:[dict valueForKey:@"fromName"] forKey:@"fromNameDepart"];
            }
        }
        vc.userData[@"callStatus"] = sub[0][@"callStatus"];
        vc.userData[@"callNotes"] = sub[0][@"callNotes"];
        vc.userData[@"callDuration"] = sub[0][@"callDuration"];
        vc.userData[@"recordingUrl"] = sub[0][@"recordingUrl"];
        
        if ([sub[0][@"toName"] isEqualToString:sub[0][@"to"]]){
            vc.userData[@"to"] = @"";
            vc.userData[@"toName"] = sub[0][@"toName"];
            vc.userData[@"fromName"] = sub[0][@"toName"];
            vc.userData[@"from"] = @"";
        }else{
            vc.userData[@"to"] = sub[0][@"to"];;
            vc.userData[@"toName"] = sub[0][@"toName"];
            vc.userData[@"fromName"] = sub[0][@"toName"];
            vc.userData[@"from"] = sub[0][@"to"];
        }
        
        
        vc.displayUrl = [NSString stringWithFormat:@"%@",sub[0][@"recordingDisplay"]];
        vc.transferCall = @"yes";
        [[self navigationController] pushViewController:vc animated:YES];
    }
    
}

-(void) playaudio:(NSString *)audioString{
    @try {
        NSURL *audioURL = [NSURL URLWithString:audioString];
        
        AVPlayerItem *PlayerItem = [AVPlayerItem playerItemWithURL:audioURL];
        //NSLog(@"playaudio PlayerItem : %@",PlayerItem);
        audioPlayer = [AVPlayer playerWithPlayerItem:PlayerItem];
        audioPlayer.volume = 1.0;
        [audioPlayer play];
        //        print("Play")
        //        start_timer()
        NSError *error = nil;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
    } @catch(NSException *exp) {
        //NSLog(@"playaudio Error : %@",exp);
    }
}
- (IBAction)btn_play_click:(UIButton *)sender
{
    NSString *istransfer = [Default valueForKey:IS_RECORDING];
    int trans = [istransfer intValue];
    if (trans == 1) {
        if ([btn_click  isEqual: @"0"])
        {
            [self audio_start];
            //        [self playaudio];
            //        [self start_timer];
            btn_click = @"1";
            UIImage *img = [UIImage imageNamed:@"AudioStop"];
            [sender setBackgroundImage:img forState:UIControlStateNormal];
        }
        else
        {
            //        [self stopplay];
            //        [self stop_timer];
            [self audio_start];
            btn_click = @"0";
            UIImage *img = [UIImage imageNamed:@"AudioPlay"];
            [sender setBackgroundImage:img forState:UIControlStateNormal];
            
        }
    }else {
        [UtilsClass showAlert:@"Call Recording is not Available in selected plan." title:@"Please Upgrade your plan." contro:self];
    }
}
- (IBAction)btn_close_click:(UIButton *)sender
{
    [self.view_play_recoding setHidden:true];
    [self.player stop];
}

- (IBAction)slide_value_change:(UISlider *)sender
{
    [self stopProgressMonitor];
}
- (IBAction)slide_in_out_change:(UISlider *)sender
{
    [self startProgressMonitor];
    UISlider *slider = sender;
    double duration = self.player.duration;
    [self.player seekToTime:slider.value * duration];
    
    
    int curent_time = slider.value;
    int seconds = curent_time % 60;
    int minutes = (curent_time / 60) % 60;
    int hours = curent_time / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    
}


// custome code
-(void)audio_start
{
    HSUAudioStreamPlayBackState state = self.player.state;
    if (!self.player || state == HSU_AS_STOPPED) {
        NSString *urlString = selected_url;
        NSString *urlHash = [NSString stringWithFormat:@"%@", selected_url];
        NSString *cacheFile = DOC_FILE(urlHash);
        self.player = [[HSUAudioStreamPlayer alloc]
                       initWithURL:[NSURL URLWithString:urlString]
                       cacheFilePath:cacheFile];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(playBackStateChanged:)
         name:HSUAudioStreamPlayerStateChangedNotification
         object:self.player];
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(playBackDurationChanged:)
         name:HSUAudioStreamPlayerDurationUpdatedNotification
         object:self.player];
        [self.player play];
    }
    else if (state == HSU_AS_PLAYING) {
        [self.player pause];
    }
    else if (state == HSU_AS_WAITTING) {
        [self.player stop];
    }
    else if (state == HSU_AS_PAUSED) {
        [self.player play];
    }
    else if (state == HSU_AS_STOPPED) {
        [self.player play];
    }
    else if (state == HSU_AS_FINISHED) {
        [self.player play];
        
    }
    
}
- (void)startProgressMonitor
{
    [self.myTimer invalidate];
    
    btn_click = @"1";
    UIImage *img = [UIImage imageNamed:@"AudioStop"];
    [_btn_play setBackgroundImage:img forState:UIControlStateNormal];
    self.myTimer = [NSTimer
                    scheduledTimerWithTimeInterval:1.0
                    target:self
                    selector:@selector(updateProgress)
                    userInfo:nil
                    repeats:YES];
}
- (void)stopProgressMonitor
{
    [self.myTimer invalidate];
    btn_click = @"0";
    UIImage *img = [UIImage imageNamed:@"AudioPlay"];
    [_btn_play setBackgroundImage:img forState:UIControlStateNormal];
    
}
- (void)updateProgress
{
    double progress = self.player.progress;
    self.slider_view.value = progress;
    
    //    self.lbl_record_time.text = [NSString
    //                                  stringWithFormat:@"%gs",
    //                                  ceil(self.player.currentTime)];
    
    int curent_time = self.player.currentTime;
    int seconds = curent_time % 60;
    int minutes = (curent_time / 60) % 60;
    int hours = curent_time / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    
    
}
- (void)playBackDurationChanged:(NSNotification *)notification
{
    
}

- (void)playBackStateChanged:(NSNotification *)notification
{
    HSUAudioStreamPlayer *player = notification.object;
    HSUAudioStreamPlayBackState state = player.state;
    
    if (state == HSU_AS_PLAYING) {
       
        [self startProgressMonitor];
    }
    else if (state == HSU_AS_PAUSED) {
        
        [self stopProgressMonitor];
    }
    else if (state == HSU_AS_WAITTING) {
        
        [self startProgressMonitor];
    }
    else if (state == HSU_AS_STOPPED) {
        
        [self updateProgress];
        [self stopProgressMonitor];
    }
    else if (state == HSU_AS_FINISHED) {
        
        [self updateProgress];
        [self stopProgressMonitor];
    }
}
#pragma mark - call logs old methods

//-(void)loadCalllogs {
//
//
//    callLogsArr = [[NSMutableArray alloc] init];
//    dateArr = [[NSMutableArray alloc] init];
//
//    //        self.tbl_logs.delegate = self;
//    //        self.tbl_logs.dataSource = self;
//
//    calllogs = [[NSMutableArray alloc] init];
//    grouped_calllogs = [NSMutableDictionary new];
//    grouped_calllogs_keys = [NSMutableArray new];
//
//
//    if([UtilsClass isNetworkAvailable])
//    {
//
//       /* if([filter_string isEqualToString:@""])
//        {
//            if ([[[GlobalData sharedGlobalData] callLogs_global_array] count] > 0) {
//
//             [self createCallLogsArrayWithSection:[[GlobalData sharedGlobalData] callLogs_global_array]];
//
//               pullTorefreshFlag = @"1";
//                [self GetCallLogs:[NSString stringWithFormat:@"%lu",(unsigned long)[[[GlobalData sharedGlobalData] callLogs_global_array] count]]];
//
//            }
//            else
//            {
//                    [self GetCallLogs:@"0"];
//            }
//        }*/
//
//        if ([filter_string isEqualToString:ALLLOGS]) {
//
//
//
//            if ([[[CallLogsModel sharedCallLogsData] sectionAllLogs] count] > 0) {
//
////                NSString *count = [[CallLogsModel sharedCallLogsData] getTotalCount:ALLLOGS];
////                [self GetCallLogs:count];
//
//                // to refresh logs on incoming call and go to detail page and call
//
//                pullTorefreshFlagForAll = @"1";
//
//                // pass 1 because is pass 0 then array cleared in get calllogs
//                is_logsTbl_reload = false;
//                 [self GetCallLogs:@"1"];
//
//            }
//            else
//            {
//                [self GetCallLogs:@"0"];
//            }
//        }
//
//
//
//    }else {
//        [_tbl_logs reloadData];
//        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
//    }
//}
//
//-(void)GetCallLogs:(NSString*)skip{
//    //NSLog(@"skit  :%@",skip);
//
//    _lblEmptyConversation.hidden = true;
//
//
//    if ([skip intValue] == 0)
//    {
//        [[CallLogsModel sharedCallLogsData] removeAllObjectsFromArray:filter_string];
//         [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
//    }
//
//    NSLog(@"api flag in call logs::: %@",APICallingFlag);
//
//    if ([APICallingFlag isEqualToString:@"0"] ) {
//
//        NSLog(@"inside aPI flag");
//
//       APICallingFlag = @"1";
//
//
//    NSString *userId = [Default valueForKey:USER_ID];
//    NSString *url = @"";
//     obj = [[WebApiController alloc] init];
//
//        if ([filter_string isEqualToString:ALLLOGS]) {
//
//
//            if([pullTorefreshFlagForAll isEqualToString:@"1"])
//            {
//
//                NSMutableDictionary *alllogs =  [[CallLogsModel sharedCallLogsData] GroupedCallLogsAll];
//                NSString *lastAddedDate = [[[CallLogsModel sharedCallLogsData] sectionAllLogs] firstObject];
//
//
//                NSString *lastCreatedDate = [[[alllogs objectForKey:lastAddedDate] firstObject] valueForKey:@"createdDate"];
//
//
//                url = [NSString stringWithFormat:@"%@/callLogMobile?skip=0&createdDate=%@",userId,lastCreatedDate];
//
//            }
//            else
//            {
//                url = [NSString stringWithFormat:@"%@/callLogMobile?skip=%@&limit=20",userId,skip];
//            }
//            filterUrlCalled = ALLLOGS;
//             [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(allCallsResponse:response:) andDelegate:self];
//
//        }
//        else if ([filter_string isEqualToString:MISSEDLOGS])
//        {
//            if([pullTorefreshFlagForMissed isEqualToString:@"1"])
//            {
//
//                NSMutableDictionary *alllogs =  [[CallLogsModel sharedCallLogsData] GroupedCallLogsMissed];
//                NSString *lastAddedDate = [[[CallLogsModel sharedCallLogsData] sectionMissedLogs] firstObject];
//
//
//                NSString *lastCreatedDate = [[[alllogs objectForKey:lastAddedDate] firstObject] valueForKey:@"createdDate"];
//
//
//               url = [NSString stringWithFormat:@"%@/callLogMobile?skip=0&createdDate=%@&filterby=%@",userId,lastCreatedDate,@"missed"];
//            }
//            else
//            {
//                 url = [NSString stringWithFormat:@"%@/callLogMobile?skip=%@&limit=20&filterby=%@",userId,skip,@"missed"];
//            }
//            filterUrlCalled = MISSEDLOGS;
//            NSLog(@"Calllogs : %@",url);
//            [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(missedCallsResponse:response:) andDelegate:self];
//
//        }
//        else if ([filter_string isEqualToString:VOICEMAILLOGS])
//        {
//            if([pullTorefreshFlagForVoice isEqualToString:@"1"])
//            {
//
//                NSMutableDictionary *alllogs =  [[CallLogsModel sharedCallLogsData] GroupedCallLogsVoicemail];
//                NSString *lastAddedDate = [[[CallLogsModel sharedCallLogsData] sectionVoicemailLogs] firstObject];
//
//
//                NSString *lastCreatedDate = [[[alllogs objectForKey:lastAddedDate] firstObject] valueForKey:@"createdDate"];
//
//
//                url = [NSString stringWithFormat:@"%@/callLogMobile?skip=0&createdDate=%@&filterby=%@",userId,lastCreatedDate,@"Voicemail"];
//            }
//            else
//            {
//                url = [NSString stringWithFormat:@"%@/callLogMobile?skip=%@&limit=20&filterby=%@",userId,skip,@"Voicemail"];
//            }
//
//            filterUrlCalled = VOICEMAILLOGS;
//
//            NSLog(@"Calllogs : %@",url);
//
//
//            [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(voicemailCallsResponse:response:) andDelegate:self];
//        }
//
//        NSLog(@"url:: %@",url);
//
//
//
//
//    }
//
//
//
//
//}
//- (void)allCallsResponse:(NSString *)apiAlias response:(NSData *)response{
//
//    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//
//    [Processcall hideLoadingWithView];
//
//    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
//    if([apiAlias isEqualToString:Status_Code])
//    {
//       // [UtilsClass logoutUser:self];
//         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
//    }
//    else
//    {
//        NSMutableArray *allData = [[NSMutableArray alloc]init];
//        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//
//            calllogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
//            NSLog(@"Calllogs List :%@",calllogs);
//
//                if (calllogs.count > 0) {
//
//                    if ([pullTorefreshFlagForAll isEqualToString:@"1"])
//                    {
//                        pullTorefreshFlagForAll = @"0";
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtTop:calllogs toArray:ALLLOGS];
//
//                    }
//                    else
//                    {
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtBottom:calllogs toArray:ALLLOGS];
//                    }
//
//
//                }
//                else
//                {
//                    pullTorefreshFlagForAll = @"0";
//
//                    [self showEmptyCallLogLable];
//                }
//
//                self.tbl_logs.delegate = self;
//                self.tbl_logs.dataSource = self;
//
//
//                if(is_logsTbl_reload == true)
//                {
//                                //[self.tbl_logs scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                                //[self.tbl_logs setContentOffset:CGPointZero animated:YES];
//
//                //                [UIView animateWithDuration:0.1 delay:0.5 options:0 animations:^{
//                                    self.tbl_logs.contentOffset = CGPointZero;
//                //                } completion:^(BOOL finished) {
//                //                     [self.tbl_logs reloadData];
//                //                }];
//                            }
//                is_logsTbl_reload = true;
//
//                _tbl_logs.scrollEnabled = true;
//
//
//            [self.tbl_logs reloadData];
//
//             APICallingFlag = @"0";
//
//
//        }else {
//
//             APICallingFlag = @"0";
//
//            @try {
//                if (response1 != (id)[NSNull null] && response1 != nil ){
//                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
//                }
//            }
//            @catch (NSException *exception) {
//            }
//        }
//
//    }
//
//}
//- (void)missedCallsResponse:(NSString *)apiAlias response:(NSData *)response{
//
//    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//
//    [Processcall hideLoadingWithView];
//
//    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
//    if([apiAlias isEqualToString:Status_Code])
//    {
//        //[UtilsClass logoutUser:self];
//         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
//    }
//    else
//    {
//        NSMutableArray *allData = [[NSMutableArray alloc]init];
//        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//
//            calllogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
//            NSLog(@"Calllogs List :%@",calllogs);
//
//                if (calllogs.count > 0) {
//
//                    if ([pullTorefreshFlagForMissed isEqualToString:@"1"])
//                    {
//                        pullTorefreshFlagForMissed = @"0";
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtTop:calllogs toArray:MISSEDLOGS];
//
//                    }
//                    else
//                    {
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtBottom:calllogs toArray:MISSEDLOGS];
//                    }
//
//
//                }
//                else
//                {
//                    pullTorefreshFlagForMissed = @"0";
//
//                    [self showEmptyCallLogLable];
//                }
//
//                self.tbl_logs.delegate = self;
//                self.tbl_logs.dataSource = self;
//
//
//                if(is_logsTbl_reload == true)
//                {
//                                //[self.tbl_logs scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                                //[self.tbl_logs setContentOffset:CGPointZero animated:YES];
//
//                //                [UIView animateWithDuration:0.1 delay:0.5 options:0 animations:^{
//                                    self.tbl_logs.contentOffset = CGPointZero;
//                //                } completion:^(BOOL finished) {
//                //                     [self.tbl_logs reloadData];
//                //                }];
//                            }
//                is_logsTbl_reload = true;
//
//                _tbl_logs.scrollEnabled = true;
//
//
//            [self.tbl_logs reloadData];
//
//             APICallingFlag = @"0";
//
//
//        }else {
//
//             APICallingFlag = @"0";
//
//            @try {
//                if (response1 != (id)[NSNull null] && response1 != nil ){
//                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
//                }
//            }
//            @catch (NSException *exception) {
//            }
//        }
//
//    }
//
//}
//- (void)voicemailCallsResponse:(NSString *)apiAlias response:(NSData *)response{
//
//    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//
//    [Processcall hideLoadingWithView];
//
//    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
//    if([apiAlias isEqualToString:Status_Code])
//    {
//       // [UtilsClass logoutUser:self];
//         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
//    }
//    else
//    {
//
//        NSMutableArray *allData = [[NSMutableArray alloc]init];
//        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//
//            calllogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
//            NSLog(@"Calllogs List :%@",calllogs);
//
//                if (calllogs.count > 0) {
//
//                    if ([pullTorefreshFlagForVoice isEqualToString:@"1"])
//                    {
//                        pullTorefreshFlagForVoice = @"0";
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtTop:calllogs toArray:VOICEMAILLOGS];
//
//                    }
//                    else
//                    {
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtBottom:calllogs toArray:VOICEMAILLOGS];
//                    }
//
//
//                }
//                else
//                {
//                    pullTorefreshFlagForVoice = @"0";
//
//                    [self showEmptyCallLogLable];
//                }
//
//
//
//
//                self.tbl_logs.delegate = self;
//                self.tbl_logs.dataSource = self;
//
//
//                if(is_logsTbl_reload == true)
//                {
//                                //[self.tbl_logs scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                                //[self.tbl_logs setContentOffset:CGPointZero animated:YES];
//
//                //                [UIView animateWithDuration:0.1 delay:0.5 options:0 animations:^{
//                                    self.tbl_logs.contentOffset = CGPointZero;
//                //                } completion:^(BOOL finished) {
//                //                     [self.tbl_logs reloadData];
//                //                }];
//                            }
//                is_logsTbl_reload = true;
//
//                _tbl_logs.scrollEnabled = true;
//
//
//            [self.tbl_logs reloadData];
//
//             APICallingFlag = @"0";
//
//
//        }else {
//
//             APICallingFlag = @"0";
//
//            @try {
//                if (response1 != (id)[NSNull null] && response1 != nil ){
//                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
//                }
//            }
//            @catch (NSException *exception) {
//            }
//        }
//
//    }
//
//}
//- (void)login1:(NSString *)apiAlias response:(NSData *)response{
//
//    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//
//    [Processcall hideLoadingWithView];
//
//    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
//    if([apiAlias isEqualToString:Status_Code])
//    {
//        //[UtilsClass logoutUser:self];
//         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
//    }
//    else
//    {
//
//
//
//        NSMutableArray *allData = [[NSMutableArray alloc]init];
//        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//
//            calllogs =  [NSMutableArray arrayWithArray:response1[@"data"][@"callLogs"]];
//            NSLog(@"Calllogs List :%@",calllogs);
//
//
//
//
//
//                if (calllogs.count > 0) {
//
//                    if ([pullTorefreshFlagForAll isEqualToString:@"1"])
//                    {
//                        pullTorefreshFlagForAll = @"0";
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtTop:calllogs toArray:filterUrlCalled];
//
//                    }
//                    else
//                    {
//                        [[CallLogsModel sharedCallLogsData] addObjectsAtBottom:calllogs toArray:filterUrlCalled];
//                    }
//
//
//                }
//                else
//                {
//                    pullTorefreshFlagForAll = @"0";
//
//                    [self showEmptyCallLogLable];
//                }
//
//
//
//
//                self.tbl_logs.delegate = self;
//                self.tbl_logs.dataSource = self;
//
//
//                if(is_logsTbl_reload == true)
//                {
//                                //[self.tbl_logs scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                                //[self.tbl_logs setContentOffset:CGPointZero animated:YES];
//
//                //                [UIView animateWithDuration:0.1 delay:0.5 options:0 animations:^{
//                                    self.tbl_logs.contentOffset = CGPointZero;
//                //                } completion:^(BOOL finished) {
//                //                     [self.tbl_logs reloadData];
//                //                }];
//                            }
//                is_logsTbl_reload = true;
//
//                _tbl_logs.scrollEnabled = true;
//
//
//            [self.tbl_logs reloadData];
//
//             APICallingFlag = @"0";
//
//
//        }else {
//
//             APICallingFlag = @"0";
//
//            @try {
//                if (response1 != (id)[NSNull null] && response1 != nil ){
//                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
//                }
//            }
//            @catch (NSException *exception) {
//            }
//        }
//
//    }
//}
//- (void)refreshTable {
//
//
//    [refreshControl endRefreshing];
//
//    if([UtilsClass isNetworkAvailable])
//    {
//       /* if([filter_string isEqualToString:@""])
//        {
//            filter_string = @"";
//            pullTorefreshFlag = @"1";
//
//            if ([[[GlobalData sharedGlobalData] callLogs_global_array] count] > 0) {
//                [self GetCallLogs:[NSString stringWithFormat:@"%lu",(unsigned long)[[[GlobalData sharedGlobalData] callLogs_global_array] count]]];
//            }
//            else
//            {
//                [self GetCallLogs:@"0"];
//            }
//        }
//        else
//        {
//            //missed or voicemail
//            pullTorefreshFlag = @"1";
//            [self GetCallLogs:[NSString stringWithFormat:@"%lu",(unsigned long)[callLogsArr count]]];
//        }*/
//
//        //pullTorefreshFlag = @"1";
//
//        if ([filter_string isEqualToString:ALLLOGS]) {
//            pullTorefreshFlagForAll = @"1";
//        }
//        else if ([filter_string isEqualToString:MISSEDLOGS])
//        {
//            pullTorefreshFlagForMissed = @"1";
//
//        }
//        else if ([filter_string isEqualToString:VOICEMAILLOGS])
//        {
//            pullTorefreshFlagForVoice = @"1";
//
//        }
//
//        // pass 1 because is pass 0 then array cleared in get calllogs
//        [self GetCallLogs:@"1"];
//
//
//    }else {
//        [_tbl_logs reloadData];
//        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
//    }
//}
@end
