//
//  InboxLogsModel.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InboxLogsModel : NSObject

@property (assign, nonatomic) NSInteger numberOfSections;
@property (assign, nonatomic) NSInteger numberOfLogs;

@property (retain, nonatomic) NSMutableArray *sectionAllInboxLogs;
@property (retain, nonatomic) NSMutableArray *sectionMissedInboxLogs;
@property (retain, nonatomic) NSMutableArray *sectionVoicemailInboxLogs;
@property (retain, nonatomic) NSMutableArray *sectionFeddbackInboxLogs;
@property (retain, nonatomic) NSMutableArray *sectionTagInboxLogs;


@property (retain, nonatomic) NSMutableDictionary *GroupedInboxLogsAll;
@property (retain, nonatomic) NSMutableDictionary *GroupedInboxLogsMissed;
@property (retain, nonatomic) NSMutableDictionary *GroupedInboxLogsVoicemail;
@property (retain, nonatomic) NSMutableDictionary *GroupedInboxLogsFeedback;
@property (retain, nonatomic) NSMutableDictionary *GroupedInboxLogsTag;


+ (InboxLogsModel *)sharedInboxLogsData;


-(NSInteger)numberOfLogsInSection:(NSInteger)section inArray:(NSString *)filterFlag;
-(NSInteger)numberOfSectionsInArray:(NSString *)filterFlag;

-(NSString *)titleForSection:(NSInteger)section inArray:(NSString *)filterFlag;

-(void)removeAllObjectsFromArray:(NSString *)filterFlag;

-(void)addObjectsAtTop:(NSMutableArray *)array toArray:(NSString *)filterFlag;

-(void)addObjectsAtBottom:(NSMutableArray *)array toArray:(NSString *)filterFlag;

-(NSString *)getTotalCount:(NSString *)filterFlag;

@end

NS_ASSUME_NONNULL_END
