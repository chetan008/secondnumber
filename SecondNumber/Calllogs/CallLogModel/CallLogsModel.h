//
//  CallLogsModel.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallLogsModel : NSObject

@property (assign, nonatomic) NSInteger numberOfSections;
@property (assign, nonatomic) NSInteger numberOfLogs;

@property (retain, nonatomic) NSMutableArray *sectionAllLogs;
@property (retain, nonatomic) NSMutableArray *sectionMissedLogs;
@property (retain, nonatomic) NSMutableArray *sectionVoicemailLogs;

@property (retain, nonatomic) NSMutableDictionary *GroupedCallLogsAll;
@property (retain, nonatomic) NSMutableDictionary *GroupedCallLogsMissed;
@property (retain, nonatomic) NSMutableDictionary *GroupedCallLogsVoicemail;

+ (CallLogsModel*)sharedCallLogsData;


-(NSInteger)numberOfLogsInSection:(NSInteger)section inArray:(NSString *)filterFlag;
-(NSInteger)numberOfSectionsInArray:(NSString *)filterFlag;

-(NSString *)titleForSection:(NSInteger)section inArray:(NSString *)filterFlag;

-(void)removeAllObjectsFromArray:(NSString *)filterFlag;

-(void)addObjectsAtTop:(NSMutableArray *)array toArray:(NSString *)filterFlag;

-(void)addObjectsAtBottom:(NSMutableArray *)array toArray:(NSString *)filterFlag;

-(NSString *)getTotalCount:(NSString *)filterFlag;

@end

NS_ASSUME_NONNULL_END
