//
//  CallLogsModel.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "CallLogsModel.h"
#import "Constant.h"
@implementation CallLogsModel
@synthesize numberOfSections;
@synthesize numberOfLogs;

@synthesize sectionAllLogs;
@synthesize sectionMissedLogs;
@synthesize sectionVoicemailLogs;

@synthesize GroupedCallLogsAll;
@synthesize GroupedCallLogsMissed;
@synthesize GroupedCallLogsVoicemail;

static CallLogsModel *sharedCallLogsData = nil;


+ (CallLogsModel *)sharedCallLogsData {
    

    if (sharedCallLogsData == nil) {
        
    sharedCallLogsData = [[super allocWithZone:NULL] init];
        
        sharedCallLogsData.sectionAllLogs = [[NSMutableArray alloc]init];
        sharedCallLogsData.sectionMissedLogs = [[NSMutableArray alloc]init];
        sharedCallLogsData.sectionVoicemailLogs = [[NSMutableArray alloc]init];

    sharedCallLogsData.GroupedCallLogsAll = [[NSMutableDictionary alloc] init];
    sharedCallLogsData.GroupedCallLogsMissed = [[NSMutableDictionary alloc] init];
    sharedCallLogsData.GroupedCallLogsVoicemail = [[NSMutableDictionary alloc] init];
    
    
}
    
    return sharedCallLogsData;
}

-(NSInteger)numberOfLogsInSection:(NSInteger)section inArray:(NSString *)filterFlag
{
    NSArray *temp;
    NSString *key;
    
    if ([filterFlag isEqualToString:ALLLOGS]) {
        
        key = sharedCallLogsData.sectionAllLogs[section];
        temp = [sharedCallLogsData.GroupedCallLogsAll valueForKey:key];
        return temp.count;
    }
    else if ([filterFlag isEqualToString:MISSEDLOGS])
    {
        key = sharedCallLogsData.sectionMissedLogs[section];
        temp = [sharedCallLogsData.GroupedCallLogsMissed valueForKey:key];
        return temp.count;
    }
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
    {
        key = sharedCallLogsData.sectionVoicemailLogs[section];
        temp = [sharedCallLogsData.GroupedCallLogsVoicemail valueForKey:key];
        return temp.count;
    }
    else
    {
        return 0;
    }
}
-(NSInteger)numberOfSectionsInArray:(NSString *)filterFlag
{
    if ([filterFlag isEqualToString:ALLLOGS])
        return sharedCallLogsData.sectionAllLogs.count;
    else if ([filterFlag isEqualToString:MISSEDLOGS])
        return sharedCallLogsData.sectionMissedLogs.count;
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
        return sharedCallLogsData.sectionVoicemailLogs.count;
    else
        return 0;

}
-(NSString *)getTotalCount:(NSString *)filterFlag
{
    int count = 0;
    if ([filterFlag isEqualToString:ALLLOGS])
    {
        
        for (int i=0; i<sharedCallLogsData.sectionAllLogs.count; i++) {
            
            NSString *key = [sharedCallLogsData.sectionAllLogs objectAtIndex:i];
            count += [[sharedCallLogsData.GroupedCallLogsAll objectForKey:key] count];
            
        }
    }
    else if ([filterFlag isEqualToString:MISSEDLOGS])
    {
        for (int i=0; i<sharedCallLogsData.sectionMissedLogs.count; i++) {
            
            NSString *key = [sharedCallLogsData.sectionMissedLogs objectAtIndex:i];
            count += [[sharedCallLogsData.GroupedCallLogsMissed objectForKey:key] count];
            
        }
    }
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
    {
        for (int i=0; i<sharedCallLogsData.sectionVoicemailLogs.count; i++) {
            
            NSString *key = [sharedCallLogsData.sectionVoicemailLogs objectAtIndex:i];
            count += [[sharedCallLogsData.GroupedCallLogsVoicemail objectForKey:key] count];
            
        }
    }
    
    return [NSString stringWithFormat:@"%d",count];
}
-(NSString *)titleForSection:(NSInteger)section inArray:(NSString *)filterFlag
{
    if ([filterFlag isEqualToString:ALLLOGS])
        return [sharedCallLogsData.sectionAllLogs objectAtIndex:section];
    else if ([filterFlag isEqualToString:MISSEDLOGS])
        return [sharedCallLogsData.sectionMissedLogs objectAtIndex:section];
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
        return [sharedCallLogsData.sectionVoicemailLogs objectAtIndex:section];
    else
        return @"";
}
-(void)removeAllObjectsFromArray:(NSString *)filterFlag
{
    if ([filterFlag isEqualToString:ALLLOGS])
    {
        [sharedCallLogsData.sectionAllLogs removeAllObjects];
        [sharedCallLogsData.GroupedCallLogsAll removeAllObjects];
    }
    else if ([filterFlag isEqualToString:MISSEDLOGS])
    {
        [sharedCallLogsData.sectionMissedLogs removeAllObjects];
        [sharedCallLogsData.GroupedCallLogsMissed removeAllObjects];
    }
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
    {
        [sharedCallLogsData.sectionVoicemailLogs removeAllObjects];
        [sharedCallLogsData.GroupedCallLogsVoicemail removeAllObjects];
    }
}
-(void)addObjectsAtTop:(NSMutableArray *)array toArray:(NSString *)filterFlag
{
    
    
    
    if ([filterFlag isEqualToString:ALLLOGS])
    {
        array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
        
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedCallLogsData.GroupedCallLogsAll[dateToshow];
            
            if (!arrayWithSameDate) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedCallLogsData.sectionAllLogs insertObject:dateToshow atIndex:0];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsAll[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
//
                    // if not latest add to top because already reversed
               
                [arrayWithSameDate insertObject:dict atIndex:0];
                sharedCallLogsData.GroupedCallLogsAll[dateToshow] = arrayWithSameDate;
            }
        }
        
//        NSLog(@"object added top:: %@",sharedCallLogsData.GroupedCallLogsAll);
//        NSLog(@"sections:: %@",sharedCallLogsData.sectionAllLogs);
        
    }
    else if ([filterFlag isEqualToString:MISSEDLOGS])
    {
         array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
                
                NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

                for (int i = 0; i < array.count; i++) {
                   
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
                    
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";

                    [array replaceObjectAtIndex:i withObject:dict];

                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = sharedCallLogsData.GroupedCallLogsMissed[dateToshow];
                    
                    if (!arrayWithSameDate) {
                        
                        // section is not exist so create section
                        
                        //we are adding object at the top of the array
                        
                        [sharedCallLogsData.sectionMissedLogs insertObject:dateToshow atIndex:0];
                        arrayWithSameDate = [NSMutableArray new];
                        [arrayWithSameDate addObject:dict];
                        sharedCallLogsData.GroupedCallLogsMissed[dateToshow] = arrayWithSameDate;
                        
                    }
                    else
                    {
                        // section exist
                        
                        //we are adding object at the top of array
        //
                            // if not latest add to top because already reversed
                       
                        [arrayWithSameDate insertObject:dict atIndex:0];
                        sharedCallLogsData.GroupedCallLogsMissed[dateToshow] = arrayWithSameDate;
                    }
                }
    }
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
    {
         array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
                
                NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

                for (int i = 0; i < array.count; i++) {
                   
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
                    
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";

                    [array replaceObjectAtIndex:i withObject:dict];

                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = sharedCallLogsData.GroupedCallLogsVoicemail[dateToshow];
                    
                    if (!arrayWithSameDate) {
                        
                        // section is not exist so create section
                        
                        //we are adding object at the top of the array
                        
                        [sharedCallLogsData.sectionVoicemailLogs insertObject:dateToshow atIndex:0];
                        arrayWithSameDate = [NSMutableArray new];
                        [arrayWithSameDate addObject:dict];
                        sharedCallLogsData.GroupedCallLogsVoicemail[dateToshow] = arrayWithSameDate;
                        
                    }
                    else
                    {
                        // section exist
                        
                        //we are adding object at the top of array
        //
                            // if not latest add to top because already reversed
                       
                        [arrayWithSameDate insertObject:dict atIndex:0];
                        sharedCallLogsData.GroupedCallLogsVoicemail[dateToshow] = arrayWithSameDate;
                    }
                }
    }
}
-(void)addObjectsAtBottom:(NSMutableArray *)array toArray:(NSString *)filterFlag
{
    
    if ([filterFlag isEqualToString:ALLLOGS])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedCallLogsData.GroupedCallLogsAll[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedCallLogsData.sectionAllLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsAll[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsAll[dateToshow] = arrayWithSameDate;
            }
        }
        
//        NSLog(@"object added bottom:: %@",sharedCallLogsData.GroupedCallLogsAll);
//        NSLog(@"sections:: %@",sharedCallLogsData.sectionAllLogs);
    }
    else if ([filterFlag isEqualToString:MISSEDLOGS])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedCallLogsData.GroupedCallLogsMissed[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedCallLogsData.sectionMissedLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsMissed[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsMissed[dateToshow] = arrayWithSameDate;
            }
        }
    }
    else if ([filterFlag isEqualToString:VOICEMAILLOGS])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedCallLogsData.GroupedCallLogsVoicemail[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedCallLogsData.sectionVoicemailLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsVoicemail[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedCallLogsData.GroupedCallLogsVoicemail[dateToshow] = arrayWithSameDate;
            }
        }
    }
    
   
    
    
}

@end

