//
//  InboxLogsModel.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "InboxLogsModel.h"
#import "Constant.h"

@implementation InboxLogsModel

@synthesize numberOfSections;
@synthesize numberOfLogs;

@synthesize sectionAllInboxLogs;
@synthesize sectionMissedInboxLogs;
@synthesize sectionVoicemailInboxLogs;
@synthesize sectionFeddbackInboxLogs;
@synthesize sectionTagInboxLogs;


@synthesize GroupedInboxLogsAll;
@synthesize GroupedInboxLogsMissed;
@synthesize GroupedInboxLogsVoicemail;
@synthesize GroupedInboxLogsFeedback;
@synthesize GroupedInboxLogsTag;


static InboxLogsModel *sharedInboxLogsData = nil;


+ (InboxLogsModel *)sharedInboxLogsData {
    

    if (sharedInboxLogsData == nil) {
        
    sharedInboxLogsData = [[super allocWithZone:NULL] init];
        
        sharedInboxLogsData.sectionAllInboxLogs = [[NSMutableArray alloc]init];
        sharedInboxLogsData.sectionMissedInboxLogs = [[NSMutableArray alloc]init];
        sharedInboxLogsData.sectionVoicemailInboxLogs = [[NSMutableArray alloc]init];
         sharedInboxLogsData.sectionFeddbackInboxLogs = [[NSMutableArray alloc]init];
        sharedInboxLogsData.sectionTagInboxLogs = [[NSMutableArray alloc]init];


    sharedInboxLogsData.GroupedInboxLogsAll = [[NSMutableDictionary alloc] init];
    sharedInboxLogsData.GroupedInboxLogsMissed = [[NSMutableDictionary alloc] init];
    sharedInboxLogsData.GroupedInboxLogsVoicemail = [[NSMutableDictionary alloc] init];
    sharedInboxLogsData.GroupedInboxLogsFeedback = [[NSMutableDictionary alloc] init];
    sharedInboxLogsData.GroupedInboxLogsTag = [[NSMutableDictionary alloc] init];



}
    
    return sharedInboxLogsData;
}
-(NSInteger)numberOfLogsInSection:(NSInteger)section inArray:(NSString *)filterFlag
{
    NSArray *temp;
    NSString *key;
    
    if ([filterFlag isEqualToString:ALLINBOX]) {
        
        key = sharedInboxLogsData.sectionAllInboxLogs[section];
        temp = [sharedInboxLogsData.GroupedInboxLogsAll valueForKey:key];
        return temp.count;
    }
    else if ([filterFlag isEqualToString:MISSEDINBOX])
    {
        key = sharedInboxLogsData.sectionMissedInboxLogs[section];
        temp = [sharedInboxLogsData.GroupedInboxLogsMissed valueForKey:key];
        return temp.count;
    }
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
    {
        key = sharedInboxLogsData.sectionVoicemailInboxLogs[section];
        temp = [sharedInboxLogsData.GroupedInboxLogsVoicemail valueForKey:key];
        return temp.count;
    }
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
    {
        key = sharedInboxLogsData.sectionFeddbackInboxLogs[section];
        temp = [sharedInboxLogsData.GroupedInboxLogsFeedback valueForKey:key];
        return temp.count;
    }
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
    {
        key = sharedInboxLogsData.sectionTagInboxLogs[section];
        temp = [sharedInboxLogsData.GroupedInboxLogsTag valueForKey:key];
        return temp.count;
    }
    else
    {
        return 0;
    }
}
-(NSInteger)numberOfSectionsInArray:(NSString *)filterFlag
{
    if ([filterFlag isEqualToString:ALLINBOX])
        return sharedInboxLogsData.sectionAllInboxLogs.count;
    else if ([filterFlag isEqualToString:MISSEDINBOX])
        return sharedInboxLogsData.sectionMissedInboxLogs.count;
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
        return sharedInboxLogsData.sectionVoicemailInboxLogs.count;
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
        return sharedInboxLogsData.sectionFeddbackInboxLogs.count;
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
        return sharedInboxLogsData.sectionTagInboxLogs.count;
    else
        return 0;

}
-(NSString *)getTotalCount:(NSString *)filterFlag
{
    int count = 0;
    if ([filterFlag isEqualToString:ALLINBOX])
    {
        
        for (int i=0; i<sharedInboxLogsData.sectionAllInboxLogs.count; i++) {
            
            NSString *key = [sharedInboxLogsData.sectionAllInboxLogs objectAtIndex:i];
            count += [[sharedInboxLogsData.GroupedInboxLogsAll objectForKey:key] count];
            
        }
    }
    else if ([filterFlag isEqualToString:MISSEDINBOX])
    {
        for (int i=0; i<sharedInboxLogsData.sectionMissedInboxLogs.count; i++) {
            
            NSString *key = [sharedInboxLogsData.sectionMissedInboxLogs objectAtIndex:i];
            count += [[sharedInboxLogsData.GroupedInboxLogsMissed objectForKey:key] count];
            
        }
    }
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
    {
        for (int i=0; i<sharedInboxLogsData.sectionVoicemailInboxLogs.count; i++) {
            
            NSString *key = [sharedInboxLogsData.sectionVoicemailInboxLogs objectAtIndex:i];
            count += [[sharedInboxLogsData.GroupedInboxLogsVoicemail objectForKey:key] count];
            
        }
    }
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
    {
        for (int i=0; i<sharedInboxLogsData.sectionFeddbackInboxLogs.count; i++) {
            
            NSString *key = [sharedInboxLogsData.sectionFeddbackInboxLogs objectAtIndex:i];
            count += [[sharedInboxLogsData.GroupedInboxLogsFeedback objectForKey:key] count];
            
        }
    }
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
    {
        for (int i=0; i<sharedInboxLogsData.sectionTagInboxLogs.count; i++) {
            
            NSString *key = [sharedInboxLogsData.sectionTagInboxLogs objectAtIndex:i];
            count += [[sharedInboxLogsData.GroupedInboxLogsTag objectForKey:key] count];
            
        }
    }
    
    return [NSString stringWithFormat:@"%d",count];
}
-(NSString *)titleForSection:(NSInteger)section inArray:(NSString *)filterFlag
{
    if ([filterFlag isEqualToString:ALLINBOX])
        return [sharedInboxLogsData.sectionAllInboxLogs objectAtIndex:section];
    else if ([filterFlag isEqualToString:MISSEDINBOX])
        return [sharedInboxLogsData.sectionMissedInboxLogs objectAtIndex:section];
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
        return [sharedInboxLogsData.sectionVoicemailInboxLogs objectAtIndex:section];
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
        return [sharedInboxLogsData.sectionFeddbackInboxLogs objectAtIndex:section];
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
        return [sharedInboxLogsData.sectionTagInboxLogs objectAtIndex:section];
    else
        return @"";
}
-(void)removeAllObjectsFromArray:(NSString *)filterFlag
{
    if ([filterFlag isEqualToString:ALLINBOX])
    {
        [sharedInboxLogsData.sectionAllInboxLogs removeAllObjects];
        [sharedInboxLogsData.GroupedInboxLogsAll removeAllObjects];
    }
    else if ([filterFlag isEqualToString:MISSEDINBOX])
    {
        [sharedInboxLogsData.sectionMissedInboxLogs removeAllObjects];
        [sharedInboxLogsData.GroupedInboxLogsMissed removeAllObjects];
    }
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
    {
        [sharedInboxLogsData.sectionVoicemailInboxLogs removeAllObjects];
        [sharedInboxLogsData.GroupedInboxLogsVoicemail removeAllObjects];
    }
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
    {
        [sharedInboxLogsData.sectionFeddbackInboxLogs removeAllObjects];
        [sharedInboxLogsData.GroupedInboxLogsFeedback removeAllObjects];
    }
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
    {
        [sharedInboxLogsData.sectionTagInboxLogs removeAllObjects];
        [sharedInboxLogsData.GroupedInboxLogsTag removeAllObjects];
    }
}
-(void)addObjectsAtTop:(NSMutableArray *)array toArray:(NSString *)filterFlag
{
    

    if ([filterFlag isEqualToString:ALLINBOX])
    {
        array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
        
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsAll[dateToshow];
            
            if (!arrayWithSameDate) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedInboxLogsData.sectionAllInboxLogs insertObject:dateToshow atIndex:0];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsAll[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
//
                    // if not latest add to top because already reversed
               
                [arrayWithSameDate insertObject:dict atIndex:0];
                sharedInboxLogsData.GroupedInboxLogsAll[dateToshow] = arrayWithSameDate;
            }
        }
        
//        NSLog(@"object added top:: %@",sharedInboxLogsData.GroupedInboxLogsAll);
//        NSLog(@"sections:: %@",sharedInboxLogsData.sectionAllInboxLogs);
        
    }
    else if ([filterFlag isEqualToString:MISSEDINBOX])
    {
         array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
                
                NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

                for (int i = 0; i < array.count; i++) {
                   
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
                    
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";

                    [array replaceObjectAtIndex:i withObject:dict];

                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsMissed[dateToshow];
                    
                    if (!arrayWithSameDate) {
                        
                        // section is not exist so create section
                        
                        //we are adding object at the top of the array
                        
                        [sharedInboxLogsData.sectionMissedInboxLogs insertObject:dateToshow atIndex:0];
                        arrayWithSameDate = [NSMutableArray new];
                        [arrayWithSameDate addObject:dict];
                        sharedInboxLogsData.GroupedInboxLogsMissed[dateToshow] = arrayWithSameDate;
                        
                    }
                    else
                    {
                        // section exist
                        
                        //we are adding object at the top of array
        //
                            // if not latest add to top because already reversed
                       
                        [arrayWithSameDate insertObject:dict atIndex:0];
                        sharedInboxLogsData.GroupedInboxLogsMissed[dateToshow] = arrayWithSameDate;
                    }
                }
    }
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
    {
         array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
                
                NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

                for (int i = 0; i < array.count; i++) {
                   
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
                    
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";

                    [array replaceObjectAtIndex:i withObject:dict];

                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsVoicemail[dateToshow];
                    
                    if (!arrayWithSameDate) {
                        
                        // section is not exist so create section
                        
                        //we are adding object at the top of the array
                        
                        [sharedInboxLogsData.sectionVoicemailInboxLogs insertObject:dateToshow atIndex:0];
                        arrayWithSameDate = [NSMutableArray new];
                        [arrayWithSameDate addObject:dict];
                        sharedInboxLogsData.GroupedInboxLogsVoicemail[dateToshow] = arrayWithSameDate;
                        
                    }
                    else
                    {
                        // section exist
                        
                        //we are adding object at the top of array
        //
                            // if not latest add to top because already reversed
                       
                        [arrayWithSameDate insertObject:dict atIndex:0];
                        sharedInboxLogsData.GroupedInboxLogsVoicemail[dateToshow] = arrayWithSameDate;
                    }
                }
    }
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
    {
         array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
                
                NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

                for (int i = 0; i < array.count; i++) {
                   
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
                    
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";

                    [array replaceObjectAtIndex:i withObject:dict];

                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsFeedback[dateToshow];
                    
                    if (!arrayWithSameDate) {
                        
                        // section is not exist so create section
                        
                        //we are adding object at the top of the array
                        
                        [sharedInboxLogsData.sectionFeddbackInboxLogs insertObject:dateToshow atIndex:0];
                        arrayWithSameDate = [NSMutableArray new];
                        [arrayWithSameDate addObject:dict];
                        sharedInboxLogsData.GroupedInboxLogsFeedback[dateToshow] = arrayWithSameDate;
                        
                    }
                    else
                    {
                        // section exist
                        
                        //we are adding object at the top of array
        //
                            // if not latest add to top because already reversed
                       
                        [arrayWithSameDate insertObject:dict atIndex:0];
                        sharedInboxLogsData.GroupedInboxLogsFeedback[dateToshow] = arrayWithSameDate;
                    }
                }
    }
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
    {
         array = (NSMutableArray *) [[array reverseObjectEnumerator] allObjects];
                
                NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

                for (int i = 0; i < array.count; i++) {
                   
                    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
                    
                    dict[@"isPlay"] = @"0";
                    dict[@"isSubPlay"] = @"0";

                    [array replaceObjectAtIndex:i withObject:dict];

                    NSString *dateToshow = dict[@"dateToShow"];
                    arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsTag[dateToshow];
                    
                    if (!arrayWithSameDate) {
                        
                        // section is not exist so create section
                        
                        //we are adding object at the top of the array
                        
                        [sharedInboxLogsData.sectionTagInboxLogs insertObject:dateToshow atIndex:0];
                        arrayWithSameDate = [NSMutableArray new];
                        [arrayWithSameDate addObject:dict];
                        sharedInboxLogsData.GroupedInboxLogsTag[dateToshow] = arrayWithSameDate;
                        
                    }
                    else
                    {
                        // section exist
                        
                        //we are adding object at the top of array
        //
                            // if not latest add to top because already reversed
                       
                        [arrayWithSameDate insertObject:dict atIndex:0];
                        sharedInboxLogsData.GroupedInboxLogsTag[dateToshow] = arrayWithSameDate;
                    }
                }
    }

}
-(void)addObjectsAtBottom:(NSMutableArray *)array toArray:(NSString *)filterFlag
{
    
    if ([filterFlag isEqualToString:ALLINBOX])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsAll[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedInboxLogsData.sectionAllInboxLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsAll[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsAll[dateToshow] = arrayWithSameDate;
            }
        }
        
//        NSLog(@"object added bottom:: %@",sharedInboxLogsData.GroupedInboxLogsAll);
//        NSLog(@"sections:: %@",sharedInboxLogsData.sectionAllInboxLogs);
    }
    else if ([filterFlag isEqualToString:MISSEDINBOX])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsMissed[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedInboxLogsData.sectionMissedInboxLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsMissed[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsMissed[dateToshow] = arrayWithSameDate;
            }
        }
    }
    else if ([filterFlag isEqualToString:VOICEMAILINBOX])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsVoicemail[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedInboxLogsData.sectionVoicemailInboxLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsVoicemail[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsVoicemail[dateToshow] = arrayWithSameDate;
            }
        }
    }
    else if ([filterFlag isEqualToString:FEEDBACKINBOX])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsFeedback[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedInboxLogsData.sectionFeddbackInboxLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsFeedback[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsFeedback[dateToshow] = arrayWithSameDate;
            }
        }
    }
    else if ([filterFlag isEqualToString:TAGFILTERINBOX])
    {
        NSMutableArray *arrayWithSameDate = [[NSMutableArray alloc]init];

        for (int i = 0; i < array.count; i++) {
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:array[i]];
            
            dict[@"isPlay"] = @"0";
            dict[@"isSubPlay"] = @"0";

            [array replaceObjectAtIndex:i withObject:dict];

            NSString *dateToshow = dict[@"dateToShow"];
            arrayWithSameDate = sharedInboxLogsData.GroupedInboxLogsTag[dateToshow];
            
            if (arrayWithSameDate == nil) {
                
                // section is not exist so create section
                
                //we are adding object at the top of the array
                
                [sharedInboxLogsData.sectionTagInboxLogs addObject:dateToshow];
                arrayWithSameDate = [NSMutableArray new];
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsTag[dateToshow] = arrayWithSameDate;
                
            }
            else
            {
                // section exist
                
                //we are adding object at the top of array
                [arrayWithSameDate addObject:dict];
                sharedInboxLogsData.GroupedInboxLogsTag[dateToshow] = arrayWithSameDate;
            }
        }
    }
   
    
    
}

@end
