//
//  CalllogsVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "Tblcell.h"
#import "CallLogsModel.h"
#import <CallKit/CallKit.h>
#import "InboxLogsModel.h"


NS_ASSUME_NONNULL_BEGIN



@interface CalllogsVC : UIViewController<SWTableViewCellDelegate,CXProviderDelegate>
{
    NSMutableArray *arrContactList;
    NSMutableArray *arrcontact;
    NSMutableArray *Mixallcontact;
}
        
@property (weak, nonatomic) IBOutlet UITableView *tbl_logs;
@property (weak, nonatomic) IBOutlet UIButton *btn_dialpad_click;
@property (weak, nonatomic) IBOutlet UIView *view_play_recoding;
@property (weak, nonatomic) IBOutlet UIView *view_main;
@property (weak, nonatomic) IBOutlet UIView *view_boder;
@property (weak, nonatomic) IBOutlet UILabel *lbl_recoding_text;
@property (weak, nonatomic) IBOutlet UISlider *slider_view;
@property (weak, nonatomic) IBOutlet UIButton *btn_close;
@property (weak, nonatomic) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UIButton *btn_play;
@property (strong, nonatomic) IBOutlet UILabel *lblEmptyConversation;

@property (weak, nonatomic) IBOutlet UIButton *btn_allCalls;
@property (weak, nonatomic) IBOutlet UIButton *btn_inbox;
@property (weak, nonatomic) IBOutlet UITableView *tbl_inbox;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btn_inboxMenu;

@property (weak, nonatomic) IBOutlet UIView *view_mark;
@property (nonatomic, assign) SWCellState stagevc;


@end

NS_ASSUME_NONNULL_END

