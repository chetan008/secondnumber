//
//  CalllogsDetailsVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "UIViewController+LGSideMenuController.h"
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol contactUpdateFromLogDelegate <NSObject>

@optional
- (void)dismissFromCallLogDetail:(BOOL)isContactUpdated;

@end
@interface CalllogsDetailsVC : UIViewController
{
    NSMutableArray *arrContactList;
    NSMutableArray *arrcontact;
    NSMutableArray *Mixallcontact;
    
}
@property (nonatomic, weak)   id<contactUpdateFromLogDelegate> contactDelegate;

@property (strong, nonatomic) NSMutableDictionary *userData;
@property (strong,nonatomic) NSString *displayUrl;
@property (strong,nonatomic) NSString *transferCall;
@property (strong,nonatomic) NSString *fromInbox;


//
@property (weak, nonatomic) IBOutlet UIView *view_profile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_profile_height_constrain;
@property (strong, nonatomic) IBOutlet UIButton *img_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_number;

//
@property (weak, nonatomic) IBOutlet UIView *view_callstatus;
@property (weak, nonatomic) IBOutlet UIImageView *img_callstatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_callstatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;

//
@property (weak, nonatomic) IBOutlet UIView *view_callername;
@property (weak, nonatomic) IBOutlet UILabel *lbl_callername_title;
@property (weak, nonatomic) IBOutlet UIImageView *img_callername_image;
@property (weak, nonatomic) IBOutlet UILabel *lbl_callername_name;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_callername_top_contrain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_recording_top_contrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_scroll_top_contrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_switch_height_contrain;
//
@property (weak, nonatomic) IBOutlet UIView *view_record;
@property (weak, nonatomic) IBOutlet UIButton *btn_img_record;
@property (weak, nonatomic) IBOutlet UILabel *lbl_record_time;

//@IBOutlet weak var view_progressbar: AMProgressBar!

//
@property (weak, nonatomic) IBOutlet UIView *view_calldetails;
@property (weak, nonatomic) IBOutlet UIImageView *img_callflag;
@property (weak, nonatomic) IBOutlet UILabel *lbl_department;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_top_contrain;
@property (weak, nonatomic) IBOutlet UISlider *slider_view;

@property (weak, nonatomic) NSTimer *myTimer;
@property int currentTimeInSeconds;
@property (weak, nonatomic) IBOutlet UILabel *lbl_contact_title;

@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIButton *btnSMS;
@property (strong, nonatomic) IBOutlet UIButton *btnBlacklist;
@property (strong, nonatomic) IBOutlet UIButton *btnBlacklistNocredit;
@property (strong, nonatomic) IBOutlet UIButton *btnAddReminder;
@property (strong, nonatomic) IBOutlet UIButton *btnAddReminderNoCredit;


@property (weak, nonatomic) IBOutlet UIView *view_all_credit;
@property (weak, nonatomic) IBOutlet UIView *view_no_credit;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblRecordingType;
@property (strong, nonatomic) IBOutlet UIImageView *imgRecording;

@property (weak, nonatomic) IBOutlet UIView *view_acw_display;
@property (weak, nonatomic) IBOutlet UITextView *txt_acw;

@property (strong, nonatomic) IBOutlet UIButton *btnNote;
@property (strong, nonatomic) IBOutlet UIButton *btnCallNotes;
@property (strong, nonatomic) IBOutlet UIButton *btn_call_detail;
@property (strong, nonatomic) IBOutlet UIButton *btn_call_history;

@property (strong, nonatomic) IBOutlet UIScrollView *scroll_call_detail;

@property (strong, nonatomic) IBOutlet UITableView *tblHistoryView;
@property (strong, nonatomic) IBOutlet UILabel *lbl_call_detail_line;
@property (strong, nonatomic) IBOutlet UILabel *lbl_call_history_line;

@property (strong, nonatomic) IBOutlet UILabel *lbl_call;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sms;
@property (strong, nonatomic) IBOutlet UIView *view_switch;
@property (strong, nonatomic) IBOutlet UILabel *lblErrorMessageDisplay;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *call_view_height_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *call_view1_height_constraint;
@property (strong, nonatomic) IBOutlet UIButton *btnTranscriptedText;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnTrascriptedHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewRecordHeight;
@property (strong, nonatomic) IBOutlet UIButton *btnSMSNoCredit;
@property (strong, nonatomic) IBOutlet UIButton *btnCallNoCredit;

@property(nonatomic, retain) AVAudioPlayer *audio_player;

@property (strong, nonatomic) IBOutlet UITableView *tbl_callDetails;
@property (strong, nonatomic) IBOutlet UITableView *tbl_feedback;

@property (strong, nonatomic) IBOutlet UIView *view_switch_callhistory;
@property (strong, nonatomic) IBOutlet UIView *view_switch_feedback;
@property (strong, nonatomic) IBOutlet UIButton *btn_feedback;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feedback_line;

@property (strong, nonatomic) IBOutlet UILabel *lbl_noFeedback;

@property (strong, nonatomic) IBOutlet UICollectionView *tagCollectionView;

@property (strong, nonatomic) IBOutlet UITableView *tbl_reminderpopup;

@end

NS_ASSUME_NONNULL_END

