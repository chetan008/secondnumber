//
//  CallLogDetailViewCell.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CallLogDetailViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *status_lbl_date;
@property (strong, nonatomic) IBOutlet UIImageView *status_img_call;
@property (strong, nonatomic) IBOutlet UILabel *status_lbl_Call;
@property (strong, nonatomic) IBOutlet UILabel *status_lbl_time;
@property (strong, nonatomic) IBOutlet UIButton *status_btn_notes;
@property (strong, nonatomic) IBOutlet UIButton *status_btn_Call_notes;

//calller view
@property (strong, nonatomic) IBOutlet UILabel *callervw_lbl_callername_title;
@property (strong, nonatomic) IBOutlet UIImageView *callervw_img_callername;
@property (strong, nonatomic) IBOutlet UILabel *callervw_lbl_callername;

//record cell
@property (strong, nonatomic) IBOutlet UILabel *recordvw_lbl_recordtype;
@property (strong, nonatomic) IBOutlet UIImageView *recordvw_img_recordimg;
@property (strong, nonatomic) IBOutlet UIButton *recordvw_btn_imgrecord;
@property (strong, nonatomic) IBOutlet UISlider *recordvw_sliderview;
@property (strong, nonatomic) IBOutlet UILabel *recordvw_lbl_recordtime;
@property (strong, nonatomic) IBOutlet UIButton *recordvw_btn_transcriptedtxt;
@property (strong, nonatomic) IBOutlet UILabel *record_noaccessLbl;


//call details
@property (strong, nonatomic) IBOutlet UIImageView *calldetailvw_img_callflag;
@property (strong, nonatomic) IBOutlet UILabel *calldetailvw_lbl_department;

@property (strong, nonatomic) IBOutlet UICollectionView *calldetailvw_tagCollectionView;



@end

NS_ASSUME_NONNULL_END
