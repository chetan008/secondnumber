//
//  VoiceMailTextVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "VoiceMailTextVC.h"
#import "UtilsClass.h"
@interface VoiceMailTextVC ()

@end

@implementation VoiceMailTextVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [UtilsClass SetLabelBottomBorder:_lblTitle];
    _txtVoicemailTransCripted.text = _strVoiceMailText;
    [self updateConstraints];
    // Do any additional setup after loading the view.
}
- (IBAction)btn_dissmiss_click:(UIButton *)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (void) updateConstraints {
    CGSize contentSize = [_txtVoicemailTransCripted sizeThatFits:CGSizeMake(_txtVoicemailTransCripted.frame.size.width, FLT_MAX)];
    
    [_txtVoicemailTransCripted.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            constraint.constant = contentSize.height;
            *stop = YES;
        }
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
