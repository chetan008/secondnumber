//
//  CalllogsDetailsVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "CalllogsDetailsVC.h"
#import "AddEditContactVC.h"
#import "UtilsClass.h"
#import "OnCallVC.h"
#import "Constant.h"
#import "NewsmsVC.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <AVFoundation/AVFoundation.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "WebApiController.h"
#import "GlobalData.h"
#import "ChatViewControllerNew.h"
#import "Processcall.h"
#import "Tblcell.h"
#import "VoiceMailTextVC.h"
#import "AppDelegate.h"
#import "CallLogDetailViewCell.h"
#import "tagCollectionCell.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define DOC_FILE(s) [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:s]

@import Firebase;

@interface CalllogsDetailsVC ()<UITableViewDataSource,UITableViewDelegate,SecVSDelegate,contactupdate,UITextViewDelegate,AVAudioPlayerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSString *btn_click;
    NSString *audioString;
    
    NSString *strFromNumber;
    NSString *strFlag;
    NSString *strCountryCode;
    
    NSString *smsFromNumber;
    NSString *smstoNumber;
    
    NSMutableArray *responseArray;
    NSMutableDictionary *respArray;
    NSMutableArray *tblArray;
    
    NSString *acwflag;
    
    NSMutableArray *CallHistoryArr;
    NSMutableArray *details_array;
    bool is_tbl_reload;
    bool is_title_added;
    WebApiController *obj;
    
    NSString *recordDuration;
    
    NSTimer *playerReadyTimer;
    
    BOOL iscontactUpdated;
    
    CallLogDetailViewCell *recordCell;
    
    NSArray *tagArray;
    
    CallLogDetailViewCell *tagListCell;

    
    BOOL hideCallerName;
    BOOL hideRecordView;
    BOOL hideCallDetail;

    
    NSArray *feedbackArray;
    BOOL isblacklisted;

    NSMutableArray *timeList;
    NSString *originalNum;
    
}
@end

@implementation CalllogsDetailsVC

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     //tagArray = [NSArray arrayWithObjects:@"Apple",@"Banana",@"Chikoo",@"Pomegranate",@"Custord Apple", nil];
     //tagArray = [NSArray arrayWithObjects:@"Apple",@"Banana", nil];
    
    timeList =  [NSMutableArray arrayWithObjects:@"15 Minutes",@"30 Minutes",@"1 Hour",@"2 Hours",@"tomorrow",@"in a week", nil];
    
    recordDuration = @"";
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    NSLog(@"%@",_userData);
    btn_click = @"0";
    acwflag = @"0";
    audioString = @"";
    Mixallcontact = [[NSMutableArray alloc] init];
    //    [self GetNumberFromContact];
    //    [self getCallHippoContactList];
    details_array = [[NSMutableArray alloc] init];
    [self contact_get];
    
    [UtilsClass view_navigation_title:self title:@"Call Details"color:UIColor.whiteColor];
    
    
    _img_callflag.image = [UIImage imageNamed:[[_userData valueForKey:@"shortName"] lowercaseString]];
    strFlag = [_userData valueForKey:@"shortName"];
    strCountryCode = [_userData valueForKey:@"countryCode"];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    [self viewUpdate];
    //    });
    
    //    double delayInSeconds = 0.1;
    //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    //        [self viewUpdate];
    //    });
    
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    is_tbl_reload = true;
    is_title_added = false;
    
    tblArray = [[NSMutableArray alloc] init];
    responseArray = [[NSMutableArray alloc] init];
    
    
    self.tblHistoryView.delegate = self;
    self.tblHistoryView.dataSource = self;
    
    
   // [tagListCell.calldetailvw_tagCollectionView registerClass:[tagCollectionCell self] forCellWithReuseIdentifier:@"tagCollectionCell"];
   

     
}
#pragma mark - table view delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView ==  _tbl_reminderpopup) {
        return timeList.count;
    }
   else if (tableView ==  _tbl_callDetails) {
        return 5;
    }
    else if(tableView == _tblHistoryView)
    {
        return details_array.count;
    }
    else
    {
        return feedbackArray.count;
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _tbl_reminderpopup)
    {
        Tblcell *cell = [_tbl_reminderpopup dequeueReusableCellWithIdentifier:@"Tblcell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblCredit.text = timeList[indexPath.row];
        return cell;
    }
    else if (tableView == _tbl_callDetails) {
        
        if (indexPath.row == 0) {
            
            CallLogDetailViewCell *cell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"CallStatusCell"];
            
            
            cell.status_lbl_Call.text = [_userData valueForKey:@"callStatus"];
            cell.status_lbl_time.text = [_userData valueForKey:@"callDuration"];
            
//            if([[_userData valueForKey:@"callStatus"]  isEqual: @"Missed"] || [[_userData valueForKey:@"callStatus"]isEqual:@"Call not setup"])
//            {
//                cell.status_lbl_time.hidden = true;
//
//            }
//            else
//            {
//                cell.status_lbl_time.hidden = false;
//
//            }
            
            if([_userData valueForKey:@"callNotes"])
            {
                if([[_userData valueForKey:@"callNotes"] isEqualToString:@""]){
                    cell.status_btn_notes.hidden = true;
                    cell.status_btn_Call_notes.hidden = true;
                }else{
                    cell.status_btn_notes.hidden = false;
                    cell.status_btn_Call_notes.hidden = false;
                }
            }
            else
            {
                _btnNote.hidden = true;
                _btnCallNotes.hidden = true;
            }
            
            [cell.status_btn_notes addTarget:self action:@selector(btn_note_click:) forControlEvents:UIControlEventTouchUpInside];
            [cell.status_btn_Call_notes addTarget:self action:@selector(btn_note_click:) forControlEvents:UIControlEventTouchUpInside];
           
            
            if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
            {
                if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
                    cell.status_img_call.image = [UIImage imageNamed:@"rejected"];
                    
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"incoming_Complete"];
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"missedincoming"];
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"incoming_Complete"];

                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Unavailable"]){
                    
                }else{
                    cell.status_img_call.image = [UIImage imageNamed:@"incoming"];
                }
            }
            else
            {
                if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
                    cell.status_img_call.image = [UIImage imageNamed:@"rejected"];
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"outgoingcomplete"];
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"missedout"];
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"outgoingcomplete"];

                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"transfer"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"outgoingcomplete"];
                }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Cancelled"]) {
                    cell.status_img_call.image = [UIImage imageNamed:@"missedout"];
                }else{
                    cell.status_img_call.image = [UIImage imageNamed:@"outgoingcomplete"];
                       }
            }
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
            [dateFormatter1 setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
            NSDate *expectedTime = [dateFormatter1 dateFromString:[_userData valueForKey:@"createdDate"]];
            
            dateFormatter1 = [[NSDateFormatter alloc] init] ;
            [dateFormatter1 setDateFormat:@"MMM d, yyyy h:mm a"];// here set format which you want...
            
            NSString *createdDate = [dateFormatter1 stringFromDate:expectedTime]; //here convert date in NSString
            
            cell.status_lbl_date.text = createdDate;
            
            
            return cell;
        }
        
        else if (indexPath.row == 1)
        {
            CallLogDetailViewCell *cell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"CallerNameCell"];
            
            // view hide show
            
          /*  if (![[_userData valueForKey:@"recordingUrl"]  isEqual: @""] && [_displayUrl isEqualToString:@"1"]) {
            
                if ([[_userData valueForKey:@"callStatus"]  isEqual: @"Completed"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
                {
                    if (![[_userData valueForKey:@"callerName"]  isEqual: @""] && [_userData valueForKey:@"callerName"]  != nil && ![[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
                
                        hideCallerName = false;
                    else
                        hideCallerName = true;
                    
                }
                else
                    hideCallerName = true;
            }
            else
            {*/
                if (![[_userData valueForKey:@"callerName"]  isEqual: @""] && [_userData valueForKey:@"callerName"]  != nil)
                        hideCallerName = false;
                           
                else
                      hideCallerName = true;

           // }
            
           /* if([[_userData valueForKey:@"callStatus"]  isEqual: @"Missed"] || [[_userData valueForKey:@"callStatus"]isEqual:@"Call not setup"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"IVR message"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Welcome message"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Unavailable"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
            {
                hideCallerName = true;

            }
            else
            {
                hideCallerName = false;

            }*/
            
            
            
            //fill the details
            
            if ([_userData valueForKey:@"callerName"] != nil)
               {
                   if([[_userData valueForKey:@"callerName"] isEqualToString: @""])
                   {
                       cell.callervw_lbl_callername.text = @"";
                       
                   }
                   else
                   {
                       cell.callervw_lbl_callername.text = [_userData valueForKey:@"callerName"];
                   }
               }
               else
               {
                   cell.callervw_lbl_callername.text = @"";
                   
               }
            return cell;

        }
        else if (indexPath.row == 2)
        {
            
            CallLogDetailViewCell *cell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"RecordCell"];
            

            // hide show record view
           // cell.recordvw_lbl_recordtime.text = [_userData valueForKey:@"callDuration"];//"\(userData?.callDuration! ?? "")"

        hideRecordView = true;
            cell.record_noaccessLbl.hidden = true;
            cell.recordvw_lbl_recordtype.hidden = false;

//            [Default setBool:true forKey:kListenTocallRecordRights];
            
            if([[_userData valueForKey:@"callStatus"]  isEqual: @"Missed"] || [[_userData valueForKey:@"callStatus"]isEqual:@"Call not setup"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"IVR message"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Welcome message"])
            {
                hideRecordView = true;

            }
            
            
            
        if (![[_userData valueForKey:@"recordingUrl"]  isEqual: @""] && [_displayUrl isEqualToString:@"1"]) {
                       
                    if ([[_userData valueForKey:@"callStatus"]  isEqual: @"Completed"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
                    {
                           // if (![[_userData valueForKey:@"callerName"]  isEqual: @""] && [_userData valueForKey:@"callerName"]  != nil && ![[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
                        
                    
                        if([Default boolForKey:kListenTocallRecordRights] ==false)
                        {
                            hideRecordView = false;
                            cell.record_noaccessLbl.hidden = false;
                            cell.recordvw_lbl_recordtype.hidden = true;
                            cell.recordvw_img_recordimg.hidden = true;


                        }
                        else
                        {
                            hideRecordView = false;
                            cell.record_noaccessLbl.hidden = true;
                            cell.recordvw_lbl_recordtype.hidden = false;
                            cell.recordvw_img_recordimg.hidden = false;
                        }
                            
//                            else
//                                   hideRecordView = true;
                               
                    }
                    else
                               hideRecordView = true;
            }
        else
        {
            hideRecordView = true;

        }
            
            //fill the details
            
            cell.recordvw_lbl_recordtype.text = @"Call Recording";
            cell.recordvw_img_recordimg.image = [UIImage imageNamed:@"callRecording"];
            //cell.recordvw_lbl_recordtime.text = @"00:00";
            
            if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
            cell.recordvw_img_recordimg.image = [UIImage imageNamed:@"voiceMail"];
            cell.recordvw_lbl_recordtype.text = @"Voicemail Recording";
            }
            
            if (([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"] || [[_userData valueForKey:@"callStatus"] isEqualToString:@"Voicemail"] )&& _userData[@"transcriptionText"] != (id)[NSNull null] && _userData[@"transcriptionText"] != nil && ![[_userData valueForKey:@"transcriptionText"] isEqualToString:@""] ) {
                
              cell.recordvw_btn_transcriptedtxt.hidden = false;
                
            }else{
              cell.recordvw_btn_transcriptedtxt.hidden = true;
             // _viewRecordHeight.constant = 70;
            }
            
            cell.recordvw_btn_transcriptedtxt.layer.cornerRadius = 15.0;

            recordCell = cell;
            return cell;

        }
        else if(indexPath.row == 3)
        {
            CallLogDetailViewCell *cell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"CallDetailsCell"];
         
            
            //hide show view
            
            NSString *extensionCall = [_userData valueForKey:@"extensionCall"];
               int exten = [extensionCall intValue];
               if(exten == 1 ){
                   hideCallDetail = true;
               }else{
                   hideCallDetail = false;
               }
            

            //fill the details
            
            cell.calldetailvw_img_callflag.image = [UIImage imageNamed:[[_userData valueForKey:@"shortName"] lowercaseString]];
            strFlag = [_userData valueForKey:@"shortName"];
            strCountryCode = [_userData valueForKey:@"countryCode"];
            
            if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
            {
                if ([_transferCall isEqualToString:@"yes"]) {
                               
                    if ([[_userData valueForKey:@"fromNameDepart"]  isEqual: @""]) {
                        cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromDepart"]];
                    } else {
                        cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromNameDepart"]];
                            }
                }else{
                               
                        if ([[_userData valueForKey:@"toName"]  isEqual: @""])
                        {
                            cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Incoming call via %@",[_userData valueForKey:@"to"]];
                        } else
                        {
                            cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Incoming call via %@",[_userData valueForKey:@"toName"]];
                        }
                               
                }
            }
            else
            {
                if ([_transferCall isEqualToString:@"yes"]) {
                    
                    if ([[_userData valueForKey:@"fromNameDepart"]  isEqual: @""]) {
                        cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromDepart"]];//"\(userData?.from! ?? "")"
                    } else {
                        cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromNameDepart"]];
                    }
                }else{
                    
                    if ([[_userData valueForKey:@"fromName"]  isEqual: @""]) {
                        cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"from"]];//"\(userData?.from! ?? "")"
                    } else {
                        cell.calldetailvw_lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromName"]];
                       
                    }
                }
            }
           
            
            return cell;

        }
        else
        {
            //CallLogDetailViewCell *cell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"TagListCell"];
            
            
           // [tagListCell.calldetailvw_tagCollectionView registerClass:[tagCollectionCell class] forCellWithReuseIdentifier:@"tagCollectionCell"];
            tagListCell.calldetailvw_tagCollectionView.dataSource = self;
            tagListCell.calldetailvw_tagCollectionView.delegate = self;

            tagListCell.calldetailvw_tagCollectionView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            tagListCell.calldetailvw_tagCollectionView.layer.borderWidth = 1.0;
            tagListCell.calldetailvw_tagCollectionView.clipsToBounds = true;
            
            tagListCell.calldetailvw_tagCollectionView.layer.cornerRadius = 5.0;
            

            //tagListCell = cell;
            [tagListCell.calldetailvw_tagCollectionView reloadData];
            
            return tagListCell;
        }
         
    }
    else if(tableView == _tblHistoryView)
    {
        NSDictionary *dic = [details_array objectAtIndex:indexPath.row];
        //
        Tblcell *cell1 = [_tblHistoryView dequeueReusableCellWithIdentifier:@"cell321"];
        //             cell1.con_det_img_call_status.image = [UIImage imageNamed:@"calldetails_company"];
        cell1.con_det_lbl_call_date.text = [NSString stringWithFormat:@"%@ %@",[dic valueForKey:@"date"],[dic valueForKey:@"time"]];
        cell1.con_det_lbl_call_time.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"callDuration"]];
        
        cell1.con_det_lbl_call_status.text = [NSString stringWithFormat:@"via %@ | %@",[dic valueForKey:@"DepartmentName"] , [dic valueForKey:@"callStatus"]];
        
        cell1.con_det_lbl_call_username.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"callerName"]];
        
        
        
        if ([[dic valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
            
            if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"rejected"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"incoming_Complete"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"missedincoming"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"incoming_Complete"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Unavailable"]) {
                
            }else{
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"incoming"];
            }
        }
        else
        {
            if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"rejected"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"missedout"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"transfer"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
            }else if ([[dic valueForKey:@"callStatus"] isEqualToString:@"Cancelled"]) {
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"missedout"];
            }else{
                cell1.con_det_img_call_status.image = [UIImage imageNamed:@"outgoingcomplete"];
            }
            
            
        }
        
        _tblHistoryView.rowHeight = 64.0;
        return cell1;
    }
    else
    {
         Tblcell *cell1 = [_tbl_feedback dequeueReusableCellWithIdentifier:@"cell321"];
        
        cell1.lbl_feedbackTitle.text = [[feedbackArray objectAtIndex:indexPath.row] valueForKey:@"userName"];
        cell1.lbl_feedbackDesc.text = [[feedbackArray objectAtIndex:indexPath.row] valueForKey:@"feedback"];
        
        
        return cell1;
    }
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_callDetails) {
           
        
        if (indexPath.row == 0 )
            return 70;
        else if (indexPath.row == 1 )
        {
            if (hideCallerName == false)
            return 70;
            else
                return 0;
        }
        else if (indexPath.row == 2 )
        {
            if (hideRecordView == false)
            {
                //CallLogDetailViewCell *cell = [_tbl_callDetails cellForRowAtIndexPath:indexPath];
                
               // if (recordCell.recordvw_btn_transcriptedtxt.hidden == true)
                
                
                if (([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"] || [[_userData valueForKey:@"callStatus"] isEqualToString:@"Voicemail"] )&& _userData[@"transcriptionText"] != (id)[NSNull null] && _userData[@"transcriptionText"] != nil && ![[_userData valueForKey:@"transcriptionText"] isEqualToString:@""] )
                {
                    if([Default boolForKey:kListenTocallRecordRights] ==false)
                        return 30;
                    else
                        return 100;
                }
                    
                else
                {
                    if([Default boolForKey:kListenTocallRecordRights] ==false)
                        return 30;
                    else
                        return 70;
                }
                
            }
            else
            {
                    return 0;
            }
                
        }
        else if (indexPath.row == 3 )
        {
            if (hideCallDetail == false)
                return 50;
            else
                return 0;
        }
        else if(indexPath.row == 4)
        {
             if(tagArray.count > 0)
             {
                 int s = (int)((tagArray.count) + 1) / 2;
                 NSLog(@"s %d", s);
//            //1. for cell height 2. for spacing b/w cell 3. bottom space
                 long p = (s * 40) + (s * 20) + tagListCell.calldetailvw_tagCollectionView.frame.origin.y;
           
                return p ;
             }
            else
                return 0;
        
        }
        else
            return 0;
        
       }
    else if (tableView == _tbl_reminderpopup)
    {
        return 40;
    }
    else
    {
        return 64;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    btn_click = @"0";
//    [self.myTimer invalidate];
    
   
    //  [_audio_player stop];
    
    //[self stopProgressMonitor];
    
    if (tableView == _tbl_reminderpopup) {
        
            if(indexPath.row == 0){
                [self setReminder:@"15"];
            }else if(indexPath.row == 1){
                [self setReminder:@"30"];
            }else if(indexPath.row == 2){
                [self setReminder:@"1"];
            }else if(indexPath.row == 3){
                [self setReminder:@"2"];
            }else if(indexPath.row == 4){
                [self setReminder:@"24"];
            }else if(indexPath.row == 5){
                [self setReminder:@"7"];
            }
        
        
        
        
    }
    
    else if (tableView == _tbl_callDetails) {
        
    }
    else if(tableView == _tblHistoryView)
    {
        [_userData setValue:[[details_array valueForKey:@"callDuration"] objectAtIndex:indexPath.row] forKey:@"callDuration"];
           
            [_userData setValue:[[details_array valueForKey:@"callNotes"] objectAtIndex:indexPath.row] forKey:@"callNotes"];
           
            [_userData setValue:[[details_array valueForKey:@"callStatus"] objectAtIndex:indexPath.row] forKey:@"callStatus"];
           
            [_userData setValue:[[details_array valueForKey:@"callType"] objectAtIndex:indexPath.row] forKey:@"callType"];
           
           
            [_userData setValue:[[details_array valueForKey:@"date"] objectAtIndex:indexPath.row] forKey:@"date"];
           
          
            [_userData setValue:[[details_array valueForKey:@"recordingUrl"] objectAtIndex:indexPath.row] forKey:@"recordingUrl"];
           

            [_userData setValue:[[details_array valueForKey:@"time"] objectAtIndex:indexPath.row] forKey:@"time"];
           
           [_userData setValue:[[details_array valueForKey:@"createdDate"] objectAtIndex:indexPath.row] forKey:@"createdDate"];
           
           [_userData setValue:[[details_array valueForKey:@"to"] objectAtIndex:indexPath.row] forKey:@"to"];
           
           [_userData setValue:[[details_array valueForKey:@"toName"] objectAtIndex:indexPath.row] forKey:@"toName"];
           
           [_userData setValue:[[details_array valueForKey:@"from"] objectAtIndex:indexPath.row] forKey:@"from"];
           
           [_userData setValue:[[details_array valueForKey:@"fromName"] objectAtIndex:indexPath.row] forKey:@"fromName"];
           
           [_userData setValue:[[details_array valueForKey:@"callerName"] objectAtIndex:indexPath.row] forKey:@"callerName"];
           
           [_userData setValue:[[details_array valueForKey:@"transcriptionText"] objectAtIndex:indexPath.row] forKey:@"transcriptionText"];
           
           _displayUrl = @"1";
           
          if([[[details_array valueForKey:@"callStatus"] objectAtIndex:indexPath.row] isEqualToString:@"Voicemail"]){
            [_userData setValue:@"Voice Mail" forKey:@"callStatus"];
          }
          
           
          [self viewUpdate];

    }
    
}

#pragma mark - collection view delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return tagArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    tagCollectionCell *cell = (tagCollectionCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"tagCollectionCell" forIndexPath:indexPath];
    
    cell.calldetailvw_tagTitle.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.calldetailvw_tagTitle.layer.borderWidth = 1.0;
    cell.calldetailvw_tagTitle.clipsToBounds = true;
    
    cell.calldetailvw_tagTitle.layer.cornerRadius = 15.0;
    
    
    [cell.calldetailvw_tagTitle setTitle:[tagArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    
    return cell;
}
//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(200, 40);
//}
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    
    
    float  height = self.tblHistoryView.frame.size.height;
    float contentYoffset = aScrollView.contentOffset.y;
    float distanceFromBottom = aScrollView.contentSize.height - contentYoffset;
    //    //NSLog(@" distanceFromBottom : %f",distanceFromBottom);
    //    //NSLog(@" height : %f",height);
    
    float halfpoint = aScrollView.contentSize.height / 2;
    if (halfpoint < contentYoffset ) {
        //NSLog(@" you reached end of the table");
        [self scroll_api];
    }else{
        
    }
}

-(void)scroll_api
{
    if(is_tbl_reload == true)
    {
        //NSLog(@"Api call Counter %@",[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]]);
        is_tbl_reload = false;
        //        [self GetCallLogs:[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]]];
        
        
        if ([_userData valueForKey:@"from"] == nil || [_userData valueForKey:@"from"] == (id)[NSNull null] || [_userData valueForKey:@"to"] == nil || [_userData valueForKey:@"to"] == (id)[NSNull null] ||[_userData valueForKey:@"callSid"] == nil || [_userData valueForKey:@"callSid"] == (id)[NSNull null])
        {
            
        }
        else
        {
            NSString *fromNumber = [_userData valueForKey:@"from"];
            fromNumber = [fromNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            NSString *toNumber = [_userData valueForKey:@"to"];
            toNumber = [toNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            NSString *callsid = [_userData valueForKey:@"callSid"];
            [self call_history:[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]] callSID:callsid fromNumber:fromNumber toNumber:toNumber];
        }
    }
    else
    {
        
    }
}

-(void)Foreground_Action:(NSNotification *)notification
{
    [self viewUpdate];
    [self contact_get];
}
-(void)contact_get
{
  //old  Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(void)viewUpdate {
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]init];
    tapGesture.numberOfTapsRequired = 1;
    [_btnAddReminder addGestureRecognizer:tapGesture];
   
    [tapGesture addTarget:self action:@selector(reminderBtnClick:)];
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc]init];
    tapGesture1.numberOfTapsRequired = 1;
    [_btnAddReminderNoCredit addGestureRecognizer:tapGesture1];
    [tapGesture1 addTarget:self action:@selector(reminderBtnClick:)];
    
    recordCell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"RecordCell"];
    
    tagListCell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"TagListCell"];

    
    //recordCell.recordvw_lbl_recordtime.text = [_userData valueForKey:@"callDuration"];//"\(userData?.callDuration! ?? "")"

    
   //recordCell = [_tbl_callDetails cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];

    tagArray = [[_userData valueForKey:@"callTags"] valueForKey:@"tagName"];
    
    if ([_fromInbox isEqualToString:@"1"]) {
        _view_switch_callhistory.hidden = true;
        _view_switch_feedback.hidden = false;
        
        feedbackArray = [_userData valueForKey:@"feedbacks"];
        [_tbl_feedback reloadData];
    }
    else
    {
        _view_switch_callhistory.hidden = false;
        _view_switch_feedback.hidden = true;
    }
    
    [_tbl_callDetails reloadData];

    NSString *extensionCall = [_userData valueForKey:@"extensionCall"];
    int exten = [extensionCall intValue];
    if(exten == 1 ){
        _view_calldetails.hidden = true;
    }else{
        _view_calldetails.hidden = false;
    }
   
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self->_viewRecordHeight.constant = 100;
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if (([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"] || [[_userData valueForKey:@"callStatus"] isEqualToString:@"Voicemail"] )&& _userData[@"transcriptionText"] != (id)[NSNull null] && _userData[@"transcriptionText"] != nil && ![[_userData valueForKey:@"transcriptionText"] isEqualToString:@""] ) {
      _btnTranscriptedText.hidden = false;
        
    }else{
      _btnTranscriptedText.hidden = true;
      _viewRecordHeight.constant = 70;
    }
     });
    
    
    [UtilsClass button_shadow_boder:_btnEdit];
    [UtilsClass button_shadow_boder:_btnCall];
    [UtilsClass button_shadow_boder:_btnSMS];
    
    [UtilsClass button_shadow_boder:_btnSMSNoCredit];
    [UtilsClass button_shadow_boder:_btnCallNoCredit];
    
    [UtilsClass button_shadow_boder:_btnAddReminderNoCredit];
    [UtilsClass button_shadow_boder:_btnAddReminder];
    [UtilsClass button_shadow_boder:_btnBlacklist];
    [UtilsClass button_shadow_boder:_btnBlacklistNocredit];
    
    [UtilsClass button_shadow_boder:_img_user];
    
    
    
     _btnTranscriptedText.layer.cornerRadius = 15.0;
    
   
    _imgRecording.image = [UIImage imageNamed:@"callRecording"];
    _lblRecordingType.text = @"Call Recording";
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    [dateFormatter1 setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    NSDate *expectedTime = [dateFormatter1 dateFromString:[_userData valueForKey:@"createdDate"]];
    
    dateFormatter1 = [[NSDateFormatter alloc] init] ;
    [dateFormatter1 setDateFormat:@"MMM d, yyyy h:mm a"];// here set format which you want...
    
    NSString *createdDate = [dateFormatter1 stringFromDate:expectedTime]; //here convert date in NSString
    
    _lblDate.text = createdDate;
    _txt_acw.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _txt_acw.layer.borderWidth = 1.0;
    _txt_acw.layer.cornerRadius = 5.0;
    _txt_acw.delegate = self;
    //NSLog(@"callNotes Check %@",[_userData valueForKey:@"callNotes"]);
    if([_userData valueForKey:@"callNotes"])
    {
        if([[_userData valueForKey:@"callNotes"] isEqualToString:@""]){
            _btnNote.hidden = true;
            _btnCallNotes.hidden = true;
        }else{
            _btnNote.hidden = false;
            _btnCallNotes.hidden = false;
        }
    }
    else
    {
        _btnNote.hidden = true;
        _btnCallNotes.hidden = true;
    }
    
    
    if ([_userData valueForKey:@"contactId"] != nil)
    {
        if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
        {
            _lbl_contact_title.text = @"ADD";
            UIImage *btnImage = [UIImage imageNamed:@"addcontactplus"];
            [_btnEdit setImage:btnImage forState:UIControlStateNormal];
        }
        else
        {
            _lbl_contact_title.text = @"EDIT";
            UIImage *btnImage = [UIImage imageNamed:@"contactedit"];
            [_btnEdit setImage:btnImage forState:UIControlStateNormal];
        }
    }
    else
    {
        _lbl_contact_title.text = @"ADD";
        UIImage *btnImage = [UIImage imageNamed:@"addcontactplus"];
        [_btnEdit setImage:btnImage forState:UIControlStateNormal];
        
    }
    
    if ([_userData valueForKey:@"callerName"] != nil)
    {
        if([[_userData valueForKey:@"callerName"] isEqualToString: @""])
        {
            _lbl_callername_name.text = @"";
            
        }
        else
        {
            _lbl_callername_name.text = [_userData valueForKey:@"callerName"];
        }
    }
    else
    {
        _lbl_callername_name.text = @"";
        
    }
    

    if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
    {
        
        smsFromNumber = [_userData valueForKey:@"to"];
        smstoNumber = [_userData valueForKey:@"from"];
        if ([[_userData valueForKey:@"fromName"]  isEqual: @""])
        {
            _lbl_name.text = [_userData valueForKey:@"from"];// "\(userData?.from! ?? "")";
            if([Default boolForKey:kIsNumberMask] == true)
            {
                _lbl_name.text = [UtilsClass get_masked_number:[_userData valueForKey:@"from"]];
            }
        }
        else
        {
            _lbl_name.text = [_userData valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
        }
        
        strFromNumber = [_userData valueForKey:@"to"];
        //        [Default setValue:[_userData valueForKey:@"to"] forKey:SELECTEDNO];
        
        if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
        {
            _lbl_number.hidden = true;
            _lbl_number.text = [_userData valueForKey:@"from"];
            
        }else {
            _lbl_number.hidden = false;
            _lbl_number.text = [_userData valueForKey:@"from"];//"\(userData?.from! ?? "")";
        }
        
        if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
            _img_callstatus.image = [UIImage imageNamed:@"rejected"];
            
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"incoming_Complete"];
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"missedincoming"];
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
            _img_callstatus.image = [UIImage imageNamed:@"incoming_Complete"];
            _imgRecording.image = [UIImage imageNamed:@"voiceMail"];
            //[_userData valueForKey:@"transcriptionText"]
            _lblRecordingType.text = @"Voicemail Recording";
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Unavailable"]){
            
        }else{
            _img_callstatus.image = [UIImage imageNamed:@"incoming"];
        }
        
        _lbl_callstatus.text = [_userData valueForKey:@"callStatus"];
        
        if ([_transferCall isEqualToString:@"yes"]) {
            _call_view_height_constraint.constant = 0;
            _call_view1_height_constraint.constant = 0;
            _view_profile_height_constrain.constant = _view_profile_height_constrain.constant - 80.0;
            if ([[_userData valueForKey:@"fromNameDepart"]  isEqual: @""]) {
                _lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromDepart"]];
            } else {
                _lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromNameDepart"]];
            }
        }else{
            
            if ([[_userData valueForKey:@"toName"]  isEqual: @""])
            {
                _lbl_department.text = [NSString stringWithFormat:@"Incoming call via %@",[_userData valueForKey:@"to"]];
            } else
            {
                _lbl_department.text = [NSString stringWithFormat:@"Incoming call via %@",[_userData valueForKey:@"toName"]];
            }
            
        }
        
    }
    else
    {
        smsFromNumber = [_userData valueForKey:@"from"];
        smstoNumber = [_userData valueForKey:@"to"];
        if ([[_userData valueForKey:@"toName"]  isEqual: @""])
        {
            _lbl_name.text =  [_userData valueForKey:@"to"];//"\(userData?.to! ?? "")"
            if([Default boolForKey:kIsNumberMask] == true)
            {
                _lbl_name.text = [UtilsClass get_masked_number:[_userData valueForKey:@"to"]];
            }
        }
        else
        {
            _lbl_name.text = [_userData valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
        }
        strFromNumber = [_userData valueForKey:@"from"];
        //    [Default setValue:[_userData valueForKey:@"from"] forKey:SELECTEDNO];
        if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
        {
            _lbl_number.text = [_userData valueForKey:@"to"];
            _lbl_number.hidden = true;
        }else {
            _lbl_number.text = [_userData valueForKey:@"to"];
            _lbl_number.hidden = false;
        }
        
        if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Rejected"]){
            _img_callstatus.image = [UIImage imageNamed:@"rejected"];
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Completed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Missed"]) {
            _img_callstatus.image = [UIImage imageNamed:@"missedout"];
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Voice Mail"]) {
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
            _imgRecording.image = [UIImage imageNamed:@"voiceMail"];
            _lblRecordingType.text = @"Voicemail Recording";
            //[_userData valueForKey:@"transcriptionText"]
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"transfer"]) {
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
        }else if ([[_userData valueForKey:@"callStatus"] isEqualToString:@"Cancelled"]) {
            _img_callstatus.image = [UIImage imageNamed:@"missedout"];
        }else{
            _img_callstatus.image = [UIImage imageNamed:@"outgoingcomplete"];
        }
        
        // "\(userData?.to! ?? "")"
        
        _lbl_callstatus.text = [_userData valueForKey:@"callStatus"];
        if ([_transferCall isEqualToString:@"yes"]) {
            _call_view_height_constraint.constant = 0;
            _call_view1_height_constraint.constant = 0;
            _view_profile_height_constrain.constant = _view_profile_height_constrain.constant - 80.0;
            if ([[_userData valueForKey:@"fromNameDepart"]  isEqual: @""]) {
                _lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromDepart"]];//"\(userData?.from! ?? "")"
            } else {
                _lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromNameDepart"]];
            }
        }else{
            
            if ([[_userData valueForKey:@"fromName"]  isEqual: @""]) {
                //             img_callflag.image = UIImage(named: "")
                _lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"from"]];//"\(userData?.from! ?? "")"
            } else {
                //             img_callflag.image = UIImage(named: "")
                _lbl_department.text = [NSString stringWithFormat:@"Outgoing call via %@",[_userData valueForKey:@"fromName"]];
                //            _lbl_department.text = [_userData valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")"
            }
        }
    }
    _lbl_time.text = [_userData valueForKey:@"callDuration"];//"\(userData?.time! ?? "")"
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    [self playerFunction];
    //    });
    
    
    
    
    NSString *credit = [_userData valueForKey:@"deviceLowCreditFlage"];
    float cre = [credit floatValue];
    if(cre == 1 && ![[_userData valueForKey:@"contactType"] isEqualToString:@"mobile"]){
        _view_all_credit.hidden = false;
        _view_no_credit.hidden = true;
        //        [_btnEdit setUserInteractionEnabled:false];
    }else{
        _view_all_credit.hidden = true;
        _view_no_credit.hidden = false;
        //        [_btnEdit setUserInteractionEnabled:true];
    }
    NSString *contactType = [_userData valueForKey:@"contactType"];
    
    NSString *transferflag = [_userData valueForKey:@"warmTransferFlag"];
    int transferflagint = [transferflag intValue];
    if(transferflagint == 1)
    {
        if ([contactType isEqualToString:@"SubUser"]){
            _view_all_credit.hidden = true;
            _view_no_credit.hidden = false;
            _view_switch.hidden = true;
            _view_switch_height_contrain.constant = 0;
            _btnSMSNoCredit.enabled = false;
            
        }else{
            [_userData setValue:[_userData valueForKey:@"warmTransferFrom"] forKey:@"from"];
        }
    }
    
    [_btn_call_history setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btn_call_detail setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_lbl_call_detail_line setBackgroundColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0]];
    [_lbl_call_history_line setBackgroundColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];
    [_btn_feedback setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_lbl_feedback_line setBackgroundColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];

    _scroll_call_detail.hidden = false;
    _tblHistoryView.hidden = true;
    _lblErrorMessageDisplay.hidden = false;
    _tbl_feedback.hidden = true;
    
    //
    _tbl_callDetails.hidden = false;

    
    if ([_transferCall isEqualToString:@"yes"]) {
        _view_all_credit.hidden = true;
        _view_no_credit.hidden = true;
        _btnSMS.hidden = true;
        _lbl_sms.hidden = true;
        _btnEdit.hidden = true;
        _lbl_contact_title.hidden = true;
        _view_switch.hidden = true;
        _view_switch_height_contrain.constant = 0;
    }else {
        if ([_userData valueForKey:@"from"] == nil || [_userData valueForKey:@"from"] == (id)[NSNull null] || [_userData valueForKey:@"to"] == nil || [_userData valueForKey:@"to"] == (id)[NSNull null] ||[_userData valueForKey:@"callSid"] == nil || [_userData valueForKey:@"callSid"] == (id)[NSNull null])
        {
            
        }
        else
        {
            NSString *fromNumber = [_userData valueForKey:@"from"];
            fromNumber = [fromNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            NSString *toNumber = [_userData valueForKey:@"to"];
            toNumber = [toNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            NSString *callsid = [_userData valueForKey:@"callSid"];
            [self call_history:[NSString stringWithFormat:@"%lu",(unsigned long)[details_array count]] callSID:callsid fromNumber:fromNumber toNumber:toNumber];
        }
    }
    
    
    
    NSString *numbVer = _userData[@"creditFlage"];
    int num = [numbVer intValue];
    if (num == 0)
    {
        _lbl_name.hidden = true;
        _lbl_number.hidden = false;
        _view_switch_height_contrain.constant = 0;
        _view_switch.hidden = true;
    }
    else
    {
        _lbl_name.hidden = false;
    }
    
    //find blacklisted number or not
    isblacklisted = [GlobalData isNumberBlacklisted:_lbl_number.text];
    if (isblacklisted) {
        
        [_btnBlacklist setImage:[UIImage imageNamed:@"icon_unblock"] forState:UIControlStateNormal];
        [_btnBlacklistNocredit setImage:[UIImage imageNamed:@"icon_unblock"] forState:UIControlStateNormal];
        
        _btnAddReminder.enabled = false;
        _btnAddReminderNoCredit.enabled = false;
    }
    else
    {
        [_btnBlacklist setImage:[UIImage imageNamed:@"icon_block"] forState:UIControlStateNormal];
        [_btnBlacklistNocredit setImage:[UIImage imageNamed:@"icon_block"] forState:UIControlStateNormal];
        
        _btnAddReminder.enabled = true;
        _btnAddReminderNoCredit.enabled = true;
    }
    
   
    
    if([[_userData valueForKey:@"from"] length] < 6 )
    {
        _btnSMS.enabled = false;
        _btnSMSNoCredit.enabled = false;
        
        _btnBlacklist.enabled = false;
        _btnBlacklistNocredit.enabled = false;
    }
    else
    {
        _btnSMS.enabled = true;
        _btnSMSNoCredit.enabled = true;
        
        _btnBlacklist.enabled = true;
        _btnBlacklistNocredit.enabled = true;
    }
    
    //pri
    NSString *credit1 = [_userData valueForKey:@"deviceLowCreditFlage"];
    float cre1 = [credit1 floatValue];
    if(cre1 == 1)
    {
        _btnBlacklist.enabled = true;
        _btnBlacklistNocredit.enabled = true;

        _btnAddReminder.enabled = true;
        _btnAddReminderNoCredit.enabled=true;
    }
    else
    {
        _btnBlacklist.enabled = false;
        _btnBlacklistNocredit.enabled = false;

        _btnAddReminder.enabled = false;
        _btnAddReminderNoCredit.enabled=false;
    }
    
     
    //
    
//    NSLog(@"call callername y : %f",_view_callername_top_contrain.constant);
//    NSLog(@"call recording y : %f",_view_recording_top_contrain.constant);
//
//    NSLog(@"call detail y : %f",_view_top_contrain.constant);
    
   

//    [_tblHistoryView reloadData];
    
   
    
//    [tagListCell.calldetailvw_tagCollectionView reloadData];

    
    originalNum = _lbl_number.text;
    
    NSString *masknum = _lbl_number.text;
    if ([Default boolForKey:kIsNumberMask] == true) {
        if (_lbl_number.text.length>6) {
            masknum = [UtilsClass get_masked_number:_lbl_number.text];
        }
        
    }
    _lbl_number.text = masknum;
    
    
   
    
    
}

-(void)playerFunction {
    
   
    
    _view_record.hidden = true;
    if (![[_userData valueForKey:@"recordingUrl"]  isEqual: @""] && [_displayUrl isEqualToString:@"1"]) {
        
        if ([[_userData valueForKey:@"callStatus"]  isEqual: @"Completed"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
        {
            
            _view_record.hidden = false;

            
            if (![[_userData valueForKey:@"callerName"]  isEqual: @""] && [_userData valueForKey:@"callerName"]  != nil && ![[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
               {
                //p
                   _view_callername.hidden = false;
                   
                   _view_record.frame = CGRectMake(_view_record.frame.origin.x,
                   _view_callername.frame.origin.y + _view_callername.frame.size.height + 10 ,
                                 _view_record.frame.size.width,
                                 _view_record.frame.size.height);
                   
               }
               else
               {
                 // p
                   _view_callername.hidden = true;
                  
                   _view_record.frame = CGRectMake(_view_record.frame.origin.x,
                                                                               _view_callstatus.frame.origin.y + _view_callstatus.frame.size.height  ,
                                                                               _view_record.frame.size.width,
                                                                               _view_record.frame.size.height);
                   
                   
                       //    _view_top_contrain.constant =  -_view_callstatus.frame.size.height;
                   
                   _view_recording_top_contrain.constant =  -_view_callername.frame.size.height;
                
               }
            
           
            
              //old  _lbl_record_time.text = [_userData valueForKey:@"callDuration"];//"\(userData?.callDuration! ?? "")"
            recordCell.recordvw_lbl_recordtime.text = [_userData valueForKey:@"callDuration"];//"\(userData?.callDuration! ?? "")"
            
           // _lbl_record_time.text = @"00:00";
            
            
            
            audioString = [_userData valueForKey:@"recordingUrl"];
            
            [self setupAudioPlayer];
            
            if (_userData[@"transcriptionText"] != (id)[NSNull null] && _userData[@"transcriptionText"] != nil && ![[_userData valueForKey:@"transcriptionText"] isEqualToString:@""] ){
                _viewRecordHeight.constant = 100;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                                            _view_record.frame.origin.y + _viewRecordHeight.constant + 10,
                                                                            _view_calldetails.frame.size.width,
                                                                            _view_calldetails.frame.size.height);
                           });
            }else{
                _viewRecordHeight.constant = 70;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                                       _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                                            _view_record.frame.origin.y + _view_record.frame.size.height + 10,
                                                                            _view_calldetails.frame.size.width,
                                                                            _view_calldetails.frame.size.height);
                           });
            }
            
           
            [_scroll_call_detail updateConstraints];
            
        }
        else
        {
            if (![[_userData valueForKey:@"callerName"]  isEqual: @""] && [_userData valueForKey:@"callerName"]  != nil)
            {
                _view_record.hidden = true;
                
                
                
                _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                     _view_callername.frame.origin.y + _view_callername.frame.size.height + 10,
                                                     _view_calldetails.frame.size.width,
                                                     _view_calldetails.frame.size.height);
                _view_top_contrain.constant =  -_view_callstatus.frame.size.height;
            }
            else
            {
                _view_record.hidden = true;
               
                
                _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                     _view_callstatus.frame.origin.y + _view_callstatus.frame.size.height + 10,
                                                     _view_calldetails.frame.size.width,
                                                     _view_calldetails.frame.size.height);
                _view_top_contrain.constant =  -_view_callstatus.frame.size.height;
            }
            
            
            
        }
    }else{
        
       if (![[_userData valueForKey:@"callerName"]  isEqual: @""] && [_userData valueForKey:@"callerName"]  != nil)
        {
            _view_record.hidden = true;
            _view_callername.hidden = false;
            _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                 _view_callername.frame.origin.y + _view_callername.frame.size.height + 10,
                                                 _view_calldetails.frame.size.width,
                                                 _view_calldetails.frame.size.height);
            _view_top_contrain.constant =  -_view_callstatus.frame.size.height;
            
        }
        else
        {
            _view_record.hidden = true;

            _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                 _view_callstatus.frame.origin.y + _view_callstatus.frame.size.height + 10,
                                                 _view_calldetails.frame.size.width,
                                                 _view_calldetails.frame.size.height);
            _view_top_contrain.constant =  -_view_callstatus.frame.size.height;
            
        }
    }
    
    if([[_userData valueForKey:@"callStatus"]  isEqual: @"Missed"] || [[_userData valueForKey:@"callStatus"]isEqual:@"Call not setup"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"IVR message"] || [[_userData valueForKey:@"callStatus"]  isEqual: @"Welcome message"])
    {
       // _view_callername.hidden =  true;
       // _view_record.hidden = true;
        
       
      /*   _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                                                       _view_callstatus.frame.origin.y + _view_callstatus.frame.size.height + 10,
                                                       _view_calldetails.frame.size.width,
                                                       _view_calldetails.frame.size.height);
                  
            
                  
                  _view_top_contrain.constant = -_view_callername.frame.size.height- _view_callstatus.frame.size.height;*/
        
        
        _view_callername.hidden = true;
        _view_record.hidden = true;
        _view_record.frame = CGRectMake(_view_record.frame.origin.x,
                                      _view_callstatus.frame.origin.y + _view_callstatus.frame.size.height ,
                                      _view_record.frame.size.width,
                                      _view_record.frame.size.height);
        _view_recording_top_contrain.constant = -_view_callername.frame.size.height;
        _view_calldetails.frame = CGRectMake(_view_calldetails.frame.origin.x,
                   83,
                   _view_calldetails.frame.size.width,
                   50);
        
        
    }
   
    
    recordCell = [_tbl_callDetails cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    //recordCell = [_tbl_callDetails dequeueReusableCellWithIdentifier:@"RecordCell"];

    
}


- (void)viewWillDisappear:(BOOL)animated
{
     [playerReadyTimer invalidate];
    playerReadyTimer = nil;
    [_audio_player stop];
}

- (IBAction)btn_back_click:(id)sender {
    _userData = nil;
    _userData = [[NSMutableDictionary alloc]init];
    //[self.navigationController popViewControllerAnimated:YES];
    
    [[self navigationController] popViewControllerAnimated:true];

    if(_contactDelegate && [_contactDelegate respondsToSelector:@selector(dismissFromCallLogDetail:)])
    {
        [_contactDelegate dismissFromCallLogDetail:iscontactUpdated];
    }
}

- (IBAction)btn_add_edit_click:(UIButton *)sender
{
     if ([Default boolForKey:kIsNumberMask] == false) {
         
    if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
    {
        _lbl_contact_title.text = @"ADD";
        AddEditContactVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddEditContactVC"];
        vc.delegate = self;
        vc.controller = @"addFromDialer";
        vc.number = _lbl_name.text;
        [[self navigationController] pushViewController:vc animated:YES];
    }
    else
    {
        _lbl_contact_title.text = @"EDIT";
        AddEditContactVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddEditContactVC"];
        //NSLog(@"User Details : %@",_userData);
        vc.delegate = self;
        //vc.number = _lbl_number.text;
        vc.number = originalNum;
        vc.name = _lbl_name.text;
        vc.company = [_userData valueForKey:@"contactCompany"];
        vc.email = [_userData valueForKey:@"contactEmail"];
        vc.controller = @"editfromcalllogs";
        vc.ContactDetailsDic = _userData;
        [[self navigationController] pushViewController:vc animated:YES];
    }
     }
    else
    {
        [UtilsClass makeToast:@"number can not be edited as number masking is on , please contact your admin ."];
    }
}
- (IBAction)btn_note_click:(UIButton *)sender
{
    
    NSString *numbVer = [Default valueForKey:ACW_PLAN];
    int num = [numbVer intValue];
    //NSLog(@"Callplanner -------------- %@",numbVer);
    if (num == 1) {
        if([acwflag isEqualToString:@"0"])
        {
            acwflag = @"1";
            _txt_acw.text = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"callNotes"]];
            _view_acw_display.hidden = false;
            _btnCallNotes.hidden = true;
            [self updateConstraints];
            
        }
        else
        {
            acwflag = @"0";
            _view_acw_display.hidden = true;
            _btnCallNotes.hidden = false;
        }
    }else{
        [UtilsClass showAlert:@"After call work is not available in your plan Please Upgrade your plan" contro:self];
    }
    
    
}

- (IBAction)btnCallNotes:(id)sender {
    _btnCallNotes.hidden = true;
    NSString *numbVer = [Default valueForKey:ACW_PLAN];
    int num = [numbVer intValue];
    //NSLog(@"Callplanner -------------- %@",numbVer);
    if (num == 1) {
        if([acwflag isEqualToString:@"0"])
        {
            acwflag = @"1";
            _txt_acw.text = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"callNotes"]];
            _view_acw_display.hidden = false;
            [self updateConstraints];
            
        }
        else
        {
            acwflag = @"0";
            _view_acw_display.hidden = true;
        }
    }else{
        [UtilsClass showAlert:@"After call work is not available in your plan Please Upgrade your plan" contro:self];
    }
}
- (IBAction)btnCallNotsBack:(id)sender {
    acwflag = @"0";
    _view_acw_display.hidden = true;
    _btnCallNotes.hidden = false;
}





- (IBAction)btn_smsclick:(id)sender {
    


    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
//        if(cre > 0){
        NSString *smsDisp = [Default valueForKey:kSMSRIGHTS];
        int num = [smsDisp intValue];
        if (num == 0) {
            [self getChat];
        }else{
            [UtilsClass showAlert:kSMSMODUALMSG contro:self];
        }
            
    }else{
        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
    }
}

- (IBAction)btn_call_click:(UIButton *)sender
{
    [self fireCallOnDidSelect];
}

-(void)fireCallOnDidSelect{
    
   //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *callfromnumber = @"";// = [Default valueForKey:SELECTEDNO];
    NSString *callToName= @"";
    NSString *callTonumber = @"";
    NSString *callDepartment = @"";
    NSString *contactType = [_userData valueForKey:@"contactType"];
    NSString *warmTransferFrom = [_userData valueForKey:@"warmTransferFrom"];
    NSString *warmTransferTo = [_userData valueForKey:@"warmTransferTo"];
    NSString *transferflag = [_userData valueForKey:@"warmTransferFlag"];
    
    if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
    {
        if ([[_userData valueForKey:@"fromName"]  isEqual: @""])
        {
            callToName = [_userData valueForKey:@"from"];// "\(userData?.from! ?? "")";
        }
        else
        {
            callToName = [_userData valueForKey:@"fromName"];//"\(userData?.fromName! ?? "")";
            
        }
        
        callfromnumber = [_userData valueForKey:@"to"];
        
        [Default setValue:callfromnumber forKey:SELECTEDNO];
        //        [Default setValue:callDepartment forKey:Selected_Department];
        callTonumber = [_userData valueForKey:@"from"];//"\(userData?.from! ?? "")";
        callDepartment = [NSString stringWithFormat:@"%@ ",[_userData valueForKey:@"toName"]];
        [Default setValue:callDepartment forKey:Selected_Department];
        //                if ([[_userData valueForKey:@"toName"]  isEqual: @""])
        //                {
        //
        //                    //             img_callflag.image = UIImage(named: "")
        //                    _lbl_department.text = [_userData valueForKey:@"to"];// "\(userData?.to! ?? "")"
        //
        //                } else
        //                {
        //                    //             img_callflag.image = UIImage(named: "")
        //                    _lbl_department.text = [_userData valueForKey:@"toName"];//"\(userData?.toName! ?? "")"
        //                }
    }
    else
    {
        
        if ([[_userData valueForKey:@"toName"]  isEqual: @""])
        {
            callToName =  [_userData valueForKey:@"to"];//"\(userData?.to! ?? "")"
        }
        else
        {
            callToName = [_userData valueForKey:@"toName"];//"\(userData?.toName! ?? "")";
        }
        callfromnumber = [_userData valueForKey:@"from"];
        
        [Default setValue:callfromnumber forKey:SELECTEDNO];
        //      [Default setValue:callDepartment forKey:Selected_Department];
        callTonumber = [_userData valueForKey:@"to"];// "\(userData?.to! ?? "")"
        callDepartment = [NSString stringWithFormat:@"%@ ",[_userData valueForKey:@"fromName"]];
        [Default setValue:callDepartment forKey:Selected_Department];
    }
    
    int num = [transferflag intValue];
    if(num != 1)
    {
        
        
        if(purchase_number.count > 0 || callTonumber.length == 4 || callTonumber.length == 5)
        {
            NSString *credit = [Default valueForKey:CREDIT];
            float cre = [credit floatValue];
            if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                
                [Default removeObjectForKey:@"extraHeader"];
                //NSLog(@"Contact details name : %@",_userData);
                NSString *Departmentcall = OUTGOING;
                if ([_userData valueForKey:@"contactId"] != nil)
                {
                    if([[_userData valueForKey:@"contactId"] isEqualToString: @""])
                    {
                        
                        Departmentcall = OUTGOING;
                    }
                    else
                    {
                        
                        if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
                        {
                            
                            Departmentcall = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"toName"]];
                        }
                        else
                        {
                            Departmentcall = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"fromName"]];
                        }
                    }
                }
                else
                {
                    Departmentcall = OUTGOING;
                }
                
                
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                
                NSLog(@"results results : %lu",(unsigned long)results.count);
                NSString *numbVer = @"";
                if (results.count == 0){
                    numbVer = @"1";
                }else{
                    NSDictionary *dic1 = [results objectAtIndex:0];
                    numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                }
                
                
                int num = [numbVer intValue];
                if (num == 1 || callTonumber.length == 4 || callTonumber.length == 5) {
                    NSString *credit = [Default valueForKey:CREDIT];
                    float cre = [credit floatValue];
                    if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                        
                        switch ([[AVAudioSession sharedInstance] recordPermission]) {
                            case AVAudioSessionRecordPermissionGranted:
                            {
                                //NSLog(@"ToNumber logs : %@",_lbl_number.text);
                                
                                if (results.count>0) {
                                    NSString *imagename = [[[[results objectAtIndex:0] objectForKey:@"number"]  objectForKey:@"shortName"] lowercaseString];
                                    [Default setValue:imagename forKey:Selected_Department_Flag];
                                }
                                
                                if ([Default boolForKey:kIsNumberMask] == true && [_lbl_name.text isEqualToString:_lbl_number.text]) {
                                    
                                     [UtilsClass make_outgoing_call_validate:self callfromnumber:callfromnumber ToName:originalNum ToNumber:originalNum calltype:callDepartment Mixallcontact:Mixallcontact];
                                }
                                else
                                {
                                     [UtilsClass make_outgoing_call_validate:self callfromnumber:callfromnumber ToName:_lbl_name.text ToNumber:originalNum calltype:callDepartment Mixallcontact:Mixallcontact];
                                }
                               
                                break;
                            }
                            case AVAudioSessionRecordPermissionDenied:
                            {
                                //                [UtilityClass makeToast:@"Please go to settings and turn on Microphone service for incoming/outgoing calls."];
                                [self micPermiiton];
                                break;
                            }
                            case AVAudioSessionRecordPermissionUndetermined:
                                // This is the initial state before a user has made any choice
                                // You can use this spot to request permission here if you want
                                break;
                            default:
                                break;
                                
                        }
                    }else {
                        [UtilsClass showAlert:kCREDITLAW contro:self];
                    }
                }else{
                    [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                }
            }else{
                [UtilsClass showAlert:kCREDITLAW contro:self];
            }}else{
                [UtilsClass showAlert:kNUMBERASSIGN contro:self];
            }}else{
                
                NSData *data = [Default valueForKey:PURCHASE_NUMBER];
                NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                if(purchase_number.count > 0 || [contactType isEqualToString:@"SubUser"])
                {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",callfromnumber];
                    NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
                    
                    NSLog(@"results results : %lu",(unsigned long)results.count);
                    NSString *numbVer = @"";
                    if (results.count == 0){
                        numbVer = @"1";
                    }else{
                        NSDictionary *dic1 = [results objectAtIndex:0];
                        numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
                    }
                    int num = [numbVer intValue];
                    if (num == 1) {
                        NSString *credit = [Default valueForKey:CREDIT];
                        float cre = [credit floatValue];
                        if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                            
                            if ([contactType isEqualToString:@"SubUser"]) {
                                [Default setValue:@"true" forKey:ExtentionCall];
                                [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                                [Default setValue:@"+1" forKey:klastDialCountryCode];
                                [Default setValue:@"United States" forKey:klastDialCountryName];
                                [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:contactType ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                            }else{
                                [Default setValue:@"false" forKey:ExtentionCall];
                                [Default setValue:@"" forKey:ExtentionCallProvider];
                                [Default setValue:warmTransferFrom forKey:SELECTEDNO];
                                [UtilsClass make_outgoing_call_warmtransfer:self callfromnumber:warmTransferFrom ToName:callToName ToNumber:warmTransferTo calltype:callDepartment Mixallcontact:Mixallcontact];
                            }}else {
                                [UtilsClass showAlert:kCREDITLAW contro:self];
                            }}else{
                                [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                            }}else{
                                [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                            }
            }
}

- (IBAction)btn_play_click:(UIButton*)sender {
    
    
    NSString *istransfer = [Default valueForKey:IS_RECORDING];
    int trans = [istransfer intValue];
    if([[_userData valueForKey:@"callStatus"]  isEqual: @"Voice Mail"])
    {
        if ([btn_click  isEqual: @"0"])
        {
            if ([recordDuration isEqualToString:@""]) {
                
                [UtilsClass makeToast:kRECORDINGWAITMSG];
                
                if(!playerReadyTimer)
                        playerReadyTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(isPlayerReady) userInfo:nil repeats:true];
                
            }
            else
            {
                [self audio_start];
                
                btn_click = @"1";
                UIImage *img = [UIImage imageNamed:@"AudioStop"];
                [sender setImage:img forState:UIControlStateNormal];
            }
            
        }
        else
        {
            [_audio_player pause];
            //            _slider_view.value=0.0;
            btn_click = @"0";
            UIImage *img = [UIImage imageNamed:@"AudioPlay"];
            [sender setImage:img forState:UIControlStateNormal];
            // _lbl_record_time.text = [_userData valueForKey:@"callDuration"];
        }
    }
    else
    {
        if (trans == 1) {
            if ([btn_click  isEqual: @"0"])
            {
                if ([recordDuration isEqualToString:@""]) {
                               
                [UtilsClass makeToast:kRECORDINGWAITMSG];
                    
                 if(!playerReadyTimer)
                        playerReadyTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(isPlayerReady) userInfo:nil repeats:true];
                               
            }
            else
            {
                [self audio_start];
                               //                [_audio_player pause];
                btn_click = @"1";
                UIImage *img = [UIImage imageNamed:@"AudioStop"];
                [sender setImage:img forState:UIControlStateNormal];
            }
               
            }
            else
            {
                [_audio_player pause];
                //                _slider_view.value=0.0;
                btn_click = @"0";
                UIImage *img = [UIImage imageNamed:@"AudioPlay"];
                [sender setImage:img forState:UIControlStateNormal];
            }
        }else {
            [UtilsClass showAlert:@"Call Recording is not Available in selected plan." title:@"Please Upgrade your plan." contro:self];
        }
    }
}

-(void)stop_timer
{
    [_myTimer invalidate];
}

- (void)secVCDidDismisWithData:(NSMutableDictionary *)data {
    
    //NSLog(@"datadara :%@",data);
    
    
    if ([data count] != 0) {
        [_userData setValue:[data valueForKey:@"_id"] forKey:@"contactId"];
        _lbl_number.hidden = false;
        _lbl_name.text = [data valueForKey:@"name"];
        
        if ([[data valueForKey:@"numberArray"] count]>0) {
            
            
            originalNum = [[[data valueForKey:@"numberArray"] objectAtIndex:0]valueForKey:@"number"];
            
            NSString *masknum = originalNum;
            
            if ([Default boolForKey:kIsNumberMask] == true) {
                masknum = [UtilsClass get_masked_number:originalNum];
            }
            
            _lbl_number.text = masknum;
           
        }
        _lbl_contact_title.text = @"EDIT";
        UIImage *btnImage = [UIImage imageNamed:@"contactedit"];
        [_btnEdit setImage:btnImage forState:UIControlStateNormal];
    
            iscontactUpdated = true;
     
    }
    
}

-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //button click event
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                //NSLog(@"Opened url");
            }
        }];
    }];
    
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}





- (IBAction)slider_value_change:(UISlider *)sender
{
    [self stopProgressMonitor];
}

- (IBAction)slider_touch_click:(UISlider *)sender
{
        
    _audio_player.currentTime =  sender.value ;
    
    //old _slider_view.value = _audio_player.currentTime;
    recordCell.recordvw_sliderview.value = _audio_player.currentTime;
    
    if(!_audio_player.playing)
    {
        [_audio_player play];
    }
    [self startProgressMonitor];
}


// custome code
-(void)setupAudioPlayer
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        NSURL *url = [NSURL URLWithString:self->audioString];
           NSData *data = [NSData dataWithContentsOfURL:url];
                   self->_audio_player = [[AVAudioPlayer alloc] initWithData:data error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
                   
            self->_audio_player.delegate = self;
            
          /* old  self->_slider_view.value = 0.0;
            self->_slider_view.minimumValue=0.0;
            self->_slider_view.maximumValue=self->_audio_player.duration;
*/
            self->recordCell.recordvw_sliderview.value = 0.0;
            self->recordCell.recordvw_sliderview.minimumValue=0.0;
            self->recordCell.recordvw_sliderview.maximumValue=self->_audio_player.duration;
            
           //old [self->_audio_player setCurrentTime:self->_slider_view.value];
            
            [self->_audio_player setCurrentTime:self->recordCell.recordvw_sliderview.value];
                   
                    int curent_time =  self->_audio_player.duration;
                   int seconds = curent_time % 60 ;
                   int minutes = (curent_time / 60) % 60;
                
            
            self->recordDuration = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
                   
           //old self->_lbl_record_time.text = self->recordDuration;
            self->recordCell.recordvw_lbl_recordtime.text = self->recordDuration;
           

            
               });
    });
    
       
    
    
}
-(void)audio_start
{
    
    //NSLog(@"Duration : %f",_audio_player.duration);
    
    
    
        [_audio_player play];
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateProgress) userInfo:nil repeats:true];
    
    
    
}

- (void)startProgressMonitor
{
    [self.myTimer invalidate];
    
    btn_click = @"1";
    UIImage *img = [UIImage imageNamed:@"AudioStop"];
    
    
   //old [_btn_img_record setImage:img forState:UIControlStateNormal];
    [recordCell.recordvw_btn_imgrecord setImage:img forState:UIControlStateNormal];
    
    self.myTimer = [NSTimer
                    scheduledTimerWithTimeInterval:1.0
                    target:self
                    selector:@selector(updateProgress)
                    userInfo:nil
                    repeats:YES];
}
- (void)isPlayerReady
{
    if (![recordDuration isEqualToString:@""])
    {
       
        
        btn_click = @"1";
        UIImage *img = [UIImage imageNamed:@"AudioStop"];
        
       //old [_btn_img_record setImage:img forState:UIControlStateNormal];
         [recordCell.recordvw_btn_imgrecord setImage:img forState:UIControlStateNormal];
        
        [_audio_player play];
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateProgress) userInfo:nil repeats:true];
    
    }
    
    
}
- (void)stopProgressMonitor
{
    [self.myTimer invalidate];
    btn_click = @"0";
    UIImage *img = [UIImage imageNamed:@"AudioPlay"];
  
    //old [_btn_img_record setImage:img forState:UIControlStateNormal];
     [recordCell.recordvw_btn_imgrecord setImage:img forState:UIControlStateNormal];
    
    //_lbl_record_time.text = @"00:00";
    //_lbl_record_time.text = [NSString stringWithFormat:@"%@",]_slider_view.maximumValue;
    
    //_lbl_record_time.text = recordDuration;
    int curent_time =   _audio_player.duration - _audio_player.currentTime;
    int seconds = curent_time % 60;
    int minutes = (curent_time / 60) % 60;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    //old _lbl_record_time.text = finalTime;
     recordCell.recordvw_lbl_recordtime.text = finalTime;

}

- (void)updateProgress
{
    [playerReadyTimer invalidate];
    playerReadyTimer = nil;
    
    // old _slider_view.value = _audio_player.currentTime;
    recordCell.recordvw_sliderview.value = _audio_player.currentTime;
    
    int curent_time = _audio_player.duration - _audio_player.currentTime  ;

    int seconds = curent_time % 60 ;
    int minutes = (curent_time / 60) % 60;
    int hours = curent_time / 3600;
    
    NSString *finalTime = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    
   //old _lbl_record_time.text = finalTime;
     recordCell.recordvw_lbl_recordtime.text = finalTime;

    
   // NSLog(@"final time:: %@",finalTime);
}

#pragma mark - Phone Contact
-(void)GetNumberFromContact
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        //NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
            // Contact one each function block is executed whenever you get
            NSString *phoneNumber = @"";
            if( contact.phoneNumbers)
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            
            //             //NSLog(@"phoneNumber = %@", phoneNumber);
            //             //NSLog(@"givenName = %@", contact.givenName);
            //             //NSLog(@"familyName = %@", contact.familyName);
            //             //NSLog(@"email = %@", contact.emailAddresses);
            
            
            NSString *FullName = [NSString stringWithFormat:@"%@%@",contact.givenName,contact.familyName];
            NSString *Number = [NSString stringWithFormat:@"%@",phoneNumber];
            if([Number isEqualToString:@"(null)"])
            {
                Number = @"";
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:FullName forKey:@"name"];
            [dic setObject:Number forKey:@"number"];
            [arrContactList addObject:contact];
            [Mixallcontact addObject:dic];
        }];
        
        //        //NSLog(@"arr contact : %lu",(unsigned long)arrContactList.count);
        
    }
}
#pragma mark - CallHippo Contact
-(void)getCallHippoContactList
{
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contact",userId];
    
    NSLog(@"URL Contact check : %@",aStrUrl);

    
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallHippoContactListresponse:response:) andDelegate:self];
}

- (void)getCallHippoContactListresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        //NSLog(@"getCallHippoContactListresponse : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            arrcontact = [response1 valueForKey:@"data"];
            [Mixallcontact addObjectsFromArray:arrcontact];
        } else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
}


-(void)getChat {
    
    if([UtilsClass isNetworkAvailable]) {
        
        NSString *url = [NSString stringWithFormat:@"getUserSmsDetails"];
        NSDictionary *passDict = @{@"user":@"",
                                   @"contact":@"",
                                   @"from":smsFromNumber,
                                   @"to":smstoNumber,
                                   @"chNumberId":[_userData valueForKey:@"chNumberId"]};
        [Default setValue:[_userData valueForKey:@"chNumberId"] forKey:NUMBERID];
        //NSLog(@"Params : %@",passDict);
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
            
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        
        //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
        
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
        
        
    } else {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : get chat : %@",response1);

        
        if ([[_userData valueForKey:@"callType"]  isEqual: @"Incoming"])
        {
            [_userData setValue:[_userData valueForKey:@"toName"] forKey:@"chNumberName"];
        }
        else
        {
            [_userData setValue:[_userData valueForKey:@"fromName"] forKey:@"chNumberName"];
        }
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            //NSLog(@"Response : %@",response1);
            respArray = response1[@"data"];
            
            //NSLog(@"respArray count %lu",(unsigned long)respArray.count);
            
            [tblArray addObject:respArray];
            
            
            ChatViewControllerNew *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
//            //NSLog(@"Response Array : %@",[[responseArray objectAtIndex:0] valueForKey:@"last_message"]);
//            controller.chat = [responseArray objectAtIndex:0];
//            controller.chat_dic = respArray;
            controller.ThreadMessageDic = response1[@"data"];
            controller.delegate = self;
            //NSLog(@"TBLArray Array : %@",respArray);
            controller.number = [respArray valueForKey:@"threadNumber"];
            controller.threadId = [respArray valueForKey:@"threadId"];
            controller.frmNumber = [[respArray valueForKey:@"chNumber"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            
            //NSLog(@"Calllogs_1 contactId---------%@",[_userData valueForKey:@"contactId"]);
            
            if([_userData valueForKey:@"contactId"])
            {
                //NSLog(@"contactId : not nill");
            }
            else
            {
                //NSLog(@"contactId : nill");
            }
            
            [_userData setValue:[respArray valueForKey:@"threadName"] forKey:@"threadName"];
            [_userData setValue:[respArray valueForKey:@"threadNumber"] forKey:@"threadNumber"];
            [_userData setValue:[respArray valueForKey:@"chNumber"] forKey:@"chNumber"];
            
            
            controller.contactId = [_userData valueForKey:@"contactId"];
            controller.VcFrom = @"CalllogsDetailsVC";
            controller.VcFrom1 = @"CalllogsDetailsVCChat";
            controller.tblContactArray = _userData;
            controller.titleString = [respArray valueForKey:@"threadName"];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else {
            
            
            [respArray setValue:_lbl_name.text forKey:@"threadName"];
            [respArray setValue:smstoNumber forKey:@"threadNumber"];
            [respArray setValue:smsFromNumber forKey:@"chNumber"];
            [respArray setValue:[_userData valueForKey:@"contactEmail"] forKey:@"contactEmail"];
            [respArray setValue:[_userData valueForKey:@"contactCompany"] forKey:@"contactCompany"];
            
            
            [_userData setValue:_lbl_name.text forKey:@"threadName"];
            [_userData setValue:smstoNumber forKey:@"threadNumber"];
            [_userData setValue:smsFromNumber forKey:@"chNumber"];
            
            
            
            
            
            
            ChatViewControllerNew *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
//            controller.chat = [responseArray objectAtIndex:0];
//            controller.chat_dic = respArray;
            controller.ThreadMessageDic = [[NSMutableDictionary alloc] init];
            controller.VcFrom1 = @"CalllogsDetailsVCChat";
            controller.delegate = self;
            //        controller.VcFrom1 = @"CalllogsDetailsVC";
            controller.number = smstoNumber;
            
            controller.frmNumber = [smsFromNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
            
            //NSLog(@"Calllogs_2 contactId---------%@",_userData);
            
            if([_userData valueForKey:@"contactId"])
            {
                //NSLog(@"contactId : not nill");
            }
            else
            {
                //NSLog(@"contactId : nill");
            }
            controller.contactId = [_userData valueForKey:@"contactId"];
            controller.VcFrom = @"CalllogsDetailsVC";
            //NSLog(@"*****UserData********%@",_userData);
            controller.tblContactArray = _userData;
            controller.titleString = _lbl_name.text;
            //        controller.contactId = @"calllogsDetails";
            [self.navigationController pushViewController:controller animated:YES];
            
            
            //        @try {
            //            if (response1 != (id)[NSNull null] && response1 != nil ){
            //                [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
            //            }
            //        }
            //        @catch (NSException *exception) {
            //        }
        }
        
    }
}

-(NSDate *)getdateString : (NSString*)strDate {
    NSArray *strArray = [strDate componentsSeparatedByString:@" "];
    //    //NSLog(@"one : %@",strArray[0]);
    //    //NSLog(@"two : %@",strArray[1]);
    //    //NSLog(@"three : %@",strArray[2]);
    //    //NSLog(@"four : %@",strArray[3]);
    //    //NSLog(@"five : %@",strArray[4]);
    NSString *finalStr = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",strArray[0],strArray[1],strArray[2],strArray[3],strArray[4]];
    //    //NSLog(@"finalStr : %@",finalStr);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
    //    [dateFormatter setFormatterBehavior:NSDateFormatterBehaviorDefault];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *date = [dateFormatter dateFromString:finalStr];
    //    //NSLog(@"Complet date : %@",date);
    
    
    return date;
}

- (void) updateConstraints {
    
    
    
    CGSize contentSize = [_txt_acw sizeThatFits:CGSizeMake(_txt_acw.frame.size.width, FLT_MAX)];
    
    [_txt_acw.constraints enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            constraint.constant = contentSize.height;
            *stop = YES;
        }
    }];
    
    
}


-(void)secDismisWithData:(NSMutableDictionary *)data{
    
    //NSLog(@"*******UpdateFromChatdata******** %@",data);
    //NSLog(@"**************@@@@@@@@@@@@ %@",data[@"name"]);
    if (data[@"name"] != (id)[NSNull null] && data[@"name"] != nil ){
        [_userData setValue:[data valueForKey:@"_id"] forKey:@"contactId"];
        _lbl_name.text = [data valueForKey:@"name"];
        
        originalNum = [data valueForKey:@"number"];
        NSString *masknum = originalNum;
        
        if ([Default boolForKey:kIsNumberMask] == true) {
            masknum = [UtilsClass get_masked_number:originalNum];
        }
        _lbl_number.text = masknum;
        
        //_lbl_number.text = [data valueForKey:@"number"];
    }
    
    
}

- (IBAction)btn_call_detail_click:(UIButton *)sender
{
    [self showCallDetails];
}

- (IBAction)btn_call_history_click:(UIButton *)sender
{

    
       btn_click = @"0";
       UIImage *img = [UIImage imageNamed:@"AudioPlay"];
    
      //old [_btn_img_record setImage:img forState:UIControlStateNormal];
     [recordCell.recordvw_btn_imgrecord setImage:img forState:UIControlStateNormal];

    [playerReadyTimer invalidate];
    playerReadyTimer = nil;
    [_audio_player stop];
    
    [self showCallHistory];
    
}

- (IBAction)btn_feedback_click:(UIButton *)sender
{
    btn_click = @"0";
       UIImage *img = [UIImage imageNamed:@"AudioPlay"];
    
      //old [_btn_img_record setImage:img forState:UIControlStateNormal];
     [recordCell.recordvw_btn_imgrecord setImage:img forState:UIControlStateNormal];

    [playerReadyTimer invalidate];
    playerReadyTimer = nil;
    [_audio_player stop];
    
    [self showFeedback];
}
-(void)showCallDetails
{
    [_btn_call_history setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btn_call_detail setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_lbl_call_detail_line setBackgroundColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0]];
    [_lbl_call_history_line setBackgroundColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];
    
     [_btn_feedback setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_lbl_feedback_line setBackgroundColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];

    _scroll_call_detail.hidden = false;
    _tblHistoryView.hidden = true;
    
    _tbl_callDetails.hidden = false;
    _tbl_feedback.hidden = true;
    
    _lbl_noFeedback.hidden = true;


}
-(void)showCallHistory
{

    
    [FIRAnalytics setUserPropertyString:@"" forName:@"callHistory_callLog"];
    [_btn_call_detail setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btn_call_history setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_lbl_call_history_line setBackgroundColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0]];
    [_lbl_call_detail_line setBackgroundColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];
    _scroll_call_detail.hidden = true;
    _tblHistoryView.hidden = false;
    _lblErrorMessageDisplay.hidden = true;
    
    //
    _tbl_callDetails.hidden = true;
}
-(void)showFeedback
{
    [FIRAnalytics setUserPropertyString:@"" forName:@"feedback_calllog"];
    [_btn_call_detail setTitleColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btn_feedback setTitleColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_lbl_feedback_line setBackgroundColor:[UIColor colorWithRed:75/255.0 green:85/255.0 blue:85/255.0 alpha:1.0]];
    [_lbl_call_detail_line setBackgroundColor:[UIColor colorWithRed:180/255.0 green:187/255.0 blue:187/255.0 alpha:1.0]];
    
    
    _scroll_call_detail.hidden = true;
    _tblHistoryView.hidden = true;

    _tbl_feedback.hidden = false;
    _lblErrorMessageDisplay.hidden = true;
    
    //
    _tbl_callDetails.hidden = true;
    
    BOOL flagfeedback = false;
    if ([[Default valueForKey:kISFeedbackInPlan] intValue] == 0 ) {
        _lbl_noFeedback.text = @"Feedback feature is not available in your plan.";
        flagfeedback = true;
        
    }
    else
    {
        if ([[Default valueForKey:kISFeedbackRightsForSub] intValue] == 0) {
            _lbl_noFeedback.text = @"You have no access rights for this module.";
            flagfeedback = true;
        }
        else
        {
            if (feedbackArray.count == 0) {
                _lbl_noFeedback.text = @"No Feedback Found.";
                flagfeedback = true;
            }
            
            
        }
    }
    
   
    
    if (flagfeedback == true ) {
        _lbl_noFeedback.hidden = false;
        _tbl_feedback.hidden = true;
    }
    else
    {
        _lbl_noFeedback.hidden = true;
        _tbl_feedback.hidden = false;

    }
}
-(void)call_history:(NSString*)skip callSID:(NSString*)callsid fromNumber:(NSString*)fromNumber toNumber:(NSString*)toNumber {
    //NSLog(@"Api call : %@",details_array);
    if ([skip intValue] == 0)
    {
        CallHistoryArr = [[NSMutableArray alloc] init];
    }
    if ([skip intValue] > 0)
    {
        
    }
    else
    {
        //[Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    }
    
    
    
    NSString *url = @"";
    
    //getContactCallLogs/callsid/fromNumber/toNumber?skip=0&limit=10
    url = [NSString stringWithFormat:@"getCallLogsDetails/%@/%@/%@/?skip=%@&limit=20",callsid,fromNumber,toNumber,skip];
    NSLog(@"Trush Calllogs : URL  :  %@%@ ",SERVERNAME,url);
    obj = [[WebApiController alloc] init];
    
    //  [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(login:response:) andDelegate:self];
    [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(login1:response:) andDelegate:self];
    
}
- (void)login1:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];

   // NSLog(@"Call_history : response1 : %@",response1);
   // [Processcall hideLoadingWithView];
    
   // NSLog(@"Encrypted Response : call history : %@",response1);


    
    //NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            
            //NSLog(@"call details response:: %@",response1);
            
            
            _lblErrorMessageDisplay.hidden = true;
            NSMutableArray *arr = response1[@"data"][@"contactLogs"];
            if(arr.count > 0)
            {
                
                [details_array addObjectsFromArray:arr];
                is_tbl_reload = true;
                //            //NSLog(@"Response : details_array : %@",details_array);
                _tblHistoryView.delegate = self;
                _tblHistoryView.dataSource = self;
                [_tblHistoryView reloadData];
                
            }
            
            //NSLog(@"@@@@@@@@@ %@",details_array);
        }
        else
        {
            @try
            {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    _tblHistoryView.hidden = true;
                    _lblErrorMessageDisplay.hidden = false;
                    _lblErrorMessageDisplay.text = [response1 valueForKey:@"error"][@"error"];
                }
            }
            @catch (NSException *exception) {
            }
        }
        
    }
    
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    
    
    [self.myTimer invalidate];
    btn_click = @"0";
    UIImage *img = [UIImage imageNamed:@"AudioPlay"];
    
    //old [_btn_img_record setImage:img forState:UIControlStateNormal];
     [recordCell.recordvw_btn_imgrecord setImage:img forState:UIControlStateNormal];
    
    //_lbl_record_time.text = @"00:00";
    
    //old _lbl_record_time.text = recordDuration;
    recordCell.recordvw_lbl_recordtime.text = recordDuration;
}

- (IBAction)btn_transcripted_voice_click:(UIButton *)sender
{

            if (@available(iOS 13, *))
            {
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UIWindow *alertWindow = [appDelegate window];//[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
                //      alertWindow.rootViewController = [[UIViewController alloc] init];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                VoiceMailTextVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"VoiceMailTextVC"];
                vc.strVoiceMailText = [_userData valueForKey:@"transcriptionText"];
                if (IS_IPAD) {
                    
                    vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                    //[self presentViewController:vc animated:YES completion:nil];
                } else {
                    [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
                    //[self presentViewController:vc animated:YES completion:nil];
                }
                [alertWindow.rootViewController presentViewController:vc animated:NO completion:nil];
                
            }
            else
            {
                UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                alertWindow.rootViewController = [[UIViewController alloc] init];
                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                [alertWindow makeKeyAndVisible];
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                VoiceMailTextVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"VoiceMailTextVC"];
                vc.strVoiceMailText = [_userData valueForKey:@"transcriptionText"];
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                } else {
                    [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                    vc.modalPresentationCapturesStatusBarAppearance = YES;
                }
                
                [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
            }
}

#pragma  mark - reminder methods
- (void)reminderBtnClick:(UITapGestureRecognizer *)touch
{
    NSString *numbVer = [Default valueForKey:IS_CALL_PLANNER];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner -------------- %@",numbVer);
    
    if (num == 1) {
        
        
     /*   if (_tbl_reminderpopup.hidden == true) {
            
            _tbl_reminderpopup.hidden = false;
            
            
            [self.view bringSubviewToFront:_tbl_reminderpopup];
          //  [[UIApplication sharedApplication].keyWindow addSubview:_tbl_reminderpopup];
            //[[UIApplication sharedApplication].keyWindow bringSubviewToFront:_tbl_reminderpopup];
            
        }
        else
        {
            _tbl_reminderpopup.hidden = true;
        }*/
        
        
        CGFloat x = [touch locationInView:self.view].x;
        CGFloat y = [touch locationInView:self.view].y;
        
        _tbl_reminderpopup.frame = CGRectMake(x-_tbl_reminderpopup.frame.size.width, y+10, _tbl_reminderpopup.frame.size.width, _tbl_reminderpopup.frame.size.height);
        
        NSLog(@"touch location :: %f, %f",x,y);
        
        if (_tbl_reminderpopup.hidden == true) {
            
            _tbl_reminderpopup.hidden = false;
            [self.view bringSubviewToFront:_tbl_reminderpopup];
          
        }
        else
        {
            _tbl_reminderpopup.hidden = true;
        }
    }
    else
    {
         [UtilsClass showAlert:@"Call Reminder is not available in your plan Please Upgrade your plan" contro:self];
    }
    
    
}
-(void)setReminder:(NSString *)reminderType  {
    
    NSString *parentId = [Default valueForKey:PARENT_ID];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = @"createCallReminder";
    /*
     "":"",
     "reminderNumber":"13093265247",
     "parentId":"5b977a8df19c7c1724650608",
     "createdById":"5b977a8df19c7c1724650608",
     "reminderType":"1",
     "deviceType":"ios"
     
     */
    NSDictionary *passDict = @{@"networkStrengthRandomString":@"",
                               @"reminderNumber":originalNum,
                               @"parentId":parentId,
                               @"createdById":userId,
                               @"reminderType":reminderType,
                               @"deviceType":@"ios"
                               };
    
    
    NSLog(@"Dictonary ****** : %@",passDict);

    
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(setreminderResponse:response:) andDelegate:self];
}

- (void)setreminderResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"REMINDER SET RESPONSE: %@",response1);
    [Processcall hideLoadingWithView];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Encrypted Response : set reminder : %@",response1);

    
    if ([[response1 valueForKey:@"success"] integerValue] == 1)
    {
        [UtilsClass makeToast:KRIMINDERSET];
        
        //notification to load call logs
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"newReminderSet_notification" object:self userInfo:nil];
        
        _tbl_reminderpopup.hidden = true;
        
    }
    
   // NSLog(@"TRUSHANG : STATUSCODE **************17  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
    
}
#pragma mark - blacklist methods

- (IBAction)blackListClick:(UIButton *)sender
{
    
    if([[Default valueForKey:kIsBlackList] boolValue] == true)
    {
        if ([[sender imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"icon_unblock"]]) {
            //already blacklisted then call API to unblock number
            
            UIAlertController * alert = [UIAlertController
                                                    alertControllerWithTitle:kAlertTitle
                                                    message:@"Are you sure you want to remove this number from blacklist?"
                                                    preferredStyle:UIAlertControllerStyleAlert];
                       
                       //Add Buttons
                       
                       UIAlertAction* yesButton = [UIAlertAction
                                                   actionWithTitle:@"Yes"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                           //Handle your yes please button action here
                           
                           
                       [self removeFromBlackList];

                           
                           
                       }];
                       
                       UIAlertAction* noButton = [UIAlertAction
                                                  actionWithTitle:@"Cancel"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                           //Handle no, thanks button
                       }];
                       
                       //Add your buttons to alert controller
                       
                       
                       
                       [alert addAction:yesButton];
                       [alert addAction:noButton];
                       
                       [self presentViewController:alert animated:NO completion:nil];
           
        
        }
        else
        {
            //not blacklisted then add to blacklist
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:kAlertTitle
                                         message:@"Are you sure you want to add this number to blacklist?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Yes"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                //Handle your yes please button action here
                
                
            [self addToBlackList];

                
                
            }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                //Handle no, thanks button
            }];
            
            //Add your buttons to alert controller
            
            
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:NO completion:nil];
        }
    }
    else
    {
        [UtilsClass makeToast:@"Block Number feature is not available in your plan."];
    }
    
}
-(void)addToBlackList
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *url = [NSString stringWithFormat:@"billing/plan/addNumberToBlockList"];
    
    NSString *userid = [Default valueForKey:USER_ID];
    NSString *parentId = [Default valueForKey:PARENT_ID];

    NSString *number = /*_lbl_number.text*/originalNum;
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSDictionary *passDict = @{@"deviceType":@"ios",
                               @"number":number,
                               @"parentId":parentId,
                               @"user":userid
    };
    
    
    
        NSLog(@"blacklist URL : %@",url);
        NSLog(@"blacklist Dic : %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(addblockListResponse:response:) andDelegate:self];
}
- (void)addblockListResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];


        if([apiAlias isEqualToString:Status_Code])
        {
                [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            
            NSLog(@"Encrypted Response : add blacklist : %@",response1);

            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                NSDictionary*dic = [response1 valueForKey:@"data"];
                
                    [Default setValue:[dic valueForKey:@"number"] forKey:kblackListedArray];
                    [Default synchronize];
                
                
                    [_btnBlacklist setImage:[UIImage imageNamed:@"icon_unblock"] forState:UIControlStateNormal];
                
                     [_btnBlacklistNocredit setImage:[UIImage imageNamed:@"icon_unblock"] forState:UIControlStateNormal];
                
                _btnAddReminder.enabled = false;
                _btnAddReminderNoCredit.enabled = false;

                [UtilsClass makeToast:@"Successfully number blacklisted."];
                   
                
            }
            else
            {
                [UtilsClass makeToast:[response1 valueForKey:@"error"]];
            }
        }
}
-(void)removeFromBlackList
{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *url = [NSString stringWithFormat:@"billing/plan/removeNumberFromBlockList"];
    
    NSString *userid = [Default valueForKey:USER_ID];
    NSString *parentId = [Default valueForKey:PARENT_ID];

    NSString *number = /*_lbl_number.text*/originalNum;
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
       number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSDictionary *passDict = @{@"deviceType":@"ios",
                               @"number":number,
                               @"parentId":parentId,
                               @"user":userid
    };
    
    
        NSLog(@"blacklist URL : %@",url);
        NSLog(@"blacklist Dic : %@",passDict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(removeblockListResponse:response:) andDelegate:self];
}
- (void)removeblockListResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];


        if([apiAlias isEqualToString:Status_Code])
        {
                [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            
            NSLog(@"Encrypted Response : remove blacklist : %@",response1);

            
            if ([[response1 valueForKey:@"success"] integerValue] == 1)
            {
                NSDictionary*dic = [response1 valueForKey:@"data"];
                               
                [Default setValue:[dic valueForKey:@"number"] forKey:kblackListedArray];
                [Default synchronize];
                
                
                    
                    [_btnBlacklist setImage:[UIImage imageNamed:@"icon_block"] forState:UIControlStateNormal];
                
                     [_btnBlacklistNocredit setImage:[UIImage imageNamed:@"icon_block"] forState:UIControlStateNormal];
                
                _btnAddReminder.enabled = true;
                _btnAddReminderNoCredit.enabled = true;
                
                  [UtilsClass makeToast:@"Successfully removed number from blacklist."];
            }
            else
            {
                [UtilsClass makeToast:[response1 valueForKey:@"error"]];
            }
        }
    
    
}
@end

