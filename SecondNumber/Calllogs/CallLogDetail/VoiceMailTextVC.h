//
//  VoiceMailTextVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VoiceMailTextVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *txtVoicemailTransCripted;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) NSString *strVoiceMailText;
@end

NS_ASSUME_NONNULL_END
