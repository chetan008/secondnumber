//
//  ACWViewController.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ACWViewController.h"
#import "Processcall.h"
#import "WebApiController.h"
#import "UtilsClass.h"
#import "CallReminderPopUp.h"
#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "Tblcell.h"


#define IS_IPHONE5 ([UIScreen mainScreen].bounds.size.height==568)
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface ACWViewController ()<EDStarRatingProtocol,UITextViewDelegate,UITextFieldDelegate>
{
    WebApiController *obj;
    NSDictionary *extraHeader;
    
    NSArray *tagList;
    NSArray *searchedTagList;
    
     
    
}
@property (strong,nonatomic) NSArray *colors;
@end

@implementation ACWViewController
@synthesize currMinute,currSeconds;
@synthesize lblTimer;
@synthesize timer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    tagList = [Default valueForKey:kTagList];
    NSLog(@"tag list ::%@",tagList);
   // [UtilsClass view_shadow_boder:_selectTagBtn];
    //_selectedTag = [[NSMutableArray alloc]init];
    
    [self starRatting];
    
    [_txtview_seelctedTag.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_txtview_seelctedTag.layer setShadowOpacity:0.8];
    [_txtview_seelctedTag.layer setShadowRadius:3.0];
    [_txtview_seelctedTag.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    _txtview_seelctedTag.layer.cornerRadius = 3.0;
    _txtview_seelctedTag.layer.borderWidth = 0.5;
    _txtview_seelctedTag.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [_tagdropdownBtn.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_tagdropdownBtn.layer setShadowOpacity:0.8];
    [_tagdropdownBtn.layer setShadowRadius:3.0];
    [_tagdropdownBtn.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    _tagdropdownBtn.layer.cornerRadius = 3.0;
    _tagdropdownBtn.layer.borderWidth = 0.5;
    _tagdropdownBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
     
    if (_selectedTag.count>0) {
    _txtview_seelctedTag.text = [[_selectedTag valueForKey:@"name"] componentsJoinedByString:@", "];
        _txtview_seelctedTag.textColor = [UIColor blackColor];
    }
    else
    {
        _txtview_seelctedTag.text = @"Please Select";
        _txtview_seelctedTag.textColor = [UIColor lightGrayColor];
    }
    
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]init];
    tapgesture.numberOfTapsRequired = 1;
    [tapgesture addTarget:self action:@selector(tapOnBackground:)];
    
    [self.view addGestureRecognizer:tapgesture];
    
    UITapGestureRecognizer *tapgesture1 = [[UITapGestureRecognizer alloc]init];
    tapgesture1.numberOfTapsRequired = 1;
    [tapgesture1 addTarget:self action:@selector(tapOnTagTxtview:)];
    
    [_txtview_seelctedTag addGestureRecognizer:tapgesture1];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [self keybord_setup];
}

-(void)starRatting {

    extraHeader = [[NSDictionary alloc]init];
    extraHeader = [[NSUserDefaults standardUserDefaults]objectForKey:@"extraHeader"];

    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.starRating addGestureRecognizer:swipeLeft];

    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(handleSingleTap:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.starRating addGestureRecognizer:swipeRight];
    
    UITapGestureRecognizer *singleFingerTap =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(handleSingleTap1:)];
    [self.starRating addGestureRecognizer:singleFingerTap];
    
    
    [self TimerStart];
    
    _lblCallDuration.text = _strCallDuration;
    _lblCallieeName.text = _strCallieeName;
    _lblDepartmentName.text = _strDepartmentName;
    
    _imgDepartment.image = [UIImage imageNamed:_strDepartmentFlag];
    _imgDepartment.layer.cornerRadius = 10.0;
    
    
    _txt_view.text = @"  Write Here...";
    _txt_view.textColor = [UIColor lightGrayColor];
    _txt_view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _txt_view.layer.borderWidth = 1.0;
    _txt_view.layer.cornerRadius = 5.0;
    _txt_view.delegate = self;
    _txtExtraComment.delegate = self;

    
    _txtExtraComment.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _txtExtraComment.layer.borderWidth = 1.0;
    _txtExtraComment.layer.cornerRadius = 5.0;
    
    _viewTimer.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _viewTimer.layer.borderWidth = 1.0;
    _viewTimer.layer.cornerRadius = 5.0;
    
    _btnEndAfterWork.layer.borderWidth = 1.0;
    _btnEndAfterWork.layer.cornerRadius = 5.0;
                                    
    _starRating.currentVC = @"acw";
    _starRating.backgroundColor  = [UIColor whiteColor];
    _starRating.starImage = [[UIImage imageNamed:@"acwstarselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starRating.starHighlightedImage = [[UIImage imageNamed:@"acwstarselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _starRating.maxRating = 5.0;
    _starRating.delegate = self;
    _starRating.horizontalMargin = 10.0;
    _starRating.editable=YES;
    _starRating.rating= 4;
    _starRating.displayMode=EDStarRatingDisplayHalf;
    [_starRating  setNeedsDisplay];
    self.colors = @[ [UIColor colorWithRed:227.0f/255.0f green:121.0f/255.0f blue:73.0f/255.0f alpha:1.0f], [UIColor colorWithRed:1.0f green:0.22f blue:0.22f alpha:1.0f], [UIColor colorWithRed:0.27f green:0.85f blue:0.46f alpha:1.0f], [UIColor colorWithRed:0.35f green:0.35f blue:0.81f alpha:1.0f]];
    _starRating.tintColor = self.colors[0];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 5, 20)];
    _txtExtraComment.leftView = paddingView;
    _txtExtraComment.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    // 20x20 is the size of the checkbox that you want
    // create 2 images sizes 20x20 , one empty square and
    // another of the same square with the checkmark in it
    // Create 2 UIImages with these new images, then:
    
}

//The event handling method
- (void)handleSingleTap:(UISwipeGestureRecognizer*)swipe
{
  if (_starRating.rating <= 2.5) {
      [_txtExtraComment setHidden:false];
  }else {
      [_txtExtraComment setHidden:true];
  }
}

//The event handling method
- (void)handleSingleTap1:(UITapGestureRecognizer *)recognizer
{
  if (_starRating.rating <= 2.5) {
      [_txtExtraComment setHidden:false];
  }else {
      [_txtExtraComment setHidden:true];
  }
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    if (textView.textColor == UIColor.lightGrayColor) {
        textView.text = @"";
        textView.textColor = UIColor.blackColor;
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"  Write Here...";
        textView.textColor = UIColor.lightGrayColor;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField.text isEqualToString:@""]) {
        textField.placeholder = @"Write Here...";
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.placeholder = @"";
}
-(void)TimerStart
{
    
    timer = [[NSTimer alloc] init];
    NSString *numbVer = [Default valueForKey:ACW_TIME];
    int num = [numbVer intValue];
    currMinute=num;
    currSeconds=00;
    timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(TimerStart:) userInfo:nil repeats:YES];
}

-(void)TimerStart:(NSTimer *)timer {
    //NSLog(@"\n \n Timer Call \n \n ");
    lblTimer.hidden = false;
//    currSeconds-=1;
//    if(currSeconds==60) {
//
//        currSeconds=0;
//        currMinute-=1;
//
//        if(currMinute==0 && currSeconds ==0) {
//            currMinute=0;
//            currSeconds=0;
//            [self TimerStop];
//        }
//    }
    
    
    if((currMinute>0 || currSeconds>0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        NSString *Trush_timer1 = [NSString stringWithFormat:@"%02d%@%02d",currMinute,@":",currSeconds];
        lblTimer.text = Trush_timer1;
        //NSLog(@"%@",Trush_timer1);
        //    [self updateStats];
    }
    else
    {
        [self TimerStop];
    }
   
    

    
}

-(void)TimerStop
{
    [timer invalidate];
    [self dismissViewControllerAnimated:YES completion:nil];
    NSString *numbVer = [Default valueForKey:IS_CALL_REMINDER];
    int num = [numbVer intValue];
    if (num == 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self reminderPopup];
        });
    }
    [self saveNotes];
    [self endWorkApi];
    [self submintReview];
}

-(void)reminderPopup {
    
    if (@available(iOS 13, *))
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UIWindow *alertWindow = [appDelegate window];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CallReminderPopUp *vc = [storyboard instantiateViewControllerWithIdentifier:@"CallReminderPopUp"];
        
//        if([_strCallStatus isEqualToString: OUTGOING])
        if([_strCallStatus isEqualToString: @"Outgoing"])
        {
             vc.incOutNumb = _strToNumber;
            vc.originalToNum = _originalToNumTopass;

            
        }else{
             vc.incOutNumb = _strFromNumber;
            vc.originalToNum = _strFromNumber;

            
        }
        
        vc.strDisplayName = _strCallieeName;
        
        
        
        //        if([_CallStatus isEqualToString: OUTGOING])
        //        {
        //            vc.strIncOutCall = @"Outgoing";
        //        }
        //        else
        //        {
        //            vc.strIncOutCall = @"Incoming";
        //        }
        
        //    vc.view.backgroundColor = [UIColor clearColor];
        //    self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        
        if (IS_IPAD) {
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
        } else {
            [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
            vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        }
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
    else
    {
        UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        alertWindow.rootViewController = [[UIViewController alloc] init];
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [alertWindow makeKeyAndVisible];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CallReminderPopUp *vc = [storyboard instantiateViewControllerWithIdentifier:@"CallReminderPopUp"];
      
        //if([_strCallStatus isEqualToString: OUTGOING])
         if([_strCallStatus isEqualToString: @"Outgoing"])
        {
             vc.incOutNumb = _strToNumber;
             vc.originalToNum = _originalToNumTopass;
        }else{
             vc.incOutNumb = _strFromNumber;
             vc.originalToNum = _strFromNumber;
        }
        vc.strDisplayName = _strCallieeName;
       
        
        
        
        //        if([_CallStatus isEqualToString: OUTGOING])
        //        {
        //            vc.strIncOutCall = @"Outgoing";
        //        }
        //        else
        //        {
        //            vc.strIncOutCall = @"Incoming";
        //        }
        
        //    vc.view.backgroundColor = [UIColor clearColor];
        //    self.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        
        if (IS_IPAD) {
            vc.popoverPresentationController.sourceView = alertWindow.rootViewController.view;
        } else {
            [vc.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
            vc.modalPresentationStyle = UIModalPresentationPopover;
        }
        
        [alertWindow.rootViewController presentViewController:vc animated:YES completion:nil];
    }
    
    
}


-(BOOL)prefersStatusBarHidden{
    return NO;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
//    CGPoint touchLocation = [touch locationInView:touch.view];
    [self.starRating endEditing:true];
    
        if (_starRating.rating <= 2.5) {
            [_txtExtraComment setHidden:false];
        }else {
            [_txtExtraComment setHidden:true];
        }
    
    

}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)endWorkApi {
    
//    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *parentId = [Default valueForKey:PARENT_ID];
    //NSLog(@"-------*****%@",_txt_view.text);
    NSString *trimmed = [_txt_view.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
//    NSString *newString = [someString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //NSLog(@"-------*****%@",trimmed);
    //NSLog(@"Test 1 %@",userId);
    //NSLog(@"Test 2 %@",parentId);
    //NSLog(@"Test 3 %@",trimmed);
    //NSLog(@"Test 4 %@",lblTimer.text);
    //NSLog(@"Test 5 %@",[NSString stringWithFormat:@"%f",_starRating.rating]);
    //NSLog(@"Test 6 %@",_strToNumber);
    //NSLog(@"Test 7 %@",_strFromNumber);
    NSDictionary *passDict;
    if([trimmed isEqualToString:@"Write Here..."]){
        trimmed = @"";
    }

    /*
<<<<<<< HEAD
/*
=======
 
>>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
    if ([_strCallStatus isEqualToString:@"Outgoing"]){
        passDict = @{
          @"userId":userId,
          @"parentId":parentId,
          @"afterCallWorkNote" : trimmed,
          @"acwDuration" : lblTimer.text,
          @"CallratingStar":[NSString stringWithFormat:@"%f",_starRating.rating],
          @"to" : _strToNumber,
          @"from":_strFromNumber,
          @"isTransferedCall":@false
        };
    }else{
        
        BOOL boolValue = [[extraHeader valueForKey:@"xphcalltransfer"] boolValue];
        
        passDict = @{
          @"userId":userId,
          @"parentId":parentId,
          @"afterCallWorkNote" : trimmed,
          @"acwDuration" : lblTimer.text,
          @"CallratingStar":[NSString stringWithFormat:@"%f",_starRating.rating],
          @"to" : _strToNumber,
          @"from":_strFromNumber,
          @"isTransferedCall":@(!boolValue)
        };
    }*/
    if ([_strCallStatus isEqualToString:@"Outgoing"]){
           passDict = @{
             @"userId":userId,
             @"parentId":parentId,
             @"afterCallWorkNote" : trimmed,
             @"acwDuration" : lblTimer.text,
             @"CallratingStar":[NSString stringWithFormat:@"%f",_starRating.rating],
             @"to" : _originalToNumTopass,
             @"from":_strFromNumber,
             @"isTransferedCall":@false
           };
       }else{
           
           BOOL boolValue = [[extraHeader valueForKey:@"xphcalltransfer"] boolValue];
           
           passDict = @{
             @"userId":userId,
             @"parentId":parentId,
             @"afterCallWorkNote" : trimmed,
             @"acwDuration" : lblTimer.text,
             @"CallratingStar":[NSString stringWithFormat:@"%f",_starRating.rating],
             @"to" : _originalToNumTopass,
             @"from":_strFromNumber,
             @"isTransferedCall":@(!boolValue)
           };
       }
    NSLog(@"-------*****%@",passDict);
    

    //NSLog(@"Login Dic ************ : %@",passDict);
    

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {

    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];

//    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];

    [obj callAPI_POST_RAW:@"endaftercallwork" andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"ACW Login_response : %@",response1);
    //NSLog(@"TRUSHANG : STATUSCODE **************11  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
        [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
    }
    
//
//    if ([[response1 valueForKey:@"success"] integerValue] == 1) {
//
//
//    }else {
////        _txtUserName.text = @"";
//
//        [Processcall hideLoadingWithView];
//        @try {
//            if (response1 != (id)[NSNull null] && response1 != nil ){
//                [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
//            }
//        }
//        @catch (NSException *exception) {
//        }
//    }
}

- (IBAction)btnEndAW:(id)sender {
    
   //  if ([_notesInPlan isEqualToString:@"true"]) {
         
            [self saveNotes];
  //   }
    
 
    [self TimerStop];
}

-(void)submintReview
{
    
//    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"rating/%@/add",userID];
    //NSLog(@"URL : %@",url);
    //NSLog(@"ratingStar : %f",_starRating.rating);
    //NSLog(@"userId : %@",userID);
    NSDictionary *passDict = @{@"ratingStar":[NSString stringWithFormat:@"%f",_starRating.rating],
                               @"reason":[NSString stringWithFormat:@"%@",_txtExtraComment.text],
                               @"otherComment":@"",
                               @"callType":_strCallStatus,
                               @"userId":userID};
    //NSLog(@"Dictonary : %@",passDict);
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(review:response:) andDelegate:self];
    
}

- (void)review:(NSString *)apiAlias response:(NSData *)response
{
    //NSLog(@"TRUSHANG : STATUSCODE **************12  : %@",apiAlias);
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
        [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];

    }
    else
    {
        
    }
}

-(void)keybord_setup
{
    //Enabling keyboard manager
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15];
    //Enabling autoToolbar behaviour. If It is set to NO. You have to manually create IQToolbar for keyboard.
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    //Setting toolbar behavious to IQAutoToolbarBySubviews. Set it to IQAutoToolbarByTag to manage previous/next according to UITextField's tag property in increasing order.
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    
    //Resign textField if touched outside of UITextField/UITextView.
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:NO];
    
    //Giving permission to modify TextView's frame
    [[UINavigationBar appearance] setTranslucent:false];
    UINavigationBar.appearance.translucent = false;
    
    //    IQKeyboardManager.shared.keyboardDistanceFromTextField = 50
    
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:100.0];
}

#pragma mark - Tag dropdown methods

- (IBAction)tagDropdownPressed:(id)sender {
    
    [self openTagList];
    
}
-(void)adjustTableHeight
{
    if ([searchedTagList count]>6) {
        _tagTableHeightConstraint.constant = 43.5 * 6;
    }
    else
    {
        _tagTableHeightConstraint.constant = 43.5 * [searchedTagList count];
    }
    _tagTableViewConstraint.constant = _tagTableHeightConstraint.constant + 85;
}
-(IBAction)clearAllClick:(id)sender
{
    [_selectedTag removeAllObjects];
    [_tagselectionTable reloadData];
}
-(IBAction)doneClick:(id)sender
{
    [self closeTagList];
}
-(IBAction)selectAllClick:(id)sender
{
    [_selectedTag removeAllObjects];
    [_selectedTag addObjectsFromArray:searchedTagList];
    [_tagselectionTable reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [searchedTagList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     Tblcell *cell = [_tagselectionTable dequeueReusableCellWithIdentifier:@"acwTagCell"];
    
    
    cell.lbl_acwTagListTitle.text = [[searchedTagList valueForKey:@"name"]  objectAtIndex:indexPath.row];
    
    if ([[_selectedTag valueForKey:@"id"] containsObject:[[searchedTagList valueForKey:@"id"] objectAtIndex:indexPath.row]]) {

        [cell.btn_acwTagCheckmark setImage:[UIImage imageNamed:@"check-box.png"] forState:UIControlStateNormal];
    }
    else
    {
         [cell.btn_acwTagCheckmark setImage:[UIImage imageNamed:@"blank-check-box"] forState:UIControlStateNormal];
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[_selectedTag valueForKey:@"id"] containsObject:[[searchedTagList valueForKey:@"id"] objectAtIndex:indexPath.row]]) {
        
        
        [_selectedTag removeObject:[searchedTagList objectAtIndex:indexPath.row]];
        
        
    }
    else
    {
        
          [_selectedTag addObject:[searchedTagList objectAtIndex:indexPath.row]];
    }
    NSLog(@"selected tag :: %@",_selectedTag);
    
    [_tagselectionTable reloadData];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"name contains[c] %@",searchBar.text];
    

    NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name]];
    
    
    NSArray *_filteredData1 = [tagList  filteredArrayUsingPredicate:predicate_final];

    if(_filteredData1.count != 0)
    {
        searchedTagList = [[NSMutableArray alloc] init];
        searchedTagList = (NSMutableArray*)_filteredData1;
        [self adjustTableHeight];

        [_tagselectionTable reloadData];
    }
    else
    {
        if ([searchBar.text isEqualToString:@""]) {
            searchedTagList = [[NSMutableArray alloc] init];
            searchedTagList = tagList;
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES];
            
            NSArray *arr = [searchedTagList sortedArrayUsingDescriptors:@[sortDescriptor] ];
            searchedTagList = (NSMutableArray*)arr;
            [self adjustTableHeight];

            [_tagselectionTable reloadData];
        } else {
            searchedTagList = [[NSMutableArray alloc] init];
            [self adjustTableHeight];

            [_tagselectionTable reloadData];
        }
        
    }
    
}

-(void)saveNotes
{
    NSLog(@"selected tag :: %@",_selectedTag);
    
    NSString *userId = [Default valueForKey:USER_ID];
          NSString *url = [NSString stringWithFormat:@"/savenote/twilio"];
    
    NSArray *tagIdArr = [[NSArray alloc]init];
    if (_selectedTag.count > 0 ) {
        tagIdArr = [_selectedTag valueForKey:@"id"];
    }
    
    if (_notesPassDict == nil) {
        
        
    }
        NSDictionary *passDict = @{@"description":@"",
                                   @"userId":userId,
                                   @"tags":tagIdArr,
                                   @"calluid":@"",
                                   @"internalCalling":[_notesPassDict valueForKey:@"internalCalling"],
                                   @"isBlindTransfer":[_notesPassDict valueForKey:@"isBlindTransfer"],
                                   @"isTransferedCall": [_notesPassDict valueForKey:@"isTransferedCall"]
        };
            
           
            
        NSLog(@"notes param dict acw: %@",passDict);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {

        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(saveNotesResponse:response:) andDelegate:self];
        
   // }
    
    
}

- (void)saveNotesResponse:(NSString *)apiAlias response:(NSData *)response {

    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];

    NSLog(@"api alias %@",apiAlias);

    NSLog(@"save notes response %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
    //[UtilsClass logoutUser:self];
     [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
}
-(void)tapOnBackground:(UITapGestureRecognizer *)tap
{
    [self closeTagList];
}
-(void)tapOnTagTxtview:(UITapGestureRecognizer *)tap
{
    [self openTagList];
}
-(void)openTagList
{
    _tagSelectionView.hidden = false;
    _tagMainView.hidden = false;
    _tagAlphaView.hidden= false;
    
    searchedTagList = tagList;
    
    [self adjustTableHeight];
    
    [_tagselectionTable reloadData];
}
-(void)closeTagList
{
    if (_tagMainView.hidden == false) {
   
    _tagSelectionView.hidden = true;
    _tagMainView.hidden = true;
    _tagAlphaView.hidden= true;
    
   // _selectTagBtn.titleLabel.text = [[_selectedTag valueForKey:@"name"] componentsJoinedByString:@","];
    if (_selectedTag.count>0) {
    _txtview_seelctedTag.text = [[_selectedTag valueForKey:@"name"] componentsJoinedByString:@", "];
        _txtview_seelctedTag.textColor = [UIColor blackColor];
    }
    else
    {
        _txtview_seelctedTag.text = @"Please Select";
        _txtview_seelctedTag.textColor = [UIColor lightGrayColor];
    }
        
    }
}
@end

