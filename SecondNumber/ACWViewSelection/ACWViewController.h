//
//  ACWViewController.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "SecondNumber-Swift.h"

@import Tagging;

NS_ASSUME_NONNULL_BEGIN

@interface ACWViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

//-(void)hideTextField;
//-(void)showTextField;
@property (strong, nonatomic) IBOutlet UILabel *lblTimer;
@property (strong, nonatomic) IBOutlet EDStarRating *starRating;
@property (weak, nonatomic) IBOutlet UITextView *txt_view;
@property (strong, nonatomic) IBOutlet UITextField *txtExtraComment;
@property (strong, nonatomic) IBOutlet UIView *viewTimer;
@property (strong, nonatomic) IBOutlet UIButton *btnEndAfterWork;


@property (strong, nonatomic) IBOutlet UILabel *lblCallieeName;
@property (strong, nonatomic) IBOutlet UILabel *lblDepartmentName;
@property (strong, nonatomic) IBOutlet UIImageView *imgDepartment;
@property (strong, nonatomic) IBOutlet UILabel *lblCallDuration;

@property (strong, nonatomic) NSString *strCallieeName;
@property (strong, nonatomic) NSString *strDepartmentName;
@property (strong, nonatomic) NSString *strDepartmentFlag;
@property (strong, nonatomic) NSString *strCallDuration;

@property (strong, nonatomic) NSString *strFromNumber;
@property (strong, nonatomic) NSString *strToNumber;
@property (strong, nonatomic) NSString *strText;
@property (strong, nonatomic) NSString *strCallStatus;
@property (strong, nonatomic) NSString *strRattingText;
@property (strong, nonatomic) IBOutlet UIButton *selectTagBtn;
@property (strong, nonatomic) IBOutlet UIButton *tagdropdownBtn;


@property (strong, nonatomic) IBOutlet UIButton *clearAllBtn;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong, nonatomic) IBOutlet UIView *tagSelectionView;
@property (strong, nonatomic) IBOutlet UISearchBar *tagsearchBar;
@property (strong, nonatomic) IBOutlet UITableView *tagselectionTable;
@property (strong, nonatomic)  NSMutableArray *selectedTag;
@property (strong, nonatomic)  NSDictionary *notesPassDict;
@property (assign, nonatomic)  NSString *notesInPlan;
@property (assign, nonatomic) IBOutlet UITextView *txtview_seelctedTag;


@property (strong, nonatomic) IBOutlet UIView *tagMainView;
@property (strong, nonatomic) IBOutlet UIView *tagAlphaView;

@property (strong, nonatomic) NSString *originalToNumTopass;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagTableHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagTableViewConstraint;


@property   int currMinute;
@property  int currSeconds;

@property NSTimer *timer;

@end

NS_ASSUME_NONNULL_END

