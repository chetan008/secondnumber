//
//  SMSListModel.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "SMSListModel.h"

@implementation SMSListModel

@synthesize SMSListArray;

static SMSListModel  *sharedSMSListData = nil;


+ (SMSListModel *)sharedSMSListData {
    

    if (sharedSMSListData == nil) {
        
        sharedSMSListData = [[super allocWithZone:NULL] init];
        sharedSMSListData.SMSListArray = [[NSMutableArray alloc]init];
        
    }
    
    return sharedSMSListData;
}

-(NSInteger)numberOfSMSINArray
{
    return [SMSListArray count];
}
-(void)removeAllObjectsFromSMSArray
{
    
}
-(void)addObjectsAtTop:(NSMutableArray *)array
{
    for (int i = 0; i<array.count; i++) {
        
        [sharedSMSListData.SMSListArray insertObject:[array objectAtIndex:i] atIndex:0];
    }
}
-(void)addObjectsAtBottom:(NSMutableArray *)array
{
    for (int i = 0; i<array.count; i++) {
        
        [sharedSMSListData.SMSListArray addObject:[array objectAtIndex:i]];
    }
}
@end

