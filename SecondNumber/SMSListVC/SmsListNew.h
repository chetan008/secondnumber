//
//  SmsListNew.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface SmsListNew : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btn_edit_message;
@property (weak, nonatomic) IBOutlet UIButton *view_right_menu;
@property (weak, nonatomic) IBOutlet UIView *view_mark;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nav_right_menu;
@property (nonatomic, assign) SWCellState stagevc;
@property (strong, nonatomic) IBOutlet UILabel *lblEmptyConversation;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *nav_search_btn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *nav_search_btn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

NS_ASSUME_NONNULL_END
