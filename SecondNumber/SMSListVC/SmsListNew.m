//
//  SmsListNew.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//


#import "SmsListNew.h"
#import "Tblcell.h"
#import "UIImageView+Letters.h"
#import "WebApiController.h"
#import "UtilsClass.h"
#import "Processcall.h"
#import "NewsmsVC.h"
#import "GlobalData.h"
#import "ChatViewControllerNew.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SMSListModel.h"

@interface SmsListNew ()<UITableViewDataSource, UITableViewDelegate,SWTableViewCellDelegate,UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate>
{
    UISearchController *searchController;
    WebApiController *obj;
    NSMutableArray *ApiResponseArray;
    NSMutableArray *FilterResponseArray;
    NSMutableArray *SelectedThreadid;
    NSMutableArray *Selected_not_index_arr;
    bool is_tbl_reload;
    NSMutableArray *Mixallcontact;
    
    BOOL isSearch;
    NSMutableArray *smsListArray;
    NSMutableArray *searchedSMSListArray;
     NSString *temp;
      NSString *temp1;
    
    BOOL isSMSLimitReached;

}
@end
NSString *btnclick_flag = @"0";
NSString *btnclick_flag_title = @"list";
NSString *Select_all_click = @"false";
NSString *smssearchFlag = @"0";
@implementation SmsListNew
@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self contact_get];
}
- (void)viewWillAppear:(BOOL)animated
{
    ApiResponseArray = [[NSMutableArray alloc] init];
    FilterResponseArray = [[NSMutableArray alloc] init];
    SelectedThreadid = [[NSMutableArray alloc] init];
    Selected_not_index_arr = [[NSMutableArray alloc] init];
    _view_right_menu.hidden = true;
    _view_mark.hidden = true;
    Select_all_click = @"false";
    _btn_edit_message.layer.cornerRadius = _btn_edit_message.frame.size.height / 2;
    _btn_edit_message.clipsToBounds = true;
    [UtilsClass view_navigation_title:self title:@"SMS"color:UIColor.whiteColor];
    [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
    self.tableView.allowsMultipleSelection = true;
    self.tableView.tableHeaderView = nil;
    
    //working without model
   // [self GetSmsList:@"0"];
    
    smsListArray = [[NSMutableArray alloc]init];
    isSearch = false;
    isSMSLimitReached = false;
    [self fetchSMS];
    
    //FilterResponseArray =  [[SMSListModel sharedSMSListData] SMSListArray];
    
    NSLog(@"filter respone array : %@",FilterResponseArray);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
   [self.tableView reloadData];
    
    temp = @"searchfirstime";
}
- (void)viewWillDisappear:(BOOL)animated
{
    
}
-(void)GetSmsList:(NSString*)skip{
    
    _lblEmptyConversation.hidden = true;
    
    
    if([UtilsClass isNetworkAvailable])
    {
        if ([skip intValue] == 0){
            ApiResponseArray = [[NSMutableArray alloc] init];
            FilterResponseArray = [[NSMutableArray alloc] init];
            [ApiResponseArray removeAllObjects];
            [FilterResponseArray removeAllObjects];
        }
        if ([skip intValue] > 0) {
            
        }else
        {
            [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
        }
        NSString *userId = [Default valueForKey:USER_ID];
        NSString *url = [NSString stringWithFormat:@"%@%@?skip=%@&limit=20",SMSLOGWITHSKIP,userId,skip];

        obj = [[WebApiController alloc] init];
        [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(GetSmsList:response:) andDelegate:self];
    }else
    {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

- (void)GetSmsList:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
   // NSLog(@"GetSmsList Response : %@",response1);
    
    NSLog(@"Encrypted Response : get sms : %@",response1);

    
    [Processcall hideLoadingWithView];
    is_tbl_reload = true;
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSMutableArray *responsearr = [NSMutableArray arrayWithArray:response1[@"data"]];
            [ApiResponseArray addObjectsFromArray:responsearr];
            FilterResponseArray = ApiResponseArray;
            
            
            if(FilterResponseArray.count > 0)
            {
                 _lblEmptyConversation.hidden = true;
            }
            else
            {
                _lblEmptyConversation.hidden = false;
            }
            
            if([Select_all_click isEqualToString:@"true"])
            {
                [self select_all];
            }
            
            self.tableView.tableHeaderView = nil;
            [searchController.searchBar becomeFirstResponder];
            tableView.delegate = self;
            tableView.dataSource = self;
            
            if ([tableView isEditing]) {
                NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                  for (NSIndexPath *path in indexPaths) {
                    [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
                  }
                });
            }
            
            [tableView reloadData];
           
        }
    }
}
-(void)contact_get
{
    Mixallcontact = [[NSMutableArray alloc] init];
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
        float contentYoffset = aScrollView.contentOffset.y;
           float halfpoint = aScrollView.contentSize.height / 2;
           if (halfpoint < contentYoffset )
           {
              if(is_tbl_reload == true)
               {
                   is_tbl_reload = false;
                
                   //working without model

                 //  [self GetSmsList:[NSString stringWithFormat:@"%lu",(unsigned long)[ApiResponseArray count]]];
                   
                   [self fetchSMS];
               }
           }
   
   
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _view_right_menu.hidden = true;
    btnclick_flag = @"0";
    btnclick_flag_title = @"list";
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /* old working code
     
     if(FilterResponseArray.count > 0)
     {
           return FilterResponseArray.count;
     }
     else
     {
         return 0;
     }
     
     */
    if (isSearch) {
        return [searchedSMSListArray count];
    }
    else
    {
        return [smsListArray count];
    }
  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   /* old working code
    
    Tblcell *tblcell = [tableView dequeueReusableCellWithIdentifier:@"Tblcell"];
    if(FilterResponseArray.count == 0)
    {
        return tblcell;
    }
    tblcell.selectionStyle = UITableViewCellSelectionStyleGray;
    NSDictionary *IndexDic  =  [FilterResponseArray objectAtIndex:indexPath.row];
    
    // Image
    if ([[IndexDic valueForKey:@"contactType"] isEqualToString:@"mobile"])
    {
        [tblcell.imgIconeSms setImageWithString:[IndexDic valueForKey:@"threadName"] color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
        tblcell.imgIconeSms.layer.cornerRadius = tblcell.imgIconeSms.frame.size.height / 2;
        tblcell.imgIconeSms.layer.cornerRadius = tblcell.imgIconeSms.frame.size.height / 2;
        tblcell.imgIconeSms.layer.borderWidth = 1.0;
        tblcell.imgIconeSms.layer.masksToBounds = YES;
        tblcell.imgIconeSms.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        tblcell.imgIconeSms.clipsToBounds = YES;
    }
    else if ([[IndexDic valueForKey:@"contactType"] isEqualToString:@"callhippo"])
    {
        [tblcell.imgIconeSms setImage:[UIImage imageNamed:Cell_callhippo_image]];
    }else{
        [tblcell.imgIconeSms setImage:[UIImage imageNamed:@"logo_drawer_user_v2"]];
    }
    
    // threadname
    if([IndexDic valueForKey:@"threadName"] != nil && [IndexDic valueForKey:@"threadName"] != [NSNull null])
    {
        tblcell.lblAutherName.text =[IndexDic valueForKey:@"threadName"];
    }
    
    //department name
    
   //pri NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 1)
    {
        if([IndexDic valueForKey:@"chNumberName"] != nil && [IndexDic valueForKey:@"chNumberName"] != [NSNull null])
        {
            tblcell.lblDeptName.text =[IndexDic valueForKey:@"chNumberName"];
        }
    
    }
    else
    {
        tblcell.lblDeptName.text =@"";
    }
    
    // lastmessage and date
    if([IndexDic valueForKey:@"messages"] != nil && [IndexDic valueForKey:@"messages"] != [NSNull null])
    {
        NSArray *msgArray = [IndexDic valueForKey:@"messages"];
        if(msgArray.count != 0)
        {
            tblcell.lblLatestMessage.text =[msgArray[msgArray.count-1] valueForKey:@"smsContent"];
            NSString *str=[msgArray[msgArray.count-1] valueForKey:@"smsTime"];
            NSArray *arr = [str componentsSeparatedByString:@" "];
            NSString *dateString = [NSString stringWithFormat:@"%@ %@",arr[1],arr[2]];
            tblcell.lblDate.text = dateString;
        }
        else
        {
            tblcell.lblLatestMessage.text = @"";
        }
    }
    
    // Read Unread
    if([[IndexDic valueForKey:@"threadStatus"]  isEqual: @"unread"]) {
        tblcell.lblAutherName.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        tblcell.lblLatestMessage.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    }else {
        tblcell.lblAutherName.font = [UIFont fontWithName:@"Helvetica-Regular" size:18];
        tblcell.lblLatestMessage.font = [UIFont fontWithName:@"Helvetica-Regular" size:15];
    }
    
    if ([tableView isEditing])
    {
        tblcell.stagecell = kCellStateCenter;
        _stagevc = kCellStateCenter;
        
        
        
        
    }
    else
    {
        NSMutableArray *leftUtilityButtons = [NSMutableArray new];
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        [leftUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                   title:@"Read"];
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                    title:@"Delete"];
        
        tblcell.leftUtilityButtons = leftUtilityButtons;
        tblcell.rightUtilityButtons = rightUtilityButtons;
        tblcell.delegate = self;
    }
    
   tblcell.tag = indexPath.row;
    
    return tblcell;
    
    */
    
    Tblcell *tblcell = [tableView dequeueReusableCellWithIdentifier:@"Tblcell"];
    
    NSMutableArray *arr;
    
    if (isSearch) {
        arr = [NSMutableArray arrayWithArray:searchedSMSListArray];
    }
    else
    {
        arr = [NSMutableArray arrayWithArray:smsListArray];
    }
     
     if(arr.count == 0)
     {
         return tblcell;
     }
    
     tblcell.selectionStyle = UITableViewCellSelectionStyleGray;
     NSDictionary *IndexDic  =  [arr objectAtIndex:indexPath.row];
     
     // Image
     if ([[IndexDic valueForKey:@"contactType"] isEqualToString:@"mobile"])
     {
         [tblcell.imgIconeSms setImageWithString:[IndexDic valueForKey:@"threadName"] color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
         tblcell.imgIconeSms.layer.cornerRadius = tblcell.imgIconeSms.frame.size.height / 2;
         tblcell.imgIconeSms.layer.cornerRadius = tblcell.imgIconeSms.frame.size.height / 2;
         tblcell.imgIconeSms.layer.borderWidth = 1.0;
         tblcell.imgIconeSms.layer.masksToBounds = YES;
         tblcell.imgIconeSms.layer.borderColor = [[UIColor lightGrayColor] CGColor];
         tblcell.imgIconeSms.clipsToBounds = YES;
     }
     else if ([[IndexDic valueForKey:@"contactType"] isEqualToString:@"callhippo"])
     {
         [tblcell.imgIconeSms setImage:[UIImage imageNamed:Cell_callhippo_image]];
     }else{
         [tblcell.imgIconeSms setImage:[UIImage imageNamed:@"logo_drawer_user_v2"]];
     }
     
     // threadname
     if([IndexDic valueForKey:@"threadName"] != nil && [IndexDic valueForKey:@"threadName"] != [NSNull null])
     {
         tblcell.lblAutherName.text =[IndexDic valueForKey:@"threadName"];
         
         if ([[IndexDic valueForKey:@"threadName"] isEqualToString:[IndexDic valueForKey:@"threadNumber"]]) {
             
             if ([Default boolForKey:kIsNumberMask] == true) {
                 tblcell.lblAutherName.text = [UtilsClass get_masked_number:[IndexDic valueForKey:@"threadName"]];
             }
         }
         
     }
    
   
     
     //department name
     
    //pri NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
     
     NSData *data = [Default valueForKey:PURCHASE_NUMBER];
     NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
     
     if(purchase_number.count > 1)
     {
         if([IndexDic valueForKey:@"chNumberName"] != nil && [IndexDic valueForKey:@"chNumberName"] != [NSNull null])
         {
             tblcell.lblDeptName.text =[IndexDic valueForKey:@"chNumberName"];
         }
     
     }
     else
     {
         tblcell.lblDeptName.text =@"";
     }
     
     // lastmessage and date
     if([IndexDic valueForKey:@"messages"] != nil && [IndexDic valueForKey:@"messages"] != [NSNull null])
     {
         NSArray *msgArray = [IndexDic valueForKey:@"messages"];
         if(msgArray.count != 0)
         {
             tblcell.lblLatestMessage.text =[msgArray[msgArray.count-1] valueForKey:@"smsContent"];
             NSString *str=[msgArray[msgArray.count-1] valueForKey:@"smsTime"];
             NSArray *arr = [str componentsSeparatedByString:@" "];
             NSString *dateString = [NSString stringWithFormat:@"%@ %@",arr[1],arr[2]];
             tblcell.lblDate.text = dateString;
         }
         else
         {
             tblcell.lblLatestMessage.text = @"";
         }
     }
     
     // Read Unread
     if([[IndexDic valueForKey:@"threadStatus"]  isEqual: @"unread"]) {
         tblcell.lblAutherName.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
         tblcell.lblLatestMessage.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
     }else {
         tblcell.lblAutherName.font = [UIFont fontWithName:@"Helvetica-Regular" size:18];
         tblcell.lblLatestMessage.font = [UIFont fontWithName:@"Helvetica-Regular" size:15];
     }
     
     if ([tableView isEditing])
     {
         tblcell.stagecell = kCellStateCenter;
         _stagevc = kCellStateCenter;
         
         
         
         
     }
     else
     {
         NSMutableArray *leftUtilityButtons = [NSMutableArray new];
         NSMutableArray *rightUtilityButtons = [NSMutableArray new];
         
         [leftUtilityButtons sw_addUtilityButtonWithColor:
          [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                    title:@"Read"];
         [rightUtilityButtons sw_addUtilityButtonWithColor:
          [UIColor colorWithRed:227.0/255.0 green:121.0/255.0 blue:73.0/255.0 alpha:1.0]
                                                     title:@"Delete"];
         
         tblcell.leftUtilityButtons = leftUtilityButtons;
         tblcell.rightUtilityButtons = rightUtilityButtons;
         tblcell.delegate = self;
     }
     
    tblcell.tag = indexPath.row;
     
     return tblcell;
     
    
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"Select_all_click  %@",Select_all_click);
    
    NSArray *arr ;
    if (isSearch) {
        arr = searchedSMSListArray;
    }
    else
    {
        arr = smsListArray;
    }
    
    if(arr.count != 0)
    {
        if ([tableView isEditing])
        {
         if([Select_all_click isEqualToString:@"true"])
         {
             if([Selected_not_index_arr containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
             {
                
             }
             else
             {
                [cell setSelected:YES animated:NO];
             }
            
         }
        
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr ;
    if (isSearch) {
        arr = searchedSMSListArray;
    }
    else
    {
        arr = smsListArray;
    }
    
    _view_right_menu.hidden = true;
    btnclick_flag = @"0";
    if ([tableView isEditing])
    {
         [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
         UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
         [cell setSelected:YES];
        if(arr.count != 0)
        {
             [SelectedThreadid addObject:[arr[indexPath.row] valueForKey:@"threadId"]];
            _view_mark.hidden = false;
            
        }
        else
        {
            _view_mark.hidden = true;
        }
        //
    }
    else
    {
        NSArray *array;
        
        if (isSearch) {
            
            array = searchedSMSListArray;
        }
        else
        {
            array = smsListArray;
        }
        
        
        [SelectedThreadid addObject:[array[indexPath.row] valueForKey:@"threadId"]];
            [self Unread_multiple_thread];
            [self save_contact:[array[indexPath.row] valueForKey:@"threadNumber"]];
            ChatViewControllerNew *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
            controller.ThreadMessageDic = [array objectAtIndex:indexPath.row];
            controller.contactId = [array[indexPath.row] valueForKey:@"contactId"];
            controller.number = [array[indexPath.row] valueForKey:@"threadNumber"];
            controller.frmNumber = [array[indexPath.row] valueForKey:@"chNumber"];
            controller.contactId = [array[indexPath.row] valueForKey:@"contactId"];
            controller.threadId = [array[indexPath.row] valueForKey:@"threadId"];
            controller.tblContactArray = array[indexPath.row];
            controller.VcFrom = @"SmsListNew";
        
        controller.titleString = [array[indexPath.row] valueForKey:@"threadName"];
        
        if ([[array[indexPath.row] valueForKey:@"threadName"] isEqualToString:[array[indexPath.row] valueForKey:@"threadNumber"]]) {
            
            if ([Default boolForKey:kIsNumberMask] == true) {
                controller.titleString =[UtilsClass get_masked_number:[array[indexPath.row] valueForKey:@"threadName"]];
            }
        }
        
            
            [self.navigationController pushViewController:controller animated:YES];
            
        searchController.searchBar.text = @"";
        [searchController dismissViewControllerAnimated:NO completion:nil];
        isSearch = false;
        
        if (smsListArray.count>0) {
           [smsListArray removeAllObjects];
        }
        if (searchedSMSListArray.count>0) {
           [searchedSMSListArray removeAllObjects];
        }
        
        
        /* working
        if(FilterResponseArray.count != 0)
        {
            [SelectedThreadid addObject:[FilterResponseArray[indexPath.row] valueForKey:@"threadId"]];
            [self Unread_multiple_thread];
            [self save_contact:[FilterResponseArray[indexPath.row] valueForKey:@"threadNumber"]];
            ChatViewControllerNew *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
            controller.ThreadMessageDic = [FilterResponseArray objectAtIndex:indexPath.row];
            controller.contactId = [FilterResponseArray[indexPath.row] valueForKey:@"contactId"];
            controller.number = [FilterResponseArray[indexPath.row] valueForKey:@"threadNumber"];
            controller.frmNumber = [FilterResponseArray[indexPath.row] valueForKey:@"chNumber"];
            controller.contactId = [FilterResponseArray[indexPath.row] valueForKey:@"contactId"];
            controller.threadId = [FilterResponseArray[indexPath.row] valueForKey:@"threadId"];
            controller.tblContactArray = FilterResponseArray[indexPath.row];
            controller.VcFrom = @"SmsListNew";
            controller.titleString = [FilterResponseArray[indexPath.row] valueForKey:@"threadName"];
            [self.navigationController pushViewController:controller animated:YES];
            
           searchController.searchBar.text = @"";
        [searchController dismissViewControllerAnimated:NO completion:nil];
            smssearchFlag = @"0";
            
          
        }*/
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *arr ;
    if (isSearch) {
        arr = searchedSMSListArray;
    }
    else
    {
        arr = smsListArray;
    }
    
    if ([tableView isEditing])
    {
        if([Select_all_click isEqualToString:@"true"])
        {
            [Selected_not_index_arr addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        }
        
        if(arr.count != 0)
        {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
            [cell setSelected:NO];
            [SelectedThreadid removeObject:[arr[indexPath.row] valueForKey:@"threadId"]];
            if(SelectedThreadid.count == 0)
            {
                _view_mark.hidden = true;
            }
            
        }
    }
    else
    {
        
    }
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    
 /*
  working
  switch (index) {
        case 0:
        {
            if(FilterResponseArray.count != 0)
            {
//                NSLog(@"tagggg ------ %ld",(long)cell.tag);
                [SelectedThreadid addObject:[FilterResponseArray[cell.tag] valueForKey:@"threadId"]];
                [self Unread_multiple_thread];
            }
            break;
        }
        default:
            break;
    }*/
    
    switch (index) {
            case 0:
            {
                NSArray *arr;
                if (isSearch) {
                    arr = searchedSMSListArray;
                }
                else
                {
                    arr = smsListArray;
                }
                if(arr.count != 0)
                {
    //                NSLog(@"tagggg ------ %ld",(long)cell.tag);
                    [SelectedThreadid addObject:[arr[cell.tag] valueForKey:@"threadId"]];
                    [self Unread_multiple_thread];
                }
                break;
            }
            default:
                break;
        }
    
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    /* working
    switch (index) {
        case 0:
        {
            if(FilterResponseArray.count != 0)
            {
//                NSLog(@"taggggg ---------- %ld",(long)cell.tag);
                [SelectedThreadid addObject:[FilterResponseArray[cell.tag] valueForKey:@"threadId"]];
                [self deleteSms];
            }
            break;
        }
        default:
            break;
    }*/
    
    switch (index) {
            case 0:
            {
                NSArray *arr;
                if (isSearch) {
                    arr = searchedSMSListArray;
                }
                else
                {
                    arr = smsListArray;
                }
                
                if(arr.count != 0)
                {
    //                NSLog(@"taggggg ---------- %ld",(long)cell.tag);
                    [SelectedThreadid addObject:[arr[cell.tag] valueForKey:@"threadId"]];
                    [self deleteSms];
                }
                break;
            }
            default:
                break;
        }
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}
- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    return YES;
}

-(void)Unread_multiple_thread
{
    NSString *getstr = [SelectedThreadid componentsJoinedByString:@","];
    NSString *url = [NSString stringWithFormat:@"updatesmsstatus/%@",getstr];
    NSDictionary *passDict = @{@"threadIds":getstr};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(Unread_delete_multiple_thread_response:response:) andDelegate:self];
}
- (void)Unread_delete_multiple_thread_response:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
    
        NSLog(@"Encrypted Response : unread thread : %@",response1);

    if ([[response1 valueForKey:@"success"] integerValue] == 1)
    {
        [_nav_right_menu setImage:[UIImage imageNamed:@"rightmenu"]];
        btnclick_flag = @"0";
        btnclick_flag_title = @"read";
        Select_all_click = @"false";
        
        _view_mark.hidden = true;
        _view_right_menu.hidden = true;
        UITableView *tableView = [self tableView];
        [tableView setEditing:false animated:YES];
        
        [SelectedThreadid removeAllObjects];
        [Selected_not_index_arr removeAllObjects];
       
        //[self GetSmsList:@"0"];
         isSMSLimitReached = false;
        smsListArray = [[NSMutableArray alloc]init];
        [self fetchSMS];
        
        _nav_search_btn.tintColor = [UIColor lightGrayColor];
        _nav_search_btn.enabled = true;
        
        
    }else
    {
        @try {
            if (response1 != (id)[NSNull null] && response1 != nil ){
[UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
        }
        }
        @catch (NSException *exception) {
        }
    }
    
    }
}

- (void)deleteSms
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:kAlertTitle
                                 message:@"Are You Sure Want to Delete Messages!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [self delete_multiple_thread];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                        
                            NSArray *arr;
                            if (self->isSearch) {
                                    arr = self->searchedSMSListArray;
                            }
                            else
                            {
                                arr = self->smsListArray;
                            }
                                    if(arr.count != 0)
                                    {
                                        [self.tableView reloadData];
                                    }
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)delete_multiple_thread
{
    NSString *getstr = [SelectedThreadid componentsJoinedByString:@","];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *url = [NSString stringWithFormat:@"sms/%@/remove",userId];
    NSDictionary *passDict = @{@"threadIds":getstr,
                               };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(Unread_delete_multiple_thread_response:response:) andDelegate:self];
}
-(void)save_contact:(NSString *)contact_number
{
    NSString *num = contact_number;
    NSString *string = [num stringByReplacingOccurrencesOfString:@"(" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    //pNSPredicate *filter = [NSPredicate predicateWithFormat:@"number_int contains[c] %@ ",string];
    
    //-mixallcontact
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",num] ;
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num] ;
    
    NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];

    
    NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
    
    if(filteredContacts.count != 0)
    {
        NSDictionary *dic = [filteredContacts objectAtIndex:0];
        if(!dic[@"_id"])
        {
            NSString *ContactName = [dic valueForKey:@"name"];
            
            //p NSString *ContactNumber = [dic valueForKey:@"number"];
            NSString *ContactNumber;
            if ([[dic valueForKey:@"numberArray"] count]>0) {
                ContactNumber = [[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            else
            {
                ContactNumber =@"";
            }

            [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
        }
    }
}
- (IBAction)btn_right_menu_click:(UIBarButtonItem *)sender
{
    NSArray *arr ;
    if (isSearch) {
        arr = searchedSMSListArray;
    }
    else
    {
        arr = smsListArray;
    }
    
    if(sender.image == [UIImage imageNamed:@"close"])
    {
        for (int i = 0; i<arr.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
        }
        
        [_nav_right_menu setImage:[UIImage imageNamed:@"rightmenu"]];
        btnclick_flag = @"0";
        btnclick_flag_title = @"read";
        Select_all_click = @"false";
        Selected_not_index_arr = [[NSMutableArray alloc] init];
        _view_mark.hidden = true;
        UITableView *tableView = [self tableView];
        [tableView setEditing:false animated:YES];
        _stagevc = kCellStateCenter;
        
        _nav_search_btn.tintColor = [UIColor lightGrayColor];
           _nav_search_btn.enabled = true;
    
    }
    else
    {
        if ([btnclick_flag  isEqual: @"0"])
        {
            _view_right_menu.hidden = false;
            btnclick_flag = @"1";
            
        }
        else
        {
            _view_right_menu.hidden = true;
            btnclick_flag = @"0";
        }
    }
    
}
- (IBAction)btn_menu_click:(UIBarButtonItem *)sender
{
    _view_right_menu.hidden = true;
    btnclick_flag = @"0";
    btnclick_flag_title = @"list";
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}
- (IBAction)btn_delete_click:(id)sender
{
     _nav_search_btn.tintColor = [UIColor clearColor];
       _nav_search_btn.enabled = false;
    
    UITableView *tableView = [self tableView];
    [tableView setEditing:true animated:YES];
    _view_right_menu.hidden = true;
    btnclick_flag = @"0";
    btnclick_flag_title = @"delete";
    Select_all_click = @"false";
    Selected_not_index_arr = [[NSMutableArray alloc] init];
    [_nav_right_menu setImage:[UIImage imageNamed:@"close"]];
}

- (IBAction)btn_new_sms_click:(id)sender {
    _view_right_menu.hidden = true;
    btnclick_flag = @"0";
    btnclick_flag_title = @"list";
    Select_all_click = @"false";
    Selected_not_index_arr = [[NSMutableArray alloc] init];
    NewsmsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"NewsmsVC"];
    vc.strFromNumber = @"";
    vc.VcFrom = @"ViewController";
    [[self navigationController] pushViewController:vc animated:YES];
}

- (IBAction)btn_read_click:(id)sender
{
    _nav_search_btn.tintColor = [UIColor clearColor];
    _nav_search_btn.enabled = false;
    
    _stagevc = kCellStateCenter;
    UITableView *tableView = [self tableView];
    [tableView setEditing:true animated:YES];
    _view_right_menu.hidden = true;
    btnclick_flag = @"0";
    btnclick_flag_title = @"read";
    Select_all_click = @"true";
    [self select_all];
    [_nav_right_menu setImage:[UIImage imageNamed:@"close"]];
}
-(void)select_all
{
    NSArray *arr ;
    if (isSearch) {
        arr = searchedSMSListArray;
    }
    else
    {
        arr = smsListArray;
    }
    
    
    if([Select_all_click isEqualToString:@"true"])
    {
        
        if(Selected_not_index_arr.count == 0)
        {
            [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
            for (int i = 0; i<arr.count; i++) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [self tableView:tableView didSelectRowAtIndexPath:indexPath];
                [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
            }
        }
        else
        {
            [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
            for (int i = 0; i<arr.count; i++) {
                if(![Selected_not_index_arr containsObject:[NSString stringWithFormat:@"%d",i]])
                {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                    [self tableView:tableView didSelectRowAtIndexPath:indexPath];
                    [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
                }
            }
        }
        
    }
}
- (IBAction)btn_group_read_click:(UIButton *)sender
{
    if (SelectedThreadid.count == 0){

    }else {
        [self Unread_multiple_thread];
    }


}
- (IBAction)btn_group_delete_click:(UIButton *)sender
{
    if (SelectedThreadid.count == 0){

    }else {
        [self deleteSms];
    }

}
- (IBAction)btn_search_list:(UIBarButtonItem *)sender
{
   
    if(isSearch == false)
    {
        if ([tableView numberOfRowsInSection:0] > 0) {
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                              atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
       
        isSearch = true;
        
        searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        searchController.searchResultsUpdater = self;
        
        searchController.hidesNavigationBarDuringPresentation = true;
        searchController.dimsBackgroundDuringPresentation = false;
        
        searchController.delegate = self;
        searchController.searchBar.delegate = self;
        
        [searchController.searchBar sizeToFit];
        self.tableView.tableHeaderView = searchController.searchBar;
        self.definesPresentationContext = true;
        [searchController setActive:YES];
    }
    else
    {
        if(searchedSMSListArray.count != 0)
        {
            isSearch = false;
            self.tableView.tableHeaderView = nil;
            [self.tableView reloadData];
        }
    }
   /*
    
    old working
    
    if ([smssearchFlag isEqual: @"0"])
    {
        if(FilterResponseArray.count != 0)
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                       atScrollPosition:UITableViewScrollPositionTop animated:NO];
            smssearchFlag = @"1";
            
            searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
            searchController.searchResultsUpdater = self;
            
            searchController.hidesNavigationBarDuringPresentation = true;
            searchController.dimsBackgroundDuringPresentation = false;
            
            searchController.delegate = self;
            searchController.searchBar.delegate = self;
            
            [searchController.searchBar sizeToFit];
            self.tableView.tableHeaderView = searchController.searchBar;
            self.definesPresentationContext = true;
            [searchController setActive:YES];
        }
    }else {
        if(FilterResponseArray.count != 0)
        {
            smssearchFlag = @"0";
            self.tableView.tableHeaderView = nil;
            [self.tableView reloadData];
        }
    }*/
    

    
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    /*old working
     
     [self search_thread];
     
     */
    
    if (searchController.searchBar.text.length>3 ) {
        
        isSearch = true;
        [self fetchSMS];
    }
    else if ([searchController.searchBar.text isEqualToString:@""])
    {
        isSearch = false;
        searchedSMSListArray = [[NSMutableArray alloc]init];
        [self.tableView reloadData];
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.tableView.tableHeaderView = nil;
    FilterResponseArray = [[NSMutableArray alloc] init];
    
    //working
    /*
    FilterResponseArray = ApiResponseArray;
    
   
    smssearchFlag = @"0";
    self.tableView.tableHeaderView = nil;
    [self.tableView reloadData];*/
    
    isSearch = false;
    searchedSMSListArray = [[NSMutableArray alloc]init];
    self.tableView.tableHeaderView = nil;
    [self.tableView reloadData];
}

- (void)didPresentSearchController:(UISearchController *)searchController
{
    [searchController.searchBar becomeFirstResponder];
}
-(void)search_thread
{
    if(![searchController.searchBar.text isEqualToString:@""])
    {
        NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"threadName contains[c] %@",searchController.searchBar.text];
        NSPredicate *predicate_number = [NSPredicate predicateWithFormat:@"threadNumber contains[c] %@",searchController.searchBar.text];
        NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name,predicate_number]];
        
        //working
        
        //NSArray *seararray = [ApiResponseArray filteredArrayUsingPredicate:predicate_final];
        
        NSArray *seararray = [[[SMSListModel sharedSMSListData] SMSListArray] filteredArrayUsingPredicate:predicate_final];
        
        if(seararray.count != 0)
        {
            FilterResponseArray = [[NSMutableArray alloc] init];
            FilterResponseArray = (NSMutableArray*)seararray;
            [tableView reloadData];
        }
        else
        {
            FilterResponseArray = [[NSMutableArray alloc] init];
            [tableView reloadData];
        }
    }
    else
    {
        FilterResponseArray = [[NSMutableArray alloc] init];
        
        //working
        //FilterResponseArray = ApiResponseArray;
        FilterResponseArray = [[SMSListModel sharedSMSListData] SMSListArray];
         [tableView reloadData];
    }
}
    
#pragma mark - get sms list API
    
-(void)fetchSMS
{
    if (isSearch) {
        
        [self getSMSBySearch:0];
    }
    else
    {
        [self getSMS:(int)[smsListArray count]];
    }
}
-(void)getSMSBySearch:(int)skip
{
   
   
    int delay = 0;

    if (temp1 == temp) {
        delay = 2;
    }
    
    
    
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void) {
         
         
         tableView.userInteractionEnabled = false;
         
         self->_lblEmptyConversation.hidden = true;
        
        if([UtilsClass isNetworkAvailable])
        {
            if (skip  == 0){
                
                self->searchedSMSListArray = [[NSMutableArray alloc]init];
            }
            
            self->temp1 = self->temp;
            NSString *userId = [Default valueForKey:USER_ID];
            NSString *url = [NSString stringWithFormat:@"%@%@?skip=%d&limit=20&searchText=%@",SMSLOGWITHSKIP,userId,skip,self->searchController.searchBar.text];
            
        
            self->obj = [[WebApiController alloc] init];
            [self->obj callAPI_GET:url andParams:nil SuccessCallback:@selector(getSMSBySearchResponse:response:) andDelegate:self];
             NSLog(@"sms search url  :: %@",url);
        }else
        {
            [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
        }
     });
   
    
}
- (void)getSMSBySearchResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    tableView.userInteractionEnabled = true;

    //NSLog(@"GetSmsList search Response : %@",response1);
    
    NSLog(@"Encrypted Response : sms search : %@",response1);


    is_tbl_reload = true;
    
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
        
    {
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSMutableArray *responsearr = [NSMutableArray arrayWithArray:response1[@"data"]];
           
            
            if (responsearr.count > 0) {
                
           
               //       dispatch_get_main_queue(), ^{
            
            searchedSMSListArray = responsearr;
            
//            if([searchedSMSListArray count] > 0)
//            {
//                 _lblEmptyConversation.hidden = true;
//            }
//            else
//            {
//                _lblEmptyConversation.hidden = false;
//            }
                
            
            if([Select_all_click isEqualToString:@"true"])
            {
                [self select_all];
            }
            
//            self.tableView.tableHeaderView = nil;
//            [searchController.searchBar becomeFirstResponder];
            tableView.delegate = self;
            tableView.dataSource = self;
            
            if ([tableView isEditing]) {
                NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                  for (NSIndexPath *path in indexPaths) {
                    [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
                  }
                });
            }
            
                          [self->tableView reloadData];
                          
                     // };
            }
            else
            {
                isSearch = false;
            }
            
           if([smsListArray count] > 0)
           {
                _lblEmptyConversation.hidden = true;
           }
           else
           {
               _lblEmptyConversation.hidden = false;

           }
        }
    }
}
   
-(void)getSMS:(int)skip
{
    if (isSMSLimitReached == false) {
    
            _lblEmptyConversation.hidden = true;
            
            if([UtilsClass isNetworkAvailable])
            {
                if (skip  == 0){
                    
                    smsListArray = [[NSMutableArray alloc]init];
                    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
                    
                }
                
                NSString *userId = [Default valueForKey:USER_ID];
                NSString *url = [NSString stringWithFormat:@"%@%@?skip=%d&limit=20",SMSLOGWITHSKIP,userId,skip];

               
                
                obj = [[WebApiController alloc] init];
                [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(getSMSResponse:response:) andDelegate:self];
                 NSLog(@"sms url  :: %@",url);
            }else
            {
                [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
            }
    }
}

- (void)getSMSResponse:(NSString *)apiAlias response:(NSData *)response{
    
    
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"GetSmsList Response : %@",response1);
    
    NSLog(@"Encrypted Response : get sms : %@",response1);

    
    [Processcall hideLoadingWithView];
    
    is_tbl_reload = true;
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSMutableArray *responsearr = [NSMutableArray arrayWithArray:response1[@"data"]];
           
            if (responsearr.count > 0) {
                
                [smsListArray addObjectsFromArray:responsearr];
            }
            else
            {
                 isSMSLimitReached = true;
                
            }
            
            if([smsListArray count] > 0)
            {
                 _lblEmptyConversation.hidden = true;
            }
            else
            {
                _lblEmptyConversation.hidden = false;

            }
            
            if([Select_all_click isEqualToString:@"true"])
            {
                [self select_all];
            }
            
            self.tableView.tableHeaderView = nil;
//            [searchController.searchBar becomeFirstResponder];
            tableView.delegate = self;
            tableView.dataSource = self;
            
            if ([tableView isEditing]) {
                NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                  for (NSIndexPath *path in indexPaths) {
                    [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
                  }
                });
            }
            
                [tableView reloadData];
            
            
           
        }
    }
}
    
    
@end
