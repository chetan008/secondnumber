//
//  NewsmsVC.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//


#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "NewsmsVC.h"
#import "Tblcell.h"
#import "SecondNumber-Swift.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "IQKeyboardManager.h"
#import "UIImageView+Letters.h"
#import "countryListVC.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "GlobalData.h"
#import "ChatViewControllerNew.h"
#import "WebApiController.h"
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
@import NKVPhonePicker;
@interface NewsmsVC ()<KSTokenViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating>{
    WebApiController *obj;
    NSArray *filteredData;
    CGFloat keyboardSize;
    NSArray *purchase_number;
    UIView *Blur_view;
    NSMutableArray *responseArray;
    NSMutableDictionary *respArray;
    NSMutableArray *tblArray;
    NSString *smsFromNumber;
    NSString *smstoNumber;
    NSString *chnumberID;
    NSString *ContactEmaiID;
    NSString *ContactCompnayName;
    NSString *TheredName;
    NSString *ContactID;
    NSString *Group_ToNum;
    CGSize keyboardSizeNew;
    NSMutableArray *selectedIndexPath;
    
    NSString *flagDeletedByRowSelection;
    
    NSArray *allContactsArray;
    NSMutableArray *allContactsArrayOriginal;
    
    NSString *selectedContactNumber;
    NSString *selectedContactName;
    
    UISearchController *searchController;
    NSString *searchFlag ;

    
    
}
@property (strong, nonatomic) IBOutlet KSTokenView *view_tag;
@end

@implementation NewsmsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    searchFlag = @"0";
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    _strNumber = @"";
    _strTypNumber = @"";
    _view_tag.delegate = self;
    _view_tag.placeholder = @"";
    _view_tag.style = KSTokenViewStyleSquared;
    [self getNumbers];
    _view_tag.backgroundColor = [UIColor clearColor];
    _view_tag.searchResultHeight = 0;
    _tbl_seletion_number.delegate = self;
    _tbl_seletion_number.dataSource = self;
    
    _txt_send_textview.text = @"Write message";
    _txt_send_textview.textColor = [UIColor lightGrayColor];
    _txt_send_textview.layer.borderWidth = 1.0;
    _txt_send_textview.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    _txt_send_textview.layer.cornerRadius = _txt_send_textview.frame.size.height / 2;
    _txt_send_textview.delegate = self;
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [_view_tag._tokenField setKeyboardType:UIKeyboardTypePhonePad];

    _view_tag._tokenField.placeholder = @"Type a phone number with country code";
    _flag_num_selection = @"0";
    if ([_controller isEqualToString:@"dialerVC"]){
        if ([_namefromothercontroller isEqualToString:@""]){
            
            selectedContactNumber = _numberfromothercontroller;
            
            [_view_tag addTokenWithTitle:_numberfromothercontroller tokenObject:_numberfromothercontroller];
        }else{
            
            selectedContactName = _namefromothercontroller;
            selectedContactNumber = _numberfromothercontroller;
            
            [_view_tag addTokenWithTitle:_namefromothercontroller tokenObject:_numberfromothercontroller];
        }
    }
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    selectedIndexPath = [[NSMutableArray alloc]init];
    flagDeletedByRowSelection = @"0";
    
}

-(void)viewWillAppear:(BOOL)animated{
    tblArray = [[NSMutableArray alloc] init];
    responseArray = [[NSMutableArray alloc] init];
    self.view_send_message_bottom_constain.constant = 0;
    [UtilsClass view_navigation_title:self title:@"SMS"color:UIColor.whiteColor];
    Mixallcontact = [[NSMutableArray alloc] init];
    [self contact_get];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)Foreground_Action:(NSNotification *)notification
{
    [self contact_get];
    _view_tag.delegate = self;
}
-(void)contact_get
{
    //oldMixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    
    filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *arr = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
    filteredData = (NSMutableArray*)arr;
    
    [self makeSingleArrayForContacts];

    _tableview_number_selection.delegate = self;
    _tableview_number_selection.dataSource = self;
    [_tableview_number_selection reloadData];
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
     [_btn_openContact setSelected:NO];
    [_tableview_number_selection setHidden:true];
    if (textView.textColor == UIColor.lightGrayColor) {
        textView.text = @"";
        textView.textColor = UIColor.blackColor;
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write message";
        textView.textColor = UIColor.lightGrayColor;
        
    }
    
   
}
- (void)textViewDidChange:(UITextView *)textView
{
//    if (_view_tag.tokens.count == 1) {
//
//        NSString *lastentered = [NSString stringWithFormat:@"%c",[textView.text characterAtIndex:textView.text.length-1]] ;
//
//        const char * _char = [lastentered cStringUsingEncoding:NSUTF8StringEncoding];
//
//            int isBackSpace = strcmp(_char, "\b");
//
//            if (isBackSpace == -8) {
//                 NSLog(@"Backspace was pressed");
//            }
//
//    }
    
    if(_txt_send_textview.contentSize.height >= 100)
    {
        _txt_send_textview.scrollEnabled = true;
        self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height;
        _txt_send_textview.frame = CGRectMake(_txt_send_textview.frame.origin.x, _txt_send_textview.frame.origin.y, _txt_send_textview.frame.size.width, 100);
        _view_send_height_constraint.constant = 140;
    }
    else
    {
        _view_send_height_constraint.constant = 60;
        _txt_send_textview.frame = CGRectMake(_txt_send_textview.frame.origin.x, _txt_send_textview.frame.origin.y, _txt_send_textview.frame.size.width, _txt_send_textview.contentSize.height);
        _txt_send_textview.scrollEnabled = false;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == _tableview_number_selection)
    {
        
        
        Tblcell *cell = [_tableview_number_selection dequeueReusableCellWithIdentifier:@"Tblcell"];
        
        
       //p NSDictionary *dic = [filteredData objectAtIndex:indexPath.row];
        
        NSDictionary *dic = [allContactsArray objectAtIndex:indexPath.row];
        
        cell.lblAutherName.text = [dic valueForKey:@"name"];
        
        cell.imgIconeSms.layer.borderWidth=0.0;
//        cell.lblLatestMessage.text =[dic valueForKey:@"number"];
        NSString *name = [dic valueForKey:@"name"];
        name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
  /*p      if ([[dic valueForKey:@"numberArray"] count]>0)
        {
                           
            cell.lblLatestMessage.text =[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
        }*/
        cell.lblLatestMessage.text =[[dic valueForKey:@"numberArray"]  valueForKey:@"number"];
        
       
        
        
        
      /*  if (![UtilsClass isValidNumber:name])
        {
            if (dic[@"_id"])
            {
                if(![[dic valueForKey:@"_id"] isEqualToString:@""])
                {
                    cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                }
                cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
            }
            else
            {
                [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
                cell.imgIconeSms.layer.cornerRadius = cell.imgIconeSms.frame.size.height / 2;
                
                cell.imgIconeSms.layer.cornerRadius=cell.imgIconeSms.frame.size.height / 2;
                cell.imgIconeSms.layer.borderWidth=1.0;
                cell.imgIconeSms.layer.masksToBounds = YES;
                cell.imgIconeSms.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                cell.imgIconeSms.clipsToBounds = YES;
            }
        }
        else
        {
            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
        }*/
        
        if ([cell.lblAutherName.text isEqualToString:selectedContactName] && [cell.lblLatestMessage.text isEqualToString:selectedContactNumber]) {
            
            // selected
            cell.imgIconeSms.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:122.0/255.0 blue:74.0/255.0 alpha:1.0];
            cell.imgIconeSms.image = [UIImage imageNamed:@"next-arrow-white"];
        }
        else
        {
            if (![UtilsClass isValidNumber:name])
            {
                if(dic[@"_id"])
                    {
                        cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                    }
                    else
                    {
                         [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
                    }
                         
                                                    
                        }
                    else
                        {
                                cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                            }
                                              
                    cell.imgIconeSms.layer.cornerRadius = cell.imgIconeSms.frame.size.height / 2;
                                              
                cell.imgIconeSms.layer.cornerRadius=cell.imgIconeSms.frame.size.height / 2;
                    
                    cell.imgIconeSms.layer.borderWidth=1.0;
                          cell.imgIconeSms.layer.masksToBounds = YES;
                    
                          cell.imgIconeSms.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                        cell.imgIconeSms.clipsToBounds = YES;
                                              
                    cell.imgIconeSms.backgroundColor = [UIColor clearColor];
                                    
                      
    
        }
        
        if ([Default boolForKey:kIsNumberMask] == true) {
                    cell.lblLatestMessage.text = [UtilsClass get_masked_number:[[dic valueForKey:@"numberArray"]  valueForKey:@"number"]];
               }
        
        
              /*
               working for single contact
               
               
                if ([selectedIndexPath containsObject:indexPath]) {
    
                      // selected
                      cell.imgIconeSms.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:122.0/255.0 blue:74.0/255.0 alpha:1.0];
                      cell.imgIconeSms.image = [UIImage imageNamed:@"next-arrow-white"];
                      
                      //cell.imgIconeSms.layer.borderWidth=0.0;
                    
                    
                      
                  }
                  else
                  {
                      
                      
                      //unselected
            if (![UtilsClass isValidNumber:name])
            {
                                               
                                                
                 
//                  working for single device contact
//
//                  if (dic[@"_id"])
//                    {
//                        if ([[dic valueForKey:@"numberArray"] count]>0) {
//
//                            cell.lblLatestMessage.text =[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
//                        }
//
//
//                        if(![[dic valueForKey:@"_id"] isEqualToString:@""])
//                        {
//                                cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
//                        }
//                            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
//                    }
//                    else
//                    {
//                            cell.lblLatestMessage.text =[dic valueForKey:@"number"];
//
//
//                            [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
//
//                    }
                

                
                
//                if(![[dic valueForKey:@"_id"] isEqualToString:@""])
                if(dic[@"_id"])
                {
                    cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                }
                else
                {
                     [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
                }
                     
                                                
                    }
                else
                    {
                            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                        }
                                          
                cell.imgIconeSms.layer.cornerRadius = cell.imgIconeSms.frame.size.height / 2;
                                          
            cell.imgIconeSms.layer.cornerRadius=cell.imgIconeSms.frame.size.height / 2;
                
                cell.imgIconeSms.layer.borderWidth=1.0;
                      cell.imgIconeSms.layer.masksToBounds = YES;
                
                      cell.imgIconeSms.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    cell.imgIconeSms.clipsToBounds = YES;
                                          
                cell.imgIconeSms.backgroundColor = [UIColor clearColor];
                                
                  }*/
        
        return cell;
    }
    else
    {
        Tblcell *cell = [_tbl_seletion_number dequeueReusableCellWithIdentifier:@"cell123"];
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        

        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        
        
        
        cell.img_select_con_num.image = [UIImage imageNamed:imagename];
        cell.lbl_select_con_name.text = contactName;
        cell.lbl_select_con_number.text  = phoneNumber;
        _tbl_seletion_number.rowHeight = 64.0;
        [cell layoutIfNeeded];
        [cell.contentView layoutIfNeeded];
        return cell;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableview_number_selection)
    {
        //p return self->filteredData.count;
        //return [self countNumberOfRows];
        return allContactsArray.count;
    }
    else
    {
        return purchase_number.count;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_seletion_number)
    {
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        NSString *imagename = [[numberdic valueForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic valueForKey:@"contactName"];
        NSString *phoneNumber = [numberdic valueForKey:@"phoneNumber"];
        NSString *numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic valueForKey:@"numberVerify"]];
        [Default setValue:numberVeryfy forKey:SELECTED_NUMBER_VERIFY];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
        UIImage *img = [UIImage imageNamed:imagename];
        _numberId = [numberdic valueForKey:@"_id"];
        [Default setValue:_numberId forKey:NUMBERID];
        _selectedNumber = phoneNumber;
        [_btn_number_selection setImage:img forState:UIControlStateNormal];
        [_btn_contry_select setImage:img forState:UIControlStateNormal];
        [_btn_contry_select setTitle:[NSString stringWithFormat:@"+%@",[numberdic objectForKey:@"countryCode"]] forState:UIControlStateNormal];
        [self view_selectnumber_down];
    }
    else
    {
        if([_flag_num_selection isEqualToString:@"1"])
        {
            _flag_num_selection = @"0";
            [self view_selectnumber_down];
        }
        
    
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        
       //p NSDictionary *dic = [filteredData objectAtIndex:indexPath.row];
        NSDictionary *dic = [allContactsArray objectAtIndex:indexPath.row];
        
        //p NSString *selected_number = [dic valueForKey:@"number"];
        NSString *selected_number = [[dic valueForKey:@"numberArray"] valueForKey:@"number"];
        
        NSString *selected_name = [dic valueForKey:@"name"];
        NSArray *arr = [[NSArray alloc] init];
        arr = (NSArray *)[_view_tag tokens];
//        if ([_view_tag.tokens count] < 5)
//        {
        
//        if ([_view_tag.tokens count] < 5)
//        {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title LIKE[c] %@", selected_name];
            NSArray *filteredArray = [arr filteredArrayUsingPredicate:predicate];
//            if ([filteredArray count] > 0)
//            {
//
//            }
//            else
//            {
                selected_name = [selected_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                // add selected image to the seelcted contact
                Tblcell *cell = [_tableview_number_selection cellForRowAtIndexPath:indexPath];
                
                
                
                NSString *name = [dic valueForKey:@"name"];
                name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                UIImage *image = [UIImage imageNamed:@"next-arrow-white"];
        
                //p if ([selectedIndexPath containsObject:indexPath]) {
                 if ([cell.lblAutherName.text isEqualToString:selectedContactName] && [cell.lblLatestMessage.text isEqualToString:selectedContactNumber]) {
                     
                    // selected so revert back
                   
                    //p [selectedIndexPath removeObject:indexPath];
                    
                    selectedContactName = @"";
                    selectedContactNumber = @"";
                    
                    if (![UtilsClass isValidNumber:name])
                    {
                        if (dic[@"_id"])
                        {
                            if(![[dic valueForKey:@"_id"] isEqualToString:@""])
                            {
                                cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                            }
                            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                        }
                        else
                        {
                            [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
//
                        }
                    }
                    else
                    {
                        if (dic[@"_id"]) {
                            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                        }else{
                            cell.imgIconeSms.image = [UIImage imageNamed:cell_user_image];
                        }
                    }
                    
                  
                    cell.imgIconeSms.layer.cornerRadius = cell.imgIconeSms.frame.size.height / 2;
                    
                cell.imgIconeSms.layer.cornerRadius=cell.imgIconeSms.frame.size.height / 2;
                    cell.imgIconeSms.layer.borderWidth=1.0;
                    cell.imgIconeSms.layer.masksToBounds = YES;
                    cell.imgIconeSms.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                    cell.imgIconeSms.clipsToBounds = YES;
                    cell.imgIconeSms.backgroundColor = [UIColor clearColor];
                    
                    flagDeletedByRowSelection = @"1";
                    [_view_tag deleteTokenWithObject:selected_number];
                    flagDeletedByRowSelection = @"0";
                   
                }
                else
                {
                    
                    if ([_view_tag.tokens count] < 1)
                    {
                    
                    // unselected so perform select
                    
                    //p  [selectedIndexPath addObject:indexPath];
                        
                    selectedContactName = cell.lblAutherName.text;
                    selectedContactNumber = cell.lblLatestMessage.text;
                        
                    
                    cell.imgIconeSms.backgroundColor = [UIColor colorWithRed:228.0/255.0 green:122.0/255.0 blue:74.0/255.0 alpha:1.0];
                    cell.imgIconeSms.image = [UIImage imageNamed:@"next-arrow-white"];
                    
                    
                    if ([selected_name isEqualToString:@""]){
                        [_view_tag addTokenWithTitle:selected_number tokenObject:selected_number];
                    }else{
                        [_view_tag addTokenWithTitle:selected_name tokenObject:selected_number];
                    }
                         
                        }
                        else
                        {
                                    [UtilsClass showAlert:@"You can't send message more than 1 recipients at same time." contro:self];
                        }
                }
                
                
                
           //b  }
            
            
            
            
          
            
            
            
            
//        }
//        else
//        {
//            [UtilsClass showAlert:@"You can't send message more than 5 recipients at same time." contro:self];
//        }
    }
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(BOOL)tokenView:(KSTokenView *)tokenView shouldAddToken:(KSToken *)token
{
    CGFloat height = 25.0 + _view_tag.frame.size.height;
    _tag_view_constraint.constant = height;
    if ([token.title isEqualToString:@"f"]) {
        return NO;
    }
    return YES;
}

-(void)tokenView:(KSTokenView *)tokenView performSearchWithString:(NSString *)string completion:(void (^)(NSArray * _Nonnull))completion
{
    
    
    if (tokenView.tokens.count == 0 ){
    
        if([string isEqualToString:@""]){
            if (tokenView.tokens.count == 0 ){
                [_btn_openContact setSelected:NO];
                 [_tableview_number_selection setHidden:true];
            }
            
            
            /*p filteredData = [[NSArray alloc] init];
            filteredData = Mixallcontact;
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES];
            filteredData = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor]];
            [_tableview_number_selection reloadData];*/
            
            allContactsArray = [[NSArray alloc] init];
            allContactsArray = allContactsArrayOriginal;
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES];
            allContactsArray = [allContactsArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            [_tableview_number_selection reloadData];
            
        }else{
            [_tableview_number_selection setHidden:false];
            [self search_number:string];
            
        }
    }
    
}
- (NSString *)tokenView:(KSTokenView *)tokenView displayTitleForObject:(id)object {
    NSString *string = [NSString stringWithFormat:@"%@",object];
    return string;
}
-(void)tokenViewDidShowSearchResults:(KSTokenView *)tokenView {
}
- (void)tokenView:(KSTokenView *)tokenView willAddToken:(KSToken *)token {
//    filteredData = [[NSArray alloc] init];
//    filteredData = [[NSArray alloc] init];
//    filteredData = Mixallcontact;
//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
//                                                 ascending:YES];
//    filteredData = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor]];
//    _tableview_number_selection.hidden = false;
//      [_tableview_number_selection reloadData];
    

    if(_view_tag.tokens.count == 1)
    {
        [UtilsClass showAlert:@"You can't send message more than 1 recipients at same time." contro:self];

    }
}

- (void)tokenView:(KSTokenView *)tokenView didAddToken:(KSToken *)token {
    //NSLog(@"didAddToken %@", token);
    if(_view_tag.tokens.count > 1)
    {
         [_view_tag deleteToken:token];
    }

   /* else
    {
        if ([Default boolForKey:kIsNumberMask] == true) {
           token.title =  [UtilsClass get_masked_number:[token title]];
        }
       
    }*/
    
    
}

- (void)tokenView:(KSTokenView *)tokenView didFailToAdd:(KSToken *)token {
    //NSLog(@"didFailToAdd %@", token);
}

- (BOOL)tokenView:(KSTokenView *)tokenView shouldDeleteToken:(KSToken *)token {
    return YES;
}

- (void)tokenView:(KSTokenView *)tokenView willDeleteToken:(KSToken *)token {
    //NSLog(@"willDeleteToken %@", token);
}

- (void)tokenView:(KSTokenView *)tokenView didDeleteToken:(KSToken *)token {
    CGFloat height = 20.0 + _view_tag.frame.size.height;
    _tag_view_constraint.constant = height;
    
    if ([flagDeletedByRowSelection isEqualToString:@"0"]) {

       
        
      //p  if(_view_tag.tokens.count < selectedIndexPath.count)
        if(_view_tag.tokens.count < 1)
        {
          //  [selectedIndexPath removeLastObject];
            selectedContactNumber = @"";
            selectedContactName = @"";
            
            [_tableview_number_selection reloadData];
        }
        
    }
//p    if (selectedIndexPath.count == 0) {
    if ([selectedContactName isEqualToString:@""]) {
        
         [_view_tag._tokenField resignFirstResponder];
    }
    
}

- (void)tokenView:(KSTokenView *)tokenView didFailToDeleteToken:(KSToken *)token {
    //NSLog(@"didFailToDeleteToken %@", token);
}

- (void)tokenView:(KSTokenView *)tokenView willChangeFrame:(CGRect)frame {
    //NSLog(@"willChangeFrame %@", NSStringFromCGRect(frame));
}

- (void)tokenView:(KSTokenView *)tokenView didChangeFrame:(CGRect)frame {
    //NSLog(@"didChangeFrame %@", NSStringFromCGRect(frame));
}

- (void)tokenView:(KSTokenView *)tokenView didSelectToken:(KSToken *)token {
    //NSLog(@"didSelectToken %@", token);
}

- (void)tokenViewDidBeginEditing:(KSTokenView *)tokenView {
    //NSLog(@"tokenViewDidBeginEditing %@", tokenView);
    //    [self search_number:tokenView.text];
    
}

- (void)tokenViewDidEndEditing:(KSTokenView *)tokenView
{
    //NSLog(@"endediting");
   

}

- (IBAction)btn_number_selection_click:(id)sender
{
    if([_flag_num_selection isEqualToString:@"0"])
    {
        [_txt_send_textview resignFirstResponder];
        [_view_tag._tokenField resignFirstResponder];
        [self view_selectnumber_up];
    }
    else
    {
        [self view_selectnumber_down];
    }
}
- (IBAction)btn_send_message_click:(UIButton *)sender
{
   //pri NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        NSString *numbVer = [Default valueForKey:ISDummyNumber];
        int num = [numbVer intValue];
        if (num == 1)
        {
        NSString *numbVer = [Default valueForKey:SELECTED_NUMBER_VERIFY];
        int num = [numbVer intValue];
        if (num == 1) {
            NSString *credit = [Default valueForKey:CREDIT];
            float cre = [credit floatValue];
            if(cre > 0 ){
               
                if ([_txt_send_textview.text isEqualToString:@""] || [_txt_send_textview.text isEqualToString:@"Write message"]){
                [UtilsClass showAlert:@"You can't send empty message." contro:self];
                }
                else
                {
                    if(_view_tag.tokens.count == 1)
                    {
                         [self sendSms];
                    }
                    else
                    {
                        [UtilsClass showAlert:@"You can't send message more than 1 recipients at same time." contro:self];
                    }
                }
             /*   if(_view_tag.tokens.count == 1)
                {
                    if ([_txt_send_textview.text isEqualToString:@""] || [_txt_send_textview.text isEqualToString:@"Write message"]){
                        [UtilsClass showAlert:@"You can't send empty message." contro:self];
                    }else{
                        [self sendSms];
                    }
                }
                else
                {
                        [UtilsClass showAlert:@"You can't send message more than 1 recipients at same time." contro:self];
                }
                */
                
            }else {
                [UtilsClass showAlert:kCREDITLAW contro:self];
            }
        }else{
            [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
        }}
        else
        {
            [UtilsClass showAlert:kDUMMYNUMBERMSG contro:self];
        }}
    else{
        [UtilsClass showAlert:kNUMBERASSIGN contro:self];}
}
-(void)save_contact:(NSArray *)contactarr
{
    for (int i = 0 ; i < contactarr.count ; i++)
    {
        
        NSString *num = contactarr[i];
        NSString *string = [num stringByReplacingOccurrencesOfString:@"(" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        //p NSPredicate *filter = [NSPredicate predicateWithFormat:@"number_int contains[c] %@ ",string];
        
        //-mixallcontact
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",num] ;
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",num] ;
        
        NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];
        
        NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
        
        if(filteredContacts.count != 0)
        {
            NSDictionary *dic = [filteredContacts objectAtIndex:0];
            if(!dic[@"_id"])
            {
                NSString *ContactName = [dic valueForKey:@"name"];
                //p NSString *ContactNumber = [dic valueForKey:@"number"];
                NSString *ContactNumber = [[dic valueForKey:@"numberArray"] valueForKey:@"number"];

                [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
            }
            else
            {
            }
        }
    }
}
-(void)sendSms {
    
    NSArray *arr = [_view_tag tokens];
    NSArray *arr1 = [_view_tag objects];
    NSString *num;
     NSLog(@"chnumber code :%@",_btn_contry_select.titleLabel.text);
    if (arr1.count > 0){
        for (int i = 0 ; i < arr1.count ; i++) {
            
            NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
            txtText.text = @"";
            [txtText insertText:arr1[i]];
            NSString *code = txtText.code ? txtText.code : @"";
            
            if([[NSString stringWithFormat:@"+%@",code] isEqualToString:_btn_contry_select.titleLabel.text]){
               _strNumber = [_strNumber stringByAppendingString:[NSString stringWithFormat:@"%@%@",arr1[i],@"<"]];
            }
            else{
                _strNumber = [_strNumber stringByAppendingString:[NSString stringWithFormat:@"%@%@%@",_btn_contry_select.titleLabel.text,arr1[i],@"<"]];
            }
            
        }
        _strNumber = [_strNumber substringToIndex:[_strNumber length] - 1];
    }else{
        [UtilsClass showAlert:@"Atleast one contact is required to send SMS." contro:self];
        return;
    }
    _strNumber = [_strNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    _strTypNumber = [_strTypNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    _strTypNumber = [_strTypNumber stringByReplacingOccurrencesOfString:@"," withString:@"<"];
    if ([_strTypNumber isEqualToString:@""]) {
        num = _strNumber;
    } else {
        if (_strNumber.length > 0){
            num = [NSString stringWithFormat:@"%@<%@",_strNumber,_strTypNumber] ;
        } else {
            num = [NSString stringWithFormat:@"%@",_strTypNumber] ;
        }
        
    }
    //NSString *authToken = [Default valueForKey:AUTH_TOKEN];
    NSString *userId = [Default valueForKey:USER_ID];
    //NSString *fromNo = [Default valueForKey:SELECTEDNO];
    NSString *smsAuther = [Default valueForKey:FULL_NAME];
    NSString *url = [NSString stringWithFormat:@"smssend/%@",userId];
    NSString *Numberid = [Default valueForKey:NUMBERID];
    num = [num stringByReplacingOccurrencesOfString:@"(" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
    Group_ToNum = num;
    NSDictionary *passDict = @{@"from":_selectedNumber,
                               @"to":num,
                               @"numberId":Numberid,
                               @"smsBody":_txt_send_textview.text,
                               @"smsAuther":smsAuther};
    NSLog(@"Dictonary : %@",passDict);
    NSLog(@"auth token : %@",[Default valueForKey:AUTH_TOKEN]);
    
    num = @"";
//    _strNumber = @"";
    arr = [[NSArray alloc] init];
    arr1 = [[NSArray alloc] init];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login1:response:) andDelegate:self];
}
- (void)login1:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    
    
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : send sms : %@",response1);

        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            //show success message
            [UtilsClass makeToast:kMESSAGESENTSUCCESSMSG];
            
            NSArray *arr = [_view_tag tokens];
            NSArray *arr1 = [_view_tag objects];
            if (arr1.count == 1)
            {
                NSString *threadId = [response1 valueForKey:@"data"][@"threadId"];
                NSString *num;
                num = @"";
                num = _strNumber;
                _strNumber = @"";
                num = [num stringByReplacingOccurrencesOfString:@"(" withString:@""];
                num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
                num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
                num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
                num = [num stringByReplacingOccurrencesOfString:@"<" withString:@""];
                num = [NSString stringWithFormat:@"+%@",num];
                smsFromNumber = _selectedNumber;
                smstoNumber = num;
                NSString *Numberid = [Default valueForKey:NUMBERID];
                chnumberID = Numberid;
                [self getChat];
            }
            else
            {
                _strNumber = @"";
                [respArray setValue:[arr componentsJoinedByString:@","] forKey:@"threadName"];
                [respArray setValue:smstoNumber forKey:@"threadNumber"];
                [respArray setValue:smsFromNumber forKey:@"chNumber"];
                [respArray setValue:@"" forKey:@"contactEmail"];
                [respArray setValue:@"" forKey:@"contactCompany"];

                NSMutableDictionary *sendDic1 = [[NSMutableDictionary alloc] init];
                [sendDic1 setValue:[arr componentsJoinedByString:@","] forKey:@"threadName"];
                [sendDic1 setValue:smstoNumber forKey:@"threadNumber"];
                [sendDic1 setValue:smsFromNumber forKey:@"chNumber"];
                [sendDic1 setValue:@"" forKey:@"contactEmail"];
                [sendDic1 setValue:@"" forKey:@"contactCompany"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
                NSString *stringDate = [dateFormatter stringFromDate:[NSDate date]];
                NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] init];
                NSMutableArray *sendDicarray = [[NSMutableArray alloc] init];
                [sendDic setValue:@"read" forKey:@"readStatus"];
                [sendDic setValue:[Default valueForKey:FULL_NAME] forKey:@"smsAuther"];
                [sendDic setValue:_txt_send_textview.text forKey:@"smsContent"];
                [sendDic setValue:stringDate forKey:@"smsTime"];
                [sendDic setValue:@"" forKey:@"smsUUID"];
                [sendDicarray addObject:sendDic];
                [sendDic1 setValue:sendDicarray forKey:@"messages"];
                
                
                NSLog(@" CALLHIPPO : %@",sendDic1);
 
                ChatViewControllerNew *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
                controller.ThreadMessageDic = sendDic1;
                controller.titleString = [arr componentsJoinedByString:@","];
                
                
                controller.VcFrom = @"SmsListNew";
                controller.VcFrom_group = @"GroupSMS";
                controller.number = Group_ToNum;
                controller.frmNumber = _selectedNumber;
                controller.contactId = @"";
                
                [self.navigationController pushViewController:controller animated:YES];
                
            }
            [self save_contact:arr1];
            
            
        }
        else {
            _strNumber = @"";
            if(response1 != nil)
            {
                @try {
                    if (response1 != (id)[NSNull null] && response1 != nil ){
                        [UtilsClass makeToast:[response1 valueForKey:@"error"][@"error"]];
                    }
                }
                @catch (NSException *exception) {
                }
            }
        }
    }
}
-(void)getChat {

    if([UtilsClass isNetworkAvailable]) {
        NSString *url = [NSString stringWithFormat:@"getUserSmsDetails"];
        NSDictionary *passDict = @{@"user":@"",
                                   @"contact":@"",
                                   @"from":smsFromNumber,
                                   @"to":smstoNumber,
                                   @"chNumberId":chnumberID};
        [Default setValue:chnumberID forKey:NUMBERID];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getmessage:response:) andDelegate:self];
    } else {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

- (void)getmessage:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        NSLog(@"Encrypted Response : get chat : %@",response1);

        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            respArray = response1[@"data"];
            [tblArray addObject:respArray];
            ChatViewControllerNew *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
            controller.ThreadMessageDic = response1[@"data"];
            controller.titleString = [respArray valueForKey:@"threadName"];
            controller.number = [respArray valueForKey:@"threadNumber"];
            controller.frmNumber = [respArray valueForKey:@"chNumber"];
            controller.contactId = [respArray valueForKey:@"contactId"];
            controller.threadId = [respArray valueForKey:@"threadId"];
            if([_VcFrom isEqualToString:@"ContactDetailVC"])
            {
                controller.VcFrom = @"ContactDetailVC";
            }else if([_VcFrom isEqualToString:@"DialerVC"]){
                controller.VcFrom = @"DialerVC";
            }
            else
            {
                controller.VcFrom = @"SmsListNew";
            }
            controller.tblContactArray = respArray;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else
        {
            
        }
        
    }
}
#pragma mark - CallHippo Contact
-(void)getCallHippoContactList
{
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"%@/contact",userId];
    obj = [[WebApiController alloc] init];
    
    NSLog(@"URL Contact check : %@",aStrUrl);

    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getCallHippoContactListresponse:response:) andDelegate:self];
}
- (void)getCallHippoContactListresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            arrcontact = [response1 valueForKey:@"data"];
            [Mixallcontact addObjectsFromArray:arrcontact];
            filteredData = [[NSArray alloc] init];
            filteredData = [[NSArray alloc] initWithArray:Mixallcontact];
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES selector:@selector(caseInsensitiveCompare:)];
            filteredData = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
            _tableview_number_selection.delegate = self;
            _tableview_number_selection.dataSource = self;
            [_tableview_number_selection reloadData];
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
-(void)search_number:(NSString*)text
{
    text = [text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@")" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    //p filteredData = [Mixallcontact filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(number_int contains[c] %@)",text]];
    
    allContactsArray = [allContactsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"numberArray.number contains[c] %@",text]];
    
    //p if(filteredData.count != 0)
    if(allContactsArray.count != 0)
    {
        [_tableview_number_selection reloadData];
    }
    else
    {
         [_tableview_number_selection reloadData];
    }
}

- (IBAction)btn_menu_click:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)keyboardWillShow:(NSNotification *)notification
{
    NSLog(@"Call 1.2");
    keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    if([_flag_num_selection isEqualToString:@"1"])
    {
        [self view_selectnumber_down];
    }
    else
    {
        keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        _tableview_number_selection.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        [_tableview_number_selection layoutIfNeeded];
    }
    [self movetoolbar:true notification:notification];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(void)keyboardWillHide:(NSNotification *)notification
{
    NSLog(@"Call 1.1");
    
   /*p filteredData = [[NSArray alloc] init];
    filteredData = Mixallcontact;
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES];
    filteredData = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor]];
    [_tableview_number_selection reloadData];*/
    
    allContactsArray = [[NSArray alloc] init];
    allContactsArray = allContactsArrayOriginal;
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES];
    allContactsArray = [allContactsArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    [_tableview_number_selection reloadData];
    
    keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    if([_flag_num_selection isEqualToString:@"1"])
    {
        [self view_selectnumber_down];
    }
    else
    {
        keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        _tableview_number_selection.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        [_tableview_number_selection layoutIfNeeded];
    }
    [self movetoolbar:false notification:notification];
    
}

-(void) view_selectnumber_up {
    
    
    if ([self.flag_num_selection isEqual : @"0" ])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height-64;
        Blur_view = [[UIView alloc] init];
        Blur_view.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
        Blur_view.backgroundColor = UIColor.blackColor;
        Blur_view.alpha = 0.7;
        [self.view insertSubview:Blur_view belowSubview:_view_number_selection];
        
        
        self.flag_num_selection = @"1";
        [self updateViewConstraints];
        self->_view_nmbr_selection_bottomConstraint.constant = 0;
       
    }
}
-(void)updateViewConstraints
{
    
    if ([self.flag_num_selection isEqual : @"1"])
    {
        _tbl_nmbr_selection_heightConstraint.constant = _tbl_seletion_number.contentSize.height ;
       
    }
    else
    {
        _tbl_nmbr_selection_heightConstraint.constant = 0;
    }
     [super updateViewConstraints];
}
-(void) view_selectnumber_down
{
    if ([self.flag_num_selection isEqual : @"1" ])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        [Blur_view removeFromSuperview];
//
        
        self.flag_num_selection = @"0";
        [self updateViewConstraints];
        self->_view_nmbr_selection_bottomConstraint.constant = 100;
    }
    
     
       
}

-(void)movetoolbar:(BOOL)up notification:(NSNotification *)notification
{
    NSLog(@"Call 1");
    if(up == true)
    {
        NSDictionary* userInfo = [notification userInfo];
        keyboardSizeNew = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat bottomPadding = window.safeAreaInsets.bottom;
            
            if (bottomPadding < 1.0)
            {
                self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height;
                [self.view layoutIfNeeded]; // Called on parent view
            }
            else
            {
                self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height - 30;
                [self.view layoutIfNeeded]; // Called on parent view
            }
            
        }
        else
        {
            self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height;
            [self.view layoutIfNeeded]; // Called on parent view
        }
    }
    else
    {
        self.view_send_message_bottom_constain.constant = 0;
    }
}
-(void)getNumbers {
    
    purchase_number = [[NSArray alloc]init];
    //pri purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        
        NSDictionary *dic1 = [purchase_number objectAtIndex:0];
        NSDictionary *numberdic = [dic1 objectForKey:@"number"];
        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        _selectedNumber = phoneNumber;
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        UIImage *img = [UIImage imageNamed:imagename];
        [_btn_contry_select setImage:img forState:UIControlStateNormal];
        [_btn_number_selection setImage:img forState:UIControlStateNormal];
        [_btn_contry_select setTitle:[NSString stringWithFormat:@"+%@",[numberdic objectForKey:@"countryCode"]] forState:UIControlStateNormal];
        [Default setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
        _numberId = [numberdic valueForKey:@"_id"];
        if ([_strFromNumber isEqualToString:@""]){
        }else{
            NSString *imagename = [_strFlag lowercaseString];
            UIImage *img = [UIImage imageNamed:imagename];
            [_btn_contry_select setImage:img forState:UIControlStateNormal];
            [_btn_number_selection setImage:img forState:UIControlStateNormal];
            [_btn_contry_select setTitle:_strCountryCode forState:UIControlStateNormal];
            _selectedNumber = _strFromNumber;
            _strFromNumber = @"";
        }
        _tbl_seletion_number.delegate = self;
        _tbl_seletion_number.dataSource = self;
        [_tbl_seletion_number reloadData];
    }
    
}
- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            NSDictionary*dic = [response1 valueForKey:@"data"];
            purchase_number = [dic valueForKey:@"numbers"];
            NSDictionary *dic1 = [purchase_number objectAtIndex:0];
            NSDictionary *numberdic = [dic1 objectForKey:@"number"];
            NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
            NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
            UIImage *img = [UIImage imageNamed:imagename];
            [_btn_contry_select setImage:img forState:UIControlStateNormal];
            [_btn_number_selection setImage:img forState:UIControlStateNormal];
            [_btn_contry_select setTitle:[NSString stringWithFormat:@"+%@",[numberdic objectForKey:@"countryCode"]] forState:UIControlStateNormal];
            [Default setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
            _numberId = [numberdic valueForKey:@"_id"];
            _selectedNumber = phoneNumber;
            _tbl_seletion_number.delegate = self;
            _tbl_seletion_number.dataSource = self;
            [_tbl_seletion_number reloadData];
            
        }
        else {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
- (IBAction)btn_conty_select_from_tbl:(UIButton *)sender
{
    countryListVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"countryListVC"];
    vc.view.backgroundColor = [UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:0.5];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    if (IS_IPAD) {
        vc.popoverPresentationController.sourceView = self.view;
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:true completion:nil];
    }
}
-(void) receiveCountry:(NSNotification *) notification
{
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_view_tag tagcreate];
    [textField resignFirstResponder];
    [[_view_tag _tokenField] resignFirstResponder];
}
 -(int)countNumberOfRows
{
    int count = 0;
    
    for (int i =0; i<filteredData.count; i++) {
        
        
        for (int j =0; j < [[[filteredData objectAtIndex:i] valueForKey:@"numberArray"] count]; j++) {
            
            count++;
        }
        
    }
    return count;
}

-(void)makeSingleArrayForContacts
{
    allContactsArray = [[NSArray alloc]init];
     allContactsArrayOriginal = [[NSMutableArray alloc]init];
    
    for (int i =0; i<filteredData.count; i++) {
        
//        NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
//
//
//        temp = [filteredData objectAtIndex:i];
        
       
       // NSLog(@"count total j :: %lu",(unsigned long)[[[filteredData objectAtIndex:i] valueForKey:@"numberArray"] count]);
        
        for (int j =0; j<[[[filteredData objectAtIndex:i] objectForKey:@"numberArray"] count]; j++) {
            
           // NSLog(@"i :: %d\nj :: %d",i,j);

            NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:[filteredData objectAtIndex:i]];
            
            
            //temp = [filteredData objectAtIndex:i];
            
             //NSLog(@"temp :: %@",temp);
          

            
            NSDictionary *number = [[[filteredData objectAtIndex:i] valueForKey:@"numberArray"] objectAtIndex:j];
            
            //NSLog(@"number :: %@",number);

            [temp setObject:number forKey:@"numberArray"];
            
            //NSLog(@"temp after :: %@",temp);

            
            [allContactsArrayOriginal addObject:temp];
            
              //NSLog(@"filter data obj  :: %@",[filteredData objectAtIndex:i]);
            
        }
    }
    
    
    allContactsArray = allContactsArrayOriginal;
    
//    NSLog(@"all contcts:: %@",allContactsArray);
    
}

#pragma mark - Phone Contact
-(void)GetNumberFromContact
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
    }
    else
    {
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey, CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, CNContactViewController.descriptorForRequiredKeys, nil];
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
            NSString *phoneNumber = @"";
            if( contact.phoneNumbers)
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
            NSString *FullName = [NSString stringWithFormat:@"%@%@",contact.givenName,contact.familyName];
            NSString *Number = [NSString stringWithFormat:@"%@",phoneNumber];
            if([Number isEqualToString:@"(null)"])
            {
                Number = @"";
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:FullName forKey:@"name"];
            [dic setObject:Number forKey:@"number"];
            [self->arrContactList addObject:contact];
            [self->Mixallcontact addObject:dic];
        }];
        _tableview_number_selection.delegate = self;
        _tableview_number_selection.dataSource = self;
        [_tableview_number_selection reloadData];
    }
}
- (IBAction)openContact:(id)sender {
    if ([sender isSelected]) {
        [sender setSelected: NO];
        [_tableview_number_selection setHidden:true];
    } else {
        [sender setSelected: YES];
        [_tableview_number_selection setHidden:false];
         
        
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:true];
    [self view_selectnumber_down];
}

-(NSDate *)getdateString : (NSString*)strDate {
    NSArray *strArray = [strDate componentsSeparatedByString:@" "];
    NSString *finalStr = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",strArray[0],strArray[1],strArray[2],strArray[3],strArray[4]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *date = [dateFormatter dateFromString:finalStr];
    return date;
}


- (IBAction)btn_right_menu_search:(UIBarButtonItem *)sender{
    
    
    _tableview_number_selection.hidden = false;
    
    
    if ([searchFlag isEqual: @"0"])
    {
        if(filteredData.count != 0)
        {
            [_tableview_number_selection scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                    atScrollPosition:UITableViewScrollPositionTop animated:NO];
            searchFlag = @"1";
            
            searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
            searchController.searchResultsUpdater = self;
            
            
            searchController.hidesNavigationBarDuringPresentation = true;
            searchController.dimsBackgroundDuringPresentation = false;
           // _tableview_number_selection.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
            
            searchController.delegate = self;
            searchController.searchBar.delegate = self;
            
            [searchController.searchBar sizeToFit];
            _tableview_number_selection.tableHeaderView = searchController.searchBar;
            
            
            self.definesPresentationContext = true;
            [searchController setActive:YES];
            
            
             _tableview_number_selection.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
         //   [searchController.searchBar becomeFirstResponder];
            
        }
    }else {
        if(filteredData.count != 0)
        {
            searchFlag = @"0";
            _tableview_number_selection.tableHeaderView = nil;
            [_tableview_number_selection reloadData];
        }
    }
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSLog(@"update search %@",searchController.searchBar.text);
    
    if(![searchController.searchBar.text isEqualToString:@""])
    {
        [self searchNumberwithName];
    }
    else
    {
        NSLog(@"else");
        [self contact_get];
    }
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _tableview_number_selection.tableHeaderView = nil;
    allContactsArray = [[NSMutableArray alloc] init];
    allContactsArray = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    searchFlag = @"0";
    _tableview_number_selection.tableHeaderView = nil;
    [_tableview_number_selection reloadData];
}

- (void)didPresentSearchController:(UISearchController *)searchController
{
    [searchController.searchBar becomeFirstResponder];
}

-(void)searchNumberwithName
{
    NSString *num = [searchController.searchBar.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@")" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"-" withString:@""];
    num = [num stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSPredicate *predicate_name = [NSPredicate predicateWithFormat:@"name contains[c] %@",searchController.searchBar.text];
    
    
    //NSPredicate *predicate_number = [NSPredicate predicateWithFormat:@"number_int contains[c] %@",num];
    
    NSPredicate *predicate_number2 = [NSPredicate predicateWithFormat:@" numberArray.number contains[c] %@",num];
    
    NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate_name, predicate_number2]];
    
    
    //    NSLog(@"MixContact : %@",Mixallcontact);
    NSArray *_filteredData1 = [allContactsArray filteredArrayUsingPredicate:predicate_final];
    //      ////NSLog(@"_filteredData : %@",_filteredData1);
    if(_filteredData1.count != 0)
    {
        allContactsArray = [[NSMutableArray alloc] init];
        allContactsArray = (NSMutableArray*)_filteredData1;
        [_tableview_number_selection reloadData];
    }
    else
    {
        if ([searchController.searchBar.text isEqualToString:@""]) {
            allContactsArray = [[NSMutableArray alloc] init];
            allContactsArray = Mixallcontact;
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES];
            
            NSArray *arr = [allContactsArray sortedArrayUsingDescriptors:@[sortDescriptor] ];
            allContactsArray = (NSMutableArray*)arr;
            [_tableview_number_selection reloadData];
        } else {
            allContactsArray = [[NSMutableArray alloc] init];
            [_tableview_number_selection reloadData];
        }
        
    }
    
    
}

- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        return [predicate evaluateWithObject:string];
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    
}
@end
