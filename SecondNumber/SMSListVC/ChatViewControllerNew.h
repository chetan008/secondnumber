//
//  ChatViewControllerNew.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN
@protocol contactupdate <NSObject>

@optional
- (void)secDismisWithData:(NSMutableDictionary *)data;
@end
@interface ChatViewControllerNew : UIViewController
{
    NSMutableArray *Mixallcontact;
}

@property (strong, nonatomic) NSMutableDictionary *ThreadMessageDic;
@property (strong, nonatomic) NSMutableArray *MessageArray;
@property (strong, nonatomic) NSMutableArray *MessageArraySection;
@property (strong, nonatomic) NSMutableArray *MessageArrayRow;
@property (strong, nonatomic) NSString *VcFrom;
@property (strong, nonatomic) NSString *titleString;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *nav_right_call;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nav_right_menu;









@property (strong, nonatomic) IBOutlet UIImageView *imgAddEditContact;
@property (nonatomic, strong)   id<contactupdate> delegate;
//@property (strong, nonatomic) NSString *VcFrom;
@property (strong, nonatomic) NSString *VcFrom_group;
@property (strong, nonatomic) NSString *VcFrom1;
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *frmNumber;
@property (strong, nonatomic) NSString *contactId;
@property (strong, nonatomic) NSString *threadId;
@property (strong, nonatomic) NSMutableDictionary *tblContactArray;
@property (strong, nonatomic) NSMutableDictionary *chat_dic;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//toolbar

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_send_message_bottom_constain;
@property (weak, nonatomic) IBOutlet UITextView *txt_send_textview;
@property (weak, nonatomic) IBOutlet UIView *view_send_bottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_send_height_constraint;

@property (strong, nonatomic) IBOutlet UIView *view_right_menu;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *nav_right_menu;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *nav_right_call;

@property (strong, nonatomic) IBOutlet UIButton *btn_contact_add_edit;

//@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) NSString *Caller_Name;
@property (strong, nonatomic) NSString *Caller_Number;
@property (strong, nonatomic) NSString *My_Number;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btn_back;
@property (strong, nonatomic) NSString *My_Department;

@property (strong, nonatomic) IBOutlet TooltipView *numToolTip;
@property (strong, nonatomic) IBOutlet UILabel *toolTipNumLbl;
@end

NS_ASSUME_NONNULL_END
