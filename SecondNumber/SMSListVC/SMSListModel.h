//
//  SMSListModel.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SMSListModel : NSObject

@property (retain, nonatomic) NSMutableArray *SMSListArray;

+ (SMSListModel *)sharedSMSListData;

-(NSInteger)numberOfSMSINArray;
-(void)removeAllObjectsFromSMSArray;
-(void)addObjectsAtTop:(NSMutableArray *)array;
-(void)addObjectsAtBottom:(NSMutableArray *)array;

@end

NS_ASSUME_NONNULL_END
