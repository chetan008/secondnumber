//
//  NewsmsVC.h
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import <UIKit/UIKit.h>
#import "SecondNumber-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsmsVC : UIViewController
{
    NSMutableArray *arrContactList;
    NSMutableArray *arrcontact;
    NSMutableArray *Mixallcontact;
    
}
@property (weak, nonatomic) IBOutlet UIView *view_tag_view;
//let names: Array<String> = Array<String>()
@property NSString *strNumber;
@property NSString *strTypNumber;
@property NSString *selectedNumber;
@property NSString *numberId;

@property NSString *strFromNumber;
@property NSString *strFlag;
@property NSString *strCountryCode;

@property (strong, nonatomic) NSString *VcFrom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tag_view_constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_send_message_bottom_constain;
@property (weak, nonatomic) IBOutlet UITableView *tableview_number_selection;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *view_nmbr_selection_bottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tbl_nmbr_selection_heightConstraint;


@property (strong, nonatomic) NSMutableArray *filterarrcontact;
@property (weak, nonatomic) IBOutlet UIView *view_number_selection;
@property (weak, nonatomic) IBOutlet UITableView *tbl_seletion_number;
@property (strong, nonatomic) NSMutableArray *arrNumbers;
@property (nonatomic) _Bool numberVerifye;
@property (strong, nonatomic) NSString *flag_num_selection;
@property (weak, nonatomic) IBOutlet UITextView *txt_send_textview;
@property (weak, nonatomic) IBOutlet UIButton *btn_number_selection;
@property (weak, nonatomic) IBOutlet UIButton *btn_contry_select;

@property (strong, nonatomic) NSString *controller;
@property (strong, nonatomic) NSString *numberfromothercontroller;
@property (strong, nonatomic) NSString *namefromothercontroller;
@property (weak, nonatomic) IBOutlet UIView *view_send_bottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_send_height_constraint;

@property (weak, nonatomic) IBOutlet UIButton *btn_openContact;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tbl_contact_yConstraint;
//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tbl_contact_heightConstraint;


@end

NS_ASSUME_NONNULL_END
