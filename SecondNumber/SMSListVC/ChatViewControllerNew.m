//
//  ChatViewControllerNew.m
//  SecondNumber
//
//  Created by Apple on 01/12/21.
//

#import "ChatViewControllerNew.h"
#import "GlobalData.h"
#import "Tblcell.h"
#import "IQKeyboardManager.h"
#import "AddEditContactVC.h"
#import "UtilsClass.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "SmsListNew.h"
#import <AVFoundation/AVFoundation.h>
#import "UIViewController+LGSideMenuController.h"
@interface ChatViewControllerNew ()<UITableViewDataSource, UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>
{
    WebApiController *obj;
    long DefultValue;
    long sendSmsIndex;
    long receiveSmsIndex;
    long sendSmsSection;
    long receiveSmsSection;
    CGFloat keyboardSize;
    CGSize keyboardSizeNew;
    NSString *SelectedMenu;
    NSArray *purchase_number;
    NSMutableArray *SelectedIndex;
    NSMutableArray *SelectedsmsUUID;
    NSIndexPath *SelectedIndexPath;
    NSString *RighMenuEnable;
}
@end
NSString *btnclick_flag_chat = @"0";
@implementation ChatViewControllerNew
@synthesize ThreadMessageDic;
@synthesize MessageArray;
@synthesize MessageArraySection;
@synthesize MessageArrayRow;
@synthesize Caller_Name,Caller_Number,My_Number,My_Department,titleString;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"ThreadID :: %@",_threadId);
    [Default setValue:@"1" forKey:IS_ChatviewDisplayed];
    [Default setValue:_threadId forKey:IS_ChatviewDisplayed_ThredID];
    
    
    [[NSNotificationCenter defaultCenter]
    addObserver:self selector:@selector(Conversation_update:) name:@"Conversation_update" object:nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    [Default setValue:@"1" forKey:IS_ChatviewDisplayed];
    NSArray *namearray = [titleString componentsSeparatedByString:@","];
 /*   if(namearray.count == 1)
    {
        self.title = titleString;
    }
    else
    {
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor blackColor],
           NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]}];
        self.title = titleString;
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor blackColor],
           NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]}];
    }*/
    
    //navigation title with department name
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *imagename;
    
    if(purchase_number.count > 0)
    {
        NSString *preNumStr = [_tblContactArray objectForKey:@"chNumber"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",preNumStr];
        
        NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
        
        if (results.count>0) {
           
            
            imagename = [[[[results objectAtIndex:0] objectForKey:@"number"]  objectForKey:@"shortName"] lowercaseString];
        }
           
         
        
    }

    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 140, 44)];
   //[view setBackgroundColor:[UIColor yellowColor]];
    
    UILabel *fromNumLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 140, 25)];
    fromNumLbl.backgroundColor = [UIColor clearColor];
    fromNumLbl.font = [UIFont boldSystemFontOfSize: 18.0f];
    fromNumLbl.textColor = [UIColor blackColor];
    //label.textAlignment = UIListContentTextAlignmentCenter;

    fromNumLbl.text = titleString;
    
    
    UIImage *img = [UIImage imageNamed:imagename];
    UIImageView *imgvw = [[UIImageView alloc]initWithFrame:CGRectMake(0, 25, 20, 20)];
    imgvw.image = img;
    
    
    
    UILabel *deptnameLbl = [[UILabel alloc] initWithFrame:CGRectMake(25, 25, self.view.frame.size.width - 170, 16)];
    deptnameLbl.backgroundColor = [UIColor clearColor];
    deptnameLbl.font = [UIFont boldSystemFontOfSize: 17.0f];
    deptnameLbl.textColor = [UIColor lightGrayColor];
    //label2.textAlignment = UITextAlignmentCenter;
    deptnameLbl.text = [_tblContactArray objectForKey:@"chNumberName"];
    
    deptnameLbl.lineBreakMode = NSLineBreakByTruncatingTail ;
    deptnameLbl.userInteractionEnabled = true;
    
    UITapGestureRecognizer *depttapgeature = [[UITapGestureRecognizer alloc]init ];
    depttapgeature.numberOfTapsRequired = 1.0;
    [depttapgeature addTarget:self action:@selector(deptTapGesture:)];
    depttapgeature.delegate=self;
    
    [deptnameLbl addGestureRecognizer:depttapgeature];
    
    [view addSubview:fromNumLbl];
    [view addSubview:imgvw];
    [view addSubview:deptnameLbl];
    
    
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]initWithCustomView:view];
    
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:_btn_back,btn, nil];
    
   // self.navigationItem.leftBarButtonItem = view;
    //self.navigationItem.titleView = view;
   
    

    if ([Default boolForKey:kIsNumberMask] == true) {
        if([self validateString:[titleString stringByReplacingOccurrencesOfString:@"+" withString:@""] withPattern:@"^[0-9]+$"])
        {
            self.title = [UtilsClass get_masked_number:titleString];
        }
    }
   
    [Default setValue:@"menu_sms_active" forKey:SelectedSideMenuImage];
    [[UINavigationBar appearance] setTranslucent:false];
    UINavigationBar.appearance.translucent = false;
    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.contactId != nil)
        {
            if ([self->_contactId isEqualToString:@""]) {
                [self->_btn_contact_add_edit setTitle:@"         Add Contact" forState:UIControlStateNormal];
                self->_imgAddEditContact.image =  [UIImage imageNamed:@"addContact"];
                self->_imgAddEditContact.contentMode = UIViewContentModeScaleToFill;
            }else{
                [self->_btn_contact_add_edit setTitle:@"         Edit Contact" forState:UIControlStateNormal];
                self->_imgAddEditContact.image =  [UIImage imageNamed:@"editContact"];
                self->_imgAddEditContact.contentMode = UIViewContentModeCenter;
            }
        }
        else
        {
            [self->_btn_contact_add_edit setTitle:@"         Edit Contact" forState:UIControlStateNormal];
            self->_imgAddEditContact.image =  [UIImage imageNamed:@"editContact"];
            self->_imgAddEditContact.contentMode = UIViewContentModeCenter;
        }
    });
    [self view_desing];
}
- (void)viewWillDisappear:(BOOL)animated
{
   [Default setValue:@"0" forKey:IS_ChatviewDisplayed];
   [Default setValue:@"" forKey:IS_ChatviewDisplayed_ThredID];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self->_view_send_message_bottom_constain.constant = 0;
    [IQKeyboardManager sharedManager].enable = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self tableViewScrollToBottomAnimated:NO];
}
-(void)viewDidDisappear:(BOOL)animated
{
     
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
    
}
-(void)view_desing
{
    
    NSLog(@"thread detail :: %@",ThreadMessageDic);
    
    
    
    DefultValue = 9999999;
    sendSmsIndex = DefultValue;
    receiveSmsIndex = DefultValue;
    SelectedMenu = @"";
    sendSmsSection = DefultValue;
    receiveSmsSection = DefultValue;
    RighMenuEnable = @"yes";
    MessageArray = [[NSMutableArray alloc] init];
    Mixallcontact = [[NSMutableArray alloc] init];
    SelectedsmsUUID = [[NSMutableArray alloc]init];
    SelectedIndex = [[NSMutableArray alloc]init];
    _view_right_menu.hidden = true;
    _view_right_menu.layer.cornerRadius = 5.0;
    _view_right_menu.layer.borderWidth = 1.0;
    _view_right_menu.layer.borderColor = UIColor.lightGrayColor.CGColor;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    self->_view_send_message_bottom_constain.constant = 0;
    if([_VcFrom_group isEqualToString:@"GroupSMS"])
    {
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        RighMenuEnable = @"no";
        [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@""]];
        [_nav_right_call setEnabled:NO];
        [_nav_right_call setImage:[UIImage imageNamed:@""]];
    }
    if ([ThreadMessageDic valueForKey:@"contactType"]!= (id)[NSNull null] && [ThreadMessageDic valueForKey:@"contactType"]!= nil){
        if ([[ThreadMessageDic valueForKey:@"contactType"] isEqualToString:@"mobile"]){
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
            RighMenuEnable = @"no";
            [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@""]];
        }else {
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
            RighMenuEnable = @"yes";
        }
    }
    else
    {
        RighMenuEnable = @"no";
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
    }
    _txt_send_textview.text = @"Write message";
    _txt_send_textview.textColor = [UIColor lightGrayColor];
    _txt_send_textview.layer.borderWidth = 1.0;
    _txt_send_textview.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    _txt_send_textview.layer.cornerRadius = _txt_send_textview.frame.size.height / 2;
    _txt_send_textview.delegate = self;
    self.sideMenuController.leftViewSwipeGestureEnabled = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(normalTap:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.delegate = self; // This is not required
    [self.view addGestureRecognizer:tapGesture];
    if([ThreadMessageDic.allValues count] != 0)
    {
        MessageArray = [ThreadMessageDic valueForKey:@"messages"];
            NSLog(@"Messagearray : %@",MessageArray);
            [self arrayfilter];
    }
    
   // [_nav_right_menu setEnabled:NO];
    [_nav_right_menu setImage:[UIImage imageNamed:@""]];
    
    
    

}
-(void)arrayfilter
{
    MessageArraySection = [[NSMutableArray alloc] init];
    MessageArrayRow = [[NSMutableArray alloc] init];
    MessageArraySection = [NSMutableArray new];
    MessageArrayRow = [NSMutableArray new];
    NSMutableDictionary *extractedData = [NSMutableDictionary new];
    if(MessageArray.count != 0)
    {
        for (int i = 0; i<MessageArray.count; i++)
        {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:MessageArray[i]];
            
         NSMutableDictionary *datedic = [self get_time_string:[dict valueForKey:@"smsTime"]];
            
            NSString *dateToshow = datedic[@"DateDisplay"];
            NSString *TimeDisplay = datedic[@"TimeDisplay"];
            [dict setValue:dateToshow forKey:@"DateDisplay"];
            [dict setValue:TimeDisplay forKey:@"TimeDisplay"];
            NSMutableArray *rowArray = extractedData[dateToshow];
            if (rowArray == nil) {
                rowArray = [NSMutableArray new];
                extractedData[dateToshow] = rowArray;
                [MessageArraySection addObject:dateToshow];
            }
            [rowArray addObject:dict];
        }
         [MessageArrayRow addObject:[NSDictionary dictionaryWithDictionary:extractedData]];
    }
   
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    [self tableViewScrollToBottomAnimated:NO];
}
-(void)Foreground_Action:(NSNotification *)notification
{
    [self contact_get];
}
-(void)contact_get
{
    Mixallcontact = [[NSMutableArray alloc] init];
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}
- (void)normalTap:(UITapGestureRecognizer *)sender
{
    _view_right_menu.hidden = true;
    btnclick_flag_chat = @"0";
}
#pragma mark - Tableview Methods
- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    NSInteger numberOfSections = self.MessageArraySection.count;
    if(numberOfSections == 0)
    {
        
    }
    else
    {
        NSString *sectionStr = [MessageArraySection objectAtIndex:self.MessageArraySection.count-1];
        NSMutableArray *sectionValue = [MessageArrayRow[0] valueForKey:sectionStr];
        NSInteger numberOfRows = sectionValue.count;
        if (numberOfRows)
        {
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:numberOfRows-1 inSection:numberOfSections-1]
                          atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        }
    }
}
- (BOOL)canBecomeFirstResponder
{
    return YES;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (MessageArraySection.count != 0) {
        return MessageArraySection.count; }
    else {
        return 0; }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (MessageArraySection.count != 0) {
        NSString *sectionStr = [MessageArraySection objectAtIndex:section];
        NSMutableArray *sectionValue = [MessageArrayRow[0] valueForKey:sectionStr];
        return sectionValue.count; }
    else {
        return 0; }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (MessageArraySection.count != 0) {
    NSString *sectionStr = [MessageArraySection objectAtIndex:indexPath.section];
    NSMutableArray *sectionValue = [MessageArrayRow valueForKey:sectionStr];
    NSDictionary *dic = [sectionValue[0] objectAtIndex:indexPath.row];
    NSString *MessageSender = [dic valueForKey:@"smsAuther"];
    if(![MessageSender isEqualToString:@""])
    {
        Tblcell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tblcell"];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        cell.lbl_send_time.hidden = true;
        if (sendSmsSection == indexPath.section && sendSmsIndex == indexPath.row){
            cell.lbl_send_time.hidden = false;
        }else{
            cell.lbl_send_time.hidden = true;
        }
        cell.lbl_send_text.text = [dic valueForKey:@"smsContent"];
        cell.lbl_send_time.text = [dic valueForKey:@"TimeDisplay"];
        cell.tag = indexPath.row;
        cell.contentView.tag=indexPath.section;
        UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [cell addGestureRecognizer:recognizer];
        return cell;
    }
    else
    {
        Tblcell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tblcell1"];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgColorView];
        cell.lbl_rec_time.hidden = false;
        if (receiveSmsSection == indexPath.section &&  receiveSmsIndex == indexPath.row){
            cell.lbl_rec_time.hidden = false;
        }else{
            cell.lbl_rec_time.hidden = true;
        }
        cell.lbl_rec_text.text = [dic valueForKey:@"smsContent"];
        cell.lbl_rec_time.text = [dic valueForKey:@"TimeDisplay"];
        cell.tag = indexPath.row;
        cell.contentView.tag = indexPath.section;
        UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [cell addGestureRecognizer:recognizer];
        return cell;
        }
    }
    else
    {
        Tblcell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tblcell"];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionStr = [MessageArraySection objectAtIndex:indexPath.section];
    NSMutableArray *sectionValue = [MessageArrayRow valueForKey:sectionStr];
    NSDictionary *dic = [sectionValue[0] objectAtIndex:indexPath.row];
    NSString *MessageSender = [dic valueForKey:@"smsAuther"];
    if ([tableView isEditing])
    {
        [[self tableView] setAllowsMultipleSelectionDuringEditing:YES];
        if (![MessageSender isEqualToString:@""])
        {
            Tblcell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tblcell"];
            
            [cell setSelected:YES];
        }else {
            Tblcell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Tblcell1"];
            [cell setSelected:YES];
            
        }
        if(![SelectedsmsUUID containsObject:[dic valueForKey:@"smsUUID"]])
        {
            [SelectedsmsUUID addObject:[dic valueForKey:@"smsUUID"]];
            [SelectedIndex addObject:indexPath];
        }
        if(SelectedsmsUUID.count != 0)
        {
            SelectedMenu = @"delete";
            [_nav_right_menu setImage:[UIImage imageNamed:@"delete"]];
        }
        else
        {
            SelectedMenu = @"close";
            [_nav_right_menu setImage:[UIImage imageNamed:@"close"]];
        }
    }
    else
    {
        if (![MessageSender isEqualToString:@""])
        {
            sendSmsIndex = indexPath.row;
            sendSmsSection = indexPath.section;
            receiveSmsIndex = DefultValue;
            receiveSmsSection = DefultValue;
        }else
        {
            receiveSmsIndex = indexPath.row;
            receiveSmsSection = indexPath.section;
            sendSmsIndex = DefultValue;
            sendSmsSection = DefultValue;
        }
        [_tableView reloadData];
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    Tblcell *cell = (Tblcell *)[_tableView cellForRowAtIndexPath:indexPath];
    NSString *sectionStr = [MessageArraySection objectAtIndex:indexPath.section];
    NSMutableArray *sectionValue = [MessageArrayRow valueForKey:sectionStr];
    NSDictionary *dic = [sectionValue[0] objectAtIndex:indexPath.row];
    if ([tableView isEditing])
    {
        if([SelectedsmsUUID containsObject:[dic valueForKey:@"smsUUID"]])
        {
            [SelectedsmsUUID removeObject:[dic valueForKey:@"smsUUID"]];
            [SelectedIndex removeObject:indexPath];
        }
        if(SelectedsmsUUID.count != 0)
        {
            SelectedMenu = @"delete";
            [_nav_right_menu setImage:[UIImage imageNamed:@"delete"]];
        }
        else
        {
            SelectedMenu = @"close";
            [_nav_right_menu setImage:[UIImage imageNamed:@"close"]];
        }
        [cell setSelected:NO];
    }
    else
    {
        cell.lbl_send_time.hidden = true;
        cell.lbl_rec_time.hidden = true;
        [_tableView reloadData];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(MessageArraySection.count > 0){
        return [MessageArraySection objectAtIndex:section];
    }else{
        return 0;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica" size:20.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    return view;
}
- (void)longPress:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"call");
        Tblcell *cell = (Tblcell *)recognizer.view;
        NSLog(@"Cell Tag : %ld",(long)cell.tag);
        SelectedIndexPath = [NSIndexPath indexPathForItem:cell.tag inSection:cell.contentView.tag];
        UIMenuItem *Copy = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(Copy:)];
        UIMenuItem *Delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(Delete:)];
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:Copy, Delete, nil]];
        [menu setTargetRect:cell.frame inView:cell.superview];
        [menu setMenuVisible:YES animated:YES];
        [_nav_right_menu setEnabled:YES];
        
    }
}
- (void)Copy:(id)sender
{
    NSLog(@"Cell was Copied");
    NSString *sectionStr = [MessageArraySection objectAtIndex:SelectedIndexPath.section];
    NSMutableArray *sectionValue = [MessageArrayRow valueForKey:sectionStr];
    NSDictionary *dic = [sectionValue[0] objectAtIndex:SelectedIndexPath.row];
    
    [UIPasteboard generalPasteboard].string = [dic valueForKey:@"smsContent"];
    
//    [_nav_right_menu setEnabled:NO];
    [_nav_right_menu setImage:[UIImage imageNamed:@""]];

}
- (void)Delete:(id)sender
{
    UITableView *tableView = [self tableView];
    [tableView setEditing:true animated:YES];
    NSLog(@"SMS UUID : %@",SelectedsmsUUID);
    SelectedMenu = @"close";
    [_nav_right_menu setImage:[UIImage imageNamed:@"close"]];
    if([RighMenuEnable isEqualToString:@"no"])
    {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
    
//    [_nav_right_menu setEnabled:NO];
       
}
-(NSMutableDictionary *)get_time_string:(NSString*)strDate {
    
    NSString *DateDisplay = @"";
    NSString *TimeDisplay = @"";
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    NSArray *strArray = [strDate componentsSeparatedByString:@" "];
    NSString *finalStr = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",strArray[0],strArray[1],strArray[2],strArray[3],strArray[4]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *date = [dateFormatter dateFromString:finalStr];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    dateFormatter.doesRelativeDateFormatting = YES;
    NSString *time = [dateFormatter stringFromDate:date];
    TimeDisplay = time;
    dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
   // [dateFormatter setDateFormat:@"EEEE dd MMM"];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSString *datestr = [dateFormatter stringFromDate:date];
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    if (components.year > 0) {
        DateDisplay = datestr;
    } else if (components.month > 0) {
        DateDisplay = datestr;
    } else if (components.weekOfYear > 0) {
        DateDisplay = datestr;
    } else if (components.day > 0) {
        if (components.day > 1) {
            DateDisplay = datestr;
        } else {
            DateDisplay =  @"Yesterday";
        }
    } else {
        DateDisplay =  @"Today";
    }
    [dic setValue:DateDisplay forKey:@"DateDisplay"];
    [dic setValue:TimeDisplay forKey:@"TimeDisplay"];
    return dic;
}
- (void)secVCDidDismisWithData:(NSMutableDictionary *)data{
    
   
    
    btnclick_flag_chat = @"0";
    _contactId = [data valueForKey:@"_id"];
    [_tblContactArray setValue:[data valueForKey:@"name"] forKey:@"threadName"];
    
    if ([[data valueForKey:@"numberArray"] count]>0) {
         [_tblContactArray setValue:[[[data valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] forKey:@"threadNumber"];
    }
   
    
    titleString = [data valueForKey:@"name"];
   // [UtilsClass view_navigation_title:self title:[NSString stringWithFormat:@"%@",[data valueForKey:@"name"]] color:UIColor.whiteColor];
   

    
}

- (IBAction)btn_call_action:(UIBarButtonItem *)sender
{
    Caller_Name = [_tblContactArray objectForKey:@"threadName"];
    Caller_Number = [_tblContactArray objectForKey:@"threadNumber"];
    My_Number = [_tblContactArray objectForKey:@"chNumber"];
    My_Department = [_tblContactArray objectForKey:@"chNumberName"];
    if(Caller_Number != (id)[NSNull null])
    {
        if(Caller_Name != (id)[NSNull null])
        {
            if(Caller_Name == Caller_Number)
            {
                [self make_call:Caller_Number Number:Caller_Number Mynumber:My_Number Mydepartment:My_Department];
            }
            else
            {
                [self make_call:Caller_Name Number:Caller_Number Mynumber:My_Number Mydepartment:My_Department];
            }
        }
        else
        {
            [self make_call:Caller_Number Number:Caller_Number Mynumber:My_Number Mydepartment:My_Department];
        }
        
    }
}
-(void)make_call:(NSString *)Name Number:(NSString *)Number Mynumber:(NSString *)Mynumber Mydepartment:(NSString *)Mydepartment
{
    if([UtilsClass isNetworkAvailable])
    {
       //pri  NSArray *purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
        
        NSData *data = [Default valueForKey:PURCHASE_NUMBER];
        NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if(purchase_number.count > 0)
        {
            NSString *preNumStr = _frmNumber;
            
            preNumStr=[preNumStr stringByReplacingOccurrencesOfString:@"+" withString:@""];
            preNumStr = [NSString stringWithFormat:@"+%@",preNumStr];
            
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",_frmNumber];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",preNumStr];
            
            NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
//            NSDictionary *dic1 = [results objectAtIndex:0];
//            NSString *numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
            
            NSString *numbVer = @"";
            if (results.count == 0){
                numbVer = @"1";
            }else{
                NSDictionary *dic1 = [results objectAtIndex:0];
                numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
            }
            
            
            int num = [numbVer intValue];
            if (num == 1) {
                NSString *credit = [Default valueForKey:CREDIT];
                float cre = [credit floatValue];
                if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1){
                    switch ([[AVAudioSession sharedInstance] recordPermission]) {
                        case AVAudioSessionRecordPermissionGranted:
                        {
                            NSString *selected_fromnumner = Mynumber;
                            NSString *selected_callToName= Name;
                            NSString *selected_callTonumber= Number;
                            NSString *selected_calltypes= OUTGOING;
                            [Default setValue:Mynumber forKey:SELECTEDNO];
                            [Default setValue:Mydepartment forKey:Selected_Department];
                            
                             if (results.count>0) {
                                 
                                 NSString *imagename = [[[[results objectAtIndex:0] objectForKey:@"number"]  objectForKey:@"shortName"] lowercaseString];
                                 [Default setValue:imagename forKey:Selected_Department_Flag];
                             }
                            
                            
                            
                            [UtilsClass make_outgoing_call_validate:self callfromnumber:selected_fromnumner ToName:selected_callToName ToNumber:selected_callTonumber calltype:selected_calltypes Mixallcontact:Mixallcontact];
                            break;
                        }
                        case AVAudioSessionRecordPermissionDenied:
                        {
                            [self micPermiiton];
                            break;
                        }
                        case AVAudioSessionRecordPermissionUndetermined:
                            break;
                        default:
                            break;
                            
                    }}else {
                        [UtilsClass showAlert:kCREDITLAW contro:self];
                    }}else{
                        [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
                    }}else{
                        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                    }
    }
    else
    {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}

-(void)micPermiiton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)textViewDidBeginEditing:(UITextView *)textView {
    if (textView.textColor == UIColor.lightGrayColor) {
        textView.text = @"";
        textView.textColor = UIColor.blackColor;
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write message";
        textView.textColor = UIColor.lightGrayColor;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    if(_txt_send_textview.contentSize.height >= 100)
    {
        
        _txt_send_textview.scrollEnabled = true;
        self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height;
        _txt_send_textview.frame = CGRectMake(_txt_send_textview.frame.origin.x, _txt_send_textview.frame.origin.y, _txt_send_textview.frame.size.width, 100);
        _view_send_height_constraint.constant = 140;
    }
    else
    {

        _view_send_height_constraint.constant = 60;
        _txt_send_textview.frame = CGRectMake(_txt_send_textview.frame.origin.x, _txt_send_textview.frame.origin.y, _txt_send_textview.frame.size.width, _txt_send_textview.contentSize.height);
        _txt_send_textview.scrollEnabled = false;
    }
}
- (IBAction)btn_back_click:(id)sender
{
    if(![_VcFrom isEqualToString:@""] )
    {
        [self.MessageArraySection removeAllObjects];
        [self.MessageArraySection removeAllObjects];
        SmsListNew *vc = [[self storyboard] instantiateViewControllerWithIdentifier: _VcFrom];
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[vc class]]) {
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                if ([_VcFrom isEqualToString:@"CalllogsDetailsVC"]){
                    if(_delegate && [_delegate respondsToSelector:@selector(secDismisWithData:)])
                    {
                        [_delegate secDismisWithData:_tblContactArray];
                    }
                }
                break;
            }
        }
    }
    else
    {
        [self.MessageArraySection removeAllObjects];
        [self.MessageArraySection removeAllObjects];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)keyboardWillShow:(NSNotification *)notification
{
    keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self movetoolbar:true notification:notification];
}
-(void)keyboardWillHide:(NSNotification *)notification
{
    keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self movetoolbar:false notification:notification];
}
-(void)movetoolbar:(BOOL)up notification:(NSNotification *)notification
{
    if(up == true)
    {
        NSDictionary* userInfo = [notification userInfo];
        keyboardSizeNew = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat bottomPadding = window.safeAreaInsets.bottom;
            if (bottomPadding < 1.0)
            {
                self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height;
                [self.view layoutIfNeeded];
            }
            else
            {
                self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height - 30;
                [self.view layoutIfNeeded];
            }
        }
        else
        {
            self.view_send_message_bottom_constain.constant = self->keyboardSizeNew.height;
            [self.view layoutIfNeeded];
        }
    }
    else
    {
        self.view_send_message_bottom_constain.constant = 0;
    }
}
- (IBAction)btn_add_contact_click:(id)sender
{
    _view_right_menu.hidden = true;
    AddEditContactVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddEditContactVC"];
    vc.delegate = self;
    if([_VcFrom1 isEqualToString:@"CalllogsDetailsVC"])
    {
        if ([_contactId isEqualToString:@""]) {
            vc.controller = @"addcalllogsDetails";
            vc.ContactDetailsDic = _tblContactArray;
        }else{
            vc.controller = @"calllogsDetails";
            vc.ContactDetailsDic = _tblContactArray;
        }
    }else if ([_VcFrom1 isEqualToString:@"CalllogsDetailsVCChat"]){
        
        if ([_contactId isEqualToString:@""]) {
            vc.controller = @"AddCalllogsDetailsVCChat";
            vc.ContactDetailsDic = _tblContactArray;
        }else{
            vc.controller = @"CalllogsDetailsVCChat";
            vc.ContactDetailsDic = _tblContactArray;
        }
    }else {
        if ([_contactId isEqualToString:@""]) {
            vc.controller = @"addFromChat";
            vc.ContactDetailsDic = _tblContactArray;
        }else{
            vc.controller = @"editFromChat";
            vc.ContactDetailsDic = _tblContactArray;
        }
    }
    [[self navigationController] pushViewController:vc animated:YES];
}
- (IBAction)btn_right_menu_click:(id)sender {
    if([SelectedMenu isEqualToString:@""])
    {
        if ([btnclick_flag_chat  isEqual: @"0"])
        {
            _view_right_menu.hidden = false;
            btnclick_flag_chat = @"1";
        }
        else
        {
            _view_right_menu.hidden = true;
            btnclick_flag_chat = @"0";
        }
    }
    else if ([SelectedMenu isEqualToString:@"close"])
    {
        UITableView *tableView = [self tableView];
        [tableView setEditing:false animated:YES];
        SelectedMenu = @"";
        [_nav_right_menu setImage:[UIImage imageNamed:@"rightmenu"]];
        if([RighMenuEnable isEqualToString:@"no"])
        {
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
            [_nav_right_menu setImage:[UIImage imageNamed:@""]];
        }
        [_nav_right_menu setImage:[UIImage imageNamed:@""]];
    }
    else if ([SelectedMenu isEqualToString:@"delete"])
    {
        
        UIAlertController * alert = [UIAlertController
                       alertControllerWithTitle:kAlertTitle
                       message:@"Are you sure you want to delete selected sms?"
                       preferredStyle:UIAlertControllerStyleAlert];
         
         
        UIAlertAction* yesButton = [UIAlertAction
                      actionWithTitle:@"Yes"
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction * action) {
                       
             [self DeleteSMS];
            [self->_nav_right_menu setImage:[UIImage imageNamed:@""]];
            
                      }];
         
        UIAlertAction* noButton = [UIAlertAction
                      actionWithTitle:@"Cancel"
                      style:UIAlertActionStyleDefault
                      handler:^(UIAlertAction * action) {
                            
              }];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
       
    }
}

-(void)DeleteSMS{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *url = [NSString stringWithFormat:@"deleteSms/%@",_threadId];
    NSString *delete_sms_id = [SelectedsmsUUID componentsJoinedByString:@","];
    NSDictionary *passDict = @{
        @"smsid":delete_sms_id
    };

    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_DELETE_RAW:url andParams:jsonString SuccessCallback:@selector(Delete:response:) andDelegate:self];
    
}
- (void)Delete:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"Delete SMS : %@",response1);
    [Processcall hideLoadingWithView];
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : delete sms : %@",response1);

        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            if (SelectedIndex.count != 0)
            {

                
                if(MessageArray.count == SelectedIndex.count)
                {
                       MessageArraySection = [[NSMutableArray alloc] init];
                       MessageArrayRow = [[NSMutableArray alloc] init];
                       MessageArraySection = [NSMutableArray new];
                       MessageArrayRow = [NSMutableArray new];
                       MessageArray = [[NSMutableArray alloc] init];
                       MessageArray = [NSMutableArray new];
                       [self arrayfilter];
                }
                else
                {
                for (int i = 0; i< SelectedIndex.count; i++)
                {
                    NSIndexPath *indexpath = [SelectedIndex objectAtIndex:i];
                    NSString *sectionStr = [MessageArraySection objectAtIndex:indexpath.section];
                    NSMutableArray *sectionValue = [MessageArrayRow valueForKey:sectionStr];
                    NSMutableDictionary *dic = [sectionValue[0] objectAtIndex:indexpath.row];
                    [dic removeObjectForKey:@"DateDisplay"];
                    [dic removeObjectForKey:@"TimeDisplay"];
                    NSMutableArray *arr = [[NSMutableArray alloc] init];
                    [arr addObject:dic];
                    [MessageArray removeObject:dic];
                    [self arrayfilter];
                }
                SelectedIndex = [[NSMutableArray alloc] init];
                SelectedsmsUUID = [[NSMutableArray alloc] init];
                UITableView *tableView = [self tableView];
                [tableView setEditing:false animated:YES];
                SelectedMenu = @"";
                [_nav_right_menu setImage:[UIImage imageNamed:@"rightmenu"]];
                if([RighMenuEnable isEqualToString:@"no"])
                {
                    [self.navigationItem.rightBarButtonItem setEnabled:NO];
                    [_nav_right_menu setImage:[UIImage imageNamed:@""]];
                }
                     [_nav_right_menu setImage:[UIImage imageNamed:@""]];
                [self.tableView reloadData];
            }
        }
        }else {
            SelectedIndex = [[NSMutableArray alloc] init];
            SelectedsmsUUID = [[NSMutableArray alloc] init];
            UITableView *tableView = [self tableView];
            [tableView setEditing:false animated:YES];
            SelectedMenu = @"";
            [_nav_right_menu setImage:[UIImage imageNamed:@"rightmenu"]];
            if([RighMenuEnable isEqualToString:@"no"])
            {
                [self.navigationItem.rightBarButtonItem setEnabled:NO];
                [_nav_right_menu setImage:[UIImage imageNamed:@""]];
            }
             [_nav_right_menu setImage:[UIImage imageNamed:@""]];
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
- (IBAction)btn_send_message_click:(UIButton *)sender {
    
  
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    NSArray *purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(purchase_number.count > 0)
    {
        
        
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",_frmNumber];
        
         NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",_frmNumber];
        
        NSArray *results = [purchase_number filteredArrayUsingPredicate:predicate];
        NSString *numbVer = @"";
        if (results.count == 0){
            numbVer = @"1";
        }else{
            NSDictionary *dic1 = [results objectAtIndex:0];
            numbVer = [[dic1 objectForKey:@"number"]valueForKey:@"numberVerify"];
        }
        int num = [numbVer intValue];
        if (num == 1) {
            NSString *credit = [Default valueForKey:CREDIT];
            float cre = [credit floatValue];
            if(cre > 0){
                if ([_txt_send_textview.text isEqualToString:@""] || [_txt_send_textview.text isEqualToString:@"Write message"]){
                    [UtilsClass showAlert:@"Please write some message." contro:self];
                }else{
                    [self sendsmsm:_txt_send_textview.text];
                }
            }else {
                [UtilsClass showAlert:kCREDITLAW contro:self];
            }
        }else{
            [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self];
        }}else{
            [UtilsClass showAlert:kNUMBERASSIGN contro:self];
        }
}
-(void)sendsmsm:(NSString *)text {
    NSString *sendtext = _txt_send_textview.text;
    [self sendSMS:sendtext];
}
-(void)sendSMS:(NSString*)sms{
    [Processcall showLoadingWithView:self.navigationController.view withLabel:nil];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *smsAuther = [Default valueForKey:FULL_NAME];
    NSString *url = [NSString stringWithFormat:@"smssend/%@",userId];
    NSString *Numberid = [Default valueForKey:NUMBERID];
    NSDictionary *passDict = @{@"from":_frmNumber,
                               @"to":_number,
                               @"numberId":Numberid,
                               @"smsBody":sms,
                               @"smsAuther":smsAuther};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}
- (void)login:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    [Processcall hideLoadingWithView];
    //NSLog(@"Send Sms Response : %@",response1);
    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        NSLog(@"Encrypted Response : send sms : %@",response1);

        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSString *smsUUID = response1[@"data"][@"messageObj"][@"messageObj"][@"smsUUID"];
            NSString *smsText = response1[@"data"][@"messageObj"][@"messageObj"][@"smsContent"];
            NSString *smsTime = response1[@"data"][@"messageObj"][@"messageObj"][@"smsTime"];
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
//            NSString *stringDate = [dateFormatter stringFromDate:[NSDate date]];
            NSString *stringDate = smsTime;
            NSString *FullName = [Default valueForKey:FULL_NAME];
            NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] init];
            [sendDic setValue:@"read" forKey:@"readStatus"];
            [sendDic setValue:FullName forKey:@"smsAuther"];
            [sendDic setValue:smsText forKey:@"smsContent"];
            [sendDic setValue:stringDate forKey:@"smsTime"];
            [sendDic setValue:smsUUID forKey:@"smsUUID"];
            [MessageArray addObject:sendDic];
            if(MessageArray.count != 0)
            {
                [self arrayfilter];
            }
            _txt_send_textview.text = @"";
            _view_send_height_constraint.constant = 60;
            _txt_send_textview.frame = CGRectMake(_txt_send_textview.frame.origin.x, _txt_send_textview.frame.origin.y, _txt_send_textview.frame.size.width, _txt_send_textview.contentSize.height);
            _txt_send_textview.scrollEnabled = false;
        }
        else
        {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
-(void)Conversation_update:(NSNotification*)notification
{
    if ([notification.name isEqualToString:@"Conversation_update"])
    {
        NSDictionary* userInfo = notification.userInfo;
        if([_threadId isEqualToString:userInfo[@"smsthreadId"]])
        {
            NSMutableDictionary *sendDic = [[NSMutableDictionary alloc] init];
            [sendDic setValue:userInfo[@"readStatus"] forKey:@"readStatus"];
            [sendDic setValue:userInfo[@"smsAuther"] forKey:@"smsAuther"];
            [sendDic setValue:userInfo[@"smsContent"] forKey:@"smsContent"];
            [sendDic setValue:userInfo[@"smsTime"] forKey:@"smsTime"];
            [sendDic setValue:userInfo[@"smsUUID"] forKey:@"smsUUID"];
            [MessageArray addObject:sendDic];
            if(MessageArray.count != 0)
            {
                [self arrayfilter];
            }
        }
    }
}
- (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
        return [predicate evaluateWithObject:string];
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    
}

-(void)deptTapGesture:(id)tapgesture
{
    _numToolTip.hidden = false;
    _toolTipNumLbl.text = [_tblContactArray objectForKey:@"chNumber"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self->_numToolTip.hidden = true;
    });
}
@end

