//
//  DialerVC.m
//  SecondNumber
//
//  Created by Apple on 30/11/21.
//


#import "Tblcell.h"
#import "WebApiController.h"
#import "Processcall.h"
#import "UtilsClass.h"
#import "DialerVC.h"
#import "AddEditContactVC.h"
#import "NewsmsVC.h"
#import "countryListVC.h"
#import "MainViewController.h"
#import "UIViewController+LGSideMenuController.h"
#import "OnCallVC.h"
#import "NewsmsVC.h"
#import "TransferVC.h"
#import "UIImageView+Letters.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "IQKeyboardManager.h"
#import "Singleton.h"
#import "GlobalData.h"
#import "NSData+Base64.h"
#import "Lin_Utility.h"
#import "NBPhoneNumberUtil.h"
#import "Phone.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
#import "CallKitInstance.h"
#import <WootricSDK/WootricSDK.h>
#import "ACWViewController.h"
#import "AppDelegate.h"
#import <Smartlook/Smartlook.h>
#import "twilio_callkit.h"

#import "SMSListModel.h"
#import "UserProfileVC.h"
#import <CoreLocation/CoreLocation.h>

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@import PhoneNumberKit;


@interface DialerVC ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate,PlivoEndpointDelegate>
{
    WebApiController *obj;
    NSArray *purchase_number;
   // NSMutableArray *purchase_number;
    NSArray *filteredData;
    TransferVC *transferVC;
    NSString *numberVeryfy;
    NSString *call_number;
    NSString *lastCallData;
    NSString *lastCall_code;
    UIView *Blur_view;
    NSString *pri;
    NSString *First_Flag;
    PlivoOutgoing *outCall;
    PlivoIncoming *incCall;
    NSString *endpointUserName;
    NSString *endpointPassWord;
    NSString *extentionName;
    NSString *extentionNumber;
    UIBarButtonItem *mailbutton;
    NSArray  *activeSubuser;
    
}

@end

@implementation DialerVC

- (void)viewDidLoad
{

  
    
    [super viewDidLoad];
    

    NSData *datap = [Default valueForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:datap];

    
    extentionName = @"";
    extentionNumber = @"";
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:ACTIVE_SUB_USER];
    activeSubuser = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    //NSLog(@"------------- %@",activeSubuser);
    
    self->_tbl_number_selection.delegate = self;
    self->_tbl_number_selection.dataSource = self;
    [self.tbl_number_selection reloadData];
   
    //rem [self addThinqSwitch];
    
    [UtilsClass twilio_token_registration];
    NSString *user_email = [Default valueForKey:kUSEREMAIL];
    if(user_email != nil)
    {
        [Smartlook setUserIdentifier:user_email];
    }
    [Default setValue:@"" forKey:Outgoing_Call_Code];
    [Default setValue:@"" forKey:Outgoing_Call_CodeName];
    
    [Default setValue:APP_VERSION forKey:@"current_app_version"];
    
    [Default synchronize];
    [_txt_call_number_text addTarget:self action:@selector(txt_call_number_value_change:) forControlEvents:UIControlEventEditingChanged];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(txt_value_change:) name:@"UITextFieldTextDidChangeNotification_value" object:nil];
    
    [self keypad_setup];

    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
    self.flag_num_selection = @"0";
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callfromcallhippo:) name:@"callfromcallhippo" object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(callfromcallhippo:) name:@"UITextFieldTextDidChangeNotification_value" object:nil];
    lastCall_code = @"";
    First_Flag = @"";
    _txt_call_number_text.delegate = self;

    [self checkContactSyncing];
    
    [self contact_get];
    [self viewdesing];
    self.sideMenuController.leftViewSwipeGestureEnabled = YES;
    
    [Default setValue:@"0" forKey:IS_CallLogDisplayed];
        
    [self getTagList];
    
    
    
   
}
-(void)checkContactSyncing
{
    
    NSLog(@" count of sqlite db >>>>> %lu",(unsigned long)[Contact allContactCount]);
    
 /*   if ([Default boolForKey:kisContactStoredInDB] == false && [Default boolForKey:kisContactSyncingRunning] == false)
    {
        dispatch_async(dispatch_get_main_queue(), ^{

            NSLog(@"count:: %lu",[Contact allContactCount]);
            
              [GlobalData  getCallhippoContactBySkipLimit:[NSString stringWithFormat:@"%lu",(unsigned long)[Contact allContactCount]]];
              
          });
    }
        
    if ([Default boolForKey:kNeedToUpdateContact] == true && [Default boolForKey:kisContactSyncingRunning] == false)
    {
        dispatch_async(dispatch_get_main_queue(), ^{

            [GlobalData getCallhippoUpdatedContacts:[NSString stringWithFormat:@"0"]];

        });
    }*/
    
    if ([Default boolForKey:kisContactStoredInDB] == true && [[[GlobalData sharedGlobalData] chContactListFromDB] count] == 0) {
        [[GlobalData sharedGlobalData] makeArrayFromDBChContact];
    }

    
}

-(void)Foreground_Action:(NSNotification *)notification
{
    NSLog(@"\n \n \n \n Trushang_code : Foreground_Action  Dialer:");
    [self contact_foreground_test];
}
-(void)callfromcallhippo:(NSNotification*)notification
{
    NSLog(@"Call from call hippo Done");
    _txt_call_number_text.text = @"";
    if ([notification.name isEqualToString:@"callfromcallhippo"])
    {
        [FIRAnalytics logEventWithName:@"ch_callthroughcallhippo" parameters:@{@"username": [Default valueForKey:kUSEREMAIL]}];
        
        NSDictionary* userInfo = notification.userInfo;
        NSString* Number = (NSString*)userInfo[@"Number"];
        NSArray *arr = [Number componentsSeparatedByString:@"+"];
        NSString *final_number = @"";
        _txt_call_number_text.delegate = self;
        if(arr.count != 1)
        {
            final_number = Number;
            final_number = [final_number stringByReplacingOccurrencesOfString:@"(" withString:@""];
            final_number = [final_number stringByReplacingOccurrencesOfString:@")" withString:@""];
            final_number = [final_number stringByReplacingOccurrencesOfString:@" " withString:@""];
            final_number = [final_number stringByReplacingOccurrencesOfString:@"-" withString:@""];
            _txt_call_number_text.text = @"";
            [_txt_call_number_text insertText:final_number];
        }
        else
        {
            final_number = Number;
            final_number = [final_number stringByReplacingOccurrencesOfString:@"(" withString:@""];
            final_number = [final_number stringByReplacingOccurrencesOfString:@")" withString:@""];
            final_number = [final_number stringByReplacingOccurrencesOfString:@" " withString:@""];
            final_number = [final_number stringByReplacingOccurrencesOfString:@"-" withString:@""];
            [_txt_call_number_text insertText:final_number];
        }
    }
}
-(BOOL)prefersStatusBarHidden{
    return NO;
}
-(void) receiveCountry:(NSNotification *) notification
{
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[Default valueForKey:selected_country_image_by_popup]]];
    NSString *code = [NSString stringWithFormat:@"%@", [Default valueForKey:selected_country_code_by_popup]];
    NSString *trimmed = [code stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *trimmed1 = [trimmed stringByReplacingOccurrencesOfString:@" " withString:@""];
    _img_country.image = image;
    [_btn_contry_select setTitle:[NSString stringWithFormat:@"%@",trimmed1] forState:UIControlStateNormal];
    [self getCountryTime:[NSString stringWithFormat:@"%@",[Default valueForKey:selected_country_name_by_popup]]];
}
-(void)contact_foreground_test
{
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *arr = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
    filteredData = (NSMutableArray*)arr;
    NSString *user_status =  [Default valueForKey:UserLoginStatus];
    if (user_status != nil)
    {
        if([user_status isEqualToString:@"login"])
        {
            [self getCredits];
        }
    }
}
-(void)contact_get
{
   // NSLog(@"mixallarray contact %lu",(unsigned long)Mixallcontact.count);
    
  //old  Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
    
    //NSLog(@"mixallarray contact1 %lu",(unsigned long)Mixallcontact.count);
    filteredData = [[NSMutableArray alloc] initWithArray:Mixallcontact];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *arr = [filteredData sortedArrayUsingDescriptors:@[sortDescriptor] ];
    filteredData = (NSMutableArray*)arr;
    _tbl_contact_search.delegate = self;
    _tbl_contact_search.dataSource = self;
    [_tbl_contact_search reloadData];
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"IncomingCallReceived" object:nil];
}
- (void) receiveIncomingCallReceived:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"IncomingCallReceived"])
        NSLog (@"Successfully received the test notification!");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
   // NewOnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

    if (IS_IPAD) {
        vc.popoverPresentationController.sourceView = self.view;
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        vc.modalPresentationStyle = UIModalPresentationPopover;
        [self presentViewController:vc animated:YES completion:nil];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     [self getNumbers];
    
    _txt_call_number_text.delegate = self;
//    _txt_call_number_text.text = @"";
    NSString *user_status =  [Default valueForKey:UserLoginStatus];
    if (user_status != nil)
    {
        if([user_status isEqualToString:@"login"])
        {
            _tbl_contact_search.hidden = true;
            [self getCredits];
        }
    }
    [UtilsClass view_navigation_title:self title:@""color:UIColor.whiteColor];
    _txt_call_number_text.userInteractionEnabled = true;
    _txt_call_number_text.inputView = [[UIView alloc] init];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveIncomingCallReceived:)
                                                 name:@"IncomingCallReceived"
                                               object:nil];
    if([Default valueForKey:IS_PHONENO])
    {
        NSString *phonenumber = [Default valueForKey:IS_PHONENO];
        self->_txt_call_number_text.text = phonenumber;
        [Default removeObjectForKey:IS_PHONENO];
        [Default synchronize];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self wootric];
    });
    
}
- (void)viewdesing {
    _view_tab_click.hidden = true;
//    [GlobalData  get_callhippo_number_selection];
  //old  [GlobalData  get_all_callhippo_contacts];
   
    [UtilsClass view_navigation_title:self title:@"" color:UIColor.whiteColor];
    _img_flag.layer.cornerRadius = 8.0;
    _img_flag.clipsToBounds = YES;
    _img_selected_image.image = [UIImage imageNamed:@""];
    self.img_selected_image.image = [UIImage imageNamed:@""];
    [self.lbl_selected_name1 setText:@""];
    
    
    
//    _txt_call_number_text.text = @"";
//    [self ip_address_check];
   
    _tbl_contact_search.hidden = true;
    _tbl_contact_search.delegate = self;
    _tbl_contact_search.dataSource = self;
    _btnSMS.hidden = true;
    _btnNewContact.hidden = true;
    [UtilsClass view_shadow_boder_custom:_view_tab_click];
    [UtilsClass view_shadow_boder_custom:_view_text];
    [UtilsClass button_shadow_boder:_btn_call];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tap.delegate = self; // This is not required
    [self.view_tab_click addGestureRecognizer:tap];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(normalTap:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.delegate = self; // This is not required
    [self.btn_clear addGestureRecognizer:tapGesture];
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTap:)];
    longGesture.delegate = self; // This is not required
    [self.btn_clear addGestureRecognizer:longGesture];
    UILongPressGestureRecognizer *longGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTap1:)];
    longGesture1.delegate = self; // This is not required
    [self.btn_zero addGestureRecognizer:longGesture1];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
           [self contact_get];
    });
}

- (void)handleTap:(UITapGestureRecognizer *)sender
{
    if ([self.flag_num_selection  isEqual: @"0"]) {
        [self view_selectnumber_up];
    }
    else {
        [self view_selectnumber_down];
    }
}
- (void)normalTap:(UITapGestureRecognizer *)sender
{
    if (![_txt_call_number_text.text isEqualToString:@""])
    {
        if(_txt_call_number_text.editing)
        {
//            UITextRange *selRange = _txt_call_number_text.selectedTextRange;
//            UITextPosition *selStartPos = selRange.start;
//            NSInteger idx =  [_txt_call_number_text offsetFromPosition:_txt_call_number_text.beginningOfDocument toPosition:selStartPos];
        }
        else
        {
//            UITextPosition *positionBeginning = [_txt_call_number_text endOfDocument];
//            UITextRange *textRange =[_txt_call_number_text textRangeFromPosition:positionBeginning
//                                                                      toPosition:positionBeginning];
//            [_txt_call_number_text setSelectedTextRange:textRange];
//            UITextRange *selRange = _txt_call_number_text.selectedTextRange;
//            UITextPosition *selStartPos = selRange.start;
//            NSInteger idx =  [_txt_call_number_text offsetFromPosition:_txt_call_number_text.beginningOfDocument toPosition:selStartPos];
        }
        
        [_txt_call_number_text deleteBackward];
//        NSString *text = _txt_call_number_text.text;
        [self search_number];
        if([_txt_call_number_text.text isEqualToString:@""])
        {
            _txt_call_number_text.userInteractionEnabled = true;
        }
        else
        {
            _txt_call_number_text.userInteractionEnabled = true;
        }
    }
    else
    {
        [_txt_call_number_text deleteBackward];
        if([_txt_call_number_text.text isEqualToString:@""])
        {
            _txt_call_number_text.userInteractionEnabled = true;
        }
        else
        {
            _txt_call_number_text.userInteractionEnabled = true;
        }
    }
}

- (void)longTap:(UITapGestureRecognizer *)sender
{
    _txt_call_number_text.text = @"";
    [self search_number];
    if([_txt_call_number_text.text isEqualToString:@""])
    {
        _txt_call_number_text.userInteractionEnabled = true;
    }
    else
    {
        _txt_call_number_text.userInteractionEnabled = true;
    }
}
- (void)longTap1:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
        UITextRange *selRange = _txt_call_number_text.selectedTextRange;
        UITextPosition *selStartPos = selRange.start;
        NSInteger idx =  [_txt_call_number_text offsetFromPosition:_txt_call_number_text.beginningOfDocument toPosition:selStartPos];
//        NSUInteger index = idx+1;
        if(_txt_call_number_text.editing)
        {
            if(_txt_call_number_text.text.length > 0)
            {
                if(idx == 0)
                {
                    [_txt_call_number_text insertText:@"+"];
                }
                else
                {
                    _txt_call_number_text.text = @"";
                    [_txt_call_number_text insertText:@"+"];
                }
            }
            else
            {
                _txt_call_number_text.text = @"";
                [_txt_call_number_text insertText:@"+"];
            }
        }
        else
        {
            _txt_call_number_text.text = @"";
            [_txt_call_number_text insertText:@"+"];
        }
    }
}
-(void)keypad_setup
{
    [UtilsClass keypadtextset:_btn_one text:@"1\n  " tot:2];
    [UtilsClass keypadtextset:_btn_two text:@"2\nABC" tot:3];
    [UtilsClass keypadtextset:_btn_three text:@"3\nDEF" tot:3];
    [UtilsClass keypadtextset:_btn_four text:@"4\nGHI" tot:3];
    [UtilsClass keypadtextset:_btn_five text:@"5\nJKL" tot:3];
    [UtilsClass keypadtextset:_btn_six text:@"6\nMNO" tot:3];
    [UtilsClass keypadtextset:_btn_seven text:@"7\nPQRS" tot:4];
    [UtilsClass keypadtextset:_btn_eight text:@"8\nTUV" tot:3];
    [UtilsClass keypadtextset:_btn_nine text:@"9\nWXYZ" tot:4];
    [UtilsClass keypadtextset:_btn_zero text:@"0\n+ " tot:2];
    [UtilsClass keypadtextset:_btn_star text:@"*\n  " tot:2];
    [UtilsClass keypadtextset:_btn_has text:@"#\n  " tot:2];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {

    if (action == @selector(paste:)) {
        return YES;
    }

    if (action == @selector(copy:)) {
        return YES;
    }
    return [super canPerformAction:action withSender:sender];
}

- (void)copy:(id)sender
{
    [UIPasteboard generalPasteboard].string = _txt_call_number_text.text;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tbl_number_selection)
    {
        return purchase_number.count;
    }
    else
    {
        return filteredData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_number_selection)
    {
        Tblcell *cell = [_tbl_number_selection dequeueReusableCellWithIdentifier:@"cell123"];
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        cell.img_select_con_num.image = [UIImage imageNamed:imagename];
        cell.lbl_select_con_name.text = contactName;
        cell.lbl_select_con_number.text  = phoneNumber;
        _tbl_number_selection.rowHeight = 64.0;
        [cell layoutIfNeeded];
        [cell.contentView layoutIfNeeded];
        return cell;
    }
    else
    {
        Tblcell *cell = [_tbl_contact_search dequeueReusableCellWithIdentifier:@"Tblcell123"];
        
        NSDictionary *dic = [filteredData objectAtIndex:indexPath.row];
        cell.lblAutherName.text = [dic valueForKey:@"name"];
        
        //p cell.lblLatestMessage.text = [dic valueForKey:@"number"];
        
        if ([[dic valueForKey:@"numberArray"] count]>0) {
            
            if (dic != nil || dic != NULL) {
                  [[FIRCrashlytics crashlytics] setCustomValue:dic  forKey:@"NumberLableDialer"];
                
            }
           
            cell.lblLatestMessage.text = [[[dic valueForKey:@"numberArray"]objectAtIndex:0] valueForKey:@"number"];
            
            
            if ([Default boolForKey:kIsNumberMask] == true) {
                cell.lblLatestMessage.text = [UtilsClass get_masked_number:[[[dic valueForKey:@"numberArray"]objectAtIndex:0] valueForKey:@"number"]];
            }
        }
        else
        {
            cell.lblLatestMessage.text = @"";
        }
        
        cell.imgIconeSms.layer.borderWidth = 0.0;
        NSString *name = [dic valueForKey:@"name"];
        name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (![UtilsClass isValidNumber:name])
        {
            if (dic[@"_id"])
            {
                if(![[dic valueForKey:@"_id"] isEqualToString:@""])
                {
                    cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
                }
                cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
            }
            else
            {
                [cell.imgIconeSms setImageWithString:name color:UIColor.groupTableViewBackgroundColor circular:true textAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f], NSForegroundColorAttributeName:[UIColor grayColor]}];
                cell.imgIconeSms.layer.cornerRadius = cell.imgIconeSms.frame.size.height / 2;
                cell.imgIconeSms.layer.cornerRadius=cell.imgIconeSms.frame.size.height / 2;
                cell.imgIconeSms.layer.borderWidth=1.0;
                cell.imgIconeSms.layer.masksToBounds = YES;
                cell.imgIconeSms.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                cell.imgIconeSms.clipsToBounds = YES;
                
            }
        }
        else
        {
            cell.imgIconeSms.image = [UIImage imageNamed:Cell_callhippo_image];
        }
        [cell layoutIfNeeded];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_number_selection)
    {
        NSDictionary *dic = [purchase_number objectAtIndex:indexPath.row];
        NSDictionary *numberdic = [dic objectForKey:@"number"];
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        [Default setValue:phoneNumber forKey:SELECTEDNO];
        numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]];
        [Default setValue:[numberdic objectForKey:@"numberVerify"] forKey:SELECTED_NUMBER_VERIFY];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
        [Default setValue:contactName forKey:Selected_Department];
        [Default setValue:imagename forKey:Selected_Department_Flag];
        [self.lbl_selected_name1 setText:contactName];
        First_Flag = imagename;
        _img_selected_image.image = [UIImage imageNamed:imagename];
        _img_selected_image.layer.cornerRadius = 10.0;
        [self view_selectnumber_down];
        
        NSLog(@"provider ::>>>>>> %@",[numberdic valueForKey:@"provider"]);

    }
    else
    {
        NSDictionary *dic = [filteredData objectAtIndex:indexPath.row];
        
        //p if([dic objectForKey:@"number"])
        if([[dic valueForKey:@"numberArray"] count]>0)
        {
            if ([[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"]) {
                
                _txt_call_number_text.text = @"";
               //p [_txt_call_number_text insertText:[dic valueForKey:@"number"]];
                [_txt_call_number_text insertText:[[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"]];
                
                NSString *ImageName = [self->_txt_call_number_text.country.countryCode lowercaseString];
                NSString *TimeName = self->_txt_call_number_text.country.name;
                self->_img_country.image = [UIImage imageNamed:ImageName];;
                _btn_call.enabled = true;
               // [self getCountryTime:[NSString stringWithFormat:@"%@",TimeName]];
                [self computeLocalTimeZone:[NSDate date] countryCode:_txt_call_number_text.country.countryCode];

            }
            
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==  _tbl_number_selection)
    {
        return 64;
    }
    else
    {
        return 70;
    }
}
-(void) view_selectnumber_up {

    
    if ([self.flag_num_selection isEqual : @"0" ])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height-64;
        Blur_view = [[UIView alloc] init];
        Blur_view.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
        Blur_view.backgroundColor = UIColor.blackColor;
        Blur_view.alpha = 0.7;
        [self.view insertSubview:Blur_view belowSubview:_view_number_selection];
        
        
        self.flag_num_selection = @"1";
        [self updateViewConstraints];
        self->_view_nmbr_selection_bottomConstraint.constant = 0;
       
    }
}
-(void)updateViewConstraints
{
    
    if ([self.flag_num_selection isEqual : @"1"])
    {
        _tbl_nmbr_selection_heightConstraint.constant = _tbl_number_selection.contentSize.height ;
       
    }
    else
    {
        _tbl_nmbr_selection_heightConstraint.constant = 0;
    }
     [super updateViewConstraints];
}
-(void) view_selectnumber_down
{
    if ([self.flag_num_selection isEqual : @"1" ])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        [Blur_view removeFromSuperview];
//
        
        self.flag_num_selection = @"0";
        [self updateViewConstraints];
        self->_view_nmbr_selection_bottomConstraint.constant = 100;
    }
    
}
-(void)getNumbers {
    
    [self displayTime];
//    [self keypad_setup];
    
    purchase_number = [[NSMutableArray alloc]init];
   //pri  purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
    purchase_number = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    //NSLog(@"TRUSHANG PURCHASENUMBER : %@",[[GlobalData sharedGlobalData] get_number_selection]);
    if(purchase_number.count > 0)
    {
        _view_tab_click.hidden = false;
        NSDictionary *dic1 = [purchase_number objectAtIndex:0];
        NSDictionary *numberdic = [dic1 objectForKey:@"number"];
        NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
        NSString *contactName = [numberdic objectForKey:@"contactName"];
        NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
        [Default setValue:phoneNumber forKey:SELECTEDNO];
        NSString *number_id = [numberdic objectForKey:@"_id"];
        numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
        [Default setValue:number_id forKey:NUMBERID];
        First_Flag = imagename;
        [Default setValue:contactName forKey:Selected_Department];
        [Default setValue:imagename forKey:Selected_Department_Flag];
        [_lbl_selected_name1 setText:contactName];
        _img_selected_image.image = [UIImage imageNamed:imagename];
        _img_selected_image.layer.cornerRadius = 10.0;
        self.tbl_number_selection.delegate = self;
        self.tbl_number_selection.dataSource = self;
        self.tbl_number_selection.rowHeight = 61.0;
        [self.tbl_number_selection setNeedsLayout];
        [self.tbl_number_selection reloadData];
    } else {
                _view_tab_click.hidden = true;
    }
        
}

- (void)displayTime {
    
    
    _lbl_time_date.text = [Default valueForKey:COUNTRYTIME];
    NSString *numbVer = [Default valueForKey:IS_DISPLAY_TIME];
    int isDisplayTime = [numbVer intValue];
    if (isDisplayTime == 1) {
        _lbl_time_date.hidden = false;
        _img_country.frame =  CGRectMake(79,
                                         8,
                                         25,
                                         25);
    }else {
        _lbl_time_date.hidden = true;
        _img_country.frame =  CGRectMake((self.view.frame.size.width/2) - (25/2),
                                         (self.viewTimeZone.frame.size.height / 2) - (25/2),
                                         25,
                                         25);
    }
    
    _txt_call_number_text.delegate = self;
    _txt_call_number_text.text = @"";
    
//b    [_txt_call_number_text insertText:[Default valueForKey:LAST_COUNTRY_CODE]];
//    lastCall_code = [Default valueForKey:LAST_COUNTRY_CODE];
    
    
    [_txt_call_number_text insertText:[Default valueForKey:klastDialCountryCode]];
    lastCall_code = [Default valueForKey:klastDialCountryCode];
    
    NSString *numberLength = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString *countryCode = _txt_call_number_text.code;
    long lengthOfDialNumber = 3;//countryCode.length + 3;
    if((numberLength.length >= lengthOfDialNumber || [_txt_call_number_text.text isEqualToString:lastCall_code] || [_txt_call_number_text.text isEqualToString:@""]) && ![_txt_call_number_text.text isEqualToString:@"+"])
    {
        _btn_call.enabled = true;
    }else{
        _btn_call.enabled = false;
    }
    
}

- (IBAction)btn_menu_click:(id)sender {
    MainViewController *mainViewController = (MainViewController *)self.sideMenuController;
    [mainViewController showLeftViewAnimated:true completionHandler:nil];
}

-(void)getCountryTime:(NSString *)name
{
    //NSLog(@"name:: %@",name);
    
    NSString *aStrUrl = @"timezone/country";
    NSDictionary *dic =  [[NSDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    if(userId != NULL)
    {
        
        if ([name isEqualToString:@""] || name == nil)
        {
            dic = @{@"countryname":@"United States",
                    @"userId":userId,
            };
        }
        else
        {
            dic = @{@"countryname":name,
                    @"userId":userId,
            };
        }
      //before  [obj callAPI_POST:aStrUrl andParams:dic SuccessCallback:@selector(getCountryTimeresponse:response:) andDelegate:self];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {

        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
            obj = [[WebApiController alloc] init];
        
        [obj callAPI_POST_RAW:aStrUrl andParams:jsonString SuccessCallback:@selector(getCountryTimeresponse:response:) andDelegate:self];
    }
    else
    {
        
    }
}
- (void)getCountryTimeresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
//    NSLog(@"Trushang : Response : getCountryTimeresponse : statuscode **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        NSLog(@"Encrypted Response : getCountryTimeresponse : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            NSString *time = [response1 valueForKey:@"data"][@"time"];
            [Default setValue:time forKey:COUNTRYTIME];
            _lbl_time_date.text = time;
            
            
        }else
        {
            @try {
                //NSLog(@"Error Blank popup : getCountryTimeresponse : %@",response1);
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
- (IBAction)btn_county_list_select:(id)sender
{
    countryListVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"countryListVC"];
    vc.view.backgroundColor = [UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:0.5];
    if (IS_IPAD) {
        vc.popoverPresentationController.sourceView = self.view;
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:true completion:nil];
    }
}

#pragma mark - nkvphonepicker textfield methods
- (IBAction)txt_call_number_touchupinside:(UITextField *)sender
{
    
}
- (IBAction)txt_call_number_change:(UITextField *)sender
{
    if([_txt_call_number_text.text isEqualToString:@""])
    {
        _txt_call_number_text.userInteractionEnabled = false;
    }
    else
    {
        _txt_call_number_text.userInteractionEnabled = true;
    }
}
-(void)textChanged:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Handle backspace/delete
    if (!string.length)
    {
        // Backspace detected, allow text change, no need to process the text any further
        return YES;
    }
    // Input Validation
    // Prevent invalid character input, if keyboard is numberpad
    if ([string rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location != NSNotFound)
    {
        return NO;
    }
    return YES;
}


- (void)txt_value_change:(NSNotification *)notif
{
}


- (IBAction)btn_thinq:(UIButton*)sender
{
    UIImage *secondImage = [UIImage imageNamed:@"thimqswithchb.png"];
       UIImage *img = [sender imageForState:UIControlStateNormal];
       NSData *imgData1 = UIImagePNGRepresentation(img);
       NSData *imgData2 = UIImagePNGRepresentation(secondImage);
       
       BOOL isCompare =  [imgData1 isEqual:imgData2];
       if(isCompare)
       {
           [sender setImage:[UIImage imageNamed:@"thimqswithcha.png"] forState:UIControlStateNormal];
           //[Default setValue:@"0" forKey:kTHINQVALUE];
           [Default setValue:@"A" forKey:kTelephonyProviderSwitch];
           [self thinq:@"false"];
           
       }else {
           [sender setImage:[UIImage imageNamed:@"thimqswithchb.png"] forState:UIControlStateNormal];
           //[Default setValue:@"1" forKey:kTHINQVALUE];
           [Default setValue:@"B" forKey:kTelephonyProviderSwitch];

           [self thinq:@"true"];
       }
    
    NSLog(@"telephony after switch change>> %@",[Default valueForKey:kTelephonyProviderSwitch]);
}

- (IBAction)txt_call_number_value_change:(id)sender
{
    NSLog(@"txt country:: %@",_txt_call_number_text.country);
    NSLog(@"txt country:: %@",_txt_call_number_text.text);
    
    if(_txt_call_number_text.country)
    {

        NSString *ImageName = [_txt_call_number_text.country.countryCode lowercaseString];
        NSString *TimeName = _txt_call_number_text.country.name;
        
        NSLog(@"time name:: %@",TimeName);
        _img_country.image = [UIImage imageNamed:ImageName];;
        
        NSString *numbVer = [Default valueForKey:IS_DISPLAY_TIME];
        int isDisplayTime = [numbVer intValue];
        if (isDisplayTime == 1) {
            if(_txt_call_number_text.text.length <= 5 && _txt_call_number_text.text.length > 1)
            {
               // [self getCountryTime:[NSString stringWithFormat:@"%@",TimeName]];
                
                // Now convert it to a local time zone string
                 [self computeLocalTimeZone:[NSDate date] countryCode:_txt_call_number_text.country.countryCode];
            
                //NSLog(@"Computed Date String: %@", localizedDateString);
            }
            
        }
       
        //check for crash
//        NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
//        txtText.text = @"";
//        [txtText insertText:_txt_call_number_text.text];
//
//
//        NSString *code = txtText.code ? txtText.code : @"";
//        NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
        
         NSString *code = _txt_call_number_text.code ? _txt_call_number_text.code : @"";
        NSString *codeName = _txt_call_number_text.country.countryCode ? _txt_call_number_text.country.countryCode : @"";
                
        
        if(_txt_call_number_text.text.length > 3)
        {
            NSString *numbVer = [Default valueForKey:IS_AutoSwitch];
            int num = [numbVer intValue];
            if (num == 1)
            {
                if([[First_Flag lowercaseString] isEqualToString:[codeName lowercaseString]])
                {
                }
                else
                {
                    if([[Default valueForKey:Outgoing_Call_CodeName] isEqualToString:codeName])
                    {
                    }
                    else
                    {
                        [Default setValue:code forKey:Outgoing_Call_Code];
                        [Default setValue:codeName forKey:Outgoing_Call_CodeName];
                        [Default synchronize];
                        First_Flag = [codeName lowercaseString];
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.shortName  contains[c] %@",codeName];
                        NSArray *filteredData = [purchase_number filteredArrayUsingPredicate:predicate];
                        if (filteredData.count != 0)
                        {
                            @try{
                                NSDictionary *dic1 = [filteredData objectAtIndex:0];
                                NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                                NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
                                NSString *contactName = [numberdic objectForKey:@"contactName"];
                                NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                                [Default setValue:phoneNumber forKey:SELECTEDNO];
                                NSString *number_id = [numberdic objectForKey:@"_id"];
                                numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVeryfy"]];
                                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
                                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
                                [Default setValue:number_id forKey:NUMBERID];
                                [Default setValue:contactName forKey:Selected_Department];
                                [Default setValue:imagename forKey:Selected_Department_Flag];
                                [_lbl_selected_name1 setText:contactName];
                                _img_selected_image.image = [UIImage imageNamed:imagename];
                                _img_selected_image.layer.cornerRadius = 10.0;
                            }
                            @catch (NSException *exception) {
                            }
                            
                        }
                        else
                        {
                            
                            if(purchase_number.count > 0)
                            {
                                
                                NSDictionary *dic1 = [purchase_number objectAtIndex:0];
                                NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                                NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
                                NSString *contactName = [numberdic objectForKey:@"contactName"];
                                NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                                [Default setValue:phoneNumber forKey:SELECTEDNO];
                                NSString *number_id = [numberdic objectForKey:@"_id"];
                                numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVeryfy"]];
                                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
                                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
                                [Default setValue:number_id forKey:NUMBERID];
                                [Default setValue:contactName forKey:Selected_Department];
                                [Default setValue:imagename forKey:Selected_Department_Flag];
                                [_lbl_selected_name1 setText:contactName];
                                _img_selected_image.image = [UIImage imageNamed:imagename];
                                _img_selected_image.layer.cornerRadius = 10.0;
                            }
                        }
                        [UtilsClass callinge_from_number_check];
                    }
                }
            }
        }
    }
    NSString *numberLength = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString *countryCode = _txt_call_number_text.code;
    long lengthOfDialNumber = 3;//countryCode.length + 3;
    if((numberLength.length >= lengthOfDialNumber || [_txt_call_number_text.text isEqualToString:lastCall_code] || [_txt_call_number_text.text isEqualToString:@""]) && ![_txt_call_number_text.text isEqualToString:@"+"])
    {
        _btn_call.enabled = true;
    }else{
        _btn_call.enabled = false;
    }
}
- (IBAction)btn_call:(id)sender
{
//    purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
    
    //b   _txt_call_number_text.text = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSString *tempstr = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    if(purchase_number.count > 0 || tempstr.length == 3 || tempstr.length == 4)
    {
    NSString *numbVer = [Default valueForKey:ISDummyNumber];
    int num = [numbVer intValue];
    if (num == 1)
    {
        
     //   _txt_call_number_text.text = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
      //  _txt_call_number_text.text = [NSString stringWithFormat:@"+%@",_txt_call_number_text.text];

        
    if([_txt_call_number_text.text isEqualToString:lastCall_code])
    {
        _txt_call_number_text.delegate = self;
        _txt_call_number_text.text = @"";
//b        [_txt_call_number_text setText:lastCallData];
        
        //pri
        [_txt_call_number_text insertText:[Default valueForKey:klastDialNumber]];
        
//        UITextRange *selRange = _txt_call_number_text.selectedTextRange;
//        UITextPosition *selStartPos = selRange.start;
//        NSInteger idx =  [_txt_call_number_text offsetFromPosition:_txt_call_number_text.beginningOfDocument toPosition:selStartPos];
        NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
        txtText.text = @"";
        [txtText insertText:_txt_call_number_text.text];
        NSString *code = txtText.code ? txtText.code : @"";
        NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
        NSString *CountryName = txtText.country.name ? txtText.country.name : @"";
        _img_country.image = [UIImage imageNamed:[codeName lowercaseString]];;
        //[self getCountryTime:[NSString stringWithFormat:@"%@",CountryName]];
        [self computeLocalTimeZone:[NSDate date] countryCode:codeName];
        if(_txt_call_number_text.text.length > 3)
        {
            NSString *numbVer = [Default valueForKey:IS_AutoSwitch];
            int num = [numbVer intValue];
            if (num == 1)
            {
                if([[First_Flag lowercaseString] isEqualToString:[codeName lowercaseString]])
                {
                }
                else
                {
                    if([[Default valueForKey:Outgoing_Call_CodeName] isEqualToString:codeName])
                    {
                    }
                    else
                    {
                        [Default setValue:code forKey:Outgoing_Call_Code];
                        [Default setValue:codeName forKey:Outgoing_Call_CodeName];
                        [Default synchronize];
                        First_Flag = [codeName lowercaseString];
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.shortName  contains[c] %@",codeName];
                        NSArray *filteredData = [purchase_number filteredArrayUsingPredicate:predicate];
                        if (filteredData.count != 0)
                        {
                            NSDictionary *dic1 = [filteredData objectAtIndex:0];
                            NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                            NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
                            NSString *contactName = [numberdic objectForKey:@"contactName"];
                            NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                            [Default setValue:phoneNumber forKey:SELECTEDNO];
                            NSString *number_id = [numberdic objectForKey:@"_id"];
                            numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVeryfy"]];
                            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
                            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
                            [Default setValue:number_id forKey:NUMBERID];
                            [Default setValue:contactName forKey:Selected_Department];
                            [Default setValue:imagename forKey:Selected_Department_Flag];
                            [_lbl_selected_name1 setText:contactName];
//                            [_lbl_selected_name1 setText:@"hello every one how r you fine"];
                            _img_selected_image.image = [UIImage imageNamed:imagename];
                            _img_selected_image.layer.cornerRadius = 10.0;
                        }
                        else
                        {
                        }
                        [UtilsClass callinge_from_number_check];
                    }
                }
            }
        }
        [self search_number];
    } else if([_txt_call_number_text.text isEqualToString:@""]) {
        
        
        _txt_call_number_text.text = @"";
 //b       [_txt_call_number_text setText:lastCallData];
        
        //pri
        [_txt_call_number_text insertText:[Default valueForKey:klastDialNumber]];
        NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
        
        //        UITextRange *selRange = _txt_call_number_text.selectedTextRange;
        //        UITextPosition *selStartPos = selRange.start;
        //        NSInteger idx =  [_txt_call_number_text offsetFromPosition:_txt_call_number_text.beginningOfDocument toPosition:selStartPos];
        txtText.text = @"";
        [txtText insertText:_txt_call_number_text.text];
        NSString *code = txtText.code ? txtText.code : @"";
        NSString *codeName = txtText.country.countryCode ? txtText.country.countryCode : @"";
        NSString *CountryName = txtText.country.name ? txtText.country.name : @"";
        _img_country.image = [UIImage imageNamed:[codeName lowercaseString]];;
        //[self getCountryTime:[NSString stringWithFormat:@"%@",CountryName]];
        [self computeLocalTimeZone:[NSDate date] countryCode:codeName];
        if(_txt_call_number_text.text.length > 3)
        {
            NSString *numbVer = [Default valueForKey:IS_AutoSwitch];
            int num = [numbVer intValue];
            
            if (num == 1)
            {
                if([[First_Flag lowercaseString] isEqualToString:[codeName lowercaseString]])
                {
                }
                else
                {
                    if([[Default valueForKey:Outgoing_Call_CodeName] isEqualToString:codeName])
                    {
                    }
                    else
                    {
                        [Default setValue:code forKey:Outgoing_Call_Code];
                        [Default setValue:codeName forKey:Outgoing_Call_CodeName];
                        [Default synchronize];
                        First_Flag = [codeName lowercaseString];
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.shortName  contains[c] %@",codeName];
                        NSArray *filteredData = [purchase_number filteredArrayUsingPredicate:predicate];
                        if (filteredData.count != 0)
                        {
                            NSDictionary *dic1 = [filteredData objectAtIndex:0];
                            NSDictionary *numberdic = [dic1 objectForKey:@"number"];
                            NSString *imagename = [[numberdic objectForKey:@"shortName"] lowercaseString];
                            NSString *contactName = [numberdic objectForKey:@"contactName"];
                            NSString *phoneNumber = [numberdic objectForKey:@"phoneNumber"];
                            [Default setValue:phoneNumber forKey:SELECTEDNO];
                            NSString *number_id = [numberdic objectForKey:@"_id"];
                            numberVeryfy = [NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]];
                            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[numberdic objectForKey:@"numberVerify"]] forKey:SELECTED_NUMBER_VERIFY];
                            [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[dic1 objectForKey:ISDummyNumber]] forKey:ISDummyNumber];
                            [Default setValue:number_id forKey:NUMBERID];
                            [Default setValue:contactName forKey:Selected_Department];
                            [Default setValue:imagename forKey:Selected_Department_Flag];
                            [_lbl_selected_name1 setText:contactName];
                            _img_selected_image.image = [UIImage imageNamed:imagename];
                            _img_selected_image.layer.cornerRadius = 10.0;
                        }
                        else
                        {
                        }
                        [UtilsClass callinge_from_number_check];
                    }
                }
            }
        }
        
        [self search_number];
    }
    else {
        [Default removeObjectForKey:@"extraHeader"];
        [self callFire:sender];
    }
        
    }
        else
        {
            //_txt_call_number_text.text = [NSString stringWithFormat:@"+%@",_txt_call_number_text.text];
            [UtilsClass showAlert:kDUMMYNUMBERMSG contro:self];
        }
        
    }
    else{
        
       // _txt_call_number_text.text = [NSString stringWithFormat:@"+%@",_txt_call_number_text.text];
        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
    }
}
-(void)callFire:(id)sender{
    
    //
    if([UtilsClass isNetworkAvailable])
    {
//        purchase_number = [[GlobalData sharedGlobalData] get_number_selection];
        
        _txt_call_number_text.text = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        if(purchase_number.count > 0 || _txt_call_number_text.text.length == 3 || _txt_call_number_text.text.length == 4)
        {
            
            _txt_call_number_text.text = [NSString stringWithFormat:@"+%@",_txt_call_number_text.text];
            NSString *numbVer = [Default valueForKey:ISDummyNumber];
            int num = [numbVer intValue];
            if (num == 1 || _txt_call_number_text.text.length == 4 || _txt_call_number_text.text.length == 5)
            {
                
                NSString *numbVer = [Default valueForKey:SELECTED_NUMBER_VERIFY];
                int num = [numbVer intValue];
                if (num == 1 || _txt_call_number_text.text.length == 4 || _txt_call_number_text.text.length == 5) {
                NSString *credit = [Default valueForKey:CREDIT];
                float cre = [credit floatValue];
                    
                if(cre > 0 || [[Default valueForKey:kIsCustomPlan] intValue] == 1 ){
                    switch ([[AVAudioSession sharedInstance] recordPermission]) {
                        case AVAudioSessionRecordPermissionGranted:
                        {
//                            NSString *ipcode  =  [Default valueForKey:CALLING_CODE];
                            NSString *contactcode  = @"";
                            NSString *callnumber  = @"";
                            NSArray *arr = [_txt_call_number_text.text componentsSeparatedByString:@"+"];
                            if(arr.count != 1)
                            {
                                call_number = [NSString stringWithFormat:@"%@",arr[1]];
                                contactcode  =  [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"+%@",call_number]];
                                callnumber = [NSString stringWithFormat:@"%@",call_number];
                            }
                            else
                            {
                                call_number = _txt_call_number_text.text;
                                contactcode  =  [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"+%@",call_number]];
                                callnumber = [NSString stringWithFormat:@"+%@",call_number];
                            }
//                            NSString *countryCode = [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"+%@",call_number]];
//                            NSLog(@"Print out Going Call Provider : %@",countryCode);
//                            if([ipcode isEqualToString:[NSString stringWithFormat:@"+%@",contactcode]])
//                            {
//                                if([ipcode isEqualToString:@"+91"])
//                                {
//                                    [UtilsClass showAlert:CALLBAND contro:self];
//                                }
//                                else
//                                {
//                                    [self callme:sender];
//                                }
//                            }
//                            else
//                            {
                            
                            NSData *outGoingCallData = [[NSUserDefaults standardUserDefaults] objectForKey:OUTGOING_CALL_COUNTRY];
                            NSArray *outGoingCallRest = [NSKeyedUnarchiver unarchiveObjectWithData:outGoingCallData];
                            
                            for (int i = 0; i<outGoingCallRest.count; i++)
                            {
                                if ([_txt_call_number_text.code isEqualToString:[NSString stringWithFormat:@"%@",outGoingCallRest[i][@"code"]]] && call_number.length != 3 && call_number.length != 4){
                                    [UtilsClass showAlert:@"Calls to this country are restricted." contro:self];
                                    return;
                                }
                            }
                            
                                if([contactcode isEqualToString:@"invalid_number"])
                                {
                                    if (call_number.length != 3 && call_number.length != 4){
                                        [UtilsClass makeToast:@"The dialed number might be Invalid."];
                                    }
                                    [self callme:sender];
                                }
                                else
                                {
                                    [self callme:sender];
                                }
//                            }
                            break;
                        }
                        case AVAudioSessionRecordPermissionDenied:
                        {
                            [self micPermiiton];
                            break;
                        }
                        case AVAudioSessionRecordPermissionUndetermined:
                            break;
                        default:
                            break;
                            
                    }}else {
                        [UtilsClass showAlert:kCREDITLAW contro:self];
                    }}
                else{ [UtilsClass showAlert:kNUMBERVERIFYMSG contro:self]; }
            }
            else
            {
                [UtilsClass showAlert:kDUMMYNUMBERMSG contro:self];
            }
            
        }else{
                        
                        [UtilsClass showAlert:kNUMBERASSIGN contro:self];
                    }
    }
    else
    {
        [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
    }
}
-(void)callme:(id)sender
{
    NSString *address = @"";
    NSString *number = @"";
    NSArray *arr = [_txt_call_number_text.text componentsSeparatedByString:@"+"];
    if(arr.count != 1)
    {
        address = [NSString stringWithFormat:@"+%@",call_number];
        number = [NSString stringWithFormat:@"+%@",call_number];
    }
    else
    {
        address = [NSString stringWithFormat:@"+%@",call_number];
        number = [NSString stringWithFormat:@"+%@",call_number];
    }
    
    address = [address stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *numbStr = @"";
    numbStr = [NSString stringWithFormat:@"%@",address];
    NSString *name = @"";
    NSString *string = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString *string1 = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *string2 = [string1 stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *string3 = [string2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //pri
    [Default setValue:string3 forKey:klastDialNumber];
    [Default setValue:_txt_call_number_text.country.name forKey:klastDialCountryName];
    [Default setValue:[NSString stringWithFormat:@"+%@",_txt_call_number_text.code] forKey:klastDialCountryCode];
    
    string3 = [string3 stringByReplacingOccurrencesOfString:@"+" withString:@""];
     NSPredicate *filterint = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@ ",string3];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",string3];
    
    //4.4 added
    NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[filter, filterint]];

    NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicate_final];
    
//    name = number;
    name =  [string2 stringByReplacingOccurrencesOfString:@" " withString:@""];
 
    
    
    if(filteredContacts.count != 0)
    {
        NSDictionary *dic = [filteredContacts objectAtIndex:0];
        NSString *str = [dic valueForKey:@"name"];
        for (int i = 0; i<filteredContacts.count; i++)
        {
            
            //p NSString *numbers = [filteredContacts[i][@"number"] stringByReplacingOccurrencesOfString:@"+" withString:@""];
            
            NSString *numbers;
            
            if ([[[filteredContacts objectAtIndex:i] valueForKey:@"numberArray"] count]>0) {
                
                numbers = [[[[[filteredContacts objectAtIndex:i] valueForKey:@"numberArray"]objectAtIndex:0] valueForKey:@"number"]  stringByReplacingOccurrencesOfString:@"+" withString:@""];
            }
            else
            {
                numbers = @"";
            }
            if ([call_number isEqualToString:numbers]) {
                extentionName = filteredContacts[i][@"name"];
                break;
            }
        }
        
        //p if([string3 isEqualToString:[dic valueForKey:@"number_int"]])
        
        
        if ([[dic valueForKey:@"numberArray"] count]>0) {
            
            if([string3 isEqualToString:[[[[dic valueForKey:@"numberArray"]objectAtIndex:0] valueForKey:@"number"]  stringByReplacingOccurrencesOfString:@"+" withString:@""]] || [string3 isEqualToString:[[[[dic valueForKey:@"numberArray"]objectAtIndex:0] valueForKey:@"number_int"]  stringByReplacingOccurrencesOfString:@"+" withString:@""]])
            {
                name = str;
                
               // if ([Default boolForKey:kIsNumberMask] == true) {
                    
                 //   name = [UtilsClass get_masked_number:str];
              //  }
                
                if(!dic[@"_id"])
                {
                    NSString *ContactName = [dic valueForKey:@"name"];
                    //p NSString *ContactNumber = [dic valueForKey:@"number"];
                    NSString *ContactNumber = [[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"];
                    [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
                }
            }
        }
        
    }else{
        extentionName = @"";
    }
    
    
    if (call_number.length == 3 || call_number.length == 4){
        NSString *extentionPlan = [Default valueForKey:EXTENSION_PLAN];
        int num = [extentionPlan intValue];
        if (num == 0){
            [UtilsClass showAlert:@"Internal team calling is not available in your plan Please Upgrade your plan" contro:self];
            return;
        }
        
        NSString *ownExtNumber = [Default valueForKey:ownExtensionNumber];
        if ([ownExtNumber isEqualToString:call_number]) {
            [UtilsClass makeToast:@"You can't dial own extension number."];
            return;
        }
        
        BOOL invalidExtention = false;
        for (int i = 0; i<activeSubuser.count; i++)
        {
            //NSLog(@"extensionNumber  :%@",activeSubuser[i][@"extensionNumber"]);
            
            if ([call_number isEqualToString:[NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]]]){
                invalidExtention = false;
                [Default setValue:@"true" forKey:ExtentionCall];
                [Default setValue:@"twilio" forKey:ExtentionCallProvider];
                if ([extentionName isEqualToString:@""]) {
                    extentionName = activeSubuser[i][@"fullName"];
                }
                extentionNumber = [NSString stringWithFormat:@"%@",activeSubuser[i][@"extensionNumber"]];
                [Default setValue:@"+1" forKey:klastDialCountryCode];
                [Default setValue:@"United States" forKey:klastDialCountryName];
                [self twlio_calling:address name:name numbStr:activeSubuser[i][@"twilioEndpoint"][@"webIdentity"]];
                break;
            }else{
                invalidExtention = true;
            }
        }
        
        if (invalidExtention == true) {
            [UtilsClass makeToast:@"Please enter valid extension number."];
        }
        
    }else{
        [Default setValue:@"false" forKey:ExtentionCall];
        [Default setValue:@"" forKey:ExtentionCallProvider];
        NSString *calling_number = [Default valueForKey:SELECTEDNO];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
        NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
        NSDictionary *numberdic = [numbergetarr objectAtIndex:0];
        NSDictionary *dic = [numberdic valueForKey:@"number"];
        NSString *twilioVerify = [dic valueForKey:@"twilioVerify"];
        int twlVeri = [twilioVerify intValue];
        
// <<<<<<< HEAD
       /*
        old
        NSString *providerStr = [dic valueForKey:@"outgoingCallProvider"];
        if ([providerStr isEqualToString:@""] || providerStr == nil ) {
======= */
            
        NSString *fromShortCode = [dic valueForKey:@"shortName"];
        NSString *toShortCode = _txt_call_number_text.country.countryCode;
       // >>>>>>> 1863855b4e30100399e3bad6164c2cbbec168c8b
               
               BOOL initiateCall = false;
               
        /*       if ([fromShortCode isEqualToString:@"IN"] && [[dic valueForKey:@"provider"] isEqualToString: @"airtel"]) {
                   
                   initiateCall = false;
                   
                   if ([fromShortCode isEqualToString:toShortCode]) {
                       
                      
                       [UtilsClass outboundc2cAPICall:address provider:[dic valueForKey:@"provider"] phoneNum:calling_number];
                   }
                   else
                   {
                       [UtilsClass makeToast:@"You can't make calls using india number to any other country"];
                   }
               }
        else
        {*/
            if ([fromShortCode isEqualToString:toShortCode]) {
                
               if([GlobalData getContryRestrictedForSameCallId:fromShortCode] == true)
               {
                   initiateCall = false;
                    [UtilsClass showAlert:@"Calling with the same caller id is not allowed" contro:self];
               }
                else
                {
                    initiateCall = true;
                }
            }
            else
            {
                initiateCall = true;
            }
       // }
       
        

        if (initiateCall == true) {
            
          
            
            NSString *frontendprovider = [GlobalData getCallingProvider:_txt_call_number_text.country.countryCode];
            
            NSLog(@"final provider :: %@",frontendprovider);
            
            if ([frontendprovider isEqualToString:@"freeswitch"]) {
                
               // [self initiateSIPCalling:sender];
                [self linphone_call:address name:name numbStr:numbStr];
            }
            else if([frontendprovider isEqualToString:@"plivo"]){
                [self provider_vise_call:address name:name numbStr:numbStr];
            }
            else
            {
                [self twlio_calling:address name:name numbStr:numbStr];
            }
            
        }
        
        
        }
        

    
}
//-(void)number_api_call:(NSString *)address name:(NSString *)name numbStr:(NSString *)numbStr
//{
//    NSString *calling_number = [Default valueForKey:SELECTEDNO];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.number.phoneNumber == %@",calling_number];
//    NSArray *numbergetarr = [purchase_number filteredArrayUsingPredicate:predicate];
//    if(numbergetarr.count != 0)
//    {
//        NSDictionary *numberdic = [numbergetarr objectAtIndex:0];
//        NSDictionary *dic = [numberdic valueForKey:@"number"];
//        NSString *outgoingCallProvider = [dic valueForKey:@"outgoingCallProvider"];
//        NSLog(@"*** outgoingCallProvider : number_api_call D : %@",outgoingCallProvider);
//        if ([outgoingCallProvider isEqualToString:Login_Twilio])
//        {
//            [self twlio_calling:address name:name numbStr:numbStr];
//        }
//        else
//        {
//            [self provider_vise_call:address name:name numbStr:numbStr];
//        }
//    }
//}
-(void)twlio_calling:(NSString *)address name:(NSString *)name numbStr:(NSString *)numbStr
{
    if ([address length] > 0)
    {
        if([UtilsClass isNetworkAvailable])
        {
            NSString *calling_provider = @"";
            calling_provider = Login_Twilio;
            [Default setValue:calling_provider forKey:CallingProvider];
            [Default synchronize];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            OnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
            //NewOnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

            vc.CallStatus = OUTGOING;
            vc.CallStatusfinal = OUTGOING;
            if (call_number.length == 3 || call_number.length == 4){
                vc.ContactName =  extentionName;
                vc.ContactNumber = extentionNumber;
            }else{
                vc.ContactName =  name;  //[name stringByReplacingOccurrencesOfString:@" " withString:@""];
                vc.ContactNumber = numbStr;//[[NSString stringWithFormat:@"+%@",call_number] stringByReplacingOccurrencesOfString:@" " withString:@""];
            }
            vc.IncCallOutCall = @"Outgoing";
            vc.Plivo_outgoingcall = outCall;
            NSUUID *uuid = [NSUUID UUID];
            NSString *handle = numbStr;
            [twilio_callkit sharedInstance].calls_uuids_twilio = [[NSMutableArray alloc] init];
            [[twilio_callkit sharedInstance] performStartCallActionWithUUID:uuid handle:handle];
            if (IS_IPAD) {
                vc.popoverPresentationController.sourceView = self.view;
                [self presentViewController:vc animated:YES completion:nil];
            } else {
                vc.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:vc animated:true completion:nil];
            }
        }
        else
        {
            [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
        }
    }
    
}
-(void)provider_vise_call:(NSString *)address name:(NSString *)name numbStr:(NSString *)numbStr
{
//    NSString *LoginProvider = [Default valueForKey:Login_Provider];
//    if([LoginProvider isEqualToString:Login_Plivo])
//    {
        if ([address length] > 0) {
            
            if([UtilsClass isNetworkAvailable])
            {
                NSString *calling_provider = @"";
                calling_provider = Login_Plivo;
                [Default setValue:calling_provider forKey:CallingProvider];
                [Default synchronize];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                OnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
                //NewOnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewOnCallVC"];

                vc.CallStatus = OUTGOING;
                vc.CallStatusfinal = OUTGOING;
                vc.ContactName = name; //[name stringByReplacingOccurrencesOfString:@" " withString:@""];
                vc.ContactNumber = [numbStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                vc.IncCallOutCall = @"Outgoing";
                vc.Plivo_outgoingcall = outCall;
                [Lin_Utility plivo_call_action:numbStr];
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = self.view;
                    [self presentViewController:vc animated:YES completion:nil];
                } else {
                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                    [self presentViewController:vc animated:true completion:nil];
                }
            }
            else
            {
                [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
            }
        }
    //}
    
}

- (IBAction)btn_celar_text:(id)sender {
}
- (IBAction)btn_dialpad_number_click:(id)sender
{
    if (!_txt_call_number_text.editing)
    {
        UITextPosition *positionBeginning = [_txt_call_number_text endOfDocument];
        UITextRange *textRange =[_txt_call_number_text textRangeFromPosition:positionBeginning
                                                                  toPosition:positionBeginning];
        [_txt_call_number_text setSelectedTextRange:textRange];
    }
    if([sender tag] == 1)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@1",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"1"];
        
    }
    else if ([sender tag] == 2)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@2",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"2"];
    }
    else if ([sender tag] == 3)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@3",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"3"];
    }
    else if ([sender tag] == 4)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@4",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"4"];
    }
    else if ([sender tag] == 5)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@5",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"5"];
        
    }
    else if ([sender tag] == 6)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@6",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"6"];
    }
    else if ([sender tag] == 7)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@7",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"7"];
    }
    else if ([sender tag] == 8)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@8",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"8"];
    }
    else if ([sender tag] == 9)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@9",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"9"];
    }
    else if ([sender tag] == 10)
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@*",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"*"];
        
    }
    else if ([sender tag] == 11)
    {
        //        _txt_call_number_text.text =  [NSString stringWithFormat:@"%@0",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"0"];
    }
    else
    {
        //        _txt_call_number_text.text = [NSString stringWithFormat:@"%@#",_txt_call_number_text.text];
        [_txt_call_number_text insertText:@"#"];
    }
    

        [self search_number];
    //    [self textformate];
    if([_txt_call_number_text.text isEqualToString:@""])
    {
        _txt_call_number_text.userInteractionEnabled = false;
    }
    else
    {
        _txt_call_number_text.userInteractionEnabled = true;
    }
}
- (IBAction)btn_send_message:(id)sender
{
    NSString *smsDisp = [Default valueForKey:kSMSRIGHTS];
    int num = [smsDisp intValue];
    if (num == 0) {
        NewsmsVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"NewsmsVC"];
        vc.controller = @"dialerVC";
        vc.strFromNumber = @"";
        vc.numberfromothercontroller = [NSString stringWithFormat:@"%@",_txt_call_number_text.text ] ;
        vc.VcFrom = @"DialerVC";
        vc.namefromothercontroller = @"";
        [[self navigationController] pushViewController:vc animated:YES];
    }else{
        [UtilsClass showAlert:kSMSMODUALMSG contro:self];
    }
}
- (IBAction)btn_new_contact:(id)sender
{
//    if([Default boolForKey:kIsNumberMask] == false)
//    {
    AddEditContactVC *vc = [[self storyboard] instantiateViewControllerWithIdentifier: @"AddEditContactVC"];
    vc.controller = @"addFromDialer";
    vc.number = [NSString stringWithFormat:@"%@",_txt_call_number_text.text ];
    NSLog(@"NUmber : %@",vc.number);
    [[self navigationController] pushViewController:vc animated:YES];
//    }
//    else
//    {
//
//               [UtilsClass makeToast:@"number can not be edited as number masking is on , please contact your admin ."];
//
//    }
}

-(void)search_number
{
   
    NSLog(@"search called");
   
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        
    // Perform the sorting
        __block int displayNewContact;
         
        bool isTableDisplay = false;
        
        //check for crash
//    NKVPhonePickerTextField *txtText = [[NKVPhonePickerTextField alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
//    txtText.text = @"";
//    [txtText insertText:_txt_call_number_text.text];
//    NSString *codeName = txtText.code ? txtText.code : @"";
   
        NSString *codeName = self->_txt_call_number_text.code ? self->_txt_call_number_text.code : @"";
        
        NSString *string = [self->_txt_call_number_text.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString *string1 = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *string2 = [string1 stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *string0 = [string2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *string3 = [string0 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    string3 = [string3 stringByReplacingOccurrencesOfString:@"*" withString:@""];
    string3 = [string3 stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
   NSString *CountryCodeText = [_txt_call_number_text.text stringByReplacingOccurrencesOfString:codeName withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@"(" withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@")" withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@"-" withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@" " withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@"+" withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@"*" withString:@""];
             CountryCodeText = [CountryCodeText stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    NSLog(@"CountryCodeText :: %@",CountryCodeText);
    
    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *characterSetInverted = [characterSet invertedSet];
    NSArray *arrayOfComponents =
    [string3 componentsSeparatedByCharactersInSet:characterSetInverted];
    string3 = [arrayOfComponents componentsJoinedByString:@""];
    NSUInteger length = [string3 length];
 /*   if(length > 3)
    {
        if([string3 isEqualToString:@""])
        {
            if([string3 isEqualToString:@""]) {
//                self->_btnSMS.hidden = true;
//                self->_btnNewContact.hidden = true;
                displayNewContact = 0;
            }else{
//                self->_btnSMS.hidden = false;
//                self->_btnNewContact.hidden = false;
                displayNewContact = 1;
            }
            
            //pcrash  self->_tbl_contact_search.hidden = true;
            isTableDisplay = false;
            //pp
          //4.4  [self->_tbl_contact_search reloadData];
        }
        else
        {
                NSMutableArray *parr = [NSMutableArray array];
                if(CountryCodeText.length < 6)
                {
                    NSArray *combination_array = [[KSTokenView alloc] letterCombinations:CountryCodeText];
                    for (id locaArrayObject in combination_array) {
                        [parr addObject:[NSPredicate predicateWithFormat:@"name contains[c] %@ ",locaArrayObject]];
                    }
                }
                if(string3.length > 3)
                {

//p                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"number_int contains[c] %@",string3];
                    
                   
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",string3] ;
                     NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",string3] ;
                    
                    
                    if(CountryCodeText.length < 6)
                    {
                        NSPredicate *predicate1 = [NSCompoundPredicate orPredicateWithSubpredicates: parr];
                        NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate, predicate1,predicate2]];
                        
                        
                        self->filteredData = [self->Mixallcontact filteredArrayUsingPredicate:predicate_final];
                        
                       // NSLog(@"filtered arr:: %@",self->filteredData);
                        
                    }
                    else
                    {
                        self->filteredData = [self->Mixallcontact filteredArrayUsingPredicate:predicate];
                    }
                    if(self->filteredData.count != 0)
                    {
                       //pcrash self->_tbl_contact_search.hidden = false;
                        isTableDisplay = true;
//                        self->_btnSMS.hidden = false;
//                        self->_btnNewContact.hidden = false;
                        displayNewContact = 1;
                        //pp
                      //4.4  [self->_tbl_contact_search reloadData];
                    }
                    else
                    {
                        if([string3 isEqualToString:@""]) {
//                            self->_btnSMS.hidden = true;
//                            self->_btnNewContact.hidden = true;
                            displayNewContact = 0;
                        }else{
//                            self->_btnSMS.hidden = false;
//                            self->_btnNewContact.hidden = false;
                            displayNewContact = 1;
                        }
                      //pcrash  self->_tbl_contact_search.hidden = true;
                        isTableDisplay = false;
                        //pp
                    //4.4    [self->_tbl_contact_search reloadData];
                    }

                }
            }
        }
        else
        {
            if([string3 isEqualToString:@""]) {
//                self->_btnSMS.hidden = true;
//                self->_btnNewContact.hidden = true;
                displayNewContact = 0;
            }
          //pcrash  self->_tbl_contact_search.hidden = true;
            isTableDisplay = false;
         //pp
          //4.4  [self->_tbl_contact_search reloadData];
        }

        
    
        */
        
        if (length > 3) {
            
            NSMutableArray *parr = [NSMutableArray array];
            
            if(CountryCodeText.length < 6)
            {
                NSArray *combination_array = [[KSTokenView alloc] letterCombinations:CountryCodeText];
                for (id locaArrayObject in combination_array) {
                    [parr addObject:[NSPredicate predicateWithFormat:@"name contains[c] %@ ",locaArrayObject]];
                }
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",string3] ;
             NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",string3] ;
            
            
//            if(CountryCodeText.length < 6)
//            {
                NSPredicate *predicate1 = [NSCompoundPredicate orPredicateWithSubpredicates: parr];
                NSPredicate *predicate_final = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate, predicate1,predicate2]];
                
                
                self->filteredData = [self->Mixallcontact filteredArrayUsingPredicate:predicate_final];
                
               // NSLog(@"filtered arr:: %@",self->filteredData);
                
//            }
//            else
//            {
//                self->filteredData = [self->Mixallcontact filteredArrayUsingPredicate:predicate];
//            }
            
            NSLog(@"filtered arr:: %@",self->filteredData);
            if(self->filteredData.count != 0)
            {
              
                isTableDisplay = true;

                displayNewContact = 1;
               
            }
            else
            {
                isTableDisplay = false;
                displayNewContact = 1;
            }
        }
        else
        {
            if ([string3 isEqualToString:@""]) {
                isTableDisplay = false;
                displayNewContact = 0;
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void) {
                // Tell the main thread I'm done.
            
          
            
            if (isTableDisplay == true) {
                self->_tbl_contact_search.hidden = false;
              //  NSLog(@"result display");

            }
            else
            {
                self->_tbl_contact_search.hidden = true;
            }
            
            
            [self->_tbl_contact_search reloadData];
            
            if ((displayNewContact = 1)) {
                self->_btnSMS.hidden = false;
                self->_btnNewContact.hidden = false;
            }
            else
            {
                self->_btnSMS.hidden = true;
                self->_btnNewContact.hidden = true;
            }
            });
        
        
        });

    
}

-(void)textformate
{
    NSInteger length = [self getLength:_txt_call_number_text.text];
    if ([_txt_call_number_text.text hasPrefix:@"1"]) {
        if(length == 11)
        {
        }
        if(length == 4)
        {
            NSString *num = [self formatNumber:_txt_call_number_text.text];
            _txt_call_number_text.text = [NSString stringWithFormat:@"%@ (%@) ",[num substringToIndex:1],[num substringFromIndex:1]];
            UITextPosition *positionBeginning = [_txt_call_number_text endOfDocument];
            UITextRange *textRange =[_txt_call_number_text textRangeFromPosition:positionBeginning
                                                                      toPosition:positionBeginning];
            [_txt_call_number_text setSelectedTextRange:textRange];
        }
        else if(length == 7)
        {
            NSString *num = [self formatNumber:_txt_call_number_text.text];
            NSRange numRange = NSMakeRange(1, 3);
            _txt_call_number_text.text = [NSString stringWithFormat:@"%@ (%@) %@-",[num substringToIndex:1] ,[num substringWithRange:numRange],[num substringFromIndex:4]];
            UITextPosition *positionBeginning = [_txt_call_number_text endOfDocument];
            UITextRange *textRange =[_txt_call_number_text textRangeFromPosition:positionBeginning
                                                                      toPosition:positionBeginning];
            [_txt_call_number_text setSelectedTextRange:textRange];
        }
        
    } else {
        if(length == 10)
        {
        }
        if(length == 3)
        {
            NSString *num = [self formatNumber:_txt_call_number_text.text];
            _txt_call_number_text.text = [NSString stringWithFormat:@"(%@) ",num];
            UITextPosition *positionBeginning = [_txt_call_number_text endOfDocument];
            UITextRange *textRange =[_txt_call_number_text textRangeFromPosition:positionBeginning
                                                                      toPosition:positionBeginning];
            [_txt_call_number_text setSelectedTextRange:textRange];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:_txt_call_number_text.text];
            _txt_call_number_text.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
            UITextPosition *positionBeginning = [_txt_call_number_text endOfDocument];
            UITextRange *textRange =[_txt_call_number_text textRangeFromPosition:positionBeginning
                                                                      toPosition:positionBeginning];
            [_txt_call_number_text setSelectedTextRange:textRange];
        }
    }
//    UITextRange *selRange = _txt_call_number_text.selectedTextRange;
//    UITextPosition *selStartPos = selRange.start;
//    NSInteger idx =  [_txt_call_number_text offsetFromPosition:_txt_call_number_text.beginningOfDocument toPosition:selStartPos];
}
-(NSString*)phoneNumber_formate:(NSString*)lastcall prifix:(NSString*)prifix
{
    static NSCharacterSet* set = nil;
    if (set == nil){
        set = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    }
//    NSString* phoneString = [[lastcall componentsSeparatedByCharactersInSet:set] componentsJoinedByString:@""];
//    NSString *contactcode  =  [UtilsClass phonenumbercheck:[NSString stringWithFormat:@"+%@",phoneString]];
    NSString *result = lastcall;
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:result
                                 defaultRegion:prifix error:&anError];
    if (anError == nil) {
        return [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatNATIONAL error:&anError];
    } else {
        return result;
    }
}
- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
    }
    return mobileNumber;
}
- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    return length;
}
-(void)micPermiiton {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAlertTitle message:@"Please go to settings and turn on Microphone service for incoming/outgoing calls." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    UIAlertAction *seting = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                NSLog(@"Opened url");
            }
        }];
    }];
    [alert addAction:ok];
    [alert addAction:seting];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)ip_address_check
{
//    NSString *Calling_Code = @"";
//    NSLog(@"Calling_Code : %@",Calling_Code);
//    [[NSUserDefaults standardUserDefaults] setValue:Calling_Code forKey:CALLING_CODE];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    [self.view endEditing:true];
    if (touch.view == _view_tab_click)
    {
        NSLog(@"click");
    }
    else
    {
        [self view_selectnumber_down];
    }
}
-(void)getCredits {
    
    
//    NSLog(@"getCredits : Trushang : Api Call : ");
    NSString *userID = [Default valueForKey:USER_ID];
    NSString *Device_Info = [Default valueForKey:IS_DEVICEINFO] ? [Default valueForKey:IS_DEVICEINFO] : @"";
//    NSString *Version_Info = [Default valueForKey:IS_VERSIONINFO] ? [Default valueForKey:IS_VERSIONINFO] : @"";
//    NSString *Auth_Token = [Default valueForKey:AUTH_TOKEN] ? [Default valueForKey:AUTH_TOKEN] : @"";
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *url = [NSString stringWithFormat:@"credit/%@/view",userID];
    NSDictionary *passDict = @{@"userId":userID,
                               @"device":@"ios",
                               @"deviceInfo":Device_Info,
                               @"token":fcmtoken,
                               @"versionInfo":VERSIONINFO,};
    NSLog(@"getcredit : %@",passDict);
    NSLog(@"getcredit : %@%@",SERVERNAME,url);
    NSLog(@"getcredit auth token: %@",[Default valueForKey:AUTH_TOKEN]);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getCredits:response:) andDelegate:self];
}

- (void)getCredits:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"TRUSHANG : STATUSCODE **************22  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        //[UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
            // NSLog(@"Trushang : getCredits : %@",response1);
        NSLog(@"Encrypted Response : get credit : %@",response1);
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            lastCallData  = response1[@"lastCallData"];
            NSString *Provider = [response1 valueForKey:@"mobileEndPointData"][@"provider"];
            NSString *LoginProvider = [Default valueForKey:Login_Provider];
            [Default setValue:response1[@"autoSwitchPlan"] forKey:IS_AUTO_SWITCH_PLAN];
            [Default setValue:response1[@"autoSwitch"] forKey:IS_AutoSwitch];
            [Default setValue:response1[@"defaultProvider"] forKey:DefaultProvider];
            [Default setValue:response1[@"isDisableSms"] forKey:kSMSRIGHTS];
           // [Default setValue:response1[@"thinqValue"] forKey:kTHINQVALUE];
            [Default setValue:response1[@"thinqAccess"] forKey:kTHINQACCESS];
            [Default synchronize];
            endpointUserName = [response1 valueForKey:@"mobileEndPointData"][@"username"];
            endpointPassWord = [response1 valueForKey:@"mobileEndPointData"][@"password"];
           //rem [self addThinqSwitch];
            
            
            if([Provider isEqualToString:Login_Plivo])
            {
                [[Phone sharedInstance] setDelegate:self];
            }
            
            
//            else
//            {
//                if(![Provider isEqualToString:Login_Plivo])
//                {
//                    if(response1[@"lastEndpointUpdateTime"] != nil && response1[@"lastOutCallPriceUpdateTime"] != nil)
//                    {
//                        if(![response1[@"lastEndpointUpdateTime"] isEqualToString:@""] && ![response1[@"lastOutCallPriceUpdateTime"] isEqualToString:@""])
//                        {
//                            [self getEndPoint:response1[@"lastEndpointUpdateTime"]];
//                            [self getprovider:response1[@"lastOutCallPriceUpdateTime"]];
//                        }
//
//                    }
//                }
//            }
            
           // [[GlobalData sharedGlobalData] setCallCountryProviderList:[response1 valueForKey:@"callCountriesProvider"]] ;

            NSDictionary *dic = response1[@"mobileEndPointData"];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"credit"] forKey:CREDIT];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"callHold"] forKey:IS_HOLD];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"callHoldByApi"] forKey:IS_HOLD_BY_API];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"DTMFByApi"] forKey:IS_DTMF_BY_API];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"callTransfer"] forKey:IS_TRANSFER];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"callReminder"] forKey:IS_CALL_REMINDER];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"isAcwEnabled"] forKey:IS_ACW_ENABLE];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"planCallReminder"] forKey:IS_CALL_PLANNER];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"acwPlan"] forKey:ACW_PLAN];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"extensionPlan"] forKey:EXTENSION_PLAN];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"record"] forKey:IS_RECORDING];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"displayTimezone"] forKey:IS_DISPLAY_TIME];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"timezone"] forKey:IS_DISPLAY_TIME_PLAN];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"isUserLive"] forKey:IS_USER_LIVE];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"defaultNumberPlan"] forKey:IS_DEFAULT_NUMBER_PLAN];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"userLastCallDetail"] forKey:USER_LAST_CALL_DETAIL];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"isLastCallDetailInPlan"] forKey:IS_LAST_CALL_DDETAIL_IN_PLAN];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"acwDuration"] forKey:ACW_TIME];
            [[NSUserDefaults standardUserDefaults] setValue:response1[@"transferForTwilio"] forKey:TRANSFER_FOR_TWILIO];
            if(response1[@"networkLowCreditLimit"] != nil) {
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",response1[@"networkLowCreditLimit"]] forKey:Network_Speed_Value];
            } else {
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"50"] forKey:Network_Speed_Value];
            }
            
            
            [[NSUserDefaults standardUserDefaults] setValue:[response1 valueForKey:@"feedbackForCalls"] forKey:kISFeedbackInPlan];
            
            [[NSUserDefaults standardUserDefaults] setValue:[response1 valueForKey:@"callTagging"] forKey:kISTaggingInPlan];
            
            [[NSUserDefaults standardUserDefaults] setValue:[response1 valueForKey:@"callNote"] forKey:kISNotesInPlan];
            
            
            [[NSUserDefaults standardUserDefaults] setValue:[response1 valueForKey:@"isCustomPlan"] forKey:kIsCustomPlan];
            
            [Default setValue:[response1  valueForKey:@"telephonyProviderSwitch"] forKey:kTelephonyProviderSwitch];
            [Default setValue:[response1  valueForKey:@"frontend_switch_A"] forKey:kProviderSwitchA];
            [Default setValue:[response1  valueForKey:@"frontend_switch_B"] forKey:kProviderSwitchB];
            
            [Default setValue:[response1 valueForKey:@"iosFreeSwitchSipUser"] forKey:kFreeswitchUsername];
            [Default setValue:[response1 valueForKey:@"iosFreeSwitchSipPass"] forKey:kFreeswitchPassword];
            [Default setValue:[response1 valueForKey:@"iosFreeSwitchDomain"] forKey:kFreeswitchDomain];

            
            
//            linphone_core_clear_proxy_config(LC);
//            linphone_core_clear_all_auth_info(LC);
            
    
           
         /*  if ([Default valueForKey:kFreeswitchUsername]) {
                
             //rem  [LinphoneManager.instance startLinphoneCore];

               
                LinphoneProxyConfig *config = linphone_core_get_default_proxy_config(LC);
               
               if (config != nil) {
                   
                   switch (linphone_proxy_config_get_state(config)) {
                       case LinphoneRegistrationNone:
                           [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                           break;
                       case LinphoneRegistrationFailed:
                           [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                           break;
                           
                       default:
                           break;
                   }
                   
              /*     LinphoneRegistrationState  state = linphone_proxy_config_get_state(config);
                  
                  // NSLog(@"state counted :; %u",state);
                   
                   if (state == LinphoneRegistrationNone || state == LinphoneRegistrationFailed) {
                       
                   NSLog(@"Need to relogin in linphone");
                       
                  [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
               }*/
           
               
                    
                   
                    
          //      }
                
         //   }
            
            
            NSLog(@"telephony in view cerdit >> %@",[response1 valueForKey:@"telephonyProviderSwitch"]);

          /*  [Default setValue:[response1 valueForKey:@"disableRecordingAutomation"] forKey:kdisableRecordingAutomation];
            [Default setValue:[response1 valueForKey:@"autoPauseRecordingAutomation"] forKey:kautoPauseRecordingAutomation];
           
           NSLog(@"disable :: %@",[Default valueForKey:kdisableRecordingAutomation]);*/
            
        [Default setBool:[[response1 valueForKey:@"recordingPartialRights"]boolValue] forKey:kListenTocallRecordRights];
            
                    
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *numbVer = [Default valueForKey:IS_DISPLAY_TIME];
            int isDisplayTime = [numbVer intValue];
            if (isDisplayTime == 1) {
                _lbl_time_date.hidden = false;
                _img_country.frame =  CGRectMake(79,
                                                 8,
                                                 25,
                                                 25);
            }else {
                _lbl_time_date.hidden = true;
                _img_country.frame =  CGRectMake((self.view.frame.size.width/2) - (25/2),
                                                 (self.viewTimeZone.frame.size.height / 2) - (25/2),
                                                 25,
                                                 25);
            }
            

            [[GlobalData sharedGlobalData] setCallCountryProviderList:[response1 valueForKey:@"callCountriesProvider"]];
            
            
            [self setRestrictedCountryList];
            
            int isBlocklistupdated = [[response1 valueForKey:@"isBlockNumberUpdate"] intValue];
            //NSLog(@"is blacklisted number update:: %d",isBlocklistupdated);

            if (isBlocklistupdated == 1) {
                
                [[GlobalData sharedGlobalData] getUpdatedBlackListedArray];
                
                //NSLog(@"updated blacklisted number :: %@",[Default valueForKey:kblackListedArray]);
            }
            

            [self purchaseNumber:response1];
            
            NSString *isnumberupdate = [response1 valueForKey:@"isNumberUpdate"];
            int num = [isnumberupdate intValue];

            if (num == 1) {
                
                //if number update true then get data from number selection
                [[GlobalData sharedGlobalData] get_Updated_Purchased_Number];
                
            }
            [self getNumbers];
            [self lastdialcountry:response1];
            
            //get updated contact
           
            NSLog(@"anymobile flag :: %@",[response1 valueForKey:@"anyMobileContacts"]);
            
//            if ([[response1 valueForKey:@"anyMobileContacts"] intValue]  == 1 && [Default boolForKey:kisContactStoredInDB] == true ) {
        
            
            if ([[response1 valueForKey:@"anyMobileContacts"] intValue]  == 1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
               // [UtilsClass makeToast:@" Syncing contacts this may take upto few minutes,Please don't close the app."];
                   
                [Default setValue:[NSDate date] forKey:@"updatestartdate"];
                [GlobalData getCallhippoUpdatedContacts:@"0"];
                [Default setBool:true forKey:kNeedToUpdateContact];
                    
                });
            }
            else
            {
               // [Default setBool:false forKey:kNeedToUpdateContact];
              
            }
            
            if ([[response1 valueForKey:@"anyIntegrationSync"] intValue]  == 1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{

                    [UtilsClass makeToast:@"Syncing Integration contacts this may take upto few minutes,Please don't close the app."];
                    
                    [GlobalData  getCallhippoIntegrationContacts:[NSString stringWithFormat:@"%lu",(unsigned long)[Contact allContactCount]]];
                   // [Default setBool:true forKey:kNeedToUpdateContact];
                      
                  });
            }
            
            
            if ([[response1 valueForKey:@"removedIntegrationContact"] intValue]  == 1) {
                [[GlobalData sharedGlobalData] flushAllContactsAndFetchNew];
            }
            
            
          /*  if(dic[@"iosMinVersion"])
            {
                NSString *current_version = APP_VERSION;
                NSString *api_version = dic[@"iosMinVersion"];
                CGFloat cur_ver = [[NSString stringWithFormat:@"%@",current_version] floatValue];
                CGFloat api_ver = [[NSString stringWithFormat:@"%@",api_version] floatValue];
                NSLog(@"\n \n cur_ver : %f",cur_ver);
                NSLog(@"\n \n api_ver : %f",api_ver);
                if (cur_ver < api_ver)
                {
                    NSLog(@"\n \n cur_ver Lower Version \n \n ");
                    if([Force_update isEqualToString:@"true"])
                    {
                        [self app_update_alert];
                    }
                }
                else
                {
                    NSLog(@"\n \n cur_ver  Higher Version \n \n ");
                }
            }
            */
        }
        else
        {
            @try {
                if (response1 != (id)[NSNull null] && response1 != nil ){
                    [UtilsClass showAlert:[response1 valueForKey:@"error"][@"error"] contro:self];
                }
            }
            @catch (NSException *exception) {
            }
        }
    }
}
-(void)getTagList
{
    
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] init];
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"tags/%@/gettags",userId];
    
    //NSLog(@"tag URL : %@",aStrUrl);

    obj = [[WebApiController alloc] init];
    
    [obj callAPI_GET:aStrUrl andParams:dic SuccessCallback:@selector(getTagListResponse:response:) andDelegate:self];
}
- (void)getTagListResponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //NSLog(@"get tag response : %@",response1);


    NSLog(@"Encrypted Response : get tag : %@",response1);

    if([apiAlias isEqualToString:Status_Code])
    {
       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:self error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        if ([[response1 valueForKey:@"data"] count]>0) {
            [Default setObject:[response1 valueForKey:@"data"] forKey:kTagList];
        }
    
    }
}
-(void)setRestrictedCountryList
{
    dispatch_async(dispatch_get_main_queue(), ^{
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    for (int p=0; p<[[[GlobalData sharedGlobalData] callCountryProviderList] count]; p++) {
        
        BOOL res = [[[[[GlobalData sharedGlobalData] callCountryProviderList] objectAtIndex:p] valueForKey:@"restrictLocalCallerId"] boolValue];
        
        if (res == true) {
            
            [arr addObject:[[[[GlobalData sharedGlobalData] callCountryProviderList] objectAtIndex:p] valueForKey:@"shortName"]];
        
        }
    }
    
    [[GlobalData sharedGlobalData] setSameCallerIdRestricCountries:arr];
        
        NSLog(@"restricted list:: %@",[[GlobalData sharedGlobalData] sameCallerIdRestricCountries]);
        
    });
    
   
    
}
-(void)purchaseNumber:(NSDictionary *)response1{
    
    NSDictionary*numberData = [response1 objectForKey:@"numberData"];
    NSArray *purchase_number = [numberData valueForKey:@"numbers"];
    
    if ([purchase_number count] == 1) {
        if ([[[purchase_number objectAtIndex:0] objectForKey:ISDummyNumber] intValue] == 0) {
            
            return;
        }
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:purchase_number];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:PURCHASE_NUMBER];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [GlobalData sharedGlobalData].Number_Selection = [[NSMutableArray alloc] init];
    [[GlobalData sharedGlobalData].Number_Selection addObjectsFromArray:purchase_number];
    
    [self getNumbers];
}

- (void)lastdialcountry:(NSDictionary *)response1{
    
    
   //b NSString *str = response1[@"lastCountryCode"][@"code"];
    //pri
    NSString *str = [Default valueForKey:klastDialCountryCode];
    
    [Default setValue:str forKey:LAST_COUNTRY_CODE];
    [Default synchronize];
    lastCall_code = str;
    
    
    
    if([_callThroughCallHippo isEqualToString:@""] || _callThroughCallHippo == nil)
    {
        _txt_call_number_text.delegate = self;
        _txt_call_number_text.text = @"";
        [_txt_call_number_text insertText:str];
        
    }
    else
    {
        NSArray *arr = [_callThroughCallHippo componentsSeparatedByString:@"+"];
        _callThroughCallHippo = [_callThroughCallHippo stringByReplacingOccurrencesOfString:@"(" withString:@""];
        _callThroughCallHippo = [_callThroughCallHippo stringByReplacingOccurrencesOfString:@")" withString:@""];
        _callThroughCallHippo = [_callThroughCallHippo stringByReplacingOccurrencesOfString:@" " withString:@""];
        _callThroughCallHippo = [_callThroughCallHippo stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        if (arr.count == 1)
        {
            _txt_call_number_text.delegate = self;
            _txt_call_number_text.text = @"";
            [_txt_call_number_text insertText:[NSString stringWithFormat:@"%@%@",str,_callThroughCallHippo]];
        }
        else
        {
            _txt_call_number_text.delegate = self;
            _txt_call_number_text.text = @"";
            [_txt_call_number_text insertText:_callThroughCallHippo];
        }
        
    }
    
  //b  [self getCountryTime:response1[@"lastCountryCode"][@"countryName"]];
  //pri
  //  [self getCountryTime:[Default valueForKey:klastDialCountryName]];
    [self computeLocalTimeZone:[NSDate date] countryCode:_txt_call_number_text.country.countryCode];
    
}
- (void)app_update_alert
{
    NSString *version_message = [NSString stringWithFormat:@"Please update the app to latest version"];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"New Version"
                                 message:version_message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Update now"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        [self itunesopen];
    }];
    [alert addAction:yesButton];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIWindow *alertWindow = [appDelegate window];
    alertWindow = [[appDelegate window] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}

-(void)itunesopen
{
    NSString* appID = APP_ID;
    NSString *urlString = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@",appID];
    NSLog(@"urlString : %@",urlString);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(void)getprovider:(NSString *)lastCallDate {
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *firstCall = [dateFormatter1 dateFromString:lastCallDate];
    NSDate *lastCall = [Default objectForKey:@"oneCallProvider"];
    NSComparisonResult result;
    result = [lastCall compare:firstCall];
    if(result==NSOrderedAscending) {
        [GlobalData get_outcallprovider];
        [Default setObject:firstCall forKey:@"oneCallProvider"];
        [Default synchronize];
    } else {
    }
}

-(void)wootric{
    //for testing
    //f6daf2fb900988a7e032059eb6914d860216f84070b16bfab9764bc1a187a731
    //NPS-d58791f5
    // live :8b97d31bd4a3d2d0406d53ded6f725ff29c9f71fcba163f5e38d7fa3746038d7
    //  NPS-dd6ff276
    NSString *clientID = LIVE_NPS_CLIENT_ID;
    NSString *accountToken = LIVE_NPS_ACCOUNT_TOKEN;
    [Wootric configureWithClientID:clientID accountToken:accountToken];
    [Wootric setEndUserEmail:[Default valueForKey:kUSEREMAIL]];
    [Wootric forceSurvey:NO];
    [Wootric accessibilityScroll:UIAccessibilityScrollDirectionDown];
    [Wootric showSurveyInViewController:self];
}

-(void)tranfer_call
{
    transferVC = [[[NSBundle mainBundle] loadNibNamed:@"TransferVC" owner:self options:nil] objectAtIndex:0];
    transferVC.delegate = self;
    if (IS_IPHONE_5){
        transferVC.frame = CGRectMake(0, 0, self.view.frame.size.width,320);
    }else{
        transferVC.frame = CGRectMake(0, 0, self.view.frame.size.width,350);
    }
    transferVC.layer.borderWidth = 0.0;
    transferVC.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    transferVC.callStatus = @"Incoming";
    [self.view addSubview:transferVC];
}

- (void)onLogin
{
        NSLog(@"Login Chee");
}
- (void)onLoginFailed
{
    NSLog(@"Login nathi");
}
-(void)onLogout
{
    NSLog(@"logout endpoint");

}
-(void)thinq:(NSString *)val
{
    NSString *userId = [Default valueForKey:USER_ID];
    NSString *aStrUrl = [NSString stringWithFormat:@"updateThinqButton/%@/%@",val,userId];
    
    obj = [[WebApiController alloc] init];
    [obj callAPI_PUT_RAW:aStrUrl andParams:nil SuccessCallback:@selector(thinqresponse:response:) andDelegate:self];
   
}
- (void)thinqresponse:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"Encrypted Response : thinq response : %@",response1);

    
    [UtilsClass makeToast:@"Telephony provider changed. If you are facing quality issue on both provider A&B, you might need to check your internet connection."];
//    NSLog(@"thinqresponse : **************  : %@",response1);
    
}

-(void)addThinqSwitch {
    UIImage* image3 = [UIImage imageNamed:@""];
        
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@""]];
    NSString *thinqAccess =  [Default valueForKey:kTHINQACCESS];
    //NSString *thinqValue = [Default valueForKey:kTHINQVALUE];
    int thinqAccessInt = [thinqAccess intValue];
    //int thinqValueInt = [thinqValue intValue];
    
    NSString *thinqValue = [Default valueForKey:kTelephonyProviderSwitch];
    
    if ([thinqValue isEqualToString:@"A"])
    {
        image3 = [UIImage imageNamed:@"thimqswithcha.png"];
    }else{
        image3 = [UIImage imageNamed:@"thimqswithchb.png"];
    }
    
    
    CGRect frameimg = CGRectMake(0, 0, 36, 20);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(btn_thinq:)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem=mailbutton;
    if (thinqAccessInt == 1)
    {
        self.navigationItem.rightBarButtonItem.customView.hidden=NO;
    }else{
        self.navigationItem.rightBarButtonItem.customView.hidden=YES;
    }
}

#pragma mark - timezone API
-(void)computeLocalTimeZone:(NSDate *)dateToCompute countryCode:(NSString *)countryCode
{
    // Create a localized date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
    [dateFormatter setDateFormat:[NSString stringWithFormat:@"hh.mm aa, dd'\%@' MMMM, EEEE",[self dateSuffix]]];
  
   //by country [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:[self getTimeZoneByCountryCode:countryCode]]];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:[self getTimeZoneByCountryCodeAndAreaCode:countryCode]]];
    
   
    NSString *date = [dateFormatter stringFromDate:dateToCompute];
    
    _lbl_time_date.text = date;
   // return date;
}
-(NSString *)dateSuffix
{
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [calender components:NSCalendarUnitDay fromDate:[NSDate date]];
    NSInteger day = components.day;
    switch (day) {
        case 1| 21| 31:
            return @"st";
            break;
        case 2| 22:
            return @"nd";
            break;
        case 3| 23:
            return @"rd";
            break;
        default:
            return @"th";
            break;
    }
    
    
    
}
/*
-(NSString *)getTimeZoneByCountryCode:(NSString *)countryCode
{
   // NSString *path = [[NSBundle mainBundle] pathForResource:@"timezone" ofType:@"json"];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"timezone_areacode" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    NSArray *jsonFile = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingFragmentsAllowed error:nil];
    
    NSLog(@"jsonfile :: %@",jsonFile);
    
   NSArray *countries = [[jsonFile objectAtIndex:0] objectForKey:@"countries"];

    NSLog(@"zone :: %@",[[[countries valueForKey:countryCode] objectForKey:@"zones"] firstObject]);
    
    
    return [[[countries valueForKey:countryCode] objectForKey:@"zones"] firstObject];
    
}*/
-(NSString *)getTimeZoneByCountryCodeAndAreaCode:(NSString *)countryCode
{
   // NSString *path = [[NSBundle mainBundle] pathForResource:@"timezone" ofType:@"json"];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"timezone_areacode" ofType:@"json"];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    NSArray *jsonFile = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingFragmentsAllowed error:nil];

    
   NSArray *countries = [[jsonFile objectAtIndex:0] objectForKey:@"countries"];

    NSString *zoneStr;
    
    NSDictionary *countryDict =  [countries valueForKey:countryCode];

    
    if ([[countryDict allKeys] containsObject:@"areaCode"]) {
        
     
        NSArray *allAreaCode = [[countryDict objectForKey:@"areaCode"] allKeys];
            NSString *typednum = _txt_call_number_text.text;
            
        
        NSString *phonenum =  [typednum stringByReplacingOccurrencesOfString:@"+" withString:@""];
            phonenum = [phonenum stringByReplacingOccurrencesOfString:@"(" withString:@""];
            phonenum = [phonenum stringByReplacingOccurrencesOfString:@")" withString:@""];
            phonenum = [phonenum stringByReplacingOccurrencesOfString:@" " withString:@""];
            phonenum = [phonenum stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        
//        NSString *phonenum = [typednum stringByReplacingOccurrencesOfString:_txt_call_number_text.code withString:@""];
        
       
        
            NSString *areaPrefix = @"";
            NSString *areaPrefix2 = @"";
            
       
            if ([countryCode isEqualToString:@"CA"] || [countryCode isEqualToString:@"US"]) {
                
                //for US & canda
                phonenum = [phonenum substringFromIndex:1];
                if(phonenum.length>=3)
                {
                areaPrefix =  [phonenum substringToIndex:3];
                    
                    if([allAreaCode containsObject:areaPrefix] )
                    {
                        NSDictionary *zoneObj = [[countryDict valueForKey:@"areaCode"] valueForKey:areaPrefix];
                        
                        zoneStr = [zoneObj valueForKey:@"timezone"];
                        
                    }
                }
                else
                {
                    zoneStr = [[[countries valueForKey:countryCode] objectForKey:@"zones"] objectAtIndex:0];
                }
            }
            else if ([countryCode isEqualToString:@"AU"])
            {
                if(phonenum.length>=2)
                {
                phonenum = [phonenum substringFromIndex:2];
                if(phonenum.length>=2)
                {
                   
                //for australia
                areaPrefix = [phonenum substringToIndex:2];
                    if([allAreaCode containsObject:areaPrefix] )
                    {
                        NSDictionary *zoneObj = [[countryDict valueForKey:@"areaCode"] valueForKey:areaPrefix];
                        
                        zoneStr = [zoneObj valueForKey:@"timezone"];
                        
                    }
                    else
                    {
                       zoneStr = [[[countries valueForKey:countryCode] objectForKey:@"zones"] objectAtIndex:0];
                    }
                }
                else if (phonenum.length >= 1)
                {
                    areaPrefix2 = [phonenum substringToIndex:1];
                    if([allAreaCode containsObject:areaPrefix2] )
                    {
                        NSDictionary *zoneObj = [[countryDict valueForKey:@"areaCode"] valueForKey:areaPrefix2];
                        
                        zoneStr = [zoneObj valueForKey:@"timezone"];
                        
                    }
                    else
                    {
                       zoneStr = [[[countries valueForKey:countryCode] objectForKey:@"zones"] objectAtIndex:0];
                    }
                }
            }
            }
            else
            {
                zoneStr = [[[countries valueForKey:countryCode] objectForKey:@"zones"] objectAtIndex:0];
            }
       
        
            
           /* if([allAreaCode containsObject:areaPrefix] || [allAreaCode containsObject:areaPrefix2])
            {
                NSDictionary *zoneObj = [[countryDict valueForKey:@"areaCode"] valueForKey:areaPrefix];
                
                zoneStr = [zoneObj valueForKey:@"timezone"];
                
            }
            else
            {
               zoneStr = [[[countries valueForKey:countryCode] objectForKey:@"zones"] firstObject];
            }*/
            
            
       // }
       
    }
    else
    {
        zoneStr = [[[countries valueForKey:countryCode] objectForKey:@"zones"] objectAtIndex:0];
    }
    
    return zoneStr;
    
}



#pragma  mark- linphone methods

-(void)linphone_call:(NSString *)address name:(NSString *)name numbStr:(NSString *)numbStr

{
    
    if (!linphone_core_is_network_reachable(LC)) {
        
        [LinphoneManager.instance setupNetworkReachabilityCallback];
    }
    
    
    if ([address length] > 0) {
        if([UtilsClass isNetworkAvailable])
        {
           // calling_provider = Login_Linphone;
            [Default setValue:Login_Linphone forKey:CallingProvider];
            [Default synchronize];
            
            LinphoneManager.instance.providerDelegate.calls_uuids = [[NSMutableArray alloc] init];
            LinphoneAddress *addr = [LinphoneUtils normalizeSipOrPhoneAddress:address];
            [LinphoneManager.instance call:addr];
            if (addr) {
                linphone_address_destroy(addr);
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                OnCallVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"OnCallVC"];
                vc.CallStatus = OUTGOING;//OUTGOING;
                vc.CallStatusfinal = OUTGOING;
                vc.ContactName = name; //[name stringByReplacingOccurrencesOfString:@" " withString:@""];
                vc.ContactNumber = [numbStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                vc.IncCallOutCall = @"Outgoing";
                
    
                if (IS_IPAD) {
                    vc.popoverPresentationController.sourceView = self.view;
                    [self presentViewController:vc animated:YES completion:nil];
                } else {
                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                    [self presentViewController:vc animated:true completion:nil];
                }
            }
            else
            {
            }
        }
        else
        {
            [UtilsClass showAlert:@"Please check your internet connection and try again." contro:self];
        }
    }
}
- (void)registrationUpdateFire:(NSNotification *)notif {
    
    NSLog(@"register update fire");
    
    LinphoneProxyConfig *config = linphone_core_get_default_proxy_config(LC);
    if(config != NULL)
    {
         [self proxyConfigUpdate:config];
    }
   
}
- (void)proxyConfigUpdate:(LinphoneProxyConfig *)config {
    
    NSString *message;
    NSString *Userid = [Default valueForKey:USER_ID];
    if (Userid != nil)
    {
    LinphoneRegistrationState state = LinphoneRegistrationNone;
     message = nil;
            state = linphone_proxy_config_get_state(config);
            switch (state)
            {
                case LinphoneRegistrationOk:
                {
                    message = NSLocalizedString(@"Connected", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Trushang Linphon : Connected");
                    NSLog(@"\n \n ");
                    
                    NSString *Userid = [Default valueForKey:USER_ID];
                    if (Userid != nil)
                    {
                     //pr   [self soket_disconnect];

                    }
                }
                break;
                case LinphoneRegistrationNone:
                {
                  /*  if([Default valueForKey:kFreeswitchUsername]) {
                       [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                    }*/
                }
                    break;
                case LinphoneRegistrationCleared:
                {
                    message = NSLocalizedString(@"Not connected", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Trushang Linphon : Not connected");
                    NSLog(@"\n \n ");
                    
                 //pr   [self soket_disconnect];

                }
                    break;
                case LinphoneRegistrationFailed:  {
                    message = NSLocalizedString(@"Connection failed", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Patel:Trushang Linphon : Connection failed");
                    NSLog(@"\n \n ");
                    
                 /*   if ([Default valueForKey:kFreeswitchUsername]) {
                       [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                    }
                    */
                    break;
                }
                case LinphoneRegistrationProgress:
                    message = NSLocalizedString(@"Connection in progress", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Trushang Linphon : Connection in progress");
                    NSLog(@"\n \n ");
                    break;
                default:
                    break;
            }
    

        
    }
}


//-(void)thinqSwitchDefaultValue:(NSDictionary *)response1{
//    UIImage* image3 = [UIImage imageNamed:@""];
//    NSString *thinqAccess = response1[@"thinqAccess"];
//    NSString *thinqValue = response1[@"thinqValue"];
//    int thinqAccessInt = [thinqAccess intValue];
//    int thinqValueInt = [thinqValue intValue];
//
//
//    if (thinqValueInt == 1)
//    {
//        image3 = [UIImage imageNamed:@"thimqswithchb.png"];
//    }else{
//
//        image3 = [UIImage imageNamed:@"thimqswithcha.png"];
//    }
//    CGRect frameimg = CGRectMake(0, 0, 36, 20);
//    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
//    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
//    [someButton addTarget:self action:@selector(btn_thinq:)
//         forControlEvents:UIControlEventTouchUpInside];
//    [someButton setShowsTouchWhenHighlighted:YES];
//
//    mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
//    self.navigationItem.rightBarButtonItem=mailbutton;
//    if (thinqAccessInt == 1)
//    {
//        self.navigationItem.rightBarButtonItem.customView.hidden=NO;
//    }else{
//        self.navigationItem.rightBarButtonItem.customView.hidden=YES;
//    }
//}


@end

