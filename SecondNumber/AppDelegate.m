//
//  AppDelegate.m
//  SecondNumber
//
//  Created by Apple on 18/11/21.
//

#import "AppDelegate.h"
#import "EditProfileVC.h"
#import "LoginViewController.h"
#import "SubscribeViewController.h"
#import "NumbersListVC.h"
#import "LoginVC.h"
#import "DialerVC.h"
#import "IQKeyboardManager.h"
#import "MainViewController.h"
#import "Constant.h"
#import "UtilsClass.h"
#import "Singleton.h"
#import "WebApiController.h"
#import <sys/utsname.h>
#import "GlobalData.h"
#import "CalllogsDetailsVC.h"
#import "OnCallVC.h"



#import "ChatViewControllerNew.h"

//pri-fabric
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

#import <FirebaseCrashlytics/FirebaseCrashlytics.h>

#import "NSData+Base64.h"
#import "SmsListNew.h"
#import "CalllogsVC.h"
#import "ReminderVC.h"
#import "Lin_Utility.h"
#import <Intents/Intents.h>
#import "AddCreditVC.h"


#import "Phone.h"
#import <PlivoVoiceKit/PlivoVoiceKit.h>
#import "CallKitInstance.h"
#import "twilio_callkit.h"
#import <Smartlook/Smartlook.h>"

#import "PhoneMainView.h"
#include "LinphoneManager.h"
#include "linphone/linphonecore.h"
#import "CountryListViewController.h"

@import Firebase;
//@import Bugsee;
@import SocketIO;
//@import FirebaseCore;
//@import FirebaseMessaging;
@import TwilioVoice;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()<TwilioDelegate>

@end

@implementation AppDelegate
{
    NSString *Fcmtoken;
    NSString *Devicetoken;
    NSString *Voiptoken;
    NSData *Voiptoken_data;
    WebApiController *obj;
    
    NSMutableArray *responseArray;
    NSMutableDictionary *respArray;
    NSMutableArray *tblArray;
    NSMutableArray *Mixallcontact;
    NSString *userEmailId;
    int badgecount;
    
    BOOL didUpdatePushCredentials;
    NSData *deviceTokenForPlivo;
}

@synthesize configURL;
@synthesize window;
@synthesize manager;
@synthesize message;
@synthesize Call_sid;
@synthesize extHeader;
@synthesize twilio_callinvite;
@synthesize callInvite_incoming;
@synthesize socket;

- (id)init {
    self = [super init];
    if (self != nil) {
        startedInBackground = FALSE;
        
    }
    //_alreadyRegisteredForNotification = FALSE;
    _onlyPortrait = FALSE;
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
//    SubscribeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SubscribeViewController"];
//    [navigationController setViewControllers:@[vc] animated:false];
//
//    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
//    mainViewController.rootViewController = navigationController;
//    [mainViewController setupWithType:11];
//    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
//    navController.navigationBar.hidden = true;
//    self.window.rootViewController = navController;
//    [self.window setNeedsLayout];
    
    
    // Override point for customization after application launch.
    
   // Encryption commented [Default setBool:false forKey:kencryptionInMobile];
    
   
    
    
    [self settingApi];
    
    userEmailId = [Default valueForKey:kUSEREMAIL];
    [self registerForNotifications];
    NSDictionary *userActDic = [launchOptions objectForKey:UIApplicationLaunchOptionsUserActivityDictionaryKey];
    NSString *callThroughCall = @"";
    if(userActDic)
    {
        NSUserActivity *userActivity = [userActDic valueForKey:@"UIApplicationLaunchOptionsUserActivityKey"];
        INPerson *person = [[(INStartAudioCallIntent*)userActivity.interaction.intent contacts] firstObject];
        NSString *phoneNumber = person.personHandle.value;
        callThroughCall = phoneNumber;
        NSLog(@"Chetan Joshi : continueUserActivity : %@",phoneNumber);
        NSDictionary* userInfo = @{@"Number": phoneNumber};
        
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"callfromcallhippo" object:self userInfo:userInfo];
    }
    
    badgecount = 0;
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",badgecount] forKey:@"Bagecount"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgecount];
    [FIRApp configure];
    [FIRCrashlytics crashlytics];
    [FIRMessaging messaging].delegate = self;
    [FIRMessaging messaging].autoInitEnabled = YES;
    
    [GIDSignIn sharedInstance].clientID = @"524143824291-mkqmveba6d9sdpk514vq6ktkt194p102.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].delegate = self;
    
#pragma mark - RemoveAllUserDefult
    if ([[NSUserDefaults standardUserDefaults] valueForKey:RemoveAllUserDefult_text])
    {
    }
    else
    {
        NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
        NSDictionary * dict = [defs dictionaryRepresentation];
        for (id key in dict) {
            [defs removeObjectForKey:key];
        }
        [defs synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:@"plivoswitch" forKey:RemoveAllUserDefult_text];
    }
    
    NSString *Userid = [Default valueForKey:USER_ID];
    
    if(userEmailId != nil)
    {
        //pri-fabric
        //[CrashlyticsKit setObjectValue:userEmailId forKey:@"User_Email"];
        

        [[FIRCrashlytics crashlytics] setUserID:Userid];
        [[FIRCrashlytics crashlytics] setCustomValue:userEmailId forKey:@"User_Email"];

        [FIRAnalytics setUserPropertyString:userEmailId forName:@"callhippo_username"];
        //pp if([[Default valueForKey:SMARTLOOK] isEqualToString:@"NO"]){
            if([[Default valueForKey:SMARTLOOK] isEqualToString:@"ON"]){
            [Smartlook setUserIdentifier:userEmailId];
        }

    }
    
    

        if (Userid != nil)
        {
            [self linphoneSetup];
            
            if([[Default valueForKey:kStripeId] isEqualToString:@""])
            {
               
                [self openSubscribePage];
            }
            else
            {
                [self openDialerPage];
            }
          /*  if([[Default valueForKey:kISFreeTrial] boolValue]  == true)
            {
                
                NSString *trialdate = [Default valueForKey:kFreeTrialEnd];
                
                NSLog(@"trial end date :: %@",trialdate);
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            
                NSDate *date1 = [formatter dateFromString:trialdate];
                NSDate *date2 = [NSDate date];
                
               
                NSComparisonResult result = [date1 compare:date2];
                    if (result == NSOrderedAscending) {
                        //Fails
                        NSLog(@"ascending:: trial completed");
                        
                        [self openSubscribePage];
                        
                    }else if (result == NSOrderedSame){
                        
                       
                    }else{
                        NSLog(@"decending");
                       
                        
                        [self openDialerPage];
                    }
                
            
                
            }
            else
            {
                if ([[Default valueForKey:kPlanName] isEqualToString:@"second-number"] || [[Default valueForKey:kPlanName] isEqualToString:@"second_number_ft"]) {
                    [self openDialerPage];
                }
                else
                {
                    [self openSubscribePage];
                }
                
            }*/
            
            

        }
        else
        {
            
            
            [self openLoginPage];
        }
        
    //}
    
    //
    
    
    NSString *Version_Info = [[UIDevice currentDevice] systemVersion] ?  [[UIDevice currentDevice] systemVersion] : @"";
    NSString *Device_Info =  [UtilsClass deviceName] ?  [UtilsClass deviceName] : @"";
    
    [Default setValue:Version_Info forKey:IS_VERSIONINFO];
    [Default setValue:Device_Info forKey:IS_DEVICEINFO];
    
    dispatch_async( dispatch_get_main_queue(), ^{
        if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge ) completionHandler:^(BOOL granted, NSError * _Nullable error){
                NSLog(@"Notification Call 1");
                if(!error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                }
            }];
        }
        else {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeAlert ) categories:nil]];
            NSLog(@"Notification Call 2");
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        
    });
    
    if([Default valueForKey:@"FCMTOKEN"] != nil)
    {
        if([[Default valueForKey:@"FCMTOKEN"] isEqualToString:@""] )
        {
            Fcmtoken = @"";
        }
        else
        {
            Fcmtoken = [Default valueForKey:@"FCMTOKEN"];
        }
    }
    else
    {
        Fcmtoken = @"";
    }
    
    if([Default valueForKey:@"VOIPTOKEN"] != nil)
    {
        if([[Default valueForKey:@"VOIPTOKEN"] isEqualToString:@""] )
        {
            Voiptoken = @"";
        }
        else
        {
            Voiptoken = [Default valueForKey:@"VOIPTOKEN"];
        }
    }
    else
    {
        Voiptoken = @"";
    }
    
    
    
    Devicetoken = @"";
    [[NSUserDefaults standardUserDefaults] setValue:Fcmtoken forKey:@"FCMTOKEN"];
    [[NSUserDefaults standardUserDefaults] setValue:Voiptoken forKey:@"VOIPTOKEN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    NSLog(@"FCM : %@",Fcmtoken);
    
    
//    NSString *LoginProvider = [Default valueForKey:Login_Provider];
//    if([LoginProvider isEqualToString:Login_Plivo])
//    {
    
        NSLog(@"voip resgister : login plivo");
        NSString *plivoUserName = [Default valueForKey:ENDPOINT_USERNAME];
        NSString *plivoPassword = [Default valueForKey:ENDPOINT_PASSORD];
        
    if (plivoUserName != nil && plivoPassword != nil) {

        [self voipRegistration:plivoUserName password:plivoPassword];
        
    }
        
//    }
//    else
//    {
        
        
        BOOL background_mode = YES; //[instance lpConfigBoolForKey:@"backgroundmode_preference"];
        BOOL start_at_boot = NO;//[instance lpConfigBoolForKey:@"start_at_boot_preference"];
        // Register for notifications must be done ASAP to give a chance for first SIP register to be done with right token. Specially true in case of remote provisionning or re-install with new type of signing certificate, like debug to release.
        NSLog(@"Voiptoken : %@",Voiptoken);
        //        if([twilio_test isEqualToString:@"true"])
        //        {
        //
        //        }
        //        else
        //        {
        
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max)
        {
            //                NSLog(@"Its wokring call Baby Baby");
            self.del_twilio = [[twilio_callkit alloc] init];
            [[twilio_callkit sharedInstance] setProviderDelegate:self.del_twilio];
        }
        
        //        }
        
        
        UIApplication *app = [UIApplication sharedApplication];
        UIApplicationState state = app.applicationState;
        
        if (state == UIApplicationStateBackground) {
            // we've been woken up directly to background;
            if (!start_at_boot || !background_mode) {
                // autoboot disabled or no background, and no push: do nothing and wait for a real launch
                //output a log with NSLog, because the ortp logging system isn't activated yet at this time
                NSLog(@"Linphone launch doing nothing because start_at_boot or background_mode are not activated.", NULL);
                return YES;
            }
        }
        bgStartId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"Background task for application launching expired.");
            [[UIApplication sharedApplication] endBackgroundTask:self->bgStartId];
        }];
        
        
        
        if (bgStartId != UIBackgroundTaskInvalid)
            [[UIApplication sharedApplication] endBackgroundTask:bgStartId];
 //   }
    
    
    UIApplicationShortcutItem *shortcutItem = [launchOptions objectForKey:@"UIApplicationLaunchOptionsShortcutItemKey"];
    
    //    Request Record permission
    if([[AVAudioSession sharedInstance] respondsToSelector:@selector(requestRecordPermission:)])
    {
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            
        }];
        
    }
    
    if (shortcutItem) {
        _shortcutItem = shortcutItem;
        return NO;
    }
    
    [self keybord_setup];
    
    //pri-fabric
//    [Fabric with:@[[Crashlytics self]]];
//    Fabric.sharedSDK.debug = true;
    
    [twilio_callkit sharedInstance].audioDevice = [TVODefaultAudioDevice audioDevice];
    TwilioVoiceSDK.audioDevice = [twilio_callkit sharedInstance].audioDevice;
    //    [Bugsee launchWithToken:@"712763b5-f523-458b-9483-3f8d46cd685e"];

    return YES;
}

-(void)openDialerPage
{
    NSData *data = [Default valueForKey:PURCHASE_NUMBER];
   NSArray *numbersArr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (numbersArr.count == 0) {
        
        [self openCountryPage];
    }
    else
    {
        //linphone setup
        
        [NSNotificationCenter.defaultCenter addObserver:self
                                               selector:@selector(registrationUpdateEvent:)
                                                   name:kLinphoneRegistrationUpdate
                                                 object:nil];
        [NSNotificationCenter.defaultCenter addObserver:self
                                               selector:@selector(callUpdate:)
                                                   name:kLinphoneCallUpdate
                                                 object:nil];
        
        
        [self all_contact_get];
        
        

        
        
        if (userEmailId != nil){
            [FIRAnalytics logEventWithName:@"ch_open" parameters:@{@"emailId": userEmailId}];
            NSLog(@"Email Address Not Null");
        }else {
            NSLog(@"Email Address Null");
            [FIRAnalytics logEventWithName:@"ch_open" parameters:@{@"emailId": @""}];
        }
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
      // LoginViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        
       DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
        
       // NumbersListVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NumbersListVC"];
        
        //SubscribeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SubscribeViewController"];
       /// EditProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
        
        [Default setValue:@"Dialer" forKey:SelectedSideMenu];
        [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
        //vc1.callThroughCallHippo = callThroughCall;
        [navigationController setViewControllers:@[vc1] animated:false];
        
        MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        mainViewController.rootViewController = navigationController;
        [mainViewController setupWithType:11];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
        navController.navigationBar.hidden = true;
        self.window.rootViewController = navController;
        [self.window setNeedsLayout];
    }
    
}
-(void)openSubscribePage
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
   
    SubscribeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SubscribeViewController"];
   
    
    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
    //vc1.callThroughCallHippo = callThroughCall;
    [navigationController setViewControllers:@[vc] animated:false];
    
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:11];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
    navController.navigationBar.hidden = true;
    self.window.rootViewController = navController;
    [self.window setNeedsLayout];
}
-(void)openCountryPage
{
    //linphone setup
    
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(registrationUpdateEvent:)
                                               name:kLinphoneRegistrationUpdate
                                             object:nil];
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(callUpdate:)
                                               name:kLinphoneCallUpdate
                                             object:nil];
    
    
    [self all_contact_get];
    
    

    
    
    if (userEmailId != nil){
        [FIRAnalytics logEventWithName:@"ch_open" parameters:@{@"emailId": userEmailId}];
        NSLog(@"Email Address Not Null");
    }else {
        NSLog(@"Email Address Null");
        [FIRAnalytics logEventWithName:@"ch_open" parameters:@{@"emailId": @""}];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
  // DialerVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
    
   CountryListViewController *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"CountryListViewController"];
    vc1.fromStr = @"subscribe";
   // NumbersListVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NumbersListVC"];
    
    //SubscribeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SubscribeViewController"];
   /// EditProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    
    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
    //vc1.callThroughCallHippo = callThroughCall;
    [navigationController setViewControllers:@[vc1] animated:false];
    
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:11];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
    navController.navigationBar.hidden = true;
    self.window.rootViewController = navController;
    [self.window setNeedsLayout];
}
-(void)openLoginPage
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    LoginViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
   // EditProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    
   // NumbersListVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NumbersListVC"];
    
  //  SubscribeViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SubscribeViewController"];
    
   
    
    [navigationController setViewControllers:@[vc] animated:false];
    
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:11];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
    navController.navigationBar.hidden = true;
    self.window.rootViewController = navController;
    [self.window setNeedsLayout];
}
#pragma deploymate push "ignored-api-availability"
#pragma mark - registerForNotifications

- (void)registerForNotifications {

    self.voipRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    self.voipRegistry.delegate = self;
    // Initiate registration.
    self.voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}
-(void)RegisterVoipService
{
    pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    pushRegistry.delegate = self;
    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}
-(void)RegisterVoipService_twilio
{
    pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    pushRegistry.delegate = self;
    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(nonnull NSError *)error {
    NSLog(@"Hello Notificaion...%@",error);
}

#pragma mark - FIRMessagingForNotifications

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken
{
    NSLog(@"FCM token:: %@", fcmToken);
    Fcmtoken = @"";
    if (fcmToken != nil)
    {
        NSLog(@"FCM registration token not nil");
        Fcmtoken = fcmToken;
    }
    else
    {
        NSLog(@"FCM registration token nil");
        Fcmtoken = @"";
    }
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:Fcmtoken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    [[NSUserDefaults standardUserDefaults] setValue:Fcmtoken forKey:@"FCMTOKEN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *userid = [Default valueForKey:USER_ID];
    if(![Voiptoken isEqualToString: @""] && userid != nil)
    {
        [self updatefcmtoken];
    }
}
- (void)instanceIDWithHandler:(nonnull FIRInstanceIDResultHandler)handler
{
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Remote instance ID token: %@", result.token);
        }
    }];
}


-(void)keybord_setup
{
    //Enabling keyboard manager
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15];
    //Enabling autoToolbar behaviour. If It is set to NO. You have to manually create IQToolbar for keyboard.
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    //Setting toolbar behavious to IQAutoToolbarBySubviews. Set it to IQAutoToolbarByTag to manage previous/next according to UITextField's tag property in increasing order.
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    
    //Resign textField if touched outside of UITextField/UITextView.
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:NO];
    
    //Giving permission to modify TextView's frame
    [[UINavigationBar appearance] setTranslucent:false];
    UINavigationBar.appearance.translucent = false;
    
    //    IQKeyboardManager.shared.keyboardDistanceFromTextField = 50
    
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:100.0];
}

#pragma mark - ApplicationStages

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"applicationWillResignActive");
    NSLog(@"CONTACT SYNC");
    NSString *userid = [Default valueForKey:USER_ID];
    if(userid != nil) {
        dispatch_async( dispatch_get_main_queue(), ^{
         //old    [GlobalData  get_all_callhippo_contacts];
        });
    }
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
   
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
 
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(Foreground_Action:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    badgecount = 0;
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",badgecount] forKey:@"Bagecount"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgecount];
    
    
    NSString *LoginProvider = [Default valueForKey:Login_Provider];
    if([LoginProvider isEqualToString:Login_Plivo])
    {
        
        
        
    }
    else
    {
        
        
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [Default setBool:false forKey:kisContactSyncingRunning];
    [Default synchronize];
    
    
    if (userEmailId != nil){
        [FIRAnalytics logEventWithName:@"ch_close" parameters:@{@"emailId": userEmailId}];
        NSLog(@"Email Address Not Null");
    }else {
        NSLog(@"Email Address Null");
        [FIRAnalytics logEventWithName:@"ch_close" parameters:@{@"emailId": @""}];
    }
    
    
    LinphoneManager.instance.conf = TRUE;
    linphone_core_terminate_all_calls(LC);
    
}
- (BOOL)handleShortcut:(UIApplicationShortcutItem *)shortcutItem {
    BOOL success = NO;
    return success;
}
- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    completionHandler([self handleShortcut:shortcutItem]);
}
- (NSString *)stringWithDeviceToken:(NSData *)deviceToken {
    

    
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];
    
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    NSLog(@"token ---->%@",token);
    Devicetoken = token;
    
    return [token copy];
}
- (void)fixRing {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        // iOS7 fix for notification sound not stopping.
        // see http://stackoverflow.com/questions/19124882/stopping-ios-7-remote-notification-sound
        
    }
    
}
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken
{
    NSString *token = [self stringWithDeviceToken:deviceToken];
    NSLog(@"Token : %@",token);
    [FIRMessaging messaging].APNSToken = deviceToken;
}
//-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//{
//    NSLog(@"Trushang_push : Notification : didReceiveRemoteNotification 11111111 : %@",userInfo);
//    NSLog(@"Application state : %ld",(long)[UIApplication sharedApplication].applicationState);
//    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
//    {
//      NSLog(@"Application UIApplicationStateActive : %ld",(long)[UIApplication sharedApplication].applicationState);
//    }
//    else if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive)
//    {
//        NSLog(@"Application UIApplicationStateInactive : %ld",(long)[UIApplication sharedApplication].applicationState);
//    }
//    else if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
//    {
//        NSLog(@"Application UIApplicationStateBackground : %ld",(long)[UIApplication sharedApplication].applicationState);
//    }
//
//
//    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
//    {
//        if ([userInfo[@"gcm.notification.type"]  isEqual: @"sms"])
//        {
//
//            [self all_contact_get];
//            [self save_contact:userInfo[@"gcm.notification.nonChNumber"]];
//            [self UnreadStatus:userInfo[@"gcm.notification.threadId"]];
//            [self conversionFromNotification:userInfo[@"gcm.notification.nonChNumber"] fromNumber:userInfo[@"gcm.notification.chNumber"] chNumberId:userInfo[@"gcm.notification.chNumberId"]];
//            //[self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result){}];
//        }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"call"]) {
//            [self gotoLogs];
//        }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"lowCredit"]) {
//            [self gotoLowCredit];
//        }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"reminder"])
//        {
//            [self gotoReminders];
//        }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"update"]) {
//            [self itunesopen];
//        }else if ([userInfo[@"title"]  isEqual: @"removepopup"]) {
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"removipopupnotification"object:self];
//        } else if ([userInfo[@"title"]  isEqual: @"transferButtonNotify"]) {
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"transferButtonNotify"object:self];
//        }else if ([userInfo[@"title"]  isEqual: @"WarmTransferAccept"]) {
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"WarmTransferAccept"object:self];
//        }else if ([userInfo[@"title"]  isEqual: @"transferButtonNotify"]) {
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"HideTransferButton"object:self];
//        } else {
//            //        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
//        }
//    }
//    else
//    {
//        [self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result)
//         {
//             if ([userInfo[@"gcm.notification.type"]  isEqual: @"sms"]) {
//                 [self all_contact_get];
//                 [self save_contact:userInfo[@"gcm.notification.nonChNumber"]];
//                 [self UnreadStatus:userInfo[@"gcm.notification.threadId"]];
//                 [self conversionFromNotification:userInfo[@"gcm.notification.nonChNumber"] fromNumber:userInfo[@"gcm.notification.chNumber"] chNumberId:userInfo[@"gcm.notification.chNumberId"]];
//             }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"lowCredit"]) {
//                 [self gotoLowCredit];
//             }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"call"]) {
//                 [self gotoLogs];
//             }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"reminder"]) {
//                 [self gotoReminders];
//             }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"update"]) {
//                 [self itunesopen];
//             }
//         }];
//    }
//
////
//
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Trushang_push : Notification : didReceiveRemoteNotification 44444444 : %@",userInfo);
    
    NSString *userid = [Default valueForKey:USER_ID];
    if(userid != nil) {
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            
            if ([userInfo[@"gcm.notification.type"]  isEqual: @"sms"])
            {
                //                5df225052e9d17709f088f0b
               
                
                if([[Default valueForKey:IS_ChatviewDisplayed] isEqualToString:@"1"])
                {
                    if([[Default valueForKey:IS_ChatviewDisplayed_ThredID] isEqualToString:userInfo[@"gcm.notification.threadId"]])
                    {
                         completionHandler(UNNotificationPresentationOptionNone);
                        
                        [self all_contact_get];
                        [self save_contact:userInfo[@"gcm.notification.nonChNumber"]];
                        [self UnreadStatus:userInfo[@"gcm.notification.threadId"]];
                        [self conversionFromNotification:userInfo[@"gcm.notification.nonChNumber"] fromNumber:userInfo[@"gcm.notification.chNumber"] chNumberId:userInfo[@"gcm.notification.chNumberId"]];
                    }
                    else
                    {
                        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
                    }
                }
                else
                {
                    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
                }
            }else if ([userInfo[@"title"]  isEqual: @"removepopup"]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"removipopupnotification"object:self];
            } else if ([userInfo[@"title"]  isEqual: @"transferButtonNotify"]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"transferButtonNotify"object:self];
            }else if ([userInfo[@"title"]  isEqual: @"WarmTransferAccept"]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"WarmTransferAccept"object:self];
            }else if ([userInfo[@"title"]  isEqual: @"transferButtonNotify"]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"HideTransferButton"object:self];
            }
            else if([userInfo[@"gcm.notification.type"]  isEqual: @"call"])
            {
                if ([[Default valueForKey:IS_CallLogDisplayed] isEqualToString:@"1"]) {
                    completionHandler(UNNotificationPresentationOptionNone );
                    
                    //notification to load call logs
                    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                    [nc postNotificationName:@"callDisconnected_loadCallLogs" object:self userInfo:nil];
                }
                else
                {
                    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
                }
            }
            
//ppppp            if([[Default valueForKey:IS_ChatviewDisplayed] isEqualToString:@"1"])
//            {
//                if([[Default valueForKey:IS_ChatviewDisplayed_ThredID] isEqualToString:userInfo[@"gcm.notification.threadId"]])
//                {
//                    NSLog(@"Appdelegate :: Same Thread ID");
//
//                completionHandler(UNNotificationPresentationOptionNone);
//
//                  //   completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
//
//                }
//                else
//                {
//                    NSLog(@"Appdelegate :: Not Same Thread ID");
//                    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
//                }
//            }
//            else
//            {
//                NSLog(@"Appdelegate :: Out OF");
//                completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
//            }
        } else {
            badgecount = [[NSString stringWithFormat:@"%@",[Default valueForKey:@"Bagecount"]] intValue];
            badgecount = badgecount + 1;
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",badgecount] forKey:@"Bagecount"];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgecount];
            
            if ([userInfo[@"gcm.notification.type"]  isEqual: @"sms"]) {
                [self all_contact_get];
                [self save_contact:userInfo[@"gcm.notification.nonChNumber"]];
                [self UnreadStatus:userInfo[@"gcm.notification.threadId"]];
                [self conversionFromNotification:userInfo[@"gcm.notification.nonChNumber"] fromNumber:userInfo[@"gcm.notification.chNumber"] chNumberId:userInfo[@"gcm.notification.chNumberId"]];
                
            }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"lowCredit"]) {
                [self gotoLowCredit];
            }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"call"]) {
                [self gotoLogs];
            }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"reminder"]) {
                [self gotoReminders];
            }else if ([userInfo[@"gcm.notification.type"]  isEqual: @"update"]) {
                [self itunesopen];
            }
            else if ([userInfo[@"title"]  isEqual: @"dynamic_calllogs"]) {
                [self gotoLogs];
            }
            else if ([userInfo[@"gcm.notification.type"]  isEqual: @"calltagging"]) {
                [self gotoCallLogDetail:userInfo[@"gcm.notification.logId"]];
            }
            completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
        }
    }
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    
    NSLog(@"Trushang_push : Notification : willPresentNotification 2222222 : %@",notification.request.content.userInfo);
    NSDictionary *noti = notification.request.content.userInfo;
    NSDictionary *aps = noti[@"aps"];
    NSDictionary *alert = aps[@"alert"];
    
    if ([alert[@"title"]  isEqual: @"SMS"])
    {
        NSLog(@" ISchatview  :: 2222222 :: %@",[Default valueForKey:IS_ChatviewDisplayed]);
        if([[Default valueForKey:IS_ChatviewDisplayed] isEqualToString:@"1"])
        {
            if([[Default valueForKey:IS_ChatviewDisplayed_ThredID] isEqualToString:noti[@"gcm.notification.threadId"]])
            {
                [self all_contact_get];
                [self save_contact:noti[@"gcm.notification.nonChNumber"]];
                [self conversaton_screenupdate:noti[@"gcm.notification.threadId"] readStatus:@"read" smsAuther:@"" smsContent:alert[@"body"] smsTime:@"" smsUUID:@""];
            }
        }
    }
    else if ([noti[@"title"]  isEqual: @"removepopup"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"removipopupnotification"object:self];
    } else if ([noti[@"title"]  isEqual: @"transferButtonNotify"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"transferButtonNotify"object:self];
    }else if ([noti[@"title"]  isEqual: @"WarmTransferAccept"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"WarmTransferAccept"object:self];
    }else if ([noti[@"title"]  isEqual: @"transferButtonNotify"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"HideTransferButton"object:self];
    }
    else if([noti[@"gcm.notification.type"]  isEqual: @"call"])
    {
        if ([[Default valueForKey:IS_CallLogDisplayed] isEqualToString:@"1"]) {
            completionHandler(UNNotificationPresentationOptionNone);
            //notification to load call logs
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"callDisconnected_loadCallLogs" object:self userInfo:nil];
        }
        else
        {
            completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
        }
    }
    
    if([[Default valueForKey:IS_ChatviewDisplayed] isEqualToString:@"1"])
    {
        if([[Default valueForKey:IS_ChatviewDisplayed_ThredID] isEqualToString:noti[@"gcm.notification.threadId"]])
        {
            completionHandler(UNNotificationPresentationOptionNone);
            
        }
        else
        {
            completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
        }
    }
    else
    {
        NSLog(@"Appdelegate :: Out OF");
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionAlert | UNAuthorizationOptionSound);
    }
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
{
    NSDictionary *noti = response.notification.request.content.userInfo;
    NSLog(@"Trushang_push : Notification : didReceiveNotificationResponse 33333333 : %@",response.notification.request.content.userInfo);
    NSString *userid = [Default valueForKey:USER_ID];
    if(userid != nil){
        if ([noti[@"gcm.notification.type"]  isEqual: @"sms"]) {
            [self all_contact_get];
            [self save_contact:noti[@"gcm.notification.nonChNumber"]];
            [self UnreadStatus:noti[@"gcm.notification.threadId"]];
            [self conversionFromNotification:noti[@"gcm.notification.nonChNumber"] fromNumber:noti[@"gcm.notification.chNumber"] chNumberId:noti[@"gcm.notification.chNumberId"]];
        }else if ([noti[@"gcm.notification.type"]  isEqual: @"lowCredit"]) {
            [self gotoLowCredit];
        }else if ([noti[@"gcm.notification.type"]  isEqual: @"call"]) {
            [self gotoLogs];
        }else if ([noti[@"gcm.notification.type"]  isEqual: @"reminder"]) {
            [self gotoReminders];
        }else if ([noti[@"gcm.notification.type"]  isEqual: @"update"]) {
            [self itunesopen];
        }else if ([noti[@"title"]  isEqual: @"removepopup"]) {
            NSLog(@"Hello removepopup");
        }
        else if ([noti[@"title"]  isEqual: @"dynamic_calllogs"]) {
            [self gotoLogs];
        }
        else if ([noti[@"gcm.notification.type"]  isEqual: @"calltagging"]) {
            [self gotoCallLogDetail:noti[@"gcm.notification.logId"]];
        }
    }
}

-(void)conversionFromNotification:(NSString *)tonumber fromNumber:(NSString *)fromnumber chNumberId:(NSString *)chnumberid {
    
    if([UtilsClass isNetworkAvailable]) {
        
        NSString *url = [NSString stringWithFormat:@"getUserSmsDetails"];
        NSDictionary *passDict = @{@"user":@"",
                                   @"contact":@"",
                                   @"from":fromnumber,
                                   @"to":tonumber,
                                   @"chNumberId":chnumberid};
        [Default setValue:chnumberid forKey:NUMBERID];
        NSLog(@"Params : %@",passDict);
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        NSString *jsonString;
        if (! jsonData) {
            
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        obj = [[WebApiController alloc] init];
        
        [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(getChatList:response:) andDelegate:self];
        
        
    } else {
        
    }
}

- (void)getChatList:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"TRUSHANG : STATUSCODE **************8  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *viewcontroler = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:viewcontroler];
         [UtilsClass logoutUser:viewcontroler error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        tblArray = [[NSMutableArray alloc] init];
        responseArray = [[NSMutableArray alloc] init];
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            
            NSLog(@"Response : %@",response1);
            respArray = response1[@"data"];
            
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            SmsListNew *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"SmsListNew"];
            ChatViewControllerNew *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerNew"];
            
            [Default setValue:@"SMS" forKey:SelectedSideMenu];
            [Default setValue:@"menu_sms_active" forKey:SelectedSideMenuImage];
            [navigationController setViewControllers:@[vc,vc1,vc2] animated:true];
            
            MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
            mainViewController.rootViewController = navigationController;
            [mainViewController setupWithType:11];
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
            navController.navigationBar.hidden = true;
            
            vc2.ThreadMessageDic = response1[@"data"];
            vc2.number = [respArray valueForKey:@"threadNumber"];
            vc2.frmNumber = [respArray valueForKey:@"chNumber"];
            vc2.contactId = [respArray valueForKey:@"contactId"];
            vc2.VcFrom = @"";
            vc2.tblContactArray = respArray;
            vc2.titleString = [respArray valueForKey:@"threadName"];
            vc2.threadId = [respArray valueForKey:@"threadId"];
            //  [Default setValue:@"1" forKey:IS_ChatviewDisplayed];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            navController.navigationBar.hidden = true;
            [appDelegate window].rootViewController = navController;
            [[appDelegate window] setNeedsLayout];
            [[appDelegate window] makeKeyAndVisible];
            
            
        }
        
    }
}

-(NSDate *)getdateString : (NSString*)strDate {
    NSArray *strArray = [strDate componentsSeparatedByString:@" "];
    NSString *finalStr = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",strArray[0],strArray[1],strArray[2],strArray[3],strArray[4]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *date = [dateFormatter dateFromString:finalStr];
    return date;
}

-(void)gotoLogs {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
    CalllogsVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"CalllogsVC"];
    [Default setValue:@"All Calls" forKey:SelectedSideMenu];
    [Default setValue:@"menu_log_active" forKey:SelectedSideMenuImage];
    [navigationController setViewControllers:@[vc,vc1,vc2] animated:true];
    
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:11];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
    navController.navigationBar.hidden = true;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    navController.navigationBar.hidden = true;
    [appDelegate window].rootViewController = navController;
    [[appDelegate window] setNeedsLayout];
    [[appDelegate window] makeKeyAndVisible];
}
-(void)gotoCallLogDetail:(NSString *)logId
{
//    NSString *userId = [Default valueForKey:USER_ID];
       NSString *url = @"";
       obj = [[WebApiController alloc] init];
       
       
        url = [NSString stringWithFormat:@"callLog/%@",logId];
           
    NSLog(@"url :: %@",url);
    
        [obj callAPI_GET:url andParams:nil SuccessCallback:@selector(callLogDetailResponse:response:) andDelegate:self];
       
}
- (void)callLogDetailResponse:(NSString *)apiAlias response:(NSData *)response{
        
        NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];

    NSLog(@"response 1:: %@",response1);

        NSLog(@"TRUSHANG : STATUSCODE **************8  : %@",apiAlias);
        if([apiAlias isEqualToString:Status_Code])
        {
                UIViewController *viewcontroler = [[UIViewController alloc] init];
                // [UtilsClass logoutUser:viewcontroler];
                [UtilsClass logoutUser:viewcontroler error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
        }
        else
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
            LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            CalllogsVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"CalllogsVC"];
            CalllogsDetailsVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"CalllogsDetailsVC"];
                       
            [Default setValue:@"All Calls" forKey:SelectedSideMenu];
            [Default setValue:@"menu_log_active" forKey:SelectedSideMenuImage];
            [navigationController setViewControllers:@[vc,vc1,vc2] animated:true];
                       
            MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
            mainViewController.rootViewController = navigationController;
            [mainViewController setupWithType:11];
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
            navController.navigationBar.hidden = true;
                       
                       vc2.userData = response1[@"data"][@"callLog"];
                       
                       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                       
                       navController.navigationBar.hidden = true;
                       [appDelegate window].rootViewController = navController;
                       [[appDelegate window] setNeedsLayout];
                       [[appDelegate
                         window] makeKeyAndVisible];
        }
}
// <<<<<<< HEAD
//-(void)goToDialer
//=======
-(void)goToDialer:(NSString *)phoneNumber
//>>>>>>> ffb4153ec9b3bec9dab49f04c7988e1c173ad220
{

    NSLog(@"go to dialer: ");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
// <<<<<<< HEAD
    
//=======
    vc1.callThroughCallHippo = phoneNumber;
//>>>>>>> ffb4153ec9b3bec9dab49f04c7988e1c173ad220
    [Default setValue:@"Dialer" forKey:SelectedSideMenu];
    [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
    [navigationController setViewControllers:@[vc,vc1] animated:true];
    
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:11];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
    navController.navigationBar.hidden = true;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    navController.navigationBar.hidden = true;
    [appDelegate window].rootViewController = navController;
    [[appDelegate window] setNeedsLayout];
    [[appDelegate window] makeKeyAndVisible];
}
-(void)gotoLowCredit {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
    AddCreditVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"AddCreditVC"];
    [Default setValue:@"Credit" forKey:SelectedSideMenu];
    [Default setValue:@"credit_active" forKey:SelectedSideMenuImage];
    [navigationController setViewControllers:@[vc,vc1,vc2] animated:true];
    
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setupWithType:11];
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
    navController.navigationBar.hidden = true;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    navController.navigationBar.hidden = true;
    [appDelegate window].rootViewController = navController;
    [[appDelegate window] setNeedsLayout];
    [[appDelegate window] makeKeyAndVisible];
}

-(void)gotoReminders {
    
    NSString *numbVer = [Default valueForKey:IS_CALL_PLANNER];
    int num = [numbVer intValue];
    
    //NSLog(@"Callplanner -------------- %@",numbVer);
    
    if (num == 1) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        DialerVC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"DialerVC"];
        ReminderVC *vc2 = [storyboard instantiateViewControllerWithIdentifier:@"ReminderVC"];
        [Default setValue:@"Call Planner" forKey:SelectedSideMenu];
        [Default setValue:@"menu_reminder_active" forKey:SelectedSideMenuImage];
        [navigationController setViewControllers:@[vc,vc1,vc2] animated:true];
        
        MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        mainViewController.rootViewController = navigationController;
        [mainViewController setupWithType:11];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainViewController];
        navController.navigationBar.hidden = true;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        navController.navigationBar.hidden = true;
        [appDelegate window].rootViewController = navController;
        [[appDelegate window] setNeedsLayout];
        [[appDelegate window] makeKeyAndVisible];
    }else{
        //        [Default setValue:@"Dialer" forKey:SelectedSideMenu];
        //        [Default setValue:@"dialpadon" forKey:SelectedSideMenuImage];
        //    [UtilsClass showAlert:@"Call Reminder is not available in your plan Please Upgrade your plan" contro:self];
    }
}


- (void)dismissVideoActionSheet:(NSTimer *)timer
{
}

#define PushKit Delegate Methods
#pragma mark - PushKit Functions

//2.1.17

-(void)voipRegistration:(NSString *)username password:(NSString *)password
{
    
//    if (username != nil && password != nil) {
//         [[Phone sharedInstance] login:username endPointPassword:password ];
//    }
   
    
    pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    pushRegistry.delegate = self;
    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
    
    [self useVoipToken:[pushRegistry pushTokenForType:PKPushTypeVoIP]];
    
}
-(void)useVoipToken:(NSData *)tokenData
{
    deviceTokenForPlivo = tokenData;
    [Default setValue:deviceTokenForPlivo forKey:@"deviceTokenForPlivo"];
    [Default synchronize];
}

- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(PKPushType)type {
    
     NSLog(@"credentials >>>>>>>>>>>> %@",credentials.token);
    
    
    if ([credentials.token length] == 0)
    {
        NSLog(@"token blank not updated");
        return;
    }
    
    Voiptoken = @"";
    NSLog(@"Token_Voip : %@",credentials.token);
    if (credentials.token != nil)
    {
        NSString *token = [self stringWithDeviceToken:credentials.token];
        Voiptoken_data = [credentials token];
        NSLog(@"Voip data : %@",Voiptoken_data);
        Voiptoken = token;
    }
    else
    {
        Voiptoken = @"";
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:Voiptoken forKey:@"VOIPTOKEN"];
    [[NSUserDefaults standardUserDefaults] setValue:Voiptoken_data forKey:@"VOIPTOKEN_DATA"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    NSString *LoginProvider = [Default valueForKey:Login_Provider];
//    if([LoginProvider isEqualToString:Login_Plivo])
//    {
        /* old
         
         NSString *plivoUserName = [Default valueForKey:ENDPOINT_USERNAME];
        NSString *plivoPassword = [Default valueForKey:ENDPOINT_PASSORD];
        if(plivoUserName != nil && plivoPassword != nil){
            
            didUpdatePushCredentials = true;
            [[Phone sharedInstance] login:plivoUserName endPointPassword:plivoPassword deviceToken: [credentials token]];
        }*/
        NSString *plivoUserName = [Default valueForKey:ENDPOINT_USERNAME];
        NSString *plivoPassword = [Default valueForKey:ENDPOINT_PASSORD];
        
        [self useVoipToken:credentials.token];
        
    
       if (plivoUserName != nil && plivoPassword != nil) {

            NSLog(@"endpoint logged in");
                didUpdatePushCredentials = true;
                [[Phone sharedInstance] login:plivoUserName endPointPassword:plivoPassword deviceToken:credentials.token];
         }
    
        
//
        
 //   }
    
    NSString *userid = [Default valueForKey:USER_ID];
    if(![Fcmtoken isEqualToString: @""] && userid != nil)
    {
        [self updatefcmtoken];
    }
}

- (void)pushRegistry:(PKPushRegistry *)registry didInvalidatePushTokenForType:(NSString *)type {
    
    
}

- (void)processPush:(NSDictionary *)userInfo {
    NSLog(@"Trushang_push : processPush : %@",userInfo);
    
    //linphone
    NSLog(@"Trushang_push : processPush : %@",userInfo);
    LOGI(@"[PushKit] Notification [%p] received with pay load : %@", userInfo, userInfo.description);
    
    // prevent app to crash if PushKit received for msg
    if ([userInfo[@"aps"][@"loc-key"] isEqualToString:@"IM_MSG"]) {
        LOGE(@"Received a legacy PushKit notification for a chat message");
        [LinphoneManager.instance lpConfigSetInt:[LinphoneManager.instance lpConfigIntForKey:@"unexpected_pushkit" withDefault:0]+1 forKey:@"unexpected_pushkit"];
        return;
    }
    [LinphoneManager.instance startLinphoneCore];
}

- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type withCompletionHandler:(void (^)(void))completion {
    
    NSLog(@"Trushang_push_1 : didReceiveIncomingPushWithPayload : %@",payload.dictionaryPayload);
    NSDictionary *push_payload = payload.dictionaryPayload;
    NSDictionary *aps = [payload.dictionaryPayload objectForKey:@"aps"];

    NSString *Userid = [Default valueForKey:USER_ID];
    Call_sid = nil;
    NSString *calling_provider = @"";
    
    
        if([push_payload valueForKey:@"twi_bridge_token"] != nil)
        {
            NSLog(@"----------------/n /n Twilio /n /n ");
            NSString *lastcallsid = [Default valueForKey:LastCallSidTwilio] ? [Default valueForKey:LastCallSidTwilio] : @"";
            
            if (![lastcallsid isEqualToString:[push_payload valueForKey:@"twi_call_sid"]])
            {
                if (Userid != nil)
                {
                    calling_provider = Login_Twilio;
                    [Default setValue:calling_provider forKey:CallingProvider];
                    [Default synchronize];
                    if ([type isEqualToString:PKPushTypeVoIP])
                    {
                        NSString *last_call_sid = [payload.dictionaryPayload valueForKey:@"twi_call_sid"];
                        [Default setValue:last_call_sid forKey:LastCallSidTwilio];
                        [Default synchronize];
                        
                        
                        if (![TwilioVoiceSDK handleNotification:payload.dictionaryPayload delegate:self delegateQueue:nil])
                        {
                        }
                    }
                    if ([[NSProcessInfo processInfo] operatingSystemVersion].majorVersion < 13)
                    {
                        
                    }
                    else
                    {
                        completion();
                    }
                }
            }
            
        }
        else
        {
           /* NSString *LoginProvider = [Default valueForKey:Login_Provider];
            if([LoginProvider isEqualToString:Login_Plivo])
            {*/
                NSLog(@"----------------/n /n plivo /n /n ");
                calling_provider = Login_Plivo;
                [Default setValue:calling_provider forKey:CallingProvider];
                [Default synchronize];
                if (Userid != nil)
                {

                    //if(![aps objectForKey:@"call-id"])
                  //  {
                        //dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (type == PKPushTypeVoIP) {
                            
                            if (!didUpdatePushCredentials) {

                            NSString *plivoUserName = [Default valueForKey:ENDPOINT_USERNAME];
                            NSString *plivoPassword = [Default valueForKey:ENDPOINT_PASSORD];

                            if(plivoUserName != nil && plivoPassword != nil){
                                    [[Phone sharedInstance] login:plivoUserName endPointPassword:plivoPassword deviceToken:deviceTokenForPlivo];
                            }

                            }
                            
                            [[Phone sharedInstance] setDelegate:self];
                            [[Phone sharedInstance] relayVoipPushNotification:payload.dictionaryPayload];
                            
                          /*old
                           [[Phone sharedInstance] setDelegate:self];
                                                      [[Phone sharedInstance] relayVoipPushNotification:payload.dictionaryPayload];
                           */
                           
                        }
                        
                     //                            });
                  //  }

                }
            }
      //  }
}

- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type
{
    NSLog(@"Trushang_push_2 : didReceiveIncomingPushWithPayload : %@",payload.dictionaryPayload);
}

#pragma mark - NSUser notifications
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wstrict-prototypes"

- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forLocalNotification:(UILocalNotification *)notification
  completionHandler:(void (^)())completionHandler
{
    
}

- (void)application:(UIApplication *)application
handleActionWithIdentifier:(NSString *)identifier
forLocalNotification:(UILocalNotification *)notification
   withResponseInfo:(NSDictionary *)responseInfo
  completionHandler:(void (^)())completionHandler
{
    
}

-(void)updatefcmtoken
{
    NSString *userID = [Default valueForKey:USER_ID];
    //  NSString *authToken = [Default valueForKey:AUTH_TOKEN];
    NSString *fcmtoken = [Default valueForKey:@"FCMTOKEN"];
    NSString *fcmurl = [NSString stringWithFormat:@"updatefcmtoken/%@",userID];
    NSLog(@"updatefcmtoken : %@",fcmtoken);
    NSLog(@"updatefcmtoken Url :  %@%@",SERVERNAME,fcmurl);
    NSDictionary *passDict = @{@"token":fcmtoken,@"device":@"ios",@"iosvoip":Voiptoken};
    NSLog(@"passDict %@",passDict);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    [obj callAPI_POST_RAW:fcmurl andParams:jsonString SuccessCallback:@selector(login:response:) andDelegate:self];
}

- (void)login:(NSString *)apiAlias response:(NSData *)response{
    
    //    NSLog(@"TRUSHANG : STATUSCODE **************9  : %@",apiAlias);
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *viewcontroler = [[UIViewController alloc] init];
       // [UtilsClass logoutUser:viewcontroler];
         [UtilsClass logoutUser:viewcontroler error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
    }
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> *restorableObjects))restorationHandler
{
    //    if ([userActivity.interaction.intent isKindOfClass:[INStartAudioCallIntent class]])
    //    {
// <<<<<<< HEAD
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//
//        [self goToDialer];
//        dispatch_async( dispatch_get_main_queue(), ^{
// =======
   
    
//  >>>>>>> ffb4153ec9b3bec9dab49f04c7988e1c173ad220
           INPerson *person = [[(INStartAudioCallIntent*)userActivity.interaction.intent contacts] firstObject];
           NSString *phoneNumber = person.personHandle.value;
           NSLog(@"Trushang Patel : continueUserActivity : %@",phoneNumber);
           NSDictionary* userInfo = @{@"Number": phoneNumber};
// <<<<<<< HEAD
           
//           NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//           [nc postNotificationName:@"callfromcallhippo" object:self userInfo:userInfo];
//        });
//        });
// =======
           [self goToDialer:phoneNumber];
           NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
           [nc postNotificationName:@"callfromcallhippo" object:self userInfo:userInfo];
            
    
     //   });
      //  });
// >>>>>>> ffb4153ec9b3bec9dab49f04c7988e1c173ad220
    
    
    
    
    
    //    }
    return YES;
}

- (NSString *)valueForKey:(NSString *)key fromQueryItems:(NSArray *)queryItems {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems filteredArrayUsingPredicate:predicate] firstObject];
    return queryItem.value;
}

-(void)app_update_check
{
    if([UtilsClass isNetworkAvailable])
    {
        
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        //    NSString* appID = APP_ID; //[Default objectForKey:APP_ID];
        NSString* bundleId = infoDictionary[@"CFBundleIdentifier"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", bundleId]];
        NSData* data = [NSData dataWithContentsOfURL:url];
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *CurrentVersion = APP_VERSION; //[Default objectForKey:APP_VERSION];
        if ([lookup[@"resultCount"] integerValue] == 1)
        {
            NSString* appStoreVersion = lookup[@"results"][0][@"version"];
            NSString* currentVersion = CurrentVersion;
            if (![appStoreVersion isEqualToString:currentVersion])
            {
                [self app_update_alert:appStoreVersion];
                NSLog(@"\n \n Trushang_code : Need to update [%@ != %@]", appStoreVersion, currentVersion);
                
            }
        }
        else
        {
            NSLog(@"\n \n  Trushang_code : Your app is updated");
        }
    }
    
}
- (void)app_update_alert:(NSString *)newversion
{
    NSString *version_message = [NSString stringWithFormat:@"Version %@ is available on the AppStore",newversion];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"New Version"
                                 message:version_message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Update"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self itunesopen];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Not Now"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    //    [self presentViewController:alert animated:YES completion:nil];
    UIWindow *alertWindow = self.window; //[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    //    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)itunesopen
{
    NSString* appID = APP_ID; //[Default objectForKey:APP_ID];
    NSString *urlString = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@",appID];
    NSLog(@"urlString : %@",urlString);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(void)Foreground_Action:(NSNotification *)notification
{
    [self contact_get];
}
-(void)contact_get
{
    [GlobalData get_all_phone_contacts];
}


- (void)onIncomingCall:(PlivoIncoming *)incoming
{
    
    NSLog(@"\n \n \n \n \n \n \n \n ");
    NSLog(@"Trusahng call Working ");
    NSLog(@"\n \n \n \n \n \n \n \n ");
    
    switch ([[AVAudioSession sharedInstance] recordPermission]) {
            
        case AVAudioSessionRecordPermissionGranted:
        {
            
            NSDictionary *dic = [[NSDictionary alloc] init];
            NSDictionary *extraHeader = [[NSDictionary alloc] init];
            NSString *xphto = @"";
            NSString *xphfrom = @"";
            NSString *XPHFromnumber = @"";
            NSString *XPHtoTransferNumber = @"";
            NSString *XPHNetworkstrength = @"";
            NSString *XPHTransferid = @"";
            NSString *XPHfromTransferNumber = @"";
            NSString *XPHCallhold = @"";
            NSString *XPHCalltransfer = @"";
            NSString *XPHLastcallcreateddate = @"";
            NSString *XPHLastcallcalltype = @"";
            NSString *XPHLastcalluser = @"";
            NSString *XPHLastcallstatus = @"";
            NSString *XPhFirstcallusername = @"";
            NSString *XPHPostCallSurvey = @"";
            NSString *XPHBlindTransfer = @"";
            NSString *XPHRecordingPermission = @"";
            NSString *XPHRecordControl = @"";
            

            
            NSLog(@"Trusahng call Working  %@",incoming.extraHeaders);
            if(incoming.extraHeaders != nil)
            {
                dic = incoming.extraHeaders;
                
                if([dic valueForKey:@"X-PH-To"])
                {
                    NSLog(@"X-PH-To %@",[dic valueForKey:@"X-PH-To"]);
                    xphto = dic[@"X-PH-To"];
                    xphto = [xphto
                             stringByReplacingOccurrencesOfString:@": " withString:@""];
                    xphto = [xphto
                             stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    xphto = [xphto stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-From"])
                {
                    NSLog(@"X-PH-From %@",[dic valueForKey:@"X-PH-From"]);
                    xphfrom = dic[@"X-PH-From"];
                    xphfrom = [xphfrom
                               stringByReplacingOccurrencesOfString:@": " withString:@""];
                    xphfrom = [xphfrom
                               stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    xphfrom = [xphfrom stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Fromnumber"])
                {
                    NSLog(@"X-PH-Fromnumber %@",[dic valueForKey:@"X-PH-Fromnumber"]);
                    XPHFromnumber = dic[@"X-PH-Fromnumber"];
                    XPHFromnumber = [XPHFromnumber
                                     stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHFromnumber = [XPHFromnumber
                                     stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHFromnumber = [XPHFromnumber stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-toTransferNumber"])
                {
                    NSLog(@"X-PH-toTransferNumber %@",[dic valueForKey:@"X-PH-toTransferNumber"]);
                    XPHtoTransferNumber = dic[@"X-PH-toTransferNumber"];
                    XPHtoTransferNumber = [XPHtoTransferNumber
                                           stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHtoTransferNumber = [XPHtoTransferNumber
                                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHtoTransferNumber = [XPHtoTransferNumber stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Networkstrength"])
                {
                    NSLog(@"X-PH-Networkstrength %@",[dic valueForKey:@"X-PH-Networkstrength"]);
                    XPHNetworkstrength = dic[@"X-PH-Networkstrength"];
                    XPHNetworkstrength = [XPHNetworkstrength
                                          stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHNetworkstrength = [XPHNetworkstrength
                                          stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHNetworkstrength = [XPHNetworkstrength stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Transferid"])
                {
                    NSLog(@"X-PH-Transferid %@",[dic valueForKey:@"X-PH-Transferid"]);
                    XPHTransferid = dic[@"X-PH-Transferid"];
                    XPHTransferid = [XPHTransferid
                                     stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHTransferid = [XPHTransferid
                                     stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHTransferid = [XPHTransferid stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-fromTransferNumber"])
                {
                    NSLog(@"X-PH-fromTransferNumber %@",[dic valueForKey:@"X-PH-fromTransferNumber"]);
                    XPHfromTransferNumber = dic[@"X-PH-fromTransferNumber"];
                    XPHfromTransferNumber = [XPHfromTransferNumber
                                             stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHfromTransferNumber = [XPHfromTransferNumber
                                             stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHfromTransferNumber = [XPHfromTransferNumber stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Callhold"])
                {
                    NSLog(@"X-PH-Callhold %@",[dic valueForKey:@"X-PH-Callhold"]);
                    XPHCallhold = dic[@"X-PH-Callhold"];
                    XPHCallhold = [XPHCallhold
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHCallhold = [XPHCallhold
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHCallhold = [XPHCallhold stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-Ph-maincall"] || [dic valueForKey:@"X-Ph-Maincall"])
                {
                    NSLog(@"maincall >>>>");
                    [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:kISTransferCallForTag];
                }
                else
                {
                    NSLog(@"not maincall >>>>");
                    [[NSUserDefaults standardUserDefaults] setValue:@"true" forKey:kISTransferCallForTag];
                }
                if([dic valueForKey:@"X-Ph-maincall"] || [dic valueForKey:@"X-Ph-Maincall"])
                {
                    NSLog(@"---------------X-Ph-Maincall %@ -- %@",[dic valueForKey:@"X-Ph-maincall"],[dic valueForKey:@"X-Ph-Maincall"]);
                    XPHCalltransfer = dic[@"X-Ph-Maincall"] ? dic[@"X-Ph-Maincall"] : dic[@"X-Ph-maincall"];
                    XPHCalltransfer = [XPHCalltransfer
                                       stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHCalltransfer = [XPHCalltransfer
                                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHCalltransfer = [XPHCalltransfer stringByRemovingPercentEncoding];
                }
                
                
                if([dic valueForKey:@"X-PH-Lastcallcreateddate"])
                {
                    NSLog(@"X-PH-Lastcallcreateddate %@",[dic valueForKey:@"X-PH-Lastcallcreateddate"]);
                    XPHLastcallcreateddate = dic[@"X-PH-Lastcallcreateddate"];
                    XPHLastcallcreateddate = [XPHLastcallcreateddate
                                              stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHLastcallcreateddate = [XPHLastcallcreateddate
                                              stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHLastcallcreateddate = [XPHLastcallcreateddate stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Lastcallcalltype"])
                {
                    NSLog(@"X-PH-Lastcallcalltype %@",[dic valueForKey:@"X-PH-Lastcallcalltype"]);
                    XPHLastcallcalltype = dic[@"X-PH-Lastcallcalltype"];
                    XPHLastcallcalltype = [XPHLastcallcalltype
                                           stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHLastcallcalltype = [XPHLastcallcalltype
                                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHLastcallcalltype = [XPHLastcallcalltype stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Lastcalluser"])
                {
                    NSLog(@"X-PH-Lastcalluser %@",[dic valueForKey:@"X-PH-Lastcalluser"]);
                    XPHLastcalluser = dic[@"X-PH-Lastcalluser"];;
                    XPHLastcalluser = [XPHLastcalluser
                                       stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHLastcalluser = [XPHLastcalluser
                                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHLastcalluser = [XPHLastcalluser stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-Lastcallstatus"])
                {
                    NSLog(@"X-PH-Lastcallstatus %@",[dic valueForKey:@"X-PH-Lastcallstatus"]);
                    XPHLastcallstatus = dic[@"X-PH-Lastcallstatus"];
                    XPHLastcallstatus = [XPHLastcallstatus
                                         stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHLastcallstatus = [XPHLastcallstatus
                                         stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHLastcallstatus = [XPHLastcallstatus stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-PH-FirstCallUserName"])
                {
                    NSLog(@"X-PH-Lastcallstatus %@",[dic valueForKey:@"X-PH-FirstCallUserName"]);
                    XPhFirstcallusername = dic[@"X-PH-FirstCallUserName"];
                    XPhFirstcallusername = [XPhFirstcallusername
                                            stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPhFirstcallusername = [XPhFirstcallusername
                                            stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPhFirstcallusername = [XPhFirstcallusername stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-Ph-postCallSurvey"])
                {
                    NSLog(@"X-Ph-postCallSurvey %@",[dic valueForKey:@"X-Ph-postCallSurvey"]);
                    XPHPostCallSurvey = dic[@"X-Ph-postCallSurvey"];
                    XPHPostCallSurvey = [XPHPostCallSurvey
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHPostCallSurvey = [XPHPostCallSurvey
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHPostCallSurvey = [XPHPostCallSurvey stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-Ph-Blindtransfer"])
                {
                    NSLog(@"X-Ph-Blindtransfer %@",[dic valueForKey:@"X-Ph-Blindtransfer"]);
                    XPHBlindTransfer = dic[@"X-Ph-Blindtransfer"];
                    XPHBlindTransfer = [XPHBlindTransfer
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHBlindTransfer = [XPHBlindTransfer
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHBlindTransfer = [XPHBlindTransfer stringByRemovingPercentEncoding];
                }
                if([dic valueForKey:@"X-Ph-recordingPermission"])
                {
                    NSLog(@"X-Ph-recordingPermission %@",[dic valueForKey:@"X-Ph-recordingPermission"]);
                    XPHRecordingPermission = dic[@"X-Ph-recordingPermission"];
                    XPHRecordingPermission = [XPHRecordingPermission
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHRecordingPermission = [XPHRecordingPermission
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHRecordingPermission = [XPHRecordingPermission stringByRemovingPercentEncoding];
                    
                }
                if([dic valueForKey:@"X-Ph-recordcontrol"])
                {
                    NSLog(@"X-Ph-recordcontrol %@",[dic valueForKey:@"X-Ph-recordcontrol"]);
                    XPHRecordControl = dic[@"X-Ph-recordcontrol"];
                    XPHRecordControl = [XPHRecordControl
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
                    XPHRecordControl = [XPHRecordControl
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                    XPHRecordControl = [XPHRecordControl stringByRemovingPercentEncoding];
                    
                }
                
                
                NSLog(@"Trush  %@",xphto);
                NSLog(@"Trush  %@",xphfrom);
                NSLog(@"Trush  %@",XPHFromnumber);
                NSLog(@"Trush  %@",XPHtoTransferNumber);
                NSLog(@"Trush  %@",XPHNetworkstrength);
                NSLog(@"Trush  %@",XPHTransferid);
                NSLog(@"Trush  %@",XPHfromTransferNumber);
                NSLog(@"Trush  %@",XPHCallhold);
                NSLog(@"Trush  %@",XPHCalltransfer);
                NSLog(@"Trush  %@",XPHLastcallcreateddate);
                NSLog(@"Trush  %@",XPHLastcallcalltype);
                NSLog(@"Trush  %@",XPHLastcalluser);
                NSLog(@"Trush  %@",XPHLastcallstatus);
                
                if(xphto == nil  || [xphto isKindOfClass:[NSNull class]])
                {
                    xphto = @"";
                }
                if(xphfrom == nil  || [xphfrom isKindOfClass:[NSNull class]])
                {
                    xphfrom = @"";
                }
                if(XPHFromnumber == nil  || [XPHFromnumber isKindOfClass:[NSNull class]])
                {
                    XPHFromnumber = @"";
                }
                if(XPHtoTransferNumber == nil  || [XPHtoTransferNumber isKindOfClass:[NSNull class]])
                {
                    XPHtoTransferNumber = @"";
                }
                if(XPHNetworkstrength == nil  || [XPHNetworkstrength isKindOfClass:[NSNull class]])
                {
                    XPHNetworkstrength = @"";
                }
                if(XPHTransferid == nil  || [XPHTransferid isKindOfClass:[NSNull class]])
                {
                    XPHTransferid = @"";
                }
                if(XPHfromTransferNumber == nil  || [XPHfromTransferNumber isKindOfClass:[NSNull class]])
                {
                    XPHfromTransferNumber = @"";
                }
                if(XPHCallhold == nil  || [XPHCallhold isKindOfClass:[NSNull class]])
                {
                    XPHCallhold = @"";
                }
                if(XPHCalltransfer == nil  || [XPHCalltransfer isKindOfClass:[NSNull class]])
                {
                    XPHCalltransfer = @"";
                }
                if(XPHLastcallcreateddate == nil  || [XPHLastcallcreateddate isKindOfClass:[NSNull class]])
                {
                    XPHLastcallcreateddate = @"";
                }
                if(XPHLastcallcalltype == nil  || [XPHLastcallcalltype isKindOfClass:[NSNull class]])
                {
                    XPHLastcallcalltype = @"";
                }
                if(XPHLastcalluser == nil  || [XPHLastcalluser isKindOfClass:[NSNull class]])
                {
                    XPHLastcalluser = @"";
                }
                if(XPHLastcallstatus == nil  || [XPHLastcallstatus isKindOfClass:[NSNull class]])
                {
                    XPHLastcallstatus = @"";
                }
                if(XPhFirstcallusername == nil  || [XPhFirstcallusername isKindOfClass:[NSNull class]])
                {
                    XPhFirstcallusername = @"";
                }
                if(XPHPostCallSurvey == nil  || [XPHPostCallSurvey isKindOfClass:[NSNull class]])
                {
                    XPHPostCallSurvey = @"";
                }
                if(XPHBlindTransfer == nil  || [XPHBlindTransfer isKindOfClass:[NSNull class]])
                {
                    XPHBlindTransfer = @"";
                }
                if(XPHRecordingPermission == nil  || [XPHRecordingPermission isKindOfClass:[NSNull class]])
                {
                    XPHRecordingPermission = @"";
                }
                if(XPHRecordControl == nil  || [XPHRecordControl isKindOfClass:[NSNull class]])
                {
                    XPHRecordControl = @"";
                }
                
                
                extraHeader = @{               @"xphto" : xphto,
                                               @"xphfrom" : xphfrom,
                                               @"xphfromnumber" : XPHFromnumber,
                                               @"xphtotransferNumber" : XPHtoTransferNumber,
                                               @"xphnetworkstrength" : XPHNetworkstrength,
                                               @"xphtransferid" : XPHTransferid,
                                               @"xphfromtransferNumber" : XPHfromTransferNumber,
                                               @"xphcallhold" : XPHCallhold,
                                               @"xphcalltransfer" : XPHCalltransfer,
                                               @"xphlastcallcreateddate" : XPHLastcallcreateddate,
                                               @"xphlastcallcalltype" : XPHLastcallcalltype,
                                               @"xphlastcalluser" : XPHLastcalluser,
                                               @"xphlastcallstatus" : XPHLastcallstatus,
                                               @"xphfirstcallusername" :XPhFirstcallusername,@"xphpostcallsurvey":XPHPostCallSurvey,@"xphblindtransfer":XPHBlindTransfer,
                                               @"xphrecordingpermission":XPHRecordingPermission,
                                               @"xphrecordcontrol":XPHRecordControl
                                               };
                
            }
            else
            {
                extraHeader = @{               @"xphto" : xphto,
                                               @"xphfrom" : xphfrom,
                                               @"xphfromnumber" : XPHFromnumber,
                                               @"xphtotransferNumber" : XPHtoTransferNumber,
                                               @"xphnetworkstrength" : XPHNetworkstrength,
                                               @"xphtransferid" : XPHTransferid,
                                               @"xphfromtransferNumber" : XPHfromTransferNumber,
                                               @"xphcallhold" : XPHCallhold,
                                               @"xphcalltransfer" : XPHCalltransfer,
                                               @"xphlastcallcreateddate" : XPHLastcallcreateddate,
                                               @"xphlastcallcalltype" : XPHLastcallcalltype,
                                               @"xphlastcalluser" : XPHLastcalluser,
                                               @"xphlastcallstatus" : XPHLastcallstatus,
                                               @"xphfirstcallusername" :XPhFirstcallusername, @"xphrecordingpermission":XPHRecordingPermission,
                                               @"xphrecordcontrol":XPHRecordControl
                                               };
            }
            
            
            
            self.extHeader = extraHeader;
            NSLog(@"Trushang : ExtraHeader : %@", self.extHeader);
            
            [[NSUserDefaults standardUserDefaults] setObject:self.extHeader forKey:@"extraHeader"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *incName;
            incName = @"";
            incName = self.extHeader[@"X-PH-From"];
            incName = [incName
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            incName = [incName
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            
            //2.1.12
            //incCall = incoming;
            
            outCall = nil;
            
            [CallKitInstance sharedInstance].callUUID = [NSUUID UUID];
            [CallKitInstance sharedInstance].calls_uuids = [[NSMutableArray alloc] init];
            [ [CallKitInstance sharedInstance] reportIncomingCallFrom:incName withUUID:[CallKitInstance sharedInstance].callUUID extHeader:extHeader incommingcall:incoming];
            break;
        }
        case AVAudioSessionRecordPermissionDenied:
            
            break;
        case AVAudioSessionRecordPermissionUndetermined:
            // This is the initial state before a user has made any choice
            // You can use this spot to request permission here if you want
            break;
        default:
            break;
    }
    
    
}


- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    
    
   return [[GIDSignIn sharedInstance] handleURL:url];
   
    

}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error != nil) {
        if (error.code == kGIDSignInErrorCodeHasNoAuthInKeychain) {
            NSLog(@"The user has not signed in before or they have since signed out.");
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
        // [START_EXCLUDE silent]
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"ToggleAuthUINotification"
         object:nil
         userInfo:nil];
        // [END_EXCLUDE]
        return;
    }
    
    
    
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    // [START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText":
                                     [NSString stringWithFormat:@"Signed in user: %@",
                                      fullName]};
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}
// [END signin_handler]

// This callback is triggered after the disconnect call that revokes data
// access to the user's resources has completed.
// [START disconnect_handler]
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // [START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}


-(void)all_contact_get
{
    Mixallcontact = [[NSMutableArray alloc] init];
   //old Mixallcontact = [[GlobalData sharedGlobalData] get_mix_contact_list];
    Mixallcontact = [[GlobalData sharedGlobalData] get_chAndDeviceContactList];
}
-(void)save_contact:(NSString *)contact_number
{
    NSLog(@"Contact Get : %@",contact_number);
    NSString *num = contact_number;
    NSString *string = [num stringByReplacingOccurrencesOfString:@"(" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@")" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"-" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    
    //p NSPredicate *filter = [NSPredicate predicateWithFormat:@"number_int contains[c] %@ ",string];
    
    //-mixallcontact
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY numberArray.number_int contains[c] %@",string] ;
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"ANY numberArray.number contains[c] %@",string] ;
    
    NSPredicate *predicatefinal = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate,predicate2]];


    
    NSArray *filteredContacts = [Mixallcontact filteredArrayUsingPredicate:predicatefinal];
    
    if(filteredContacts.count != 0)
    {
        NSLog(@"filtered Contacts : %@",filteredContacts);
        NSDictionary *dic = [filteredContacts objectAtIndex:0];
        if(!dic[@"_id"])
        {
            NSLog(@"Call Contact Find search dic == == > : %@",dic);
            NSString *ContactName = [dic valueForKey:@"name"];
            //p NSString *ContactNumber = [dic valueForKey:@"number"];
            
            NSString *ContactNumber;
            if ([[dic valueForKey:@"numberArray"] count]>0) {
                ContactNumber = [[[dic valueForKey:@"numberArray"] objectAtIndex:0] valueForKey:@"number"] ;
            }
            else
            {
                ContactNumber =@"";
            }

            [UtilsClass contact_save_in_callhippo:ContactName contact_number:ContactNumber];
        }
        else
        {
            
        }
        
    }
}

-(void)UnreadStatus:(NSString *)threadid
{
    NSString *url = [NSString stringWithFormat:@"updatesmsstatus/%@",threadid];
    NSDictionary *passDict = @{@"threadIds":threadid,
                               };
    //    [obj callAPI_POST:url andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    //    [obj callAPI_POST:Login_URL andParams:passDict SuccessCallback:@selector(login:response:) andDelegate:self];
    
    [obj callAPI_POST_RAW:url andParams:jsonString SuccessCallback:@selector(read:response:) andDelegate:self];
    
}

- (void)read:(NSString *)apiAlias response:(NSData *)response
{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"TRUSHANG : STATUSCODE **************  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        UIViewController *viewcontroler = [[UIViewController alloc] init];

       // [UtilsClass logoutUser:self];
         [UtilsClass logoutUser:viewcontroler error:[response1 valueForKey:@"error"][@"error"] showAlert:YES];
    }
    else
    {
        
        if ([[response1 valueForKey:@"success"] integerValue] == 1)
        {
            
        }
        else
        {
            
        }
        
    }
}


- (void)settingApi {
    
    NSDictionary *passDict = @{@"device":@"ios",@"features":@""};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:nil];
    NSString *jsonString;
    if (! jsonData) {
        
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    obj = [[WebApiController alloc] init];
    
    [obj callAPI_POST_RAW:@"setting/view" andParams:jsonString SuccessCallback:@selector(setting:response:) andDelegate:self];
}
- (void)setting:(NSString *)apiAlias response:(NSData *)response{
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
    
    //    NSLog(@"TRUSHANG : STATUSCODE **************25  : %@",apiAlias);
    if([apiAlias isEqualToString:Status_Code])
    {
        
    }
    else
    {
        
            NSLog(@"setting response : %@",response1);
        if ([[response1 valueForKey:@"success"] integerValue] == 1) {
            [Default setValue:[[response1 valueForKey:@"feature"] valueForKey:@"smartlook"] forKey:SMARTLOOK];
            [Default setValue:[[response1 valueForKey:@"feature"] valueForKey:@"loginGmail"] forKey:LOGINGMAIL];
            
           
            if ([[[response1 valueForKey:@"feature"] valueForKey:@"smartlook"] isEqualToString:@"ON"]){
                
                
              //  [Smartlook setupWithKey:@"558d49e421641609757c7f49f9dd7a4d5fbd1b5c"];
                [Smartlook setupWithKey:@"049cf4bb392629aa30fac6448254dcad44f73063"];
                [Smartlook startRecording];
           }
            
      // Encryption commented  [Default setBool:[[[response1 valueForKey:@"feature"] valueForKey:@"encryptionInMobile"] boolValue] forKey:kencryptionInMobile];
            
           // [Default setBool:true forKey:kencryptionInMobile];

        }
        
    }
}




#pragma mark - Twilio TVONotificationDelegate
- (void)callInviteReceived:(TVOCallInvite *)callInvite {
    
    /**
     * Calling `[TwilioVoice handleNotification:delegate:]` will synchronously process your notification payload and
     * provide you a `TVOCallInvite` object. Report the incoming call to CallKit upon receiving this callback.
     */
    
    NSString *calling_provider = @"";
    calling_provider = Login_Twilio;
    [Default setValue:calling_provider forKey:CallingProvider];
    [Default synchronize];
    twilio_callinvite = [[NSMutableDictionary alloc] init];
    twilio_callinvite[[callInvite.uuid UUIDString]] = callInvite;
    [twilio_callkit sharedInstance].twilio_callinvite = callInvite;
    [twilio_callkit sharedInstance].activeCallInvites = twilio_callinvite;
    
    
    callInvite_incoming = callInvite;
    TVOCall *call = [[NSString stringWithFormat:@"%@",callInvite_incoming.uuid.UUIDString] mutableCopy];
    
    NSLog(@"callInviteReceived: %@",callInvite.customParameters);
    
    NSDictionary *dic = [[NSDictionary alloc] init];
    NSDictionary *extraHeader = [[NSDictionary alloc] init];
    NSString *xphto = @"";
    NSString *xphfrom = @"";
    NSString *XPHFromnumber = @"";
    NSString *XPHtoTransferNumber = @"";
    NSString *XPHNetworkstrength = @"";
    NSString *XPHTransferid = @"";
    NSString *XPHfromTransferNumber = @"";
    NSString *XPHCallhold = @"";
    NSString *XPHCalltransfer = @"";
    NSString *XPHLastcallcreateddate = @"";
    NSString *XPHLastcallcalltype = @"";
    NSString *XPHLastcalluser = @"";
    NSString *XPHLastcallstatus = @"";
    NSString *XPhFirstcallusername = @"";
    NSString *xphextensioncall = @"";
    NSString *XPHPostCallSurvey = @"";
    NSString *XPHBlindTransfer = @"";
    NSString *XPHRecordingPermission = @"";
    NSString *XPHRecordControl = @"";


    [Default setValue:@"false" forKey:ExtentionCall];
    if(callInvite.customParameters != nil && callInvite.customParameters.allValues.count != 0)
    {
        
        NSString *sip = [callInvite.customParameters valueForKey:@"sip"];
        NSArray *arr = [sip componentsSeparatedByString:@","];
        NSMutableDictionary *values = [[NSMutableDictionary alloc] init];
        for (int i = 0; i<arr.count; i++)
        {
            NSString *obj = [arr objectAtIndex:i];
            NSArray *obj_arr = [obj componentsSeparatedByString:@"="];
            NSString *key = [obj_arr objectAtIndex:0];
            NSString *value = [obj_arr objectAtIndex:1];
            [values setValue:value forKey:key];
        }
        
        NSLog(@"Headers : %@",values);
        
        dic = values;
        if([dic valueForKey:@"X-Ph-To"])
        {
            NSLog(@"X-Ph-To %@",[dic valueForKey:@"X-Ph-To"]);
            xphto = dic[@"X-Ph-To"];
            xphto = [xphto
                     stringByReplacingOccurrencesOfString:@": " withString:@""];
            xphto = [xphto
                     stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            xphto = [xphto stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-From"])
        {
            NSLog(@"X-Ph-From %@",[dic valueForKey:@"X-Ph-From"]);
            xphfrom = dic[@"X-Ph-From"];
            xphfrom = [xphfrom
                       stringByReplacingOccurrencesOfString:@": " withString:@""];
            xphfrom = [xphfrom
                       stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            xphfrom = [xphfrom stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Fromnumber"])
        {
            NSLog(@"X-Ph-Fromnumber %@",[dic valueForKey:@"X-Ph-Fromnumber"]);
            XPHFromnumber = dic[@"X-Ph-Fromnumber"];
            XPHFromnumber = [XPHFromnumber
                             stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHFromnumber = [XPHFromnumber
                             stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHFromnumber = [XPHFromnumber stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-toTransferNumber"])
        {
            NSLog(@"X-Ph-toTransferNumber %@",[dic valueForKey:@"X-Ph-toTransferNumber"]);
            XPHtoTransferNumber = dic[@"X-Ph-toTransferNumber"];
            XPHtoTransferNumber = [XPHtoTransferNumber
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHtoTransferNumber = [XPHtoTransferNumber
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHtoTransferNumber = [XPHtoTransferNumber stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Networkstrength"])
        {
            NSLog(@"X-Ph-Networkstrength %@",[dic valueForKey:@"X-Ph-Networkstrength"]);
            XPHNetworkstrength = dic[@"X-Ph-Networkstrength"];
            XPHNetworkstrength = [XPHNetworkstrength
                                  stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHNetworkstrength = [XPHNetworkstrength
                                  stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHNetworkstrength = [XPHNetworkstrength stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Transferid"])
        {
            NSLog(@"X-Ph-Transferid %@",[dic valueForKey:@"X-Ph-Transferid"]);
            XPHTransferid = dic[@"X-Ph-Transferid"];
            XPHTransferid = [XPHTransferid
                             stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHTransferid = [XPHTransferid
                             stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHTransferid = [XPHTransferid stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-fromTransferNumber"])
        {
            NSLog(@"X-Ph-fromTransferNumber %@",[dic valueForKey:@"X-Ph-fromTransferNumber"]);
            XPHfromTransferNumber = dic[@"X-Ph-fromTransferNumber"];
            XPHfromTransferNumber = [XPHfromTransferNumber
                                     stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHfromTransferNumber = [XPHfromTransferNumber
                                     stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHfromTransferNumber = [XPHfromTransferNumber stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Callhold"])
        {
            NSLog(@"X-Ph-Callhold %@",[dic valueForKey:@"X-Ph-Callhold"]);
            XPHCallhold = dic[@"X-Ph-Callhold"];
            XPHCallhold = [XPHCallhold
                           stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHCallhold = [XPHCallhold
                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHCallhold = [XPHCallhold stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-maincall"] || [dic valueForKey:@"X-Ph-Maincall"])
        {
            NSLog(@"maincall >>>>");
            [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:kISTransferCallForTag];
        }
        else
        {
            NSLog(@"not maincall >>>>");
            [[NSUserDefaults standardUserDefaults] setValue:@"true" forKey:kISTransferCallForTag];
        }
        if([dic valueForKey:@"X-Ph-maincall"] || [dic valueForKey:@"X-Ph-Maincall"])
        {
            NSLog(@"---------------X-Ph-Maincall %@ -- %@",[dic valueForKey:@"X-Ph-maincall"],[dic valueForKey:@"X-Ph-Maincall"]);
            XPHCalltransfer = dic[@"X-Ph-Maincall"] ? dic[@"X-Ph-Maincall"] : dic[@"X-Ph-maincall"];
            XPHCalltransfer = [XPHCalltransfer
                               stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHCalltransfer = [XPHCalltransfer
                               stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHCalltransfer = [XPHCalltransfer stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Lastcallcreateddate"])
        {
            NSLog(@"X-Ph-Lastcallcreateddate %@",[dic valueForKey:@"X-Ph-Lastcallcreateddate"]);
            XPHLastcallcreateddate = dic[@"X-Ph-Lastcallcreateddate"];
            XPHLastcallcreateddate = [XPHLastcallcreateddate
                                      stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHLastcallcreateddate = [XPHLastcallcreateddate
                                      stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHLastcallcreateddate = [XPHLastcallcreateddate stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Lastcallcalltype"])
        {
            NSLog(@"X-Ph-Lastcallcalltype %@",[dic valueForKey:@"X-Ph-Lastcallcalltype"]);
            XPHLastcallcalltype = dic[@"X-Ph-Lastcallcalltype"];
            XPHLastcallcalltype = [XPHLastcallcalltype
                                   stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHLastcallcalltype = [XPHLastcallcalltype
                                   stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHLastcallcalltype = [XPHLastcallcalltype stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Lastcalluser"])
        {
            NSLog(@"X-Ph-Lastcalluser %@",[dic valueForKey:@"X-Ph-Lastcalluser"]);
            XPHLastcalluser = dic[@"X-Ph-Lastcalluser"];;
            XPHLastcalluser = [XPHLastcalluser
                               stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHLastcalluser = [XPHLastcalluser
                               stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHLastcalluser = [XPHLastcalluser stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Lastcallstatus"])
        {
            NSLog(@"X-Ph-Lastcallstatus %@",[dic valueForKey:@"X-Ph-Lastcallstatus"]);
            XPHLastcallstatus = dic[@"X-Ph-Lastcallstatus"];
            XPHLastcallstatus = [XPHLastcallstatus
                                 stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHLastcallstatus = [XPHLastcallstatus
                                 stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHLastcallstatus = [XPHLastcallstatus stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-FirstCallUserName"])
        {
            NSLog(@"X-Ph-Lastcallstatus %@",[dic valueForKey:@"X-Ph-FirstCallUserName"]);
            XPhFirstcallusername = dic[@"X-Ph-FirstCallUserName"];
            XPhFirstcallusername = [XPhFirstcallusername
                                    stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPhFirstcallusername = [XPhFirstcallusername
                                    stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPhFirstcallusername = [XPhFirstcallusername stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Extensioncall"])
        {
            NSLog(@"X-Ph-Extensioncall %@",[dic valueForKey:@"X-Ph-Extensioncall"]);
            xphextensioncall = dic[@"X-Ph-Extensioncall"];
            xphextensioncall = [xphextensioncall
                     stringByReplacingOccurrencesOfString:@": " withString:@""];
            xphextensioncall = [xphextensioncall
                     stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            xphextensioncall = [xphextensioncall stringByRemovingPercentEncoding];
            [Default setValue:xphextensioncall forKey:ExtentionCall];
        }
        if([dic valueForKey:@"X-Ph-postCallSurvey"])
        {
            NSLog(@"X-Ph-postCallSurvey %@",[dic valueForKey:@"X-Ph-postCallSurvey"]);
            XPHPostCallSurvey = dic[@"X-Ph-postCallSurvey"];
            XPHPostCallSurvey = [XPHPostCallSurvey
                           stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHPostCallSurvey = [XPHPostCallSurvey
                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHPostCallSurvey = [XPHPostCallSurvey stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-Blindtransfer"])
        {
            NSLog(@"X-Ph-Blindtransfer %@",[dic valueForKey:@"X-Ph-Blindtransfer"]);
            XPHBlindTransfer = dic[@"X-Ph-Blindtransfer"];
            XPHBlindTransfer = [XPHBlindTransfer
                           stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHBlindTransfer = [XPHBlindTransfer
                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHBlindTransfer = [XPHBlindTransfer stringByRemovingPercentEncoding];
        }
        if([dic valueForKey:@"X-Ph-recordingPermission"])
        {
            NSLog(@"X-Ph-recordingPermission %@",[dic valueForKey:@"X-Ph-recordingPermission"]);
            XPHRecordingPermission = dic[@"X-Ph-recordingPermission"];
            XPHRecordingPermission = [XPHRecordingPermission
                           stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHRecordingPermission = [XPHRecordingPermission
                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHRecordingPermission = [XPHRecordingPermission stringByRemovingPercentEncoding];
            
        }
        if([dic valueForKey:@"X-Ph-recordcontrol"])
        {
            NSLog(@"X-Ph-recordcontrol %@",[dic valueForKey:@"X-Ph-recordcontrol"]);
            XPHRecordControl = dic[@"X-Ph-recordcontrol"];
            XPHRecordControl = [XPHRecordControl
                           stringByReplacingOccurrencesOfString:@": " withString:@""];
            XPHRecordControl = [XPHRecordControl
                           stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
            XPHRecordControl = [XPHRecordControl stringByRemovingPercentEncoding];
            
        }
        
        
        if(xphto == nil  || [xphto isKindOfClass:[NSNull class]])
        {
            xphto = @"";
        }
        if(xphfrom == nil  || [xphfrom isKindOfClass:[NSNull class]])
        {
            xphfrom = @"";
        }
        if(XPHFromnumber == nil  || [XPHFromnumber isKindOfClass:[NSNull class]])
        {
            XPHFromnumber = @"";
        }
        if(XPHtoTransferNumber == nil  || [XPHtoTransferNumber isKindOfClass:[NSNull class]])
        {
            XPHtoTransferNumber = @"";
        }
        if(XPHNetworkstrength == nil  || [XPHNetworkstrength isKindOfClass:[NSNull class]])
        {
            XPHNetworkstrength = @"";
        }
        if(XPHTransferid == nil  || [XPHTransferid isKindOfClass:[NSNull class]])
        {
            XPHTransferid = @"";
        }
        if(XPHfromTransferNumber == nil  || [XPHfromTransferNumber isKindOfClass:[NSNull class]])
        {
            XPHfromTransferNumber = @"";
        }
        if(XPHCallhold == nil  || [XPHCallhold isKindOfClass:[NSNull class]])
        {
            XPHCallhold = @"";
        }
        if(XPHCalltransfer == nil  || [XPHCalltransfer isKindOfClass:[NSNull class]])
        {
            XPHCalltransfer = @"";
        }
        if(XPHLastcallcreateddate == nil  || [XPHLastcallcreateddate isKindOfClass:[NSNull class]])
        {
            XPHLastcallcreateddate = @"";
        }
        if(XPHLastcallcalltype == nil  || [XPHLastcallcalltype isKindOfClass:[NSNull class]])
        {
            XPHLastcallcalltype = @"";
        }
        if(XPHLastcalluser == nil  || [XPHLastcalluser isKindOfClass:[NSNull class]])
        {
            XPHLastcalluser = @"";
        }
        if(XPHLastcallstatus == nil  || [XPHLastcallstatus isKindOfClass:[NSNull class]])
        {
            XPHLastcallstatus = @"";
        }
        if(XPhFirstcallusername == nil  || [XPhFirstcallusername isKindOfClass:[NSNull class]])
        {
            XPhFirstcallusername = @"";
        }
        if(xphextensioncall == nil  || [xphextensioncall isKindOfClass:[NSNull class]])
        {
            xphextensioncall = @"";
        }
        if(XPHPostCallSurvey == nil  || [XPHPostCallSurvey isKindOfClass:[NSNull class]])
        {
            XPHPostCallSurvey = @"";
        }
        if(XPHBlindTransfer == nil  || [XPHBlindTransfer isKindOfClass:[NSNull class]])
        {
            XPHBlindTransfer = @"";
        }
        if(XPHRecordingPermission == nil  || [XPHRecordingPermission isKindOfClass:[NSNull class]])
        {
            XPHRecordingPermission = @"";
        }
        if(XPHRecordControl == nil  || [XPHRecordControl isKindOfClass:[NSNull class]])
        {
            XPHRecordControl = @"";
        }
        
        
        extraHeader = @{               @"xphto" : xphto,
                                       @"xphfrom" : xphfrom,
                                       @"xphfromnumber" : XPHFromnumber,
                                       @"xphtotransferNumber" : XPHtoTransferNumber,
                                       @"xphnetworkstrength" : XPHNetworkstrength,
                                       @"xphtransferid" : XPHTransferid,
                                       @"xphfromtransferNumber" : XPHfromTransferNumber,
                                       @"xphcallhold" : XPHCallhold,
                                       @"xphcalltransfer" : XPHCalltransfer,
                                       @"xphlastcallcreateddate" : XPHLastcallcreateddate,
                                       @"xphlastcallcalltype" : XPHLastcallcalltype,
                                       @"xphlastcalluser" : XPHLastcalluser,
                                       @"xphlastcallstatus" : XPHLastcallstatus,
                                       @"xphfirstcallusername" :XPhFirstcallusername,
                                       @"xphextensioncall": xphextensioncall,
                                       @"xphpostcallsurvey":XPHPostCallSurvey,
                                       @"xphblindtransfer":XPHBlindTransfer,
                                       @"xphrecordingpermission":XPHRecordingPermission,
                                       @"xphrecordcontrol":XPHRecordControl
                                       };
        
    }
    else
    {
        extraHeader = @{               @"xphto" : xphto,
                                       @"xphfrom" : xphfrom,
                                       @"xphfromnumber" : XPHFromnumber,
                                       @"xphtotransferNumber" : XPHtoTransferNumber,
                                       @"xphnetworkstrength" : XPHNetworkstrength,
                                       @"xphtransferid" : XPHTransferid,
                                       @"xphfromtransferNumber" : XPHfromTransferNumber,
                                       @"xphcallhold" : XPHCallhold,
                                       @"xphcalltransfer" : XPHCalltransfer,
                                       @"xphlastcallcreateddate" : XPHLastcallcreateddate,
                                       @"xphlastcallcalltype" : XPHLastcallcalltype,
                                       @"xphlastcalluser" : XPHLastcalluser,
                                       @"xphlastcallstatus" : XPHLastcallstatus,
                                       @"xphfirstcallusername" :XPhFirstcallusername,
                                       @"xphrecordingpermission":XPHRecordingPermission,
                                       @"xphrecordcontrol":XPHRecordControl
                                       };
    }
    
    NSString *from = @"Twilio Test";
    self.extHeader = extraHeader;
    [[NSUserDefaults standardUserDefaults] setObject:self.extHeader forKey:@"extraHeader"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //    twilio_callkit.sharedInstance.delegate = self;
    [twilio_callkit sharedInstance].calls_uuids_twilio = [[NSMutableArray alloc] init];
    [[twilio_callkit sharedInstance] reportIncomingCallFrom:from withUUID:callInvite.uuid extHeader:extraHeader callinvite:callInvite call:call];
    
//    if ([[NSProcessInfo processInfo] operatingSystemVersion].majorVersion < 13) {
//        [self incomingPushHandled];
//    }
    
}
//- (void)incomingPushHandled {
//    if (self.incomingPushCompletionCallback) {
//        self.incomingPushCompletionCallback();
//        self.incomingPushCompletionCallback = nil;
//    }
//}
- (void)cancelledCallInviteReceived:(TVOCancelledCallInvite *)cancelledCallInvite error:(NSError *)error
{
    NSLog(@"cancelledCallInviteReceived:");
    NSLog(@"twilio_callinvite: %@",twilio_callinvite);
    TVOCallInvite *callInvite;
    
    if([cancelledCallInvite.callSid isEqualToString:callInvite_incoming.callSid])
    {
        callInvite = callInvite_incoming;
        if (callInvite) {
            [[twilio_callkit sharedInstance] performEndCallActionWithUUID:callInvite_incoming.uuid];
        }
    }
    else
    {
        
    }
}

-(void)conversaton_screenupdate:(NSString *)threadId readStatus:(NSString *)readStatus smsAuther:(NSString *)smsAuther smsContent:(NSString *)smsContent smsTime:(NSString *)smsTime smsUUID:(NSString *)smsUUID
{
    
    
    NSString *smsthreadId = threadId;
    NSString *smsreadStatus = readStatus;
    NSString *smssmsAuther = smsAuther;
    NSString *smssmsContent = smsContent;
    NSString *smssmsTime = smsTime;
    NSString *smssmsUUID = smsUUID;
    
    if([smssmsTime isEqualToString:@""])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE MMMM dd yyyy HH:mm:ss"];
        smssmsTime = [dateFormatter stringFromDate:[NSDate date]];
    }
    
    NSDictionary* userInfo = @{
                               @"readStatus": smsreadStatus,
                               @"smsAuther": smssmsAuther,
                               @"smsContent": smssmsContent,
                               @"smsTime": smssmsTime,
                               @"smsUUID": smssmsUUID,
                               @"smsthreadId": smsthreadId
                               };
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"Conversation_update" object:self userInfo:userInfo];
}

#pragma mark - linphone methods
-(void)linphoneSetup
{
    LinphoneManager *instance = [LinphoneManager instance];
    //init logs asap
    [Log enableLogs:[[LinphoneManager instance] lpConfigIntForKey:@"debugenable_preference"]];
    
    BOOL background_mode = YES; //[instance lpConfigBoolForKey:@"backgroundmode_preference"];
    BOOL start_at_boot = NO;//[instance lpConfigBoolForKey:@"start_at_boot_preference"];
    [self registerForNotifications]; // Register for notifications must be done ASAP to give a chance for first SIP register to be done with right token. Specially true in case of remote provisionning or re-install with new type of signing certificate, like debug to release.
    
//        if([twilio_test isEqualToString:@"true"])
//        {
//
//        }
//        else
//        {
    NSString *provider = [Default valueForKey:CallingProvider];
    if([provider isEqualToString:Login_Linphone])
    {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) {
            self.del = [[ProviderDelegate alloc] init];
            [LinphoneManager.instance setProviderDelegate:self.del];
            
        }
    }
//        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max)
//        {
////                NSLog(@"Its wokring call Baby Baby");
//                       self.del_twilio = [[twilio_callkit alloc] init];
//                        [[twilio_callkit sharedInstance] setProviderDelegate:self.del_twilio];
//        }
    
//        }
    
    
    UIApplication *app = [UIApplication sharedApplication];
    UIApplicationState state = app.applicationState;
    
    if (state == UIApplicationStateBackground) {
        // we've been woken up directly to background;
        if (!start_at_boot || !background_mode) {
            // autoboot disabled or no background, and no push: do nothing and wait for a real launch
            //output a log with NSLog, because the ortp logging system isn't activated yet at this time
            NSLog(@"Linphone launch doing nothing because start_at_boot or background_mode are not activated.", NULL);
            
        }
    }
    bgStartId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        LOGW(@"Background task for application launching expired.");
        [[UIApplication sharedApplication] endBackgroundTask:self->bgStartId];
    }];
    
    [LinphoneManager.instance startLinphoneCore];
    [PhoneMainView.instance startUp];
    
    if (bgStartId != UIBackgroundTaskInvalid)
        [[UIApplication sharedApplication] endBackgroundTask:bgStartId];
}
- (void)registrationUpdateEvent:(NSNotification *)notif {
    
    LinphoneProxyConfig *config = linphone_core_get_default_proxy_config(LC);
    if(config != NULL)
    {
         [self proxyConfigUpdate:config];
    }
   
}

- (void)proxyConfigUpdate:(LinphoneProxyConfig *)config {
    
    
    NSString *Userid = [Default valueForKey:USER_ID];
    if (Userid != nil)
    {
    LinphoneRegistrationState state = LinphoneRegistrationNone;
     message = nil;
            state = linphone_proxy_config_get_state(config);
            switch (state)
            {
                case LinphoneRegistrationOk:
                {
                    message = NSLocalizedString(@"Connected", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Trushang Linphon : Connected");
                    NSLog(@"\n \n ");
                    
                    NSString *Userid = [Default valueForKey:USER_ID];
                    if (Userid != nil)
                    {
                        [self soket_disconnect];

                    }
                }
                break;
                case LinphoneRegistrationNone:
                {
                  /*  if([Default valueForKey:kFreeswitchUsername]) {
                       [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                    }*/
                }
                    break;
                case LinphoneRegistrationCleared:
                {
                    message = NSLocalizedString(@"Not connected", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Trushang Linphon : Not connected");
                    NSLog(@"\n \n ");
                    
                    [self soket_disconnect];

                }
                    break;
                case LinphoneRegistrationFailed:  {
                    message = NSLocalizedString(@"Connection failed", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Patel:Trushang Linphon : Connection failed");
                    NSLog(@"\n \n ");
                    
                    
                   
                  /*  if ([Default valueForKey:kFreeswitchUsername]) {
                       [Lin_Utility Lin_call_login:[Default valueForKey:kFreeswitchUsername] domain:[Default valueForKey:kFreeswitchDomain] password:[Default valueForKey:kFreeswitchPassword] type:[Default valueForKey:kIOSSIPPROTOCOL] ];
                    }*/
                    
                    break;
                }
                case LinphoneRegistrationProgress:
                    
                    message = NSLocalizedString(@"Connection in progress", nil);
                    NSLog(@"\n \n ");
                    NSLog(@"Trushang Linphon : Connection in progress");
                    NSLog(@"\n \n ");
                    break;
                default:
                    break;
            }
    
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    localNotification.alertBody = message;
//    localNotification.soundName = UILocalNotificationDefaultSoundName;
//    localNotification.applicationIconBadgeNumber = 0;
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
    }
}


- (void)callUpdate:(NSNotification *)notif
{
    
    NSLog(@"call update app delegate");
    
    LinphoneCall *call = [[notif.userInfo objectForKey:@"call"] pointerValue];
    LinphoneCallState state = [[notif.userInfo objectForKey:@"state"] intValue];
   message = nil;
    
    switch (state) {
        case LinphoneCallIncomingReceived:
            message = @"LinphoneCallIncomingReceived";
             break;
        case LinphoneCallIncomingEarlyMedia:
              message = @"LinphoneCallIncomingEarlyMedia";
             break;
        case LinphoneCallOutgoingInit:
            message = @"LinphoneCallOutgoingInit";
            break;
        case LinphoneCallPausedByRemote:
            message = @"LinphoneCallPausedByRemote";
            break;
        case LinphoneCallConnected: {
           message = @"LinphoneCallConnected";
            break;
        }
        case LinphoneCallStreamsRunning: {
            message = @"LinphoneCallStreamsRunning";
            break;
        }
        case LinphoneCallUpdatedByRemote: {
            message = @"LinphoneCallUpdatedByRemote";
            break;
        }
        case LinphoneCallError: {
            message = @"LinphoneCallError";
            break;
        }
        case LinphoneCallEnd: {
             message = @"LinphoneCallEnd";
            [self soket_disconnect];
            break;
        }
        case LinphoneCallEarlyUpdatedByRemote:
            message = @"LinphoneCallEarlyUpdatedByRemote";
            break;
        case LinphoneCallEarlyUpdating:
            message = @"LinphoneCallEarlyUpdating";
            break;
        case LinphoneCallIdle:
            message = @"LinphoneCallIdle";
            break;
        case LinphoneCallOutgoingEarlyMedia:
            message = @"LinphoneCallOutgoingEarlyMedia";
            break;
        case LinphoneCallOutgoingProgress: {
            message = @"LinphoneCallOutgoingProgress";
            break;
        }
        case LinphoneCallOutgoingRinging:
            message = @"LinphoneCallOutgoingRinging";
            break;
        case LinphoneCallPaused:
            message = @"LinphoneCallPaused";
            break;
        case LinphoneCallPausing:
            message = @"LinphoneCallPausing";
            break;
        case LinphoneCallRefered:
            message = @"LinphoneCallRefered";
            break;
        case LinphoneCallReleased:
            message = @"LinphoneCallReleased";
            break;
        case LinphoneCallResuming: {
            message = @"LinphoneCallResuming";
            break;
        }
        case LinphoneCallUpdating:
            message = @"LinphoneCallUpdating";
            break;
    }
    
   
    
//    NSDictionary *dict = @{@"call" : [NSValue valueWithPointer:call],
//                           @"state" : [NSNumber numberWithInt:state],
//                           @"message" : message};
//
//    NSLog(@"dictionary post notif :: %@",dict);
//
//    [NSNotificationCenter.defaultCenter postNotificationName:@"linphoneCallupdateFromappdelegate" object:self userInfo:dict];
    
    
    
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    localNotification.alertBody = message;
//    localNotification.soundName = UILocalNotificationDefaultSoundName;
//    localNotification.applicationIconBadgeNumber = 0;
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
}
-(void)soket_connection
{
    
    NSString *Userid = [Default valueForKey:USER_ID];
    NSString *Callsid = Call_sid; // [Default valueForKey:CALL_SID];
    message = nil;
    NSLog(@"Weke up function call");
    

    
    
    NSDate *currentDateTime = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSString *currentDatTime = [dateFormatter1 stringFromDate:currentDateTime];
    NSLog(@"Notification Time : %@",currentDatTime);
    if (Call_sid != (id)[NSNull null] && Call_sid != nil )
    {
        [FIRAnalytics logEventWithName:@"notify_time_wakeup" parameters:@{@"notify_time_wakeup": [NSString stringWithFormat:@"%@ %@",Call_sid,currentDatTime]}];
    }else {
        [FIRAnalytics logEventWithName:@"notify_time_wakeup" parameters:@{@"notify_time_wakeup": currentDatTime}];
    }
    
//    if (Userid != nil)
//    {
        if (Callsid != nil)
        {
           
            NSDictionary *passDict = @{@"device":@"ios",
                                       @"userId":[Default valueForKey:USER_ID],
                                       @"callSid":Callsid,
                                       };
            Call_sid = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passDict
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:nil];
            NSString *jsonString = @"";
            if (! jsonData) {
            } else {
                jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
            NSLog(@"jsonString : %@",jsonString);
        
//            if(!socket)
//            {
                NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];
                manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
                socket = manager.defaultSocket;
            
                [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    NSLog(@"\n \n \n \n  socket connected Trushang  wake_up_socketConnection\n \n \n \n ");
                     NSString *Userid1 = [Default valueForKey:USER_ID];
//                    if (Userid1 != nil)
//                    {
                        self->message = @"Wake up call";
                        [self->socket emit:@"ios" with:@[Userid1]];
                        [self->socket emit:@"wakeupcall" with:@[jsonString]];
                 //   }
                    
                    
                }];
                
                [socket on:@"ios2" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    NSLog(@"socket connected Kaushik ios 2 ");
                    [self->socket emit:@"ios1" with:@[@""]];
                    
                    
                }];
//                [socket on:@"disconnect" callback:^(NSArray* data, SocketAckEmitter* ack) {
//                    NSLog(@"\n \n \n \n  socket disconnect Trushang \n \n \n \n ");
//                    [self->socket connect];
//
//                }];
            
                [socket connect];
                
//            }
//            else
//            {
//                self->message = @"Wake up call ---";
//                [self->socket emit:@"wakeupcall" with:@[jsonString]];
////                [socket connect];
//            }
        }
        else
        {
            if(!socket)
            {
                NSURL* url = [[NSURL alloc] initWithString:SOCKET_URL];
                manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
                socket = manager.defaultSocket;
                
                [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    NSLog(@"\n \n \n \n  socket connected Trushang  wake_up_socketConnection\n \n \n \n ");
                    NSString *Userid1 = [Default valueForKey:USER_ID];
                    if (Userid1 != nil)
                    {
                        self->message = @"Soket Connect";
                        [self->socket emit:@"ios" with:@[Userid1]];
                    }
                    
                }];
                
                [socket on:@"ios2" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    NSLog(@"socket connected Kaushik ios 2 ");
                    [self->socket emit:@"ios1" with:@[@""]];
                    
                    
                }];
                [socket on:@"disconnect" callback:^(NSArray* data, SocketAckEmitter* ack) {
                    NSLog(@"\n \n \n \n  socket disconnect Trushang \n \n \n \n ");
                    [self->socket connect];

                }];
                
                [socket connect];
                
            }
            else
            {
                NSString *Userid1 = [Default valueForKey:USER_ID];
//                if (Userid1 != nil)
//                {
                    self->message = @"Soket Connect---";
                    [self->socket emit:@"ios" with:@[Userid1]];
              //  }
//                [self->socket connect];
            }
        }
        

    
}




- (void)soket_disconnect
{
    NSLog(@"\n \n ");
    NSLog(@"SOKET DISCONNECTED");
    NSLog(@"\n \n ");
    [socket disconnect];
    [socket disconnect];
//    [socket disconnect];
//    UIApplication *application = [UIApplication sharedApplication];
//    __block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
//        [application endBackgroundTask:bgTask];
//        [self.socket disconnect];
//        bgTask = UIBackgroundTaskInvalid;
//    }];
}

@end
